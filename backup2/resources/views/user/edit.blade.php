@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8 col-md-push-1">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Create New User</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(array('url' => 'update/MSOX-3592','class'=>'form-horizontal','method'=>'post')) !!}
        <!-- <form action="{{ url('save') }}" class="form-horizontal" method="post"> -->
          <div class="box-body">
            
            <div class="form-group">
              
              <label for="userid" class="col-sm-3 control-label">User Id</label>
              
              <div class="col-sm-7">

                {!! Form::text('userid', $user->userid, array('class' => 'form-control','placeholder' => '(System Generated)','required','id'=>'userid','readonly'))!!}
              
              </div>
            
            </div>
            
            <div class="form-group">
            
              <label for="role" class="col-sm-3 control-label">User Role</label>
            
              <div class="col-sm-7">
                
                <select class="form-control" id="role" name="role"> 
            
                  <option value="0">Select User Role</option>
            
                  @foreach($userrole as $role)
            
                    <option value="{{ $role->roleid }}" {{ ($user->roleid == $role->roleid) ? 'selected=selected' :''}}>{{ $role->rolename }}</option>
            
                  @endforeach
            
                </select>
              
              </div>
            
            </div>
            
            <div class="form-group">
            
              <label for="timezone" class="col-sm-3 control-label">Time Zone</label>
            
              <div class="col-sm-7">
            
                <select class="form-control" id="timezone" name="timezone">
            
                  @foreach($timezone as $zone)
            
                    <option value="{{ $zone->name }}" {{ ($user->timezoneid == $zone->name) ? 'selected=selected' :''}}>({{ $zone->timezonevalue }}) {{ $zone->name }}</option>
            
                  @endforeach
            
                </select>
            
              </div>
            
            </div>            
            
            <div class="form-group">
            
              <label for="pass" class="col-sm-3 control-label">Password</label>
            
              <div class="col-sm-7">
                
                {!! Form::password('pass',array('class'=>'form-control','maxlength'=>'15','placeholder'=>'Enter New Password','id'=>'pass','onchange'=>'checkPassLength()')) !!}

              </div>

            </div>

            <div class="form-group">

              <label for="conpass" class="col-sm-3 control-label">Confirm Password</label>

              <div class="col-sm-7">

                {!! Form::password('conpass',array('class'=>'form-control','maxlength'=>'15','placeholder'=>'Re-Enter Password','id'=>'conpass','onchange'=>'checkPass()')) !!}

              </div>

            </div>

            <div class="form-group">

              <label for="status" class="col-sm-3 control-label">Status</label>

              <div class="col-sm-7">

                {!! Form::select('status',array('1'=>'Enable','0'=>'Disabled'),null,array('class'=>'form-control','id'=>'status')) !!}

              </div>

            </div>

            <div class="form-group">

              <label for="indmultipleSelect" class="col-sm-3 control-label">Territory</label>

              <div class="col-sm-7">

                <select multiple="" class="form-control industry" id="indmultipleSelect" name="selectedindustry[]">
                  <option value="">Select Industries</option>
                  <option value="3">Test</option>
                  <option value="4">Test 2</option>
                  <option value="1">Test Industry</option>
                </select>

              </div>

            </div>

            <div class="form-group">
              <label for="cat" class="col-sm-3 control-label"></label>
              <div class="col-sm-7">
                <select multiple="" class="form-control category" id="cat" name="selectcategory[]">
                  <option>Select Categories</option>
                  <option value="1">Select 1</option>
                  <option value="2">Select 2</option>
                  <option value="3">Select 3</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="subcat" class="col-sm-3 control-label"></label>
              <div class="col-sm-7">
                <select multiple="" class="form-control" id="subcat" name="selectsubcat[]">
                  <option>Select Sub Categories</option>
                  <option value="1">Select 1</option>
                  <option value="2">Select 2</option>
                  <option value="3">Select 3</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="brands" class="col-sm-3 control-label"></label>
              <div class="col-sm-7">
                <select multiple="" class="form-control" id="brands" name="selectbrands[]">
                  <option>Select Brands</option>
                  <option value="1">Select 1</option>
                  <option value="2">Select 2</option>
                  <option value="3">Select 3</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="authpass" class="col-sm-3 control-label">Authorize Changes</label>
              <div class="col-sm-7">

                {!! Form::password('authpass',array('class'=>'form-control','id'=>'authpass','placeholder'=>'Enter Password')) !!}

                {!! Form::hidden('adduser','adduser',array('id'=>'adduser')) !!}

              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <a href="{{url('userdetails')}}" class="btn btn-default">Cancel</a>
            {!! Form::submit('Submit',array('class'=>'btn btn-info pull-right','id'=>'submitButton')) !!}
            <!-- <button type="submit" class="btn btn-info pull-right" id="submitButton">Submit</button> -->
          </div><!-- /.box-footer -->
        </form>
      </div><!-- /.box -->
    </div> 
  </div>
</section>
@endsection