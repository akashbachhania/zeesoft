@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
      </div>
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Users</h3>
          <a href="{{ url('create') }}" class="btn btn-success pull-right">Create New User</a>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table id="userdetail" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th></th>
                <th>User ID</th>
                <th>User Role</th>
                <th>Creation Date</th>
                <th>Last Login</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach ($users as $user)
              <tr>
                <td>
                  <input name="checkUser" type="checkbox" value="{{ $user->userid }}">
                </td>
                <td>{{ $user->userid }}</td>
                <td></td>
                <td>{{ date('M d Y', strtotime($user->creationdate) ) }}</td>
                <td>{{ $user->userid }}</td>
                <td>{{ ($user->isactive) ? "Active" : "Inactive" }}</td>
                <td>

                  @if ($user->roletype == 'AM')


                  @if ( Session::has('roletype') =='AD')
                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;
                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">ACTIVITY</a>&nbsp;|&nbsp;
                  <a href="{{ url('report') }}/{{ Crypt::encrypt($user->userid) }}">REPORTS</a>&nbsp;|&nbsp;
                  <a href="{{ url('customers') }}/{{ Crypt::encrypt($user->userid) }}">CUSTOMERS</a>&nbsp;|&nbsp;
                  <a href="{{ url('posting') }}/{{ Crypt::encrypt($user->userid) }}">POSTINGS</a>&nbsp;|&nbsp;
                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>';
                  @else

                  @if($checkVal[0]['EditP']=='1')

                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;
                  @endif
                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;
                  <a href="{{ url('report') }}/{{ Crypt::encrypt($user->userid) }}">REPORTS</a>&nbsp;|&nbsp;
                  <a href="{{ url('customers') }}/{{ Crypt::encrypt($user->userid) }}">CUSTOMERS</a>&nbsp;|&nbsp;
                  <a href="{{ url('posting') }}/{{ Crypt::encrypt($user->userid) }}">POSTINGS</a>&nbsp;|&nbsp;
                  @if($checkVal[0]['DeleteP']=='1')

                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>
                  @endif

                  @endif

                  @endif


                  @if ($user->roletype == 'AD')

                  @if ( Session::has('roletype')=='AD')
                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;
                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;

                  @else

                  @if($checkVal[0]['EditP']=='1')

                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;

                  @endif

                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;

                  @endif

                  @endif



                  @if ($user->roletype == 'SA')

                  @if ( Session('roletype') == 'AD')
                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;
                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;
                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>
                  @else

                  @if($checkVal[0]['EditP']=='1')

                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;

                  @endif

                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;

                  @if($checkVal[0]['DeleteP']=='1')

                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>

                  @endif

                  @endif
                  @endif


                  @if ($user->roletype == 'QF')

                  @if ( Session('roletype')=='AD')

                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;
                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;
                  <a href="{{ url('report') }}/{{ Crypt::encrypt($user->userid) }}">REPORTS</a>&nbsp;|&nbsp;
                  <a href="{{ url('quotes') }}/{{ Crypt::encrypt($user->userid) }}">QUOTES</a>&nbsp;|&nbsp;
                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>

                  @else

                  @if($checkVal[0]['EditP']=='1')

                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;

                  @endif

                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;
                  <a href="{{ url('report') }}/{{ Crypt::encrypt($user->userid) }}">REPORTS</a>&nbsp;|&nbsp;
                  <a href="{{ url('quotes') }}/{{ Crypt::encrypt($user->userid) }}">QUOTES</a>&nbsp;|&nbsp;
                  @if($checkVal[0]['DeleteP']=='1')

                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>

                  @endif

                  @endif

                  @endif



                  @if ($user->roletype == 'TM')

                  @if ( Session::has('roletype')=='AD')
                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;
                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;
                  <a href="{{ url('report') }}/{{ Crypt::encrypt($user->userid) }}">REPORTS</a>&nbsp;|&nbsp;
                  <a href="{{ url('order/acceptedoffers') }}">ACCEPTED OFFERS</a>&nbsp;|&nbsp;
                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>
                  @else

                  @if($checkVal[0]['EditP']=='1')

                  <a href="{{ url('edit') }}/{{ Crypt::encrypt($user->userid) }}">EDIT</a>&nbsp;|&nbsp;

                  @endif

                  <a href="{{ url('activity') }}/{{ Crypt::encrypt($user->userid) }}">Activity</a>&nbsp;|&nbsp;
                  <a href="{{ url('report') }}/{{ Crypt::encrypt($user->userid) }}">REPORTS</a>&nbsp;|&nbsp;
                  <a href="{{ url('order/acceptedoffers') }}">ACCEPTED OFFERS</a>&nbsp;|&nbsp;
                  @if($checkVal[0]['DeleteP']=='1')

                  <a  href="{{ url('delete') }}/{{ Crypt::encrypt($user->userid) }}">DELETE</a>

                  @endif

                  @endif

                  @endif                            

                </td>
              </tr>

              @endforeach
              <tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
    </section>
    @endsection