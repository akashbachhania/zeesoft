@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div>
        <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <table id="userposting" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Posting ID</th>
                        <th>Date Posted</th>
                        <th>Brand</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Product Name</th>
                        <th> Price </th>
                        <th> Curr </th>
                        <th> User Id </th>
                        <th> Cust Ref</th>
                        <th> Status</th>
                        <th> Action</th>
                      </tr>
                    </thead>
                     <tbody>
                     @foreach( $posting as $post )
                        <tr>
                          <td>{{$post->postno}}</td>
                          <td>{{ date('F d Y - h:i A',strtotime($post->pdate))}}</td>
                          <td>{{ mb_convert_encoding($post->brand, "HTML-ENTITIES", "utf8") }}</td>
                          <td>{{ strtoupper( $post->ptype ) }}</td>
                          <td>{{ $post->quantity }}</td>
                          <td>{{ mb_convert_encoding($post->productName, "HTML-ENTITIES", "utf8") }}</td>
                          <td>{{ $post->targetprice }}</td>
                          <td>{{ $post->currency }}</td>
                          <td>{{ $post->userid }}</td>
                          <td>{{ $post->customerrefno }}</td>
                          
                                  if ( ($useridstataus=='1') && ($act_inactive=='0') && ($productstatus!='REJECTED') && ($customerstatus=='1') && ($poststatus !='EXPIRED')) {
                                          $enableaction = '<span class="label label-sm label-success"> Enable </span>';
                                      } else {
                                  if($poststatus =='EXPIRED')
                                  {
                                          $enableaction = '<span class="label label-sm label-danger">EXPIRED</span>';
                                  }
                                  else
                                  {
                                  $enableaction = '<span class="label label-sm label-danger">Disable</span>';
                                  }
                                      }
                          <td>{{ $post-> }}</td>
                          <td>{{ $post-> }}</td>
                        </tr>

                    <tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    </div>
  </div>
</section>
@endsection