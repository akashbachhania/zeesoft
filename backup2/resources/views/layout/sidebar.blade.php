<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/admin-lte/dist/img/user-logo.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Welcome {{ Auth::user()->userid }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
          </div>
      </form>
      <!-- /.search form -->


      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- <li class="header"></li> -->
        <!-- Optionally, you can add icons to the links -->
        <li class=""><a href="#"><span>Dashboard</span></a></li>
        
        @if( in_array( 'Customers',Session::get('module') ) )
            
            <li class="treeview">
                <a href="#"><span>Customers</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  @if( in_array( 'Browse Customers',Session::get('displayname') ) )

                    <li><a href="#">Browse Customers</a></li>
                  
                  @endif

                </ul>
            </li>

        @endif
        
        @if( in_array( 'Product Catalog',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Product Catalog</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @if( in_array( 'Add New Product',Session::get('displayname') ) )  
                    
                    <li><a href="#">Add New Product</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Browse Products',Session::get('displayname') ) )
                  
                    <li><a href="#">Browse Products</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Suggested Product',Session::get('displayname') ) )
                  
                    <li><a href="#">Suggested Products</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Suggested Product Code',Session::get('displayname') ) )
                  
                    <li><a href="#">Suggested Products Codes</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Reported Product',Session::get('displayname') ) )
                  
                    <li><a href="#">Reported Products</a></li>
                  
                  @endif
                
                </ul>
            </li>

        @endif
        
        @if( in_array( 'Postings',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Postings</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                  @if( in_array( 'Browse Postings',Session::get('displayname') ) )  
                  
                    <li><a href="#">Browse Postings</a></li>
                  
                  @endif
                  
                  @if( in_array( 'My Postings',Session::get('displayname') ) )
                  
                    <li><a href="#">My Postings</a></li>
                  
                  @endif

                  @if( in_array( 'My Offers',Session::get('displayname') ) )
                  
                    <li><a href="#">My Offers</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Import Postings',Session::get('displayname') ) )
                  
                    <li><a href="#">Import Posting</a></li>
                  
                  @endif
                
                </ul>
            </li>

        @endif

        @if( in_array( 'Product List Download',Session::get('module') ) )
        
            <li class="treeview">
                <a href="#"><span>Product List Download</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                  @if( in_array( 'Products List Download',Session::get('displayname') ) )

                    <li><a href="#">Products List Download</a></li>
                  
                  @endif

                </ul>
            </li>
        
        @endif

        @if( in_array( 'Quote Queue',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Quote Queue</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                  @if( in_array( 'Quote Queue',Session::get('displayname') ) )  

                    <li><a href="#">Quote Queue</a></li>

                  @endif

                </ul>
            </li>

        @endif

        @if( in_array( 'Orders',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Orders</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                  @if( in_array( 'Accepted Offers',Session::get('displayname') ) )

                    <li><a href="#">Accepted Offers</a></li>
                  
                  @endif

                  @if( in_array( 'Completed Offers',Session::get('displayname') ) )

                    <li><a href="#">Completed Orders</a></li>
                  
                  @endif

                  @if( in_array( 'Cancelled Orders',Session::get('displayname') ) )

                    <li><a href="#">Cancelled Orders</a></li>
                  
                  @endif

                </ul>
            </li>

        @endif

        @if( in_array( 'Users',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                  @if( in_array( 'View Users',Session::get('displayname') ) )
                  
                    <li><a href="{{url('userdetails')}}">View Users</a></li>
                  
                  @endif

                  @if( in_array( 'Users Activity',Session::get('displayname') ) )

                    <li><a href="{{ url('activity') }}">User Activity</a></li>
                  
                  @endif

                  @if( in_array( 'Create New User',Session::get('displayname') ) )

                    <li><a href="{{url('create')}}">Create New Users</a></li>
                
                  @endif
                    
                </ul>
            </li>

        @endif

        @if( in_array( 'Settings',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @if( in_array( 'My Settings',Session::get('displayname') ) )

                    <li><a href="#">My Settings</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Notifications',Session::get('displayname') ) )

                    <li><a href="#">Notifications</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Import Data',Session::get('displayname') ) )

                    <li><a href="#">Import Data</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Industry List',Session::get('displayname') ) )

                    <li><a href="#">Industry List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Category List',Session::get('displayname') ) )

                    <li><a href="#">Category List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Sub Category List',Session::get('displayname') ) )
                  
                    <li><a href="#">Sub Category List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Brands List',Session::get('displayname') ) )  
                  
                    <li><a href="#">Brands List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Location',Session::get('displayname') ) )  
                  
                    <li><a href="#">Location</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Security Question List',Session::get('displayname') ) )  
                  
                    <li><a href="#">Security Question List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Terms & Conditions',Session::get('displayname') ) )  
                  
                    <li><a href="#">Terms & Conditions</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Database Backup',Session::get('displayname') ) )  
                  
                    <li><a href="#">Database Backup</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Database Restore',Session::get('displayname') ) )  
                  
                    <li><a href="#">Database Restore</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Website Settings',Session::get('displayname') ) )  
                  
                    <li><a href="#">Website Setting</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Menu Permission',Session::get('displayname') ) )  
                  
                    <li><a href="#">Menu Permission</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Role List',Session::get('displayname') ) )  
                  
                    <li><a href="#">Role List</a></li>
                  
                  @endif  
                
                </ul>

            </li>

        @endif

        @if( in_array( 'Reports',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @if( in_array( 'Reports',Session::get('displayname') ) )

                    <li><a href="#">Reports</a></li>
                
                  @endif
                
                </ul>
            </li>

        @endif
    </ul><!-- /.sidebar-menu -->

</section>
<!-- /.sidebar -->
</aside>