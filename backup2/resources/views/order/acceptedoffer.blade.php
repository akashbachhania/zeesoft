@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div>
        <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <table id="acceptedoffers" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Quote ID</th>
                        <th>Date Posted</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Curr</th>
                        <th>TimeFrame</th>
                        <th>Exp Date</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                     <tbody>

                      @foreach ($orders as $order)

                        <tr>
                          <td>{{ $order->quotationno }}</td>
                          <td>{{ date('F d Y - h:i A',strtotime($order->acc_date)) }} EST</td>
                          <td>{{ ucwords(strtolower($row["ptype"])) }}</td>
                          <td>{{ $order->quantity }}</td>
                          <td>{{ mb_convert_encoding($order->productName, "HTML-ENTITIES", "utf8") }}</td>
                          <td>{{ $order->price }}</td>
                          <td>{{ $order->currency }}</td>
                          <td>{{ $order->timeframe }}</td>
                          <td>{{ $order->expdate }}</td>
                          <td>{{ $order->status }}</td>
                          
                        </tr>

                      @endforeach
                    <tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    </div>
  </div>
</section>
@endsection