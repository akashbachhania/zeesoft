<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'mst_userlogin';
    protected $primaryKey = 'userid';
   
    public function userLogon()
    {
      return $this->hasMany('App\UserLogon','userid');
    }

    public function userRole()
    {
      return $this->hasOne('App\UserRole');
    }
}
