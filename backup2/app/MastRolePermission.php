<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MastRolePermission extends Model
{
    protected $table = 'mastrolepermission';

    function mastpage(){
    	return $this->belongsTo('App\MastPage','PageId','PageId');
    }
}
