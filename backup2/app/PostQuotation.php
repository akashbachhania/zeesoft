<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostQuotation extends Model
{
    protected $table = 'post_quotation';
}
