<?php

namespace App;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'mst_subcategory';

    function category(){
    	return $this->belongsTo('App\Category','catid','catid');
    }
}
