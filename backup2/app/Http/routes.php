<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
    
Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/','Auth\AuthController@showloginform');
    Route::get('dashboard', function () {

    	return view('layout.master')->with('title','Dashboard');
	});

	Route::get('userdetails','UserController@index');



	Route::get('activity','UserController@activity');
	Route::get('activity/{id}','UserController@activity');
	Route::get('create','UserController@create');
	Route::get('exportActivity','UserController@exportActivity');
	Route::get('exportActivity/{id}','UserController@exportActivity');
	Route::post('save','UserController@store');
	Route::get('checkauthpass','UserController@checkAuthPass');
    Route::get('edit/{id}', 'UserController@edit');
    Route::post('update/{id}','UserController@update');
    Route::get('delete/{id}','UserController@destroy');


    Route::get('order/acceptedoffers','OrderController@index');
    Route::get('report/{id}','ReportController@index');


    Route::get('posting/{id}','PostingController@index');
});
