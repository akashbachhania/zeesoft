<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MstProduct;
use App\PostQuotation;
use App\TMAcceptedOffers;
use DB;
class OrderController extends Controller
{
    
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = DB::table('tm_accepted_offers as ta')
                ->join('mst_product as mp','ta.productno','=','mp.productno')
                ->join('post_advertisment as pa','ta.postno', '=', 'pa.postno')
                ->join('post_quotation as pq','ta.quoteid', '=', 'pq.quoteid')
                ->select('ta.acc_offr_id', 'ta.counterid', 'ta.quotationno', 'mp.productno','mp.productid', 'ta.quoteid','pa.postno', 'ta.postno', 'ta.productno', 'ta.acceptedBy', 'ta.acc_date', 'ta.acc_price as price', 'ta.acc_quantity as quantity', 'ta.acc_timeframe as timeframe', 'ta.acc_expdate as expdate', 'ta.status', 'ta.noti_status','pa.ptype','mp.name as productName','pq.currency')
                ->get();

        // echo "<pre>"; print_r($order);die;
        return View('order.acceptedoffer',['orders'=>$order])->with('title','Accepted Offers');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
