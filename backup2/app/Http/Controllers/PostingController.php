<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Crypt;
class PostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   


        $posting = DB::table('post_advertisment as pa')
                ->leftjoin('mst_product as mp','pa.productno','=','mp.productno')
                ->leftjoin('mst_industry as i','mp.industryid','=','i.industryid')
                ->leftjoin('mst_category as c','mp.catid', '=', 'c.catid')
                ->leftjoin('mst_userlogin as mu','pa.userid', '=', 'mu.userid')
                ->leftjoin('post_customer as cu','pa.customerrefno', '=', 'cu.refno')
                ->leftjoin('mst_brand as b','mp.brandid', '=', 'b.brandid')
                ->select('pa.postno','pa.postid','pa.userid','pa.pdate','pa.ptype','pa.quantity','pa.timeframe','pa.targetprice','pa.currency','pa.customerrefno','mp.productid','mp.name as productName', 'i.name AS industry', 'c.name AS category', 'b.name AS brand','mu.isactive as useridstataus','mp.pstatus as productstatus','pa.pstatus as poststatus','cu.isactive as customerstatus','pa.act_inactive')
                ->where('pa.userid',Crypt::decrypt(\Request::segment(2)))
                ->where('pa.pstatus','!=','Completed')
                ->where('pa.pstatus','!=','CANCELLED')
                ->get();
        return View('posting.userposting',$posting)->with('title','User Postings');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
