<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\userLogon;
use App\userRole;
use App\UserActivity;
use App\Timezone;
use App\Industry;
use App\Category;
use App\SubCategory;
use App\Brand;
use App\MastRolePermission;
use App\PostAdverstisement;
use DB;
use Crypt;
use View;
use Session;
use App\MastPage;
use Illuminate\Support\Facades\Redirect;
use DateTimeZone;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::all();

        $result = $this->checkPermission();

        return View::make('user.index', array('users' => $users, 'checkVal' => $result))->with('title','Users');

        //return View('user.index', ['users' => $users,'checkVal'=>$result])->with('title','Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $userRole = UserRole::select('rolename','roleid')->get();

        $sysgenerateid = $this->sysgenerateid();

        $timezone = Timezone::select('name','timezonevalue')->where('isactive','1')->orderby('timezonevalue')->get();

        // $r = Industry::with('category')->get();
        // foreach ($r as $key => $value) {
        //     echo "<pre>"; print_r($value);
        // }

        // die;
        $data = array(
            'sysgenerateid'=> $sysgenerateid,
            'userrole'     => $userRole,
            'timezone'     => $timezone
            );

        return View('user.create',$data)->with('title','Create User');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        if ( $request->input('adduser') != null ) {
            // echo "<pre>"; print_r($request->input());die;            
            $indtemp    = '';
            $industries = $request->input('selectedindustry');
            if(count($industries) > 0){
                while (list($key, $val) = each($industries)) {
                    if (empty($indtemp)) {
                        $indtemp = $val;
                    } else {
                        $indtemp = $indtemp . ',' . $val;
                    }
                }
            }


            $catetemp   = '';
            $categories = $request->input('selectcategory');
            if(count($categories) > 0){
                while (list($key, $val) = each($categories)) {
                    if (empty($catetemp)) {
                        $catetemp = $val;
                    } else {
                        $catetemp = $catetemp . ',' . $val;
                    }
                }
            }

            $subcatetemp   = '';
            $subcategories = $request->input('selectsubcat');
            if(count($subcategories) > 0){
                while (list($key, $val) = each($subcategories)) {
                    if (empty($subcatetemp)) {
                        $subcatetemp = $val;
                    } else {
                        $subcatetemp = $subcatetemp . ',' . $val;
                    }
                }
            }

            $brandtemp = '';
            $brands = $request->input('selectbrands');
            if(count($brands) > 0){
                while (list($key, $val) = each($brands)) {
                    if (empty($brandtemp)) {
                        $brandtemp = $val;
                    } else {
                        $brandtemp = $brandtemp . ',' . $val;
                    }
                }
            }
            
            $rolecode = UserRole::select('code')->where('roleid',$request->input('role'))->get();
            
            $user = new User;

            $user->roleid = $request->input('role');
            $user->roletype = $rolecode[0]->code;
            $user->userid = $request->input('userid');
            $user->password = bcrypt($request->input('pass'));
            $user->authorizedpass = bcrypt($request->input('authpass'));
            $user->timezoneid = $request->input('timezone');
            $user->isactive = $request->input('status');
            
            date_default_timezone_set('EST');
            
            $user->creationdate = date('Y-m-d H:i:sa');
            $user->industry = $indtemp;
            $user->category = $catetemp;
            $user->subcategory = $subcatetemp;
            $user->brand = $brandtemp;
            $user->save();


            $useraction = new UserActivity;

            $useraction->userid = $request->input('userid');
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = Session::get('_token');
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Added User';

            $useraction->save();
            return redirect('userdetails');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {      

      $user = User::find(Crypt::decrypt($id));
      
      $userRole = UserRole::select('rolename','roleid')->get();

      $timezone = Timezone::select('name','timezonevalue')->where('isactive','1')->orderby('timezonevalue')->get();



      $data = array(
            'user'         => $user,
            'userrole'     => $userRole,
            'timezone'     => $timezone
            );

      return View::make('user.edit',$data)->with('title','Edit User');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( $request->input('adduser') != null ) {
            // echo "<pre>"; print_r($request->input());die;            
            $indtemp = $catetemp = $subcatetemp = '';
            $industries = $request->input('selectedindustry');
            if(count($industries) > 0){
                while (list($key, $val) = each($industries)) {
                    if (empty($indtemp)) {
                        $indtemp = $val;
                    } else {
                        $indtemp = $indtemp . ',' . $val;
                    }
                }
            }


            $categories = $request->input('selectcategory');
            if(count($categories) > 0){
                while (list($key, $val) = each($categories)) {
                    if (empty($catetemp)) {
                        $catetemp = $val;
                    } else {
                        $catetemp = $catetemp . ',' . $val;
                    }
                }
            }

            $subcategories = $request->input('selectsubcat');
            if(count($subcategories) > 0){
                while (list($key, $val) = each($subcategories)) {
                    if (empty($subcatetemp)) {
                        $subcatetemp = $val;
                    } else {
                        $subcatetemp = $subcatetemp . ',' . $val;
                    }
                }
            }

            $brandtemp = '';
            $brands = $request->input('selectbrands');
            if(count($brands) > 0){
                while (list($key, $val) = each($brands)) {
                    if (empty($brandtemp)) {
                        $brandtemp = $val;
                    } else {
                        $brandtemp = $brandtemp . ',' . $val;
                    }
                }
            }
            
            $rolecode = UserRole::select('code')->where('roleid',$request->input('role'))->get();
            
            $user = User::find($id);

            $user->roleid = $request->input('role');
            $user->roletype = $rolecode[0]->code;
            $user->password = $request->input('pass');
            $user->authorizedpass = $request->input('authpass');
            $user->timezoneid = $request->input('timezone');
            $user->isactive = $request->input('status');
            
            date_default_timezone_set('EST');
            
            $user->creationdate = date('Y-m-d H:i:sa');
            $user->industry = $indtemp;
            $user->category = $catetemp;
            $user->subcategory = $subcatetemp;
            $user->brand = $brandtemp;
            $user->save();


            $useraction = new UserActivity;

            $useraction->userid = $request->input('userid');
            $useraction->ipaddress = \Request::ip();
            $useraction->sessionid = 'asdaksjhfkjnvdweiufh23rkjflshkj';
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Added User';

            $useraction->save();
            return redirect('userdetails');
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null){

        $userid = Crypt::decrypt($id);
        
        $post_advertisement = PostAdverstisement::where('userid',$userid)->count();
        
        if( $post_advertisement > 0 ){

            Session::flash('alert-danger', 'Cannot delete User, active data exists.');
            
            return Redirect::to(url('userdetails'));
        
        }
        else{

            $user = User::find($userid);
            $user->delete();

            $insertAction = new UserActivity;
            $insertAction->userid = $userid;
            $insertAction->ipaddress = \Request::ip();
            $insertAction->sessionid = Session::get('_token');
            date_default_timezone_set('EST');
  
            $insertAction->actiondate = date('Y-m-d H:i:sa');
            $insertAction->actiontime = date('Y-m-d H:i:sa');
            $insertAction->actionname = 'Deleted User';
            $insertAction->save();

            Session::flash('alert-danger', 'Related Record Delete.');
            
            return Redirect::to(url('userdetails'));

        }

    }

    public function activity($id = null){
        if($id != null){
            $user_activity = UserActivity::where('userid',Crypt::decrypt($id))->get();
        }
        else{
            $user_activity = UserActivity::all();    
        }
        
        
        return View('user.activity',['user_activity'=>$user_activity])->with('title','Users Activity');        
    }

    public function exportActivity($id = null){
        if($id != null){
            $users = UserActivity::select('userid', 'actiontime', 'ipaddress', 'actionname')->where('userid',Crypt::decrypt($id))->get();
        }
        else{
            $users = UserActivity::select('userid', 'actiontime', 'ipaddress', 'actionname')->get();    
        }
        

        header('Content-Disposition: attachment; filename="export.csv"');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary\n");

        \Excel::create('users', function($excel) use($users) {
            $excel->sheet('Sheet 1', function($sheet) use($users) {
                $sheet->fromArray($users);
            });
        })->export('csv');
    }

    public function randStringAlpha($len = 4){
        $str   = "";
        $chars = array_merge(range('A', 'Z'));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
    public function randStringNumeric($len = 4){
        $str   = "";
        $chars = array_merge(range(0, 9));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
    public function sysgenerateid(){
        
        $userid = $this->randStringAlpha() . "-" . $this->randStringNumeric();
        return $userid;
    }

    public function checkAuthPass(Request $request){
        $authpass_input = $request->input('authpass');
        $authpass = User::select('authorizedpass')->where('userid',Auth::user()->userid)->get();
       
        if( \Hash::check($authpass_input,$authpass[0]->authorizedpass) ){
            echo 'success';
        }else{
            echo 'failure';
        }
    }

    public function checkPermission(){

        $data = MastRolePermission::select('RoleId','ViewP','AddP','EditP','DeleteP')->where('RoleId',Auth::user()->roleid)->where('PageId','42')->get();
        return $data;
    }

}