<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $table = 'mst_industry';

    function category(){
    	return $this->hasMany('App\Category','industryid','industryid');
    }
}
