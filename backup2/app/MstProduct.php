<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProduct extends Model
{
    protected $table = 'mst_product';
}
