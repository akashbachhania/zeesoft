<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MastPage extends Model
{
    protected $table = 'mastpage';
}
