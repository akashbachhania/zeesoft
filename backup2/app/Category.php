<?php

namespace App;

use App\Industry;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'mst_category';

    function industry(){
    	return $this->belongsTo('App\Industry');
    }
    function subcategory(){
    	return $this->hasMany('App\SubCategory','catid','catid');
    }
}
