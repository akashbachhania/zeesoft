if(String(window.location).match(/userdetails/)){
	$('#userdetail').dataTable();
}
else if( String(window.location).match(/posting/) ){

	$('#userposting').dataTable();

}
else if( String(window.location).match(/acceptedoffers/) ){

	$('#acceptedoffers').dataTable();

}
else if(String(window.location).match(/activity/)){
	$('#useractivity').dataTable();
	$('#exportcsv').click(function() {

	        $.ajax({
	            url: 'http://localhost/zeesoft/public/exportActivity',
	            success: function (resp) {
	            	
	            }
	        });
	    
	});
}
else if(String(window.location).match(/create/) || String(window.location).match(/edit/) ){

    function checkPass(){
        var pass = $("#pass").val();
        var conpass = $("#conpass").val();
        
        if (pass != conpass){
        	alert('Passwords do not Match!');
            $("#conpass").parent().parent().addClass('has-error');
            $("#conpass").focus();
            return false;
        }
        $("#conpass").parent().parent().removeClass('has-error');
        return true;

    }

    function checkPassLength(){
        var s = $('#pass').val();
        // check for null or too short
        if (!s || s.length < 6) {
            alert("Pasword must have at least 6 characters."); // prompt user
            $("#pass").parent().parent().addClass('has-error');
            $("#pass").focus();
            return false;
        }
        // check for a number
        else if (/[0-9]/.test(s) === false) {
            alert("Pasword must have at least one number."); // prompt user
            $("#pass").parent().parent().addClass('has-error');
            $("#pass").focus();
        	return false;
        }
        // check for a capital letter
        else if (/[a-zA-Z]/.test(s) === false) {
            alert("Pasword must have at least one letter."); // prompt user
            $("#pass").parent().parent().addClass('has-error');
            $("#pass").focus();
            return false;
        }
        $("#pass").parent().parent().removeClass('has-error');
        return true; // return true if all conditions are met
    }

    $("#role,#authpass").change(function(){
    	$(this).parent().parent().removeClass('has-error');
    });

    $('#submitButton').click(function(event){
    	event.preventDefault();
	    var authpass = "password123";
	    
	    if ($("#role").val() == 0){
	
	        $("#role").parent().parent().addClass('has-error');
	        alert("User Role Required"); // prompt user
	        $("#role").focus(); //set focus back to control
	        return false;
	
	    }
	
	    if ($("#role").val() ==1)
	    {
	        var options = $('#indmultipleSelect > option:selected');
	        if (options.length == 0) {
	            alert('Industries are Required');
	            $("#indmultipleSelect").focus();
	            return false;
	        }
	    }

	    if ($("#authpass").val() == '') {
	        alert('Authorized code is required');
	        $("#authpass").parent().parent().addClass('has-error');
	        $("#authpass").focus();
	        return false;
	    } else {
	        $.get('http://localhost/zeesoft/public/checkauthpass',{authpass:$("#authpass").val()},function(data){
	        	// alert(data);
	        	alert(data);
	        	if(data == 'success'){
	        		$("form").submit();
	        	}
	        	else{
	        		alert('Authorized Password Incorrect!');
	        		$("#authpass").parent().parent().addClass('has-error');
	        		$("#authpass").focus();
	        	}
	        	
	        });
	    }
        
    });
        

}