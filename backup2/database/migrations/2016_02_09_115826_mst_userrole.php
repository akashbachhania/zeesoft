<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstUserrole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_userrole', function (Blueprint $table) {
            $table->increments('roleid');
            $table->string('name',100);
            $table->string('isactive',1)->default(1);
            $table->string('code',10);
            $table->string('rolename',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
