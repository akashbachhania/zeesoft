<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MastPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MastPage', function (Blueprint $table) {
            $table->increments('PageId');
            $table->string('PageName',50);
            $table->string('Module',50);
            $table->string('DisplayName',50);
            $table->integer('Parent_Id');
            $table->integer('Level_Idx');
            $table->integer('Idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
