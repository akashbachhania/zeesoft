<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_customer', function (Blueprint $table) {
            $table->increments('custid');
            $table->string('refno',50);
            $table->date('edate');
            $table->string('refuserid',25);
            $table->string('isactive',1)->default(1);
            $table->string('roletype',10);
            $table->string('adduserid',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
