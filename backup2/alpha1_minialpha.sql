-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2016 at 05:48 PM
-- Server version: 5.5.48-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alpha1_minialpha`
--

DELIMITER $$
--
-- Procedures
--
$$

$$

$$

$$

$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `countdata`
--

CREATE TABLE IF NOT EXISTS `countdata` (
  `id` int(11) NOT NULL,
  `recordcount` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countdata`
--

INSERT INTO `countdata` (`id`, `recordcount`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `count_notification`
--

CREATE TABLE IF NOT EXISTS `count_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) NOT NULL,
  `tmsg` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `customer_products`
--
CREATE TABLE IF NOT EXISTS `customer_products` (
`refno` varchar(50)
,`productid` int(11)
,`product_name` varchar(250)
,`productno` varchar(25)
,`industryid` int(11)
,`brandid` int(11)
,`catid` int(11)
,`subcatid` int(11)
,`caseweight` varchar(100)
,`description` text
,`manufacture` varchar(100)
,`custid` int(11)
,`industry_name` varchar(100)
,`category_name` varchar(100)
,`subcategory_name` varchar(100)
,`brand_name` varchar(100)
,`qtypercase` int(11)
,`pakaging` varchar(100)
,`shipingcondition` varchar(100)
,`code1` int(11)
,`codevalue1` varchar(100)
,`code2` int(11)
,`codevalue2` varchar(100)
,`code3` int(11)
,`codevalue3` varchar(100)
,`ptype` varchar(5)
,`targetprice` int(11)
,`currency` varchar(10)
,`uom` varchar(50)
,`quantity` int(11)
,`location` varchar(50)
,`expdate` varchar(50)
,`expirydate` varchar(50)
,`customerrefno` varchar(50)
,`post_advertisment_pakaging` text
,`post_advertisment_language` varchar(200)
,`post_advertisment_timeframe` varchar(50)
,`post_advertisment_country` varchar(200)
,`post_advertisment_userid` varchar(50)
,`pdate` date
,`post_advertisment_pstatus` varchar(25)
,`post_advertisment_postno` varchar(6)
);
-- --------------------------------------------------------

--
-- Table structure for table `maskproname`
--

CREATE TABLE IF NOT EXISTS `maskproname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maskstatus` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `maskproname`
--

INSERT INTO `maskproname` (`id`, `maskstatus`) VALUES
(1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `MastPage`
--

CREATE TABLE IF NOT EXISTS `MastPage` (
  `PageId` int(5) NOT NULL AUTO_INCREMENT,
  `PageName` varchar(50) NOT NULL,
  `Module` varchar(50) NOT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Parent_Id` int(5) NOT NULL,
  `Level_Idx` int(5) NOT NULL,
  `Idx` int(5) NOT NULL,
  PRIMARY KEY (`PageId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `MastPage`
--

INSERT INTO `MastPage` (`PageId`, `PageName`, `Module`, `DisplayName`, `Parent_Id`, `Level_Idx`, `Idx`) VALUES
(42, 'userdetail.php', 'Users', 'View Users', 41, 2, 1),
(3, '', 'Customers', 'Customers', 0, 1, 0),
(4, 'mstcustomer.php', 'Customers', 'Browse Customers', 3, 2, 1),
(5, '', 'Product Catalog', 'Product Catalog', 0, 1, 0),
(6, 'add-product.php', 'Product Catalog', 'Add New Product', 5, 2, 1),
(7, 'product-catalog.php', 'Product Catalog', 'Browse Products', 5, 2, 2),
(8, 'suggested-product.php', 'Product Catalog', 'Suggested Products', 5, 2, 3),
(9, 'suggested-product-code.php', 'Product Catalog', 'Suggested Product Codes', 5, 2, 4),
(10, 'reported-product.php', 'Product Catalog', 'Reported Products', 5, 2, 5),
(11, '', 'Postings', 'Postings', 0, 1, 0),
(12, 'posting-catalog.php', 'Postings', 'Browse Postings', 11, 2, 1),
(13, 'my-posting.php', 'Postings', 'My Postings', 11, 2, 2),
(14, 'import-post.php', 'Postings', 'Import Posting', 11, 2, 4),
(15, '', 'Product List Download', 'Products List Download', 0, 1, 0),
(16, 'product-listdownload.php', 'Product List Download', 'Products List Download', 15, 2, 1),
(17, '', 'Quote Queue', 'Quote Queue', 0, 1, 0),
(18, 'quotesandpost_que.php', 'Quote Queue', 'Quote Queue', 17, 2, 1),
(19, '', 'Orders', 'Orders', 0, 1, 0),
(20, 'tm-accepted-offers.php', 'Orders', 'Accepted Offers', 19, 2, 1),
(41, '', 'Users', 'Users', 0, 1, 0),
(21, 'tm-completed-offers.php', 'Orders', 'Completed Orders', 19, 2, 2),
(22, 'tm-cancelled-order.php', 'Orders', 'Cancelled Orders', 19, 2, 3),
(23, '', 'Settings', 'Settings', 0, 1, 0),
(24, 'my-settings.php', 'Settings', 'My Settings', 23, 2, 1),
(25, 'notification-setting.php', 'Settings', 'Notifications', 23, 2, 2),
(26, 'notification.php', 'Settings', 'Notifications setting', 23, 2, 3),
(27, 'import-data.php', 'Settings', 'Import Data', 23, 2, 4),
(28, 'mstindustry.php', 'Settings', 'Industry List', 23, 2, 5),
(29, 'mstcategory.php', 'Settings', 'Category List', 23, 2, 6),
(30, 'mstsubcategory.php', 'Settings', 'Sub Category List', 23, 2, 7),
(31, 'mstbrand.php', 'Settings', 'Brands List', 23, 2, 8),
(32, 'mstlocation.php', 'Settings', 'Location', 23, 2, 9),
(34, 'mstsecurity-que.php', 'Settings', 'Security Question List', 23, 2, 11),
(35, 'mstterms-condition.php', 'Settings', 'Terms & Conditions', 23, 2, 12),
(36, 'databackup.php', 'Settings', 'Database Backup', 23, 2, 13),
(37, 'maskproductname.php', 'Settings', 'Website Settings', 23, 2, 15),
(38, 'menu_permission.php', 'Settings', 'Menu Permission', 23, 2, 16),
(39, '', 'Reports', 'Reports', 0, 1, 0),
(40, 'reports.php', 'Reports', 'Reports', 39, 2, 1),
(43, 'useractivity.php', 'Users', 'User Activity', 41, 2, 2),
(44, 'user-setting.php', 'Users', 'Create New User', 41, 2, 3),
(45, 'mstrole.php', 'Settings', 'Role List', 23, 2, 17),
(46, 'databaserestore.php', 'Settings', 'Database Restore', 23, 2, 14),
(47, 'my-offer.php', 'Postings', 'My Offers', 11, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `MastRolePermission`
--

CREATE TABLE IF NOT EXISTS `MastRolePermission` (
  `RoleId` int(5) NOT NULL,
  `PageId` int(5) NOT NULL,
  `ViewP` int(1) NOT NULL,
  `AddP` int(1) NOT NULL,
  `EditP` int(1) NOT NULL,
  `DeleteP` int(1) NOT NULL,
  `pagePrint` int(1) NOT NULL,
  `pageExport` int(1) NOT NULL,
  PRIMARY KEY (`RoleId`,`PageId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MastRolePermission`
--

INSERT INTO `MastRolePermission` (`RoleId`, `PageId`, `ViewP`, `AddP`, `EditP`, `DeleteP`, `pagePrint`, `pageExport`) VALUES
(14, 38, 1, 0, 0, 0, 0, 0),
(14, 32, 0, 1, 0, 0, 0, 0),
(14, 28, 0, 0, 1, 0, 0, 0),
(14, 27, 0, 0, 0, 1, 0, 0),
(14, 36, 0, 0, 1, 0, 0, 0),
(14, 29, 0, 1, 0, 0, 0, 0),
(14, 31, 1, 0, 0, 0, 0, 0),
(14, 20, 1, 0, 0, 0, 0, 0),
(14, 21, 0, 0, 1, 0, 0, 0),
(14, 22, 0, 1, 0, 1, 0, 0),
(14, 24, 0, 1, 0, 0, 0, 0),
(14, 25, 0, 0, 1, 0, 0, 0),
(14, 26, 0, 0, 0, 1, 0, 0),
(5, 4, 1, 1, 1, 1, 0, 0),
(5, 6, 1, 1, 1, 1, 0, 0),
(5, 7, 1, 1, 1, 1, 0, 0),
(5, 10, 1, 1, 1, 1, 0, 0),
(5, 9, 1, 1, 1, 1, 0, 0),
(5, 8, 1, 1, 1, 1, 0, 0),
(5, 12, 1, 0, 1, 1, 0, 0),
(5, 14, 1, 1, 1, 1, 0, 0),
(5, 13, 1, 1, 1, 0, 0, 0),
(5, 16, 1, 1, 1, 1, 1, 1),
(5, 18, 1, 1, 1, 1, 0, 0),
(5, 20, 1, 1, 1, 1, 0, 0),
(5, 22, 1, 1, 1, 1, 0, 0),
(5, 21, 1, 1, 1, 1, 0, 0),
(5, 44, 1, 1, 1, 1, 0, 0),
(5, 43, 1, 1, 1, 1, 0, 0),
(5, 42, 1, 1, 1, 1, 0, 0),
(5, 31, 1, 1, 1, 1, 0, 0),
(5, 29, 1, 1, 1, 1, 0, 0),
(5, 36, 1, 1, 1, 1, 0, 0),
(5, 27, 1, 1, 1, 1, 0, 0),
(5, 28, 1, 1, 1, 1, 0, 0),
(5, 32, 1, 1, 1, 1, 0, 0),
(5, 38, 1, 1, 1, 1, 0, 0),
(5, 24, 1, 1, 1, 1, 0, 0),
(5, 25, 1, 1, 1, 1, 0, 0),
(4, 9, 1, 1, 1, 1, 1, 0),
(5, 34, 1, 1, 1, 1, 0, 0),
(5, 30, 1, 1, 1, 1, 0, 0),
(5, 35, 1, 1, 1, 1, 0, 0),
(4, 8, 1, 1, 1, 1, 1, 0),
(5, 37, 1, 1, 1, 1, 0, 0),
(5, 40, 1, 1, 1, 1, 0, 0),
(1, 4, 1, 1, 1, 1, 1, 0),
(1, 7, 1, 1, 1, 1, 1, 1),
(1, 6, 1, 1, 1, 1, 1, 1),
(1, 14, 1, 1, 0, 0, 0, 0),
(18, 4, 1, 1, 1, 1, 0, 0),
(1, 12, 1, 1, 1, 0, 1, 0),
(15, 4, 1, 1, 0, 0, 0, 0),
(1, 13, 1, 1, 1, 0, 0, 0),
(1, 16, 1, 1, 1, 0, 1, 1),
(4, 7, 1, 1, 1, 1, 1, 0),
(4, 6, 1, 1, 1, 1, 1, 0),
(4, 4, 1, 1, 1, 1, 0, 0),
(17, 4, 1, 1, 1, 0, 0, 0),
(18, 6, 1, 1, 0, 0, 0, 0),
(18, 7, 1, 1, 0, 0, 0, 0),
(18, 12, 1, 1, 0, 0, 0, 0),
(5, 46, 1, 1, 0, 0, 0, 0),
(1, 24, 1, 1, 1, 0, 0, 0),
(1, 25, 1, 1, 1, 0, 0, 0),
(3, 25, 1, 1, 1, 1, 0, 0),
(3, 24, 1, 1, 1, 1, 0, 0),
(3, 21, 1, 1, 1, 1, 0, 0),
(3, 22, 1, 1, 1, 1, 0, 0),
(3, 20, 1, 1, 1, 1, 0, 0),
(2, 25, 1, 1, 1, 1, 0, 0),
(2, 24, 1, 1, 1, 1, 0, 0),
(2, 18, 1, 1, 1, 0, 0, 0),
(5, 45, 1, 0, 0, 0, 0, 0),
(4, 10, 1, 1, 1, 1, 1, 0),
(4, 14, 1, 1, 1, 0, 1, 0),
(4, 12, 1, 1, 1, 0, 1, 0),
(1, 47, 1, 1, 1, 0, 0, 0),
(4, 47, 1, 1, 1, 0, 1, 0),
(4, 13, 1, 1, 1, 0, 1, 0),
(4, 16, 1, 0, 0, 0, 0, 0),
(4, 18, 1, 1, 1, 0, 1, 0),
(4, 20, 1, 1, 1, 0, 1, 0),
(4, 22, 1, 1, 1, 0, 1, 0),
(4, 21, 1, 1, 1, 0, 1, 0),
(4, 44, 1, 1, 1, 0, 1, 0),
(4, 43, 1, 1, 1, 0, 1, 0),
(4, 42, 1, 1, 1, 0, 1, 0),
(4, 31, 1, 1, 1, 0, 1, 0),
(4, 29, 1, 1, 1, 0, 1, 0),
(4, 36, 1, 1, 1, 0, 1, 0),
(4, 46, 1, 1, 1, 0, 1, 0),
(4, 27, 1, 1, 1, 0, 1, 0),
(4, 28, 1, 1, 1, 0, 1, 0),
(4, 32, 1, 1, 1, 0, 1, 0),
(4, 38, 1, 1, 1, 0, 1, 0),
(4, 24, 1, 1, 1, 0, 1, 0),
(4, 25, 1, 1, 1, 0, 1, 0),
(4, 26, 1, 1, 1, 0, 1, 0),
(4, 45, 1, 1, 1, 0, 1, 0),
(4, 34, 1, 1, 1, 0, 1, 0),
(4, 30, 1, 1, 1, 0, 1, 0),
(4, 35, 1, 1, 1, 0, 1, 0),
(4, 37, 1, 1, 1, 0, 1, 0),
(4, 40, 1, 0, 0, 0, 0, 0),
(6, 4, 1, 0, 0, 0, 0, 0),
(1, 27, 1, 1, 1, 0, 1, 0),
(15, 6, 1, 1, 1, 0, 0, 0),
(15, 7, 1, 1, 1, 0, 0, 0),
(18, 14, 1, 1, 0, 0, 0, 0),
(18, 47, 1, 1, 0, 0, 0, 0),
(18, 13, 1, 1, 0, 0, 0, 0),
(18, 24, 1, 0, 0, 0, 0, 0),
(18, 25, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu_permission`
--

CREATE TABLE IF NOT EXISTS `menu_permission` (
  `rowId` int(5) NOT NULL,
  `pageView` int(5) NOT NULL,
  `pageAdd` int(5) NOT NULL,
  `pageEdit` int(5) NOT NULL,
  `pageDelete` int(5) NOT NULL,
  `pagePrint` int(5) NOT NULL,
  `pageExport` int(5) NOT NULL,
  `userid` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_permission`
--

INSERT INTO `menu_permission` (`rowId`, `pageView`, `pageAdd`, `pageEdit`, `pageDelete`, `pagePrint`, `pageExport`, `userid`) VALUES
(19, 1, 0, 1, 0, 0, 0, 'TM'),
(18, 1, 0, 1, 0, 0, 0, 'TM'),
(14, 1, 1, 1, 0, 0, 0, 'AM'),
(13, 1, 0, 0, 0, 0, 1, 'AM'),
(12, 1, 1, 0, 0, 0, 0, 'AM'),
(17, 1, 0, 1, 0, 0, 0, 'QF'),
(15, 1, 1, 0, 0, 0, 0, 'QF'),
(7, 1, 1, 1, 1, 0, 1, 'SA'),
(6, 1, 0, 0, 0, 0, 0, 'SA'),
(5, 1, 1, 0, 0, 0, 0, 'SA'),
(10, 1, 1, 1, 1, 1, 0, 'AM'),
(4, 1, 0, 0, 0, 0, 1, 'SA'),
(11, 1, 1, 0, 0, 0, 0, 'AM'),
(3, 1, 0, 0, 0, 0, 0, 'SA'),
(2, 1, 1, 1, 1, 1, 0, 'SA'),
(1, 1, 1, 1, 1, 1, 0, 'SA'),
(8, 1, 1, 1, 1, 1, 0, 'SA'),
(9, 1, 0, 0, 0, 0, 0, 'SA'),
(19, 1, 0, 1, 0, 0, 0, 'TM'),
(18, 1, 0, 1, 0, 0, 0, 'TM'),
(14, 1, 1, 1, 0, 0, 0, 'AM'),
(13, 1, 0, 0, 0, 0, 1, 'AM'),
(12, 1, 1, 0, 0, 0, 0, 'AM'),
(17, 1, 0, 1, 0, 0, 0, 'QF'),
(15, 1, 1, 0, 0, 0, 0, 'QF'),
(7, 1, 1, 1, 1, 0, 1, 'SA'),
(6, 1, 0, 0, 0, 0, 0, 'SA'),
(5, 1, 1, 0, 0, 0, 0, 'SA'),
(10, 1, 1, 1, 1, 1, 0, 'AM'),
(4, 1, 0, 0, 0, 0, 1, 'SA'),
(11, 1, 1, 0, 0, 0, 0, 'AM'),
(3, 1, 0, 0, 0, 0, 0, 'SA'),
(2, 1, 1, 1, 1, 1, 0, 'SA'),
(1, 1, 1, 1, 1, 1, 0, 'SA'),
(8, 1, 1, 1, 1, 1, 0, 'SA'),
(9, 1, 0, 0, 0, 0, 0, 'SA');

-- --------------------------------------------------------

--
-- Table structure for table `mst_admin_report`
--

CREATE TABLE IF NOT EXISTS `mst_admin_report` (
  `reportid` int(11) NOT NULL AUTO_INCREMENT,
  `reportname` varchar(100) NOT NULL,
  `reportsql` varchar(500) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`reportid`),
  KEY `reportname` (`reportname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_brand`
--

CREATE TABLE IF NOT EXISTS `mst_brand` (
  `brandid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(2) NOT NULL DEFAULT '1',
  `industryid` int(11) NOT NULL,
  PRIMARY KEY (`brandid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_category`
--

CREATE TABLE IF NOT EXISTS `mst_category` (
  `catid` int(11) NOT NULL AUTO_INCREMENT,
  `industryid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`catid`),
  KEY `name` (`name`),
  KEY `industryid` (`industryid`,`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_country`
--

CREATE TABLE IF NOT EXISTS `mst_country` (
  `countryid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`countryid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=207 ;

--
-- Dumping data for table `mst_country`
--

INSERT INTO `mst_country` (`countryid`, `name`, `isactive`) VALUES
(1, 'Afghanistan', '1'),
(2, 'Albania', '1'),
(3, 'Algeria', '1'),
(4, 'Andorra', '1'),
(5, 'Angola', '1'),
(6, 'Antigua and Barbuda', '1'),
(7, 'Argentina', '1'),
(8, 'Armenia', '1'),
(9, 'Aruba', '1'),
(10, 'Australia', '1'),
(11, 'Austria', '1'),
(12, 'Azerbaijan', '1'),
(13, 'Bahamas, The', '1'),
(14, 'Bahrain', '1'),
(15, 'Bangladesh', '1'),
(16, 'Barbados', '1'),
(17, 'Belarus', '1'),
(18, 'Belgium', '1'),
(19, 'Belize', '1'),
(20, 'Benin', '1'),
(21, 'Bhutan', '1'),
(22, 'Bolivia', '1'),
(23, 'Bosnia and Herzegovina', '1'),
(24, 'Botswana', '1'),
(25, 'Brazil', '1'),
(26, 'Brunei', '1'),
(27, 'Bulgaria', '1'),
(28, 'Burkina Faso', '1'),
(29, 'Burma', '1'),
(30, 'Burundi', '1'),
(31, 'Cambodia', '1'),
(32, 'Cameroon', '1'),
(33, 'Canada', '1'),
(34, 'Cape Verde', '1'),
(35, 'Central African Republic', '1'),
(36, 'Chad', '1'),
(37, 'Chile', '1'),
(38, 'China', '1'),
(39, 'Colombia', '1'),
(40, 'Comoros', '1'),
(41, 'Congo, Democratic Republic of the', '1'),
(42, 'Congo, Republic of the', '1'),
(43, 'Costa Rica', '1'),
(44, 'Cote d''Ivoire', '1'),
(45, 'Croatia', '1'),
(46, 'Cuba', '1'),
(47, 'Curacao', '1'),
(48, 'Cyprus', '1'),
(49, 'Czech Republic', '1'),
(50, 'Denmark', '1'),
(51, 'Djibouti', '1'),
(52, 'Dominica', '1'),
(53, 'Dominican Republic', '1'),
(54, 'East Timor (see', '1'),
(55, 'Ecuador', '1'),
(56, 'Egypt', '1'),
(57, 'El Salvador', '1'),
(58, 'Equatorial Guinea', '1'),
(59, 'Eritrea', '1'),
(60, 'Estonia', '1'),
(61, 'Ethiopia', '1'),
(62, 'Fiji', '1'),
(63, 'Finland', '1'),
(64, 'France', '1'),
(65, 'Gabon', '1'),
(66, 'Gambia, The', '1'),
(67, 'Georgia', '1'),
(68, 'Germany', '1'),
(69, 'Ghana', '1'),
(70, 'Greece', '1'),
(71, 'Grenada', '1'),
(72, 'Guatemala', '1'),
(73, 'Guinea', '1'),
(74, 'Guinea-Bissau', '1'),
(75, 'Guyana', '1'),
(76, 'Haiti', '1'),
(77, 'Holy See', '1'),
(78, 'Honduras', '1'),
(79, 'Hong Kong', '1'),
(80, 'Hungary', '1'),
(81, 'Iceland', '1'),
(82, 'India', '1'),
(83, 'Indonesia', '1'),
(84, 'Iran', '1'),
(85, 'Iraq', '1'),
(86, 'Ireland', '1'),
(87, 'Israel', '1'),
(88, 'Italy', '1'),
(89, 'Jamaica', '1'),
(90, 'Japan', '1'),
(91, 'Jordan', '1'),
(92, 'Kazakhstan', '1'),
(93, 'Kenya', '1'),
(94, 'Kiribati', '1'),
(95, 'Korea, North', '1'),
(96, 'Korea, South', '1'),
(97, 'Kosovo', '1'),
(98, 'Kuwait', '1'),
(99, 'Kyrgyzstan', '1'),
(100, 'Laos', '1'),
(101, 'Latvia', '1'),
(102, 'Lebanon', '1'),
(103, 'Lesotho', '1'),
(104, 'Liberia', '1'),
(105, 'Libya', '1'),
(106, 'Liechtenstein', '1'),
(107, 'Lithuania', '1'),
(108, 'Luxembourg', '1'),
(109, 'Macau', '1'),
(110, 'Macedonia', '1'),
(111, 'Madagascar', '1'),
(112, 'Malawi', '1'),
(113, 'Malaysia', '1'),
(114, 'Maldives', '1'),
(115, 'Mali', '1'),
(116, 'Malta', '1'),
(117, 'Marshall Islands', '1'),
(118, 'Mauritania', '1'),
(119, 'Mauritius', '1'),
(120, 'Mexico', '1'),
(121, 'Micronesia', '1'),
(122, 'Moldova', '1'),
(123, 'Monaco', '1'),
(124, 'Mongolia', '1'),
(125, 'Montenegro', '1'),
(126, 'Morocco', '1'),
(127, 'Mozambique', '1'),
(128, 'Namibia', '1'),
(129, 'Nauru', '1'),
(130, 'Nepal', '1'),
(131, 'Netherlands', '1'),
(132, 'Netherlands Antilles', '1'),
(133, 'New Zealand', '1'),
(134, 'Nicaragua', '1'),
(135, 'Niger', '1'),
(136, 'Nigeria', '1'),
(137, 'North Korea', '1'),
(138, 'Norway', '1'),
(139, 'Oman', '1'),
(140, 'Pakistan', '1'),
(141, 'Palau', '1'),
(142, 'Palestinian Territories', '1'),
(143, 'Panama', '1'),
(144, 'Papua New Guinea', '1'),
(145, 'Paraguay', '1'),
(146, 'Peru', '1'),
(147, 'Philippines', '1'),
(148, 'Poland', '1'),
(149, 'Portugal', '1'),
(150, 'Qatar', '1'),
(151, 'Romania', '1'),
(152, 'Russia', '1'),
(153, 'Rwanda', '1'),
(154, 'Saint Kitts and Nevis', '1'),
(155, 'Saint Lucia', '1'),
(156, 'Saint Vincent and the Grenadines', '1'),
(157, 'Samoa', '1'),
(158, 'San Marino', '1'),
(159, 'Sao Tome and Principe', '1'),
(160, 'Saudi Arabia', '1'),
(161, 'Senegal', '1'),
(162, 'Serbia', '1'),
(163, 'Seychelles', '1'),
(164, 'Sierra Leone', '1'),
(165, 'Singapore', '1'),
(166, 'Sint Maarten', '1'),
(167, 'Slovakia', '1'),
(168, 'Slovenia', '1'),
(169, 'Solomon Islands', '1'),
(170, 'Somalia', '1'),
(171, 'South Africa', '1'),
(172, 'South Korea', '1'),
(173, 'South Sudan', '1'),
(174, 'Spain', '1'),
(175, 'Sri Lanka', '1'),
(176, 'Sudan', '1'),
(177, 'Suriname', '1'),
(178, 'Swaziland', '1'),
(179, 'Sweden', '1'),
(180, 'Switzerland', '1'),
(181, 'Syria', '1'),
(182, 'Taiwan', '1'),
(183, 'Tajikistan', '1'),
(184, 'Tanzania', '1'),
(185, 'Thailand', '1'),
(186, 'Timor-Leste', '1'),
(187, 'Togo', '1'),
(188, 'Tonga', '1'),
(189, 'Trinidad and Tobago', '1'),
(190, 'Tunisia', '1'),
(191, 'Turkey', '1'),
(192, 'Turkmenistan', '1'),
(193, 'Tuvalu', '1'),
(194, 'Uganda', '1'),
(195, 'Ukraine', '1'),
(196, 'United Arab Emirates', '1'),
(197, 'United Kingdom', '1'),
(198, 'Uruguay', '1'),
(199, 'Uzbekistan', '1'),
(200, 'Vanuatu', '1'),
(201, 'Venezuela', '1'),
(202, 'Vietnam', '1'),
(203, 'Yemen', '1'),
(204, 'Zambia', '1'),
(205, 'Zimbabwe', '1'),
(206, 'United States', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_currency`
--

CREATE TABLE IF NOT EXISTS `mst_currency` (
  `currencyid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`currencyid`),
  KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mst_currency`
--

INSERT INTO `mst_currency` (`currencyid`, `name`, `isactive`) VALUES
(1, 'USD', '1'),
(2, 'GBP', '1'),
(3, 'EUR', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_exp_daterange`
--

CREATE TABLE IF NOT EXISTS `mst_exp_daterange` (
  `exprangeid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`exprangeid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `mst_exp_daterange`
--

INSERT INTO `mst_exp_daterange` (`exprangeid`, `name`, `isactive`) VALUES
(1, 'Within 3 Months', b'1'),
(2, '3 - 6 Months', b'1'),
(3, '6 - 12 Months', b'1'),
(4, '12 - 18 Months', b'1'),
(5, '18 - 24 Months', b'1'),
(6, '24 - 36 Months', b'1'),
(7, '36+ Months', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_industry`
--

CREATE TABLE IF NOT EXISTS `mst_industry` (
  `industryid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`industryid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mst_industry`
--

INSERT INTO `mst_industry` (`industryid`, `name`, `isactive`) VALUES
(1, 'Industry', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_language`
--

CREATE TABLE IF NOT EXISTS `mst_language` (
  `languageid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`languageid`),
  KEY `name` (`name`),
  KEY `languageid` (`languageid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=128 ;

--
-- Dumping data for table `mst_language`
--

INSERT INTO `mst_language` (`languageid`, `name`, `isactive`) VALUES
(3, 'Afrikaans', b'1'),
(5, 'Albanian', b'1'),
(9, 'Arabic', b'1'),
(11, 'Armenian', b'1'),
(16, 'Bosnian', b'1'),
(18, 'Bulgarian', b'1'),
(19, 'Cantonese', b'1'),
(23, 'Chinese', b'1'),
(28, 'Croatian', b'1'),
(29, 'Czech', b'1'),
(30, 'Danish', b'1'),
(32, 'Dutch', b'1'),
(33, 'English', b'1'),
(35, 'Estonian', b'1'),
(38, 'Filipino', b'1'),
(39, 'Finnish', b'1'),
(40, 'French', b'1'),
(42, 'Georgian', b'1'),
(43, 'German', b'1'),
(44, 'Greek', b'1'),
(47, 'Hawaiian', b'1'),
(48, 'Hebrew', b'1'),
(49, 'Hindi', b'1'),
(50, 'Hungarian', b'1'),
(51, 'Icelandic', b'1'),
(52, 'Indonesian', b'1'),
(56, 'Italian', b'1'),
(57, 'Japanese', b'1'),
(61, 'Khmer', b'1'),
(63, 'Korean', b'1'),
(70, 'Livonian', b'1'),
(74, 'Macedonian', b'1'),
(75, 'Malay', b'1'),
(77, 'Mandarin', b'1'),
(79, 'Maori', b'1'),
(82, 'Mongolian', b'1'),
(83, 'Norwegian', b'1'),
(92, 'Pashto', b'1'),
(93, 'Persian', b'1'),
(95, 'Polish', b'1'),
(96, 'Portuguese', b'1'),
(99, 'Romanian', b'1'),
(100, 'Russian', b'1'),
(101, 'Sanskrit', b'1'),
(104, 'Serbian', b'1'),
(105, 'Serbo-Croatian', b'1'),
(108, 'Slovak', b'1'),
(109, 'Slovene', b'1'),
(110, 'Spanish', b'1'),
(111, 'Swahili', b'1'),
(112, 'Swedish', b'1'),
(113, 'Tagalog', b'1'),
(118, 'Thai', b'1'),
(120, 'Turkish', b'1'),
(122, 'Ukrainian', b'1'),
(124, 'Urdu', b'1'),
(127, 'Vietnamese', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_location`
--

CREATE TABLE IF NOT EXISTS `mst_location` (
  `locationid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`locationid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mst_location`
--

INSERT INTO `mst_location` (`locationid`, `name`, `isactive`) VALUES
(1, 'DC-NA', b'1'),
(3, 'DC-EU', b'1'),
(4, 'DC-UK', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_mngnotification`
--

CREATE TABLE IF NOT EXISTS `mst_mngnotification` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(25) NOT NULL,
  `text` varchar(50) NOT NULL,
  `type` varchar(25) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `status` varchar(2) NOT NULL DEFAULT '1',
  `userid` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_model`
--

CREATE TABLE IF NOT EXISTS `mst_model` (
  `modelid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`modelid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_notification`
--

CREATE TABLE IF NOT EXISTS `mst_notification` (
  `notiid` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` varchar(50) NOT NULL,
  `userid` varchar(25) NOT NULL,
  `msgdate` datetime NOT NULL,
  `msg` varchar(500) NOT NULL,
  `type` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `fromuserid` varchar(100) NOT NULL,
  `preportid` int(11) NOT NULL,
  PRIMARY KEY (`notiid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_pageName`
--

CREATE TABLE IF NOT EXISTS `mst_pageName` (
  `rowId` int(2) NOT NULL AUTO_INCREMENT,
  `pageName` varchar(100) NOT NULL,
  `pageCheckAdd` varchar(2) NOT NULL DEFAULT 'Y',
  `pageCheckEdit` varchar(2) NOT NULL DEFAULT 'Y',
  `pageCheckDelete` varchar(2) NOT NULL DEFAULT 'Y',
  `pageCheckPrint` varchar(2) NOT NULL DEFAULT 'Y',
  `pageCheckExport` varchar(2) NOT NULL DEFAULT 'Y',
  `userid` varchar(10) NOT NULL,
  PRIMARY KEY (`rowId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `mst_pageName`
--

INSERT INTO `mst_pageName` (`rowId`, `pageName`, `pageCheckAdd`, `pageCheckEdit`, `pageCheckDelete`, `pageCheckPrint`, `pageCheckExport`, `userid`) VALUES
(1, 'Customers', 'Y', 'Y', 'Y', 'Y', 'N', 'SA'),
(2, 'Product Catalog', 'Y', 'Y', 'Y', 'Y', 'N', 'SA'),
(3, 'Postings', 'Y', 'N', 'N', 'Y', 'N', 'SA'),
(4, 'Product List Download', 'N', 'N', 'N', 'N', 'Y', 'SA'),
(5, 'Quote Queue', 'Y', 'N', 'N', 'N', 'N', 'SA'),
(6, 'Quotes', 'N', 'Y', 'N', 'N', 'N', 'SA'),
(7, 'User', 'Y', 'Y', 'Y', 'N', 'Y', 'SA'),
(8, 'Settings', 'Y', 'Y', 'Y', 'Y', 'N', 'SA'),
(9, 'Reports', 'N', 'N', 'N', 'N', 'Y', 'SA'),
(10, 'Customers', 'Y', 'Y', 'Y', 'Y', 'N', 'AM'),
(11, 'Postings', 'Y', 'N', 'N', 'N', 'N', 'AM'),
(12, 'Product Catalog', 'Y', 'N', 'N', 'N', 'N', 'AM'),
(13, 'Product List Download', 'N', 'N', 'N', 'N', 'Y', 'AM'),
(14, 'Settings', 'Y', 'Y', 'N', 'N', 'N', 'AM'),
(15, 'Quotes', 'Y', 'N', 'N', 'N', 'N', 'QF'),
(17, 'Settings', 'N', 'Y', 'N', 'N', 'N', 'QF'),
(18, 'Quotes', 'N', 'Y', 'N', 'N', 'N', 'TM'),
(19, 'Settings', 'N', 'Y', 'N', 'N', 'N', 'TM');

-- --------------------------------------------------------

--
-- Table structure for table `mst_product`
--

CREATE TABLE IF NOT EXISTS `mst_product` (
  `productid` int(11) NOT NULL AUTO_INCREMENT,
  `productno` varchar(25) NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(250) NOT NULL,
  `industryid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `subcatid` int(11) DEFAULT NULL,
  `pakaging` varchar(100) DEFAULT NULL,
  `shipingcondition` varchar(100) DEFAULT NULL,
  `caseweight` varchar(100) DEFAULT NULL,
  `qtypercase` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `manufacture` varchar(100) DEFAULT NULL,
  `pstatus` varchar(25) DEFAULT NULL,
  `mpm` int(11) DEFAULT NULL,
  `addedby` varchar(25) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `roletype` varchar(25) DEFAULT NULL,
  `img0` varchar(100) NOT NULL,
  `img1` varchar(100) NOT NULL,
  `img2` varchar(100) NOT NULL,
  `img3` varchar(100) NOT NULL,
  `img4` varchar(100) NOT NULL,
  `img5` varchar(100) NOT NULL,
  `img6` varchar(100) NOT NULL,
  `img7` varchar(100) NOT NULL,
  `code1` int(11) NOT NULL,
  `codevalue1` varchar(100) NOT NULL,
  `code2` int(11) NOT NULL,
  `codevalue2` varchar(100) NOT NULL,
  `code3` int(11) DEFAULT NULL,
  `codevalue3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`productid`),
  KEY `name` (`name`,`industryid`,`brandid`,`catid`,`subcatid`),
  KEY `brandid` (`brandid`),
  KEY `subcatid` (`subcatid`),
  KEY `industryid` (`industryid`),
  KEY `name_2` (`name`),
  KEY `productno` (`productno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productcode`
--

CREATE TABLE IF NOT EXISTS `mst_productcode` (
  `productcodeid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `prefixid` int(11) NOT NULL,
  `refbyid` varchar(15) NOT NULL,
  `approveid` varchar(15) NOT NULL,
  `code` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '',
  `productcode` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `refdate` datetime NOT NULL,
  `approvedate` datetime NOT NULL,
  PRIMARY KEY (`productcodeid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productimages`
--

CREATE TABLE IF NOT EXISTS `mst_productimages` (
  `productimageid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `imageurl` text NOT NULL,
  `srno` int(11) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`productimageid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productprefix`
--

CREATE TABLE IF NOT EXISTS `mst_productprefix` (
  `proprefixid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`proprefixid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `mst_productprefix`
--

INSERT INTO `mst_productprefix` (`proprefixid`, `name`, `isactive`) VALUES
(1, 'DIN', b'1'),
(2, 'MPN', b'1'),
(3, 'EAN', b'1'),
(4, 'GTIN', b'1'),
(5, 'IBSN', b'1'),
(6, 'Item Code', b'1'),
(7, 'Model Number', b'1'),
(8, 'NDC', b'1'),
(9, 'Other', b'1'),
(10, 'Part Number', b'1'),
(11, 'Product Code', b'1'),
(12, 'UPC', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_productreport`
--

CREATE TABLE IF NOT EXISTS `mst_productreport` (
  `preportid` int(11) NOT NULL AUTO_INCREMENT,
  `reportid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `reviewbyid` varchar(25) NOT NULL,
  `reviewdate` date NOT NULL,
  `reviewremark` varchar(500) NOT NULL,
  `reviewstatus` varchar(25) NOT NULL,
  `approvedby` varchar(25) NOT NULL,
  `approveddate` date NOT NULL,
  `isactive` varchar(1) DEFAULT '1',
  PRIMARY KEY (`preportid`),
  KEY `productid` (`productid`,`reviewdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productsimilar`
--

CREATE TABLE IF NOT EXISTS `mst_productsimilar` (
  `similarid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `spid` int(11) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`similarid`),
  KEY `pid` (`pid`,`spid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_report`
--

CREATE TABLE IF NOT EXISTS `mst_report` (
  `reportid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`reportid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mst_report`
--

INSERT INTO `mst_report` (`reportid`, `name`, `isactive`) VALUES
(1, 'Duplicate Product Listing', b'1'),
(2, 'Incorrect Information', b'1'),
(3, 'Miscategorized', b'1'),
(4, 'Other', b'1'),
(5, 'Product is Discontinued', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_security`
--

CREATE TABLE IF NOT EXISTS `mst_security` (
  `securityid` int(11) NOT NULL AUTO_INCREMENT,
  `qtype` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`securityid`),
  KEY `name` (`name`),
  KEY `qtype` (`qtype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `mst_security`
--

INSERT INTO `mst_security` (`securityid`, `qtype`, `name`, `isactive`) VALUES
(2, 2, 'What was the name of the street you grew up on?', '1'),
(3, 3, 'What was the name of your first pet?', '1'),
(5, 1, 'What make and model was your first car?', '1'),
(6, 1, 'What is your mother''s maiden name?', '1'),
(7, 1, 'Which city were you born?', '1'),
(8, 2, 'What is your father''s middle name?', '1'),
(9, 3, 'What is your favourite food?', '1'),
(13, 3, 'Where did you meet your spouse?', '1'),
(14, 2, 'What is your favourite movie?', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_shipping`
--

CREATE TABLE IF NOT EXISTS `mst_shipping` (
  `shipid` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`shipid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_subcategory`
--

CREATE TABLE IF NOT EXISTS `mst_subcategory` (
  `subcatid` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `industryid` int(11) NOT NULL,
  `isactive` varchar(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`subcatid`),
  KEY `catid` (`catid`,`name`),
  KEY `industryid` (`industryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_timeframe`
--

CREATE TABLE IF NOT EXISTS `mst_timeframe` (
  `timeframeid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`timeframeid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `mst_timeframe`
--

INSERT INTO `mst_timeframe` (`timeframeid`, `name`, `isactive`) VALUES
(1, 'Within 1 Week', b'1'),
(2, '1 - 2 Weeks', b'1'),
(3, '2 - 3 Weeks', b'1'),
(4, '3 - 4 Weeks', b'1'),
(5, '4 - 6 Weeks', b'1'),
(6, '6 - 12 Weeks', b'1'),
(7, '12+ Weeks', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_timezone`
--

CREATE TABLE IF NOT EXISTS `mst_timezone` (
  `timezoneid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  `timezonevalue` varchar(100) NOT NULL,
  PRIMARY KEY (`timezoneid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `mst_timezone`
--

INSERT INTO `mst_timezone` (`timezoneid`, `name`, `isactive`, `timezonevalue`) VALUES
(1, '(GMT-11:00) Midway Island', '1', 'Pacific/Midway'),
(2, '(GMT-11:00) Samoa', '1', 'US/Samoa'),
(3, '(GMT-10:00) Hawaii', '1', 'US/Hawaii'),
(4, '(GMT-09:00) Alaska', '1', 'US/Alaska'),
(6, '(GMT-08:00) Pacific Time (US &amp; Canada)', '1', 'US/Pacific'),
(7, '(GMT-08:00) Tijuana', '1', 'America/Tijuana'),
(8, '(GMT-07:00) Arizona', '1', 'US/Arizona'),
(9, '(GMT-07:00) Mountain Time (US &amp; Canada)', '1', 'US/Mountain'),
(10, '(GMT-07:00) Chihuahua', '1', 'America/Chihuahua'),
(11, '(GMT-07:00) Mazatlan', '1', 'America/Mazatlan'),
(12, '(GMT-06:00) Mexico City', '1', 'America/Mexico_City'),
(13, '(GMT-06:00) Monterrey', '1', 'America/Monterrey'),
(14, '(GMT-06:00) Saskatchewan', '1', 'Canada/Saskatchewan'),
(15, '(GMT-06:00) Central Time (US &amp; Canada)', '1', 'US/Central'),
(16, '(GMT-05:00) Eastern Time (US and Canada)', '1', 'US/Eastern'),
(17, '(GMT-05:00) Indiana (East)', '1', 'US/East-Indiana'),
(18, '(GMT-05:00) Bogota', '1', 'America/Bogota'),
(19, '(GMT-05:00) Lima', '1', 'America/Lima'),
(20, '(GMT-04:30) Caracas', '1', 'America/Caracas'),
(26, '(GMT-04:00) La Paz', '1', 'America/La_Paz'),
(27, '(GMT-04:00) Santiago', '1', 'America/Santiago'),
(28, '(GMT+01:00) Stockholm', '1', 'Europe/Stockholm'),
(29, '(GMT-03:30) Newfoundland', '1', 'Canada/Newfoundland'),
(30, '(GMT-03:00) Buenos Aires', '1', 'America/Buenos_Aires'),
(31, '(GMT+07:00) Bangkok', '1', 'Asia/Bangkok'),
(32, '(GMT+07:00) Jakarta', '1', 'Asia/Jakarta'),
(33, '(GMT+07:00) Krasnoyarsk', '1', 'Asia/Krasnoyarsk'),
(34, '(GMT-03:00) Greenland', '1', 'Greenland'),
(35, '(GMT-02:00) Stanley', '1', 'Atlantic/Stanley'),
(36, '(GMT+08:00) Chongqing', '1', 'Asia/Chongqing'),
(37, '(GMT+08:00) Hong Kong', '1', 'Asia/Hong_Kong'),
(38, '(GMT-01:00) Azores', '1', 'Atlantic/Azores'),
(39, '(GMT-01:00) Cape Verde Is', '1', 'Atlantic/Cape_Verde'),
(40, '(GMT+08:00) Irkutsk', '1', 'Asia/Irkutsk'),
(41, '(GMT+08:00) Kuala Lumpur', '1', 'Asia/Kuala_Lumpur'),
(42, '(GMT) Casablanca', '1', 'Africa/Casablanca'),
(43, '(GMT) Dublin', '1', 'Europe/Dublin'),
(44, '(GMT+08:00) Perth', '1', 'Australia/Perth'),
(45, '(GMT+08:00) Singapore', '1', 'Asia/Singapore'),
(46, '(GMT+08:00) Taipei', '1', 'Asia/Taipei'),
(47, '(GMT) Lisbon', '1', 'Europe/Lisbon'),
(48, '(GMT) London', '1', 'Europe/London'),
(49, '(GMT+08:00) Ulaan Bataar', '1', 'Asia/Ulaanbaatar'),
(50, '(GMT) Monrovia', '1', 'Africa/Monrovia'),
(51, '(GMT+01:00) Amsterdam', '1', 'Europe/Amsterdam'),
(52, '(GMT+08:00) Urumqi', '1', 'Asia/Urumqi'),
(53, '(GMT+09:00) Seoul', '1', 'Asia/Seoul'),
(54, '(GMT+09:00) Tokyo', '1', 'Asia/Tokyo'),
(55, '(GMT+01:00) Belgrade', '1', 'Europe/Belgrade'),
(56, '(GMT+01:00) Berlin', '1', 'Europe/Berlin'),
(57, '(GMT+09:00) Yakutsk', '1', 'Asia/Yakutsk'),
(58, '(GMT+01:00) Bratislava', '1', 'Europe/Bratislava'),
(59, '(GMT+01:00) Brussels', '1', 'Europe/Brussels'),
(60, '(GMT+01:00) Budapest', '1', 'Europe/Budapest'),
(61, '(GMT+01:00) Copenhagen', '1', 'Europe/Copenhagen'),
(62, '(GMT+09:00) Yakutsk', '1', 'Asia/Yakutsk'),
(63, '(GMT+09:30) Adelaide', '1', 'Australia/Adelaide'),
(64, '(GMT+01:00) Ljubljana', '1', 'Europe/Ljubljana'),
(65, '(GMT+01:00) Madrid', '1', 'Europe/Madrid'),
(66, '(GMT+09:30) Darwin', '1', 'Australia/Darwin'),
(67, '(GMT+01:00) Paris', '1', 'Europe/Paris'),
(68, '(GMT+01:00) Prague', '1', 'Europe/Prague'),
(69, '(GMT+10:00) Brisbane', '1', 'Australia/Brisbane'),
(70, '(GMT+10:00) Canberra', '1', 'Australia/Canberra'),
(71, '(GMT+01:00) Rome', '1', 'Europe/Rome'),
(72, '(GMT+01:00) Sarajevo', '1', 'Europe/Sarajevo'),
(73, '(GMT+10:00) Guam', '1', 'Pacific/Guam'),
(74, '(GMT+01:00) Skopje', '1', 'Europe/Skopje'),
(75, '(GMT+10:00) Hobart', '1', 'Australia/Hobart'),
(76, '(GMT+10:00) Melbourne', '1', 'Australia/Melbourne'),
(77, '(GMT+10:00) Port Moresby', '1', 'Pacific/Port_Moresby'),
(78, '(GMT+10:00) Sydney', '1', 'Australia/Sydney'),
(79, '(GMT+10:00) Vladivostok', '1', 'Asia/Vladivostok'),
(80, '(GMT+11:00) Magadan', '1', 'Asia/Magadan'),
(81, '(GMT+12:00) Auckland', '1', 'Pacific/Auckland'),
(82, '(GMT+12:00) Auckland', '1', 'Pacific/Fiji'),
(83, '(GMT+12:00) Fiji', '1', 'Pacific/Fiji'),
(84, '(GMT+12:00) Kamchatka', '1', 'Asia/Kamchatka'),
(85, 'India', '1', 'India'),
(86, 'A', '1', 'test zone');

-- --------------------------------------------------------

--
-- Table structure for table `mst_uom`
--

CREATE TABLE IF NOT EXISTS `mst_uom` (
  `uomid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`uomid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mst_uom`
--

INSERT INTO `mst_uom` (`uomid`, `name`, `isactive`) VALUES
(1, 'per unit', b'1'),
(2, 'per case', b'1'),
(3, 'per pallet', b'1'),
(4, 'per box', b'1'),
(5, 'per single unit', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_useraction`
--

CREATE TABLE IF NOT EXISTS `mst_useraction` (
  `actionid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) NOT NULL,
  `ipaddress` varchar(25) NOT NULL DEFAULT '127.0.0.0',
  `sessionid` varchar(100) NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) NOT NULL,
  PRIMARY KEY (`actionid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2630 ;

--
-- Dumping data for table `mst_useraction`
--

INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`) VALUES
(1238, 'admin', '112.196.136.78', 'fkel2dnuiiknfjdi9015mm8s74', '2015-11-20', '2015-11-20 03:24:42', 'Deleted User Activity'),
(1239, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 03:27:50', 'PENDING Suggested Product Code'),
(1240, 'ZJBX-0533', '112.196.136.78', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', '2015-11-20 03:32:20', 'Logged In'),
(1241, 'FXIO-8905', '104.236.57.54', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', '2015-11-20 04:04:07', 'Suggested New Product Code'),
(1242, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 04:04:40', 'Review Product Code'),
(1243, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 04:05:04', 'Review Product Code'),
(1244, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 04:10:02', 'Review Product Code'),
(1245, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 04:15:23', 'Pending Suggested Product Code'),
(1246, 'admin', '112.196.136.78', 'q8ga2qau1pjmior8k2vjbcjj25', '2015-11-20', '2015-11-20 04:46:27', 'Logged In'),
(1247, 'admin', '112.196.136.78', 'q8ga2qau1pjmior8k2vjbcjj25', '2015-11-20', '2015-11-20 04:47:47', 'Logged Out'),
(1248, 'admin', '112.196.136.78', 'obc5m3hf5a1oj11skv0ho81l60', '2015-11-20', '2015-11-20 04:47:52', 'Logged In'),
(1249, 'admin', '112.196.136.78', 'obc5m3hf5a1oj11skv0ho81l60', '2015-11-20', '2015-11-20 04:47:58', 'Logged Out'),
(1250, 'VUQQ-3341', '112.196.136.78', '0rolqskp80s7p42muhofmm8q74', '2015-11-20', '2015-11-20 04:48:12', 'Logged In'),
(1251, 'ZJBX-0533', '112.196.136.78', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', '2015-11-20 04:50:53', 'Submitted Quote'),
(1252, 'FXIO-8905', '104.236.225.168', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', '2015-11-20 04:52:50', 'Account Manager submitted a counter offer'),
(1253, 'ZJBX-0533', '112.196.136.78', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', '2015-11-20 05:03:14', 'Submitted Quote'),
(1254, 'VUQQ-3341', '112.196.136.78', '0rolqskp80s7p42muhofmm8q74', '2015-11-20', '2015-11-20 05:05:22', 'Submitted Offer'),
(1255, 'FXIO-8905', '104.236.225.168', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', '2015-11-20 05:13:22', 'Account Manager submitted a counter offer'),
(1256, 'ZJBX-0533', '112.196.136.78', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', '2015-11-20 05:14:26', 'Quote Facilitator Accepted by Offer'),
(1257, 'VUQQ-3341', '112.196.136.78', '0rolqskp80s7p42muhofmm8q74', '2015-11-20', '2015-11-20 05:37:36', 'Submitted Offer'),
(1258, 'ZJBX-0533', '112.196.136.78', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', '2015-11-20 05:39:08', 'Submitted Quote'),
(1259, 'ZJBX-0533', '112.196.136.78', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', '2015-11-20 05:41:03', 'Quote Faciltiato submitted a Counter Offer'),
(1260, 'VUQQ-3341', '112.196.136.78', '0rolqskp80s7p42muhofmm8q74', '2015-11-20', '2015-11-20 05:41:37', 'Offer accepted by Account Manager'),
(1261, 'admin', '66.49.237.16', 'nji2f6gihijeih9gdrhg79fbv0', '2015-11-20', '2015-11-20 15:08:55', 'Logged In'),
(1262, 'FXIO-8905', '66.49.237.16', 'hbje6n6dtsg814qttk6ijpfgh7', '2015-11-20', '2015-11-20 15:09:54', 'Logged In'),
(1263, 'FXIO-8905', '66.49.237.16', 'hbje6n6dtsg814qttk6ijpfgh7', '2015-11-20', '2015-11-20 15:10:16', 'Logged Out'),
(1264, 'ZJBX-0533', '66.49.237.16', 'crtb5qgpvm1p1k61quqn2t7bq4', '2015-11-20', '2015-11-20 15:10:22', 'Logged In'),
(1265, 'ZJBX-0533', '66.49.237.16', 'crtb5qgpvm1p1k61quqn2t7bq4', '2015-11-20', '2015-11-20 15:11:09', 'Submitted Quote'),
(1266, 'ZJBX-0533', '66.49.237.16', 'crtb5qgpvm1p1k61quqn2t7bq4', '2015-11-20', '2015-11-20 15:49:37', 'Logged Out'),
(1267, 'FXIO-8905', '66.49.237.16', 'lp0hpe8hjc3i7vuvs806po9g92', '2015-11-20', '2015-11-20 15:49:47', 'Logged In'),
(1268, 'FXIO-8905', '66.49.237.16', 'lp0hpe8hjc3i7vuvs806po9g92', '2015-11-20', '2015-11-20 15:50:00', 'Suggested New Product Code'),
(1269, 'admin', '66.49.237.16', 'nji2f6gihijeih9gdrhg79fbv0', '2015-11-20', '2015-11-20 15:50:16', 'Approved Suggested Product Code'),
(1270, 'admin', '66.49.237.16', 'nji2f6gihijeih9gdrhg79fbv0', '2015-11-20', '2015-11-20 15:50:24', 'Rejected Suggested Product Code'),
(1271, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-22', '2015-11-22 23:22:12', 'Logged In'),
(1272, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-22', '2015-11-22 23:42:45', 'Export Products to CSV'),
(1273, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-22', '2015-11-22 23:53:25', 'Export User Activity'),
(1274, 'FXIO-8905', '112.196.136.78', 'f7he2c89v4fg7j3o43pb851hp6', '2015-11-22', '2015-11-22 23:55:04', 'Logged In'),
(1275, 'FTTP-8399', '104.131.161.230', '88h7lms1tgirki1mmu2goc3pj6', '2015-11-22', '2015-11-22 23:57:54', 'Logged In'),
(1276, 'FXIO-8905', '112.196.136.78', 'f7he2c89v4fg7j3o43pb851hp6', '2015-11-23', '2015-11-23 00:00:11', 'Logged Out'),
(1277, 'VUQQ-3341', '112.196.136.78', '69rpagoi8debbep5hlm6jrdvo6', '2015-11-23', '2015-11-23 00:00:21', 'Logged In'),
(1278, 'admin', '112.196.136.78', 'ic9vth7tkn2vg2f8rdt5aqpvp5', '2015-11-23', '2015-11-23 00:10:13', 'Logged In'),
(1279, 'FMVI-7264', '112.196.136.78', 'te8rrqsriojtunehvb58n7kc11', '2015-11-23', '2015-11-23 00:11:09', 'Logged In'),
(1280, 'FTTP-8399', '104.131.161.230', '88h7lms1tgirki1mmu2goc3pj6', '2015-11-23', '2015-11-23 00:56:18', 'Logged Out'),
(1281, 'FXIO-8905', '112.196.136.78', 'ekn0m4otq7b1ca22r9l0fa8tj0', '2015-11-23', '2015-11-23 01:01:08', 'Logged In'),
(1282, 'admin', '112.196.136.78', 'en885o1ugpqfrmlgqkitvv3tt5', '2015-11-23', '2015-11-23 01:14:04', 'Logged In'),
(1283, 'VUQQ-3341', '112.196.136.78', '69rpagoi8debbep5hlm6jrdvo6', '2015-11-23', '2015-11-23 01:30:32', 'Submitted Offer'),
(1284, 'ZJBX-0533', '104.131.161.230', 'soiila8v3d4ur7iosro0lgh5m4', '2015-11-23', '2015-11-23 01:31:34', 'Logged In'),
(1285, 'FXIO-8905', '112.196.136.78', '8tirqlv322msde0k2vlv99ftf4', '2015-11-23', '2015-11-23 02:14:13', 'Logged In'),
(1286, 'RAUU-1599', '112.196.136.78', '7odksbacnpr4plnhh3a4ao3n15', '2015-11-23', '2015-11-23 02:14:29', 'Logged In'),
(1287, 'RAUU-1599', '112.196.136.78', '7odksbacnpr4plnhh3a4ao3n15', '2015-11-23', '2015-11-23 02:17:49', 'Submitted Offer'),
(1288, 'ZJBX-0533', '104.131.161.230', 'soiila8v3d4ur7iosro0lgh5m4', '2015-11-23', '2015-11-23 02:19:24', 'Quote Faciltiato submitted a Counter Offer'),
(1289, 'VUQQ-3341', '112.196.136.78', '69rpagoi8debbep5hlm6jrdvo6', '2015-11-23', '2015-11-23 03:28:08', 'Submitted Offer'),
(1290, 'ZJBX-0533', '112.196.136.78', 'ucqjhqq4m0kf3dcduu81v7i2u5', '2015-11-23', '2015-11-23 03:34:13', 'Logged In'),
(1291, 'ZJBX-0533', '112.196.136.78', 'ucqjhqq4m0kf3dcduu81v7i2u5', '2015-11-23', '2015-11-23 03:34:50', 'Quote Facilitator Accepted by Offer'),
(1292, 'FMVI-7264', '112.196.136.78', 'vrd8p3ksq3rpdaa35d6vtd36i4', '2015-11-23', '2015-11-23 03:54:50', 'Logged In'),
(1293, 'VUQQ-3341', '112.196.136.78', '69rpagoi8debbep5hlm6jrdvo6', '2015-11-23', '2015-11-23 04:51:46', 'Logged Out'),
(1294, 'FXIO-8905', '112.196.136.78', 'su6ds7otpfqqjsd8tpqmleeib2', '2015-11-23', '2015-11-23 04:52:06', 'Logged In'),
(1295, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-23', '2015-11-23 04:53:34', 'Edited Product'),
(1296, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-23', '2015-11-23 05:01:41', 'Edited Product'),
(1297, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-23', '2015-11-23 05:30:07', 'Edited Product'),
(1298, 'admin', '112.196.136.78', 'ksppokhtaegvaac74k27m84i71', '2015-11-23', '2015-11-23 05:32:47', 'Edited Product'),
(1299, 'FMVI-7264', '112.196.136.78', 'ej413h15k3mvajjm9t32npr7i3', '2015-11-23', '2015-11-23 08:11:03', 'Logged In'),
(1300, 'FMVI-7264', '112.196.136.78', 'ej413h15k3mvajjm9t32npr7i3', '2015-11-23', '2015-11-23 08:11:19', 'Logged Out'),
(1301, 'FXIO-8905', '112.196.136.78', '9o0mtl6s2frahaln043qb5ii27', '2015-11-23', '2015-11-23 08:11:43', 'Logged In'),
(1302, 'admin', '66.49.237.16', '2kr6vhmck8j1mvn1n51srumah5', '2015-11-24', '2015-11-24 14:07:36', 'Logged In'),
(1303, 'admin', '112.196.136.78', '8foe99vc4uvil1b7ngrm1bvvn1', '2015-11-25', '2015-11-25 01:32:52', 'Logged In'),
(1304, 'FMVI-7264', '112.196.136.44', '6ci6brmlcgtjt3ncf9dq636do3', '2015-11-26', '2015-11-26 07:59:09', 'Logged In'),
(1305, 'FMVI-7264', '112.196.136.44', '6ci6brmlcgtjt3ncf9dq636do3', '2015-11-26', '2015-11-26 07:59:34', 'Logged Out'),
(1306, 'admin', '112.196.136.44', 'odvfkqig37ns3h9k7vbllb72l0', '2015-11-26', '2015-11-26 07:59:45', 'Logged In'),
(1307, 'admin', '66.49.157.92', 'h385t9ddh9nbjlfftj96ti05h7', '2015-11-26', '2015-11-26 08:00:18', 'Logged In'),
(1308, 'admin', '112.196.136.44', 'k7fhhjc3871dl6ertbk897kjo4', '2015-11-27', '2015-11-27 02:07:00', 'Logged In'),
(1309, 'admin', '112.196.136.78', 'c3upht443lc43kvegqi9mf0kr2', '2015-11-27', '2015-11-27 02:25:28', 'Logged In'),
(1310, 'admin', '112.196.136.44', 'k7fhhjc3871dl6ertbk897kjo4', '2015-11-27', '2015-11-27 02:28:30', 'Logged Out'),
(1311, 'admin', '112.196.136.44', 'fbtcj7bm5047cf1s0ngrfb1370', '2015-11-27', '2015-11-27 03:17:18', 'Logged In'),
(1312, 'admin', '112.196.136.44', 'fbtcj7bm5047cf1s0ngrfb1370', '2015-11-27', '2015-11-27 03:18:05', 'Logged Out'),
(1313, 'admin', '112.196.136.44', 'ubt0ltuu4g14916kgh2rr0n610', '2015-11-27', '2015-11-27 03:18:38', 'Logged In'),
(1314, 'admin', '112.196.136.44', 'ubt0ltuu4g14916kgh2rr0n610', '2015-11-27', '2015-11-27 03:48:47', 'Logged Out'),
(1315, 'admin', '112.196.136.78', 'c3upht443lc43kvegqi9mf0kr2', '2015-11-27', '2015-11-27 03:49:43', 'Logged Out'),
(1316, 'admin', '112.196.136.78', 'psuiunu16fgk2t4058504ssc34', '2015-11-27', '2015-11-27 03:58:25', 'Logged In'),
(1317, 'admin', '112.196.136.78', 'psuiunu16fgk2t4058504ssc34', '2015-11-27', '2015-11-27 03:58:51', 'Logged Out'),
(1318, 'admin', '112.196.136.44', 'nperir5i6ebk3s1j0d6obc9fh2', '2015-11-27', '2015-11-27 04:06:00', 'Logged In'),
(1319, 'admin', '112.196.136.44', '2s0h9edchi3imq49ch33qplkl7', '2015-11-27', '2015-11-27 04:06:48', 'Logged In'),
(1320, 'admin', '112.196.136.44', '0bl75ff7phcrkcg79krhj7opi5', '2015-11-27', '2015-11-27 04:15:49', 'Logged In'),
(1321, 'admin', '14.98.12.253', '3ejds2gjatb9665o71df2g2tt5', '2015-11-27', '2015-11-27 07:48:44', 'Logged In'),
(1322, 'admin', '66.49.237.16', 'h4imh299sv9r5e83lssark8s47', '2015-11-27', '2015-11-27 07:49:41', 'Logged In'),
(1323, 'admin', '112.196.136.78', 'al7o5dfn1ujlo43l4p4lnnrl47', '2015-11-27', '2015-11-27 07:52:24', 'Logged In'),
(1324, 'admin', '66.49.237.16', 'h4imh299sv9r5e83lssark8s47', '2015-11-27', '2015-11-27 07:55:50', 'Logged Out'),
(1325, 'admin', '66.49.237.16', 'dapi4ojssfoa3e8t54t5bnc027', '2015-11-27', '2015-11-27 07:56:01', 'Logged In'),
(1326, 'admin', '66.49.237.16', 'dapi4ojssfoa3e8t54t5bnc027', '2015-11-27', '2015-11-27 07:56:42', 'Logged Out'),
(1327, 'admin', '14.98.12.253', 'nntpk8bdgng4mpi36qcpreeqv2', '2015-11-27', '2015-11-27 08:08:11', 'Logged In'),
(1328, 'admin', '14.98.12.253', 'rteme37ohkqdn59nt1a9760606', '2015-11-27', '2015-11-27 08:10:45', 'Logged In'),
(1329, 'admin', '14.98.12.253', 'rteme37ohkqdn59nt1a9760606', '2015-11-27', '2015-11-27 08:11:59', 'Logged Out'),
(1330, 'admin', '14.98.12.253', '16rbrlig71pqkso5pfadtn2tb7', '2015-11-27', '2015-11-27 08:12:04', 'Logged In'),
(1331, 'admin', '59.161.69.157', 'i006pa89jis11cnp7icf45pn11', '2015-11-27', '2015-11-27 08:50:08', 'Logged In'),
(1332, 'admin', '112.196.136.44', 'f6s1mkcbac4mupjpdomde9fhe4', '2015-11-28', '2015-11-28 05:21:36', 'Logged In'),
(1333, 'admin', '112.196.136.78', 'omv4v7j72ckofncdncoppuc3h0', '2015-11-29', '2015-11-29 22:52:40', 'Logged In'),
(1334, 'admin', '112.196.136.78', 'omv4v7j72ckofncdncoppuc3h0', '2015-11-29', '2015-11-29 23:11:16', 'Logged Out'),
(1335, 'admin', '112.196.136.44', 'm3t2sp2438beo94ssfj2h32p20', '2015-11-29', '2015-11-29 23:12:11', 'Logged In'),
(1336, 'admin', '112.196.136.78', 'jd3uk297ntn93u1hf45ev9s5i6', '2015-11-29', '2015-11-29 23:12:16', 'Logged In'),
(1337, 'admin', '112.196.136.78', 'jd3uk297ntn93u1hf45ev9s5i6', '2015-11-29', '2015-11-29 23:12:39', 'Logged Out'),
(1338, 'admin', '112.196.136.78', 'apedtv53jjd4t3cj0eigp9eh42', '2015-11-29', '2015-11-29 23:15:35', 'Logged In'),
(1339, 'admin', '112.196.136.78', '14up6es8okg63tjfshar02r143', '2015-11-29', '2015-11-29 23:27:51', 'Logged In'),
(1340, 'admin', '112.196.136.78', 'apedtv53jjd4t3cj0eigp9eh42', '2015-11-29', '2015-11-29 23:32:23', 'Logged Out'),
(1341, 'admin', '112.196.136.78', 'sac42is08mv83i03f6kjofsnp2', '2015-11-29', '2015-11-29 23:32:50', 'Logged In'),
(1342, 'admin', '112.196.136.78', 'sac42is08mv83i03f6kjofsnp2', '2015-11-29', '2015-11-29 23:33:02', 'Logged Out'),
(1343, 'admin', '112.196.136.44', 'm3t2sp2438beo94ssfj2h32p20', '2015-11-29', '2015-11-29 23:36:15', 'Logged Out'),
(1344, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-29', '2015-11-29 23:37:06', 'Logged In'),
(1345, 'admin', '112.196.136.44', 'k1ulf7cisgdm8jidv76ra1v7m3', '2015-11-29', '2015-11-29 23:39:30', 'Logged In'),
(1346, 'admin', '112.196.136.78', 'v26vo4bgddh31v1eokhi37i0t4', '2015-11-30', '2015-11-30 00:01:56', 'Logged In'),
(1347, 'admin', '112.196.136.44', 'k1ulf7cisgdm8jidv76ra1v7m3', '2015-11-30', '2015-11-30 00:29:03', 'Logged Out'),
(1348, 'FMVI-7264', '112.196.136.44', 'i6pkilridah7uelia8ufvtdfn0', '2015-11-30', '2015-11-30 00:29:37', 'Logged In'),
(1349, 'FMVI-7264', '112.196.136.44', 'i6pkilridah7uelia8ufvtdfn0', '2015-11-30', '2015-11-30 00:36:26', 'Suggested Product'),
(1350, 'FMVI-7264', '112.196.136.44', 'i6pkilridah7uelia8ufvtdfn0', '2015-11-30', '2015-11-30 00:37:52', 'Logged Out'),
(1351, 'admin', '112.196.136.44', 'fushfoti7glf3idr80f15jsj53', '2015-11-30', '2015-11-30 00:38:09', 'Logged In'),
(1352, 'admin', '112.196.136.78', 'q1oj5kgkicj03a5s6qe3p546n3', '2015-11-30', '2015-11-30 00:39:28', 'Logged In'),
(1353, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 00:41:36', 'Logged In'),
(1354, 'admin', '112.196.136.44', 'fushfoti7glf3idr80f15jsj53', '2015-11-30', '2015-11-30 00:43:31', 'Logged Out'),
(1355, 'FMVI-7264', '112.196.136.44', '7l42u9eoamgcakafnirq2mqpr0', '2015-11-30', '2015-11-30 00:48:56', 'Logged In'),
(1356, 'FMVI-7264', '112.196.136.44', '7l42u9eoamgcakafnirq2mqpr0', '2015-11-30', '2015-11-30 00:53:20', 'Suggested Product'),
(1357, 'FMVI-7264', '112.196.136.44', '7l42u9eoamgcakafnirq2mqpr0', '2015-11-30', '2015-11-30 00:53:27', 'Logged Out'),
(1358, 'admin', '112.196.136.44', 't782qmm2ofcf6n7f41j5jqj585', '2015-11-30', '2015-11-30 00:54:00', 'Logged In'),
(1359, 'admin', '112.196.136.44', 't782qmm2ofcf6n7f41j5jqj585', '2015-11-30', '2015-11-30 01:03:35', 'APPROVED New Product'),
(1360, 'admin', '112.196.136.44', 't782qmm2ofcf6n7f41j5jqj585', '2015-11-30', '2015-11-30 01:03:47', 'Logged Out'),
(1361, 'FMVI-7264', '112.196.136.44', '3r44q5g634233dk08f10q8qpl1', '2015-11-30', '2015-11-30 01:04:22', 'Logged In'),
(1362, 'admin', '112.196.136.78', 'mv8qddnnv8ru4vb55ivh951pt6', '2015-11-30', '2015-11-30 01:04:32', 'Logged In'),
(1363, 'FMVI-7264', '112.196.136.44', '3r44q5g634233dk08f10q8qpl1', '2015-11-30', '2015-11-30 01:33:15', 'Logged Out'),
(1364, 'FMVI-7264', '112.196.136.44', '0cmccb0oc7ne5ef21gb37h3sb3', '2015-11-30', '2015-11-30 01:40:01', 'Logged In'),
(1365, 'FMVI-7264', '112.196.136.78', 'q6a802bks3mjm00p9j7inna1e2', '2015-11-30', '2015-11-30 01:56:52', 'Logged In'),
(1366, 'admin', '14.98.100.29', 'fb8tsor8a7avtokbrehiltl000', '2015-11-30', '2015-11-30 01:57:51', 'Logged In'),
(1367, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 02:17:39', 'Added Product'),
(1368, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 02:19:10', 'Suggested Product'),
(1369, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 02:20:58', 'Suggested Product'),
(1370, 'admin', '112.196.136.78', 'kafp215khju891ju6rtbo1r0l7', '2015-11-30', '2015-11-30 03:11:43', 'Logged In'),
(1371, 'FMVI-7264', '112.196.136.44', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', '2015-11-30 03:15:30', 'Logged In'),
(1372, 'FMVI-7264', '112.196.136.44', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', '2015-11-30 03:17:30', 'Added Customer'),
(1373, 'FMVI-7264', '112.196.136.44', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', '2015-11-30 03:18:10', 'Updated Customer Status'),
(1374, 'FMVI-7264', '112.196.136.44', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', '2015-11-30 03:18:28', 'Updated Customer Status'),
(1375, 'FMVI-7264', '112.196.136.44', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', '2015-11-30 03:37:38', 'Logged Out'),
(1376, 'admin', '112.196.136.44', 'qb2674n1q8k4107pb81vfaid45', '2015-11-30', '2015-11-30 03:37:44', 'Logged In'),
(1377, 'admin', '112.196.136.44', 'qb2674n1q8k4107pb81vfaid45', '2015-11-30', '2015-11-30 03:44:29', 'Logged Out'),
(1378, 'admin', '112.196.136.44', '10b57bgekun0tts2f00t4l0707', '2015-11-30', '2015-11-30 03:44:37', 'Logged In'),
(1379, 'admin', '112.196.136.44', '3vif8na3np3b1svs6u8hm5pdh0', '2015-11-30', '2015-11-30 03:44:40', 'Logged In'),
(1380, 'admin', '112.196.136.44', '3ogk0o60l3ietluqcm2hqit9g3', '2015-11-30', '2015-11-30 03:50:32', 'Logged In'),
(1381, 'admin', '112.196.136.78', 'q1oj5kgkicj03a5s6qe3p546n3', '2015-11-30', '2015-11-30 03:51:31', 'Added Product'),
(1382, 'admin', '112.196.136.44', 'ppp34n3s88n27f774cdev1u394', '2015-11-30', '2015-11-30 03:54:04', 'Logged In'),
(1383, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 03:57:20', 'Suggested Product'),
(1384, 'admin', '112.196.136.44', 'ppp34n3s88n27f774cdev1u394', '2015-11-30', '2015-11-30 03:58:05', 'Logged Out'),
(1385, 'admin', '112.196.136.44', 'dejvljmodgirhqtiqrnph0qdl5', '2015-11-30', '2015-11-30 03:59:47', 'Logged In'),
(1386, 'admin', '112.196.136.44', '3vif8na3np3b1svs6u8hm5pdh0', '2015-11-30', '2015-11-30 03:59:54', 'Logged Out'),
(1387, 'FMVI-7264', '112.196.136.44', 'k1ek1n6cbo69ac886p7apfulf1', '2015-11-30', '2015-11-30 04:00:01', 'Logged In'),
(1388, 'admin', '112.196.136.44', 'hoq8ls72qukelkfq0arlu6pne2', '2015-11-30', '2015-11-30 04:24:40', 'Logged In'),
(1389, 'admin', '112.196.136.44', 'ekg2hnb3va7e3jr4f6nr4biaf5', '2015-11-30', '2015-11-30 04:27:09', 'Logged In'),
(1390, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 04:28:22', 'Suggested Product'),
(1391, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 04:30:04', 'APPROVED New Product'),
(1392, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 04:30:17', 'APPROVED New Product'),
(1393, 'FMVI-7264', '112.196.136.44', 'k1ek1n6cbo69ac886p7apfulf1', '2015-11-30', '2015-11-30 05:00:10', 'Added Posting'),
(1394, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 05:03:06', 'Added Posting'),
(1395, 'FMVI-7264', '112.196.136.44', 'k1ek1n6cbo69ac886p7apfulf1', '2015-11-30', '2015-11-30 05:11:49', 'Logged Out'),
(1396, 'FTTP-8399', '112.196.136.44', '3jppqgtf6o4826f76nasm9amb3', '2015-11-30', '2015-11-30 05:12:24', 'Logged In'),
(1397, 'admin', '112.196.136.44', 'quujsft0bq7ernt7pimd40st23', '2015-11-30', '2015-11-30 05:14:37', 'Logged In'),
(1398, 'FTTP-8399', '112.196.136.78', 'eurba15s4ptcspfn7f9uu8vc20', '2015-11-30', '2015-11-30 05:14:43', 'Logged In'),
(1399, 'FTTP-8399', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 05:24:31', 'Updated User'),
(1400, 'FMVI-7264', '112.196.136.78', 'q6a802bks3mjm00p9j7inna1e2', '2015-11-30', '2015-11-30 05:29:54', 'Logged Out'),
(1401, 'FTTP-8399', '112.196.136.78', 'id3tcobt143spo3od62g5prje2', '2015-11-30', '2015-11-30 05:30:28', 'Logged In'),
(1402, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 05:54:16', 'Suggested Product'),
(1403, 'admin', '112.196.136.44', '2m9dsempgvgb750mj07v185fl3', '2015-11-30', '2015-11-30 05:54:35', 'Logged In'),
(1404, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 05:54:58', 'APPROVED New Product'),
(1405, 'FTTP-8399', '112.196.136.44', 'ekoufk75qv0g3dq7f4cce010u7', '2015-11-30', '2015-11-30 05:55:24', 'Logged In'),
(1406, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 06:03:51', 'Suggested Product'),
(1407, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 06:04:15', 'APPROVED New Product'),
(1408, 'FTTP-8399', '112.196.136.44', 'ekoufk75qv0g3dq7f4cce010u7', '2015-11-30', '2015-11-30 06:06:50', 'Logged Out'),
(1409, 'FMVI-7264', '112.196.136.44', 'ppndq1ih4fvco4ih3gjc5f8a65', '2015-11-30', '2015-11-30 06:07:04', 'Logged In'),
(1410, 'admin', '112.196.136.44', 'jecjb32tkb1h2178ef4ncmd2v6', '2015-11-30', '2015-11-30 06:17:18', 'Logged In'),
(1411, 'admin', '112.196.136.44', '9lq3detemcjhajqc7rtgf3aep2', '2015-11-30', '2015-11-30 06:25:21', 'Logged In'),
(1412, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 06:28:48', 'Added Customer'),
(1413, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 06:28:58', 'Updated Customer Status'),
(1414, 'FMVI-7264', '112.196.136.44', 'ppndq1ih4fvco4ih3gjc5f8a65', '2015-11-30', '2015-11-30 06:34:20', 'Added Posting'),
(1415, 'FMVI-7264', '112.196.136.44', 'ppndq1ih4fvco4ih3gjc5f8a65', '2015-11-30', '2015-11-30 06:37:12', 'Logged Out'),
(1416, 'FTTP-8399', '112.196.136.44', '3i86tntuv50bbp5unc5ui0f3p5', '2015-11-30', '2015-11-30 06:37:20', 'Logged In'),
(1417, 'FTTP-8399', '112.196.136.44', '3i86tntuv50bbp5unc5ui0f3p5', '2015-11-30', '2015-11-30 06:42:23', 'Logged Out'),
(1418, 'ZJBX-0533', '112.196.136.44', '3hr4mq21r0j3qjgul975l479q1', '2015-11-30', '2015-11-30 06:48:09', 'Logged In'),
(1419, 'ZJBX-0533', '112.196.136.44', '3hr4mq21r0j3qjgul975l479q1', '2015-11-30', '2015-11-30 06:48:28', 'Logged Out'),
(1420, 'ZJBX-0533', '112.196.136.44', 'mpma262jva7hrrmtegpa059gi2', '2015-11-30', '2015-11-30 06:48:31', 'Logged In'),
(1421, 'FTTP-8399', '112.196.136.78', 'eurba15s4ptcspfn7f9uu8vc20', '2015-11-30', '2015-11-30 06:49:36', 'Logged Out'),
(1422, 'ZJBX-0533', '112.196.136.78', 'dvr3injeb8jna7d5qdekjoh487', '2015-11-30', '2015-11-30 06:49:45', 'Logged In'),
(1423, 'FTTP-8399', '112.196.136.78', '9nrpn4jgem3i9ipta3mnkivm02', '2015-11-30', '2015-11-30 06:50:11', 'Logged In'),
(1424, 'admin', '112.196.136.78', 'q1oj5kgkicj03a5s6qe3p546n3', '2015-11-30', '2015-11-30 06:58:13', 'Added Customer'),
(1425, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 06:59:47', 'Added Posting'),
(1426, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 07:08:03', 'Suggested Product'),
(1427, 'admin', '112.196.136.78', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-30', '2015-11-30 07:08:20', 'APPROVED New Product'),
(1428, 'FMVI-7264', '112.196.136.78', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', '2015-11-30 07:09:21', 'Added Posting'),
(1429, 'admin', '112.196.136.78', 'v26vo4bgddh31v1eokhi37i0t4', '2015-11-30', '2015-11-30 07:13:51', 'Logged Out'),
(1430, 'FMVI-7264', '112.196.136.78', '3m3kv27fl39k675niq5g9r8541', '2015-11-30', '2015-11-30 07:14:22', 'Logged In'),
(1431, 'ZJBX-0533', '112.196.136.78', 'dvr3injeb8jna7d5qdekjoh487', '2015-11-30', '2015-11-30 07:14:55', 'Logged Out'),
(1432, 'FMVI-7264', '112.196.136.78', '3m3kv27fl39k675niq5g9r8541', '2015-11-30', '2015-11-30 07:16:04', 'Suggested Product'),
(1433, 'FTTP-8399', '112.196.136.78', 'v8sbg7e3ojsfo381vjn2hkn025', '2015-11-30', '2015-11-30 07:19:30', 'Logged In'),
(1434, 'FTTP-8399', '112.196.136.78', 'v8sbg7e3ojsfo381vjn2hkn025', '2015-11-30', '2015-11-30 07:20:08', 'Logged Out'),
(1435, 'ZJBX-0533', '112.196.136.44', 'mpma262jva7hrrmtegpa059gi2', '2015-11-30', '2015-11-30 07:27:55', 'Logged Out'),
(1436, 'FMVI-7264', '112.196.136.44', '7n83l1u88v7o1napcn8b3q3i92', '2015-11-30', '2015-11-30 07:28:04', 'Logged In'),
(1437, 'FMVI-7264', '112.196.136.44', '7n83l1u88v7o1napcn8b3q3i92', '2015-11-30', '2015-11-30 07:28:34', 'Logged Out'),
(1438, 'admin', '112.196.136.44', 'pcm6eoen26uk7cccafj4i9pdd4', '2015-11-30', '2015-11-30 07:28:47', 'Logged In'),
(1439, 'admin', '112.196.136.44', 'pcm6eoen26uk7cccafj4i9pdd4', '2015-11-30', '2015-11-30 07:30:21', 'Export Products to CSV'),
(1440, 'admin', '112.196.136.44', 'audhrhknlsa4holft2ei33nq95', '2015-11-30', '2015-11-30 07:46:52', 'Logged In'),
(1441, 'admin', '66.49.237.16', 'dduv45or67ndp30m0igkc6qr04', '2015-11-30', '2015-11-30 09:00:22', 'Logged In'),
(1442, 'admin', '66.49.237.16', 'dkm5rb1lr3u8tkm7co8ll7l891', '2015-11-30', '2015-11-30 13:42:27', 'Logged In'),
(1443, 'admin', '66.49.237.16', 'dkm5rb1lr3u8tkm7co8ll7l891', '2015-11-30', '2015-11-30 13:44:22', 'Edited Product'),
(1444, 'admin', '112.196.136.44', 'h2ke4u8at6nfmpcun3orfhs9m1', '2015-11-30', '2015-11-30 23:20:46', 'Logged In'),
(1445, 'admin', '112.196.136.44', 'h2ke4u8at6nfmpcun3orfhs9m1', '2015-11-30', '2015-11-30 23:23:45', 'Edited Product'),
(1446, 'admin', '112.196.136.44', 'skj04bv5qs1pva4uf1ssqcs320', '2015-11-30', '2015-11-30 23:41:21', 'Logged In'),
(1447, 'admin', '112.196.136.78', 'ktp68dfmnmcitu7g699evsajc7', '2015-11-30', '2015-11-30 23:46:21', 'Logged In'),
(1448, 'admin', '112.196.136.78', '0e10s9votphl41prtnrkuqrlp7', '2015-12-01', '2015-12-01 00:00:47', 'Logged In'),
(1449, 'FTTP-8399', '112.196.136.78', 'fd5nnia17hlcl8488250beqdq5', '2015-12-01', '2015-12-01 00:01:32', 'Logged In'),
(1450, 'FMVI-7264', '112.196.136.78', 'sr38sit5b1qlhrikmv2shf62u6', '2015-12-01', '2015-12-01 00:03:12', 'Logged In'),
(1451, 'FMVI-7264', '112.196.136.78', 'sr38sit5b1qlhrikmv2shf62u6', '2015-12-01', '2015-12-01 00:04:57', 'Suggested Product'),
(1452, 'admin', '112.196.136.78', '0e10s9votphl41prtnrkuqrlp7', '2015-12-01', '2015-12-01 00:05:33', 'APPROVED New Product'),
(1453, 'ZJBX-0533', '112.196.136.78', '2s7r9kf669r6lsjmc301u4i3g6', '2015-12-01', '2015-12-01 00:15:59', 'Logged In'),
(1454, 'FMVI-7264', '112.196.136.78', 'sr38sit5b1qlhrikmv2shf62u6', '2015-12-01', '2015-12-01 00:19:11', 'Added Customer'),
(1455, 'FMVI-7264', '112.196.136.78', 'sr38sit5b1qlhrikmv2shf62u6', '2015-12-01', '2015-12-01 00:21:03', 'Added Posting'),
(1456, 'FXIO-8905', '112.196.136.78', 'tv8dnbl0hhi1qfb1muis6ld1b6', '2015-12-01', '2015-12-01 00:30:29', 'Logged In'),
(1457, 'admin', '112.196.136.78', 'lj65hsqdu9jrca54es5ophchv2', '2015-12-01', '2015-12-01 00:49:34', 'Logged In'),
(1458, 'admin', '112.196.136.78', 'jfoiq6ueci8dkt12v0kkqbk1s2', '2015-12-01', '2015-12-01 00:58:35', 'Logged In'),
(1459, 'ZJBX-0533', '112.196.136.78', '2s7r9kf669r6lsjmc301u4i3g6', '2015-12-01', '2015-12-01 01:05:59', 'Submitted Quote'),
(1460, 'FXIO-8905', '112.196.136.78', 'tv8dnbl0hhi1qfb1muis6ld1b6', '2015-12-01', '2015-12-01 01:11:11', 'Submitted Offer'),
(1461, 'admin', '112.196.136.44', '415v7j6uavb7p79nmnh67lcve1', '2015-12-01', '2015-12-01 01:38:17', 'Logged In'),
(1462, 'FMVI-7264', '112.196.136.44', 'r40sglna17g4rrjk2cb7vngou6', '2015-12-01', '2015-12-01 01:40:03', 'Logged In'),
(1463, 'admin', '112.196.136.44', '415v7j6uavb7p79nmnh67lcve1', '2015-12-01', '2015-12-01 01:54:17', 'Edited Product'),
(1464, 'admin', '112.196.136.44', '415v7j6uavb7p79nmnh67lcve1', '2015-12-01', '2015-12-01 01:59:47', 'Edited Product'),
(1465, 'admin', '112.196.136.78', 'lj65hsqdu9jrca54es5ophchv2', '2015-12-01', '2015-12-01 04:03:58', 'Edited Product'),
(1466, 'admin', '112.196.136.78', 'lj65hsqdu9jrca54es5ophchv2', '2015-12-01', '2015-12-01 04:04:33', 'Edited Product'),
(1467, 'admin', '112.196.136.78', 'lj65hsqdu9jrca54es5ophchv2', '2015-12-01', '2015-12-01 04:06:37', 'Edited Product'),
(1468, 'FTTP-8399', '112.196.136.78', 're412d4ec4nv0u5oc6dlh587a0', '2015-12-01', '2015-12-01 04:10:41', 'Logged In'),
(1469, 'admin', '112.196.136.44', 'ejl6nqnm01a93dumea767dcfs5', '2015-12-01', '2015-12-01 04:29:16', 'Logged In'),
(1470, 'admin', '112.196.136.78', 'cv4utotdu8m10onrneksv198v6', '2015-12-01', '2015-12-01 04:52:49', 'Logged In'),
(1471, 'admin', '112.196.136.78', 'jfoiq6ueci8dkt12v0kkqbk1s2', '2015-12-01', '2015-12-01 07:01:52', 'Logged Out'),
(1472, 'FMVI-7264', '112.196.136.78', 'tdua5nunl38ooo7h636pdsnr72', '2015-12-01', '2015-12-01 07:02:19', 'Logged In'),
(1473, 'admin', '112.196.136.78', 'iqqvmieufejnl5kkvk29qgjlr6', '2015-12-01', '2015-12-01 23:33:15', 'Logged In'),
(1474, 'RAUU-1599', '112.196.136.78', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-01', '2015-12-01 23:38:35', 'Logged In'),
(1475, 'RAUU-1599', '112.196.136.78', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-01', '2015-12-01 23:47:44', 'Suggested Product'),
(1476, 'admin', '112.196.136.78', 'iqqvmieufejnl5kkvk29qgjlr6', '2015-12-02', '2015-12-02 01:20:24', 'APPROVED New Product'),
(1477, 'RAUU-1599', '112.196.136.78', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-02', '2015-12-02 01:30:50', 'Suggested Product'),
(1478, 'admin', '112.196.136.78', 'iqqvmieufejnl5kkvk29qgjlr6', '2015-12-02', '2015-12-02 01:31:17', 'APPROVED New Product'),
(1479, 'RAUU-1599', '112.196.136.78', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-02', '2015-12-02 01:34:38', 'Suggested Product'),
(1480, 'ZJBX-0533', '112.196.136.78', '1msn9f8tpp7vfhtru9nb6sgfh1', '2015-12-02', '2015-12-02 02:09:14', 'Logged In'),
(1481, 'FTTP-8399', '112.196.136.78', 'ou66pi7qjs2vd99c6j1p0k9jd3', '2015-12-02', '2015-12-02 02:14:51', 'Logged In'),
(1482, 'FMVI-7264', '112.196.136.78', 'gl8cf67177e2drtmhpg2gialq2', '2015-12-02', '2015-12-02 02:16:13', 'Logged In'),
(1483, 'admin', '112.196.136.78', 'iqqvmieufejnl5kkvk29qgjlr6', '2015-12-02', '2015-12-02 02:22:07', 'Logged Out'),
(1484, 'ZJBX-0533', '112.196.136.78', 's6usil5vigttecqu3rquchmc23', '2015-12-02', '2015-12-02 02:22:14', 'Logged In'),
(1485, 'ZJBX-0533', '112.196.136.78', 's6usil5vigttecqu3rquchmc23', '2015-12-02', '2015-12-02 02:23:02', 'Submitted Quote'),
(1486, 'ZJBX-0533', '112.196.136.78', '1msn9f8tpp7vfhtru9nb6sgfh1', '2015-12-02', '2015-12-02 05:21:01', 'Logged Out'),
(1487, 'admin', '112.196.136.78', '0nn076gp7fbublja6d4gkm1hv6', '2015-12-02', '2015-12-02 05:21:26', 'Logged In'),
(1488, 'ZJBX-0533', '112.196.136.78', 's6usil5vigttecqu3rquchmc23', '2015-12-02', '2015-12-02 05:24:44', 'Logged Out'),
(1489, 'admin', '112.196.136.78', 'f6ah80t5eeojbpf8pdi2n63tp5', '2015-12-02', '2015-12-02 05:24:49', 'Logged In'),
(1490, 'RAUU-1599', '112.196.136.78', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-02', '2015-12-02 05:32:03', 'Added Posting'),
(1491, 'admin', '112.196.136.78', 'f6ah80t5eeojbpf8pdi2n63tp5', '2015-12-02', '2015-12-02 05:32:41', 'Logged Out'),
(1492, 'ZJBX-0533', '112.196.136.78', 'j6ra0i72efjpdnacq83lnchog7', '2015-12-02', '2015-12-02 05:32:44', 'Logged In'),
(1493, 'ZJBX-0533', '112.196.136.78', 'j6ra0i72efjpdnacq83lnchog7', '2015-12-02', '2015-12-02 05:33:05', 'Submitted Quote'),
(1494, 'ZJBX-0533', '112.196.136.78', 'j6ra0i72efjpdnacq83lnchog7', '2015-12-02', '2015-12-02 06:06:05', 'Logged Out'),
(1495, 'admin', '112.196.136.78', 'sjggm9i8bcndl7ugn7a21vr101', '2015-12-02', '2015-12-02 06:06:09', 'Logged In'),
(1496, 'admin', '112.196.136.78', 'sjggm9i8bcndl7ugn7a21vr101', '2015-12-02', '2015-12-02 06:22:27', 'Logged Out'),
(1497, 'ZJBX-0533', '112.196.136.78', 'qiq1o1tiuoqarqliuomju5e3q7', '2015-12-02', '2015-12-02 06:22:31', 'Logged In'),
(1498, 'ZJBX-0533', '112.196.136.78', 'qiq1o1tiuoqarqliuomju5e3q7', '2015-12-02', '2015-12-02 06:23:10', 'Submitted Quote'),
(1499, 'admin', '1.23.147.56', 'ci1728dn2pi19010t4fstirde6', '2015-12-02', '2015-12-02 06:36:16', 'Logged In'),
(1500, 'admin', '1.23.147.56', 'ci1728dn2pi19010t4fstirde6', '2015-12-02', '2015-12-02 06:39:15', 'Logged Out'),
(1501, 'RAUU-1599', '112.196.136.78', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-02', '2015-12-02 06:57:10', 'Offer accepted by Account Manager'),
(1502, 'FMVI-7264', '112.196.136.78', 'gl8cf67177e2drtmhpg2gialq2', '2015-12-02', '2015-12-02 07:12:39', 'Offer accepted by Account Manager'),
(1503, 'ZJBX-0533', '112.196.136.78', 'qiq1o1tiuoqarqliuomju5e3q7', '2015-12-02', '2015-12-02 07:22:42', 'Logged Out'),
(1504, 'admin', '112.196.136.78', 'fif6gvlukksj8ir1il8nfudr42', '2015-12-02', '2015-12-02 07:22:46', 'Logged In'),
(1505, 'admin', '112.196.136.78', '7qrcnna61l6e9i4stc936ejkh1', '2015-12-02', '2015-12-02 07:50:07', 'Logged In'),
(1506, 'admin', '66.49.237.16', 'm0p12r4gv3n4k29t9f6baijiv3', '2015-12-02', '2015-12-02 13:45:32', 'Logged In'),
(1507, 'admin', '66.49.237.16', 'm0p12r4gv3n4k29t9f6baijiv3', '2015-12-02', '2015-12-02 13:45:40', 'Logged Out'),
(1508, 'FXIO-8905', '66.49.237.16', '0lqev9caj7k4ogqk0rq0ah4c34', '2015-12-02', '2015-12-02 13:45:43', 'Logged In'),
(1509, 'admin', '112.196.136.78', '6gcdp7q45uqub0tf6tcrb66731', '2015-12-02', '2015-12-02 23:06:24', 'Logged In'),
(1510, 'FMVI-7264', '112.196.136.78', 'ngl0omakfkebbapvudl1v1doj0', '2015-12-02', '2015-12-02 23:08:47', 'Logged In'),
(1511, 'FXIO-8905', '112.196.136.78', 'qo748s234nte8s3h95j2vttac4', '2015-12-02', '2015-12-02 23:11:16', 'Logged In'),
(1512, 'admin', '112.196.136.78', 'c02jgt35tlacklpp133f1rimi7', '2015-12-02', '2015-12-02 23:34:41', 'Logged In'),
(1513, 'admin', '112.196.136.78', '6gcdp7q45uqub0tf6tcrb66731', '2015-12-02', '2015-12-02 23:42:54', 'Edited Product'),
(1514, 'FXIO-8905', '112.196.136.78', 'qo748s234nte8s3h95j2vttac4', '2015-12-03', '2015-12-03 00:47:39', 'Logged Out'),
(1515, 'FTTP-8399', '112.196.136.78', 'hbfnmh9774tetijaimge1lr2q1', '2015-12-03', '2015-12-03 00:48:20', 'Logged In'),
(1516, 'FTTP-8399', '112.196.136.78', 'hbfnmh9774tetijaimge1lr2q1', '2015-12-03', '2015-12-03 01:59:07', 'Logged Out'),
(1517, 'FXIO-8905', '112.196.136.78', 'ibe12lkbiegl4r4e6c054169k4', '2015-12-03', '2015-12-03 01:59:16', 'Logged In'),
(1518, 'admin', '112.196.136.78', 'jq8tn3bbs9p7lbe5bh7ll561j7', '2015-12-03', '2015-12-03 02:01:52', 'Logged In'),
(1519, 'admin', '112.196.136.78', '2nt8isql08huklolklr40l7db2', '2015-12-03', '2015-12-03 03:39:50', 'Logged In'),
(1520, 'admin', '112.196.136.78', '8kj2bqpnkgd51ho830j99t7la3', '2015-12-03', '2015-12-03 04:48:02', 'Logged In'),
(1521, 'admin', '112.196.136.78', '74ank0k3u67so1btb53j9jfh52', '2015-12-03', '2015-12-03 04:53:22', 'Logged In'),
(1522, 'admin', '112.196.136.78', 'gdpld5g7uichp84moofstv7g23', '2015-12-03', '2015-12-03 05:03:07', 'Logged In'),
(1523, 'FXIO-8905', '112.196.136.78', 'ibe12lkbiegl4r4e6c054169k4', '2015-12-03', '2015-12-03 07:05:39', 'Logged Out'),
(1524, 'admin', '112.196.136.78', '0gs6fqgipgon8aoq0pq4ibvu86', '2015-12-03', '2015-12-03 07:06:00', 'Logged In'),
(1525, 'admin', '112.196.136.78', '6gcdp7q45uqub0tf6tcrb66731', '2015-12-03', '2015-12-03 07:52:56', 'Logged Out'),
(1526, 'admin', '112.196.136.78', 'grclihkvo8orbj3fs2g5j8l065', '2015-12-03', '2015-12-03 07:57:48', 'Logged In'),
(1527, 'FMVI-7264', '112.196.136.78', 'ngl0omakfkebbapvudl1v1doj0', '2015-12-03', '2015-12-03 08:01:32', 'Logged Out'),
(1528, 'admin', '112.196.136.78', '806bdcbd3j8kjcuk09smfl12v0', '2015-12-03', '2015-12-03 08:01:39', 'Logged In'),
(1529, 'admin', '112.196.136.78', 'ht51fcs967bulte2im8l1u19s3', '2015-12-03', '2015-12-03 08:07:32', 'Logged In'),
(1530, 'admin', '66.49.237.16', 'vqvtb136tir3q8st8ejlfkrq72', '2015-12-03', '2015-12-03 08:45:44', 'Logged In'),
(1531, 'admin', '66.49.237.16', '90h9jsn16jbgqkrcgukjb0u7f7', '2015-12-03', '2015-12-03 08:46:35', 'Logged In'),
(1532, 'admin', '112.196.136.78', 'jpndp296bhs8kujg5iv9frvl30', '2015-12-03', '2015-12-03 23:34:54', 'Logged In'),
(1533, 'admin', '112.196.136.78', '2d6dc2n3va0vlgtd7oesup9986', '2015-12-04', '2015-12-04 01:14:38', 'Logged In'),
(1534, 'RAUU-1599', '112.196.136.78', 'krc5oversqe662012ag5kkmo62', '2015-12-04', '2015-12-04 01:15:48', 'Logged In'),
(1535, 'admin', '112.196.136.78', '3vhqll018iipdqvd0mq038i8j0', '2015-12-04', '2015-12-04 01:18:56', 'Logged In'),
(1536, 'admin', '112.196.136.78', '9eh1jp9irk2gqf33k648bm2ra7', '2015-12-04', '2015-12-04 01:19:28', 'Logged In'),
(1537, 'admin', '112.196.136.78', 'c7v2gp7vvgbe7gu87fh3p9enh5', '2015-12-04', '2015-12-04 02:27:46', 'Logged In'),
(1538, 'admin', '112.196.136.44', '040domab8pbprlpiiffgj8hlg0', '2015-12-04', '2015-12-04 03:20:02', 'Logged In'),
(1539, 'admin', '112.196.136.44', 'h29ue1qcla9b1l8u45s6mud8n6', '2015-12-04', '2015-12-04 03:30:58', 'Logged In'),
(1540, 'admin', '112.196.136.44', 'apai8l5g7nc4m4sqsiu7q9du37', '2015-12-04', '2015-12-04 03:32:31', 'Logged In'),
(1541, 'admin', '112.196.136.78', 'n53p1htc7am4gvjtp4ua4v8u03', '2015-12-04', '2015-12-04 03:35:53', 'Logged In'),
(1542, 'admin', '112.196.136.78', 'usf1d4ab2vargelfbi12u4fe52', '2015-12-04', '2015-12-04 03:38:59', 'Logged In'),
(1543, 'admin', '112.196.136.44', '4kp4r5ojt6s8fjeefvrkqtfiv3', '2015-12-04', '2015-12-04 04:39:48', 'Logged In'),
(1544, 'admin', '112.196.136.78', 'jpndp296bhs8kujg5iv9frvl30', '2015-12-04', '2015-12-04 04:53:35', 'Logged Out'),
(1545, 'admin', '112.196.136.78', 'rnipou756rg0jiq5jpbd8bcqk6', '2015-12-04', '2015-12-04 04:55:43', 'Logged In'),
(1546, 'admin', '112.196.136.78', 'ammi1do12o4s2e7kqrfcu3srj1', '2015-12-04', '2015-12-04 06:55:45', 'Logged In'),
(1547, 'admin', '112.196.136.78', 'l9u39se11s95e9fvp49ke8i020', '2015-12-04', '2015-12-04 07:01:16', 'Logged In'),
(1548, 'admin', '112.196.136.78', 'nl4474lvfj8tkviu53u93kgm55', '2015-12-04', '2015-12-04 07:03:22', 'Logged In'),
(1549, 'admin', '112.196.136.78', '90kobqtp0640rn1mv6inot0un3', '2015-12-04', '2015-12-04 07:06:15', 'Logged In'),
(1550, 'admin', '66.49.237.16', 'q70gqp0usic6mmmcngj6o35210', '2015-12-04', '2015-12-04 11:16:43', 'Logged In'),
(1551, 'admin', '112.196.136.78', 'pl467uq1lht1kgl91lbhdsaug0', '2015-12-04', '2015-12-04 22:45:52', 'Logged In'),
(1552, 'admin', '112.196.136.78', 'pl467uq1lht1kgl91lbhdsaug0', '2015-12-05', '2015-12-05 01:04:39', 'Logged Out'),
(1553, 'admin', '112.196.136.78', 'vv9hpmvviogfuvuje0ae1dlnm4', '2015-12-05', '2015-12-05 01:04:42', 'Logged In'),
(1554, 'admin', '112.196.136.78', 'vv9hpmvviogfuvuje0ae1dlnm4', '2015-12-05', '2015-12-05 01:45:27', 'Logged Out'),
(1555, 'admin', '112.196.136.78', 'qgl96u6lkj03joccms71uo6tp3', '2015-12-05', '2015-12-05 01:45:31', 'Logged In'),
(1556, 'admin', '112.196.136.78', 'qgl96u6lkj03joccms71uo6tp3', '2015-12-05', '2015-12-05 04:35:34', 'Logged Out'),
(1557, 'admin', '112.196.136.78', 'lk8k61g0jk7q9vkqjkod772vk0', '2015-12-05', '2015-12-05 04:35:38', 'Logged In'),
(1558, 'admin', '112.196.136.44', 'lk8k61g0jk7q9vkqjkod772vk0', '2015-12-05', '2015-12-05 04:54:55', 'Logged Out'),
(1559, 'admin', '112.196.136.44', '2umtstpt4vre1uvqldfbgrfem0', '2015-12-05', '2015-12-05 04:54:59', 'Logged In'),
(1560, 'admin', '112.196.136.78', 'o10bs9foru3urbau6nabip2bh4', '2015-12-07', '2015-12-07 06:43:42', 'Logged In'),
(1561, 'admin', '112.196.136.78', 'joiha1vvjbil8dtuhhocdmuu37', '2015-12-07', '2015-12-07 07:09:16', 'Logged In'),
(1562, 'admin', '112.196.136.78', 'm37lq65h1oau58q66j3c3179p5', '2015-12-07', '2015-12-07 07:11:18', 'Logged In'),
(1563, 'admin', '112.196.136.78', 'ro28rdsak80j92tjhe9leg6ah2', '2015-12-07', '2015-12-07 07:23:59', 'Logged In'),
(1564, 'admin', '112.196.136.78', 'qgifun6j82df3b63f2d69r98m7', '2015-12-07', '2015-12-07 07:25:19', 'Logged In'),
(1565, 'admin', '112.196.136.78', 'qgifun6j82df3b63f2d69r98m7', '2015-12-07', '2015-12-07 07:29:11', 'Logged Out'),
(1566, 'admin', '112.196.136.78', 'lp2pnl8ecnu8lhh86b8ec46q90', '2015-12-07', '2015-12-07 07:33:28', 'Logged In'),
(1567, 'admin', '112.196.136.78', 'lp2pnl8ecnu8lhh86b8ec46q90', '2015-12-07', '2015-12-07 07:38:00', 'Logged Out'),
(1568, 'admin', '112.196.136.78', '6pbuook4p7aln7j7401m0t8a96', '2015-12-07', '2015-12-07 08:38:19', 'Logged In'),
(1569, 'admin', '112.196.136.78', 'v90jriqnc0lolf0ocaop431ae7', '2015-12-07', '2015-12-07 08:42:59', 'Logged In'),
(1570, 'admin', '112.196.136.78', 'knpffnmi49tibiig2uunv402v6', '2015-12-10', '2015-12-10 23:33:56', 'Logged In'),
(1571, 'admin', '112.196.136.78', 'knpffnmi49tibiig2uunv402v6', '2015-12-10', '2015-12-10 23:34:30', 'Logged Out'),
(1572, 'admin', '112.196.136.78', 'a7pkqc4iuvceq31rgk7b8lron4', '2015-12-10', '2015-12-10 23:34:57', 'Logged In'),
(1573, 'admin', '112.196.136.78', 'trhaqmk21d7ut75j2p00t5nm20', '2015-12-11', '2015-12-11 00:53:35', 'Logged In'),
(1574, 'admin', '112.196.136.78', '2ndesa9fuart74nph22m9kq8b7', '2015-12-11', '2015-12-11 05:11:15', 'Logged In'),
(1575, 'admin', '112.196.136.78', '242gon00shqu0es6b9rso9c5e7', '2015-12-11', '2015-12-11 05:39:21', 'Logged In'),
(1576, 'admin', '112.196.136.78', '242gon00shqu0es6b9rso9c5e7', '2015-12-11', '2015-12-11 05:39:43', 'Logged Out'),
(1577, 'admin', '112.196.136.78', 'bvddo72nlaogpv6u4cb8m6j7i3', '2015-12-12', '2015-12-12 05:25:47', 'Logged In'),
(1578, 'admin', '112.196.136.78', 'm7jfj5lb8tnck1ke5sem2n12i5', '2015-12-14', '2015-12-14 07:11:58', 'Logged In'),
(1579, 'admin', '112.196.136.78', 'm7jfj5lb8tnck1ke5sem2n12i5', '2015-12-14', '2015-12-14 07:12:05', 'Logged Out'),
(1580, 'admin', '112.196.136.78', '79obctf8v3mb56i17qs9nu7ht2', '2015-12-14', '2015-12-14 07:12:27', 'Logged In'),
(1581, 'admin', '112.196.136.78', '79obctf8v3mb56i17qs9nu7ht2', '2015-12-14', '2015-12-14 07:13:16', 'Logged Out'),
(1582, 'admin', '112.196.136.78', 'qcli65jp4urukrdso04jk2l526', '2015-12-14', '2015-12-14 23:10:43', 'Logged In'),
(1583, 'admin', '112.196.136.78', 'qcli65jp4urukrdso04jk2l526', '2015-12-14', '2015-12-14 23:11:13', 'Logged Out'),
(1584, 'admin', '66.49.237.16', 'ri0651r213n786f924kv6526i3', '2015-12-15', '2015-12-15 13:44:35', 'Logged In'),
(1585, 'admin', '66.49.237.16', 'ri0651r213n786f924kv6526i3', '2015-12-15', '2015-12-15 14:15:22', 'Added Customer'),
(1586, 'admin', '66.49.237.16', 'ri0651r213n786f924kv6526i3', '2015-12-15', '2015-12-15 14:19:32', 'Logged Out'),
(1587, 'admin', '66.49.237.16', 'e07qj4frsvk2n9r09aqp7jn1c7', '2015-12-15', '2015-12-15 14:19:39', 'Logged In'),
(1588, 'admin', '66.49.237.16', 'e07qj4frsvk2n9r09aqp7jn1c7', '2015-12-15', '2015-12-15 14:19:45', 'Deleted Customer'),
(1589, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:28:47', 'Logged In'),
(1590, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:29:15', 'Updated Question'),
(1591, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:29:37', 'Updated Question'),
(1592, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:29:50', 'Updated Question'),
(1593, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:30:09', 'Added Question'),
(1594, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:30:19', 'Deleted Question'),
(1595, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:30:26', 'Added Question'),
(1596, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:30:34', 'Added Question'),
(1597, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:30:41', 'Added Question'),
(1598, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:30:45', 'Deleted Question'),
(1599, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:31:01', 'Deleted Question'),
(1600, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:31:03', 'Deleted Question'),
(1601, 'FMVI-7264', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:32:16', 'Deleted User'),
(1602, 'FTTP-8399', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:32:17', 'Deleted User'),
(1603, 'FXIO-8905', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:32:19', 'Deleted User'),
(1604, 'RAUU-1599', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:32:20', 'Deleted User'),
(1605, 'VUQQ-3341', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:32:22', 'Deleted User'),
(1606, 'ZJBX-0533', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:32:23', 'Deleted User'),
(1607, 'admin', '66.49.237.16', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', '2015-12-15 14:33:31', 'Updated User Settings'),
(1608, 'admin', '66.49.237.16', 'up7tgkg98kohmmevsgsv9gnl02', '2015-12-15', '2015-12-15 14:54:12', 'Logged In'),
(1609, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:12:10', 'Logged In'),
(1610, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:12:24', 'Added Industry'),
(1611, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:12:42', 'Added Category'),
(1612, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:13:01', 'Added SubCategory'),
(1613, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:13:48', 'Updated Industry'),
(1614, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:14:00', 'Updated Category'),
(1615, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:14:29', 'Added Brand'),
(1616, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:14:47', 'Updated Industry'),
(1617, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:14:54', 'Updated Industry'),
(1618, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:15:02', 'Added Industry'),
(1619, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:15:19', 'Added Category'),
(1620, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:15:33', 'Added SubCategory'),
(1621, 'admin', '66.49.237.16', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', '2015-12-15 15:15:45', 'Added Brand'),
(1622, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:16', 'Logged In'),
(1623, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:32', 'Deleted Brand'),
(1624, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:34', 'Deleted Brand'),
(1625, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:36', 'Deleted SubCategory'),
(1626, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:39', 'Deleted SubCategory'),
(1627, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:42', 'Deleted Category'),
(1628, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:46', 'Deleted Category'),
(1629, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:50', 'Deleted Industry'),
(1630, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:35:51', 'Deleted Industry'),
(1631, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:41:35', 'Imported Industries'),
(1632, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:43:18', 'Imported Categories'),
(1633, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:43:38', 'Imported SubCategories'),
(1634, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:43:57', 'Imported Brands'),
(1635, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:44:20', 'Imported Products'),
(1636, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:46:11', 'Imported Products'),
(1637, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:48:15', 'Imported Products'),
(1638, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:50:35', 'Imported Products'),
(1639, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:54:24', 'Imported Products'),
(1640, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:54:55', 'Imported Products'),
(1641, 'admin', '66.49.237.16', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', '2015-12-15 16:55:53', 'Imported Products'),
(1642, 'admin', '112.196.136.78', 'cpa9gl0posoi7ro0hr012p8bv3', '2015-12-16', '2015-12-16 00:25:08', 'Logged In'),
(1643, 'admin', '66.49.157.92', 'f7iu1p3j82r3ebd5r171filsv1', '2015-12-16', '2015-12-16 07:44:47', 'Logged In'),
(1644, 'ORYW-7613', '66.49.157.92', 'f7iu1p3j82r3ebd5r171filsv1', '2015-12-16', '2015-12-16 07:49:30', 'Added User'),
(1645, 'admin', '66.49.157.92', 'f7iu1p3j82r3ebd5r171filsv1', '2015-12-16', '2015-12-16 07:49:56', 'Logged Out'),
(1646, 'ORYW-7613', '66.49.157.92', 'dpcgitbniarq73rftvab4eb4n1', '2015-12-16', '2015-12-16 07:50:15', 'Logged In'),
(1647, 'ORYW-7613', '66.49.157.92', 'dpcgitbniarq73rftvab4eb4n1', '2015-12-16', '2015-12-16 07:57:27', 'Logged Out'),
(1648, 'admin', '66.49.237.16', 'aslsg34fk31flpd34snkd5d7d0', '2015-12-16', '2015-12-16 15:13:34', 'Logged In'),
(1649, 'admin', '66.49.237.16', 'js6d68hmdive6808vps9h77ch7', '2015-12-16', '2015-12-16 16:36:25', 'Logged In'),
(1650, 'admin', '69.171.142.218', 'ccc3rbv1p01pfcujiinagqkle6', '2015-12-17', '2015-12-17 11:30:36', 'Logged In'),
(1651, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:08:54', 'Logged In'),
(1652, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:24:55', 'Imported Industries'),
(1653, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:29:04', 'Imported Products'),
(1654, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:52:14', 'Added Product'),
(1655, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:57:52', 'Deleted Product'),
(1656, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:58:00', 'Deleted Product'),
(1657, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 15:58:17', 'Deleted Product'),
(1658, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:00:57', 'Edited Product'),
(1659, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:04:54', 'Deleted Product'),
(1660, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:08:28', 'Export Products to CSV'),
(1661, 'ORYW-7613', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:11:04', 'Deleted User'),
(1662, 'IXBB-4438', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:12:18', 'Added User');
INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`) VALUES
(1663, 'JUQI-3911', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:20:25', 'Added User'),
(1664, 'MSOX-3592', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:21:19', 'Added User'),
(1665, 'YWFZ-6999', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:22:06', 'Added User'),
(1666, 'GPUJ-2680', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:22:38', 'Added User'),
(1667, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:26:12', 'Updated User Settings'),
(1668, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:26:19', 'Updated User Settings'),
(1669, 'admin', '69.171.142.218', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', '2015-12-17 16:26:57', 'Updated User Settings'),
(1670, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-17', '2015-12-17 23:03:25', 'Logged In'),
(1671, 'admin', '112.196.136.78', 'nl9s6f9f0q3ikqte3ll7jfuch0', '2015-12-17', '2015-12-17 23:21:37', 'Logged In'),
(1672, 'admin', '112.196.136.78', 'nl9s6f9f0q3ikqte3ll7jfuch0', '2015-12-17', '2015-12-17 23:22:13', 'Added Question'),
(1673, 'admin', '112.196.136.78', 'nl9s6f9f0q3ikqte3ll7jfuch0', '2015-12-17', '2015-12-17 23:41:29', 'Added Product'),
(1674, 'admin', '112.196.136.78', 'nl9s6f9f0q3ikqte3ll7jfuch0', '2015-12-17', '2015-12-17 23:46:17', 'Deleted Product'),
(1675, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 00:06:53', 'Added Question'),
(1676, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 00:07:00', 'Deleted Question'),
(1677, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 00:13:59', 'Added Question'),
(1678, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 00:14:15', 'Deleted Question'),
(1679, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 00:39:13', 'Imported Industries'),
(1680, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 00:57:53', 'Imported Brands'),
(1681, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:11:03', 'Imported Brands'),
(1682, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:11:50', 'Imported Brands'),
(1683, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:16:55', 'Imported Brands'),
(1684, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:17:08', 'Imported Brands'),
(1685, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:21:19', 'Imported Brands'),
(1686, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:22:20', 'Imported Brands'),
(1687, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:44:36', 'Imported Brands'),
(1688, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 01:44:58', 'Imported Brands'),
(1689, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:00:12', 'Imported Brands'),
(1690, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:01:48', 'Imported Brands'),
(1691, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:14:50', 'Imported Brands'),
(1692, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:15:36', 'Imported Brands'),
(1693, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:16:38', 'Imported Brands'),
(1694, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:17:27', 'Imported Brands'),
(1695, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:19:21', 'Imported Brands'),
(1696, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:19:52', 'Imported Brands'),
(1697, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 02:30:43', 'Added Product'),
(1698, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:25:13', 'Deleted Product'),
(1699, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:26:29', 'Added Industry'),
(1700, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:26:34', 'Deleted Industry'),
(1701, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:29:37', 'Added Product'),
(1702, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:30:01', 'Deleted Product'),
(1703, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:31:04', 'Deleted Product'),
(1704, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:35:28', 'Deleted Product'),
(1705, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:36:01', 'Deleted Product'),
(1706, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:37:31', 'Deleted Product'),
(1707, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:41:47', 'Deleted Product'),
(1708, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:41:57', 'Deleted Product'),
(1709, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:42:02', 'Deleted Product'),
(1710, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:42:43', 'Deleted Product'),
(1711, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:43:29', 'Deleted Product'),
(1712, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:45:01', 'Deleted Product'),
(1713, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:45:34', 'Deleted Product'),
(1714, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:46:30', 'Deleted Product'),
(1715, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:47:08', 'Deleted Product'),
(1716, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:47:53', 'Deleted Product'),
(1717, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:48:10', 'Deleted Product'),
(1718, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:48:47', 'Deleted Product'),
(1719, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:48:56', 'Deleted Product'),
(1720, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:49:05', 'Deleted Product'),
(1721, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:49:35', 'Deleted Product'),
(1722, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:51:00', 'Updated User Settings'),
(1723, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:52:42', 'Updated User Settings'),
(1724, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 03:55:30', 'Export Products to CSV'),
(1725, 'admin', '112.196.136.78', 'jl7f5ufpso1dqo1a1bvc9bdni4', '2015-12-18', '2015-12-18 04:04:20', 'Logged In'),
(1726, 'admin', '112.196.136.78', 'jl7f5ufpso1dqo1a1bvc9bdni4', '2015-12-18', '2015-12-18 04:53:52', 'Deleted Product'),
(1727, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:02:47', 'Added Product'),
(1728, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:10:13', 'Added Product'),
(1729, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:15:11', 'Added Product'),
(1730, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:17:33', 'Added Product'),
(1731, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:38:07', 'Imported Products'),
(1732, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:39:12', 'Imported Products'),
(1733, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:39:43', 'Imported Products'),
(1734, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:40:03', 'Imported Products'),
(1735, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:40:35', 'Imported Products'),
(1736, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:41:03', 'Imported Products'),
(1737, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:41:44', 'Imported Products'),
(1738, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:42:53', 'Imported Products'),
(1739, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:43:25', 'Imported Products'),
(1740, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:43:58', 'Imported Products'),
(1741, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:44:34', 'Imported Products'),
(1742, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:45:02', 'Imported Products'),
(1743, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:52:27', 'Imported Products'),
(1744, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:53:39', 'Imported Products'),
(1745, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:54:20', 'Imported Products'),
(1746, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:54:50', 'Imported Products'),
(1747, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:55:16', 'Imported Products'),
(1748, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:56:19', 'Imported Products'),
(1749, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:57:43', 'Imported Products'),
(1750, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:58:14', 'Imported Products'),
(1751, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:59:17', 'Imported Products'),
(1752, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 05:59:50', 'Imported Products'),
(1753, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:18:02', 'Imported Products'),
(1754, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:18:35', 'Imported Products'),
(1755, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:20:50', 'Imported Products'),
(1756, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:23:53', 'Imported Products'),
(1757, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:24:45', 'Imported Products'),
(1758, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:28:56', 'Imported Products'),
(1759, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:30:00', 'Imported Products'),
(1760, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:31:37', 'Imported Products'),
(1761, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:33:07', 'Imported Products'),
(1762, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:33:51', 'Imported Products'),
(1763, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:34:29', 'Imported Products'),
(1764, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:36:03', 'Imported Products'),
(1765, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:36:54', 'Imported Products'),
(1766, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:37:21', 'Imported Products'),
(1767, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:41:35', 'Imported Products'),
(1768, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:42:49', 'Imported Products'),
(1769, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:43:16', 'Imported Products'),
(1770, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:44:39', 'Imported Products'),
(1771, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:45:02', 'Imported Products'),
(1772, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:45:52', 'Imported Products'),
(1773, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:46:56', 'Imported Products'),
(1774, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:47:32', 'Imported Products'),
(1775, 'admin', '112.196.136.78', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-18', '2015-12-18 06:47:59', 'Imported Products'),
(1776, 'admin', '112.196.136.78', 'slcri2jpsim4pm55n8edjd52l3', '2015-12-18', '2015-12-18 07:56:16', 'Logged In'),
(1777, 'admin', '112.196.136.78', 'slcri2jpsim4pm55n8edjd52l3', '2015-12-18', '2015-12-18 08:14:49', 'Added Product'),
(1778, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-18', '2015-12-18 22:53:36', 'Logged In'),
(1779, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 00:42:53', 'Imported Industries'),
(1780, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 00:57:15', 'Imported Categories'),
(1781, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 01:27:42', 'Imported Categories'),
(1782, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 01:29:34', 'Imported Categories'),
(1783, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 01:30:27', 'Imported Categories'),
(1784, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 02:02:00', 'Imported Products'),
(1785, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 02:13:09', 'Imported SubCategories'),
(1786, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 02:17:50', 'Imported SubCategories'),
(1787, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 02:21:36', 'Imported SubCategories'),
(1788, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 02:25:06', 'Imported SubCategories'),
(1789, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 02:28:50', 'Imported SubCategories'),
(1790, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:24:47', 'Imported Categories'),
(1791, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:25:49', 'Imported Categories'),
(1792, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:29:15', 'Imported Categories'),
(1793, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:33:24', 'Imported Categories'),
(1794, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:35:29', 'Imported Categories'),
(1795, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:38:18', 'Imported SubCategories'),
(1796, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:42:02', 'Imported Brands'),
(1797, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:45:51', 'Imported Brands'),
(1798, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:46:55', 'Imported Brands'),
(1799, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:49:41', 'Imported Brands'),
(1800, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 03:57:11', 'Imported Products'),
(1801, 'admin', '112.196.136.78', '76r0gg7lmc419surio3uhiu893', '2015-12-19', '2015-12-19 04:15:23', 'Logged Out'),
(1802, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 04:15:29', 'Logged In'),
(1803, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 04:50:07', 'Export Products to CSV'),
(1804, 'IXBB-4438', '112.196.136.78', 'm1o77jmp68jmni1gvh7u403el1', '2015-12-19', '2015-12-19 05:28:19', 'Logged In'),
(1805, 'IXBB-4438', '112.196.136.78', 'm1o77jmp68jmni1gvh7u403el1', '2015-12-19', '2015-12-19 05:31:26', 'Export Products to CSV'),
(1806, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 05:42:42', 'Export Products to CSV'),
(1807, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 05:49:30', 'Export Products to CSV'),
(1808, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 05:59:50', 'Export Products to CSV'),
(1809, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 06:03:38', 'Export Products to CSV'),
(1810, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 06:14:09', 'Export Products to CSV'),
(1811, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 06:16:25', 'Export Products to CSV'),
(1812, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 06:22:54', 'Export Products to CSV'),
(1813, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 06:29:08', 'Export Products to CSV'),
(1814, 'admin', '112.196.136.78', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', '2015-12-19 06:32:00', 'Logged Out'),
(1815, 'admin', '112.196.136.78', 'jlh1b6kf6ke1mng9kfncs6i6a6', '2015-12-19', '2015-12-19 06:32:07', 'Logged In'),
(1816, 'admin', '112.196.136.78', 'jlh1b6kf6ke1mng9kfncs6i6a6', '2015-12-19', '2015-12-19 06:32:15', 'Export Products to CSV'),
(1817, 'IXBB-4438', '112.196.136.78', 'm1o77jmp68jmni1gvh7u403el1', '2015-12-19', '2015-12-19 07:00:04', 'Logged Out'),
(1818, 'admin', '112.196.136.78', 'dtjfrl4d6ab9jaguf5t28vnse5', '2015-12-19', '2015-12-19 07:00:31', 'Logged In'),
(1819, 'admin', '112.196.136.78', 'dtjfrl4d6ab9jaguf5t28vnse5', '2015-12-19', '2015-12-19 07:00:41', 'Export Products to CSV'),
(1820, 'admin', '112.196.136.78', 'dtjfrl4d6ab9jaguf5t28vnse5', '2015-12-19', '2015-12-19 07:03:37', 'Export Products to CSV'),
(1821, 'admin', '112.196.136.78', '4jj7lbuuke91civv623roitq63', '2015-12-19', '2015-12-19 08:26:51', 'Logged In'),
(1822, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-20', '2015-12-20 23:18:32', 'Logged In'),
(1823, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-20', '2015-12-20 23:22:00', 'Export Products to CSV'),
(1824, 'admin', '112.196.136.78', '8u5j20km39lnu350209hrtqr73', '2015-12-20', '2015-12-20 23:35:45', 'Logged In'),
(1825, 'admin', '112.196.136.78', '8u5j20km39lnu350209hrtqr73', '2015-12-21', '2015-12-21 00:23:56', 'Export Products to CSV'),
(1826, 'IXBB-4438', '112.196.136.78', 'v3s0brrvs965dvgp9m60uk5se2', '2015-12-21', '2015-12-21 00:33:38', 'Logged In'),
(1827, 'IXBB-4438', '112.196.136.78', 'v3s0brrvs965dvgp9m60uk5se2', '2015-12-21', '2015-12-21 00:33:56', 'Added Customer'),
(1828, 'IXBB-4438', '112.196.136.78', 'v3s0brrvs965dvgp9m60uk5se2', '2015-12-21', '2015-12-21 00:43:15', 'Added Posting'),
(1829, 'admin', '112.196.136.78', 'iom10lk7k3j2l4ikg6810cume3', '2015-12-21', '2015-12-21 00:51:02', 'Logged In'),
(1830, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 03:39:24', 'Imported Brands'),
(1831, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 03:49:17', 'Imported Brands'),
(1832, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 03:55:49', 'Imported Brands'),
(1833, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 03:56:43', 'Imported Brands'),
(1834, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:07:26', 'Imported Brands'),
(1835, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:09:14', 'Imported Brands'),
(1836, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:10:10', 'Imported Brands'),
(1837, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:14:07', 'Imported Brands'),
(1838, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:16:20', 'Imported Brands'),
(1839, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:19:17', 'Imported Brands'),
(1840, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:20:48', 'Imported Brands'),
(1841, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:24:06', 'Imported Brands'),
(1842, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:26:05', 'Imported Brands'),
(1843, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:27:19', 'Imported Brands'),
(1844, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:28:51', 'Imported Brands'),
(1845, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:33:02', 'Imported Brands'),
(1846, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:37:49', 'Imported Brands'),
(1847, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:38:14', 'Imported Brands'),
(1848, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:40:17', 'Imported Brands'),
(1849, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:42:28', 'Imported Brands'),
(1850, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:44:05', 'Imported Brands'),
(1851, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:45:12', 'Imported Brands'),
(1852, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:49:14', 'Imported Brands'),
(1853, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 04:55:12', 'Imported Brands'),
(1854, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:00:03', 'Imported Brands'),
(1855, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:05:47', 'Imported Brands'),
(1856, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:06:32', 'Imported Brands'),
(1857, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:35:09', 'Imported Categories'),
(1858, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:40:04', 'Imported Categories'),
(1859, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:42:57', 'Imported Categories'),
(1860, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:45:12', 'Imported Categories'),
(1861, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:45:30', 'Imported Categories'),
(1862, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 05:49:43', 'Imported SubCategories'),
(1863, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 06:04:08', 'Imported SubCategories'),
(1864, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 06:30:00', 'Imported SubCategories'),
(1865, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 06:32:11', 'Imported SubCategories'),
(1866, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 06:41:38', 'Imported SubCategories'),
(1867, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 06:43:35', 'Imported SubCategories'),
(1868, 'admin', '112.196.136.78', 'lt2202cgdcb12pejog6es3n7g4', '2015-12-21', '2015-12-21 07:10:26', 'Logged In'),
(1869, 'admin', '112.196.136.78', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-21', '2015-12-21 07:22:10', 'Imported Products'),
(1870, 'IXBB-4438', '112.196.136.78', 'v3s0brrvs965dvgp9m60uk5se2', '2015-12-21', '2015-12-21 07:43:07', 'Added Posting'),
(1871, 'admin', '112.196.136.78', 'lt2202cgdcb12pejog6es3n7g4', '2015-12-21', '2015-12-21 07:53:40', 'Logged Out'),
(1872, 'admin', '112.196.136.78', 'neeo6di6gtfu32lupogie2imt0', '2015-12-21', '2015-12-21 07:53:43', 'Logged In'),
(1873, 'IXBB-4438', '112.196.136.78', 'epg9pm5upjn5ppgqselk18u821', '2015-12-21', '2015-12-21 07:54:48', 'Logged In'),
(1874, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:24:29', 'Logged In'),
(1875, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:25:22', 'Deleted Question'),
(1876, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:28:24', 'Updated User Settings'),
(1877, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:29:48', 'Export Products to CSV'),
(1878, 'IXBB-4438', '69.171.142.218', 'iu6sjs6m341q9msphuhh4n3sc0', '2015-12-23', '2015-12-23 14:47:02', 'Logged In'),
(1879, 'IXBB-4438', '69.171.142.218', 'iu6sjs6m341q9msphuhh4n3sc0', '2015-12-23', '2015-12-23 14:47:21', 'Updated User Settings'),
(1880, 'IXBB-4438', '69.171.142.218', 'iu6sjs6m341q9msphuhh4n3sc0', '2015-12-23', '2015-12-23 14:47:25', 'Logged Out'),
(1881, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:47:34', 'Logged In'),
(1882, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:48:13', 'Deleted Customer'),
(1883, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:48:22', 'Added Customer'),
(1884, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:48:28', 'Added Customer'),
(1885, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:48:35', 'Added Customer'),
(1886, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:48:53', 'Updated Customer Status'),
(1887, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:48:57', 'Updated Customer Status'),
(1888, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:51:04', 'Suggested Product'),
(1889, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:51:31', 'APPROVED New Product'),
(1890, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:52:50', 'Suggested New Product Code'),
(1891, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:53:05', 'Reported Product'),
(1892, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:53:19', 'Approved Suggested Product Code'),
(1893, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:54:00', 'Added Producted Report'),
(1894, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 14:54:10', 'Edited Product'),
(1895, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:55:02', 'Added Posting'),
(1896, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 14:55:32', 'Added Posting'),
(1897, 'IXBB-4438', '69.171.142.218', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', '2015-12-23 15:00:29', 'Logged Out'),
(1898, 'JUQI-3911', '69.171.142.218', 'hd8s79jebd63uvkrn1oa0vdig6', '2015-12-23', '2015-12-23 15:00:35', 'Logged In'),
(1899, 'JUQI-3911', '69.171.142.218', 'hd8s79jebd63uvkrn1oa0vdig6', '2015-12-23', '2015-12-23 15:00:45', 'Updated User Settings'),
(1900, 'JUQI-3911', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 15:01:47', 'Updated User'),
(1901, 'JUQI-3911', '69.171.142.218', 'hd8s79jebd63uvkrn1oa0vdig6', '2015-12-23', '2015-12-23 15:01:56', 'Logged Out'),
(1902, 'JUQI-3911', '69.171.142.218', '1j2b38h4v9oqrtb8ehfsb7ns54', '2015-12-23', '2015-12-23 15:02:00', 'Logged In'),
(1903, 'JUQI-3911', '69.171.142.218', '1j2b38h4v9oqrtb8ehfsb7ns54', '2015-12-23', '2015-12-23 15:03:51', 'Submitted Offer'),
(1904, 'JUQI-3911', '69.171.142.218', '1j2b38h4v9oqrtb8ehfsb7ns54', '2015-12-23', '2015-12-23 15:04:42', 'Submitted Offer'),
(1905, 'YWFZ-6999', '69.171.142.218', 'khvtmb79peur43k9u8tlor2aa7', '2015-12-23', '2015-12-23 15:06:59', 'Logged In'),
(1906, 'YWFZ-6999', '69.171.142.218', 'khvtmb79peur43k9u8tlor2aa7', '2015-12-23', '2015-12-23 15:07:51', 'Submitted Quote'),
(1907, 'YWFZ-6999', '69.171.142.218', 'khvtmb79peur43k9u8tlor2aa7', '2015-12-23', '2015-12-23 15:08:13', 'Submitted Quote'),
(1908, 'YWFZ-6999', '69.171.142.218', 'khvtmb79peur43k9u8tlor2aa7', '2015-12-23', '2015-12-23 15:09:01', 'Logged Out'),
(1909, 'JUQI-3911', '69.171.142.218', '1j2b38h4v9oqrtb8ehfsb7ns54', '2015-12-23', '2015-12-23 15:57:55', 'Logged Out'),
(1910, 'IXBB-4438', '69.171.142.218', 'go6c3pd8bfo6m07shtgbbbfjk1', '2015-12-23', '2015-12-23 15:58:09', 'Logged In'),
(1911, 'JUQI-3911', '69.171.142.218', 'ckihs3f3hc90cilenob3fkjbu2', '2015-12-23', '2015-12-23 15:58:19', 'Logged In'),
(1912, 'IXBB-4438', '69.171.142.218', 'go6c3pd8bfo6m07shtgbbbfjk1', '2015-12-23', '2015-12-23 15:59:52', 'Account Manager submitted a counter offer'),
(1913, 'IXBB-4438', '69.171.142.218', 'go6c3pd8bfo6m07shtgbbbfjk1', '2015-12-23', '2015-12-23 15:59:55', 'Offer declined by Account Manager'),
(1914, 'admin', '69.171.142.218', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', '2015-12-23 16:00:04', 'Logged Out'),
(1915, 'YWFZ-6999', '69.171.142.218', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', '2015-12-23 16:00:09', 'Logged In'),
(1916, 'YWFZ-6999', '69.171.142.218', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', '2015-12-23 16:01:33', 'Submitted Quote'),
(1917, 'YWFZ-6999', '69.171.142.218', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', '2015-12-23 16:01:44', 'Submitted Quote'),
(1918, 'YWFZ-6999', '69.171.142.218', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', '2015-12-23 16:02:10', 'Quote Faciltiato submitted a Counter Offer'),
(1919, 'YWFZ-6999', '69.171.142.218', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', '2015-12-23 16:02:14', 'Quote Faciltiato submitted a Counter Offer'),
(1920, 'IXBB-4438', '69.171.142.218', 'go6c3pd8bfo6m07shtgbbbfjk1', '2015-12-23', '2015-12-23 16:06:11', 'Logged Out'),
(1921, 'admin', '69.171.142.218', '6sta4lnpg460m2q2sqkcg5gm36', '2015-12-23', '2015-12-23 16:06:23', 'Logged In'),
(1922, 'YWFZ-6999', '69.171.142.218', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', '2015-12-23 16:07:25', 'Logged Out'),
(1923, 'admin', '69.171.142.218', 'oua1pa1k1l11oflri3kne9r8e1', '2015-12-23', '2015-12-23 16:07:28', 'Logged In'),
(1924, 'admin', '69.171.142.218', '6sta4lnpg460m2q2sqkcg5gm36', '2015-12-23', '2015-12-23 16:07:32', 'Logged Out'),
(1925, 'PZOI-0348', '69.171.142.218', 'oua1pa1k1l11oflri3kne9r8e1', '2015-12-23', '2015-12-23 16:08:46', 'Added User'),
(1926, 'PZOI-0348', '69.171.142.218', 'ak2edc9l7dr5oqp2kjrcdfqhq1', '2015-12-23', '2015-12-23 16:08:49', 'Logged In'),
(1927, 'PZOI-0348', '69.171.142.218', 'ak2edc9l7dr5oqp2kjrcdfqhq1', '2015-12-23', '2015-12-23 16:09:38', 'Logged Out'),
(1928, 'PZOI-0348', '69.171.142.218', 'kmkvhnh2e5hgvfnchfk7hieco1', '2015-12-23', '2015-12-23 16:09:40', 'Logged In'),
(1929, 'admin', '112.196.136.78', '3g7jseom1rnh03ecbugq46bel6', '2015-12-23', '2015-12-23 23:43:25', 'Logged In'),
(1930, 'YWFZ-6999', '112.196.136.78', '2g4u1cmsgjhiikr5ci7osm39k7', '2015-12-24', '2015-12-24 00:49:03', 'Logged In'),
(1931, 'YWFZ-6999', '112.196.136.78', '2g4u1cmsgjhiikr5ci7osm39k7', '2015-12-24', '2015-12-24 01:10:44', 'Logged Out'),
(1932, 'JUQI-3911', '112.196.136.78', '0nt7cjgm42ph1tdnscepkd9t27', '2015-12-24', '2015-12-24 01:11:33', 'Logged In'),
(1933, 'JUQI-3911', '112.196.136.78', '0nt7cjgm42ph1tdnscepkd9t27', '2015-12-24', '2015-12-24 01:12:02', 'Logged Out'),
(1934, 'IXBB-4438', '112.196.136.78', 'icgki9s7p6t8hpcojaro491pv6', '2015-12-24', '2015-12-24 01:13:51', 'Logged In'),
(1935, 'admin', '112.196.136.78', 'hqpe8knaluu1tn4kt525f3jcp0', '2015-12-24', '2015-12-24 04:34:48', 'Logged In'),
(1936, 'admin', '112.196.136.78', '3g7jseom1rnh03ecbugq46bel6', '2015-12-24', '2015-12-24 04:43:48', 'Logged Out'),
(1937, 'admin', '112.196.136.78', 'cik6egau3flv9th5cp5co44pu2', '2015-12-24', '2015-12-24 04:44:10', 'Logged In'),
(1938, 'admin', '112.196.136.78', 'cik6egau3flv9th5cp5co44pu2', '2015-12-24', '2015-12-24 04:44:17', 'Logged Out'),
(1939, 'IXBB-4438', '112.196.136.78', 'icgki9s7p6t8hpcojaro491pv6', '2015-12-24', '2015-12-24 04:45:48', 'Logged Out'),
(1940, 'IXBB-4438', '112.196.136.78', 'l4s13psseb5tri7pac8s6p2007', '2015-12-24', '2015-12-24 04:46:10', 'Logged In'),
(1941, 'IXBB-4438', '112.196.136.78', 'l4s13psseb5tri7pac8s6p2007', '2015-12-24', '2015-12-24 04:46:22', 'Logged Out'),
(1942, 'IXBB-4438', '112.196.136.78', '7jmjf4ipp4bt4n8sfpimtmccu2', '2015-12-24', '2015-12-24 04:46:42', 'Logged In'),
(1943, 'IXBB-4438', '112.196.136.78', '7jmjf4ipp4bt4n8sfpimtmccu2', '2015-12-24', '2015-12-24 04:46:44', 'Logged Out'),
(1944, 'admin', '112.196.136.78', 'ob1moumdagkl0ftcs7giqngi86', '2015-12-24', '2015-12-24 04:50:38', 'Logged In'),
(1945, 'admin', '112.196.136.78', 'ob1moumdagkl0ftcs7giqngi86', '2015-12-24', '2015-12-24 04:50:43', 'Logged Out'),
(1946, 'admin', '112.196.136.78', 'cot46q4om4jlck3ckpv6599fd5', '2015-12-24', '2015-12-24 04:51:02', 'Logged In'),
(1947, 'IXBB-4438', '112.196.136.78', 'hnfrv0nuf9ieectm4e0in9k722', '2015-12-24', '2015-12-24 04:52:25', 'Logged In'),
(1948, 'IXBB-4438', '112.196.136.78', 'nk7qlpkjdovhmdc55iab16avg0', '2015-12-24', '2015-12-24 04:59:46', 'Logged In'),
(1949, 'IXBB-4438', '112.196.136.78', 'hnfrv0nuf9ieectm4e0in9k722', '2015-12-24', '2015-12-24 05:21:13', 'Logged Out'),
(1950, 'IXBB-4438', '112.196.136.78', 'og9vjkndlf78ni0d0bet29t2i3', '2015-12-24', '2015-12-24 05:21:15', 'Logged In'),
(1951, 'admin', '112.196.136.78', 'cot46q4om4jlck3ckpv6599fd5', '2015-12-24', '2015-12-24 05:36:06', 'Logged Out'),
(1952, 'IXBB-4438', '112.196.136.78', 'ct8elu7bfn9img4pq9m44g5js2', '2015-12-24', '2015-12-24 05:36:15', 'Logged In'),
(1953, 'IXBB-4438', '112.196.136.78', 'og9vjkndlf78ni0d0bet29t2i3', '2015-12-24', '2015-12-24 05:47:15', 'Logged Out'),
(1954, 'admin', '112.196.136.78', 'n4f6o7dn2rf6trabjns1phpk60', '2015-12-24', '2015-12-24 05:47:30', 'Logged In'),
(1955, 'IXBB-4438', '112.196.136.78', 'ct8elu7bfn9img4pq9m44g5js2', '2015-12-24', '2015-12-24 05:57:45', 'Imported Products'),
(1956, 'IXBB-4438', '112.196.136.78', 'ct8elu7bfn9img4pq9m44g5js2', '2015-12-24', '2015-12-24 06:00:49', 'Imported Products'),
(1957, 'admin', '112.196.136.78', 'n4f6o7dn2rf6trabjns1phpk60', '2015-12-24', '2015-12-24 06:02:47', 'Logged Out'),
(1958, 'JUQI-3911', '112.196.136.78', 'as63fmtc5la603nbv6tqceeio3', '2015-12-24', '2015-12-24 06:02:59', 'Logged In'),
(1959, 'JUQI-3911', '112.196.136.78', 'as63fmtc5la603nbv6tqceeio3', '2015-12-24', '2015-12-24 06:18:03', 'Logged Out'),
(1960, 'JUQI-3911', '112.196.136.78', 'cv6es6o2soaflvhdb4o3n3cth4', '2015-12-24', '2015-12-24 06:18:06', 'Logged In'),
(1961, 'JUQI-3911', '112.196.136.78', '2m565l964h0l3f01i18t9tg0r0', '2015-12-24', '2015-12-24 06:18:09', 'Logged In'),
(1962, 'IXBB-4438', '112.196.136.78', 'ct8elu7bfn9img4pq9m44g5js2', '2015-12-24', '2015-12-24 06:18:19', 'Logged Out'),
(1963, 'admin', '112.196.136.78', '5h9muelcm2vig24hon5b7r3uh1', '2015-12-24', '2015-12-24 06:18:25', 'Logged In'),
(1964, 'JUQI-3911', '112.196.136.78', '2m565l964h0l3f01i18t9tg0r0', '2015-12-24', '2015-12-24 06:18:56', 'Logged Out'),
(1965, 'JUQI-3911', '112.196.136.78', 'cjtt34fjt58ajhb6iqg7da88m2', '2015-12-24', '2015-12-24 06:19:02', 'Logged In'),
(1966, 'admin', '112.196.136.78', '5h9muelcm2vig24hon5b7r3uh1', '2015-12-24', '2015-12-24 06:30:59', 'Logged Out'),
(1967, 'JUQI-3911', '112.196.136.78', 'i73a2ndst98fttjbmusc5ur695', '2015-12-24', '2015-12-24 06:31:07', 'Logged In'),
(1968, 'JUQI-3911', '112.196.136.78', 'i73a2ndst98fttjbmusc5ur695', '2015-12-24', '2015-12-24 06:36:32', 'Logged Out'),
(1969, 'JUQI-3911', '112.196.136.78', '4imhqhmreb29q6b66ieiqbrla6', '2015-12-24', '2015-12-24 06:37:00', 'Logged In'),
(1970, 'JUQI-3911', '112.196.136.78', '4imhqhmreb29q6b66ieiqbrla6', '2015-12-24', '2015-12-24 06:45:52', 'Logged Out'),
(1971, 'JUQI-3911', '112.196.136.78', 'eg8d4bp2p4m1csrskpvo6cudv3', '2015-12-24', '2015-12-24 06:46:02', 'Logged In'),
(1972, 'JUQI-3911', '112.196.136.78', '27edg0580tps53m8hfqilj9io4', '2015-12-24', '2015-12-24 07:13:29', 'Logged In'),
(1973, 'admin', '112.196.136.78', '5nm827ha51prpmno3ejsp9vsc1', '2015-12-24', '2015-12-24 23:13:22', 'Logged In'),
(1974, 'IXBB-4438', '112.196.136.78', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-24', '2015-12-24 23:31:28', 'Logged In'),
(1975, 'admin', '112.196.136.78', 'afqnsu6g61g2auktnrhp3v7nd5', '2015-12-25', '2015-12-25 00:10:55', 'Logged In'),
(1976, 'JUQI-3911', '112.196.136.78', 'oo0aiein258bfes4unq54ib3j1', '2015-12-25', '2015-12-25 00:20:33', 'Logged In'),
(1977, 'IXBB-4438', '112.196.136.78', 'mnfsm6k57gpqvue02ut4uacna2', '2015-12-25', '2015-12-25 00:21:32', 'Logged In'),
(1978, 'admin', '112.196.136.78', 'afqnsu6g61g2auktnrhp3v7nd5', '2015-12-25', '2015-12-25 00:45:20', 'Logged Out'),
(1979, 'JUQI-3911', '112.196.136.78', 'uio22ek710hmuut3c2e6ong7c0', '2015-12-25', '2015-12-25 00:45:27', 'Logged In'),
(1980, 'JUQI-3911', '112.196.136.78', 'uio22ek710hmuut3c2e6ong7c0', '2015-12-25', '2015-12-25 01:09:21', 'Logged Out'),
(1981, 'admin', '112.196.136.78', 'ioh6jo6ftaq2bj8lt0b3e69ku2', '2015-12-25', '2015-12-25 01:09:27', 'Logged In'),
(1982, 'admin', '112.196.136.78', 'ioh6jo6ftaq2bj8lt0b3e69ku2', '2015-12-25', '2015-12-25 01:16:12', 'Logged Out'),
(1983, 'admin', '112.196.136.78', 'p4ktrq8l6r6qns6e46f02d1dg5', '2015-12-25', '2015-12-25 01:17:03', 'Logged In'),
(1984, 'YWFZ-6999', '112.196.136.78', 'grll2pcfpc96nevoqqrl4jai56', '2015-12-25', '2015-12-25 01:17:46', 'Logged In'),
(1985, 'VYOC-6443', '112.196.136.78', 'p4ktrq8l6r6qns6e46f02d1dg5', '2015-12-25', '2015-12-25 01:33:54', 'Added User'),
(1986, 'YWFZ-6999', '112.196.136.78', 'grll2pcfpc96nevoqqrl4jai56', '2015-12-25', '2015-12-25 01:34:13', 'Logged Out'),
(1987, 'VYOC-6443', '112.196.136.78', '80onbhmj7j3qe9k6uujllp6j32', '2015-12-25', '2015-12-25 01:34:17', 'Logged In'),
(1988, 'admin', '112.196.136.78', '5nm827ha51prpmno3ejsp9vsc1', '2015-12-25', '2015-12-25 01:58:53', 'Export Products to CSV'),
(1989, 'admin', '112.196.136.78', 'p4ktrq8l6r6qns6e46f02d1dg5', '2015-12-25', '2015-12-25 02:08:30', 'Export Products to CSV'),
(1990, 'IXBB-4438', '112.196.136.78', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-25', '2015-12-25 04:24:06', 'Offer accepted by Account Manager'),
(1991, 'IXBB-4438', '112.196.136.78', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-25', '2015-12-25 04:35:47', 'Offer accepted by Account Manager'),
(1992, 'admin', '112.196.136.78', '5nm827ha51prpmno3ejsp9vsc1', '2015-12-25', '2015-12-25 06:19:40', 'Logged Out'),
(1993, 'admin', '112.196.136.78', '31ji9641eh16b4r8rr3kshh291', '2015-12-25', '2015-12-25 06:19:52', 'Logged In'),
(1994, 'admin', '112.196.136.78', '31ji9641eh16b4r8rr3kshh291', '2015-12-25', '2015-12-25 06:19:59', 'Logged Out'),
(1995, 'admin', '112.196.136.78', 's3kesp8h5tbadsbogdkorripd0', '2015-12-25', '2015-12-25 06:27:51', 'Logged In'),
(1996, 'admin', '112.196.136.78', 'p4ktrq8l6r6qns6e46f02d1dg5', '2015-12-25', '2015-12-25 06:36:46', 'Logged Out'),
(1997, 'JUQI-3911', '112.196.136.78', 'us2q3c8k998dq02lk21ab9ao33', '2015-12-25', '2015-12-25 06:36:52', 'Logged In'),
(1998, 'JUQI-3911', '112.196.136.78', 'us2q3c8k998dq02lk21ab9ao33', '2015-12-25', '2015-12-25 06:38:51', 'Submitted Offer'),
(1999, 'JUQI-3911', '112.196.136.78', 'us2q3c8k998dq02lk21ab9ao33', '2015-12-25', '2015-12-25 06:41:52', 'Logged Out'),
(2000, 'admin', '112.196.136.78', 'vbchp4ivjtm1uhq8d1f500mr56', '2015-12-25', '2015-12-25 06:41:56', 'Logged In'),
(2001, 'admin', '112.196.136.78', 'vbchp4ivjtm1uhq8d1f500mr56', '2015-12-25', '2015-12-25 06:42:10', 'Logged Out'),
(2002, 'GPUJ-2680', '112.196.136.78', '90rdomvsocb3kt51o679ouvht4', '2015-12-25', '2015-12-25 06:42:16', 'Logged In'),
(2003, 'IXBB-4438', '112.196.136.78', 'mnfsm6k57gpqvue02ut4uacna2', '2015-12-25', '2015-12-25 06:47:35', 'Logged Out'),
(2004, 'admin', '112.196.136.78', 'clprsu4403tvvgc7dfupsh4nd6', '2015-12-25', '2015-12-25 06:47:43', 'Logged In'),
(2005, 'IXBB-4438', '112.196.136.78', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-25', '2015-12-25 06:50:59', 'Offer accepted by Account Manager'),
(2006, 'IXBB-4438', '112.196.136.78', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-25', '2015-12-25 06:58:17', 'Offer accepted by Account Manager'),
(2007, 'GPUJ-2680', '112.196.136.78', '90rdomvsocb3kt51o679ouvht4', '2015-12-25', '2015-12-25 06:59:58', 'Logged Out'),
(2008, 'YWFZ-6999', '112.196.136.78', 'iht1rdirsnaqsediimr54rgqs3', '2015-12-25', '2015-12-25 07:00:02', 'Logged In'),
(2009, 'YWFZ-6999', '112.196.136.78', 'iht1rdirsnaqsediimr54rgqs3', '2015-12-25', '2015-12-25 07:00:18', 'Submitted Quote'),
(2010, 'IXBB-4438', '112.196.136.78', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-25', '2015-12-25 07:01:17', 'Offer accepted by Account Manager'),
(2011, 'admin', '112.196.136.78', 'clprsu4403tvvgc7dfupsh4nd6', '2015-12-25', '2015-12-25 07:01:32', 'Logged Out'),
(2012, 'YWFZ-6999', '112.196.136.78', 'iht1rdirsnaqsediimr54rgqs3', '2015-12-25', '2015-12-25 07:01:39', 'Logged Out'),
(2013, 'GPUJ-2680', '112.196.136.78', 'g4qo7t7sbaebtel72rac3sgr43', '2015-12-25', '2015-12-25 07:01:43', 'Logged In'),
(2014, 'admin', '112.196.136.78', '342nee7gjanhikc745383esc94', '2015-12-26', '2015-12-26 00:11:29', 'Logged In'),
(2015, 'IXBB-4438', '112.196.136.78', 'mp37m6k3sc0g3u4fn2qsvteuk4', '2015-12-26', '2015-12-26 00:13:07', 'Logged In'),
(2016, 'IXBB-4438', '112.196.136.78', 'ntop3ir6hkhqec5e9hbo8muh85', '2015-12-26', '2015-12-26 00:13:07', 'Logged In'),
(2017, 'IXBB-4438', '112.196.136.78', 'ntop3ir6hkhqec5e9hbo8muh85', '2015-12-26', '2015-12-26 01:27:59', 'Added Posting'),
(2018, 'YWFZ-6999', '112.196.136.78', 'itl8rga55j7psid280hijf3pj0', '2015-12-26', '2015-12-26 01:29:02', 'Logged In'),
(2019, 'YWFZ-6999', '112.196.136.78', 'itl8rga55j7psid280hijf3pj0', '2015-12-26', '2015-12-26 01:33:30', 'Submitted Quote'),
(2020, 'admin', '112.196.136.78', 'h8i9ii97dt4e7193g8h4mfqt27', '2015-12-26', '2015-12-26 01:34:29', 'Logged In'),
(2021, 'admin', '112.196.136.78', 'h8i9ii97dt4e7193g8h4mfqt27', '2015-12-26', '2015-12-26 01:34:40', 'Logged Out'),
(2022, 'IXBB-4438', '112.196.136.78', '14d4ll2k71cv2g86hgk90q7v44', '2015-12-26', '2015-12-26 01:34:54', 'Logged In'),
(2023, 'admin', '112.196.136.78', 'p9etqsiofa0hf41bhukrit5jt3', '2015-12-26', '2015-12-26 01:39:06', 'Logged In'),
(2024, 'YWFZ-6999', '112.196.136.78', 'gmp5otg60io0sbvb8a8qodqj26', '2015-12-26', '2015-12-26 01:40:26', 'Logged In'),
(2025, 'YWFZ-6999', '112.196.136.78', 'gmp5otg60io0sbvb8a8qodqj26', '2015-12-26', '2015-12-26 01:41:06', 'Submitted Quote'),
(2026, 'IXBB-4438', '112.196.136.78', 'ntop3ir6hkhqec5e9hbo8muh85', '2015-12-26', '2015-12-26 02:16:22', 'Offer accepted by Account Manager'),
(2027, 'YWFZ-6999', '112.196.136.78', 'itl8rga55j7psid280hijf3pj0', '2015-12-26', '2015-12-26 03:28:43', 'Logged Out'),
(2028, 'GPUJ-2680', '112.196.136.78', 'fi3kl3kalnnthldcoi3ohipjp4', '2015-12-26', '2015-12-26 03:31:29', 'Logged In'),
(2029, 'IXBB-4438', '112.196.136.78', '14d4ll2k71cv2g86hgk90q7v44', '2015-12-26', '2015-12-26 03:34:36', 'Logged Out'),
(2030, 'GPUJ-2680', '112.196.136.78', 'fi3kl3kalnnthldcoi3ohipjp4', '2015-12-26', '2015-12-26 03:34:37', 'Logged Out'),
(2031, 'JUQI-3911', '112.196.136.78', '92rtqbj0he4j7piee2rvmlca24', '2015-12-26', '2015-12-26 03:34:42', 'Logged In'),
(2032, 'JUQI-3911', '112.196.136.78', '92rtqbj0he4j7piee2rvmlca24', '2015-12-26', '2015-12-26 03:35:18', 'Logged Out'),
(2033, 'admin', '112.196.136.78', '02ecuc66sj99r76vle5hrvgus3', '2015-12-26', '2015-12-26 03:35:28', 'Logged In'),
(2034, 'YWFZ-6999', '112.196.136.78', '6mk0km395cpbnsol5lrcn3r082', '2015-12-26', '2015-12-26 03:35:37', 'Logged In'),
(2035, 'YWFZ-6999', '112.196.136.78', 'gmp5otg60io0sbvb8a8qodqj26', '2015-12-26', '2015-12-26 03:42:48', 'Logged Out'),
(2036, 'GPUJ-2680', '112.196.136.78', 'm997jc9mprblrf68a04hc84vh5', '2015-12-26', '2015-12-26 03:42:58', 'Logged In'),
(2037, 'GPUJ-2680', '112.196.136.78', 'm997jc9mprblrf68a04hc84vh5', '2015-12-26', '2015-12-26 03:44:25', 'Logged Out'),
(2038, 'YWFZ-6999', '112.196.136.78', '6mk0km395cpbnsol5lrcn3r082', '2015-12-26', '2015-12-26 04:17:24', 'Logged Out'),
(2039, 'YWFZ-6999', '112.196.136.78', 'v8vouiqv8j6qtpg21nu8fildl6', '2015-12-26', '2015-12-26 04:18:08', 'Logged In'),
(2040, 'YWFZ-6999', '112.196.136.78', 'v8vouiqv8j6qtpg21nu8fildl6', '2015-12-26', '2015-12-26 04:18:21', 'Logged Out'),
(2041, 'IXBB-4438', '112.196.136.78', 'ntop3ir6hkhqec5e9hbo8muh85', '2015-12-26', '2015-12-26 04:22:04', 'Logged Out'),
(2042, 'IXBB-4438', '112.196.136.78', 'e348fvu76t1vassf1albh6opv3', '2015-12-26', '2015-12-26 04:22:48', 'Logged In'),
(2043, 'admin', '112.196.136.78', '342nee7gjanhikc745383esc94', '2015-12-26', '2015-12-26 04:23:50', 'Logged Out'),
(2044, 'admin', '112.196.136.78', 'i2o97rnnc9augkbfkldku493p3', '2015-12-26', '2015-12-26 05:44:14', 'Logged In'),
(2045, 'ZSWD-9699', '112.196.136.78', 'i2o97rnnc9augkbfkldku493p3', '2015-12-26', '2015-12-26 05:50:43', 'Added User'),
(2046, 'admin', '112.196.136.78', 'i2o97rnnc9augkbfkldku493p3', '2015-12-26', '2015-12-26 05:51:13', 'Logged Out'),
(2047, 'ZSWD-9699', '112.196.136.78', '8j4k2mftnisl7778uceh1eaf83', '2015-12-26', '2015-12-26 05:51:25', 'Logged In'),
(2048, 'admin', '112.196.136.78', 'p9etqsiofa0hf41bhukrit5jt3', '2015-12-26', '2015-12-26 05:54:28', 'Logged Out'),
(2049, 'ZSWD-9699', '112.196.136.78', '1cf9sfu5hot392bkb6och0e4l7', '2015-12-26', '2015-12-26 05:55:03', 'Logged In'),
(2050, 'JUQI-3911', '112.196.136.78', 'rp6jebvpmenuaf1f2mkuem7930', '2015-12-26', '2015-12-26 05:56:54', 'Logged In'),
(2051, 'ZSWD-9699', '112.196.136.78', '1cf9sfu5hot392bkb6och0e4l7', '2015-12-26', '2015-12-26 06:00:00', 'Logged Out'),
(2052, 'ZSWD-9699', '112.196.136.78', 'op5os5pav3pdur6bnmcdj9htr6', '2015-12-26', '2015-12-26 06:00:03', 'Logged In'),
(2053, 'admin', '112.196.136.78', '7sibth08koo0pfpvk8nlhe11l2', '2015-12-26', '2015-12-26 06:02:15', 'Logged In'),
(2054, 'ZSWD-9699', '112.196.136.78', 'op5os5pav3pdur6bnmcdj9htr6', '2015-12-26', '2015-12-26 06:21:19', 'Logged Out'),
(2055, 'ZSWD-9699', '112.196.136.78', '2jr2hnq1dpn1rld86ugc7lgnc6', '2015-12-26', '2015-12-26 06:21:21', 'Logged In'),
(2056, 'RQYY-2239', '112.196.136.78', '02ecuc66sj99r76vle5hrvgus3', '2015-12-26', '2015-12-26 06:26:57', 'Added User'),
(2057, 'ZSWD-9699', '112.196.136.78', '2jr2hnq1dpn1rld86ugc7lgnc6', '2015-12-26', '2015-12-26 06:27:10', 'Logged Out'),
(2058, 'RQYY-2239', '112.196.136.78', 'bgscuetc73nlvl6ius4fm742f3', '2015-12-26', '2015-12-26 06:27:26', 'Logged In'),
(2059, 'RQYY-2239', '112.196.136.78', 'bgscuetc73nlvl6ius4fm742f3', '2015-12-26', '2015-12-26 06:28:44', 'Added Customer'),
(2060, 'ZSWD-9699', '112.196.136.78', '8j4k2mftnisl7778uceh1eaf83', '2015-12-26', '2015-12-26 06:44:41', 'Logged Out'),
(2061, 'admin', '112.196.136.78', 'omufm417ekpl5k3459d2kgul74', '2015-12-26', '2015-12-26 06:44:45', 'Logged In'),
(2062, 'JUQI-3911', '112.196.136.78', 'rp6jebvpmenuaf1f2mkuem7930', '2015-12-26', '2015-12-26 07:00:29', 'Logged Out'),
(2063, 'IXBB-4438', '112.196.136.78', 'atol93qnva4q1i60t3snb6krc5', '2015-12-26', '2015-12-26 07:01:31', 'Logged In'),
(2064, 'admin', '99.224.27.68', 'h6a6a8rf8neunhv7irfm77ca27', '2015-12-28', '2015-12-28 10:16:11', 'Logged In'),
(2065, 'admin', '99.224.27.68', 'h6a6a8rf8neunhv7irfm77ca27', '2015-12-28', '2015-12-28 10:25:44', 'Logged Out'),
(2066, 'admin', '99.224.27.68', 'dpt9ejsu1d1d11vlm53ho5adj2', '2015-12-28', '2015-12-28 10:25:49', 'Logged In'),
(2067, 'admin', '99.224.27.68', 'dpt9ejsu1d1d11vlm53ho5adj2', '2015-12-28', '2015-12-28 10:28:06', 'Logged Out'),
(2068, 'PZOI-0348', '99.224.27.68', '0j26jp0orcusljtslr0in5f2k2', '2015-12-28', '2015-12-28 10:28:09', 'Logged In'),
(2069, 'PZOI-0348', '99.224.27.68', '0j26jp0orcusljtslr0in5f2k2', '2015-12-28', '2015-12-28 10:28:29', 'Logged Out'),
(2070, 'admin', '99.224.27.68', 'log1jadm3vhpc9lmgeofm1oi74', '2015-12-28', '2015-12-28 10:28:43', 'Logged In'),
(2071, 'admin', '99.224.27.68', 'log1jadm3vhpc9lmgeofm1oi74', '2015-12-28', '2015-12-28 10:28:57', 'Logged Out'),
(2072, 'PZOI-0348', '99.224.27.68', 'h2a694iq0o0l54n2626ddu4e91', '2015-12-28', '2015-12-28 10:29:00', 'Logged In'),
(2073, 'PZOI-0348', '99.224.27.68', 'h2a694iq0o0l54n2626ddu4e91', '2015-12-28', '2015-12-28 10:29:20', 'Logged Out'),
(2074, 'admin', '99.224.27.68', '3ad7o0p2rqnuscijjstt2oq1f6', '2015-12-28', '2015-12-28 10:31:46', 'Logged In'),
(2075, 'IXBB-4438', '99.224.27.68', '3rofbai4vgkph686vlccivqh50', '2015-12-28', '2015-12-28 10:42:05', 'Logged In'),
(2076, 'IXBB-4438', '99.224.27.68', '3rofbai4vgkph686vlccivqh50', '2015-12-28', '2015-12-28 10:42:16', 'Reposted Posting'),
(2077, 'IXBB-4438', '99.224.27.68', '3rofbai4vgkph686vlccivqh50', '2015-12-28', '2015-12-28 10:42:35', 'Reposted Posting'),
(2078, 'IXBB-4438', '99.224.27.68', '3rofbai4vgkph686vlccivqh50', '2015-12-28', '2015-12-28 10:42:41', 'Logged Out'),
(2079, 'JUQI-3911', '99.224.27.68', 'rdirgh5aqfu75t1fqsh7mmobh5', '2015-12-28', '2015-12-28 10:42:53', 'Logged In');
INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`) VALUES
(2080, 'JUQI-3911', '99.224.27.68', 'rdirgh5aqfu75t1fqsh7mmobh5', '2015-12-28', '2015-12-28 10:45:12', 'Submitted Offer'),
(2081, 'JUQI-3911', '99.224.27.68', 'rdirgh5aqfu75t1fqsh7mmobh5', '2015-12-28', '2015-12-28 10:45:28', 'Submitted Offer'),
(2082, 'JUQI-3911', '99.224.27.68', 'rdirgh5aqfu75t1fqsh7mmobh5', '2015-12-28', '2015-12-28 10:45:40', 'Logged Out'),
(2083, 'IXBB-4438', '99.224.27.68', 'iagiilc067qj4n38mh549irmc7', '2015-12-28', '2015-12-28 10:45:56', 'Logged In'),
(2084, 'IXBB-4438', '99.224.27.68', 'iagiilc067qj4n38mh549irmc7', '2015-12-28', '2015-12-28 10:46:44', 'Logged Out'),
(2085, 'YWFZ-6999', '99.224.27.68', 'cpv75i6n76hheuoo07s631uf26', '2015-12-28', '2015-12-28 10:46:47', 'Logged In'),
(2086, 'YWFZ-6999', '99.224.27.68', 'cpv75i6n76hheuoo07s631uf26', '2015-12-28', '2015-12-28 10:47:44', 'Submitted Quote'),
(2087, 'YWFZ-6999', '99.224.27.68', 'cpv75i6n76hheuoo07s631uf26', '2015-12-28', '2015-12-28 10:47:59', 'Submitted Quote'),
(2088, 'YWFZ-6999', '99.224.27.68', 'cpv75i6n76hheuoo07s631uf26', '2015-12-28', '2015-12-28 10:48:51', 'Logged Out'),
(2089, 'IXBB-4438', '99.224.27.68', 'k9bbggkvkphc7do61kj2givq01', '2015-12-28', '2015-12-28 10:48:55', 'Logged In'),
(2090, 'IXBB-4438', '99.224.27.68', 'k9bbggkvkphc7do61kj2givq01', '2015-12-28', '2015-12-28 10:52:02', 'Offer declined by Account Manager'),
(2091, 'IXBB-4438', '99.224.27.68', 'k9bbggkvkphc7do61kj2givq01', '2015-12-28', '2015-12-28 10:52:19', 'Offer accepted by Account Manager'),
(2092, 'IXBB-4438', '99.224.27.68', 'k9bbggkvkphc7do61kj2givq01', '2015-12-28', '2015-12-28 10:52:32', 'Logged Out'),
(2093, 'GPUJ-2680', '99.224.27.68', 'ep9gfjqpugluhbuthb4migjlu4', '2015-12-28', '2015-12-28 10:52:44', 'Logged In'),
(2094, 'GPUJ-2680', '99.224.27.68', 'ep9gfjqpugluhbuthb4migjlu4', '2015-12-28', '2015-12-28 10:53:02', 'Logged Out'),
(2095, 'YWFZ-6999', '99.224.27.68', 'rre1qgpqkk5567fr1dp4os7cl4', '2015-12-28', '2015-12-28 10:53:18', 'Logged In'),
(2096, 'YWFZ-6999', '99.224.27.68', 'rre1qgpqkk5567fr1dp4os7cl4', '2015-12-28', '2015-12-28 10:53:52', 'Logged Out'),
(2097, 'admin', '112.196.136.78', 'ff27njtgeqlvjdj802h0bmc232', '2015-12-29', '2015-12-29 00:23:59', 'Logged In'),
(2098, 'admin', '112.196.136.78', 'ktqp019t9parolllku4disl836', '2015-12-29', '2015-12-29 00:56:52', 'Logged In'),
(2099, 'IXBB-4438', '112.196.136.78', 'nrqu0kndcie61bj1uillit27h1', '2015-12-29', '2015-12-29 00:57:43', 'Logged In'),
(2100, 'IXBB-4438', '112.196.136.78', 'nrqu0kndcie61bj1uillit27h1', '2015-12-29', '2015-12-29 02:03:30', 'Logged Out'),
(2101, 'YWFZ-6999', '112.196.136.78', '8aokcm8vg6ktq5ekv365gpmug0', '2015-12-29', '2015-12-29 02:03:42', 'Logged In'),
(2102, 'IXBB-4438', '112.196.136.78', '65mueame9igd60skqee2fg5og5', '2015-12-29', '2015-12-29 02:23:21', 'Logged In'),
(2103, 'IXBB-4438', '112.196.136.78', '65mueame9igd60skqee2fg5og5', '2015-12-29', '2015-12-29 04:07:18', 'Added Posting'),
(2104, 'IXBB-4438', '112.196.136.78', 'ktqp019t9parolllku4disl836', '2015-12-29', '2015-12-29 04:39:22', 'Updated User'),
(2105, 'IXBB-4438', '112.196.136.78', 'ktqp019t9parolllku4disl836', '2015-12-29', '2015-12-29 04:44:18', 'Updated User'),
(2106, 'IXBB-4438', '112.196.136.78', '65mueame9igd60skqee2fg5og5', '2015-12-29', '2015-12-29 04:45:00', 'Logged Out'),
(2107, 'IXBB-4438', '112.196.136.78', 'hcgtp00ph6neg6m6f2ltfuvpr7', '2015-12-29', '2015-12-29 04:45:12', 'Logged In'),
(2108, 'JUQI-3911', '112.196.136.78', '9ie64thrik8bbnemccispll0a2', '2015-12-29', '2015-12-29 04:58:24', 'Logged In'),
(2109, 'JUQI-3911', '112.196.136.78', 'ktqp019t9parolllku4disl836', '2015-12-29', '2015-12-29 04:59:21', 'Updated User'),
(2110, 'JUQI-3911', '112.196.136.78', '9ie64thrik8bbnemccispll0a2', '2015-12-29', '2015-12-29 04:59:31', 'Logged Out'),
(2111, 'JUQI-3911', '112.196.136.78', 'supipg03vneh13o1klj1n10dg6', '2015-12-29', '2015-12-29 04:59:39', 'Logged In'),
(2112, 'JUQI-3911', '112.196.136.78', 'supipg03vneh13o1klj1n10dg6', '2015-12-29', '2015-12-29 05:00:19', 'Submitted Offer'),
(2113, 'YWFZ-6999', '112.196.136.78', '8aokcm8vg6ktq5ekv365gpmug0', '2015-12-29', '2015-12-29 05:01:59', 'Submitted Quote'),
(2114, 'IXBB-4438', '112.196.136.78', 'hcgtp00ph6neg6m6f2ltfuvpr7', '2015-12-29', '2015-12-29 05:03:08', 'Offer accepted by Account Manager'),
(2115, 'admin', '112.196.136.78', 'ktqp019t9parolllku4disl836', '2015-12-29', '2015-12-29 05:10:37', 'Logged Out'),
(2116, 'admin', '112.196.136.78', 'fde36loj72nv0kks83npua5903', '2015-12-29', '2015-12-29 05:10:46', 'Logged In'),
(2117, 'JUQI-3911', '112.196.136.78', 'supipg03vneh13o1klj1n10dg6', '2015-12-29', '2015-12-29 06:25:07', 'Submitted Offer'),
(2118, 'YWFZ-6999', '112.196.136.78', '8aokcm8vg6ktq5ekv365gpmug0', '2015-12-29', '2015-12-29 06:26:05', 'Submitted Quote'),
(2119, 'IXBB-4438', '112.196.136.78', 'hcgtp00ph6neg6m6f2ltfuvpr7', '2015-12-29', '2015-12-29 06:27:05', 'Offer accepted by Account Manager'),
(2120, 'admin', '112.196.136.78', 'h88lrsbi7js2mqr9m36n3b1nh2', '2015-12-29', '2015-12-29 06:33:37', 'Logged In'),
(2121, 'admin', '99.224.27.68', 'k72u4rl9i5il3ir699muctgnt5', '2015-12-30', '2015-12-30 14:08:28', 'Logged In'),
(2122, 'admin', '99.224.27.68', 'bqfdccp8sqcccpnupcuhcn2hf2', '2015-12-30', '2015-12-30 14:19:21', 'Logged In'),
(2123, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:19:27', 'Logged In'),
(2124, 'admin', '99.224.27.68', 'bqfdccp8sqcccpnupcuhcn2hf2', '2015-12-30', '2015-12-30 14:19:47', 'Added Customer'),
(2125, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:20:04', 'Added Customer'),
(2126, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:20:10', 'Updated Customer Status'),
(2127, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:20:13', 'Updated Customer Status'),
(2128, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:20:17', 'Updated Customer Status'),
(2129, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:23:08', 'Added Product'),
(2130, 'admin', '99.224.27.68', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', '2015-12-30 14:24:15', 'Logged Out'),
(2131, 'IXBB-4438', '99.224.27.68', 'khrbekfuqek4efbctdue3rf4j0', '2015-12-30', '2015-12-30 14:24:21', 'Logged In'),
(2132, 'IXBB-4438', '99.224.27.68', 'khrbekfuqek4efbctdue3rf4j0', '2015-12-30', '2015-12-30 14:25:13', 'Suggested Product'),
(2133, 'admin', '112.196.136.78', '1nrstvrsoh1l3mqq1qo658oh14', '2015-12-30', '2015-12-30 23:26:32', 'Logged In'),
(2134, 'admin', '112.196.136.78', '1nrstvrsoh1l3mqq1qo658oh14', '2015-12-30', '2015-12-30 23:26:46', 'Logged Out'),
(2135, 'IXBB-4438', '112.196.136.78', '72tr7qn0tcgs1677lttu8puqo6', '2015-12-30', '2015-12-30 23:26:56', 'Logged In'),
(2136, 'admin', '112.196.136.78', 'co67s7a8o7v008r2c2ukajh360', '2015-12-31', '2015-12-31 01:09:13', 'Logged In'),
(2137, 'admin', '112.196.136.78', '0hb8tgtbrl4evhdsvov88du857', '2015-12-31', '2015-12-31 02:00:44', 'Logged In'),
(2138, 'admin', '112.196.136.78', 'rg7k31n5714j46jelqg5r5n0c6', '2015-12-31', '2015-12-31 02:04:03', 'Logged In'),
(2139, 'IXBB-4438', '112.196.136.78', '7skrdv0svch92dh20shjdp9bn7', '2015-12-31', '2015-12-31 02:09:43', 'Logged In'),
(2140, 'JUQI-3911', '112.196.136.78', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', '2015-12-31 02:10:28', 'Logged In'),
(2141, 'JUQI-3911', '112.196.136.78', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', '2015-12-31 02:11:08', 'Submitted Offer'),
(2142, 'YWFZ-6999', '112.196.136.78', 'ppvig9hpu6v8pfhdmo86803rg3', '2015-12-31', '2015-12-31 03:21:35', 'Logged In'),
(2143, 'YWFZ-6999', '112.196.136.78', 'ppvig9hpu6v8pfhdmo86803rg3', '2015-12-31', '2015-12-31 03:39:19', 'Submitted Quote'),
(2144, 'YWFZ-6999', '112.196.136.78', 'ppvig9hpu6v8pfhdmo86803rg3', '2015-12-31', '2015-12-31 03:51:39', 'Submitted Quote'),
(2145, 'JUQI-3911', '112.196.136.78', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', '2015-12-31 03:52:37', 'Submitted Offer'),
(2146, 'JUQI-3911', '112.196.136.78', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', '2015-12-31 04:02:34', 'Submitted Offer'),
(2147, 'GPUJ-2680', '112.196.136.78', '7b0latmadsl5e73f0a1co818i0', '2015-12-31', '2015-12-31 04:06:20', 'Logged In'),
(2148, 'IXBB-4438', '112.196.136.78', '7skrdv0svch92dh20shjdp9bn7', '2015-12-31', '2015-12-31 04:09:32', 'Account Manager submitted a counter offer'),
(2149, 'JUQI-3911', '112.196.136.78', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', '2015-12-31 04:19:20', 'Submitted Offer'),
(2150, 'YWFZ-6999', '112.196.136.78', 'ppvig9hpu6v8pfhdmo86803rg3', '2015-12-31', '2015-12-31 04:28:16', 'Quote Faciltiato submitted a Counter Offer'),
(2151, 'GPUJ-2680', '112.196.136.78', '7b0latmadsl5e73f0a1co818i0', '2015-12-31', '2015-12-31 04:42:34', 'Logged Out'),
(2152, 'admin', '112.196.136.78', 'ns3trubjgbqo25587dqudvm372', '2015-12-31', '2015-12-31 04:42:39', 'Logged In'),
(2153, 'IXBB-4438', '112.196.136.78', '7skrdv0svch92dh20shjdp9bn7', '2015-12-31', '2015-12-31 05:17:46', 'Suggested New Product Code'),
(2154, 'IXBB-4438', '112.196.136.78', '7skrdv0svch92dh20shjdp9bn7', '2015-12-31', '2015-12-31 05:34:49', 'Suggested New Product Code'),
(2155, 'admin', '112.196.136.78', 'n07sngk1nlnhpj92mv9odtpeu0', '2015-12-31', '2015-12-31 05:52:11', 'Logged In'),
(2156, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 05:53:05', 'Logged In'),
(2157, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 05:53:25', 'Imported Products'),
(2158, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 06:00:51', 'Imported Products'),
(2159, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 06:12:07', 'Imported Products'),
(2160, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 06:20:08', 'Suggested New Product Code'),
(2161, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 06:35:47', 'Suggested New Product Code'),
(2162, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 06:52:15', 'Suggested Product'),
(2163, 'IXBB-4438', '112.196.136.78', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', '2015-12-31 07:00:46', 'Reported Product'),
(2164, 'admin', '112.196.136.78', 'imasv2tdtjeoojmqd8vrbkl1l0', '2016-01-04', '2016-01-04 04:05:52', 'Logged In'),
(2165, 'admin', '69.171.142.218', '3vakdj75b9r9pf7ijtvc3949d2', '2016-01-05', '2016-01-05 12:41:56', 'Logged In'),
(2166, 'IXBB-4438', '69.171.142.218', '9fhvvmdtpbv2d7vdeuvb3tu0r1', '2016-01-05', '2016-01-05 12:42:21', 'Logged In'),
(2167, 'admin', '69.171.142.218', '26njcqns322331ferm1furjup2', '2016-01-05', '2016-01-05 14:05:02', 'Logged In'),
(2168, 'IXBB-4438', '69.171.142.218', '73m71k3llg5eqor4pcnbmmcrf2', '2016-01-05', '2016-01-05 14:05:25', 'Logged In'),
(2169, 'IXBB-4438', '69.171.142.218', '73m71k3llg5eqor4pcnbmmcrf2', '2016-01-05', '2016-01-05 14:10:18', 'Suggested Product'),
(2170, 'IXBB-4438', '69.171.142.218', '73m71k3llg5eqor4pcnbmmcrf2', '2016-01-05', '2016-01-05 14:13:37', 'Suggested New Product Code'),
(2171, 'admin', '69.171.142.218', '26njcqns322331ferm1furjup2', '2016-01-05', '2016-01-05 14:15:13', 'Approved Suggested Product Code'),
(2172, 'IXBB-4438', '69.171.142.218', '73m71k3llg5eqor4pcnbmmcrf2', '2016-01-05', '2016-01-05 14:16:46', 'Reported Product'),
(2173, 'admin', '69.171.142.218', '26njcqns322331ferm1furjup2', '2016-01-05', '2016-01-05 14:41:51', 'Logged Out'),
(2174, 'JUQI-3911', '69.171.142.218', 'km4fr9agvt429kcqqm6u6d78n5', '2016-01-05', '2016-01-05 14:41:55', 'Logged In'),
(2175, 'JUQI-3911', '69.171.142.218', 'km4fr9agvt429kcqqm6u6d78n5', '2016-01-05', '2016-01-05 14:42:35', 'Submitted Offer'),
(2176, 'JUQI-3911', '69.171.142.218', 'km4fr9agvt429kcqqm6u6d78n5', '2016-01-05', '2016-01-05 14:43:12', 'Logged Out'),
(2177, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:43:16', 'Logged In'),
(2178, 'IXBB-4438', '69.171.142.218', 'b9jaqkhb88dm873udp9nio4300', '2016-01-05', '2016-01-05 14:46:54', 'Logged In'),
(2179, 'IXBB-4438', '69.171.142.218', 'b9jaqkhb88dm873udp9nio4300', '2016-01-05', '2016-01-05 14:47:03', 'Suggested New Product Code'),
(2180, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:49:43', 'Updated Customer Status'),
(2181, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:50:00', 'Updated Customer Status'),
(2182, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:54:16', 'Edited Product'),
(2183, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:55:23', 'Edited Product'),
(2184, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:56:24', 'PROCESSING New Product'),
(2185, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:59:08', 'Approved Suggested Product Code'),
(2186, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:59:11', 'Approved Suggested Product Code'),
(2187, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:59:14', 'Approved Suggested Product Code'),
(2188, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:59:37', 'Approved Suggested Product Code'),
(2189, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:59:52', 'Approved Suggested Product Code'),
(2190, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 14:59:55', 'Waiting Suggested Product Code'),
(2191, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:00:32', 'Approved Suggested Product Code'),
(2192, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:00:35', 'Approved Suggested Product Code'),
(2193, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:01:29', 'Approved Suggested Product Code'),
(2194, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:01:33', 'Approved Suggested Product Code'),
(2195, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:01:45', 'Approved Suggested Product Code'),
(2196, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:01:59', 'Approved Suggested Product Code'),
(2197, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:02:25', 'Approved Suggested Product Code'),
(2198, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:02:50', 'Approved Suggested Product Code'),
(2199, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:03:12', 'Added Producted Report'),
(2200, 'admin', '69.171.142.218', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', '2016-01-05 15:05:17', 'Added Posting'),
(2201, 'admin', '112.196.136.78', '0bkpj464roq7l32imtbf6b63s1', '2016-01-05', '2016-01-05 23:18:23', 'Logged In'),
(2202, 'admin', '112.196.136.78', '0bkpj464roq7l32imtbf6b63s1', '2016-01-05', '2016-01-05 23:18:36', 'Logged Out'),
(2203, 'IXBB-4438', '112.196.136.78', 'l8oqnt4r2eouki9i87not5k922', '2016-01-05', '2016-01-05 23:18:49', 'Logged In'),
(2204, 'IXBB-4438', '112.196.136.78', 'l8oqnt4r2eouki9i87not5k922', '2016-01-05', '2016-01-05 23:19:10', 'Suggested New Product Code'),
(2205, 'IXBB-4438', '112.196.136.78', 'l8oqnt4r2eouki9i87not5k922', '2016-01-05', '2016-01-05 23:20:49', 'Suggested New Product Code'),
(2206, 'admin', '112.196.136.78', '4tb5jrt9spjklhjpbvcvdtjfp7', '2016-01-05', '2016-01-05 23:28:01', 'Logged In'),
(2207, 'IXBB-4438', '112.196.136.78', 'l8oqnt4r2eouki9i87not5k922', '2016-01-06', '2016-01-06 00:28:49', 'Logged Out'),
(2208, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 00:29:02', 'Logged In'),
(2209, 'IXBB-4438', '112.196.136.78', 'da54lmr0f4kkv10sv1jj2r9sm3', '2016-01-06', '2016-01-06 00:30:56', 'Logged In'),
(2210, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 00:32:30', 'Logged In'),
(2211, 'YWFZ-6999', '104.131.14.167', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 00:36:15', 'Logged In'),
(2212, 'YWFZ-6999', '104.131.14.167', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 00:38:46', 'Submitted Quote'),
(2213, 'YWFZ-6999', '104.131.14.167', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 00:40:56', 'Submitted Quote'),
(2214, 'YWFZ-6999', '104.131.14.167', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 00:57:24', 'Submitted Quote'),
(2215, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:00:41', 'Reported Product'),
(2216, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:02:05', 'Reported Product'),
(2217, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:09:24', 'Reported Product'),
(2218, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:33:49', 'Reported Product'),
(2219, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:35:46', 'Reported Product'),
(2220, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:54:07', 'Reported Product'),
(2221, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 01:54:27', 'Suggested New Product Code'),
(2222, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 02:02:09', 'Suggested Product'),
(2223, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 02:08:41', 'APPROVED New Product'),
(2224, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 02:21:01', 'Suggested Product'),
(2225, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 04:20:50', 'Approved Suggested Product Code'),
(2226, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 04:24:23', 'Approved Suggested Product Code'),
(2227, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 04:31:58', 'Approved Suggested Product Code'),
(2228, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 04:32:25', 'Approved Suggested Product Code'),
(2229, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 04:34:06', 'Approved Suggested Product Code'),
(2230, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 04:47:25', 'APPROVED New Product'),
(2231, 'IXBB-4438', '112.196.136.78', 'da54lmr0f4kkv10sv1jj2r9sm3', '2016-01-06', '2016-01-06 04:49:30', 'Suggested New Product Code'),
(2232, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 04:57:27', 'Suggested New Product Code'),
(2233, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 05:03:19', 'APPROVED New Product'),
(2234, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 05:06:17', 'REJECTED New Product'),
(2235, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 05:06:27', 'APPROVED New Product'),
(2236, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 05:10:25', 'Suggested Product'),
(2237, 'admin', '112.196.136.78', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', '2016-01-06 05:31:19', 'APPROVED New Product'),
(2238, 'YWFZ-6999', '104.236.195.130', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 06:02:20', 'Submitted Quote'),
(2239, 'YWFZ-6999', '104.236.195.130', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 06:17:26', 'Submitted Quote'),
(2240, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 06:25:50', 'Added Customer'),
(2241, 'JUQI-3911', '112.196.136.78', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', '2016-01-06 06:26:32', 'Added Posting'),
(2242, 'YWFZ-6999', '104.236.195.130', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', '2016-01-06 06:27:50', 'Submitted Quote'),
(2243, 'admin', '69.171.142.218', 'urtmgs61a3cvlnhmlp78ee61f6', '2016-01-06', '2016-01-06 09:38:49', 'Logged In'),
(2244, 'admin', '69.171.142.218', 'urtmgs61a3cvlnhmlp78ee61f6', '2016-01-06', '2016-01-06 09:39:01', 'Approved Suggested Product Code'),
(2245, 'admin', '69.171.142.218', 'urtmgs61a3cvlnhmlp78ee61f6', '2016-01-06', '2016-01-06 09:39:12', 'Approved Suggested Product Code'),
(2246, 'admin', '69.171.142.218', 'urtmgs61a3cvlnhmlp78ee61f6', '2016-01-06', '2016-01-06 09:44:11', 'PROCESSING New Product'),
(2247, 'admin', '69.171.142.218', 'uu8ftsmg59dtf14ap5p0qvjqb3', '2016-01-06', '2016-01-06 09:56:47', 'Logged In'),
(2248, 'admin', '69.171.142.218', 'uu8ftsmg59dtf14ap5p0qvjqb3', '2016-01-06', '2016-01-06 09:56:49', 'Logged Out'),
(2249, 'IXBB-4438', '69.171.142.218', 'uhcmce3fi5l98k1u1pnsp05ss5', '2016-01-06', '2016-01-06 09:56:56', 'Logged In'),
(2250, 'IXBB-4438', '69.171.142.218', 'uhcmce3fi5l98k1u1pnsp05ss5', '2016-01-06', '2016-01-06 09:58:43', 'Suggested New Product Code'),
(2251, 'IXBB-4438', '69.171.142.218', 'uhcmce3fi5l98k1u1pnsp05ss5', '2016-01-06', '2016-01-06 09:58:59', 'Reported Product'),
(2252, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 00:12:53', 'Logged In'),
(2253, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 01:26:00', 'Logged In'),
(2254, 'YWFZ-6999', '112.196.136.78', '032fp9rtrigejolirc5v1gosv6', '2016-01-07', '2016-01-07 02:14:06', 'Logged In'),
(2255, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 03:38:17', 'Updated Posting'),
(2256, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 03:40:35', 'Edited Product'),
(2257, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 03:41:01', 'Updated Posting'),
(2258, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 03:41:49', 'Edited Product'),
(2259, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 03:43:01', 'Edited Product'),
(2260, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 04:00:56', 'Edited Product'),
(2261, 'YWFZ-6999', '112.196.136.78', '032fp9rtrigejolirc5v1gosv6', '2016-01-07', '2016-01-07 04:17:23', 'Logged Out'),
(2262, 'GPUJ-2680', '112.196.136.78', '1qa0ptphoqkrq5774ggnnmlf96', '2016-01-07', '2016-01-07 04:17:47', 'Logged In'),
(2263, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 04:57:25', 'Updated Posting'),
(2264, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 04:57:57', 'APPROVED New Product'),
(2265, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 06:03:35', 'Suggested New Product Code'),
(2266, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 06:03:37', 'Suggested New Product Code'),
(2267, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 06:03:48', 'Suggested New Product Code'),
(2268, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 06:33:15', 'Suggested New Product Code'),
(2269, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 06:33:26', 'Suggested New Product Code'),
(2270, 'IXBB-4438', '112.196.136.78', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', '2016-01-07 06:34:58', 'Suggested New Product Code'),
(2271, 'admin', '112.196.136.78', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', '2016-01-07 06:36:06', 'Approved Suggested Product Code'),
(2272, 'admin', '112.196.136.78', 'aqi7t2o0ql1kuuo836nl37vtl0', '2016-01-07', '2016-01-07 07:55:46', 'Logged In'),
(2273, 'YWFZ-6999', '112.196.136.78', 'htk7bu6f2de2i3vfo5vccv6br5', '2016-01-07', '2016-01-07 07:57:09', 'Logged In'),
(2274, 'YWFZ-6999', '112.196.136.78', 'htk7bu6f2de2i3vfo5vccv6br5', '2016-01-07', '2016-01-07 07:59:20', 'Logged Out'),
(2275, 'IXBB-4438', '112.196.136.78', '8hp39ti7k4upl6fjulftlore74', '2016-01-07', '2016-01-07 07:59:49', 'Logged In'),
(2276, 'admin', '69.171.142.218', 'b000ooa1eua8i13qe0ekbo3234', '2016-01-07', '2016-01-07 11:11:50', 'Logged In'),
(2277, 'YWFZ-6999', '69.171.142.218', 'efjtt1egvukuepj5uke3go2mc5', '2016-01-07', '2016-01-07 11:12:43', 'Logged In'),
(2278, 'admin', '69.171.142.218', 'b000ooa1eua8i13qe0ekbo3234', '2016-01-07', '2016-01-07 11:14:01', 'Edited Product'),
(2279, 'admin', '112.196.136.78', 'k5kv399jag7q33detd8rf32pl6', '2016-01-07', '2016-01-07 23:42:05', 'Logged In'),
(2280, 'admin', '112.196.136.78', '8jc7dvbuosi909bl708a3felm2', '2016-01-08', '2016-01-08 00:00:51', 'Logged In'),
(2281, 'admin', '112.196.136.78', '8jc7dvbuosi909bl708a3felm2', '2016-01-08', '2016-01-08 00:07:45', 'Edited Product'),
(2282, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 01:48:02', 'Logged In'),
(2283, 'IXBB-4438', '112.196.136.78', 'i12ktsj2spi5d87ftpakb083u1', '2016-01-08', '2016-01-08 02:02:31', 'Logged In'),
(2284, 'IXBB-4438', '112.196.136.78', 'i12ktsj2spi5d87ftpakb083u1', '2016-01-08', '2016-01-08 03:40:00', 'Suggested Product'),
(2285, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 03:40:42', 'REJECTED New Product'),
(2286, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 03:41:39', 'APPROVED New Product'),
(2287, 'IXBB-4438', '112.196.136.78', 'i12ktsj2spi5d87ftpakb083u1', '2016-01-08', '2016-01-08 03:43:18', 'Suggested New Product Code'),
(2288, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 03:43:48', 'Pending Suggested Product Code'),
(2289, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 03:44:13', 'Approved Suggested Product Code'),
(2290, 'IXBB-4438', '112.196.136.78', 'i12ktsj2spi5d87ftpakb083u1', '2016-01-08', '2016-01-08 05:00:24', 'Logged Out'),
(2291, 'GPUJ-2680', '112.196.136.78', 'mlii3e71npt5ju9bht40h5no01', '2016-01-08', '2016-01-08 05:00:34', 'Logged In'),
(2292, 'GPUJ-2680', '112.196.136.78', 'mlii3e71npt5ju9bht40h5no01', '2016-01-08', '2016-01-08 06:58:16', 'Logged Out'),
(2293, 'IXBB-4438', '112.196.136.78', '5t3c1psc52vre1hamif7cj0d35', '2016-01-08', '2016-01-08 06:58:49', 'Logged In'),
(2294, 'IXBB-4438', '112.196.136.78', '5t3c1psc52vre1hamif7cj0d35', '2016-01-08', '2016-01-08 06:59:47', 'Suggested Product'),
(2295, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 07:00:11', 'APPROVED New Product'),
(2296, 'IXBB-4438', '112.196.136.78', '5t3c1psc52vre1hamif7cj0d35', '2016-01-08', '2016-01-08 07:02:18', 'Suggested Product'),
(2297, 'admin', '112.196.136.78', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', '2016-01-08 07:10:02', 'APPROVED New Product'),
(2298, 'admin', '112.196.136.78', 'j24l66nrodeihiiaq3p8a97r90', '2016-01-08', '2016-01-08 07:27:33', 'Logged In'),
(2299, 'admin', '69.171.142.218', 'us0oc45eo2ud4lp4ffjj9jivk6', '2016-01-08', '2016-01-08 09:13:08', 'Logged In'),
(2300, 'admin', '69.171.142.218', 'krjphnl23queo7jaulp5jpp2m6', '2016-01-08', '2016-01-08 09:28:00', 'Logged In'),
(2301, 'IXBB-4438', '69.171.142.218', 'tkraoovj0sg0e9u7m8oimoukv7', '2016-01-08', '2016-01-08 09:28:22', 'Logged In'),
(2302, 'IXBB-4438', '69.171.142.218', 'tkraoovj0sg0e9u7m8oimoukv7', '2016-01-08', '2016-01-08 09:28:54', 'Suggested Product'),
(2303, 'admin', '69.171.142.218', 'krjphnl23queo7jaulp5jpp2m6', '2016-01-08', '2016-01-08 09:29:58', 'APPROVED New Product'),
(2304, 'admin', '112.196.136.78', 'qt2ul7bevull36vjjp69tk4he6', '2016-01-09', '2016-01-09 00:29:07', 'Logged In'),
(2305, 'YWFZ-6999', '112.196.136.78', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', '2016-01-09 05:51:51', 'Logged In'),
(2306, 'admin', '112.196.136.78', 'qt2ul7bevull36vjjp69tk4he6', '2016-01-09', '2016-01-09 05:56:19', 'Added Posting'),
(2307, 'IXBB-4438', '112.196.136.78', 'h92lq5s976uglpo30lqogfur53', '2016-01-09', '2016-01-09 05:57:29', 'Logged In'),
(2308, 'IXBB-4438', '112.196.136.78', 'h92lq5s976uglpo30lqogfur53', '2016-01-09', '2016-01-09 05:58:22', 'Added Posting'),
(2309, 'IXBB-4438', '112.196.136.78', 'h92lq5s976uglpo30lqogfur53', '2016-01-09', '2016-01-09 05:59:15', 'Added Posting'),
(2310, 'YWFZ-6999', '112.196.136.78', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', '2016-01-09 06:03:59', 'Submitted Quote'),
(2311, 'YWFZ-6999', '112.196.136.78', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', '2016-01-09 06:05:14', 'Submitted Quote'),
(2312, 'IXBB-4438', '112.196.136.78', 'h92lq5s976uglpo30lqogfur53', '2016-01-09', '2016-01-09 06:07:33', 'Account Manager submitted a counter offer'),
(2313, 'YWFZ-6999', '112.196.136.78', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', '2016-01-09 06:09:28', 'Quote Facilitator Accepted by Offer'),
(2314, 'YWFZ-6999', '112.196.136.78', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', '2016-01-09 06:20:14', 'Submitted Quote'),
(2315, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:39:11', 'Logged In'),
(2316, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:40:32', 'Added Customer'),
(2317, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:40:33', 'Added Customer'),
(2318, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:40:34', 'Added Customer'),
(2319, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:40:49', 'Deleted Customer'),
(2320, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:41:31', 'Deleted Customer'),
(2321, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:42:31', 'Added Posting'),
(2322, 'YWFZ-6999', '112.196.136.78', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', '2016-01-09 06:43:09', 'Submitted Quote'),
(2323, 'MSOX-3592', '45.55.199.109', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', '2016-01-09 06:44:04', 'Offer declined by Account Manager'),
(2324, 'admin', '112.196.136.78', 'ihu5m078qg9q64acalaq515dg5', '2016-01-09', '2016-01-09 06:50:42', 'Logged In'),
(2325, 'IXBB-4438', '112.196.136.78', 'p4esprbobn4v9tu0obel2h6tc0', '2016-01-09', '2016-01-09 06:51:28', 'Logged In'),
(2326, 'IXBB-4438', '112.196.136.78', 'p4esprbobn4v9tu0obel2h6tc0', '2016-01-09', '2016-01-09 06:53:34', 'Suggested Product'),
(2327, 'admin', '112.196.136.78', 'ihu5m078qg9q64acalaq515dg5', '2016-01-09', '2016-01-09 06:56:17', 'APPROVED New Product'),
(2328, 'admin', '112.196.136.78', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', '2016-01-11 00:07:43', 'Logged In'),
(2329, 'admin', '112.196.136.78', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', '2016-01-11 00:40:02', 'Imported Categories'),
(2330, 'JUQI-3911', '112.196.136.78', 'q2dg0c72vs6lli1vhh22llgp02', '2016-01-11', '2016-01-11 05:32:16', 'Logged In'),
(2331, 'MSOX-3592', '112.196.136.78', 'fi9v727lvavlumt1dmo7a4ffc2', '2016-01-11', '2016-01-11 05:33:18', 'Logged In'),
(2332, 'MSOX-3592', '112.196.136.78', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', '2016-01-11 05:34:41', 'Updated User'),
(2333, 'MSOX-3592', '112.196.136.78', 'fi9v727lvavlumt1dmo7a4ffc2', '2016-01-11', '2016-01-11 05:34:57', 'Logged Out'),
(2334, 'MSOX-3592', '112.196.136.78', '39v9ghc018k321d86341puaq64', '2016-01-11', '2016-01-11 05:35:07', 'Logged In'),
(2335, 'admin', '112.196.136.78', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', '2016-01-11 05:46:34', 'Added Product Code'),
(2336, 'JUQI-3911', '112.196.136.78', 'q2dg0c72vs6lli1vhh22llgp02', '2016-01-11', '2016-01-11 05:47:23', 'Suggested New Product Code'),
(2337, 'MSOX-3592', '112.196.136.78', '39v9ghc018k321d86341puaq64', '2016-01-11', '2016-01-11 05:47:54', 'Suggested New Product Code'),
(2338, 'admin', '112.196.136.78', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', '2016-01-11 06:27:36', 'Approved Suggested Product Code'),
(2339, 'admin', '69.171.142.218', 'dmh0fv0hbsbbe5dgvoifn8v856', '2016-01-11', '2016-01-11 15:20:21', 'Logged In'),
(2340, 'admin', '69.171.142.218', 'dmh0fv0hbsbbe5dgvoifn8v856', '2016-01-11', '2016-01-11 15:23:49', 'Approved Suggested Product Code'),
(2341, 'admin', '69.171.142.218', 'dmh0fv0hbsbbe5dgvoifn8v856', '2016-01-11', '2016-01-11 15:24:24', 'Approved Suggested Product Code'),
(2342, 'admin', '112.196.136.78', 'tkpa2u7trmo9sod8ulci80de87', '2016-01-12', '2016-01-12 01:45:06', 'Logged In'),
(2343, 'IXBB-4438', '112.196.136.78', 'q2gkgd0lbm7unb3t6bsvsvkpm1', '2016-01-12', '2016-01-12 01:56:28', 'Logged In'),
(2344, 'IXBB-4438', '112.196.136.78', 'q2gkgd0lbm7unb3t6bsvsvkpm1', '2016-01-12', '2016-01-12 02:11:31', 'Added Product'),
(2345, 'admin', '112.196.136.78', 'tkpa2u7trmo9sod8ulci80de87', '2016-01-12', '2016-01-12 02:15:34', 'APPROVED New Product'),
(2346, 'IXBB-4438', '112.196.136.78', 'q2gkgd0lbm7unb3t6bsvsvkpm1', '2016-01-12', '2016-01-12 05:30:40', 'Logged Out'),
(2347, 'VYOC-6443', '112.196.136.78', 'as4a1a86df3r807n6ml7g2dqf3', '2016-01-12', '2016-01-12 05:31:05', 'Logged In'),
(2348, 'YWFZ-6999', '112.196.136.78', 'tkpa2u7trmo9sod8ulci80de87', '2016-01-12', '2016-01-12 05:35:09', 'Updated User'),
(2349, 'YSZJ-4486', '112.196.136.78', 'tkpa2u7trmo9sod8ulci80de87', '2016-01-12', '2016-01-12 05:36:23', 'Added User'),
(2350, 'VYOC-6443', '112.196.136.78', 'as4a1a86df3r807n6ml7g2dqf3', '2016-01-12', '2016-01-12 05:37:17', 'Logged Out'),
(2351, 'YSZJ-4486', '112.196.136.78', 'tlmfjukipnpenqr2f7p5sg2h80', '2016-01-12', '2016-01-12 05:37:33', 'Logged In'),
(2352, 'YWFZ-6999', '112.196.136.78', 'j097l0o1jdsrkkn0pvo3njb3o3', '2016-01-12', '2016-01-12 05:38:56', 'Logged In'),
(2353, 'admin', '112.196.136.78', 'q98m9dljm9nkee0vdvu1b0kdj3', '2016-01-12', '2016-01-12 06:51:54', 'Logged In'),
(2354, 'admin', '112.196.136.78', 'q98m9dljm9nkee0vdvu1b0kdj3', '2016-01-12', '2016-01-12 06:52:23', 'Logged Out'),
(2355, 'JUQI-3911', '112.196.136.78', '486he98k7q0dcpd6alolhevla5', '2016-01-12', '2016-01-12 06:52:30', 'Logged In'),
(2356, 'JUQI-3911', '112.196.136.78', '486he98k7q0dcpd6alolhevla5', '2016-01-12', '2016-01-12 06:54:33', 'Added Product'),
(2357, 'JUQI-3911', '112.196.136.78', '486he98k7q0dcpd6alolhevla5', '2016-01-12', '2016-01-12 06:55:24', 'Logged Out'),
(2358, 'admin', '112.196.136.78', 'ure7nfpocp4bcfbm9lb0741gb2', '2016-01-12', '2016-01-12 06:55:32', 'Logged In'),
(2359, 'admin', '112.196.136.78', 'ure7nfpocp4bcfbm9lb0741gb2', '2016-01-12', '2016-01-12 06:56:16', 'APPROVED New Product'),
(2360, 'admin', '69.171.142.218', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', '2016-01-12 08:24:17', 'Logged In'),
(2361, 'IXBB-4438', '69.171.142.218', 'muibrjrk65nmqfvlalmnnnosj4', '2016-01-12', '2016-01-12 08:26:57', 'Logged In'),
(2362, 'admin', '69.171.142.218', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', '2016-01-12 08:27:40', 'Added Industry'),
(2363, 'admin', '69.171.142.218', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', '2016-01-12 08:27:47', 'Added Category'),
(2364, 'admin', '69.171.142.218', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', '2016-01-12 08:27:56', 'Added SubCategory'),
(2365, 'admin', '69.171.142.218', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', '2016-01-12 08:28:06', 'Added Brand'),
(2366, 'IXBB-4438', '69.171.142.218', 'muibrjrk65nmqfvlalmnnnosj4', '2016-01-12', '2016-01-12 08:32:52', 'Added Product'),
(2367, 'admin', '69.171.142.218', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', '2016-01-12 08:33:10', 'APPROVED New Product'),
(2368, 'admin', '99.224.139.23', 'mp8berb68n7lfhfhdpjrrrd5g4', '2016-01-12', '2016-01-12 10:23:49', 'Logged In'),
(2369, 'admin', '99.224.139.23', 'mp8berb68n7lfhfhdpjrrrd5g4', '2016-01-12', '2016-01-12 10:25:39', 'Added Posting'),
(2370, 'admin', '99.224.139.23', 'mp8berb68n7lfhfhdpjrrrd5g4', '2016-01-12', '2016-01-12 10:26:55', 'Added Posting'),
(2371, 'admin', '69.171.142.218', '1vmh6orvh1gvlv6a2acp535q97', '2016-01-12', '2016-01-12 12:26:43', 'Logged In'),
(2372, 'admin', '69.171.142.218', 'jitenpa55kcvuf80k2mtf3tl00', '2016-01-12', '2016-01-12 13:36:37', 'Logged In'),
(2373, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:37:50', 'Logged In'),
(2374, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:38:57', 'Updated Customer'),
(2375, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:42:42', 'Updated Posting'),
(2376, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:42:53', 'Cancelled Posting'),
(2377, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:44:37', 'Added Product Code'),
(2378, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:46:29', 'Submitted Quote'),
(2379, 'admin', '69.171.142.218', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', '2016-01-12 13:47:12', 'Quote Faciltiato submitted a Counter Offer'),
(2380, 'admin', '69.171.142.218', 's9kha4ju66qsri6miuljnv1l12', '2016-01-12', '2016-01-12 13:56:38', 'Logged In'),
(2381, 'admin', '69.171.142.218', 'idmibtscmue94kb7ghg8a74al3', '2016-01-12', '2016-01-12 14:02:40', 'Logged In'),
(2382, 'admin', '69.171.142.218', 'idmibtscmue94kb7ghg8a74al3', '2016-01-12', '2016-01-12 14:04:24', 'Logged Out'),
(2383, 'admin', '69.171.142.218', 'ci7ld3bhc8nem24vuhg10k2l83', '2016-01-12', '2016-01-12 14:04:31', 'Logged In'),
(2384, 'admin', '69.171.142.218', 'ci7ld3bhc8nem24vuhg10k2l83', '2016-01-12', '2016-01-12 14:06:44', 'Reposted Posting'),
(2385, 'admin', '69.171.142.218', 'voesuspvtlfuns07rduc5ojj30', '2016-01-12', '2016-01-12 14:12:26', 'Logged In'),
(2386, 'admin', '112.196.136.78', 'm1tmb4jssostepgig1bjnh4o20', '2016-01-12', '2016-01-12 23:47:00', 'Logged In'),
(2387, 'admin', '112.196.136.78', 'sns6fqlmrlj7aasm3t4jknhbv2', '2016-01-13', '2016-01-13 02:08:33', 'Logged In'),
(2388, 'admin', '112.196.136.78', '9epk2un2mthour0oqrsj8m06q3', '2016-01-13', '2016-01-13 04:32:42', 'Logged In'),
(2389, 'admin', '112.196.136.78', 'm1tmb4jssostepgig1bjnh4o20', '2016-01-13', '2016-01-13 04:33:13', 'Logged Out'),
(2390, 'admin', '69.171.142.218', '953m32pertu8fnil2q0jppc2b0', '2016-01-13', '2016-01-13 12:14:29', 'Logged In'),
(2391, 'admin', '69.171.142.218', '7ciogo0n9o6umrt27s5qlli2r6', '2016-01-13', '2016-01-13 12:14:51', 'Logged In'),
(2392, 'admin', '69.171.142.218', '7biev57t5uqnplkpqjf52lr8b3', '2016-01-13', '2016-01-13 12:19:19', 'Logged In'),
(2393, 'admin', '69.171.142.218', 'hs35o953gppjatam7t29pi0p97', '2016-01-13', '2016-01-13 12:21:25', 'Logged In'),
(2394, 'admin', '69.171.142.218', 'ndrmie2a9a88mr3vv7v1rma8t2', '2016-01-13', '2016-01-13 12:22:28', 'Logged In'),
(2395, 'admin', '69.171.142.218', '97mco6v7p8tjsn1jegtpdfi3f1', '2016-01-13', '2016-01-13 14:50:37', 'Logged In'),
(2396, 'admin', '69.171.142.218', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', '2016-01-13 15:00:50', 'Logged In'),
(2397, 'admin', '69.171.142.218', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', '2016-01-13 15:04:00', 'Added Customer'),
(2398, 'admin', '69.171.142.218', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', '2016-01-13 15:05:35', 'Updated Customer'),
(2399, 'admin', '69.171.142.218', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', '2016-01-13 15:06:43', 'Deleted Customer'),
(2400, 'admin', '69.171.142.218', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', '2016-01-13 15:12:42', 'Imported Customers'),
(2401, 'admin', '69.171.142.218', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', '2016-01-13 15:18:14', 'Updated Customer Status'),
(2402, 'admin', '112.196.136.78', 'cmdf9e6dvcnq40trhrf0ou6de4', '2016-01-13', '2016-01-13 23:34:37', 'Logged In'),
(2403, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-13', '2016-01-13 23:57:12', 'Logged In'),
(2404, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-13', '2016-01-13 23:58:17', 'Updated Customer Status'),
(2405, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-13', '2016-01-13 23:58:26', 'Updated Customer Status'),
(2406, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-14', '2016-01-14 01:29:16', 'Added Posting'),
(2407, 'JUQI-3911', '112.196.136.78', 'r97ncj49lr65ovhurse8abk000', '2016-01-14', '2016-01-14 01:30:31', 'Logged In'),
(2408, 'JUQI-3911', '112.196.136.78', 'r97ncj49lr65ovhurse8abk000', '2016-01-14', '2016-01-14 01:35:34', 'Added Posting'),
(2409, 'YSZJ-4486', '104.236.225.168', '3i24qccaducrvam51702rfdn83', '2016-01-14', '2016-01-14 01:37:35', 'Logged In'),
(2410, 'YSZJ-4486', '104.236.225.168', '3i24qccaducrvam51702rfdn83', '2016-01-14', '2016-01-14 01:39:03', 'Submitted Quote'),
(2411, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-14', '2016-01-14 01:44:09', 'Account Manager submitted a counter offer'),
(2412, 'YSZJ-4486', '104.236.225.168', '3i24qccaducrvam51702rfdn83', '2016-01-14', '2016-01-14 01:47:02', 'Quote Faciltiato submitted a Counter Offer'),
(2413, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-14', '2016-01-14 01:48:05', 'Offer accepted by Account Manager'),
(2414, 'IXBB-4438', '112.196.136.78', 'g5petieosbsuk80l0qjaq139h3', '2016-01-14', '2016-01-14 03:39:21', 'Logged Out'),
(2415, 'YWFZ-6999', '112.196.136.78', 'etng0f9k3phuic9vh0lsc6nse3', '2016-01-14', '2016-01-14 03:39:33', 'Logged In'),
(2416, 'YWFZ-6999', '112.196.136.78', 'etng0f9k3phuic9vh0lsc6nse3', '2016-01-14', '2016-01-14 03:44:11', 'Submitted Quote'),
(2417, 'admin', '112.196.136.78', '9k70mfkeq967emptremgq463p6', '2016-01-14', '2016-01-14 05:13:03', 'Logged In'),
(2418, 'JUQI-3911', '112.196.136.78', 'r97ncj49lr65ovhurse8abk000', '2016-01-14', '2016-01-14 05:14:54', 'Logged Out'),
(2419, 'YSZJ-4486', '104.236.229.155', '3i24qccaducrvam51702rfdn83', '2016-01-14', '2016-01-14 05:15:26', 'Logged Out'),
(2420, 'admin', '112.196.136.78', 'p2ot53rdvsusum2ccl1j6s2cd5', '2016-01-14', '2016-01-14 05:20:34', 'Logged In'),
(2421, 'admin', '112.196.136.78', 'bbo965d6gk7lr7ju9e599umbh7', '2016-01-14', '2016-01-14 05:31:18', 'Logged In'),
(2422, 'admin', '112.196.136.78', 'p2ot53rdvsusum2ccl1j6s2cd5', '2016-01-14', '2016-01-14 05:33:08', 'Logged Out'),
(2423, 'admin', '112.196.136.78', 'b98amn09bhb0u8b1j1ieg9lte1', '2016-01-14', '2016-01-14 05:33:25', 'Logged In'),
(2424, 'admin', '112.196.136.78', 'n1jrcbo73uh95lhj3cvv5m6js2', '2016-01-14', '2016-01-14 06:01:10', 'Logged In'),
(2425, 'admin', '112.196.136.78', 'pr5v6r5dre6gtve7049krftvi2', '2016-01-14', '2016-01-14 06:54:29', 'Logged In'),
(2426, 'admin', '112.196.136.78', '77d11e76rlmgrrjs8o0eghbur5', '2016-01-14', '2016-01-14 07:50:15', 'Logged In'),
(2427, 'admin', '112.196.136.78', 'dnatvvg6m474s0m13sl6361514', '2016-01-14', '2016-01-14 07:51:16', 'Logged In'),
(2428, 'admin', '69.171.142.218', '3qg4att5odfbo6logequb1aac4', '2016-01-14', '2016-01-14 09:09:30', 'Logged In'),
(2429, 'admin', '69.171.142.218', '2d80rt06drj5cfproulpun7fm1', '2016-01-14', '2016-01-14 11:51:13', 'Logged In'),
(2430, 'admin', '66.49.211.86', 'ubbrcml747om3i17sc3ikcjbf2', '2016-01-14', '2016-01-14 12:28:36', 'Logged In'),
(2431, 'admin', '66.49.211.86', 'ubbrcml747om3i17sc3ikcjbf2', '2016-01-14', '2016-01-14 12:29:18', 'Logged Out'),
(2432, 'admin', '69.171.142.218', 'oulojqdqolfpraaueeejop9tf5', '2016-01-14', '2016-01-14 15:11:53', 'Logged In'),
(2433, 'admin', '69.171.142.218', '632bdoq4is8qgbfj65n58ofva5', '2016-01-14', '2016-01-14 15:12:32', 'Logged In'),
(2434, 'admin', '69.171.142.218', '632bdoq4is8qgbfj65n58ofva5', '2016-01-14', '2016-01-14 15:23:05', 'Added Product'),
(2435, 'admin', '69.171.142.218', '632bdoq4is8qgbfj65n58ofva5', '2016-01-14', '2016-01-14 15:25:16', 'Logged Out'),
(2436, 'admin', '69.171.142.218', 'eg9aqvs0d21itj3r8rnu4t0hl2', '2016-01-14', '2016-01-14 15:28:07', 'Logged In'),
(2437, 'admin', '66.49.211.86', 'srra49s09v8u3gn7jb0vu3bkk5', '2016-01-14', '2016-01-14 16:28:26', 'Logged In'),
(2438, 'admin', '69.171.142.218', '5ihrc27bt7rfl5nk3r66ao9jm2', '2016-01-14', '2016-01-14 16:29:57', 'Logged In'),
(2439, 'admin', '66.49.211.86', 'srra49s09v8u3gn7jb0vu3bkk5', '2016-01-14', '2016-01-14 16:31:54', 'Logged Out'),
(2440, 'admin', '112.196.136.78', 'vhb2mhms257vccn1ecp7avpsc6', '2016-01-15', '2016-01-15 00:23:49', 'Logged In'),
(2441, 'admin', '112.196.136.78', '02fviedigf6b6soa1joe2voll6', '2016-01-15', '2016-01-15 03:35:22', 'Logged In'),
(2442, 'admin', '112.196.136.78', 'bm636bsfjfbgos8vu5ita3r1j4', '2016-01-15', '2016-01-15 05:03:52', 'Logged In'),
(2443, 'admin', '112.196.136.78', '2ik1sdl3uvvecohonvub1vpqf5', '2016-01-15', '2016-01-15 05:05:37', 'Logged In'),
(2444, 'admin', '112.196.136.78', 'nh2u8ou0b14dq98g379fq918k6', '2016-01-15', '2016-01-15 05:05:37', 'Logged In'),
(2445, 'admin', '112.196.136.78', 'vlhllntale95geu2hfetqnmnt5', '2016-01-15', '2016-01-15 05:15:29', 'Logged In'),
(2446, 'admin', '112.196.136.78', 'nh2u8ou0b14dq98g379fq918k6', '2016-01-15', '2016-01-15 05:23:39', 'Logged Out'),
(2447, 'admin', '112.196.136.78', 'qndd0kbge7sob8mnb22nh5nvi7', '2016-01-15', '2016-01-15 05:23:49', 'Logged In'),
(2448, 'admin', '112.196.136.78', 'mohdp1032ao97an3bjlf7vr0i5', '2016-01-15', '2016-01-15 07:13:21', 'Logged In'),
(2449, 'admin', '112.196.136.78', 'qndd0kbge7sob8mnb22nh5nvi7', '2016-01-15', '2016-01-15 07:16:19', 'Logged Out'),
(2450, 'admin', '112.196.136.78', 'd3vbip0npr39aqu1vfcnejv3q6', '2016-01-15', '2016-01-15 07:16:35', 'Logged In'),
(2451, 'admin', '112.196.136.78', 'ac0ot43b18gth3pd2r1vgkp0q4', '2016-01-15', '2016-01-15 07:35:54', 'Logged In'),
(2452, 'admin', '112.196.136.78', 'lad206vpaveu1a9nm6tkvvhh07', '2016-01-16', '2016-01-16 00:52:53', 'Logged In'),
(2453, 'admin', '112.196.136.78', '868dvlgspkilkssd5go49ksdc1', '2016-01-16', '2016-01-16 00:54:20', 'Logged In'),
(2454, 'admin', '112.196.136.78', 'c7p5pvmf0c66noq132jh26rtt0', '2016-01-16', '2016-01-16 00:55:50', 'Logged In'),
(2455, 'admin', '112.196.136.78', 'k6i849luii4adibrh2qp7o3ki7', '2016-01-16', '2016-01-16 03:40:10', 'Logged In'),
(2456, 'admin', '69.171.142.218', 'tplemtco3ttpfqnln3k9sfori1', '2016-01-18', '2016-01-18 08:46:10', 'Logged In'),
(2457, 'IXBB-4438', '69.171.142.218', 'pb8ibdphbe2kanbtjp19osdu05', '2016-01-18', '2016-01-18 08:58:19', 'Logged In'),
(2458, 'IXBB-4438', '69.171.142.218', 'l3q508ka1potsj2q2g6teili86', '2016-01-18', '2016-01-18 09:06:31', 'Logged In'),
(2459, 'IXBB-4438', '69.171.142.218', 'l3q508ka1potsj2q2g6teili86', '2016-01-18', '2016-01-18 09:06:36', 'Deleted Customer'),
(2460, 'IXBB-4438', '69.171.142.218', 'l3q508ka1potsj2q2g6teili86', '2016-01-18', '2016-01-18 09:48:05', 'Logged Out'),
(2461, 'YSZJ-4486', '69.171.142.218', 'ek5atlfdneup1d0la4gqgfksu0', '2016-01-18', '2016-01-18 09:48:08', 'Logged In'),
(2462, 'admin', '69.171.142.218', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', '2016-01-18 11:51:00', 'Logged In'),
(2463, 'PZOI-0348', '69.171.142.218', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', '2016-01-18 11:51:32', 'Deleted User'),
(2464, 'RQYY-2239', '69.171.142.218', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', '2016-01-18 11:51:34', 'Deleted User'),
(2465, 'VYOC-6443', '69.171.142.218', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', '2016-01-18 11:51:36', 'Deleted User'),
(2466, 'ZSWD-9699', '69.171.142.218', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', '2016-01-18 11:51:38', 'Deleted User'),
(2467, 'OORQ-0229', '69.171.142.218', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', '2016-01-18 11:53:31', 'Added User'),
(2468, 'OORQ-0229', '69.171.142.218', 'ur18h772e1cihcmmjb91qkvc91', '2016-01-18', '2016-01-18 11:53:47', 'Logged In'),
(2469, 'admin', '112.196.136.78', '5r9qkoqu932q6bst866vtm4df1', '2016-01-18', '2016-01-18 23:26:02', 'Logged In'),
(2470, 'admin', '112.196.136.78', 'cssjqnvmjhisbgk02k2cm8adg2', '2016-01-19', '2016-01-19 00:42:36', 'Logged In'),
(2471, 'admin', '112.196.136.78', '9k6dp7r57kb8vr0rj8nk3aj2d3', '2016-01-19', '2016-01-19 00:49:07', 'Logged In'),
(2472, 'admin', '69.171.142.218', 'l9jbk8hv8aij33sti84bc7lul7', '2016-01-19', '2016-01-19 09:00:04', 'Logged In'),
(2473, 'admin', '69.171.142.218', 'v5rm0jr2n8cmipb7fpc6ipp6b1', '2016-01-19', '2016-01-19 09:00:55', 'Logged In'),
(2474, 'admin', '69.171.142.218', 'u3c7fvconfanf1v8je6psubga6', '2016-01-19', '2016-01-19 09:01:47', 'Logged In'),
(2475, 'admin', '66.49.211.86', 'ld1kssfd5l9ccetbtbr2esdrl4', '2016-01-19', '2016-01-19 09:05:26', 'Logged In'),
(2476, 'admin', '69.171.142.218', 'ibfr6mn0ud0n3a73ba3siqpua1', '2016-01-19', '2016-01-19 09:05:58', 'Logged In'),
(2477, 'admin', '69.171.142.218', '0mt5o0pg0v64ssd3od8heh92o3', '2016-01-19', '2016-01-19 09:08:23', 'Logged In'),
(2478, 'admin', '69.171.142.218', 'rd4elpfjugbvgjg0en8a7krt30', '2016-01-19', '2016-01-19 09:12:46', 'Logged In'),
(2479, 'admin', '66.49.211.86', 'ld1kssfd5l9ccetbtbr2esdrl4', '2016-01-19', '2016-01-19 09:14:08', 'Logged Out'),
(2480, 'admin', '66.49.211.86', 'casqiqn1r3d55mcbrd07esi622', '2016-01-19', '2016-01-19 09:14:45', 'Logged In'),
(2481, 'admin', '66.49.211.86', 'casqiqn1r3d55mcbrd07esi622', '2016-01-19', '2016-01-19 09:16:09', 'Logged Out'),
(2482, 'admin', '66.49.211.86', '45dm48gg4s4r9cjaesuvdknko3', '2016-01-19', '2016-01-19 09:33:43', 'Logged In'),
(2483, 'admin', '66.49.211.86', '45dm48gg4s4r9cjaesuvdknko3', '2016-01-19', '2016-01-19 09:34:18', 'Logged Out'),
(2484, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:34:43', 'Logged In'),
(2485, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:35:55', 'Updated User Settings'),
(2486, 'admin', '69.171.142.218', 't65l507u3qki2dldhdtpe8hao2', '2016-01-19', '2016-01-19 09:36:10', 'Logged In'),
(2487, 'OORQ-0229', '69.171.142.218', '1mnrgmcf0kvu5pg2vtcqn7vao6', '2016-01-19', '2016-01-19 09:41:42', 'Logged In'),
(2488, 'OORQ-0229', '69.171.142.218', '1mnrgmcf0kvu5pg2vtcqn7vao6', '2016-01-19', '2016-01-19 09:43:25', 'Submitted Offer'),
(2489, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:46:53', 'Submitted Offer'),
(2490, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:50:16', 'Added Customer'),
(2491, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:50:32', 'Added Customer'),
(2492, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:53:56', 'Added Posting');
INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`) VALUES
(2493, 'admin', '69.171.142.218', 't65l507u3qki2dldhdtpe8hao2', '2016-01-19', '2016-01-19 09:54:28', 'Submitted Quote'),
(2494, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 09:57:32', 'Account Manager submitted a counter offer'),
(2495, 'admin', '69.171.142.218', 't65l507u3qki2dldhdtpe8hao2', '2016-01-19', '2016-01-19 09:59:07', 'Quote Faciltiato submitted a Counter Offer'),
(2496, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 10:01:53', 'Offer accepted by Account Manager'),
(2497, 'OORQ-0229', '69.171.142.218', '304d8nh7p67uo5dsp639sn3833', '2016-01-19', '2016-01-19 10:11:55', 'Logged In'),
(2498, 'OORQ-0229', '66.49.211.86', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', '2016-01-19 11:11:24', 'Logged Out'),
(2499, 'admin', '66.49.211.86', 'gikl515oah3e91ffb4r67s2p56', '2016-01-20', '2016-01-20 10:53:01', 'Logged In'),
(2500, 'admin', '112.196.136.78', 'kkcs572685dldr1s8nhap87s34', '2016-01-21', '2016-01-21 07:21:57', 'Logged In'),
(2501, 'admin', '69.171.142.218', 'j2qr08p2vq3hvm3rq8oinqalc3', '2016-01-21', '2016-01-21 10:49:00', 'Logged In'),
(2502, 'admin', '69.171.142.218', 'j2qr08p2vq3hvm3rq8oinqalc3', '2016-01-21', '2016-01-21 10:49:09', 'Logged Out'),
(2503, 'IXBB-4438', '69.171.142.218', '5lkr656kf1stin8ancb2ti7k24', '2016-01-21', '2016-01-21 10:49:17', 'Logged In'),
(2504, 'IXBB-4438', '69.171.142.218', '5lkr656kf1stin8ancb2ti7k24', '2016-01-21', '2016-01-21 10:56:49', 'Logged Out'),
(2505, 'admin', '69.171.142.218', '2uoehgbjafc2isregf7kmfa057', '2016-01-21', '2016-01-21 10:56:54', 'Logged In'),
(2506, 'admin', '69.171.142.218', 'lte45u5j98g8psnncljibfu0k1', '2016-01-25', '2016-01-25 11:56:15', 'Logged In'),
(2507, 'admin', '69.171.142.218', 'gemg6ujsm6miqfi81sfdlhd530', '2016-01-25', '2016-01-25 12:01:31', 'Logged In'),
(2508, 'admin', '66.49.211.86', '5j9eiohs1fjouah68lqomqd1h7', '2016-01-26', '2016-01-26 08:12:53', 'Logged In'),
(2509, 'admin', '66.49.211.86', '5j9eiohs1fjouah68lqomqd1h7', '2016-01-26', '2016-01-26 08:13:14', 'Logged Out'),
(2510, 'admin', '1.39.49.146', '6a6gq3up3gcfqgf026ncnj3i17', '2016-01-27', '2016-01-27 06:51:15', 'Logged In'),
(2511, 'admin', '1.39.49.146', '6a6gq3up3gcfqgf026ncnj3i17', '2016-01-27', '2016-01-27 06:58:20', 'Logged Out'),
(2512, 'admin', '67.55.16.226', 'h0pm204fdinqnkgeleic831137', '2016-01-27', '2016-01-27 11:07:28', 'Logged In'),
(2513, 'admin', '67.55.16.226', 'f976c9akpvf59kcqt5bqhkj1e3', '2016-01-27', '2016-01-27 12:21:05', 'Logged In'),
(2514, 'admin', '189.102.169.77', 'ar2o4uogjgq0jhbv4eft2c8g20', '2016-01-27', '2016-01-27 14:25:46', 'Logged In'),
(2515, 'admin', '67.55.16.226', 'imb6i6pmcmustre4olh09liuv3', '2016-01-27', '2016-01-27 15:42:35', 'Logged In'),
(2516, 'admin', '67.55.16.226', 'imb6i6pmcmustre4olh09liuv3', '2016-01-27', '2016-01-27 15:49:41', 'Updated User Settings'),
(2517, 'admin', '67.55.16.226', '3mfcj8is0sl8rrrf18dfqoijj0', '2016-01-28', '2016-01-28 09:29:33', 'Logged In'),
(2518, 'admin', '67.55.16.226', '0q2b89e5086h0m872uh89ranq2', '2016-01-28', '2016-01-28 12:42:21', 'Logged In'),
(2519, 'admin', '125.62.103.136', 'moi1da01iarrkadst63gsf1u25', '2016-01-28', '2016-01-28 14:52:33', 'Logged In'),
(2520, 'admin', '66.49.250.8', 'b39374nlst3ftqst2cb0qph5k2', '2016-01-28', '2016-01-28 15:34:36', 'Logged In'),
(2521, 'admin', '66.49.250.8', 'b39374nlst3ftqst2cb0qph5k2', '2016-01-28', '2016-01-28 15:34:45', 'Logged Out'),
(2522, 'admin', '182.70.216.60', '582virek8nanhpp5u2k4h6g8f3', '2016-01-28', '2016-01-28 16:09:13', 'Logged In'),
(2523, 'admin', '43.229.224.130', 'tg8affpbd5bciob8l04j5plb73', '2016-01-29', '2016-01-29 03:18:20', 'Logged In'),
(2524, 'admin', '43.229.224.130', 'f0bclghpd1ob7etaa9pkc60kp5', '2016-01-29', '2016-01-29 03:48:42', 'Logged In'),
(2525, 'admin', '103.44.18.3', 'jfhqulmdmi0halb022eg46el12', '2016-01-29', '2016-01-29 11:35:38', 'Logged In'),
(2526, 'admin', '67.55.16.226', '6ouun8mnr6pccpij38j10aifk5', '2016-01-29', '2016-01-29 14:00:46', 'Logged In'),
(2527, 'admin', '67.55.16.226', '6b5790fcq5hoe02jdaegb1e5c0', '2016-01-29', '2016-01-29 16:40:51', 'Logged In'),
(2528, 'admin', '67.55.16.226', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', '2016-01-29 16:44:27', 'Logged In'),
(2529, 'admin', '67.55.16.226', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', '2016-01-29 16:45:07', 'Updated User Settings'),
(2530, 'admin', '67.55.16.226', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', '2016-01-29 16:45:50', 'Imported Industries'),
(2531, 'admin', '67.55.16.226', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', '2016-01-29 16:46:05', 'Imported Categories'),
(2532, 'admin', '67.55.16.226', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', '2016-01-29 16:46:17', 'Imported SubCategories'),
(2533, 'admin', '67.55.16.226', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', '2016-01-29 16:46:52', 'Imported Brands'),
(2534, 'admin', '191.181.15.233', 'poe8g8mk38m9drjmoeuf11k6m0', '2016-01-29', '2016-01-29 17:11:12', 'Logged In'),
(2535, 'admin', '191.181.15.233', '69278cc7edc723a2fda868bce55e6603', '2016-01-29', '2016-01-29 17:35:48', 'Logged In'),
(2536, 'admin', '66.49.230.207', 'a17691567023d69f985c9619c261cecc', '2016-02-01', '2016-02-01 08:42:15', 'Logged In'),
(2537, 'admin', '191.181.15.233', '93fb03dac1efb70b8c2c31bf0a42e5ce', '2016-02-01', '2016-02-01 08:56:33', 'Logged In'),
(2538, 'admin', '66.49.230.207', '47b83deda3e1b11c95f27ca756434c5e', '2016-02-01', '2016-02-01 14:04:27', 'Logged In'),
(2539, 'admin', '66.49.230.207', '47b83deda3e1b11c95f27ca756434c5e', '2016-02-01', '2016-02-01 14:06:21', 'Added Industry'),
(2540, 'admin', '66.49.230.207', '47b83deda3e1b11c95f27ca756434c5e', '2016-02-01', '2016-02-01 14:06:30', 'Added Category'),
(2541, 'admin', '66.49.230.207', '47b83deda3e1b11c95f27ca756434c5e', '2016-02-01', '2016-02-01 14:07:33', 'Added SubCategory'),
(2542, 'admin', '66.49.230.207', '47b83deda3e1b11c95f27ca756434c5e', '2016-02-01', '2016-02-01 14:08:21', 'Added Brand'),
(2543, 'admin', '191.181.15.233', '2f977eebe2e40a42c6117ecbe4467e59', '2016-02-01', '2016-02-01 14:22:53', 'Logged In'),
(2544, 'admin', '66.49.230.207', '526cbe1c7e8dad5e45d1778c72c61c03', '2016-02-01', '2016-02-01 14:28:41', 'Logged In'),
(2545, 'admin', '66.49.230.207', '315228b03e52e7944243a4384a96c9b0', '2016-02-01', '2016-02-01 14:49:19', 'Logged In'),
(2546, 'admin', '66.49.230.207', 'b5af62da9deffe4f4c6f7fae4bc80d7d', '2016-02-01', '2016-02-01 14:50:15', 'Logged In'),
(2547, 'admin', '66.49.230.207', '4f56ed7d6ea32345103cde8681081b66', '2016-02-04', '2016-02-04 11:33:12', 'Logged In'),
(2548, 'admin', '66.49.230.207', '4f56ed7d6ea32345103cde8681081b66', '2016-02-04', '2016-02-04 11:33:19', 'Logged Out'),
(2549, 'admin', '66.49.230.207', 'c900c8fe7d04e3083e44c00bd0b744b9', '2016-02-04', '2016-02-04 12:00:15', 'Logged In'),
(2550, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:04:56', 'Logged In'),
(2551, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:05:12', 'Added Industry'),
(2552, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:05:32', 'Added Category'),
(2553, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:05:43', 'Added SubCategory'),
(2554, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:05:56', 'Updated Category'),
(2555, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:08:39', 'Deleted Brand'),
(2556, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:08:41', 'Deleted Brand'),
(2557, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:08:45', 'Deleted Brand'),
(2558, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:08:54', 'Added Brand'),
(2559, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:04', 'Added Brand'),
(2560, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:11', 'Added Brand'),
(2561, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:18', 'Added Brand'),
(2562, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:27', 'Added Brand'),
(2563, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:33', 'Added Brand'),
(2564, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:42', 'Added Brand'),
(2565, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:49', 'Added Brand'),
(2566, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:09:54', 'Added Brand'),
(2567, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:10:04', 'Added Brand'),
(2568, 'admin', '66.49.230.207', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', '2016-02-04 12:12:15', 'Added Product'),
(2569, 'admin', '69.171.149.223', 'dc7d908e417e3a80cd05dc1e569009b0', '2016-02-04', '2016-02-04 12:16:48', 'Logged In'),
(2570, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 14:48:02', 'Logged In'),
(2571, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 15:09:00', 'Added Product'),
(2572, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 14:11:21', 'Deleted Product'),
(2573, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 15:16:31', 'Added Product'),
(2574, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 14:31:04', 'Deleted Product'),
(2575, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 15:32:19', 'Added Product'),
(2576, 'admin', '68.55.116.144', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', '2016-02-04 15:35:25', 'Edited Product'),
(2577, 'admin', '69.171.149.223', '5a25789107085f026b3f1b072f94a1ff', '2016-02-04', '2016-02-04 15:36:31', 'Logged In'),
(2578, 'admin', '66.49.230.207', '047f36cf979a3ef97444404b25650bc7', '2016-02-04', '2016-02-04 15:42:20', 'Logged In'),
(2579, 'admin', '66.49.230.207', '047f36cf979a3ef97444404b25650bc7', '2016-02-04', '2016-02-04 14:50:08', 'Deleted Product'),
(2580, 'admin', '191.181.15.233', '661412139cfdf865a85d918828ff39ee', '2016-02-04', '2016-02-04 17:20:49', 'Logged In'),
(2581, 'admin', '66.49.230.207', 'a5c1577666cea94dcd69c79df590d157', '2016-02-05', '2016-02-05 08:35:59', 'Logged In'),
(2582, 'admin', '68.55.116.144', 'f8d6b2cc7c31c8c31d6245f37251a18e', '2016-02-05', '2016-02-05 10:56:07', 'Logged In'),
(2583, 'admin', '191.181.15.233', '4eea5e5b0143170d5a69804fe931811f', '2016-02-05', '2016-02-05 11:17:11', 'Logged In'),
(2584, 'admin', '66.49.230.207', '80499731e8b914a7940b8c0d1771c488', '2016-02-05', '2016-02-05 12:34:25', 'Logged In'),
(2585, 'admin', '66.49.230.207', '47dadd9f86859e9a3d347bfa74721b33', '2016-02-05', '2016-02-05 13:28:29', 'Logged In'),
(2586, 'admin', '68.55.116.144', 'af958cf4a8e7f67a97308d7f0da2953b', '2016-02-05', '2016-02-05 13:30:55', 'Logged In'),
(2587, 'admin', '66.49.230.207', '47dadd9f86859e9a3d347bfa74721b33', '2016-02-05', '2016-02-05 13:35:31', 'Added Category'),
(2588, 'admin', '66.49.230.207', '47dadd9f86859e9a3d347bfa74721b33', '2016-02-05', '2016-02-05 13:35:44', 'Added SubCategory'),
(2589, 'admin', '66.49.230.207', '47dadd9f86859e9a3d347bfa74721b33', '2016-02-05', '2016-02-05 13:47:49', 'Added SubCategory'),
(2590, 'admin', '66.49.230.207', '47dadd9f86859e9a3d347bfa74721b33', '2016-02-05', '2016-02-05 13:49:43', 'Added SubCategory'),
(2591, 'admin', '191.181.15.233', '875f689702b454b08e832ec8e4ad57e0', '2016-02-06', '2016-02-06 08:14:57', 'Logged In'),
(2592, 'admin', '66.49.241.175', '8d4b18ba612ff6ccc430da0401dfb2ea', '2016-02-08', '2016-02-08 08:34:58', 'Logged In'),
(2593, 'admin', '68.55.116.144', '1dfd63783ef8a29d968c9f06ef4202f1', '2016-02-08', '2016-02-08 10:07:25', 'Logged In'),
(2594, 'admin', '66.49.241.175', '12b17808b7ef4c02a2847c482d8e079d', '2016-02-10', '2016-02-10 08:45:32', 'Logged In'),
(2595, 'admin', '66.49.241.175', '950f8163518dc318902309c12b0e8bed', '2016-02-11', '2016-02-11 08:42:55', 'Logged In'),
(2596, 'admin', '66.49.241.175', 'b9ffcfed0a8edb5fdc8ccb315a35b54f', '2016-02-11', '2016-02-11 12:02:53', 'Logged In'),
(2597, 'admin', '46.71.71.18', '6b54d861497e5f1ba9273ca95821eb5a', '2016-02-11', '2016-02-11 12:03:11', 'Logged In'),
(2598, 'admin', '66.49.241.175', 'bf7f0b884fb099ad217302ac4e2a2538', '2016-02-11', '2016-02-11 13:35:16', 'Logged In'),
(2599, 'admin', '66.49.241.175', 'bf7f0b884fb099ad217302ac4e2a2538', '2016-02-11', '2016-02-11 13:38:21', 'Added Industry'),
(2600, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 13:59:24', 'Logged In'),
(2601, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 13:59:35', 'Added Customer'),
(2602, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 14:00:49', 'Imported Categories'),
(2603, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 14:01:15', 'Imported SubCategories'),
(2604, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 14:01:54', 'Imported Brands'),
(2605, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 14:02:02', 'Imported Products'),
(2606, 'admin', '66.49.241.175', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', '2016-02-11 14:03:20', 'Imported Products'),
(2607, 'admin', '66.49.241.175', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', '2016-02-11 15:34:00', 'Logged In'),
(2608, 'admin', '66.49.241.175', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', '2016-02-11 15:37:04', 'Added Industry'),
(2609, 'admin', '66.49.241.175', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', '2016-02-11 15:37:19', 'Imported Categories'),
(2610, 'admin', '66.49.241.175', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', '2016-02-11 15:37:30', 'Imported Brands'),
(2611, 'admin', '66.49.241.175', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', '2016-02-11 15:37:47', 'Imported SubCategories'),
(2612, 'admin', '66.49.241.175', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', '2016-02-11 15:38:16', 'Imported Categories'),
(2613, 'admin', '66.49.241.175', 'cf4d1c392eb4c896f9510ba6a27876a9', '2016-02-11', '2016-02-11 15:41:24', 'Logged In'),
(2614, 'admin', '66.49.241.175', 'cf4d1c392eb4c896f9510ba6a27876a9', '2016-02-11', '2016-02-11 15:41:36', 'Imported Categories'),
(2615, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 15:45:01', 'Logged In'),
(2616, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 15:45:32', 'Imported Categories'),
(2617, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 15:46:46', 'Imported Categories'),
(2618, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 15:56:44', 'Imported Categories'),
(2619, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 15:57:20', 'Imported Categories'),
(2620, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 15:58:24', 'Imported Categories'),
(2621, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 16:06:23', 'Imported Categories'),
(2622, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 16:06:42', 'Imported Categories'),
(2623, 'admin', '66.49.241.175', '133a2cc157ed5eb8cda4699919f2417e', '2016-02-11', '2016-02-11 16:07:32', 'Logged In'),
(2624, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 16:08:18', 'Imported Categories'),
(2625, 'admin', '46.71.51.249', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', '2016-02-11 16:08:49', 'Imported Categories'),
(2626, 'admin', '123.236.192.153', '7849eca257ce1e7e11c5965ccfe46530', '2016-02-15', '2016-02-15 10:27:28', 'Logged In'),
(2627, 'admin', '99.224.27.68', '7fb0b1a17063b9aaa3c93956d15fc31c', '2016-02-15', '2016-02-15 10:28:24', 'Logged In'),
(2628, 'admin', '122.175.202.130', '303837f69be7c8f9eb01a59f6424fcbf', '2016-02-15', '2016-02-15 10:36:52', 'Logged In'),
(2629, 'admin', '122.175.202.130', '303837f69be7c8f9eb01a59f6424fcbf', '2016-02-15', '2016-02-15 11:11:08', 'Logged Out');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userlogin`
--

CREATE TABLE IF NOT EXISTS `mst_userlogin` (
  `roletype` varchar(10) NOT NULL,
  `roleid` int(11) NOT NULL,
  `userid` varchar(25) NOT NULL DEFAULT '',
  `password` varchar(25) NOT NULL,
  `authorizedpass` varchar(25) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  `ftlogin` varchar(1) NOT NULL DEFAULT 'N',
  `timezoneid` varchar(100) NOT NULL,
  `secquiz1` int(11) DEFAULT NULL,
  `secquiz2` int(11) DEFAULT NULL,
  `secquiz3` int(11) DEFAULT NULL,
  `secans1` varchar(100) DEFAULT NULL,
  `secans2` varchar(100) DEFAULT NULL,
  `secans3` varchar(100) DEFAULT NULL,
  `visualnoti` varchar(1) DEFAULT '1',
  `audionoti` varchar(1) DEFAULT '1',
  `industry` varchar(100) DEFAULT NULL,
  `category` varchar(500) DEFAULT NULL,
  `subcategory` varchar(500) DEFAULT NULL,
  `brand` varchar(500) DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_userlogin`
--

INSERT INTO `mst_userlogin` (`roletype`, `roleid`, `userid`, `password`, `authorizedpass`, `isactive`, `ftlogin`, `timezoneid`, `secquiz1`, `secquiz2`, `secquiz3`, `secans1`, `secans2`, `secans3`, `visualnoti`, `audionoti`, `industry`, `category`, `subcategory`, `brand`, `creationdate`) VALUES
('AD', 5, 'admin', 'password123', 'password123', '1', 'Y', 'US/Eastern', 6, 8, 3, 'father', 'mother', 'animal', '1', '1', '', '', '', '', '2015-07-21 02:20:16'),
('TM', 3, 'GPUJ-2680', 'password1', 'password1', '1', 'N', 'Australia/Sydney', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '', '', '', '', '2015-12-17 16:22:38'),
('AM', 1, 'IXBB-4438', 'password123', 'password1', '1', 'Y', 'US/Eastern', 6, 8, 9, 'mother', 'father', 'food', '1', '1', '1', '', '', '', '2015-12-17 16:12:18'),
('AM', 1, 'JUQI-3911', 'password1', 'password1', '1', 'Y', 'US/Central', 6, 8, 9, 'mother', 'father', 'food', '1', '1', '3,1', '', '', '', '2015-12-17 16:20:25'),
('AM', 1, 'MSOX-3592', 'password1', 'password1', '1', 'N', 'US/Pacific', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,2', '', '', '', '2015-12-17 16:21:19'),
('AM', 18, 'OORQ-0229', 'password1', 'password1', '1', 'Y', 'US/Eastern', 7, 14, 3, 'ottawa', 'strawars', 'rasputin', '1', '1', '3', '', '', '', '2016-01-18 11:53:31'),
('QF', 2, 'YSZJ-4486', 'password1', 'password1', '1', 'N', 'US/Eastern', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,1,2', '', '', '', '2016-01-12 05:36:23'),
('QF', 2, 'YWFZ-6999', 'password1', 'password1', '1', 'N', 'US/Hawaii', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,1,2', '', '', '', '2015-12-17 16:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userlogon`
--

CREATE TABLE IF NOT EXISTS `mst_userlogon` (
  `logonid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) NOT NULL,
  `ipaddress` varchar(25) NOT NULL DEFAULT '127.0.0.0',
  `starttime` varchar(25) NOT NULL,
  `finishtime` varchar(25) NOT NULL,
  `sessionid` varchar(100) NOT NULL,
  `logondate` date NOT NULL,
  `roletype` varchar(10) NOT NULL,
  PRIMARY KEY (`logonid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=928 ;

--
-- Dumping data for table `mst_userlogon`
--

INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`) VALUES
(1, 'BACV-7542', '112.196.136.78', '2015-10-30 07:30:49am', '', 'cp3219ddm229djscci53bmv921', '2015-10-30', 'AM'),
(2, 'OGJJ-0337', '104.236.195.130', '2015-10-30 07:37:12am', '', 'vgk7slmf0sngivc8u70rlriji0', '2015-10-30', 'TM'),
(3, 'admin', '112.196.136.78', '2015-10-30 08:11:57am', '', 'm8a5nu3q4eljnjhne3hufr9151', '2015-10-30', 'AD'),
(4, 'TEED-0538', '112.196.136.78', '2015-10-30 08:12:19am', '2015-10-30 09:29:33am', '1v7ub1kacpai10e45mdui5mkr0', '2015-10-30', 'QF'),
(5, 'admin', '112.196.136.78', '2015-10-30 08:29:38am', '', 's2nbqdpn09ev820bn9pqbfbhe0', '2015-10-30', 'AD'),
(6, 'admin', '112.196.136.78', '2015-10-31 00:41:56am', '', 'um8eh82f906d37kkhn7f7612a0', '2015-10-31', 'AD'),
(7, 'admin', '112.196.136.78', '2015-10-31 00:49:00am', '', '0k48hr58bhjp3vj01ojhh5hjp3', '2015-10-31', 'AD'),
(8, 'admin', '112.196.136.78', '2015-10-31 00:57:27am', '', 'gbjgli2hnv19p6oafoiie1q081', '2015-10-31', 'AD'),
(9, 'admin', '112.196.136.78', '2015-10-31 01:00:52am', '', 'ukuj85l12id22kgefffcsmk7b5', '2015-10-31', 'AD'),
(10, 'admin', '112.196.136.78', '2015-10-31 01:03:32am', '2015-10-31 02:03:34am', '8garjivp1c0cuu5llrhvcpup71', '2015-10-31', 'AD'),
(11, 'BACV-7542', '112.196.136.78', '2015-10-31 01:03:43am', '', 'qb1rui0l2frlj3qfsmokeearg1', '2015-10-31', 'AM'),
(12, 'BACV-7542', '112.196.136.78', '2015-10-31 01:13:18am', '', '38ftnonjn7cq6nu3e48766ftq5', '2015-10-31', 'AM'),
(13, 'CMHY-0522', '112.196.136.78', '2015-10-31 01:16:48am', '', '03ib750vq1i4tattu47dqivft5', '2015-10-31', 'AM'),
(14, 'admin', '112.196.136.78', '2015-10-31 02:07:57am', '', '8tvbb0012omkolqsjj3m975rp2', '2015-10-31', 'AD'),
(15, 'admin', '112.196.136.78', '2015-10-31 03:06:25am', '', 'jduaqni1g3pccj15p0bgapmi82', '2015-10-31', 'AD'),
(16, 'BACV-7542', '112.196.136.78', '2015-10-31 03:08:05am', '2015-10-31 05:27:44am', '8spjb0nrterpkvvndrui86pfm6', '2015-10-31', 'AM'),
(17, 'admin', '112.196.136.78', '2015-10-31 03:13:36am', '2015-10-31 06:05:34am', 'bifkn02i2ie0m5irg20cefqr12', '2015-10-31', 'AD'),
(18, 'admin', '112.196.136.78', '2015-10-31 04:15:52am', '', 's8a2ktiosn5i4m1ul906b9ikl3', '2015-10-31', 'AD'),
(19, 'CMHY-0522', '112.196.136.78', '2015-10-31 04:28:11am', '', '63q6ihrm6a81k4kn0qp2kvlc56', '2015-10-31', 'AM'),
(20, 'admin', '112.196.136.78', '2015-10-31 05:05:54am', '', 'qj27umuh0i1nir606nk6d538u3', '2015-10-31', 'AD'),
(21, 'admin', '112.196.136.78', '2015-10-31 05:06:05am', '2015-10-31 06:14:52am', 'u426d6mumphc4p2q4ho6fhsia1', '2015-10-31', 'AD'),
(22, 'CMHY-0522', '112.196.136.78', '2015-10-31 05:14:57am', '2015-10-31 07:18:27am', '52itoieoddtmeaphlbbal42ih1', '2015-10-31', 'AM'),
(23, 'BACV-7542', '112.196.136.78', '2015-10-31 05:15:16am', '', 'h8uutmjadhb6l071n43dsmtnh5', '2015-10-31', 'AM'),
(24, 'admin', '112.196.136.78', '2015-10-31 05:45:42am', '2015-10-31 07:56:20am', 'fldsrv00ik1queag130uqncr22', '2015-10-31', 'AD'),
(25, 'OGJJ-0337', '45.55.246.162', '2015-10-31 05:49:09am', '2015-10-31 09:11:52am', 'r5l13i6q6fnk6vsinq42hptq71', '2015-10-31', 'TM'),
(26, 'admin', '112.196.136.78', '2015-10-31 06:05:47am', '', 'kgj5b0qkqidk00pnmt4jorehe5', '2015-10-31', 'AD'),
(27, 'BACV-7542', '112.196.136.78', '2015-10-31 06:17:58am', '', '24vuq9i94ck8dq5mdnhis5er35', '2015-10-31', 'AM'),
(28, 'admin', '112.196.136.78', '2015-10-31 06:18:32am', '2015-10-31 08:48:25am', 'qgbt588ur9cat2o1jpbnv9ocg3', '2015-10-31', 'AD'),
(29, 'BACV-7542', '112.196.136.78', '2015-10-31 06:56:35am', '', '1ovp1quq3pnqvpnu1l7g96tr66', '2015-10-31', 'AM'),
(30, 'TEED-0538', '112.196.136.78', '2015-10-31 07:48:29am', '2015-10-31 08:55:30am', 'g56io0h4jqj68ch8lk7aobgpb7', '2015-10-31', 'QF'),
(31, 'admin', '112.196.136.78', '2015-10-31 07:55:41am', '2015-10-31 09:31:51am', '48mmo7dersiso9haejot83hqu4', '2015-10-31', 'AD'),
(32, 'CMHY-0522', '112.196.136.78', '2015-10-31 07:56:28am', '', 'mg1vaopfi0j8l2gghmkoj5n1c4', '2015-10-31', 'AM'),
(33, 'TEED-0538', '112.196.136.78', '2015-10-31 08:02:10am', '', 's7oqdrku66brf7lt1933tsd7h1', '2015-10-31', 'QF'),
(34, 'admin', '112.196.136.78', '2015-10-31 09:05:56am', '2015-10-31 09:08:15am', 'jis2b8uimt9ed4k86tracanmr3', '2015-10-31', 'AD'),
(35, 'CMHY-0522', '112.196.136.78', '2015-10-31 08:08:35am', '2015-10-31 09:09:18am', 'lm0kd7kr3aoltlmfgp9r7eo7p3', '2015-10-31', 'AM'),
(36, 'TEED-0538', '112.196.136.78', '2015-10-31 08:09:26am', '', '35it5kplme61dsfl6f3872u4m5', '2015-10-31', 'QF'),
(37, 'TEED-0538', '45.55.199.109', '2015-10-31 08:12:09am', '', 'pkdcjo7u3tl22h4garsjo6meq7', '2015-10-31', 'QF'),
(38, 'admin', '112.196.136.78', '2015-10-31 08:22:22am', '2015-10-31 09:26:49am', '85uhisfmq63ogc774b3529tsl6', '2015-10-31', 'AD'),
(39, 'CMHY-0522', '112.196.136.78', '2015-10-31 08:26:55am', '', 'j6l2ai6f6p9i2p4214et6n5hv7', '2015-10-31', 'AM'),
(40, 'TEED-0538', '112.196.136.78', '2015-10-31 08:31:55am', '2015-10-31 09:39:47am', 'qen8an09r6ishkp2rstvu6que6', '2015-10-31', 'QF'),
(41, 'CMHY-0522', '112.196.136.78', '2015-10-31 08:34:37am', '', '3jni7bc0emsbsunan8him242v3', '2015-10-31', 'AM'),
(42, 'admin', '112.196.136.78', '2015-10-31 08:39:52am', '2015-10-31 09:40:07am', '8iddn3j6pqs5lubb6drpso2t44', '2015-10-31', 'AD'),
(43, 'BACV-7542', '112.196.136.78', '2015-10-31 08:40:26am', '2015-10-31 09:41:02am', 'tr2sdn07jm0d1j2sid7659n886', '2015-10-31', 'AM'),
(44, 'admin', '112.196.136.78', '2015-10-31 08:41:06am', '2015-10-31 09:41:22am', 'aouvatlr6qknf283ida80h1ig0', '2015-10-31', 'AD'),
(45, 'OGJJ-0337', '112.196.136.78', '2015-10-31 08:41:25am', '', 'imipe8iejnj0sjup4i4n854ku5', '2015-10-31', 'TM'),
(46, 'admin', '112.196.136.78', '2015-11-02 00:53:17am', '', 'hnetiu95qbnj4m6jifr5jasiu2', '2015-11-02', 'AD'),
(47, 'admin', '112.196.136.78', '2015-11-02 00:57:44am', '', 'uoohsa124olms1hpanuq2afdu7', '2015-11-02', 'AD'),
(48, 'admin', '112.196.136.78', '2015-11-02 01:20:33am', '2015-11-02 01:38:44am', 'qni06ttq3jnmvfh9cvcni1oa75', '2015-11-02', 'AD'),
(49, 'admin', '112.196.136.78', '2015-11-02 01:32:38am', '2015-11-02 01:36:10am', 'b3o327lqs6bo1ft3c2n1j0gsf0', '2015-11-02', 'AD'),
(50, 'TEED-0538', '112.196.136.78', '2015-11-02 01:36:49am', '2015-11-02 06:03:36am', 'rrkkc7ne3fnhngq4ib2vmogia6', '2015-11-02', 'QF'),
(51, 'TEED-0538', '112.196.136.78', '2015-11-02 02:11:41am', '', 'ko3s0tf1sm4iahpu6cljvugiq1', '2015-11-02', 'QF'),
(52, 'OGJJ-0337', '112.196.136.78', '2015-11-02 02:12:12am', '', '6c5b1n8hvsqcd497qk459btt51', '2015-11-02', 'TM'),
(53, 'CMHY-0522', '112.196.136.78', '2015-11-02 02:13:09am', '', 'tko315no1rm24a7k7amhiveop7', '2015-11-02', 'AM'),
(54, 'BACV-7542', '112.196.136.78', '2015-11-02 02:16:09am', '', 'gq7nu3kbjg61gtihlm9rbc48k7', '2015-11-02', 'AM'),
(55, 'CMHY-0522', '112.196.136.78', '2015-11-02 02:18:37am', '', 'tqpnl0a3om4egeiitlhorkmte7', '2015-11-02', 'AM'),
(56, 'CMHY-0522', '112.196.136.78', '2015-11-02 02:38:25am', '2015-11-02 07:40:14am', 'm5rr0s6cjqt7ohafh1kii4tmd7', '2015-11-02', 'AM'),
(57, 'BACV-7542', '112.196.136.78', '2015-11-02 04:22:13am', '2015-11-02 04:58:05am', '3rak50rtieut1fd8tvmd087s62', '2015-11-02', 'AM'),
(58, 'admin', '104.236.225.168', '2015-11-02 04:27:28am', '2015-11-02 05:15:41am', 'ppvfr835jbovuuht18qnici9s6', '2015-11-02', 'AD'),
(59, 'admin', '112.196.136.78', '2015-11-02 04:58:07am', '2015-11-02 05:00:04am', 'c0cm3023leik3566va4lr4n5j0', '2015-11-02', 'AD'),
(60, 'BACV-7542', '112.196.136.78', '2015-11-02 05:00:20am', '', 'a17lguk5vap0rqhgpgs4jh67o5', '2015-11-02', 'AM'),
(61, 'admin', '112.196.136.78', '2015-11-02 05:15:29am', '2015-11-02 05:29:24am', 'k70qibfldqih1r2hrbq1fm3u14', '2015-11-02', 'AD'),
(62, 'TEED-0538', '104.236.225.168', '2015-11-02 05:15:54am', '', 'nupvn8bgvh2mh0b740bpq8klr3', '2015-11-02', 'QF'),
(63, 'TEED-0538', '112.196.136.78', '2015-11-02 05:29:39am', '', 'pkuvsvtotlgp09fscjd24d76r7', '2015-11-02', 'QF'),
(64, 'admin', '104.236.195.130', '2015-11-02 05:31:53am', '', 'agngfgu0femnles77onn648p22', '2015-11-02', 'AD'),
(65, 'admin', '112.196.136.78', '2015-11-02 06:03:59am', '', '3jofjl18mgr3qtbvq7a2ss3pk4', '2015-11-02', 'AD'),
(66, 'admin', '104.131.14.167', '2015-11-02 07:35:20am', '', 'ilgucr4hvn7061qtr4etqm6a03', '2015-11-02', 'AD'),
(67, 'CMHY-0522', '112.196.136.78', '2015-11-02 07:40:33am', '', 'kn3ur5m127s0c87hpnfopiaj76', '2015-11-02', 'AM'),
(68, 'admin', '112.196.136.78', '2015-11-02 08:23:04am', '', 'pi5nsoi4jtnq6kvkqb5s1hjj11', '2015-11-02', 'AD'),
(69, 'OGJJ-0337', '112.196.136.78', '2015-11-02 08:28:41am', '', 'ajoegmeqod390ii3dnb895br45', '2015-11-02', 'TM'),
(70, 'admin', '112.196.136.78', '2015-11-02 08:55:43am', '', 'raqe3v5qpirf25jd3n186vajo3', '2015-11-02', 'AD'),
(71, 'admin', '66.49.249.43', '2015-11-02 13:45:52pm', '', 'lkrop1fkveddu8e0o39urn0qj5', '2015-11-02', 'AD'),
(72, 'admin', '66.49.249.43', '2015-11-02 13:46:46pm', '', 'udlu5ptd9il44r43f86j60l0q4', '2015-11-02', 'AD'),
(73, 'FXIO-8905', '66.49.249.43', '2015-11-02 14:00:58pm', '2015-11-02 16:12:22pm', 'keei9q00947i31jgqql06ou787', '2015-11-02', 'AM'),
(74, 'RAUU-1599', '66.49.249.43', '2015-11-02 14:01:05pm', '', 'tkhqa8cmjfmqt91haqgg173fh2', '2015-11-02', 'AM'),
(75, 'admin', '66.49.249.43', '2015-11-02 14:42:26pm', '2015-11-02 14:46:16pm', '4rfjuovbj2rsn9omu8pea0bbc3', '2015-11-02', 'AD'),
(76, 'admin', '66.49.249.43', '2015-11-02 14:46:25pm', '', '4810rls1aln5vvlo05dn45c241', '2015-11-02', 'AD'),
(77, 'admin', '66.49.249.43', '2015-11-02 16:07:45pm', '', '11vl9dhdbhc6dnoenqj1hijqt7', '2015-11-02', 'AD'),
(78, 'FTTP-8399', '66.49.249.43', '2015-11-02 16:09:43pm', '2015-11-02 16:16:49pm', 'c3brrce2c6rj8efrnghnjsktl7', '2015-11-02', 'TM'),
(79, 'ZJBX-0533', '66.49.249.43', '2015-11-02 16:09:50pm', '', '9bocub69dc73j8jrkp754uiqa4', '2015-11-02', 'QF'),
(80, 'FXIO-8905', '66.49.249.43', '2015-11-02 16:12:25pm', '2015-11-02 16:24:05pm', 'knr6qhg82jh4ggjjv1gfrsldr3', '2015-11-02', 'AM'),
(81, 'ZJBX-0533', '66.49.249.43', '2015-11-02 16:17:24pm', '', 'l5lnu3g46imu738aasmkstm5g3', '2015-11-02', 'QF'),
(82, 'FTTP-8399', '66.49.249.43', '2015-11-02 16:24:17pm', '', '10175q8cdfrs74u78mop43e0i7', '2015-11-02', 'TM'),
(83, 'admin', '112.196.136.78', '2015-11-03 00:51:33am', '', '6cfeke4tcspmfl1qvbefq5bjj2', '2015-11-03', 'AD'),
(84, 'admin', '112.196.136.78', '2015-11-03 01:15:47am', '', 'dktooevp415r3hrqsrdpvh9ho6', '2015-11-03', 'AD'),
(85, 'admin', '112.196.136.78', '2015-11-03 02:33:28am', '2015-11-03 07:01:24am', '9upnemmfr4vcdu2b4mv3u4c192', '2015-11-03', 'AD'),
(86, 'FXIO-8905', '112.196.136.78', '2015-11-03 02:46:42am', '2015-11-03 03:19:48am', '80pg61653lhnh2j7dhi0u5ffh4', '2015-11-03', 'AM'),
(87, 'RAUU-1599', '112.196.136.78', '2015-11-03 02:47:04am', '2015-11-03 03:02:11am', '76f9b9sjdclmv673il14fpsgg5', '2015-11-03', 'AM'),
(88, 'VUQQ-3341', '112.196.136.78', '2015-11-03 03:02:14am', '', '3o7n1j3v04irnfcn0tp4mj05m3', '2015-11-03', 'AM'),
(89, 'FXIO-8905', '112.196.136.78', '2015-11-03 03:19:56am', '', 'qkqtg4jsklqv6kff4831khgjv6', '2015-11-03', 'AM'),
(90, 'FXIO-8905', '112.196.136.78', '2015-11-03 03:19:57am', '2015-11-03 09:39:58am', 'flnf31hkmrh2v75u13g7t1s360', '2015-11-03', 'AM'),
(91, 'VUQQ-3341', '104.131.161.230', '2015-11-03 04:20:58am', '2015-11-03 04:50:49am', 'iqnicn738b6iih8nv8dkpmced1', '2015-11-03', 'AM'),
(92, 'FXIO-8905', '104.131.161.230', '2015-11-03 04:51:13am', '2015-11-03 08:09:32am', 'ibfg3c4rnlc02hg7lvrttskng0', '2015-11-03', 'AM'),
(93, 'FXIO-8905', '112.196.136.78', '2015-11-03 06:22:40am', '2015-11-03 09:08:46am', '67aso1s6p3a93mlqssahrmqjt3', '2015-11-03', 'AM'),
(94, 'ZJBX-0533', '112.196.136.78', '2015-11-03 06:24:43am', '', '0s8me9hu9f632235agnbbggev4', '2015-11-03', 'QF'),
(95, 'ZJBX-0533', '112.196.136.78', '2015-11-03 06:40:30am', '', '68ebl3a3503a47a0kkm3cakgk7', '2015-11-03', 'QF'),
(96, 'ZJBX-0533', '112.196.136.78', '2015-11-03 07:01:34am', '2015-11-03 09:14:43am', 'v1h9t4306ejq89qeue7go2fac6', '2015-11-03', 'QF'),
(97, 'admin', '112.196.136.78', '2015-11-03 07:11:04am', '', 'n24sfhuje3lir9fcqj2d4u94t5', '2015-11-03', 'AD'),
(98, 'RAUU-1599', '45.55.246.162', '2015-11-03 08:13:07am', '', 't7o7d0qal164cvn079jvj3cpo6', '2015-11-03', 'AM'),
(99, 'admin', '112.196.136.78', '2015-11-03 08:59:39am', '', 'vebdk5cnu7pul2ev0ocai11uq0', '2015-11-03', 'AD'),
(100, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:01:33am', '', 'u5qll191l11bbd7hs89840gdd5', '2015-11-03', 'AM'),
(101, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:01:47am', '2015-11-03 09:03:30am', 'c22lkub1mjon0mdavsu1i7d1v4', '2015-11-03', 'AM'),
(102, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:03:37am', '2015-11-03 09:41:27am', '57r6kb2puiedv81nk2akrc3kd7', '2015-11-03', 'AM'),
(103, 'admin', '112.196.136.78', '2015-11-03 09:09:05am', '2015-11-03 10:29:52am', '5nat2i5ka1q9n8d0ucnrp4r9h1', '2015-11-03', 'AD'),
(104, 'FXIO-8905', '112.196.136.78', '2015-11-03 09:15:09am', '', 'sug13mpfc2mnup9hud0ftvng81', '2015-11-03', 'AM'),
(105, 'ZJBX-0533', '112.196.136.78', '2015-11-03 09:40:03am', '2015-11-03 10:20:41am', 'mf85ir8fs1b7is6bobttm026h4', '2015-11-03', 'QF'),
(106, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:41:29am', '', 'pjhhb10j2ohjkuai7sid8iuf91', '2015-11-03', 'AM'),
(107, 'admin', '112.196.136.78', '2015-11-03 10:18:51am', '2015-11-03 10:19:52am', 'jtt04n2h8bup4fdhg41ovrggn3', '2015-11-03', 'AD'),
(108, 'RAUU-1599', '112.196.136.78', '2015-11-03 10:19:55am', '', '0gtfeh4809j84i75hkf5f839n1', '2015-11-03', 'AM'),
(109, 'admin', '112.196.136.78', '2015-11-03 10:20:49am', '', 'gt6cb56bf3aghvg8kjongb33b5', '2015-11-03', 'AD'),
(110, 'ZJBX-0533', '112.196.136.78', '2015-11-03 10:21:46am', '', 'n7ec1oqrjnq3lbbcimju2ega35', '2015-11-03', 'QF'),
(111, 'admin', '66.49.237.16', '2015-11-03 12:47:09pm', '', 'emnmg246b9elhf0gv7i21na121', '2015-11-03', 'AD'),
(112, 'admin', '66.49.237.16', '2015-11-03 13:55:34pm', '2015-11-03 14:08:19pm', 'kja1rdb7s298jrdccsdg2f2b00', '2015-11-03', 'AD'),
(113, 'FTTP-8399', '66.49.237.16', '2015-11-03 14:00:26pm', '2015-11-03 14:49:06pm', 'e270d901fpp025ikd4spmt4d92', '2015-11-03', 'TM'),
(114, 'admin', '66.49.237.16', '2015-11-03 14:08:22pm', '2015-11-03 14:08:25pm', 'fifen8sv5jiatdu65qr0o519n2', '2015-11-03', 'AD'),
(115, 'admin', '66.49.237.16', '2015-11-03 14:08:28pm', '2015-11-03 14:49:27pm', '09e6q33di46m6o63n0jl2e34q2', '2015-11-03', 'AD'),
(116, 'FXIO-8905', '66.49.237.16', '2015-11-03 14:49:10pm', '', 'h9jedo6tce6itmjvin5uraro65', '2015-11-03', 'AM'),
(117, 'ZJBX-0533', '66.49.237.16', '2015-11-03 14:49:35pm', '2015-11-03 14:54:14pm', 'e5e1fejq45iconavt6me8m2195', '2015-11-03', 'QF'),
(118, 'admin', '66.49.237.16', '2015-11-03 14:54:18pm', '2015-11-03 15:01:40pm', '5cnhft0js6gtqs7p83l3hvg0d4', '2015-11-03', 'AD'),
(119, 'admin', '66.49.237.16', '2015-11-03 15:07:01pm', '', 'acmf69hpccme5eor51pk7lfrs7', '2015-11-03', 'AD'),
(120, 'admin', '112.196.136.78', '2015-11-04 00:54:36am', '2015-11-04 01:35:34am', 'qml8airktc91npmanp5eijhv06', '2015-11-04', 'AD'),
(121, 'admin', '112.196.136.78', '2015-11-04 01:19:42am', '2015-11-04 01:19:53am', 'fbufbi0it02q7c73ae8i8jia70', '2015-11-04', 'AD'),
(122, 'admin', '112.196.136.78', '2015-11-04 01:19:56am', '', 'rhldmsm6hl3vkbafu4fujqrr66', '2015-11-04', 'AD'),
(123, 'admin', '112.196.136.78', '2015-11-04 01:35:43am', '2015-11-04 06:50:12am', 'liujos4jk2ei32h0coah6ellb5', '2015-11-04', 'AD'),
(124, 'admin', '112.196.136.78', '2015-11-04 01:42:08am', '', 'p58q6u8oq0s0r86ioe2ir2upm0', '2015-11-04', 'AD'),
(125, 'admin', '112.196.136.78', '2015-11-04 02:08:35am', '', 'o1c7p0unraiojmaq4nudrpch90', '2015-11-04', 'AD'),
(126, 'admin', '112.196.136.78', '2015-11-04 02:18:55am', '', '06me71856tqtn1fu28rlhdbvv0', '2015-11-04', 'AD'),
(127, 'admin', '112.196.136.78', '2015-11-04 02:22:26am', '2015-11-04 07:31:54am', 's54l2is4c8n7ndlh098s820ig2', '2015-11-04', 'AD'),
(128, 'admin', '112.196.136.78', '2015-11-04 02:51:05am', '', '1rr4aj4uq1mteerv597ja66gc0', '2015-11-04', 'AD'),
(129, 'admin', '112.196.136.78', '2015-11-04 03:32:35am', '', 'tqs7co8tl5rian4l0cju9tstb5', '2015-11-04', 'AD'),
(130, 'admin', '112.196.136.78', '2015-11-04 03:34:57am', '', 't7evbi5uebu2huu1dpgnuctfp2', '2015-11-04', 'AD'),
(131, 'admin', '112.196.136.78', '2015-11-04 03:35:57am', '', '2kn4e0cs5csuaqnatisnfrg104', '2015-11-04', 'AD'),
(132, 'admin', '112.196.136.78', '2015-11-04 04:17:20am', '', 'm5s05o1o7sukf8bgvhmf4m09j1', '2015-11-04', 'AD'),
(133, 'admin', '112.196.136.78', '2015-11-04 04:21:25am', '', 'kos8eap3tn1n4l2o3v654s6893', '2015-11-04', 'AD'),
(134, 'admin', '112.196.136.78', '2015-11-04 04:37:43am', '', 'o5iu1de9fgtlqr03sl9fc924u5', '2015-11-04', 'AD'),
(135, 'admin', '112.196.136.78', '2015-11-04 06:12:26am', '', 'er0hql0qkvl9jo55q679the6s0', '2015-11-04', 'AD'),
(136, 'FXIO-8905', '112.196.136.78', '2015-11-04 06:51:28am', '', 'fjvfjj9g6lk2al7sr0gifgt8a1', '2015-11-04', 'AM'),
(137, 'RAUU-1599', '112.196.136.78', '2015-11-04 07:25:03am', '2015-11-04 08:04:36am', 'h8q0noqv1i94a67g8ggqs9bjh3', '2015-11-04', 'AM'),
(138, 'FXIO-8905', '112.196.136.78', '2015-11-04 07:25:37am', '', '231holcrtngulrs4foplfhv8s7', '2015-11-04', 'AM'),
(139, 'ZJBX-0533', '112.196.136.78', '2015-11-04 07:31:58am', '2015-11-04 08:00:23am', 'kq2d3h9oiq7umndiq13vcaq8m7', '2015-11-04', 'QF'),
(140, 'admin', '112.196.136.78', '2015-11-04 07:33:42am', '', 'qgabbitk6qhcmg4v7ljngkqab7', '2015-11-04', 'AD'),
(141, 'admin', '112.196.136.78', '2015-11-04 08:00:28am', '2015-11-04 10:04:13am', 're6ak283olluf604pp45nnprg5', '2015-11-04', 'AD'),
(142, 'VUQQ-3341', '112.196.136.78', '2015-11-04 08:04:40am', '2015-11-04 08:04:58am', '174uc8ql8jdle7tdqspitromb1', '2015-11-04', 'AM'),
(143, 'admin', '112.196.136.78', '2015-11-04 09:51:02am', '2015-11-04 09:56:09am', 'cm9mkp79lp83kp0im0dm4t5m17', '2015-11-04', 'AD'),
(144, 'admin', '112.196.136.78', '2015-11-04 09:56:11am', '', 'd876qvugdg7fhbvmma1gvdme75', '2015-11-04', 'AD'),
(145, 'FXIO-8905', '112.196.136.78', '2015-11-04 10:04:17am', '', 'gu743nqkfimr0mfqbeaejitpd3', '2015-11-04', 'AM'),
(146, 'admin', '106.76.136.45', '2015-11-04 12:01:37pm', '2015-11-04 15:05:40pm', 'e4h3icf7kuf0rhm7d9bhuav735', '2015-11-04', 'AD'),
(147, 'VUQQ-3341', '106.76.136.45', '2015-11-04 12:09:36pm', '2015-11-04 12:11:38pm', 'p8cgce6j80rjs16781ifqfuuq3', '2015-11-04', 'AM'),
(148, 'FXIO-8905', '106.76.136.45', '2015-11-04 12:11:59pm', '', 'mqeruhshuekimncqkj060st8s2', '2015-11-04', 'AM'),
(149, 'admin', '66.49.237.16', '2015-11-04 15:16:51pm', '', 'pll7htret1ko6dnmsgi7fh93b7', '2015-11-04', 'AD'),
(150, 'admin', '66.49.237.16', '2015-11-04 15:19:10pm', '', 'u8cfod3jjfvb8ult6g280j1723', '2015-11-04', 'AD'),
(151, 'admin', '66.49.157.92', '2015-11-04 16:49:07pm', '2015-11-04 16:49:35pm', 'f9gjlmah7d0ntard0ns3fr7qj5', '2015-11-04', 'AD'),
(152, 'admin', '112.196.136.78', '2015-11-05 00:42:56am', '2015-11-05 03:02:47am', 'd6u3005jrv31b7gqp817gle8t1', '2015-11-05', 'AD'),
(153, 'admin', '112.196.136.78', '2015-11-05 01:01:26am', '2015-11-05 01:21:14am', 'lr1e3f4bljrvemvm33sft4acq4', '2015-11-05', 'AD'),
(154, 'admin', '112.196.136.78', '2015-11-05 01:19:09am', '', '0i8q7nmgg2g781vdbrqh7cqck5', '2015-11-05', 'AD'),
(155, 'ZJBX-0533', '112.196.136.78', '2015-11-05 01:19:49am', '2015-11-05 01:21:21am', 'qqo9n0viarlrjrbi3k3560jai7', '2015-11-05', 'QF'),
(156, 'FXIO-8905', '112.196.136.78', '2015-11-05 01:21:46am', '', 'nlfr240acdda79eaupa4keqk56', '2015-11-05', 'AM'),
(157, 'RAUU-1599', '112.196.136.78', '2015-11-05 01:21:59am', '', 'jf5rsa7qgl2n31cenl5sasp6m1', '2015-11-05', 'AM'),
(158, 'admin', '112.196.136.78', '2015-11-05 01:39:55am', '', '5ijk6k21atgfl9iipaks37lhb1', '2015-11-05', 'AD'),
(159, 'FXIO-8905', '112.196.136.78', '2015-11-05 03:00:38am', '', '09j1ll08sftkrdv3odnlu1o4o1', '2015-11-05', 'AM'),
(160, 'FXIO-8905', '112.196.136.78', '2015-11-05 03:02:50am', '', 'rv600movrecqi3t332bq6i00a1', '2015-11-05', 'AM'),
(161, 'admin', '112.196.136.78', '2015-11-05 04:30:38am', '2015-11-05 08:15:05am', '0540t6cf0p2bkrnje7ppcbdqo0', '2015-11-05', 'AD'),
(162, 'admin', '112.196.136.78', '2015-11-05 04:43:38am', '', '6tt3oeeaafh0b44kertm9qv942', '2015-11-05', 'AD'),
(163, 'RAUU-1599', '112.196.136.78', '2015-11-05 06:02:06am', '', 'mt13bos023tppo46b1ebvc9il7', '2015-11-05', 'AM'),
(164, 'RAUU-1599', '112.196.136.78', '2015-11-05 06:39:10am', '2015-11-05 09:40:40am', 'km8k7elsfqbicrl4rbn2h6j8n3', '2015-11-05', 'AM'),
(165, 'FXIO-8905', '112.196.136.78', '2015-11-05 08:05:50am', '', 'ro33lp4ilnklt6i9lsm5vpdgc7', '2015-11-05', 'AM'),
(166, 'ZJBX-0533', '112.196.136.78', '2015-11-05 08:15:09am', '2015-11-05 09:17:15am', '42nni31nuat8micrt9rlk5ji51', '2015-11-05', 'QF'),
(167, 'ZJBX-0533', '112.196.136.78', '2015-11-05 09:17:19am', '2015-11-05 09:19:43am', '7g15o8voiac5tlptdb10vlv6d5', '2015-11-05', 'QF'),
(168, 'admin', '112.196.136.78', '2015-11-05 09:19:47am', '', 'eki6ao74d8slvhhragjqrd3l77', '2015-11-05', 'AD'),
(169, 'FXIO-8905', '112.196.136.78', '2015-11-05 09:24:54am', '', '4vrg35ak0gsifiri7sbgdq53n3', '2015-11-05', 'AM'),
(170, 'RAUU-1599', '112.196.136.78', '2015-11-05 09:40:43am', '', 'fmfmkog1p53h9b4vmmp9gog926', '2015-11-05', 'AM'),
(171, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:12:49am', '', 'p7omqohrs4hqep60ttojrmaft5', '2015-11-05', 'AM'),
(172, 'RAUU-1599', '112.196.136.78', '2015-11-05 10:13:48am', '', 'hljajpbd6k1nlff615v4elv2p4', '2015-11-05', 'AM'),
(173, 'ZJBX-0533', '112.196.136.78', '2015-11-05 10:16:07am', '', 'pbc7d3ih2mct582vdi6bedtr22', '2015-11-05', 'QF'),
(174, 'VUQQ-3341', '112.196.136.78', '2015-11-05 10:21:58am', '', 'vehvfr5jl8rrqefka8mql8mfc1', '2015-11-05', 'AM'),
(175, 'FXIO-8905', '159.203.111.47', '2015-11-05 10:23:38am', '2015-11-05 10:38:53am', 'jm5v5ninsaiqi2epis56e2hji6', '2015-11-05', 'AM'),
(176, 'admin', '112.196.136.44', '2015-11-05 10:24:51am', '', 'tc0ujiindn15rdshi5j7gnm947', '2015-11-05', 'AD'),
(177, 'admin', '112.196.136.78', '2015-11-05 10:26:39am', '', 'd7j9c8dri14a2bqtgq38a464b3', '2015-11-05', 'AD'),
(178, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:32:07am', '', '80m81fib9mjhvubr4eh8l48j40', '2015-11-05', 'AM'),
(179, 'RAUU-1599', '112.196.136.78', '2015-11-05 10:35:48am', '', 'p1hshh2p1sk7t8oafhulh6gje7', '2015-11-05', 'AM'),
(180, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:36:57am', '', '0u01gs4kh0es9nhpohhv5v67t5', '2015-11-05', 'AM'),
(181, 'admin', '112.196.136.44', '2015-11-05 10:49:16am', '', 'c3rqvnrc722tb6kep4ss9s7df4', '2015-11-05', 'AD'),
(182, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:53:49am', '', 'tk3r4ohe8c9mn3imlf5lsa6q11', '2015-11-05', 'AM'),
(183, 'RAUU-1599', '112.196.136.78', '2015-11-05 11:12:12am', '', 'seju7vo9b32hdg3bpe27q9cco3', '2015-11-05', 'AM'),
(184, 'admin', '112.196.136.78', '2015-11-06 00:38:02am', '2015-11-06 04:48:12am', 'qlooms0rbbjpqdri13hdpvg6s6', '2015-11-06', 'AD'),
(185, 'admin', '112.196.136.78', '2015-11-06 00:54:11am', '', 'gbo9ln3fv94etsr3967t16ean0', '2015-11-06', 'AD'),
(186, 'RAUU-1599', '112.196.136.78', '2015-11-06 01:00:50am', '2015-11-06 01:07:42am', '46opijt0dokquu25lfj0g7drm6', '2015-11-06', 'AM'),
(187, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:02:52am', '2015-11-06 01:06:08am', '6j06053oslvoldpqvq4ft27797', '2015-11-06', 'AM'),
(188, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:06:10am', '2015-11-06 01:07:14am', '3ku34roihdpfugf3jnjs1lgkd4', '2015-11-06', 'AM'),
(189, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:07:16am', '2015-11-06 01:15:09am', '0uken6lkb2eafv838v807vnmc2', '2015-11-06', 'AM'),
(190, 'RAUU-1599', '112.196.136.78', '2015-11-06 01:07:46am', '2015-11-06 01:15:11am', 'ndj25bgvspse4uofi9j74m79o0', '2015-11-06', 'AM'),
(191, 'RAUU-1599', '112.196.136.78', '2015-11-06 01:18:18am', '', 'qspq30siv9cu57nm5t2oj7u5q3', '2015-11-06', 'AM'),
(192, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:18:20am', '2015-11-06 07:14:50am', '06gj812nl7vl2clqf5ulm7cgj5', '2015-11-06', 'AM'),
(193, 'ZJBX-0533', '112.196.136.78', '2015-11-06 02:08:31am', '', 'nv2d3m3dl1hq4c6ujukb4mqv41', '2015-11-06', 'QF'),
(194, 'FTTP-8399', '112.196.136.78', '2015-11-06 02:11:17am', '2015-11-06 05:10:13am', 'p0oe5ale1s9cdtj0b2intus7c4', '2015-11-06', 'TM'),
(195, 'RAUU-1599', '112.196.136.78', '2015-11-06 02:24:25am', '', 'nacm4k37rlgfhkkej933nb7g04', '2015-11-06', 'AM'),
(196, 'admin', '112.196.136.78', '2015-11-06 02:30:04am', '', '4kjvlcjenp2hpp8ug4tkv4r0n1', '2015-11-06', 'AD'),
(197, 'VUQQ-3341', '112.196.136.78', '2015-11-06 02:39:30am', '2015-11-06 05:23:06am', 'pq27nre6qfas9ol22stelck621', '2015-11-06', 'AM'),
(198, 'ZJBX-0533', '112.196.136.78', '2015-11-06 02:42:32am', '', 'te1e009v46ubvb7cne0r5bvqo4', '2015-11-06', 'QF'),
(199, 'admin', '112.196.136.44', '2015-11-06 03:14:23am', '', 'okpids7jpca45gn6irponmr476', '2015-11-06', 'AD'),
(200, 'admin', '112.196.136.44', '2015-11-06 03:16:31am', '', '1m4de1rg6ie5pkug9e1nu5fth2', '2015-11-06', 'AD'),
(201, 'admin', '112.196.136.78', '2015-11-06 04:48:21am', '2015-11-06 09:08:04am', 'ph68rr20h5en71o2b9u9he1950', '2015-11-06', 'AD'),
(202, 'ZJBX-0533', '112.196.136.78', '2015-11-06 05:10:26am', '', 'bf725nnppt61fhqcp2k3oqqhi2', '2015-11-06', 'QF'),
(203, 'RAUU-1599', '112.196.136.78', '2015-11-06 05:19:20am', '2015-11-06 06:47:36am', 'i8i3qphtjfm7h7mgk5j6kpi6k4', '2015-11-06', 'AM'),
(204, 'RAUU-1599', '112.196.136.78', '2015-11-06 05:24:06am', '2015-11-06 06:42:54am', 'el8v3k9e3nj8nihiu7k9ne92c7', '2015-11-06', 'AM'),
(205, 'RAUU-1599', '192.96.201.167', '2015-11-06 06:41:16am', '2015-11-06 08:58:53am', 'jbil7m61s1cnle3tl1pvvc2s23', '2015-11-06', 'AM'),
(206, 'VUQQ-3341', '112.196.136.78', '2015-11-06 06:43:44am', '', '6o2qu5gh0o44avf25inavkfgj0', '2015-11-06', 'AM'),
(207, 'FTTP-8399', '112.196.136.78', '2015-11-06 06:47:47am', '', 're9442468qf0lrctse636gkci5', '2015-11-06', 'TM'),
(208, 'VUQQ-3341', '112.196.136.78', '2015-11-06 07:14:55am', '2015-11-06 07:14:59am', '6bl8peqbeaja33mnirh4gar0o7', '2015-11-06', 'AM'),
(209, 'VUQQ-3341', '112.196.136.78', '2015-11-06 07:15:08am', '2015-11-06 07:15:15am', 'gnmc8rijckbtsj1bq2t76pq435', '2015-11-06', 'AM'),
(210, 'VUQQ-3341', '112.196.136.78', '2015-11-06 07:15:22am', '', 'sdidt1nnqo8hkg8qcun65h2tv2', '2015-11-06', 'AM'),
(211, 'RAUU-1599', '112.196.136.78', '2015-11-06 09:08:07am', '2015-11-06 10:21:24am', '1lph77l3au9p4m7ennopftuao5', '2015-11-06', 'AM'),
(212, 'admin', '112.196.136.78', '2015-11-06 10:14:51am', '', 'vo8dva4f7u3vr5rin1vamchpa5', '2015-11-06', 'AD'),
(213, 'admin', '112.196.136.78', '2015-11-06 10:21:29am', '', 'p0mp5ouvuj2db9ikafn053rpj5', '2015-11-06', 'AD'),
(214, 'RAUU-1599', '112.196.136.78', '2015-11-06 10:24:34am', '', '9im082kr8l28ad7d9tcejqnen0', '2015-11-06', 'AM'),
(215, 'VUQQ-3341', '112.196.136.78', '2015-11-06 10:24:47am', '', '2rmgp4ov7b2gkf9ll8t5m8psd5', '2015-11-06', 'AM'),
(216, 'CXBZ-9999', '112.196.136.78', '2015-11-06 10:30:40am', '', 'khn3turo1ar09d93cn5jtf7ue3', '2015-11-06', 'AM'),
(217, 'admin', '112.196.136.78', '2015-11-07 00:50:31am', '2015-11-07 01:44:11am', '3veeniik0h4so65g1ciso80dl3', '2015-11-07', 'AD'),
(218, 'admin', '112.196.136.78', '2015-11-07 01:13:21am', '2015-11-07 03:25:36am', '6oj6m6rcl97kjfk62f5el09d96', '2015-11-07', 'AD'),
(219, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:14:40am', '', 'kogaq9sg81ie4hcmrcqsfslvk3', '2015-11-07', 'QF'),
(220, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:43:35am', '', 'tvk1g9dr4aavsb5i199mf7e173', '2015-11-07', 'QF'),
(221, 'admin', '112.196.136.78', '2015-11-07 01:44:42am', '2015-11-07 03:03:49am', 'dr6mahra7414iqm80cj8e0a3u5', '2015-11-07', 'AD'),
(222, 'FXIO-8905', '112.196.136.78', '2015-11-07 01:52:28am', '2015-11-07 01:53:05am', 'mfhmhv6jrgdmu4i4cgvamliqm3', '2015-11-07', 'AM'),
(223, 'RAUU-1599', '112.196.136.78', '2015-11-07 01:53:23am', '2015-11-07 02:44:26am', 'bmiebsfaedvsi5ve8gh1lce3l0', '2015-11-07', 'AM'),
(224, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:54:15am', '2015-11-07 01:54:26am', '3dq1b1qbr37n5dbid5lvkhg525', '2015-11-07', 'QF'),
(225, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:54:39am', '2015-11-07 02:54:14am', 'k365d68rtc4nkgtmgaq8tejqb3', '2015-11-07', 'QF'),
(226, 'ZJBX-0533', '112.196.136.78', '2015-11-07 02:43:36am', '', '86bbjie4cp56tbpl1j8eu292s0', '2015-11-07', 'QF'),
(227, 'ZJBX-0533', '112.196.136.78', '2015-11-07 02:44:37am', '2015-11-07 05:39:30am', 'k9urlgo4l5l9qhoiqrud126j81', '2015-11-07', 'QF'),
(228, 'VUQQ-3341', '112.196.136.78', '2015-11-07 02:54:27am', '2015-11-07 02:54:54am', '5ul3t53qkrhaevueh8shv2uha7', '2015-11-07', 'AM'),
(229, 'FXIO-8905', '112.196.136.78', '2015-11-07 02:55:04am', '2015-11-07 02:55:09am', 'cq17r2ikaiccjuc1nd7bb4qvr2', '2015-11-07', 'AM'),
(230, 'CXBZ-9999', '112.196.136.78', '2015-11-07 02:55:29am', '2015-11-07 02:55:58am', 'ndj703gemb3nvpktltriptjfi4', '2015-11-07', 'AM'),
(231, 'RAUU-1599', '112.196.136.78', '2015-11-07 02:56:09am', '', 'qahu3b1r7bit6oc0tpvrafb5m1', '2015-11-07', 'AM'),
(232, 'VUQQ-3341', '112.196.136.78', '2015-11-07 03:03:52am', '2015-11-07 11:45:49am', 'rlet8hhdn2ji0k3f3vloi08gq0', '2015-11-07', 'AM'),
(233, 'RAUU-1599', '112.196.136.78', '2015-11-07 03:25:57am', '2015-11-07 04:32:24am', 'akknq8uaa33ed2hcucvujd0m53', '2015-11-07', 'AM'),
(234, 'VUQQ-3341', '112.196.136.78', '2015-11-07 04:32:33am', '2015-11-07 05:24:39am', 'lko84t1n5opc6b0u92qsrjmas1', '2015-11-07', 'AM'),
(235, 'ZJBX-0533', '112.196.136.78', '2015-11-07 04:37:54am', '', '46tnsjl5tfreubl4k8kidi47n6', '2015-11-07', 'QF'),
(236, 'admin', '112.196.136.78', '2015-11-07 05:29:43am', '2015-11-07 05:29:59am', 'b82c5skh3motos25frardebcj6', '2015-11-07', 'AD'),
(237, 'FTTP-8399', '112.196.136.78', '2015-11-07 05:30:08am', '2015-11-07 06:58:53am', '22p0jc3vd4pvavcfs7hm4cp8t6', '2015-11-07', 'TM'),
(238, 'RAUU-1599', '112.196.136.78', '2015-11-07 05:36:42am', '', 'g4hja5j1i7jokddce8ltlrp392', '2015-11-07', 'AM'),
(239, 'RAUU-1599', '112.196.136.78', '2015-11-07 05:39:40am', '2015-11-07 07:42:59am', '09a8vdanfubqqvngtld6e8qfs3', '2015-11-07', 'AM'),
(240, 'RAUU-1599', '112.196.136.78', '2015-11-07 06:59:18am', '2015-11-07 07:42:26am', 'isoj2figjuevs3lpi31m5k3vv1', '2015-11-07', 'AM'),
(241, 'admin', '112.196.136.78', '2015-11-07 07:42:29am', '2015-11-07 09:35:14am', 'sq25a7gfdnk09mne6t61qpj2a4', '2015-11-07', 'AD'),
(242, 'VUQQ-3341', '112.196.136.78', '2015-11-07 07:43:09am', '', 'l6t0trbc96kq1nlc8vvnk0ksj3', '2015-11-07', 'AM'),
(243, 'ZJBX-0533', '112.196.136.78', '2015-11-07 09:35:26am', '2015-11-07 10:06:10am', '0hgg0huru4qn41jg8hugejm252', '2015-11-07', 'QF'),
(244, 'admin', '112.196.136.78', '2015-11-07 10:06:13am', '', 's0e3hrb3ak12ab8aiairqkcat0', '2015-11-07', 'AD'),
(245, 'admin', '112.196.136.78', '2015-11-07 10:07:30am', '2015-11-07 10:08:34am', 'p1ed43hc4qjsq2hsq5vchdk7c3', '2015-11-07', 'AD'),
(246, 'RAUU-1599', '112.196.136.78', '2015-11-07 10:08:39am', '', '4r3rallt1v86u36onmnk7mpcr0', '2015-11-07', 'AM'),
(247, 'admin', '112.196.136.78', '2015-11-07 11:46:18am', '2015-11-07 11:46:29am', '04pjcsfiuknvinfg8bt8o2sn72', '2015-11-07', 'AD'),
(248, 'FTTP-8399', '112.196.136.78', '2015-11-07 11:46:32am', '2015-11-07 11:49:05am', '6h1l88mte3tsi0i5u4t6c2hcs7', '2015-11-07', 'TM'),
(249, 'admin', '112.196.136.78', '2015-11-07 11:49:16am', '2015-11-07 11:49:30am', '7a3dt52b57jaufa2jo7vghc0n3', '2015-11-07', 'AD'),
(250, 'VUQQ-3341', '112.196.136.78', '2015-11-07 11:49:38am', '', 'cis6ai0lqsqs2gmgkaa1j7a5k7', '2015-11-07', 'AM'),
(251, 'admin', '27.97.116.5', '2015-11-08 10:48:07am', '', 'e1ua27el08vsl640eoje3carl1', '2015-11-08', 'AD'),
(252, 'admin', '112.196.136.78', '2015-11-09 00:47:40am', '2015-11-09 01:21:17am', 'vg87cncboaj6brhun00pu87202', '2015-11-09', 'AD'),
(253, 'admin', '112.196.136.78', '2015-11-09 00:47:43am', '2015-11-09 04:47:17am', 'dolnsqiijl4p8ssjgidti6ljg7', '2015-11-09', 'AD'),
(254, 'VUQQ-3341', '112.196.136.78', '2015-11-09 00:51:28am', '2015-11-09 06:49:09am', 'cmrfp5kni8s06dgib7ua1d82t1', '2015-11-09', 'AM'),
(255, 'RAUU-1599', '112.196.136.78', '2015-11-09 00:51:50am', '', '0fsiefh3aht36lsou91ec5bk57', '2015-11-09', 'AM'),
(256, 'ZJBX-0533', '112.196.136.78', '2015-11-09 00:53:17am', '', 'elvbg1ie1q6626a9i0auaarsr4', '2015-11-09', 'QF'),
(257, 'ZJBX-0533', '112.196.136.78', '2015-11-09 00:54:48am', '', 'ebaku2t3a9h3pmgn2uv2fpqd10', '2015-11-09', 'QF'),
(258, 'RAUU-1599', '112.196.136.78', '2015-11-09 01:18:15am', '2015-11-09 02:49:31am', 'blr1i1keeqmnqpjinjmduod5i2', '2015-11-09', 'AM'),
(259, 'RAUU-1599', '112.196.136.78', '2015-11-09 01:21:21am', '', 'sp0kqr0beo40976pheiavnroo3', '2015-11-09', 'AM'),
(260, 'admin', '112.196.136.78', '2015-11-09 02:17:32am', '2015-11-09 02:20:33am', 's6la7tm582fmilk6lc1liee943', '2015-11-09', 'AD'),
(261, 'RAUU-1599', '112.196.136.78', '2015-11-09 02:20:38am', '2015-11-09 05:21:17am', 'c8r8qk7364gkpo0hr87ckd1281', '2015-11-09', 'AM'),
(262, 'admin', '112.196.136.78', '2015-11-09 02:49:40am', '2015-11-09 06:43:43am', '401rcurr6noeoqkp11a2mr2sj2', '2015-11-09', 'AD'),
(263, 'admin', '112.196.136.78', '2015-11-09 03:36:02am', '', 'f1o5g6fgp4uj7ds1rqtfhmd9r7', '2015-11-09', 'AD'),
(264, 'ZJBX-0533', '112.196.136.78', '2015-11-09 04:47:21am', '2015-11-09 05:21:23am', 'rjb5dpij5tld9oii0nd9lb5tk6', '2015-11-09', 'QF'),
(265, 'admin', '112.196.136.78', '2015-11-09 05:09:09am', '', 'k7nno78sjjf82uqs4nt518hl97', '2015-11-09', 'AD'),
(266, 'admin', '112.196.136.78', '2015-11-09 05:21:19am', '2015-11-09 05:32:48am', 'hfp59lj5ck7e6plm1mbtjshc12', '2015-11-09', 'AD'),
(267, 'admin', '112.196.136.78', '2015-11-09 05:21:26am', '2015-11-09 05:21:36am', 'i6ghlaioi8qn785jk6ckd934p2', '2015-11-09', 'AD'),
(268, 'VUQQ-3341', '112.196.136.78', '2015-11-09 05:21:39am', '', 'midpf80hnrlrulqk81gg5d6u87', '2015-11-09', 'AM'),
(269, 'RAUU-1599', '112.196.136.78', '2015-11-09 05:33:12am', '', 'b19fsl1ddmh7i9gsneddioccq2', '2015-11-09', 'AM'),
(270, 'FTTP-8399', '112.196.136.78', '2015-11-09 06:44:09am', '', 'ibo60fnb8vvv5q8f3oftq53if4', '2015-11-09', 'TM'),
(271, 'FTTP-8399', '112.196.136.78', '2015-11-09 06:49:20am', '', 'o8tkkclgi2ctk46j0n63lk5tv2', '2015-11-09', 'TM'),
(272, 'admin', '112.196.136.78', '2015-11-10 02:10:16am', '', 'vsm7lt9dust09a1o9ohl3pr3e2', '2015-11-10', 'AD'),
(273, 'FXIO-8905', '112.196.136.78', '2015-11-10 02:14:20am', '2015-11-10 02:21:10am', 'hfc2cka1iuaamfmjnso0q72t41', '2015-11-10', 'AM'),
(274, 'RAUU-1599', '112.196.136.78', '2015-11-10 02:15:49am', '', 'tp2cda2d8qo8pldjuv96e10jl2', '2015-11-10', 'AM'),
(275, 'VUQQ-3341', '112.196.136.78', '2015-11-10 02:21:26am', '', '8g2hchpcavcoffltc7qfemknf5', '2015-11-10', 'AM'),
(276, 'VUQQ-3341', '112.196.136.78', '2015-11-10 07:03:16am', '', 'n8sulnehdmsgttebvrqa8nq187', '2015-11-10', 'AM'),
(277, 'admin', '112.196.136.78', '2015-11-10 07:41:52am', '', 'a7l85cuntnjjhu1pcet3lshf06', '2015-11-10', 'AD'),
(278, 'admin', '66.49.237.16', '2015-11-10 14:31:04pm', '2015-11-10 15:07:57pm', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', 'AD'),
(279, 'FXIO-8905', '66.49.237.16', '2015-11-10 14:53:53pm', '', '447rgidpk0pnp566g0kaeu0142', '2015-11-10', 'AM'),
(280, 'FXIO-8905', '66.49.237.16', '2015-11-10 14:56:09pm', '2015-11-10 15:06:19pm', '9aj8nrh02kcf715hsin0eb0sn2', '2015-11-10', 'AM'),
(281, 'RAUU-1599', '66.49.237.16', '2015-11-10 15:07:33pm', '2015-11-10 15:07:48pm', 'j97fdhr68u0si6214763h6mlo3', '2015-11-10', 'AM'),
(282, 'RAUU-1599', '66.49.237.16', '2015-11-10 15:07:52pm', '', 'p0eg98a5vbd0ttl0pp05jeq1v4', '2015-11-10', 'AM'),
(283, 'RAUU-1599', '66.49.237.16', '2015-11-10 15:08:00pm', '2015-11-10 15:08:03pm', 'rkvacndllkpfff0vsaofh10pa4', '2015-11-10', 'AM'),
(284, 'admin', '66.49.237.16', '2015-11-10 15:08:09pm', '', 'kgp5hukf1o09e26tts3v1q1c04', '2015-11-10', 'AD'),
(285, 'admin', '66.49.237.16', '2015-11-10 16:05:44pm', '', 'bttsits146cf9id7dldlaghc73', '2015-11-10', 'AD'),
(286, 'admin', '66.49.237.16', '2015-11-10 16:06:14pm', '', '4g2pevlpcgtcc3gjm47mojsqm1', '2015-11-10', 'AD'),
(287, 'admin', '112.196.136.78', '2015-11-11 01:35:26am', '2015-11-11 01:38:18am', '1235kmrko3ombedbcubd2tn2s6', '2015-11-11', 'AD'),
(288, 'admin', '112.196.136.78', '2015-11-11 01:38:22am', '2015-11-11 01:38:26am', 'bb5f9m1m8hhfpf2spbdm59ehb5', '2015-11-11', 'AD'),
(289, 'admin', '112.196.136.78', '2015-11-11 01:55:10am', '', 'ls7e06h52k43sikbv4mlndlf52', '2015-11-11', 'AD'),
(290, 'admin', '66.49.237.16', '2015-11-11 10:56:13am', '', 'bfdjtj23ruql45j066emehp026', '2015-11-11', 'AD'),
(291, 'admin', '66.49.237.16', '2015-11-11 13:16:04pm', '', 'r6c39nvq98aegd2s1f04djr2i3', '2015-11-11', 'AD'),
(292, 'FXIO-8905', '66.49.237.16', '2015-11-11 13:16:25pm', '2015-11-11 13:25:55pm', 'i0fesmvivfvt3bs7e3sneod452', '2015-11-11', 'AM'),
(293, 'FXIO-8905', '66.49.237.16', '2015-11-11 13:26:07pm', '2015-11-11 13:26:32pm', 'efcuqcbsr8ocorhneq9rqohhp3', '2015-11-11', 'AM'),
(294, 'FXIO-8905', '66.49.237.16', '2015-11-11 13:26:37pm', '', 'o6q56paq2fe55qgvu1eon509e5', '2015-11-11', 'AM'),
(295, 'admin', '66.49.237.16', '2015-11-12 12:47:56pm', '', 'k04c0pmbt9dich8btsdimdssb3', '2015-11-12', 'AD'),
(296, 'admin', '59.161.188.117', '2015-11-12 13:43:22pm', '', 'jr51mje4ntp2ok0r4sefpfnv81', '2015-11-12', 'AD'),
(297, 'admin', '112.196.136.78', '2015-11-14 01:06:41am', '', '9dc3abfa1ehjnek6fuel4ael27', '2015-11-14', 'AD'),
(298, 'admin', '112.196.136.78', '2015-11-15 22:52:18pm', '2015-11-15 23:24:28pm', 'vfetm6r3b2jh2iodo517mikts2', '2015-11-15', 'AD'),
(299, 'CXBZ-9999', '112.196.136.78', '2015-11-15 23:16:17pm', '2015-11-15 23:16:49pm', 'qdph3l3an4dko9vpbj440fdf77', '2015-11-15', 'AM'),
(300, 'FXIO-8905', '112.196.136.78', '2015-11-15 23:17:07pm', '2015-11-15 23:20:02pm', 'h04hspagligs15svcqulsmt0p4', '2015-11-15', 'AM'),
(301, 'RAUU-1599', '112.196.136.78', '2015-11-15 23:20:19pm', '2015-11-16 03:54:50am', 'k58401043c59cbipfal8433j85', '2015-11-15', 'AM'),
(302, 'admin', '112.196.136.78', '2015-11-15 23:24:31pm', '', '7n1i9s54430um74ji804tevq06', '2015-11-15', 'AD'),
(303, 'admin', '112.196.136.78', '2015-11-16 00:45:47am', '', 'ukafcgmlai4jsofdhbdsb595l0', '2015-11-16', 'AD'),
(304, 'CXBZ-9999', '112.196.136.78', '2015-11-16 00:46:45am', '2015-11-16 00:47:33am', 'm671va9963d7jhb9hlj4v8q4h6', '2015-11-16', 'AM'),
(305, 'VUQQ-3341', '112.196.136.78', '2015-11-16 00:47:40am', '', 'vdfa54bcbf8sc8ne6s1vge0j96', '2015-11-16', 'AM'),
(306, 'admin', '112.196.136.78', '2015-11-16 03:12:34am', '2015-11-16 03:47:00am', '56qehkq203d0i4supmo4i6m525', '2015-11-16', 'AD'),
(307, 'FXIO-8905', '112.196.136.78', '2015-11-16 03:47:12am', '', '1rsveijddsnh74u9tfnjstljg4', '2015-11-16', 'AM'),
(308, 'admin', '112.196.136.78', '2015-11-16 03:54:54am', '2015-11-16 06:25:33am', '7cglicipko2l31adjons0q4td6', '2015-11-16', 'AD'),
(309, 'admin', '112.196.136.78', '2015-11-16 04:11:19am', '', 'h3qeu0un4o3hlhgijiomjof6p4', '2015-11-16', 'AD'),
(310, 'admin', '112.196.136.78', '2015-11-16 05:22:31am', '', 'kk94lf3niifr3updldiaooa130', '2015-11-16', 'AD'),
(311, 'admin', '112.196.136.78', '2015-11-16 06:24:57am', '2015-11-16 06:33:54am', '8jfuoq66e34434k5j4u2e5kbt0', '2015-11-16', 'AD'),
(312, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:25:42am', '', '6hkpdr7arusjgeftrau4j1cft1', '2015-11-16', 'QF'),
(313, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:25:50am', '', '0i01db9loqrsmujjr1smkiblm5', '2015-11-16', 'QF'),
(314, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:26:08am', '', 't4vaa53uf0rjvmhu4vpp3fgfc0', '2015-11-16', 'QF'),
(315, 'admin', '112.196.136.78', '2015-11-16 06:26:56am', '2015-11-16 06:27:26am', '0oo84j98g81mq9pgtdhchasd13', '2015-11-16', 'AD'),
(316, 'CXBZ-9999', '112.196.136.78', '2015-11-16 06:27:34am', '', 'nop7kllulo89bi8vks2542fr06', '2015-11-16', 'AM'),
(317, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:34:02am', '', 'koqehudbc272jvaojgoel6rm84', '2015-11-16', 'QF'),
(318, 'admin', '112.196.136.78', '2015-11-16 07:05:58am', '2015-11-16 07:11:27am', '4vu049g0ejg66m97ngo5ilb6a1', '2015-11-16', 'AD'),
(319, 'CXBZ-9999', '112.196.136.78', '2015-11-16 07:06:48am', '2015-11-16 07:07:18am', 'h03o91njehip461pv3n76pqk05', '2015-11-16', 'AM'),
(320, 'FTTP-8399', '112.196.136.78', '2015-11-16 07:07:27am', '2015-11-16 07:07:56am', 'gogthrs1snlaacgmmasrf25tc4', '2015-11-16', 'TM'),
(321, 'ZJBX-0533', '112.196.136.78', '2015-11-16 07:08:05am', '', 'cme3ai3ei712qd1uqmq01f8aa3', '2015-11-16', 'QF'),
(322, 'ZJBX-0533', '112.196.136.78', '2015-11-16 07:09:43am', '2015-11-16 07:11:38am', '0ggsunn4d4usr160ic612uk7g4', '2015-11-16', 'QF'),
(323, 'admin', '1.22.133.140', '2015-11-16 09:10:56am', '2015-11-16 09:16:53am', 'gd8p5qo6eon292jrs2e3qmola1', '2015-11-16', 'AD'),
(324, 'admin', '66.49.237.16', '2015-11-16 11:28:28am', '', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', 'AD'),
(325, 'FXIO-8905', '66.49.237.16', '2015-11-16 11:44:19am', '2015-11-16 11:55:49am', 'the9315kk1tkk88odt6c0q9k81', '2015-11-16', 'AM'),
(326, 'FXIO-8905', '66.49.237.16', '2015-11-16 11:56:00am', '', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', 'AM'),
(327, 'admin', '14.98.102.29', '2015-11-16 12:46:27pm', '', 'hvq4mmcki6ldlonvb892km7dn1', '2015-11-16', 'AD'),
(328, 'VUQQ-3341', '14.98.102.29', '2015-11-16 13:05:01pm', '', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', 'AM'),
(329, 'admin', '112.196.136.78', '2015-11-16 23:30:19pm', '', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-16', 'AD'),
(330, 'VUQQ-3341', '112.196.136.78', '2015-11-16 23:34:12pm', '2015-11-16 23:34:20pm', '1ba9tnf7jd8pi22io8dktjahl5', '2015-11-16', 'AM'),
(331, 'RAUU-1599', '112.196.136.78', '2015-11-16 23:34:40pm', '2015-11-16 23:34:54pm', 'ejr8911h4cn3okr8k440upont5', '2015-11-16', 'AM'),
(332, 'FMVI-7264', '112.196.136.78', '2015-11-16 23:35:03pm', '2015-11-16 23:35:10pm', 'r50n0qt3qoc8bfmv2pgjng70s6', '2015-11-16', 'AM'),
(333, 'FXIO-8905', '112.196.136.78', '2015-11-16 23:35:28pm', '2015-11-17 03:12:59am', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-16', 'AM'),
(334, 'admin', '112.196.136.78', '2015-11-17 00:18:04am', '2015-11-17 00:18:25am', '9r17f8tg3qj8tb25adni3nqdh3', '2015-11-17', 'AD'),
(335, 'FMVI-7264', '112.196.136.78', '2015-11-17 00:18:28am', '2015-11-17 00:19:18am', 'jfraaufgioas6s8fqtboia7415', '2015-11-17', 'AM'),
(336, 'admin', '112.196.136.78', '2015-11-17 00:19:03am', '', 'dklm8lgoe38utels85301d6r45', '2015-11-17', 'AD'),
(337, 'FXIO-8905', '112.196.136.78', '2015-11-17 00:19:25am', '', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', 'AM'),
(338, 'FXIO-8905', '112.196.136.78', '2015-11-17 03:15:32am', '', '6vemlkso76jl11katt8379dm17', '2015-11-17', 'AM'),
(339, 'admin', '66.49.237.16', '2015-11-17 13:52:06pm', '', 'br9qiggvb3dalpqu0m97n46h26', '2015-11-17', 'AD'),
(340, 'FXIO-8905', '66.49.237.16', '2015-11-17 13:52:24pm', '', 'e20gnmugh2hg4ous2agdsss0a5', '2015-11-17', 'AM'),
(341, 'admin', '66.49.237.16', '2015-11-17 13:56:14pm', '', 'mdfaprmtuh7mgor7813ndu5a01', '2015-11-17', 'AD'),
(342, 'admin', '14.98.228.196', '2015-11-17 14:04:20pm', '', 'ehe0ueugeloconu9696vfc4ps1', '2015-11-17', 'AD'),
(343, 'admin', '112.196.136.78', '2015-11-17 23:43:11pm', '2015-11-18 00:32:52am', '1gpk18tpmglu7q0p9h6jdk03p0', '2015-11-17', 'AD'),
(344, 'FXIO-8905', '112.196.136.78', '2015-11-17 23:44:53pm', '2015-11-18 00:42:00am', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', 'AM'),
(345, 'ZJBX-0533', '112.196.136.78', '2015-11-17 23:53:01pm', '', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-17', 'QF'),
(346, 'ZJBX-0533', '112.196.136.78', '2015-11-18 00:33:00am', '2015-11-18 01:58:13am', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', 'QF'),
(347, 'admin', '112.196.136.78', '2015-11-18 00:42:05am', '', 'u6t8cbq55sg0b8o13hcb3g6601', '2015-11-18', 'AD'),
(348, 'admin', '112.196.136.78', '2015-11-18 01:58:16am', '', '4ah8a76jcl8vpl3hre52srpg96', '2015-11-18', 'AD'),
(349, 'admin', '112.196.136.78', '2015-11-18 04:03:55am', '2015-11-18 04:08:36am', 'r347qec9vuf3ondr526hoqk7v6', '2015-11-18', 'AD'),
(350, 'admin', '112.196.136.78', '2015-11-18 04:09:11am', '2015-11-18 05:04:07am', 'd5kspsqs83dj18idkbcqr16dk7', '2015-11-18', 'AD'),
(351, 'FXIO-8905', '112.196.136.78', '2015-11-18 04:09:38am', '2015-11-18 05:02:46am', 'mt1iokkh3b18ms9hokpd6cs1k7', '2015-11-18', 'AM'),
(352, 'VUQQ-3341', '112.196.136.78', '2015-11-18 05:02:58am', '2015-11-18 05:45:56am', 'eqjbms2enugjjctqibkg85ki83', '2015-11-18', 'AM'),
(353, 'FXIO-8905', '112.196.136.78', '2015-11-18 05:04:21am', '', 't6h5ti0b9p2qlvvld3m5rmr873', '2015-11-18', 'AM'),
(354, 'admin', '112.196.136.78', '2015-11-18 05:18:33am', '', '7m3rqagoedqskbn82no2s4ikb4', '2015-11-18', 'AD'),
(355, 'FXIO-8905', '112.196.136.78', '2015-11-18 05:23:49am', '', '6mahb719nspaadvi4r0gm0p6j5', '2015-11-18', 'AM'),
(356, 'admin', '112.196.136.78', '2015-11-18 05:45:58am', '2015-11-18 06:09:41am', 'q0k3rid3tjlcuh82i4sr537hm3', '2015-11-18', 'AD'),
(357, 'VUQQ-3341', '112.196.136.78', '2015-11-18 06:52:55am', '', '2au8d50qcl8rok01p1tle2ohc1', '2015-11-18', 'AM'),
(358, 'admin', '112.196.136.78', '2015-11-18 07:41:03am', '', 'hh06imq98j8e8hp709ta5lq230', '2015-11-18', 'AD'),
(359, 'admin', '66.49.237.16', '2015-11-18 15:11:56pm', '2015-11-18 15:12:10pm', 'tgacm9t46s9ao4t54djeb61na2', '2015-11-18', 'AD'),
(360, 'FXIO-8905', '66.49.237.16', '2015-11-18 15:12:12pm', '2015-11-18 15:14:13pm', 'v9q0d10eu16vesjt9n37leloq7', '2015-11-18', 'AM'),
(361, 'admin', '66.49.237.16', '2015-11-18 15:14:20pm', '2015-11-18 15:17:43pm', 'm1p02j9irmp1bc95hmqfphirr4', '2015-11-18', 'AD'),
(362, 'FXIO-8905', '66.49.237.16', '2015-11-18 15:17:46pm', '2015-11-18 15:21:56pm', 'kjero49jmns1u9lr9bk7umio87', '2015-11-18', 'AM'),
(363, 'admin', '66.49.237.16', '2015-11-18 15:19:26pm', '2015-11-18 15:19:40pm', 'gmah7vgb0qv6vt38eg0l66o2u5', '2015-11-18', 'AD'),
(364, 'ZJBX-0533', '66.49.237.16', '2015-11-18 15:19:43pm', '', 'urp9cmldhqaippbcj5pfakrgb0', '2015-11-18', 'QF'),
(365, 'admin', '66.49.237.16', '2015-11-18 15:22:02pm', '2015-11-18 15:25:19pm', 'gp0f8iv8i1bnoiacbam2172ou5', '2015-11-18', 'AD'),
(366, 'FMVI-7264', '66.49.237.16', '2015-11-18 15:25:22pm', '', '9r9n50m7fbch3ir36rccj8g302', '2015-11-18', 'AM'),
(367, 'admin', '112.196.136.78', '2015-11-18 23:33:20pm', '2015-11-19 04:52:42am', 'qk3rgjb3v551g6tksvn5eoj1j2', '2015-11-18', 'AD'),
(368, 'admin', '112.196.136.78', '2015-11-18 23:35:13pm', '', '9mejdtj16kam4e7cc9rnv5dli0', '2015-11-18', 'AD'),
(369, 'admin', '112.196.136.78', '2015-11-19 01:06:54am', '', 's4da6vpvdmgan97mjncfc2d264', '2015-11-19', 'AD'),
(370, 'FMVI-7264', '112.196.136.78', '2015-11-19 01:16:42am', '', '2dn5i8jacvheciabqn6mm6o2m1', '2015-11-19', 'AM'),
(371, 'FXIO-8905', '112.196.136.78', '2015-11-19 02:20:09am', '', 'tcpqac5l96oph360eptgh3esk4', '2015-11-19', 'AM'),
(372, 'ZJBX-0533', '112.196.136.78', '2015-11-19 04:52:52am', '2015-11-19 07:39:23am', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', 'QF'),
(373, 'admin', '112.196.136.78', '2015-11-19 06:03:54am', '2015-11-19 06:05:04am', '7p5ur2b84g6avenupju21c0c76', '2015-11-19', 'AD'),
(374, 'FTTP-8399', '112.196.136.78', '2015-11-19 06:05:14am', '', 'la3b6jrgtralecih1fe83kghs1', '2015-11-19', 'TM'),
(375, 'FXIO-8905', '104.131.14.167', '2015-11-19 06:15:06am', '2015-11-19 06:15:35am', 'dv7djbn4sh74aqqe5ofgo4iu23', '2015-11-19', 'AM'),
(376, 'VUQQ-3341', '104.131.14.167', '2015-11-19 06:15:45am', '2015-11-19 06:16:01am', 'cku1haicc9rkul5onsnvbpa9k0', '2015-11-19', 'AM'),
(377, 'RAUU-1599', '104.131.14.167', '2015-11-19 06:16:11am', '2015-11-19 06:16:36am', '66rr53irvk4tps10ithtjbs1q6', '2015-11-19', 'AM'),
(378, 'FMVI-7264', '104.131.14.167', '2015-11-19 06:16:46am', '', 'ktgdo9uos15925trlrov8dj031', '2015-11-19', 'AM'),
(379, 'FTTP-8399', '112.196.136.78', '2015-11-19 06:52:24am', '', 'ej0r0hs20dt9mg11j19h5c9464', '2015-11-19', 'TM'),
(380, 'FTTP-8399', '112.196.136.78', '2015-11-19 07:30:42am', '', '0d0f2iqsffp13votlb6v4mg5c2', '2015-11-19', 'TM'),
(381, 'FTTP-8399', '112.196.136.78', '2015-11-19 07:31:59am', '', 'i2iffn74gkfapdn98jdrvppho6', '2015-11-19', 'TM'),
(382, 'admin', '112.196.136.78', '2015-11-19 07:39:25am', '', 'cichfsdj7b4rn5rsk6dlevklt7', '2015-11-19', 'AD'),
(383, 'admin', '112.196.136.78', '2015-11-19 08:27:52am', '2015-11-19 08:39:53am', 'd95scbsigeoba8s31s29r0gr07', '2015-11-19', 'AD'),
(384, 'FXIO-8905', '112.196.136.78', '2015-11-19 08:28:19am', '', 't49akj8e450t5ar389lh1qbc26', '2015-11-19', 'AM'),
(385, 'admin', '112.196.136.78', '2015-11-19 08:39:56am', '2015-11-19 08:40:07am', 'cp9dgte8r73rtf9suh6meiocc7', '2015-11-19', 'AD'),
(386, 'FXIO-8905', '112.196.136.78', '2015-11-19 08:40:14am', '2015-11-19 08:43:59am', 'd918pe4apnggqsm6hks8tojal4', '2015-11-19', 'AM'),
(387, 'admin', '112.196.136.78', '2015-11-19 08:44:02am', '2015-11-19 08:50:08am', '5g6sj3bk4ac9kl0fs5dmdanq42', '2015-11-19', 'AD'),
(388, 'FXIO-8905', '112.196.136.78', '2015-11-19 08:50:19am', '', 'jnjfiasb9da1g70hoad6dch7b5', '2015-11-19', 'AM'),
(389, 'admin', '66.49.237.16', '2015-11-19 15:58:53pm', '', 'nsb02t877lq967810j72l1hah7', '2015-11-19', 'AD'),
(390, 'admin', '66.49.237.16', '2015-11-19 16:02:11pm', '', 'ero1624mt3pgnmpvugt2ac8o32', '2015-11-19', 'AD'),
(391, 'FXIO-8905', '66.49.237.16', '2015-11-19 16:03:44pm', '', 'cbimgufa47qk74upafimimve75', '2015-11-19', 'AM'),
(392, 'admin', '112.196.136.78', '2015-11-19 23:58:30pm', '', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-19', 'AD'),
(393, 'admin', '112.196.136.78', '2015-11-20 00:39:09am', '', 'fkel2dnuiiknfjdi9015mm8s74', '2015-11-20', 'AD'),
(394, 'admin', '104.236.57.54', '2015-11-20 01:00:26am', '2015-11-20 03:11:54am', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', 'AD'),
(395, 'FXIO-8905', '104.236.57.54', '2015-11-20 03:12:07am', '', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', 'AM'),
(396, 'ZJBX-0533', '112.196.136.78', '2015-11-20 03:32:20am', '', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', 'QF'),
(397, 'admin', '112.196.136.78', '2015-11-20 04:46:27am', '2015-11-20 04:47:47am', 'q8ga2qau1pjmior8k2vjbcjj25', '2015-11-20', 'AD'),
(398, 'admin', '112.196.136.78', '2015-11-20 04:47:52am', '2015-11-20 04:47:58am', 'obc5m3hf5a1oj11skv0ho81l60', '2015-11-20', 'AD'),
(399, 'VUQQ-3341', '112.196.136.78', '2015-11-20 04:48:12am', '', '0rolqskp80s7p42muhofmm8q74', '2015-11-20', 'AM'),
(400, 'admin', '66.49.237.16', '2015-11-20 15:08:55pm', '', 'nji2f6gihijeih9gdrhg79fbv0', '2015-11-20', 'AD'),
(401, 'FXIO-8905', '66.49.237.16', '2015-11-20 15:09:54pm', '2015-11-20 15:10:16pm', 'hbje6n6dtsg814qttk6ijpfgh7', '2015-11-20', 'AM'),
(402, 'ZJBX-0533', '66.49.237.16', '2015-11-20 15:10:22pm', '2015-11-20 15:49:37pm', 'crtb5qgpvm1p1k61quqn2t7bq4', '2015-11-20', 'QF'),
(403, 'FXIO-8905', '66.49.237.16', '2015-11-20 15:49:47pm', '', 'lp0hpe8hjc3i7vuvs806po9g92', '2015-11-20', 'AM'),
(404, 'admin', '112.196.136.78', '2015-11-22 23:22:12pm', '', 'ksppokhtaegvaac74k27m84i71', '2015-11-22', 'AD'),
(405, 'FXIO-8905', '112.196.136.78', '2015-11-22 23:55:04pm', '2015-11-23 00:00:11am', 'f7he2c89v4fg7j3o43pb851hp6', '2015-11-22', 'AM'),
(406, 'FTTP-8399', '104.131.161.230', '2015-11-22 23:57:54pm', '2015-11-23 00:56:18am', '88h7lms1tgirki1mmu2goc3pj6', '2015-11-22', 'TM'),
(407, 'VUQQ-3341', '112.196.136.78', '2015-11-23 00:00:21am', '2015-11-23 04:51:46am', '69rpagoi8debbep5hlm6jrdvo6', '2015-11-23', 'AM'),
(408, 'admin', '112.196.136.78', '2015-11-23 00:10:13am', '', 'ic9vth7tkn2vg2f8rdt5aqpvp5', '2015-11-23', 'AD');
INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`) VALUES
(409, 'FMVI-7264', '112.196.136.78', '2015-11-23 00:11:09am', '', 'te8rrqsriojtunehvb58n7kc11', '2015-11-23', 'AM'),
(410, 'FXIO-8905', '112.196.136.78', '2015-11-23 01:01:08am', '', 'ekn0m4otq7b1ca22r9l0fa8tj0', '2015-11-23', 'AM'),
(411, 'admin', '112.196.136.78', '2015-11-23 01:14:04am', '', 'en885o1ugpqfrmlgqkitvv3tt5', '2015-11-23', 'AD'),
(412, 'ZJBX-0533', '104.131.161.230', '2015-11-23 01:31:34am', '', 'soiila8v3d4ur7iosro0lgh5m4', '2015-11-23', 'QF'),
(413, 'FXIO-8905', '112.196.136.78', '2015-11-23 02:14:13am', '', '8tirqlv322msde0k2vlv99ftf4', '2015-11-23', 'AM'),
(414, 'RAUU-1599', '112.196.136.78', '2015-11-23 02:14:29am', '', '7odksbacnpr4plnhh3a4ao3n15', '2015-11-23', 'AM'),
(415, 'ZJBX-0533', '112.196.136.78', '2015-11-23 03:34:13am', '', 'ucqjhqq4m0kf3dcduu81v7i2u5', '2015-11-23', 'QF'),
(416, 'FMVI-7264', '112.196.136.78', '2015-11-23 03:54:50am', '', 'vrd8p3ksq3rpdaa35d6vtd36i4', '2015-11-23', 'AM'),
(417, 'FXIO-8905', '112.196.136.78', '2015-11-23 04:52:06am', '', 'su6ds7otpfqqjsd8tpqmleeib2', '2015-11-23', 'AM'),
(418, 'FMVI-7264', '112.196.136.78', '2015-11-23 08:11:03am', '2015-11-23 08:11:19am', 'ej413h15k3mvajjm9t32npr7i3', '2015-11-23', 'AM'),
(419, 'FXIO-8905', '112.196.136.78', '2015-11-23 08:11:43am', '', '9o0mtl6s2frahaln043qb5ii27', '2015-11-23', 'AM'),
(420, 'admin', '66.49.237.16', '2015-11-24 14:07:36pm', '', '2kr6vhmck8j1mvn1n51srumah5', '2015-11-24', 'AD'),
(421, 'admin', '112.196.136.78', '2015-11-25 01:32:52am', '', '8foe99vc4uvil1b7ngrm1bvvn1', '2015-11-25', 'AD'),
(422, 'FMVI-7264', '112.196.136.44', '2015-11-26 07:59:09am', '2015-11-26 07:59:34am', '6ci6brmlcgtjt3ncf9dq636do3', '2015-11-26', 'AM'),
(423, 'admin', '112.196.136.44', '2015-11-26 07:59:45am', '', 'odvfkqig37ns3h9k7vbllb72l0', '2015-11-26', 'AD'),
(424, 'admin', '66.49.157.92', '2015-11-26 08:00:18am', '', 'h385t9ddh9nbjlfftj96ti05h7', '2015-11-26', 'AD'),
(425, 'admin', '112.196.136.44', '2015-11-27 02:07:00am', '2015-11-27 02:28:30am', 'k7fhhjc3871dl6ertbk897kjo4', '2015-11-27', 'AD'),
(426, 'admin', '112.196.136.78', '2015-11-27 02:25:28am', '2015-11-27 03:49:43am', 'c3upht443lc43kvegqi9mf0kr2', '2015-11-27', 'AD'),
(427, 'admin', '112.196.136.44', '2015-11-27 03:17:18am', '2015-11-27 03:18:05am', 'fbtcj7bm5047cf1s0ngrfb1370', '2015-11-27', 'AD'),
(428, 'admin', '112.196.136.44', '2015-11-27 03:18:38am', '2015-11-27 03:48:47am', 'ubt0ltuu4g14916kgh2rr0n610', '2015-11-27', 'AD'),
(429, 'admin', '112.196.136.78', '2015-11-27 03:58:25am', '2015-11-27 03:58:51am', 'psuiunu16fgk2t4058504ssc34', '2015-11-27', 'AD'),
(430, 'admin', '112.196.136.44', '2015-11-27 04:06:00am', '', 'nperir5i6ebk3s1j0d6obc9fh2', '2015-11-27', 'AD'),
(431, 'admin', '112.196.136.44', '2015-11-27 04:06:48am', '', '2s0h9edchi3imq49ch33qplkl7', '2015-11-27', 'AD'),
(432, 'admin', '112.196.136.44', '2015-11-27 04:15:49am', '', '0bl75ff7phcrkcg79krhj7opi5', '2015-11-27', 'AD'),
(433, 'admin', '14.98.12.253', '2015-11-27 07:48:44am', '', '3ejds2gjatb9665o71df2g2tt5', '2015-11-27', 'AD'),
(434, 'admin', '66.49.237.16', '2015-11-27 07:49:41am', '2015-11-27 07:55:50am', 'h4imh299sv9r5e83lssark8s47', '2015-11-27', 'AD'),
(435, 'admin', '112.196.136.78', '2015-11-27 07:52:24am', '', 'al7o5dfn1ujlo43l4p4lnnrl47', '2015-11-27', 'AD'),
(436, 'admin', '66.49.237.16', '2015-11-27 07:56:01am', '2015-11-27 07:56:42am', 'dapi4ojssfoa3e8t54t5bnc027', '2015-11-27', 'AD'),
(437, 'admin', '14.98.12.253', '2015-11-27 08:08:11am', '', 'nntpk8bdgng4mpi36qcpreeqv2', '2015-11-27', 'AD'),
(438, 'admin', '14.98.12.253', '2015-11-27 08:10:45am', '2015-11-27 08:11:59am', 'rteme37ohkqdn59nt1a9760606', '2015-11-27', 'AD'),
(439, 'admin', '14.98.12.253', '2015-11-27 08:12:04am', '', '16rbrlig71pqkso5pfadtn2tb7', '2015-11-27', 'AD'),
(440, 'admin', '59.161.69.157', '2015-11-27 08:50:08am', '', 'i006pa89jis11cnp7icf45pn11', '2015-11-27', 'AD'),
(441, 'admin', '112.196.136.44', '2015-11-28 05:21:36am', '', 'f6s1mkcbac4mupjpdomde9fhe4', '2015-11-28', 'AD'),
(442, 'admin', '112.196.136.78', '2015-11-29 22:52:40pm', '2015-11-29 23:11:16pm', 'omv4v7j72ckofncdncoppuc3h0', '2015-11-29', 'AD'),
(443, 'admin', '112.196.136.44', '2015-11-29 23:12:11pm', '2015-11-29 23:36:15pm', 'm3t2sp2438beo94ssfj2h32p20', '2015-11-29', 'AD'),
(444, 'admin', '112.196.136.78', '2015-11-29 23:12:16pm', '2015-11-29 23:12:39pm', 'jd3uk297ntn93u1hf45ev9s5i6', '2015-11-29', 'AD'),
(445, 'admin', '112.196.136.78', '2015-11-29 23:15:35pm', '2015-11-29 23:32:23pm', 'apedtv53jjd4t3cj0eigp9eh42', '2015-11-29', 'AD'),
(446, 'admin', '112.196.136.78', '2015-11-29 23:27:51pm', '', '14up6es8okg63tjfshar02r143', '2015-11-29', 'AD'),
(447, 'admin', '112.196.136.78', '2015-11-29 23:32:50pm', '2015-11-29 23:33:02pm', 'sac42is08mv83i03f6kjofsnp2', '2015-11-29', 'AD'),
(448, 'admin', '112.196.136.78', '2015-11-29 23:37:06pm', '', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-29', 'AD'),
(449, 'admin', '112.196.136.44', '2015-11-29 23:39:30pm', '2015-11-30 00:29:03am', 'k1ulf7cisgdm8jidv76ra1v7m3', '2015-11-29', 'AD'),
(450, 'admin', '112.196.136.78', '2015-11-30 00:01:56am', '2015-11-30 07:13:51am', 'v26vo4bgddh31v1eokhi37i0t4', '2015-11-30', 'AD'),
(451, 'FMVI-7264', '112.196.136.44', '2015-11-30 00:29:37am', '2015-11-30 00:37:52am', 'i6pkilridah7uelia8ufvtdfn0', '2015-11-30', 'AM'),
(452, 'admin', '112.196.136.44', '2015-11-30 00:38:09am', '2015-11-30 00:43:31am', 'fushfoti7glf3idr80f15jsj53', '2015-11-30', 'AD'),
(453, 'admin', '112.196.136.78', '2015-11-30 00:39:28am', '', 'q1oj5kgkicj03a5s6qe3p546n3', '2015-11-30', 'AD'),
(454, 'FMVI-7264', '112.196.136.78', '2015-11-30 00:41:36am', '', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', 'AM'),
(455, 'FMVI-7264', '112.196.136.44', '2015-11-30 00:48:56am', '2015-11-30 00:53:27am', '7l42u9eoamgcakafnirq2mqpr0', '2015-11-30', 'AM'),
(456, 'admin', '112.196.136.44', '2015-11-30 00:54:00am', '2015-11-30 01:03:47am', 't782qmm2ofcf6n7f41j5jqj585', '2015-11-30', 'AD'),
(457, 'FMVI-7264', '112.196.136.44', '2015-11-30 01:04:22am', '2015-11-30 01:33:15am', '3r44q5g634233dk08f10q8qpl1', '2015-11-30', 'AM'),
(458, 'admin', '112.196.136.78', '2015-11-30 01:04:32am', '', 'mv8qddnnv8ru4vb55ivh951pt6', '2015-11-30', 'AD'),
(459, 'FMVI-7264', '112.196.136.44', '2015-11-30 01:40:01am', '', '0cmccb0oc7ne5ef21gb37h3sb3', '2015-11-30', 'AM'),
(460, 'FMVI-7264', '112.196.136.78', '2015-11-30 01:56:52am', '2015-11-30 05:29:54am', 'q6a802bks3mjm00p9j7inna1e2', '2015-11-30', 'AM'),
(461, 'admin', '14.98.100.29', '2015-11-30 01:57:51am', '', 'fb8tsor8a7avtokbrehiltl000', '2015-11-30', 'AD'),
(462, 'admin', '112.196.136.78', '2015-11-30 03:11:43am', '', 'kafp215khju891ju6rtbo1r0l7', '2015-11-30', 'AD'),
(463, 'FMVI-7264', '112.196.136.44', '2015-11-30 03:15:30am', '2015-11-30 03:37:38am', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', 'AM'),
(464, 'admin', '112.196.136.44', '2015-11-30 03:37:44am', '2015-11-30 03:44:29am', 'qb2674n1q8k4107pb81vfaid45', '2015-11-30', 'AD'),
(465, 'admin', '112.196.136.44', '2015-11-30 03:44:37am', '', '10b57bgekun0tts2f00t4l0707', '2015-11-30', 'AD'),
(466, 'admin', '112.196.136.44', '2015-11-30 03:44:40am', '2015-11-30 03:59:54am', '3vif8na3np3b1svs6u8hm5pdh0', '2015-11-30', 'AD'),
(467, 'admin', '112.196.136.44', '2015-11-30 03:50:32am', '', '3ogk0o60l3ietluqcm2hqit9g3', '2015-11-30', 'AD'),
(468, 'admin', '112.196.136.44', '2015-11-30 03:54:04am', '2015-11-30 03:58:05am', 'ppp34n3s88n27f774cdev1u394', '2015-11-30', 'AD'),
(469, 'admin', '112.196.136.44', '2015-11-30 03:59:47am', '', 'dejvljmodgirhqtiqrnph0qdl5', '2015-11-30', 'AD'),
(470, 'FMVI-7264', '112.196.136.44', '2015-11-30 04:00:01am', '2015-11-30 05:11:49am', 'k1ek1n6cbo69ac886p7apfulf1', '2015-11-30', 'AM'),
(471, 'admin', '112.196.136.44', '2015-11-30 04:24:40am', '', 'hoq8ls72qukelkfq0arlu6pne2', '2015-11-30', 'AD'),
(472, 'admin', '112.196.136.44', '2015-11-30 04:27:09am', '', 'ekg2hnb3va7e3jr4f6nr4biaf5', '2015-11-30', 'AD'),
(473, 'FTTP-8399', '112.196.136.44', '2015-11-30 05:12:24am', '', '3jppqgtf6o4826f76nasm9amb3', '2015-11-30', 'TM'),
(474, 'admin', '112.196.136.44', '2015-11-30 05:14:37am', '', 'quujsft0bq7ernt7pimd40st23', '2015-11-30', 'AD'),
(475, 'FTTP-8399', '112.196.136.78', '2015-11-30 05:14:43am', '2015-11-30 06:49:36am', 'eurba15s4ptcspfn7f9uu8vc20', '2015-11-30', 'TM'),
(476, 'FTTP-8399', '112.196.136.78', '2015-11-30 05:30:28am', '', 'id3tcobt143spo3od62g5prje2', '2015-11-30', 'TM'),
(477, 'admin', '112.196.136.44', '2015-11-30 05:54:35am', '', '2m9dsempgvgb750mj07v185fl3', '2015-11-30', 'AD'),
(478, 'FTTP-8399', '112.196.136.44', '2015-11-30 05:55:24am', '2015-11-30 06:06:50am', 'ekoufk75qv0g3dq7f4cce010u7', '2015-11-30', 'TM'),
(479, 'FMVI-7264', '112.196.136.44', '2015-11-30 06:07:04am', '2015-11-30 06:37:12am', 'ppndq1ih4fvco4ih3gjc5f8a65', '2015-11-30', 'AM'),
(480, 'admin', '112.196.136.44', '2015-11-30 06:17:18am', '', 'jecjb32tkb1h2178ef4ncmd2v6', '2015-11-30', 'AD'),
(481, 'admin', '112.196.136.44', '2015-11-30 06:25:21am', '', '9lq3detemcjhajqc7rtgf3aep2', '2015-11-30', 'AD'),
(482, 'FTTP-8399', '112.196.136.44', '2015-11-30 06:37:20am', '2015-11-30 06:42:23am', '3i86tntuv50bbp5unc5ui0f3p5', '2015-11-30', 'TM'),
(483, 'ZJBX-0533', '112.196.136.44', '2015-11-30 06:48:09am', '2015-11-30 06:48:28am', '3hr4mq21r0j3qjgul975l479q1', '2015-11-30', 'QF'),
(484, 'ZJBX-0533', '112.196.136.44', '2015-11-30 06:48:31am', '2015-11-30 07:27:55am', 'mpma262jva7hrrmtegpa059gi2', '2015-11-30', 'QF'),
(485, 'ZJBX-0533', '112.196.136.78', '2015-11-30 06:49:45am', '2015-11-30 07:14:55am', 'dvr3injeb8jna7d5qdekjoh487', '2015-11-30', 'QF'),
(486, 'FTTP-8399', '112.196.136.78', '2015-11-30 06:50:11am', '', '9nrpn4jgem3i9ipta3mnkivm02', '2015-11-30', 'TM'),
(487, 'FMVI-7264', '112.196.136.78', '2015-11-30 07:14:22am', '', '3m3kv27fl39k675niq5g9r8541', '2015-11-30', 'AM'),
(488, 'FTTP-8399', '112.196.136.78', '2015-11-30 07:19:30am', '2015-11-30 07:20:08am', 'v8sbg7e3ojsfo381vjn2hkn025', '2015-11-30', 'TM'),
(489, 'FMVI-7264', '112.196.136.44', '2015-11-30 07:28:04am', '2015-11-30 07:28:34am', '7n83l1u88v7o1napcn8b3q3i92', '2015-11-30', 'AM'),
(490, 'admin', '112.196.136.44', '2015-11-30 07:28:47am', '', 'pcm6eoen26uk7cccafj4i9pdd4', '2015-11-30', 'AD'),
(491, 'admin', '112.196.136.44', '2015-11-30 07:46:52am', '', 'audhrhknlsa4holft2ei33nq95', '2015-11-30', 'AD'),
(492, 'admin', '66.49.237.16', '2015-11-30 09:00:22am', '', 'dduv45or67ndp30m0igkc6qr04', '2015-11-30', 'AD'),
(493, 'admin', '66.49.237.16', '2015-11-30 13:42:27pm', '', 'dkm5rb1lr3u8tkm7co8ll7l891', '2015-11-30', 'AD'),
(494, 'admin', '112.196.136.44', '2015-11-30 23:20:46pm', '', 'h2ke4u8at6nfmpcun3orfhs9m1', '2015-11-30', 'AD'),
(495, 'admin', '112.196.136.44', '2015-11-30 23:41:21pm', '', 'skj04bv5qs1pva4uf1ssqcs320', '2015-11-30', 'AD'),
(496, 'admin', '112.196.136.78', '2015-11-30 23:46:21pm', '', 'ktp68dfmnmcitu7g699evsajc7', '2015-11-30', 'AD'),
(497, 'admin', '112.196.136.78', '2015-12-01 00:00:47am', '', '0e10s9votphl41prtnrkuqrlp7', '2015-12-01', 'AD'),
(498, 'FTTP-8399', '112.196.136.78', '2015-12-01 00:01:32am', '', 'fd5nnia17hlcl8488250beqdq5', '2015-12-01', 'TM'),
(499, 'FMVI-7264', '112.196.136.78', '2015-12-01 00:03:12am', '', 'sr38sit5b1qlhrikmv2shf62u6', '2015-12-01', 'AM'),
(500, 'ZJBX-0533', '112.196.136.78', '2015-12-01 00:15:59am', '', '2s7r9kf669r6lsjmc301u4i3g6', '2015-12-01', 'QF'),
(501, 'FXIO-8905', '112.196.136.78', '2015-12-01 00:30:29am', '', 'tv8dnbl0hhi1qfb1muis6ld1b6', '2015-12-01', 'AM'),
(502, 'admin', '112.196.136.78', '2015-12-01 00:49:34am', '', 'lj65hsqdu9jrca54es5ophchv2', '2015-12-01', 'AD'),
(503, 'admin', '112.196.136.78', '2015-12-01 00:58:35am', '2015-12-01 07:01:52am', 'jfoiq6ueci8dkt12v0kkqbk1s2', '2015-12-01', 'AD'),
(504, 'admin', '112.196.136.44', '2015-12-01 01:38:17am', '', '415v7j6uavb7p79nmnh67lcve1', '2015-12-01', 'AD'),
(505, 'FMVI-7264', '112.196.136.44', '2015-12-01 01:40:03am', '', 'r40sglna17g4rrjk2cb7vngou6', '2015-12-01', 'AM'),
(506, 'FTTP-8399', '112.196.136.78', '2015-12-01 04:10:41am', '', 're412d4ec4nv0u5oc6dlh587a0', '2015-12-01', 'TM'),
(507, 'admin', '112.196.136.44', '2015-12-01 04:29:16am', '', 'ejl6nqnm01a93dumea767dcfs5', '2015-12-01', 'AD'),
(508, 'admin', '112.196.136.78', '2015-12-01 04:52:49am', '', 'cv4utotdu8m10onrneksv198v6', '2015-12-01', 'AD'),
(509, 'FMVI-7264', '112.196.136.78', '2015-12-01 07:02:19am', '', 'tdua5nunl38ooo7h636pdsnr72', '2015-12-01', 'AM'),
(510, 'admin', '112.196.136.78', '2015-12-01 23:33:15pm', '2015-12-02 02:22:07am', 'iqqvmieufejnl5kkvk29qgjlr6', '2015-12-01', 'AD'),
(511, 'RAUU-1599', '112.196.136.78', '2015-12-01 23:38:35pm', '', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-01', 'AM'),
(512, 'ZJBX-0533', '112.196.136.78', '2015-12-02 02:09:14am', '2015-12-02 05:21:01am', '1msn9f8tpp7vfhtru9nb6sgfh1', '2015-12-02', 'QF'),
(513, 'FTTP-8399', '112.196.136.78', '2015-12-02 02:14:51am', '', 'ou66pi7qjs2vd99c6j1p0k9jd3', '2015-12-02', 'TM'),
(514, 'FMVI-7264', '112.196.136.78', '2015-12-02 02:16:13am', '', 'gl8cf67177e2drtmhpg2gialq2', '2015-12-02', 'AM'),
(515, 'ZJBX-0533', '112.196.136.78', '2015-12-02 02:22:14am', '2015-12-02 05:24:44am', 's6usil5vigttecqu3rquchmc23', '2015-12-02', 'QF'),
(516, 'admin', '112.196.136.78', '2015-12-02 05:21:26am', '', '0nn076gp7fbublja6d4gkm1hv6', '2015-12-02', 'AD'),
(517, 'admin', '112.196.136.78', '2015-12-02 05:24:49am', '2015-12-02 05:32:41am', 'f6ah80t5eeojbpf8pdi2n63tp5', '2015-12-02', 'AD'),
(518, 'ZJBX-0533', '112.196.136.78', '2015-12-02 05:32:44am', '2015-12-02 06:06:05am', 'j6ra0i72efjpdnacq83lnchog7', '2015-12-02', 'QF'),
(519, 'admin', '112.196.136.78', '2015-12-02 06:06:09am', '2015-12-02 06:22:27am', 'sjggm9i8bcndl7ugn7a21vr101', '2015-12-02', 'AD'),
(520, 'ZJBX-0533', '112.196.136.78', '2015-12-02 06:22:31am', '2015-12-02 07:22:42am', 'qiq1o1tiuoqarqliuomju5e3q7', '2015-12-02', 'QF'),
(521, 'admin', '1.23.147.56', '2015-12-02 06:36:16am', '2015-12-02 06:39:15am', 'ci1728dn2pi19010t4fstirde6', '2015-12-02', 'AD'),
(522, 'admin', '112.196.136.78', '2015-12-02 07:22:46am', '', 'fif6gvlukksj8ir1il8nfudr42', '2015-12-02', 'AD'),
(523, 'admin', '112.196.136.78', '2015-12-02 07:50:07am', '', '7qrcnna61l6e9i4stc936ejkh1', '2015-12-02', 'AD'),
(524, 'admin', '66.49.237.16', '2015-12-02 13:45:32pm', '2015-12-02 13:45:40pm', 'm0p12r4gv3n4k29t9f6baijiv3', '2015-12-02', 'AD'),
(525, 'FXIO-8905', '66.49.237.16', '2015-12-02 13:45:43pm', '', '0lqev9caj7k4ogqk0rq0ah4c34', '2015-12-02', 'AM'),
(526, 'admin', '112.196.136.78', '2015-12-02 23:06:24pm', '2015-12-03 07:52:56am', '6gcdp7q45uqub0tf6tcrb66731', '2015-12-02', 'AD'),
(527, 'FMVI-7264', '112.196.136.78', '2015-12-02 23:08:47pm', '2015-12-03 08:01:32am', 'ngl0omakfkebbapvudl1v1doj0', '2015-12-02', 'AM'),
(528, 'FXIO-8905', '112.196.136.78', '2015-12-02 23:11:16pm', '2015-12-03 00:47:39am', 'qo748s234nte8s3h95j2vttac4', '2015-12-02', 'AM'),
(529, 'admin', '112.196.136.78', '2015-12-02 23:34:41pm', '', 'c02jgt35tlacklpp133f1rimi7', '2015-12-02', 'AD'),
(530, 'FTTP-8399', '112.196.136.78', '2015-12-03 00:48:20am', '2015-12-03 01:59:07am', 'hbfnmh9774tetijaimge1lr2q1', '2015-12-03', 'TM'),
(531, 'FXIO-8905', '112.196.136.78', '2015-12-03 01:59:16am', '2015-12-03 07:05:39am', 'ibe12lkbiegl4r4e6c054169k4', '2015-12-03', 'AM'),
(532, 'admin', '112.196.136.78', '2015-12-03 02:01:52am', '', 'jq8tn3bbs9p7lbe5bh7ll561j7', '2015-12-03', 'AD'),
(533, 'admin', '112.196.136.78', '2015-12-03 03:39:50am', '', '2nt8isql08huklolklr40l7db2', '2015-12-03', 'AD'),
(534, 'admin', '112.196.136.78', '2015-12-03 04:48:02am', '', '8kj2bqpnkgd51ho830j99t7la3', '2015-12-03', 'AD'),
(535, 'admin', '112.196.136.78', '2015-12-03 04:53:22am', '', '74ank0k3u67so1btb53j9jfh52', '2015-12-03', 'AD'),
(536, 'admin', '112.196.136.78', '2015-12-03 05:03:07am', '', 'gdpld5g7uichp84moofstv7g23', '2015-12-03', 'AD'),
(537, 'admin', '112.196.136.78', '2015-12-03 07:06:00am', '', '0gs6fqgipgon8aoq0pq4ibvu86', '2015-12-03', 'AD'),
(538, 'admin', '112.196.136.78', '2015-12-03 07:57:48am', '', 'grclihkvo8orbj3fs2g5j8l065', '2015-12-03', 'AD'),
(539, 'admin', '112.196.136.78', '2015-12-03 08:01:39am', '', '806bdcbd3j8kjcuk09smfl12v0', '2015-12-03', 'AD'),
(540, 'admin', '112.196.136.78', '2015-12-03 08:07:32am', '', 'ht51fcs967bulte2im8l1u19s3', '2015-12-03', 'AD'),
(541, 'admin', '66.49.237.16', '2015-12-03 08:45:44am', '', 'vqvtb136tir3q8st8ejlfkrq72', '2015-12-03', 'AD'),
(542, 'admin', '66.49.237.16', '2015-12-03 08:46:35am', '', '90h9jsn16jbgqkrcgukjb0u7f7', '2015-12-03', 'AD'),
(543, 'admin', '112.196.136.78', '2015-12-03 23:34:54pm', '2015-12-04 04:53:35am', 'jpndp296bhs8kujg5iv9frvl30', '2015-12-03', 'AD'),
(544, 'admin', '112.196.136.78', '2015-12-04 01:14:38am', '', '2d6dc2n3va0vlgtd7oesup9986', '2015-12-04', 'AD'),
(545, 'RAUU-1599', '112.196.136.78', '2015-12-04 01:15:48am', '', 'krc5oversqe662012ag5kkmo62', '2015-12-04', 'AM'),
(546, 'admin', '112.196.136.78', '2015-12-04 01:18:56am', '', '3vhqll018iipdqvd0mq038i8j0', '2015-12-04', 'AD'),
(547, 'admin', '112.196.136.78', '2015-12-04 01:19:28am', '', '9eh1jp9irk2gqf33k648bm2ra7', '2015-12-04', 'AD'),
(548, 'admin', '112.196.136.78', '2015-12-04 02:27:46am', '', 'c7v2gp7vvgbe7gu87fh3p9enh5', '2015-12-04', 'AD'),
(549, 'admin', '112.196.136.44', '2015-12-04 03:20:02am', '', '040domab8pbprlpiiffgj8hlg0', '2015-12-04', 'AD'),
(550, 'admin', '112.196.136.44', '2015-12-04 03:30:58am', '', 'h29ue1qcla9b1l8u45s6mud8n6', '2015-12-04', 'AD'),
(551, 'admin', '112.196.136.44', '2015-12-04 03:32:31am', '', 'apai8l5g7nc4m4sqsiu7q9du37', '2015-12-04', 'AD'),
(552, 'admin', '112.196.136.78', '2015-12-04 03:35:53am', '', 'n53p1htc7am4gvjtp4ua4v8u03', '2015-12-04', 'AD'),
(553, 'admin', '112.196.136.78', '2015-12-04 03:38:59am', '', 'usf1d4ab2vargelfbi12u4fe52', '2015-12-04', 'AD'),
(554, 'admin', '112.196.136.44', '2015-12-04 04:39:48am', '', '4kp4r5ojt6s8fjeefvrkqtfiv3', '2015-12-04', 'AD'),
(555, 'admin', '112.196.136.78', '2015-12-04 04:55:43am', '', 'rnipou756rg0jiq5jpbd8bcqk6', '2015-12-04', 'AD'),
(556, 'admin', '112.196.136.78', '2015-12-04 06:55:45am', '', 'ammi1do12o4s2e7kqrfcu3srj1', '2015-12-04', 'AD'),
(557, 'admin', '112.196.136.78', '2015-12-04 07:01:16am', '', 'l9u39se11s95e9fvp49ke8i020', '2015-12-04', 'AD'),
(558, 'admin', '112.196.136.78', '2015-12-04 07:03:22am', '', 'nl4474lvfj8tkviu53u93kgm55', '2015-12-04', 'AD'),
(559, 'admin', '112.196.136.78', '2015-12-04 07:06:15am', '', '90kobqtp0640rn1mv6inot0un3', '2015-12-04', 'AD'),
(560, 'admin', '66.49.237.16', '2015-12-04 11:16:43am', '', 'q70gqp0usic6mmmcngj6o35210', '2015-12-04', 'AD'),
(561, 'admin', '112.196.136.78', '2015-12-04 22:45:52pm', '2015-12-05 01:04:39am', 'pl467uq1lht1kgl91lbhdsaug0', '2015-12-04', 'AD'),
(562, 'admin', '112.196.136.78', '2015-12-05 01:04:42am', '2015-12-05 01:45:27am', 'vv9hpmvviogfuvuje0ae1dlnm4', '2015-12-05', 'AD'),
(563, 'admin', '112.196.136.78', '2015-12-05 01:45:31am', '2015-12-05 04:35:34am', 'qgl96u6lkj03joccms71uo6tp3', '2015-12-05', 'AD'),
(564, 'admin', '112.196.136.78', '2015-12-05 04:35:38am', '2015-12-05 04:54:55am', 'lk8k61g0jk7q9vkqjkod772vk0', '2015-12-05', 'AD'),
(565, 'admin', '112.196.136.44', '2015-12-05 04:54:59am', '', '2umtstpt4vre1uvqldfbgrfem0', '2015-12-05', 'AD'),
(566, 'admin', '112.196.136.78', '2015-12-07 06:43:42am', '', 'o10bs9foru3urbau6nabip2bh4', '2015-12-07', 'AD'),
(567, 'admin', '112.196.136.78', '2015-12-07 07:09:16am', '', 'joiha1vvjbil8dtuhhocdmuu37', '2015-12-07', 'AD'),
(568, 'admin', '112.196.136.78', '2015-12-07 07:11:18am', '', 'm37lq65h1oau58q66j3c3179p5', '2015-12-07', 'AD'),
(569, 'admin', '112.196.136.78', '2015-12-07 07:23:59am', '', 'ro28rdsak80j92tjhe9leg6ah2', '2015-12-07', 'AD'),
(570, 'admin', '112.196.136.78', '2015-12-07 07:25:19am', '2015-12-07 07:29:11am', 'qgifun6j82df3b63f2d69r98m7', '2015-12-07', 'AD'),
(571, 'admin', '112.196.136.78', '2015-12-07 07:33:28am', '2015-12-07 07:38:00am', 'lp2pnl8ecnu8lhh86b8ec46q90', '2015-12-07', 'AD'),
(572, 'admin', '112.196.136.78', '2015-12-07 08:38:19am', '', '6pbuook4p7aln7j7401m0t8a96', '2015-12-07', 'AD'),
(573, 'admin', '112.196.136.78', '2015-12-07 08:42:59am', '', 'v90jriqnc0lolf0ocaop431ae7', '2015-12-07', 'AD'),
(574, 'admin', '112.196.136.78', '2015-12-10 23:33:56pm', '2015-12-10 23:34:30pm', 'knpffnmi49tibiig2uunv402v6', '2015-12-10', 'AD'),
(575, 'admin', '112.196.136.78', '2015-12-10 23:34:57pm', '', 'a7pkqc4iuvceq31rgk7b8lron4', '2015-12-10', 'AD'),
(576, 'admin', '112.196.136.78', '2015-12-11 00:53:35am', '', 'trhaqmk21d7ut75j2p00t5nm20', '2015-12-11', 'AD'),
(577, 'admin', '112.196.136.78', '2015-12-11 05:11:15am', '', '2ndesa9fuart74nph22m9kq8b7', '2015-12-11', 'AD'),
(578, 'admin', '112.196.136.78', '2015-12-11 05:39:21am', '2015-12-11 05:39:43am', '242gon00shqu0es6b9rso9c5e7', '2015-12-11', 'AD'),
(579, 'admin', '112.196.136.78', '2015-12-12 05:25:47am', '', 'bvddo72nlaogpv6u4cb8m6j7i3', '2015-12-12', 'AD'),
(580, 'admin', '112.196.136.78', '2015-12-14 07:11:58am', '2015-12-14 07:12:05am', 'm7jfj5lb8tnck1ke5sem2n12i5', '2015-12-14', 'AD'),
(581, 'admin', '112.196.136.78', '2015-12-14 07:12:27am', '2015-12-14 07:13:16am', '79obctf8v3mb56i17qs9nu7ht2', '2015-12-14', 'AD'),
(582, 'admin', '112.196.136.78', '2015-12-14 23:10:43pm', '2015-12-14 23:11:13pm', 'qcli65jp4urukrdso04jk2l526', '2015-12-14', 'AD'),
(583, 'admin', '66.49.237.16', '2015-12-15 13:44:35pm', '2015-12-15 14:19:32pm', 'ri0651r213n786f924kv6526i3', '2015-12-15', 'AD'),
(584, 'admin', '66.49.237.16', '2015-12-15 14:19:39pm', '', 'e07qj4frsvk2n9r09aqp7jn1c7', '2015-12-15', 'AD'),
(585, 'admin', '66.49.237.16', '2015-12-15 14:28:47pm', '', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', 'AD'),
(586, 'admin', '66.49.237.16', '2015-12-15 14:54:12pm', '', 'up7tgkg98kohmmevsgsv9gnl02', '2015-12-15', 'AD'),
(587, 'admin', '66.49.237.16', '2015-12-15 15:12:10pm', '', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', 'AD'),
(588, 'admin', '66.49.237.16', '2015-12-15 16:35:16pm', '', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', 'AD'),
(589, 'admin', '112.196.136.78', '2015-12-16 00:25:08am', '', 'cpa9gl0posoi7ro0hr012p8bv3', '2015-12-16', 'AD'),
(590, 'admin', '66.49.157.92', '2015-12-16 07:44:47am', '2015-12-16 07:49:56am', 'f7iu1p3j82r3ebd5r171filsv1', '2015-12-16', 'AD'),
(591, 'ORYW-7613', '66.49.157.92', '2015-12-16 07:50:15am', '2015-12-16 07:57:27am', 'dpcgitbniarq73rftvab4eb4n1', '2015-12-16', 'AM'),
(592, 'admin', '66.49.237.16', '2015-12-16 15:13:34pm', '', 'aslsg34fk31flpd34snkd5d7d0', '2015-12-16', 'AD'),
(593, 'admin', '66.49.237.16', '2015-12-16 16:36:25pm', '', 'js6d68hmdive6808vps9h77ch7', '2015-12-16', 'AD'),
(594, 'admin', '69.171.142.218', '2015-12-17 11:30:36am', '', 'ccc3rbv1p01pfcujiinagqkle6', '2015-12-17', 'AD'),
(595, 'admin', '69.171.142.218', '2015-12-17 15:08:54pm', '', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', 'AD'),
(596, 'admin', '112.196.136.78', '2015-12-17 23:03:25pm', '', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-17', 'AD'),
(597, 'admin', '112.196.136.78', '2015-12-17 23:21:37pm', '', 'nl9s6f9f0q3ikqte3ll7jfuch0', '2015-12-17', 'AD'),
(598, 'admin', '112.196.136.78', '2015-12-18 04:04:20am', '', 'jl7f5ufpso1dqo1a1bvc9bdni4', '2015-12-18', 'AD'),
(599, 'admin', '112.196.136.78', '2015-12-18 07:56:16am', '', 'slcri2jpsim4pm55n8edjd52l3', '2015-12-18', 'AD'),
(600, 'admin', '112.196.136.78', '2015-12-18 22:53:36pm', '2015-12-19 04:15:23am', '76r0gg7lmc419surio3uhiu893', '2015-12-18', 'AD'),
(601, 'admin', '112.196.136.78', '2015-12-19 04:15:29am', '2015-12-19 06:32:00am', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', 'AD'),
(602, 'IXBB-4438', '112.196.136.78', '2015-12-19 05:28:19am', '2015-12-19 07:00:04am', 'm1o77jmp68jmni1gvh7u403el1', '2015-12-19', 'AM'),
(603, 'admin', '112.196.136.78', '2015-12-19 06:32:07am', '', 'jlh1b6kf6ke1mng9kfncs6i6a6', '2015-12-19', 'AD'),
(604, 'admin', '112.196.136.78', '2015-12-19 07:00:31am', '', 'dtjfrl4d6ab9jaguf5t28vnse5', '2015-12-19', 'AD'),
(605, 'admin', '112.196.136.78', '2015-12-19 08:26:51am', '', '4jj7lbuuke91civv623roitq63', '2015-12-19', 'AD'),
(606, 'admin', '112.196.136.78', '2015-12-20 23:18:32pm', '', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-20', 'AD'),
(607, 'admin', '112.196.136.78', '2015-12-20 23:35:45pm', '', '8u5j20km39lnu350209hrtqr73', '2015-12-20', 'AD'),
(608, 'IXBB-4438', '112.196.136.78', '2015-12-21 00:33:38am', '', 'v3s0brrvs965dvgp9m60uk5se2', '2015-12-21', 'AM'),
(609, 'admin', '112.196.136.78', '2015-12-21 00:51:02am', '', 'iom10lk7k3j2l4ikg6810cume3', '2015-12-21', 'AD'),
(610, 'admin', '112.196.136.78', '2015-12-21 07:10:26am', '2015-12-21 07:53:40am', 'lt2202cgdcb12pejog6es3n7g4', '2015-12-21', 'AD'),
(611, 'admin', '112.196.136.78', '2015-12-21 07:53:43am', '', 'neeo6di6gtfu32lupogie2imt0', '2015-12-21', 'AD'),
(612, 'IXBB-4438', '112.196.136.78', '2015-12-21 07:54:48am', '', 'epg9pm5upjn5ppgqselk18u821', '2015-12-21', 'AM'),
(613, 'admin', '69.171.142.218', '2015-12-23 14:24:29pm', '2015-12-23 16:00:04pm', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', 'AD'),
(614, 'IXBB-4438', '69.171.142.218', '2015-12-23 14:47:02pm', '2015-12-23 14:47:25pm', 'iu6sjs6m341q9msphuhh4n3sc0', '2015-12-23', 'AM'),
(615, 'IXBB-4438', '69.171.142.218', '2015-12-23 14:47:34pm', '2015-12-23 15:00:29pm', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', 'AM'),
(616, 'JUQI-3911', '69.171.142.218', '2015-12-23 15:00:35pm', '2015-12-23 15:01:56pm', 'hd8s79jebd63uvkrn1oa0vdig6', '2015-12-23', 'AM'),
(617, 'JUQI-3911', '69.171.142.218', '2015-12-23 15:02:00pm', '2015-12-23 15:57:55pm', '1j2b38h4v9oqrtb8ehfsb7ns54', '2015-12-23', 'AM'),
(618, 'YWFZ-6999', '69.171.142.218', '2015-12-23 15:06:59pm', '2015-12-23 15:09:01pm', 'khvtmb79peur43k9u8tlor2aa7', '2015-12-23', 'QF'),
(619, 'IXBB-4438', '69.171.142.218', '2015-12-23 15:58:09pm', '2015-12-23 16:06:11pm', 'go6c3pd8bfo6m07shtgbbbfjk1', '2015-12-23', 'AM'),
(620, 'JUQI-3911', '69.171.142.218', '2015-12-23 15:58:19pm', '', 'ckihs3f3hc90cilenob3fkjbu2', '2015-12-23', 'AM'),
(621, 'YWFZ-6999', '69.171.142.218', '2015-12-23 16:00:09pm', '2015-12-23 16:07:25pm', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', 'QF'),
(622, 'admin', '69.171.142.218', '2015-12-23 16:06:23pm', '2015-12-23 16:07:32pm', '6sta4lnpg460m2q2sqkcg5gm36', '2015-12-23', 'AD'),
(623, 'admin', '69.171.142.218', '2015-12-23 16:07:28pm', '', 'oua1pa1k1l11oflri3kne9r8e1', '2015-12-23', 'AD'),
(624, 'PZOI-0348', '69.171.142.218', '2015-12-23 16:08:49pm', '2015-12-23 16:09:38pm', 'ak2edc9l7dr5oqp2kjrcdfqhq1', '2015-12-23', 'AD'),
(625, 'PZOI-0348', '69.171.142.218', '2015-12-23 16:09:40pm', '', 'kmkvhnh2e5hgvfnchfk7hieco1', '2015-12-23', 'AD'),
(626, 'admin', '112.196.136.78', '2015-12-23 23:43:25pm', '2015-12-24 04:43:48am', '3g7jseom1rnh03ecbugq46bel6', '2015-12-23', 'AD'),
(627, 'YWFZ-6999', '112.196.136.78', '2015-12-24 00:49:03am', '2015-12-24 01:10:44am', '2g4u1cmsgjhiikr5ci7osm39k7', '2015-12-24', 'QF'),
(628, 'JUQI-3911', '112.196.136.78', '2015-12-24 01:11:33am', '2015-12-24 01:12:02am', '0nt7cjgm42ph1tdnscepkd9t27', '2015-12-24', 'AM'),
(629, 'IXBB-4438', '112.196.136.78', '2015-12-24 01:13:51am', '2015-12-24 04:45:48am', 'icgki9s7p6t8hpcojaro491pv6', '2015-12-24', 'AM'),
(630, 'admin', '112.196.136.78', '2015-12-24 04:34:48am', '', 'hqpe8knaluu1tn4kt525f3jcp0', '2015-12-24', 'AD'),
(631, 'admin', '112.196.136.78', '2015-12-24 04:44:10am', '2015-12-24 04:44:17am', 'cik6egau3flv9th5cp5co44pu2', '2015-12-24', 'AD'),
(632, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:46:10am', '2015-12-24 04:46:22am', 'l4s13psseb5tri7pac8s6p2007', '2015-12-24', 'AM'),
(633, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:46:42am', '2015-12-24 04:46:44am', '7jmjf4ipp4bt4n8sfpimtmccu2', '2015-12-24', 'AM'),
(634, 'admin', '112.196.136.78', '2015-12-24 04:50:38am', '2015-12-24 04:50:43am', 'ob1moumdagkl0ftcs7giqngi86', '2015-12-24', 'AD'),
(635, 'admin', '112.196.136.78', '2015-12-24 04:51:02am', '2015-12-24 05:36:06am', 'cot46q4om4jlck3ckpv6599fd5', '2015-12-24', 'AD'),
(636, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:52:25am', '2015-12-24 05:21:13am', 'hnfrv0nuf9ieectm4e0in9k722', '2015-12-24', 'AM'),
(637, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:59:46am', '', 'nk7qlpkjdovhmdc55iab16avg0', '2015-12-24', 'AM'),
(638, 'IXBB-4438', '112.196.136.78', '2015-12-24 05:21:15am', '2015-12-24 05:47:15am', 'og9vjkndlf78ni0d0bet29t2i3', '2015-12-24', 'AM'),
(639, 'IXBB-4438', '112.196.136.78', '2015-12-24 05:36:15am', '2015-12-24 06:18:19am', 'ct8elu7bfn9img4pq9m44g5js2', '2015-12-24', 'AM'),
(640, 'admin', '112.196.136.78', '2015-12-24 05:47:30am', '2015-12-24 06:02:47am', 'n4f6o7dn2rf6trabjns1phpk60', '2015-12-24', 'AD'),
(641, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:02:59am', '2015-12-24 06:18:03am', 'as63fmtc5la603nbv6tqceeio3', '2015-12-24', 'AM'),
(642, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:18:06am', '', 'cv6es6o2soaflvhdb4o3n3cth4', '2015-12-24', 'AM'),
(643, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:18:09am', '2015-12-24 06:18:56am', '2m565l964h0l3f01i18t9tg0r0', '2015-12-24', 'AM'),
(644, 'admin', '112.196.136.78', '2015-12-24 06:18:25am', '2015-12-24 06:30:59am', '5h9muelcm2vig24hon5b7r3uh1', '2015-12-24', 'AD'),
(645, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:19:02am', '', 'cjtt34fjt58ajhb6iqg7da88m2', '2015-12-24', 'AM'),
(646, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:31:07am', '2015-12-24 06:36:32am', 'i73a2ndst98fttjbmusc5ur695', '2015-12-24', 'AM'),
(647, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:37:00am', '2015-12-24 06:45:52am', '4imhqhmreb29q6b66ieiqbrla6', '2015-12-24', 'AM'),
(648, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:46:02am', '', 'eg8d4bp2p4m1csrskpvo6cudv3', '2015-12-24', 'AM'),
(649, 'JUQI-3911', '112.196.136.78', '2015-12-24 07:13:29am', '', '27edg0580tps53m8hfqilj9io4', '2015-12-24', 'AM'),
(650, 'admin', '112.196.136.78', '2015-12-24 23:13:22pm', '2015-12-25 06:19:40am', '5nm827ha51prpmno3ejsp9vsc1', '2015-12-24', 'AD'),
(651, 'IXBB-4438', '112.196.136.78', '2015-12-24 23:31:28pm', '', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-24', 'AM'),
(652, 'admin', '112.196.136.78', '2015-12-25 00:10:55am', '2015-12-25 00:45:20am', 'afqnsu6g61g2auktnrhp3v7nd5', '2015-12-25', 'AD'),
(653, 'JUQI-3911', '112.196.136.78', '2015-12-25 00:20:33am', '', 'oo0aiein258bfes4unq54ib3j1', '2015-12-25', 'AM'),
(654, 'IXBB-4438', '112.196.136.78', '2015-12-25 00:21:32am', '2015-12-25 06:47:35am', 'mnfsm6k57gpqvue02ut4uacna2', '2015-12-25', 'AM'),
(655, 'JUQI-3911', '112.196.136.78', '2015-12-25 00:45:27am', '2015-12-25 01:09:21am', 'uio22ek710hmuut3c2e6ong7c0', '2015-12-25', 'AM'),
(656, 'admin', '112.196.136.78', '2015-12-25 01:09:27am', '2015-12-25 01:16:12am', 'ioh6jo6ftaq2bj8lt0b3e69ku2', '2015-12-25', 'AD'),
(657, 'admin', '112.196.136.78', '2015-12-25 01:17:03am', '2015-12-25 06:36:46am', 'p4ktrq8l6r6qns6e46f02d1dg5', '2015-12-25', 'AD'),
(658, 'YWFZ-6999', '112.196.136.78', '2015-12-25 01:17:46am', '2015-12-25 01:34:13am', 'grll2pcfpc96nevoqqrl4jai56', '2015-12-25', 'QF'),
(659, 'VYOC-6443', '112.196.136.78', '2015-12-25 01:34:17am', '', '80onbhmj7j3qe9k6uujllp6j32', '2015-12-25', 'AM'),
(660, 'admin', '112.196.136.78', '2015-12-25 06:19:52am', '2015-12-25 06:19:59am', '31ji9641eh16b4r8rr3kshh291', '2015-12-25', 'AD'),
(661, 'admin', '112.196.136.78', '2015-12-25 06:27:51am', '', 's3kesp8h5tbadsbogdkorripd0', '2015-12-25', 'AD'),
(662, 'JUQI-3911', '112.196.136.78', '2015-12-25 06:36:52am', '2015-12-25 06:41:52am', 'us2q3c8k998dq02lk21ab9ao33', '2015-12-25', 'AM'),
(663, 'admin', '112.196.136.78', '2015-12-25 06:41:56am', '2015-12-25 06:42:10am', 'vbchp4ivjtm1uhq8d1f500mr56', '2015-12-25', 'AD'),
(664, 'GPUJ-2680', '112.196.136.78', '2015-12-25 06:42:16am', '2015-12-25 06:59:58am', '90rdomvsocb3kt51o679ouvht4', '2015-12-25', 'TM'),
(665, 'admin', '112.196.136.78', '2015-12-25 06:47:43am', '2015-12-25 07:01:32am', 'clprsu4403tvvgc7dfupsh4nd6', '2015-12-25', 'AD'),
(666, 'YWFZ-6999', '112.196.136.78', '2015-12-25 07:00:02am', '2015-12-25 07:01:39am', 'iht1rdirsnaqsediimr54rgqs3', '2015-12-25', 'QF'),
(667, 'GPUJ-2680', '112.196.136.78', '2015-12-25 07:01:43am', '', 'g4qo7t7sbaebtel72rac3sgr43', '2015-12-25', 'TM'),
(668, 'admin', '112.196.136.78', '2015-12-26 00:11:29am', '2015-12-26 04:23:50am', '342nee7gjanhikc745383esc94', '2015-12-26', 'AD'),
(669, 'IXBB-4438', '112.196.136.78', '2015-12-26 00:13:07am', '', 'mp37m6k3sc0g3u4fn2qsvteuk4', '2015-12-26', 'AM'),
(670, 'IXBB-4438', '112.196.136.78', '2015-12-26 00:13:07am', '2015-12-26 04:22:04am', 'ntop3ir6hkhqec5e9hbo8muh85', '2015-12-26', 'AM'),
(671, 'YWFZ-6999', '112.196.136.78', '2015-12-26 01:29:02am', '2015-12-26 03:28:43am', 'itl8rga55j7psid280hijf3pj0', '2015-12-26', 'QF'),
(672, 'admin', '112.196.136.78', '2015-12-26 01:34:29am', '2015-12-26 01:34:40am', 'h8i9ii97dt4e7193g8h4mfqt27', '2015-12-26', 'AD'),
(673, 'IXBB-4438', '112.196.136.78', '2015-12-26 01:34:54am', '2015-12-26 03:34:36am', '14d4ll2k71cv2g86hgk90q7v44', '2015-12-26', 'AM'),
(674, 'admin', '112.196.136.78', '2015-12-26 01:39:06am', '2015-12-26 05:54:28am', 'p9etqsiofa0hf41bhukrit5jt3', '2015-12-26', 'AD'),
(675, 'YWFZ-6999', '112.196.136.78', '2015-12-26 01:40:26am', '2015-12-26 03:42:48am', 'gmp5otg60io0sbvb8a8qodqj26', '2015-12-26', 'QF'),
(676, 'GPUJ-2680', '112.196.136.78', '2015-12-26 03:31:29am', '2015-12-26 03:34:37am', 'fi3kl3kalnnthldcoi3ohipjp4', '2015-12-26', 'TM'),
(677, 'JUQI-3911', '112.196.136.78', '2015-12-26 03:34:42am', '2015-12-26 03:35:18am', '92rtqbj0he4j7piee2rvmlca24', '2015-12-26', 'AM'),
(678, 'admin', '112.196.136.78', '2015-12-26 03:35:28am', '', '02ecuc66sj99r76vle5hrvgus3', '2015-12-26', 'AD'),
(679, 'YWFZ-6999', '112.196.136.78', '2015-12-26 03:35:37am', '2015-12-26 04:17:24am', '6mk0km395cpbnsol5lrcn3r082', '2015-12-26', 'QF'),
(680, 'GPUJ-2680', '112.196.136.78', '2015-12-26 03:42:58am', '2015-12-26 03:44:25am', 'm997jc9mprblrf68a04hc84vh5', '2015-12-26', 'TM'),
(681, 'YWFZ-6999', '112.196.136.78', '2015-12-26 04:18:08am', '2015-12-26 04:18:21am', 'v8vouiqv8j6qtpg21nu8fildl6', '2015-12-26', 'QF'),
(682, 'IXBB-4438', '112.196.136.78', '2015-12-26 04:22:48am', '', 'e348fvu76t1vassf1albh6opv3', '2015-12-26', 'AM'),
(683, 'admin', '112.196.136.78', '2015-12-26 05:44:14am', '2015-12-26 05:51:13am', 'i2o97rnnc9augkbfkldku493p3', '2015-12-26', 'AD'),
(684, 'ZSWD-9699', '112.196.136.78', '2015-12-26 05:51:25am', '2015-12-26 06:44:41am', '8j4k2mftnisl7778uceh1eaf83', '2015-12-26', 'AD'),
(685, 'ZSWD-9699', '112.196.136.78', '2015-12-26 05:55:03am', '2015-12-26 06:00:00am', '1cf9sfu5hot392bkb6och0e4l7', '2015-12-26', 'AD'),
(686, 'JUQI-3911', '112.196.136.78', '2015-12-26 05:56:54am', '2015-12-26 07:00:29am', 'rp6jebvpmenuaf1f2mkuem7930', '2015-12-26', 'AM'),
(687, 'ZSWD-9699', '112.196.136.78', '2015-12-26 06:00:03am', '2015-12-26 06:21:19am', 'op5os5pav3pdur6bnmcdj9htr6', '2015-12-26', 'AD'),
(688, 'admin', '112.196.136.78', '2015-12-26 06:02:15am', '', '7sibth08koo0pfpvk8nlhe11l2', '2015-12-26', 'AD'),
(689, 'ZSWD-9699', '112.196.136.78', '2015-12-26 06:21:21am', '2015-12-26 06:27:10am', '2jr2hnq1dpn1rld86ugc7lgnc6', '2015-12-26', 'QF'),
(690, 'RQYY-2239', '112.196.136.78', '2015-12-26 06:27:26am', '', 'bgscuetc73nlvl6ius4fm742f3', '2015-12-26', 'AM'),
(691, 'admin', '112.196.136.78', '2015-12-26 06:44:45am', '', 'omufm417ekpl5k3459d2kgul74', '2015-12-26', 'AD'),
(692, 'IXBB-4438', '112.196.136.78', '2015-12-26 07:01:31am', '', 'atol93qnva4q1i60t3snb6krc5', '2015-12-26', 'AM'),
(693, 'admin', '99.224.27.68', '2015-12-28 10:16:11am', '2015-12-28 10:25:44am', 'h6a6a8rf8neunhv7irfm77ca27', '2015-12-28', 'AD'),
(694, 'admin', '99.224.27.68', '2015-12-28 10:25:49am', '2015-12-28 10:28:06am', 'dpt9ejsu1d1d11vlm53ho5adj2', '2015-12-28', 'AD'),
(695, 'PZOI-0348', '99.224.27.68', '2015-12-28 10:28:09am', '2015-12-28 10:28:29am', '0j26jp0orcusljtslr0in5f2k2', '2015-12-28', 'QF'),
(696, 'admin', '99.224.27.68', '2015-12-28 10:28:43am', '2015-12-28 10:28:57am', 'log1jadm3vhpc9lmgeofm1oi74', '2015-12-28', 'AD'),
(697, 'PZOI-0348', '99.224.27.68', '2015-12-28 10:29:00am', '2015-12-28 10:29:20am', 'h2a694iq0o0l54n2626ddu4e91', '2015-12-28', 'AM'),
(698, 'admin', '99.224.27.68', '2015-12-28 10:31:46am', '', '3ad7o0p2rqnuscijjstt2oq1f6', '2015-12-28', 'AD'),
(699, 'IXBB-4438', '99.224.27.68', '2015-12-28 10:42:05am', '2015-12-28 10:42:41am', '3rofbai4vgkph686vlccivqh50', '2015-12-28', 'AM'),
(700, 'JUQI-3911', '99.224.27.68', '2015-12-28 10:42:53am', '2015-12-28 10:45:40am', 'rdirgh5aqfu75t1fqsh7mmobh5', '2015-12-28', 'AM'),
(701, 'IXBB-4438', '99.224.27.68', '2015-12-28 10:45:56am', '2015-12-28 10:46:44am', 'iagiilc067qj4n38mh549irmc7', '2015-12-28', 'AM'),
(702, 'YWFZ-6999', '99.224.27.68', '2015-12-28 10:46:47am', '2015-12-28 10:48:51am', 'cpv75i6n76hheuoo07s631uf26', '2015-12-28', 'QF'),
(703, 'IXBB-4438', '99.224.27.68', '2015-12-28 10:48:55am', '2015-12-28 10:52:32am', 'k9bbggkvkphc7do61kj2givq01', '2015-12-28', 'AM'),
(704, 'GPUJ-2680', '99.224.27.68', '2015-12-28 10:52:44am', '2015-12-28 10:53:02am', 'ep9gfjqpugluhbuthb4migjlu4', '2015-12-28', 'TM'),
(705, 'YWFZ-6999', '99.224.27.68', '2015-12-28 10:53:18am', '2015-12-28 10:53:52am', 'rre1qgpqkk5567fr1dp4os7cl4', '2015-12-28', 'QF'),
(706, 'admin', '112.196.136.78', '2015-12-29 00:23:59am', '', 'ff27njtgeqlvjdj802h0bmc232', '2015-12-29', 'AD'),
(707, 'admin', '112.196.136.78', '2015-12-29 00:56:52am', '2015-12-29 05:10:37am', 'ktqp019t9parolllku4disl836', '2015-12-29', 'AD'),
(708, 'IXBB-4438', '112.196.136.78', '2015-12-29 00:57:43am', '2015-12-29 02:03:30am', 'nrqu0kndcie61bj1uillit27h1', '2015-12-29', 'AM'),
(709, 'YWFZ-6999', '112.196.136.78', '2015-12-29 02:03:42am', '', '8aokcm8vg6ktq5ekv365gpmug0', '2015-12-29', 'QF'),
(710, 'IXBB-4438', '112.196.136.78', '2015-12-29 02:23:21am', '2015-12-29 04:45:00am', '65mueame9igd60skqee2fg5og5', '2015-12-29', 'AM'),
(711, 'IXBB-4438', '112.196.136.78', '2015-12-29 04:45:12am', '', 'hcgtp00ph6neg6m6f2ltfuvpr7', '2015-12-29', 'AM'),
(712, 'JUQI-3911', '112.196.136.78', '2015-12-29 04:58:24am', '2015-12-29 04:59:31am', '9ie64thrik8bbnemccispll0a2', '2015-12-29', 'AM'),
(713, 'JUQI-3911', '112.196.136.78', '2015-12-29 04:59:39am', '', 'supipg03vneh13o1klj1n10dg6', '2015-12-29', 'AM'),
(714, 'admin', '112.196.136.78', '2015-12-29 05:10:46am', '', 'fde36loj72nv0kks83npua5903', '2015-12-29', 'AD'),
(715, 'admin', '112.196.136.78', '2015-12-29 06:33:37am', '', 'h88lrsbi7js2mqr9m36n3b1nh2', '2015-12-29', 'AD'),
(716, 'admin', '99.224.27.68', '2015-12-30 14:08:28pm', '', 'k72u4rl9i5il3ir699muctgnt5', '2015-12-30', 'AD'),
(717, 'admin', '99.224.27.68', '2015-12-30 14:19:21pm', '', 'bqfdccp8sqcccpnupcuhcn2hf2', '2015-12-30', 'AD'),
(718, 'admin', '99.224.27.68', '2015-12-30 14:19:27pm', '2015-12-30 14:24:15pm', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', 'AD'),
(719, 'IXBB-4438', '99.224.27.68', '2015-12-30 14:24:21pm', '', 'khrbekfuqek4efbctdue3rf4j0', '2015-12-30', 'AM'),
(720, 'admin', '112.196.136.78', '2015-12-30 23:26:32pm', '2015-12-30 23:26:46pm', '1nrstvrsoh1l3mqq1qo658oh14', '2015-12-30', 'AD'),
(721, 'IXBB-4438', '112.196.136.78', '2015-12-30 23:26:56pm', '', '72tr7qn0tcgs1677lttu8puqo6', '2015-12-30', 'AM'),
(722, 'admin', '112.196.136.78', '2015-12-31 01:09:13am', '', 'co67s7a8o7v008r2c2ukajh360', '2015-12-31', 'AD'),
(723, 'admin', '112.196.136.78', '2015-12-31 02:00:44am', '', '0hb8tgtbrl4evhdsvov88du857', '2015-12-31', 'AD'),
(724, 'admin', '112.196.136.78', '2015-12-31 02:04:03am', '', 'rg7k31n5714j46jelqg5r5n0c6', '2015-12-31', 'AD'),
(725, 'IXBB-4438', '112.196.136.78', '2015-12-31 02:09:43am', '', '7skrdv0svch92dh20shjdp9bn7', '2015-12-31', 'AM'),
(726, 'JUQI-3911', '112.196.136.78', '2015-12-31 02:10:28am', '', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', 'AM'),
(727, 'YWFZ-6999', '112.196.136.78', '2015-12-31 03:21:35am', '', 'ppvig9hpu6v8pfhdmo86803rg3', '2015-12-31', 'QF'),
(728, 'GPUJ-2680', '112.196.136.78', '2015-12-31 04:06:20am', '2015-12-31 04:42:34am', '7b0latmadsl5e73f0a1co818i0', '2015-12-31', 'TM'),
(729, 'admin', '112.196.136.78', '2015-12-31 04:42:39am', '', 'ns3trubjgbqo25587dqudvm372', '2015-12-31', 'AD'),
(730, 'admin', '112.196.136.78', '2015-12-31 05:52:11am', '', 'n07sngk1nlnhpj92mv9odtpeu0', '2015-12-31', 'AD'),
(731, 'IXBB-4438', '112.196.136.78', '2015-12-31 05:53:05am', '', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', 'AM'),
(732, 'admin', '112.196.136.78', '2016-01-04 04:05:52am', '', 'imasv2tdtjeoojmqd8vrbkl1l0', '2016-01-04', 'AD'),
(733, 'admin', '69.171.142.218', '2016-01-05 12:41:56pm', '', '3vakdj75b9r9pf7ijtvc3949d2', '2016-01-05', 'AD'),
(734, 'IXBB-4438', '69.171.142.218', '2016-01-05 12:42:21pm', '', '9fhvvmdtpbv2d7vdeuvb3tu0r1', '2016-01-05', 'AM'),
(735, 'admin', '69.171.142.218', '2016-01-05 14:05:02pm', '2016-01-05 14:41:51pm', '26njcqns322331ferm1furjup2', '2016-01-05', 'AD'),
(736, 'IXBB-4438', '69.171.142.218', '2016-01-05 14:05:25pm', '', '73m71k3llg5eqor4pcnbmmcrf2', '2016-01-05', 'AM'),
(737, 'JUQI-3911', '69.171.142.218', '2016-01-05 14:41:55pm', '2016-01-05 14:43:12pm', 'km4fr9agvt429kcqqm6u6d78n5', '2016-01-05', 'AM'),
(738, 'admin', '69.171.142.218', '2016-01-05 14:43:16pm', '', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', 'AD'),
(739, 'IXBB-4438', '69.171.142.218', '2016-01-05 14:46:54pm', '', 'b9jaqkhb88dm873udp9nio4300', '2016-01-05', 'AM'),
(740, 'admin', '112.196.136.78', '2016-01-05 23:18:23pm', '2016-01-05 23:18:36pm', '0bkpj464roq7l32imtbf6b63s1', '2016-01-05', 'AD'),
(741, 'IXBB-4438', '112.196.136.78', '2016-01-05 23:18:49pm', '2016-01-06 00:28:49am', 'l8oqnt4r2eouki9i87not5k922', '2016-01-05', 'AM'),
(742, 'admin', '112.196.136.78', '2016-01-05 23:28:01pm', '', '4tb5jrt9spjklhjpbvcvdtjfp7', '2016-01-05', 'AD'),
(743, 'admin', '112.196.136.78', '2016-01-06 00:29:02am', '', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', 'AD'),
(744, 'IXBB-4438', '112.196.136.78', '2016-01-06 00:30:56am', '', 'da54lmr0f4kkv10sv1jj2r9sm3', '2016-01-06', 'AM'),
(745, 'JUQI-3911', '112.196.136.78', '2016-01-06 00:32:30am', '', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', 'AM'),
(746, 'YWFZ-6999', '104.131.14.167', '2016-01-06 00:36:15am', '', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', 'QF'),
(747, 'admin', '69.171.142.218', '2016-01-06 09:38:49am', '', 'urtmgs61a3cvlnhmlp78ee61f6', '2016-01-06', 'AD'),
(748, 'admin', '69.171.142.218', '2016-01-06 09:56:47am', '2016-01-06 09:56:49am', 'uu8ftsmg59dtf14ap5p0qvjqb3', '2016-01-06', 'AD'),
(749, 'IXBB-4438', '69.171.142.218', '2016-01-06 09:56:56am', '', 'uhcmce3fi5l98k1u1pnsp05ss5', '2016-01-06', 'AM'),
(750, 'admin', '112.196.136.78', '2016-01-07 00:12:53am', '', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', 'AD'),
(751, 'IXBB-4438', '112.196.136.78', '2016-01-07 01:26:00am', '', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', 'AM'),
(752, 'YWFZ-6999', '112.196.136.78', '2016-01-07 02:14:06am', '2016-01-07 04:17:23am', '032fp9rtrigejolirc5v1gosv6', '2016-01-07', 'QF'),
(753, 'GPUJ-2680', '112.196.136.78', '2016-01-07 04:17:47am', '', '1qa0ptphoqkrq5774ggnnmlf96', '2016-01-07', 'TM'),
(754, 'admin', '112.196.136.78', '2016-01-07 07:55:46am', '', 'aqi7t2o0ql1kuuo836nl37vtl0', '2016-01-07', 'AD'),
(755, 'YWFZ-6999', '112.196.136.78', '2016-01-07 07:57:09am', '2016-01-07 07:59:20am', 'htk7bu6f2de2i3vfo5vccv6br5', '2016-01-07', 'QF'),
(756, 'IXBB-4438', '112.196.136.78', '2016-01-07 07:59:49am', '', '8hp39ti7k4upl6fjulftlore74', '2016-01-07', 'AM'),
(757, 'admin', '69.171.142.218', '2016-01-07 11:11:50am', '', 'b000ooa1eua8i13qe0ekbo3234', '2016-01-07', 'AD'),
(758, 'YWFZ-6999', '69.171.142.218', '2016-01-07 11:12:43am', '', 'efjtt1egvukuepj5uke3go2mc5', '2016-01-07', 'QF'),
(759, 'admin', '112.196.136.78', '2016-01-07 23:42:05pm', '', 'k5kv399jag7q33detd8rf32pl6', '2016-01-07', 'AD'),
(760, 'admin', '112.196.136.78', '2016-01-08 00:00:51am', '', '8jc7dvbuosi909bl708a3felm2', '2016-01-08', 'AD'),
(761, 'admin', '112.196.136.78', '2016-01-08 01:48:02am', '', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', 'AD'),
(762, 'IXBB-4438', '112.196.136.78', '2016-01-08 02:02:31am', '2016-01-08 05:00:24am', 'i12ktsj2spi5d87ftpakb083u1', '2016-01-08', 'AM'),
(763, 'GPUJ-2680', '112.196.136.78', '2016-01-08 05:00:34am', '2016-01-08 06:58:16am', 'mlii3e71npt5ju9bht40h5no01', '2016-01-08', 'TM'),
(764, 'IXBB-4438', '112.196.136.78', '2016-01-08 06:58:49am', '', '5t3c1psc52vre1hamif7cj0d35', '2016-01-08', 'AM'),
(765, 'admin', '112.196.136.78', '2016-01-08 07:27:33am', '', 'j24l66nrodeihiiaq3p8a97r90', '2016-01-08', 'AD'),
(766, 'admin', '69.171.142.218', '2016-01-08 09:13:08am', '', 'us0oc45eo2ud4lp4ffjj9jivk6', '2016-01-08', 'AD'),
(767, 'admin', '69.171.142.218', '2016-01-08 09:28:00am', '', 'krjphnl23queo7jaulp5jpp2m6', '2016-01-08', 'AD'),
(768, 'IXBB-4438', '69.171.142.218', '2016-01-08 09:28:22am', '', 'tkraoovj0sg0e9u7m8oimoukv7', '2016-01-08', 'AM'),
(769, 'admin', '112.196.136.78', '2016-01-09 00:29:07am', '', 'qt2ul7bevull36vjjp69tk4he6', '2016-01-09', 'AD'),
(770, 'YWFZ-6999', '112.196.136.78', '2016-01-09 05:51:51am', '', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', 'QF'),
(771, 'IXBB-4438', '112.196.136.78', '2016-01-09 05:57:29am', '', 'h92lq5s976uglpo30lqogfur53', '2016-01-09', 'AM'),
(772, 'MSOX-3592', '45.55.199.109', '2016-01-09 06:39:11am', '', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', 'AM'),
(773, 'admin', '112.196.136.78', '2016-01-09 06:50:42am', '', 'ihu5m078qg9q64acalaq515dg5', '2016-01-09', 'AD'),
(774, 'IXBB-4438', '112.196.136.78', '2016-01-09 06:51:28am', '', 'p4esprbobn4v9tu0obel2h6tc0', '2016-01-09', 'AM'),
(775, 'admin', '112.196.136.78', '2016-01-11 00:07:43am', '', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', 'AD'),
(776, 'JUQI-3911', '112.196.136.78', '2016-01-11 05:32:16am', '', 'q2dg0c72vs6lli1vhh22llgp02', '2016-01-11', 'AM'),
(777, 'MSOX-3592', '112.196.136.78', '2016-01-11 05:33:18am', '2016-01-11 05:34:57am', 'fi9v727lvavlumt1dmo7a4ffc2', '2016-01-11', 'AM'),
(778, 'MSOX-3592', '112.196.136.78', '2016-01-11 05:35:07am', '', '39v9ghc018k321d86341puaq64', '2016-01-11', 'AM'),
(779, 'admin', '69.171.142.218', '2016-01-11 15:20:21pm', '', 'dmh0fv0hbsbbe5dgvoifn8v856', '2016-01-11', 'AD'),
(780, 'admin', '112.196.136.78', '2016-01-12 01:45:06am', '', 'tkpa2u7trmo9sod8ulci80de87', '2016-01-12', 'AD'),
(781, 'IXBB-4438', '112.196.136.78', '2016-01-12 01:56:28am', '2016-01-12 05:30:40am', 'q2gkgd0lbm7unb3t6bsvsvkpm1', '2016-01-12', 'AM'),
(782, 'VYOC-6443', '112.196.136.78', '2016-01-12 05:31:05am', '2016-01-12 05:37:17am', 'as4a1a86df3r807n6ml7g2dqf3', '2016-01-12', 'QF'),
(783, 'YSZJ-4486', '112.196.136.78', '2016-01-12 05:37:33am', '', 'tlmfjukipnpenqr2f7p5sg2h80', '2016-01-12', 'QF'),
(784, 'YWFZ-6999', '112.196.136.78', '2016-01-12 05:38:56am', '', 'j097l0o1jdsrkkn0pvo3njb3o3', '2016-01-12', 'QF'),
(785, 'admin', '112.196.136.78', '2016-01-12 06:51:54am', '2016-01-12 06:52:23am', 'q98m9dljm9nkee0vdvu1b0kdj3', '2016-01-12', 'AD'),
(786, 'JUQI-3911', '112.196.136.78', '2016-01-12 06:52:30am', '2016-01-12 06:55:24am', '486he98k7q0dcpd6alolhevla5', '2016-01-12', 'AM'),
(787, 'admin', '112.196.136.78', '2016-01-12 06:55:32am', '', 'ure7nfpocp4bcfbm9lb0741gb2', '2016-01-12', 'AD'),
(788, 'admin', '69.171.142.218', '2016-01-12 08:24:17am', '', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', 'AD'),
(789, 'IXBB-4438', '69.171.142.218', '2016-01-12 08:26:57am', '', 'muibrjrk65nmqfvlalmnnnosj4', '2016-01-12', 'AM'),
(790, 'admin', '99.224.139.23', '2016-01-12 10:23:49am', '', 'mp8berb68n7lfhfhdpjrrrd5g4', '2016-01-12', 'AD'),
(791, 'admin', '69.171.142.218', '2016-01-12 12:26:43pm', '', '1vmh6orvh1gvlv6a2acp535q97', '2016-01-12', 'AD'),
(792, 'admin', '69.171.142.218', '2016-01-12 13:36:37pm', '', 'jitenpa55kcvuf80k2mtf3tl00', '2016-01-12', 'AD'),
(793, 'admin', '69.171.142.218', '2016-01-12 13:37:50pm', '', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', 'AD'),
(794, 'admin', '69.171.142.218', '2016-01-12 13:56:38pm', '', 's9kha4ju66qsri6miuljnv1l12', '2016-01-12', 'AD'),
(795, 'admin', '69.171.142.218', '2016-01-12 14:02:40pm', '2016-01-12 14:04:24pm', 'idmibtscmue94kb7ghg8a74al3', '2016-01-12', 'AD'),
(796, 'admin', '69.171.142.218', '2016-01-12 14:04:31pm', '', 'ci7ld3bhc8nem24vuhg10k2l83', '2016-01-12', 'AD'),
(797, 'admin', '69.171.142.218', '2016-01-12 14:12:26pm', '', 'voesuspvtlfuns07rduc5ojj30', '2016-01-12', 'AD'),
(798, 'admin', '112.196.136.78', '2016-01-12 23:47:00pm', '2016-01-13 04:33:13am', 'm1tmb4jssostepgig1bjnh4o20', '2016-01-12', 'AD'),
(799, 'admin', '112.196.136.78', '2016-01-13 02:08:33am', '', 'sns6fqlmrlj7aasm3t4jknhbv2', '2016-01-13', 'AD'),
(800, 'admin', '112.196.136.78', '2016-01-13 04:32:42am', '', '9epk2un2mthour0oqrsj8m06q3', '2016-01-13', 'AD'),
(801, 'admin', '69.171.142.218', '2016-01-13 12:14:29pm', '', '953m32pertu8fnil2q0jppc2b0', '2016-01-13', 'AD'),
(802, 'admin', '69.171.142.218', '2016-01-13 12:14:51pm', '', '7ciogo0n9o6umrt27s5qlli2r6', '2016-01-13', 'AD'),
(803, 'admin', '69.171.142.218', '2016-01-13 12:19:19pm', '', '7biev57t5uqnplkpqjf52lr8b3', '2016-01-13', 'AD'),
(804, 'admin', '69.171.142.218', '2016-01-13 12:21:25pm', '', 'hs35o953gppjatam7t29pi0p97', '2016-01-13', 'AD'),
(805, 'admin', '69.171.142.218', '2016-01-13 12:22:28pm', '', 'ndrmie2a9a88mr3vv7v1rma8t2', '2016-01-13', 'AD'),
(806, 'admin', '69.171.142.218', '2016-01-13 14:50:37pm', '', '97mco6v7p8tjsn1jegtpdfi3f1', '2016-01-13', 'AD'),
(807, 'admin', '69.171.142.218', '2016-01-13 15:00:50pm', '', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', 'AD'),
(808, 'admin', '112.196.136.78', '2016-01-13 23:34:37pm', '', 'cmdf9e6dvcnq40trhrf0ou6de4', '2016-01-13', 'AD'),
(809, 'IXBB-4438', '112.196.136.78', '2016-01-13 23:57:12pm', '2016-01-14 03:39:21am', 'g5petieosbsuk80l0qjaq139h3', '2016-01-13', 'AM'),
(810, 'JUQI-3911', '112.196.136.78', '2016-01-14 01:30:31am', '2016-01-14 05:14:54am', 'r97ncj49lr65ovhurse8abk000', '2016-01-14', 'AM'),
(811, 'YSZJ-4486', '104.236.225.168', '2016-01-14 01:37:35am', '2016-01-14 05:15:26am', '3i24qccaducrvam51702rfdn83', '2016-01-14', 'QF'),
(812, 'YWFZ-6999', '112.196.136.78', '2016-01-14 03:39:33am', '', 'etng0f9k3phuic9vh0lsc6nse3', '2016-01-14', 'QF'),
(813, 'admin', '112.196.136.78', '2016-01-14 05:13:03am', '', '9k70mfkeq967emptremgq463p6', '2016-01-14', 'AD'),
(814, 'admin', '112.196.136.78', '2016-01-14 05:20:34am', '2016-01-14 05:33:08am', 'p2ot53rdvsusum2ccl1j6s2cd5', '2016-01-14', 'AD'),
(815, 'admin', '112.196.136.78', '2016-01-14 05:31:18am', '', 'bbo965d6gk7lr7ju9e599umbh7', '2016-01-14', 'AD'),
(816, 'admin', '112.196.136.78', '2016-01-14 05:33:25am', '', 'b98amn09bhb0u8b1j1ieg9lte1', '2016-01-14', 'AD'),
(817, 'admin', '112.196.136.78', '2016-01-14 06:01:10am', '', 'n1jrcbo73uh95lhj3cvv5m6js2', '2016-01-14', 'AD'),
(818, 'admin', '112.196.136.78', '2016-01-14 06:54:29am', '', 'pr5v6r5dre6gtve7049krftvi2', '2016-01-14', 'AD'),
(819, 'admin', '112.196.136.78', '2016-01-14 07:50:15am', '', '77d11e76rlmgrrjs8o0eghbur5', '2016-01-14', 'AD');
INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`) VALUES
(820, 'admin', '112.196.136.78', '2016-01-14 07:51:16am', '', 'dnatvvg6m474s0m13sl6361514', '2016-01-14', 'AD'),
(821, 'admin', '69.171.142.218', '2016-01-14 09:09:30am', '', '3qg4att5odfbo6logequb1aac4', '2016-01-14', 'AD'),
(822, 'admin', '69.171.142.218', '2016-01-14 11:51:13am', '', '2d80rt06drj5cfproulpun7fm1', '2016-01-14', 'AD'),
(823, 'admin', '66.49.211.86', '2016-01-14 12:28:36pm', '2016-01-14 12:29:18pm', 'ubbrcml747om3i17sc3ikcjbf2', '2016-01-14', 'AD'),
(824, 'admin', '69.171.142.218', '2016-01-14 15:11:53pm', '', 'oulojqdqolfpraaueeejop9tf5', '2016-01-14', 'AD'),
(825, 'admin', '69.171.142.218', '2016-01-14 15:12:32pm', '2016-01-14 15:25:16pm', '632bdoq4is8qgbfj65n58ofva5', '2016-01-14', 'AD'),
(826, 'admin', '69.171.142.218', '2016-01-14 15:28:07pm', '', 'eg9aqvs0d21itj3r8rnu4t0hl2', '2016-01-14', 'AD'),
(827, 'admin', '66.49.211.86', '2016-01-14 16:28:26pm', '2016-01-14 16:31:54pm', 'srra49s09v8u3gn7jb0vu3bkk5', '2016-01-14', 'AD'),
(828, 'admin', '69.171.142.218', '2016-01-14 16:29:57pm', '', '5ihrc27bt7rfl5nk3r66ao9jm2', '2016-01-14', 'AD'),
(829, 'admin', '112.196.136.78', '2016-01-15 00:23:49am', '', 'vhb2mhms257vccn1ecp7avpsc6', '2016-01-15', 'AD'),
(830, 'admin', '112.196.136.78', '2016-01-15 03:35:22am', '', '02fviedigf6b6soa1joe2voll6', '2016-01-15', 'AD'),
(831, 'admin', '112.196.136.78', '2016-01-15 05:03:52am', '', 'bm636bsfjfbgos8vu5ita3r1j4', '2016-01-15', 'AD'),
(832, 'admin', '112.196.136.78', '2016-01-15 05:05:37am', '', '2ik1sdl3uvvecohonvub1vpqf5', '2016-01-15', 'AD'),
(833, 'admin', '112.196.136.78', '2016-01-15 05:05:37am', '2016-01-15 05:23:39am', 'nh2u8ou0b14dq98g379fq918k6', '2016-01-15', 'AD'),
(834, 'admin', '112.196.136.78', '2016-01-15 05:15:29am', '', 'vlhllntale95geu2hfetqnmnt5', '2016-01-15', 'AD'),
(835, 'admin', '112.196.136.78', '2016-01-15 05:23:49am', '2016-01-15 07:16:19am', 'qndd0kbge7sob8mnb22nh5nvi7', '2016-01-15', 'AD'),
(836, 'admin', '112.196.136.78', '2016-01-15 07:13:21am', '', 'mohdp1032ao97an3bjlf7vr0i5', '2016-01-15', 'AD'),
(837, 'admin', '112.196.136.78', '2016-01-15 07:16:35am', '', 'd3vbip0npr39aqu1vfcnejv3q6', '2016-01-15', 'AD'),
(838, 'admin', '112.196.136.78', '2016-01-15 07:35:54am', '', 'ac0ot43b18gth3pd2r1vgkp0q4', '2016-01-15', 'AD'),
(839, 'admin', '112.196.136.78', '2016-01-16 00:52:53am', '', 'lad206vpaveu1a9nm6tkvvhh07', '2016-01-16', 'AD'),
(840, 'admin', '112.196.136.78', '2016-01-16 00:54:20am', '', '868dvlgspkilkssd5go49ksdc1', '2016-01-16', 'AD'),
(841, 'admin', '112.196.136.78', '2016-01-16 00:55:50am', '', 'c7p5pvmf0c66noq132jh26rtt0', '2016-01-16', 'AD'),
(842, 'admin', '112.196.136.78', '2016-01-16 03:40:10am', '', 'k6i849luii4adibrh2qp7o3ki7', '2016-01-16', 'AD'),
(843, 'admin', '69.171.142.218', '2016-01-18 08:46:10am', '', 'tplemtco3ttpfqnln3k9sfori1', '2016-01-18', 'AD'),
(844, 'IXBB-4438', '69.171.142.218', '2016-01-18 08:58:19am', '', 'pb8ibdphbe2kanbtjp19osdu05', '2016-01-18', 'AM'),
(845, 'IXBB-4438', '69.171.142.218', '2016-01-18 09:06:31am', '2016-01-18 09:48:05am', 'l3q508ka1potsj2q2g6teili86', '2016-01-18', 'AM'),
(846, 'YSZJ-4486', '69.171.142.218', '2016-01-18 09:48:08am', '', 'ek5atlfdneup1d0la4gqgfksu0', '2016-01-18', 'QF'),
(847, 'admin', '69.171.142.218', '2016-01-18 11:51:00am', '', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', 'AD'),
(848, 'OORQ-0229', '69.171.142.218', '2016-01-18 11:53:47am', '', 'ur18h772e1cihcmmjb91qkvc91', '2016-01-18', 'AM'),
(849, 'admin', '112.196.136.78', '2016-01-18 23:26:02pm', '', '5r9qkoqu932q6bst866vtm4df1', '2016-01-18', 'AD'),
(850, 'admin', '112.196.136.78', '2016-01-19 00:42:36am', '', 'cssjqnvmjhisbgk02k2cm8adg2', '2016-01-19', 'AD'),
(851, 'admin', '112.196.136.78', '2016-01-19 00:49:07am', '', '9k6dp7r57kb8vr0rj8nk3aj2d3', '2016-01-19', 'AD'),
(852, 'admin', '69.171.142.218', '2016-01-19 09:00:04am', '', 'l9jbk8hv8aij33sti84bc7lul7', '2016-01-19', 'AD'),
(853, 'admin', '69.171.142.218', '2016-01-19 09:00:55am', '', 'v5rm0jr2n8cmipb7fpc6ipp6b1', '2016-01-19', 'AD'),
(854, 'admin', '69.171.142.218', '2016-01-19 09:01:47am', '', 'u3c7fvconfanf1v8je6psubga6', '2016-01-19', 'AD'),
(855, 'admin', '66.49.211.86', '2016-01-19 09:05:26am', '2016-01-19 09:14:08am', 'ld1kssfd5l9ccetbtbr2esdrl4', '2016-01-19', 'AD'),
(856, 'admin', '69.171.142.218', '2016-01-19 09:05:58am', '', 'ibfr6mn0ud0n3a73ba3siqpua1', '2016-01-19', 'AD'),
(857, 'admin', '69.171.142.218', '2016-01-19 09:08:23am', '', '0mt5o0pg0v64ssd3od8heh92o3', '2016-01-19', 'AD'),
(858, 'admin', '69.171.142.218', '2016-01-19 09:12:46am', '', 'rd4elpfjugbvgjg0en8a7krt30', '2016-01-19', 'AD'),
(859, 'admin', '66.49.211.86', '2016-01-19 09:14:45am', '2016-01-19 09:16:09am', 'casqiqn1r3d55mcbrd07esi622', '2016-01-19', 'AD'),
(860, 'admin', '66.49.211.86', '2016-01-19 09:33:43am', '2016-01-19 09:34:18am', '45dm48gg4s4r9cjaesuvdknko3', '2016-01-19', 'AD'),
(861, 'OORQ-0229', '66.49.211.86', '2016-01-19 09:34:43am', '2016-01-19 11:11:24am', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', 'AM'),
(862, 'admin', '69.171.142.218', '2016-01-19 09:36:10am', '', 't65l507u3qki2dldhdtpe8hao2', '2016-01-19', 'AD'),
(863, 'OORQ-0229', '69.171.142.218', '2016-01-19 09:41:42am', '', '1mnrgmcf0kvu5pg2vtcqn7vao6', '2016-01-19', 'AM'),
(864, 'OORQ-0229', '69.171.142.218', '2016-01-19 10:11:55am', '', '304d8nh7p67uo5dsp639sn3833', '2016-01-19', 'AM'),
(865, 'admin', '66.49.211.86', '2016-01-20 10:53:01am', '', 'gikl515oah3e91ffb4r67s2p56', '2016-01-20', 'AD'),
(866, 'admin', '112.196.136.78', '2016-01-21 07:21:57am', '', 'kkcs572685dldr1s8nhap87s34', '2016-01-21', 'AD'),
(867, 'admin', '69.171.142.218', '2016-01-21 10:49:00am', '2016-01-21 10:49:09am', 'j2qr08p2vq3hvm3rq8oinqalc3', '2016-01-21', 'AD'),
(868, 'IXBB-4438', '69.171.142.218', '2016-01-21 10:49:17am', '2016-01-21 10:56:49am', '5lkr656kf1stin8ancb2ti7k24', '2016-01-21', 'AM'),
(869, 'admin', '69.171.142.218', '2016-01-21 10:56:54am', '', '2uoehgbjafc2isregf7kmfa057', '2016-01-21', 'AD'),
(870, 'admin', '69.171.142.218', '2016-01-25 11:56:15am', '', 'lte45u5j98g8psnncljibfu0k1', '2016-01-25', 'AD'),
(871, 'admin', '69.171.142.218', '2016-01-25 12:01:31pm', '', 'gemg6ujsm6miqfi81sfdlhd530', '2016-01-25', 'AD'),
(872, 'admin', '66.49.211.86', '2016-01-26 08:12:53am', '2016-01-26 08:13:14am', '5j9eiohs1fjouah68lqomqd1h7', '2016-01-26', 'AD'),
(873, 'admin', '1.39.49.146', '2016-01-27 06:51:15am', '2016-01-27 06:58:20am', '6a6gq3up3gcfqgf026ncnj3i17', '2016-01-27', 'AD'),
(874, 'admin', '67.55.16.226', '2016-01-27 11:07:28am', '', 'h0pm204fdinqnkgeleic831137', '2016-01-27', 'AD'),
(875, 'admin', '67.55.16.226', '2016-01-27 12:21:05pm', '', 'f976c9akpvf59kcqt5bqhkj1e3', '2016-01-27', 'AD'),
(876, 'admin', '189.102.169.77', '2016-01-27 14:25:46pm', '', 'ar2o4uogjgq0jhbv4eft2c8g20', '2016-01-27', 'AD'),
(877, 'admin', '67.55.16.226', '2016-01-27 15:42:35pm', '', 'imb6i6pmcmustre4olh09liuv3', '2016-01-27', 'AD'),
(878, 'admin', '67.55.16.226', '2016-01-28 09:29:33am', '', '3mfcj8is0sl8rrrf18dfqoijj0', '2016-01-28', 'AD'),
(879, 'admin', '67.55.16.226', '2016-01-28 12:42:21pm', '', '0q2b89e5086h0m872uh89ranq2', '2016-01-28', 'AD'),
(880, 'admin', '125.62.103.136', '2016-01-28 14:52:33pm', '', 'moi1da01iarrkadst63gsf1u25', '2016-01-28', 'AD'),
(881, 'admin', '66.49.250.8', '2016-01-28 15:34:36pm', '2016-01-28 15:34:45pm', 'b39374nlst3ftqst2cb0qph5k2', '2016-01-28', 'AD'),
(882, 'admin', '182.70.216.60', '2016-01-28 16:09:13pm', '', '582virek8nanhpp5u2k4h6g8f3', '2016-01-28', 'AD'),
(883, 'admin', '43.229.224.130', '2016-01-29 03:18:20am', '', 'tg8affpbd5bciob8l04j5plb73', '2016-01-29', 'AD'),
(884, 'admin', '43.229.224.130', '2016-01-29 03:48:42am', '', 'f0bclghpd1ob7etaa9pkc60kp5', '2016-01-29', 'AD'),
(885, 'admin', '103.44.18.3', '2016-01-29 11:35:38am', '', 'jfhqulmdmi0halb022eg46el12', '2016-01-29', 'AD'),
(886, 'admin', '67.55.16.226', '2016-01-29 14:00:46pm', '', '6ouun8mnr6pccpij38j10aifk5', '2016-01-29', 'AD'),
(887, 'admin', '67.55.16.226', '2016-01-29 16:40:51pm', '', '6b5790fcq5hoe02jdaegb1e5c0', '2016-01-29', 'AD'),
(888, 'admin', '67.55.16.226', '2016-01-29 16:44:27pm', '', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', 'AD'),
(889, 'admin', '191.181.15.233', '2016-01-29 17:11:12pm', '', 'poe8g8mk38m9drjmoeuf11k6m0', '2016-01-29', 'AD'),
(890, 'admin', '191.181.15.233', '2016-01-29 17:35:48pm', '', '69278cc7edc723a2fda868bce55e6603', '2016-01-29', 'AD'),
(891, 'admin', '66.49.230.207', '2016-02-01 08:42:15am', '', 'a17691567023d69f985c9619c261cecc', '2016-02-01', 'AD'),
(892, 'admin', '191.181.15.233', '2016-02-01 08:56:33am', '', '93fb03dac1efb70b8c2c31bf0a42e5ce', '2016-02-01', 'AD'),
(893, 'admin', '66.49.230.207', '2016-02-01 14:04:27pm', '', '47b83deda3e1b11c95f27ca756434c5e', '2016-02-01', 'AD'),
(894, 'admin', '191.181.15.233', '2016-02-01 14:22:53pm', '', '2f977eebe2e40a42c6117ecbe4467e59', '2016-02-01', 'AD'),
(895, 'admin', '66.49.230.207', '2016-02-01 14:28:41pm', '', '526cbe1c7e8dad5e45d1778c72c61c03', '2016-02-01', 'AD'),
(896, 'admin', '66.49.230.207', '2016-02-01 14:49:19pm', '', '315228b03e52e7944243a4384a96c9b0', '2016-02-01', 'AD'),
(897, 'admin', '66.49.230.207', '2016-02-01 14:50:15pm', '', 'b5af62da9deffe4f4c6f7fae4bc80d7d', '2016-02-01', 'AD'),
(898, 'admin', '66.49.230.207', '2016-02-04 11:33:12am', '2016-02-04 11:33:19am', '4f56ed7d6ea32345103cde8681081b66', '2016-02-04', 'AD'),
(899, 'admin', '66.49.230.207', '2016-02-04 12:00:15pm', '', 'c900c8fe7d04e3083e44c00bd0b744b9', '2016-02-04', 'AD'),
(900, 'admin', '66.49.230.207', '2016-02-04 12:04:56pm', '', 'dbc5fd1183495efe6a7981ebf61ac29a', '2016-02-04', 'AD'),
(901, 'admin', '69.171.149.223', '2016-02-04 12:16:48pm', '', 'dc7d908e417e3a80cd05dc1e569009b0', '2016-02-04', 'AD'),
(902, 'admin', '68.55.116.144', '2016-02-04 14:48:02pm', '', 'b3e5e0f90bcff526421d14cb4edbaa25', '2016-02-04', 'AD'),
(903, 'admin', '69.171.149.223', '2016-02-04 15:36:31pm', '', '5a25789107085f026b3f1b072f94a1ff', '2016-02-04', 'AD'),
(904, 'admin', '66.49.230.207', '2016-02-04 15:42:20pm', '', '047f36cf979a3ef97444404b25650bc7', '2016-02-04', 'AD'),
(905, 'admin', '191.181.15.233', '2016-02-04 17:20:49pm', '', '661412139cfdf865a85d918828ff39ee', '2016-02-04', 'AD'),
(906, 'admin', '66.49.230.207', '2016-02-05 08:35:59am', '', 'a5c1577666cea94dcd69c79df590d157', '2016-02-05', 'AD'),
(907, 'admin', '68.55.116.144', '2016-02-05 10:56:07am', '', 'f8d6b2cc7c31c8c31d6245f37251a18e', '2016-02-05', 'AD'),
(908, 'admin', '191.181.15.233', '2016-02-05 11:17:11am', '', '4eea5e5b0143170d5a69804fe931811f', '2016-02-05', 'AD'),
(909, 'admin', '66.49.230.207', '2016-02-05 12:34:25pm', '', '80499731e8b914a7940b8c0d1771c488', '2016-02-05', 'AD'),
(910, 'admin', '66.49.230.207', '2016-02-05 13:28:29pm', '', '47dadd9f86859e9a3d347bfa74721b33', '2016-02-05', 'AD'),
(911, 'admin', '68.55.116.144', '2016-02-05 13:30:55pm', '', 'af958cf4a8e7f67a97308d7f0da2953b', '2016-02-05', 'AD'),
(912, 'admin', '191.181.15.233', '2016-02-06 08:14:57am', '', '875f689702b454b08e832ec8e4ad57e0', '2016-02-06', 'AD'),
(913, 'admin', '66.49.241.175', '2016-02-08 08:34:58am', '', '8d4b18ba612ff6ccc430da0401dfb2ea', '2016-02-08', 'AD'),
(914, 'admin', '68.55.116.144', '2016-02-08 10:07:25am', '', '1dfd63783ef8a29d968c9f06ef4202f1', '2016-02-08', 'AD'),
(915, 'admin', '66.49.241.175', '2016-02-10 08:45:32am', '', '12b17808b7ef4c02a2847c482d8e079d', '2016-02-10', 'AD'),
(916, 'admin', '66.49.241.175', '2016-02-11 08:42:55am', '', '950f8163518dc318902309c12b0e8bed', '2016-02-11', 'AD'),
(917, 'admin', '66.49.241.175', '2016-02-11 12:02:53pm', '', 'b9ffcfed0a8edb5fdc8ccb315a35b54f', '2016-02-11', 'AD'),
(918, 'admin', '46.71.71.18', '2016-02-11 12:03:11pm', '', '6b54d861497e5f1ba9273ca95821eb5a', '2016-02-11', 'AD'),
(919, 'admin', '66.49.241.175', '2016-02-11 13:35:16pm', '', 'bf7f0b884fb099ad217302ac4e2a2538', '2016-02-11', 'AD'),
(920, 'admin', '66.49.241.175', '2016-02-11 13:59:24pm', '', '0d094ddba64426fc9b6e158fbc70f780', '2016-02-11', 'AD'),
(921, 'admin', '66.49.241.175', '2016-02-11 15:34:00pm', '', 'cb92be05fce3f9ede7add6841d56556a', '2016-02-11', 'AD'),
(922, 'admin', '66.49.241.175', '2016-02-11 15:41:24pm', '', 'cf4d1c392eb4c896f9510ba6a27876a9', '2016-02-11', 'AD'),
(923, 'admin', '46.71.51.249', '2016-02-11 15:45:01pm', '', '4fa9264f14f11469e997bdab9eb6e206', '2016-02-11', 'AD'),
(924, 'admin', '66.49.241.175', '2016-02-11 16:07:32pm', '', '133a2cc157ed5eb8cda4699919f2417e', '2016-02-11', 'AD'),
(925, 'admin', '123.236.192.153', '2016-02-15 10:27:28am', '', '7849eca257ce1e7e11c5965ccfe46530', '2016-02-15', 'AD'),
(926, 'admin', '99.224.27.68', '2016-02-15 10:28:24am', '', '7fb0b1a17063b9aaa3c93956d15fc31c', '2016-02-15', 'AD'),
(927, 'admin', '122.175.202.130', '2016-02-15 10:36:52am', '2016-02-15 11:11:08am', '303837f69be7c8f9eb01a59f6424fcbf', '2016-02-15', 'AD');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userrole`
--

CREATE TABLE IF NOT EXISTS `mst_userrole` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  `code` varchar(10) NOT NULL,
  `rolename` varchar(50) NOT NULL,
  PRIMARY KEY (`roleid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `mst_userrole`
--

INSERT INTO `mst_userrole` (`roleid`, `name`, `isactive`, `code`, `rolename`) VALUES
(1, 'Account Manager', '1', 'AM', 'Account Manager'),
(2, 'Quote Facilitator', '1', 'QF', 'Quote Facilitator'),
(3, 'Trigger Man', '1', 'TM', 'Trigger Man'),
(4, 'Administrator', '1', 'AD', 'Sub Administrator'),
(5, 'Administrator', '1', 'AD', 'Administrator'),
(18, 'Account Manager', '1', 'AM', 'Partner');

-- --------------------------------------------------------

--
-- Stand-in structure for view `offer_products`
--
CREATE TABLE IF NOT EXISTS `offer_products` (
`quotationno` varchar(11)
,`postno` varchar(6)
,`productid` int(11)
,`product_name` varchar(250)
,`productno` varchar(25)
,`industryid` int(11)
,`catid` int(11)
,`subcatid` int(11)
,`brandid` int(11)
,`industry` varchar(100)
,`category` varchar(100)
,`subcategory` varchar(100)
,`brand` varchar(100)
,`manufacture` varchar(100)
,`caseweight` varchar(100)
,`qtypercase` int(11)
,`pakaging` varchar(100)
,`shipingcondition` varchar(100)
,`code1` int(11)
,`codevalue1` varchar(100)
,`code2` int(11)
,`codevalue2` varchar(100)
,`code3` int(11)
,`codevalue3` varchar(100)
,`description` text
,`img0` varchar(100)
,`img1` varchar(100)
,`img2` varchar(100)
,`img3` varchar(100)
,`img4` varchar(100)
,`img5` varchar(100)
,`img6` varchar(100)
,`img7` varchar(100)
,`ptype` varchar(5)
,`targetprice` int(11)
,`currency` varchar(10)
,`uom` varchar(50)
,`quantity` int(11)
,`location` varchar(50)
,`expdate` varchar(50)
,`expirydate` varchar(50)
,`customerrefno` varchar(50)
,`post_advertisment_pakaging` text
,`post_advertisment_language` varchar(200)
,`post_advertisment_timeframe` varchar(50)
,`post_advertisment_country` varchar(200)
,`post_advertisment_userid` varchar(50)
,`post_advertisment_pdate` datetime
,`quoteid` int(11)
,`pdate` date
,`counter_date` datetime
,`offerstatus` varchar(20)
);
-- --------------------------------------------------------

--
-- Table structure for table `old_useractivitypurge`
--

CREATE TABLE IF NOT EXISTS `old_useractivitypurge` (
  `actionid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) NOT NULL,
  `ipaddress` varchar(25) NOT NULL,
  `sessionid` varchar(100) NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) NOT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=329 ;

--
-- Dumping data for table `old_useractivitypurge`
--

INSERT INTO `old_useractivitypurge` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`) VALUES
(1, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:04:15', 'Deleted SubCategory'),
(2, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:04:34', 'Added Brand'),
(3, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:04:39', 'Updated Brand'),
(4, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:04:43', 'Deleted Brand'),
(5, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:05:37', 'Export User Activity'),
(6, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:07:37', 'Export Products to CSV'),
(7, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:08:17', 'Review Product Code'),
(8, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:08:28', 'Review Product Code'),
(9, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 12:10:07', 'Suggested Product'),
(10, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 12:10:25', 'Suggested Product'),
(11, 'admin', '14.98.102.29', 'hvq4mmcki6ldlonvb892km7dn1', '2015-11-16', '2015-11-16 12:53:40', 'Deleted User Activity'),
(12, 'admin', '14.98.102.29', 'hvq4mmcki6ldlonvb892km7dn1', '2015-11-16', '2015-11-16 12:54:12', 'Deleted User Activity'),
(13, 'admin', '14.98.102.29', 'hvq4mmcki6ldlonvb892km7dn1', '2015-11-16', '2015-11-16 12:54:23', 'Deleted User Activity'),
(14, 'VUQQ-3341', '14.98.102.29', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', '2015-11-16 13:05:01', 'Logged In'),
(15, 'VUQQ-3341', '14.98.102.29', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', '2015-11-16 13:08:00', 'Submitted Offer'),
(16, 'VUQQ-3341', '14.98.102.29', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', '2015-11-16 13:08:00', 'Submitted Offer'),
(17, 'VUQQ-3341', '14.98.102.29', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', '2015-11-16 13:08:01', 'Submitted Offer'),
(18, 'VUQQ-3341', '14.98.102.29', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', '2015-11-16 13:08:01', 'Submitted Offer'),
(19, 'admin', '112.196.136.78', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-16', '2015-11-16 23:30:19', 'Logged In'),
(20, 'VUQQ-3341', '112.196.136.78', '1ba9tnf7jd8pi22io8dktjahl5', '2015-11-16', '2015-11-16 23:34:12', 'Logged In'),
(21, 'VUQQ-3341', '112.196.136.78', '1ba9tnf7jd8pi22io8dktjahl5', '2015-11-16', '2015-11-16 23:34:20', 'Logged Out'),
(22, 'RAUU-1599', '112.196.136.78', 'ejr8911h4cn3okr8k440upont5', '2015-11-16', '2015-11-16 23:34:40', 'Logged In'),
(23, 'RAUU-1599', '112.196.136.78', 'ejr8911h4cn3okr8k440upont5', '2015-11-16', '2015-11-16 23:34:54', 'Logged Out'),
(24, 'FMVI-7264', '112.196.136.78', 'r50n0qt3qoc8bfmv2pgjng70s6', '2015-11-16', '2015-11-16 23:35:03', 'Logged In'),
(25, 'FMVI-7264', '112.196.136.78', 'r50n0qt3qoc8bfmv2pgjng70s6', '2015-11-16', '2015-11-16 23:35:10', 'Logged Out'),
(26, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-16', '2015-11-16 23:35:28', 'Logged In'),
(27, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-16', '2015-11-16 23:37:41', 'Reposted Posting'),
(28, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-16', '2015-11-16 23:42:02', 'Updated Customer'),
(29, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-16', '2015-11-16 23:42:34', 'Added Customer'),
(30, 'admin', '112.196.136.78', '9r17f8tg3qj8tb25adni3nqdh3', '2015-11-17', '2015-11-17 00:18:04', 'Logged In'),
(31, 'admin', '112.196.136.78', '9r17f8tg3qj8tb25adni3nqdh3', '2015-11-17', '2015-11-17 00:18:25', 'Logged Out'),
(32, 'FMVI-7264', '112.196.136.78', 'jfraaufgioas6s8fqtboia7415', '2015-11-17', '2015-11-17 00:18:28', 'Logged In'),
(33, 'admin', '112.196.136.78', 'dklm8lgoe38utels85301d6r45', '2015-11-17', '2015-11-17 00:19:03', 'Logged In'),
(34, 'FMVI-7264', '112.196.136.78', 'jfraaufgioas6s8fqtboia7415', '2015-11-17', '2015-11-17 00:19:18', 'Logged Out'),
(35, 'FXIO-8905', '112.196.136.78', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', '2015-11-17 00:19:25', 'Logged In'),
(36, 'FXIO-8905', '112.196.136.78', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', '2015-11-17 00:19:44', 'Reposted Posting'),
(37, 'admin', '112.196.136.78', 'dklm8lgoe38utels85301d6r45', '2015-11-17', '2015-11-17 00:22:09', 'Updated Customer'),
(38, 'FXIO-8905', '112.196.136.78', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', '2015-11-17 00:22:53', 'Updated Customer'),
(39, 'FXIO-8905', '112.196.136.78', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', '2015-11-17 00:23:23', 'Added Customer'),
(40, 'admin', '112.196.136.78', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-17', '2015-11-17 01:19:28', 'Edited Product'),
(41, 'admin', '112.196.136.78', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-17', '2015-11-17 01:20:44', 'APPROVED New Product'),
(42, 'admin', '112.196.136.78', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-17', '2015-11-17 01:38:28', 'Edited Product'),
(43, 'admin', '112.196.136.78', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-17', '2015-11-17 01:38:52', 'APPROVED New Product'),
(44, 'admin', '112.196.136.78', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-17', '2015-11-17 02:16:58', 'Deleted User Activity'),
(45, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-17', '2015-11-17 02:28:28', 'Updated Posting'),
(46, 'admin', '112.196.136.78', 'dklm8lgoe38utels85301d6r45', '2015-11-17', '2015-11-17 03:06:26', 'Deleted User Activity'),
(47, 'FXIO-8905', '112.196.136.78', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', '2015-11-17 03:09:11', 'Added Product Code'),
(48, 'FXIO-8905', '112.196.136.78', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', '2015-11-17 03:10:07', 'Added Product Code'),
(49, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-17', '2015-11-17 03:12:49', 'Added Product Code'),
(50, 'FXIO-8905', '112.196.136.78', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-17', '2015-11-17 03:12:59', 'Logged Out'),
(51, 'FXIO-8905', '112.196.136.78', '6vemlkso76jl11katt8379dm17', '2015-11-17', '2015-11-17 03:15:32', 'Logged In'),
(52, 'FXIO-8905', '112.196.136.78', '6vemlkso76jl11katt8379dm17', '2015-11-17', '2015-11-17 03:17:18', 'Updated Posting'),
(53, 'admin', '66.49.237.16', 'br9qiggvb3dalpqu0m97n46h26', '2015-11-17', '2015-11-17 13:52:06', 'Logged In'),
(54, 'FXIO-8905', '66.49.237.16', 'e20gnmugh2hg4ous2agdsss0a5', '2015-11-17', '2015-11-17 13:52:24', 'Logged In'),
(55, 'admin', '66.49.237.16', 'mdfaprmtuh7mgor7813ndu5a01', '2015-11-17', '2015-11-17 13:56:14', 'Logged In'),
(56, 'admin', '66.49.237.16', 'mdfaprmtuh7mgor7813ndu5a01', '2015-11-17', '2015-11-17 13:56:53', 'Edited Product'),
(57, 'FXIO-8905', '66.49.237.16', 'e20gnmugh2hg4ous2agdsss0a5', '2015-11-17', '2015-11-17 14:01:23', 'Reposted Posting'),
(58, 'FXIO-8905', '66.49.237.16', 'e20gnmugh2hg4ous2agdsss0a5', '2015-11-17', '2015-11-17 14:01:44', 'Updated Posting'),
(59, 'admin', '14.98.228.196', 'ehe0ueugeloconu9696vfc4ps1', '2015-11-17', '2015-11-17 14:04:20', 'Logged In'),
(60, 'admin', '112.196.136.78', '1gpk18tpmglu7q0p9h6jdk03p0', '2015-11-17', '2015-11-17 23:43:11', 'Logged In'),
(61, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', '2015-11-17 23:44:53', 'Logged In'),
(62, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', '2015-11-17 23:45:57', 'Cancelled Posting'),
(63, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', '2015-11-17 23:46:12', 'Cancelled Posting'),
(64, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', '2015-11-17 23:49:52', 'Cancelled Posting'),
(65, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-17', '2015-11-17 23:53:01', 'Logged In'),
(66, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-17', '2015-11-17 23:57:01', 'Submitted Quotion'),
(67, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', '2015-11-17 23:57:35', 'Account Manager submitted a counter offer'),
(68, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-18', '2015-11-18 00:02:14', 'Quote Faciltiato submitted a Counter Offer'),
(69, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-18', '2015-11-18 00:03:52', 'Submitted Quotion'),
(70, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:13:00', 'Account Manager submitted a counter offer'),
(71, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-18', '2015-11-18 00:29:16', 'Quote Faciltiato submitted a Counter Offer'),
(72, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:29:46', 'Account Manager submitted a counter offer'),
(73, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-18', '2015-11-18 00:30:37', 'Quote Facilitator Accepted by Offer'),
(74, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-18', '2015-11-18 00:31:09', 'Submitted Quotion'),
(75, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:32:44', 'Account Manager submitted a counter offer'),
(76, 'admin', '112.196.136.78', '1gpk18tpmglu7q0p9h6jdk03p0', '2015-11-18', '2015-11-18 00:32:52', 'Logged Out'),
(77, 'ZJBX-0533', '112.196.136.78', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', '2015-11-18 00:33:00', 'Logged In'),
(78, 'ZJBX-0533', '112.196.136.78', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', '2015-11-18 00:33:20', 'Quote Facilitator Declined by Offer'),
(79, 'ZJBX-0533', '112.196.136.78', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', '2015-11-18 00:33:56', 'Submitted Quotion'),
(80, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:34:36', 'Offer accepted by Account Manager'),
(81, 'ZJBX-0533', '112.196.136.78', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', '2015-11-18 00:35:29', 'Submitted Quotion'),
(82, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:36:41', 'Account Manager submitted a counter offer'),
(83, 'ZJBX-0533', '112.196.136.78', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', '2015-11-18 00:37:20', 'Quote Faciltiato submitted a Counter Offer'),
(84, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:37:39', 'Offer declined by Account Manager'),
(85, 'FXIO-8905', '112.196.136.78', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-18', '2015-11-18 00:42:00', 'Logged Out'),
(86, 'admin', '112.196.136.78', 'u6t8cbq55sg0b8o13hcb3g6601', '2015-11-18', '2015-11-18 00:42:05', 'Logged In'),
(87, 'ZJBX-0533', '112.196.136.78', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', '2015-11-18 01:58:13', 'Logged Out'),
(88, 'admin', '112.196.136.78', '4ah8a76jcl8vpl3hre52srpg96', '2015-11-18', '2015-11-18 01:58:16', 'Logged In'),
(89, 'admin', '112.196.136.78', 'r347qec9vuf3ondr526hoqk7v6', '2015-11-18', '2015-11-18 04:03:55', 'Logged In'),
(90, 'admin', '112.196.136.78', 'r347qec9vuf3ondr526hoqk7v6', '2015-11-18', '2015-11-18 04:08:36', 'Logged Out'),
(91, 'admin', '112.196.136.78', 'd5kspsqs83dj18idkbcqr16dk7', '2015-11-18', '2015-11-18 04:09:11', 'Logged In'),
(92, 'FXIO-8905', '112.196.136.78', 'mt1iokkh3b18ms9hokpd6cs1k7', '2015-11-18', '2015-11-18 04:09:38', 'Logged In'),
(93, 'admin', '112.196.136.78', 'd5kspsqs83dj18idkbcqr16dk7', '2015-11-18', '2015-11-18 04:24:06', 'Edited Product'),
(94, 'admin', '112.196.136.78', 'd5kspsqs83dj18idkbcqr16dk7', '2015-11-18', '2015-11-18 04:34:55', 'Edited Product'),
(95, 'FXIO-8905', '112.196.136.78', 'mt1iokkh3b18ms9hokpd6cs1k7', '2015-11-18', '2015-11-18 05:02:46', 'Logged Out'),
(96, 'VUQQ-3341', '112.196.136.78', 'eqjbms2enugjjctqibkg85ki83', '2015-11-18', '2015-11-18 05:02:58', 'Logged In'),
(97, 'admin', '112.196.136.78', 'd5kspsqs83dj18idkbcqr16dk7', '2015-11-18', '2015-11-18 05:04:07', 'Logged Out'),
(98, 'FXIO-8905', '112.196.136.78', 't6h5ti0b9p2qlvvld3m5rmr873', '2015-11-18', '2015-11-18 05:04:21', 'Logged In'),
(99, 'ZJBX-0533', '112.196.136.78', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-18', '2015-11-18 05:06:59', 'Quote Faciltiato submitted a Counter Offer'),
(100, 'admin', '112.196.136.78', '7m3rqagoedqskbn82no2s4ikb4', '2015-11-18', '2015-11-18 05:18:33', 'Logged In'),
(101, 'FXIO-8905', '112.196.136.78', '6mahb719nspaadvi4r0gm0p6j5', '2015-11-18', '2015-11-18 05:23:49', 'Logged In'),
(102, 'VUQQ-3341', '112.196.136.78', 'eqjbms2enugjjctqibkg85ki83', '2015-11-18', '2015-11-18 05:45:56', 'Logged Out'),
(103, 'admin', '112.196.136.78', 'q0k3rid3tjlcuh82i4sr537hm3', '2015-11-18', '2015-11-18 05:45:58', 'Logged In'),
(104, 'admin', '112.196.136.78', 'q0k3rid3tjlcuh82i4sr537hm3', '2015-11-18', '2015-11-18 06:09:41', 'Logged Out'),
(105, 'VUQQ-3341', '112.196.136.78', '2au8d50qcl8rok01p1tle2ohc1', '2015-11-18', '2015-11-18 06:52:55', 'Logged In'),
(106, 'admin', '112.196.136.78', 'hh06imq98j8e8hp709ta5lq230', '2015-11-18', '2015-11-18 07:41:03', 'Logged In'),
(107, 'admin', '66.49.237.16', 'tgacm9t46s9ao4t54djeb61na2', '2015-11-18', '2015-11-18 15:11:56', 'Logged In'),
(108, 'admin', '66.49.237.16', 'tgacm9t46s9ao4t54djeb61na2', '2015-11-18', '2015-11-18 15:12:10', 'Logged Out'),
(109, 'FXIO-8905', '66.49.237.16', 'v9q0d10eu16vesjt9n37leloq7', '2015-11-18', '2015-11-18 15:12:12', 'Logged In'),
(110, 'FXIO-8905', '66.49.237.16', 'v9q0d10eu16vesjt9n37leloq7', '2015-11-18', '2015-11-18 15:14:13', 'Logged Out'),
(111, 'admin', '66.49.237.16', 'm1p02j9irmp1bc95hmqfphirr4', '2015-11-18', '2015-11-18 15:14:20', 'Logged In'),
(112, 'admin', '66.49.237.16', 'm1p02j9irmp1bc95hmqfphirr4', '2015-11-18', '2015-11-18 15:15:54', 'Edited Product'),
(113, 'admin', '66.49.237.16', 'm1p02j9irmp1bc95hmqfphirr4', '2015-11-18', '2015-11-18 15:17:43', 'Logged Out'),
(114, 'FXIO-8905', '66.49.237.16', 'kjero49jmns1u9lr9bk7umio87', '2015-11-18', '2015-11-18 15:17:46', 'Logged In'),
(115, 'admin', '112.196.136.78', 'qk3rgjb3v551g6tksvn5eoj1j2', '2015-11-19', '2015-11-19 03:56:50', 'Deleted User Activity'),
(116, 'admin', '112.196.136.78', 'qk3rgjb3v551g6tksvn5eoj1j2', '2015-11-19', '2015-11-19 04:26:24', 'Added Product Code'),
(117, 'FXIO-8905', '112.196.136.78', 'tcpqac5l96oph360eptgh3esk4', '2015-11-19', '2015-11-19 04:27:55', 'Added Product Code'),
(118, 'admin', '112.196.136.78', 'qk3rgjb3v551g6tksvn5eoj1j2', '2015-11-19', '2015-11-19 04:52:42', 'Logged Out'),
(119, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 04:52:52', 'Logged In'),
(120, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 04:53:23', 'Submitted Quotion'),
(121, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 05:41:01', 'Submitted Quotion'),
(122, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 05:43:19', 'Submitted Quotion'),
(123, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 05:56:22', 'Submitted Quotion'),
(124, 'FXIO-8905', '112.196.136.78', 'tcpqac5l96oph360eptgh3esk4', '2015-11-19', '2015-11-19 05:57:45', 'Account Manager submitted a counter offer'),
(125, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 05:58:59', 'Quote Faciltiato submitted a Counter Offer'),
(126, 'FXIO-8905', '112.196.136.78', 'tcpqac5l96oph360eptgh3esk4', '2015-11-19', '2015-11-19 05:59:53', 'Account Manager submitted a counter offer'),
(127, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 06:00:47', 'Quote Facilitator Accepted by Offer'),
(128, 'admin', '112.196.136.78', '7p5ur2b84g6avenupju21c0c76', '2015-11-19', '2015-11-19 06:03:54', 'Logged In'),
(129, 'admin', '112.196.136.78', '7p5ur2b84g6avenupju21c0c76', '2015-11-19', '2015-11-19 06:05:04', 'Logged Out'),
(130, 'FTTP-8399', '112.196.136.78', 'la3b6jrgtralecih1fe83kghs1', '2015-11-19', '2015-11-19 06:05:14', 'Logged In'),
(131, 'FXIO-8905', '104.131.14.167', 'dv7djbn4sh74aqqe5ofgo4iu23', '2015-11-19', '2015-11-19 06:15:06', 'Logged In'),
(132, 'FXIO-8905', '104.131.14.167', 'dv7djbn4sh74aqqe5ofgo4iu23', '2015-11-19', '2015-11-19 06:15:35', 'Logged Out'),
(133, 'VUQQ-3341', '104.131.14.167', 'cku1haicc9rkul5onsnvbpa9k0', '2015-11-19', '2015-11-19 06:15:45', 'Logged In'),
(134, 'VUQQ-3341', '104.131.14.167', 'cku1haicc9rkul5onsnvbpa9k0', '2015-11-19', '2015-11-19 06:16:01', 'Logged Out'),
(135, 'RAUU-1599', '104.131.14.167', '66rr53irvk4tps10ithtjbs1q6', '2015-11-19', '2015-11-19 06:16:11', 'Logged In'),
(136, 'RAUU-1599', '104.131.14.167', '66rr53irvk4tps10ithtjbs1q6', '2015-11-19', '2015-11-19 06:16:36', 'Logged Out'),
(137, 'FMVI-7264', '104.131.14.167', 'ktgdo9uos15925trlrov8dj031', '2015-11-19', '2015-11-19 06:16:46', 'Logged In'),
(138, 'FMVI-7264', '104.131.14.167', 'ktgdo9uos15925trlrov8dj031', '2015-11-19', '2015-11-19 06:18:44', 'Submitted Offer'),
(139, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 06:33:16', 'Quote Faciltiato submitted a Counter Offer'),
(140, 'FMVI-7264', '104.131.14.167', 'ktgdo9uos15925trlrov8dj031', '2015-11-19', '2015-11-19 06:36:06', 'Account Manager submitted a counter offer'),
(141, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 06:40:50', 'Quote Faciltiato submitted a Counter Offer'),
(142, 'FMVI-7264', '104.131.14.167', 'ktgdo9uos15925trlrov8dj031', '2015-11-19', '2015-11-19 06:47:50', 'Account Manager submitted a counter offer'),
(143, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 06:48:44', 'Quote Facilitator Accepted by Offer'),
(144, 'FTTP-8399', '112.196.136.78', 'ej0r0hs20dt9mg11j19h5c9464', '2015-11-19', '2015-11-19 06:52:24', 'Logged In'),
(145, 'FTTP-8399', '112.196.136.78', '0d0f2iqsffp13votlb6v4mg5c2', '2015-11-19', '2015-11-19 07:30:42', 'Logged In'),
(146, 'FTTP-8399', '112.196.136.78', 'i2iffn74gkfapdn98jdrvppho6', '2015-11-19', '2015-11-19 07:31:59', 'Logged In'),
(147, 'ZJBX-0533', '112.196.136.78', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', '2015-11-19 07:39:23', 'Logged Out'),
(148, 'admin', '112.196.136.78', 'cichfsdj7b4rn5rsk6dlevklt7', '2015-11-19', '2015-11-19 07:39:25', 'Logged In'),
(149, 'admin', '112.196.136.78', 'd95scbsigeoba8s31s29r0gr07', '2015-11-19', '2015-11-19 08:27:52', 'Logged In'),
(150, 'FXIO-8905', '112.196.136.78', 't49akj8e450t5ar389lh1qbc26', '2015-11-19', '2015-11-19 08:28:19', 'Logged In'),
(151, 'FXIO-8905', '112.196.136.78', 't49akj8e450t5ar389lh1qbc26', '2015-11-19', '2015-11-19 08:32:14', 'Added Posting'),
(152, 'admin', '112.196.136.78', 'd95scbsigeoba8s31s29r0gr07', '2015-11-19', '2015-11-19 08:39:53', 'Logged Out'),
(153, 'admin', '112.196.136.78', 'cp9dgte8r73rtf9suh6meiocc7', '2015-11-19', '2015-11-19 08:39:56', 'Logged In'),
(154, 'admin', '112.196.136.78', 'cp9dgte8r73rtf9suh6meiocc7', '2015-11-19', '2015-11-19 08:40:07', 'Logged Out'),
(155, 'FXIO-8905', '112.196.136.78', 'd918pe4apnggqsm6hks8tojal4', '2015-11-19', '2015-11-19 08:40:14', 'Logged In'),
(156, 'FXIO-8905', '112.196.136.78', 'd918pe4apnggqsm6hks8tojal4', '2015-11-19', '2015-11-19 08:43:59', 'Logged Out'),
(157, 'admin', '112.196.136.78', '5g6sj3bk4ac9kl0fs5dmdanq42', '2015-11-19', '2015-11-19 08:44:02', 'Logged In'),
(158, 'admin', '112.196.136.78', '5g6sj3bk4ac9kl0fs5dmdanq42', '2015-11-19', '2015-11-19 08:48:32', 'Edited Product'),
(159, 'admin', '112.196.136.78', '5g6sj3bk4ac9kl0fs5dmdanq42', '2015-11-19', '2015-11-19 08:50:08', 'Logged Out'),
(160, 'FXIO-8905', '112.196.136.78', 'jnjfiasb9da1g70hoad6dch7b5', '2015-11-19', '2015-11-19 08:50:19', 'Logged In'),
(161, 'admin', '66.49.237.16', 'nsb02t877lq967810j72l1hah7', '2015-11-19', '2015-11-19 15:58:53', 'Logged In'),
(162, 'admin', '66.49.237.16', 'ero1624mt3pgnmpvugt2ac8o32', '2015-11-19', '2015-11-19 16:02:11', 'Logged In'),
(163, 'FXIO-8905', '66.49.237.16', 'cbimgufa47qk74upafimimve75', '2015-11-19', '2015-11-19 16:03:44', 'Logged In'),
(164, 'FXIO-8905', '66.49.237.16', 'cbimgufa47qk74upafimimve75', '2015-11-19', '2015-11-19 16:04:51', 'Added Product Code'),
(165, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-19', '2015-11-19 23:58:30', 'Logged In'),
(166, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:02:21', 'Edited Product'),
(167, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:12:30', 'Edited Product'),
(168, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:13:07', 'Edited Product'),
(169, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:13:39', 'Edited Product'),
(170, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:15:47', 'Edited Product'),
(171, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:15:58', 'Edited Product'),
(172, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:18:32', 'Edited Product'),
(173, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:19:07', 'Edited Product'),
(174, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 00:27:51', 'Edited Product'),
(175, 'admin', '112.196.136.78', 'fkel2dnuiiknfjdi9015mm8s74', '2015-11-20', '2015-11-20 00:39:09', 'Logged In'),
(176, 'admin', '112.196.136.78', 'fkel2dnuiiknfjdi9015mm8s74', '2015-11-20', '2015-11-20 00:47:20', 'Export Products to CSV'),
(177, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 01:00:26', 'Logged In'),
(178, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 01:49:28', 'APPROVED New Product'),
(179, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:50:01', 'Edited Product'),
(180, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:50:40', 'Edited Product'),
(181, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:51:18', 'Edited Product'),
(182, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:51:42', 'Edited Product'),
(183, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:52:02', 'Edited Product'),
(184, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:52:12', 'Edited Product'),
(185, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:52:47', 'Edited Product'),
(186, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:53:04', 'Edited Product'),
(187, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:55:41', 'Edited Product'),
(188, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 01:56:09', 'Edited Product'),
(189, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 02:02:42', 'Edited Product'),
(190, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:04:07', 'APPROVED New Product'),
(191, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:04:16', 'APPROVED New Product'),
(192, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:04:25', 'APPROVED New Product'),
(193, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:04:34', 'APPROVED New Product'),
(194, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:04:45', 'APPROVED New Product'),
(195, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:04:54', 'APPROVED New Product'),
(196, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:05:03', 'APPROVED New Product'),
(197, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:05:11', 'APPROVED New Product'),
(198, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 02:05:25', 'APPROVED New Product'),
(199, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 02:05:52', 'Edited Product'),
(200, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 02:07:52', 'Edited Product'),
(201, 'admin', '112.196.136.78', 'vsm7lt9dust09a1o9ohl3pr3e2', '2015-11-10', '2015-11-10 04:11:18', 'Deleted User Activity'),
(202, 'admin', '112.196.136.78', 'vsm7lt9dust09a1o9ohl3pr3e2', '2015-11-10', '2015-11-10 04:45:26', 'Imported Products'),
(203, 'VUQQ-3341', '112.196.136.78', 'n8sulnehdmsgttebvrqa8nq187', '2015-11-10', '2015-11-10 07:03:16', 'Logged In'),
(204, 'admin', '112.196.136.78', 'a7l85cuntnjjhu1pcet3lshf06', '2015-11-10', '2015-11-10 07:41:52', 'Logged In'),
(205, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:31:04', 'Logged In'),
(206, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:40:19', 'Imported Industries'),
(207, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:40:27', 'Imported Categories'),
(208, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:40:48', 'Imported SubCategories'),
(209, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:41:09', 'Imported Industries'),
(210, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:41:14', 'Imported Categories'),
(211, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:41:25', 'Imported SubCategories'),
(212, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:41:32', 'Imported Brands'),
(213, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:41:50', 'Imported Products'),
(214, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:53:23', 'Imported Customers'),
(215, 'FXIO-8905', '66.49.237.16', '447rgidpk0pnp566g0kaeu0142', '2015-11-10', '2015-11-10 14:53:53', 'Logged In'),
(216, 'FXIO-8905', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:54:59', 'Updated User'),
(217, 'FXIO-8905', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:55:36', 'Updated User'),
(218, 'FXIO-8905', '66.49.237.16', '9aj8nrh02kcf715hsin0eb0sn2', '2015-11-10', '2015-11-10 14:56:09', 'Logged In'),
(219, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:57:42', 'Added Location'),
(220, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:58:09', 'Deleted Location'),
(221, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 14:59:36', 'Added Location'),
(222, 'FXIO-8905', '66.49.237.16', '9aj8nrh02kcf715hsin0eb0sn2', '2015-11-10', '2015-11-10 15:06:19', 'Logged Out'),
(223, 'RAUU-1599', '66.49.237.16', 'j97fdhr68u0si6214763h6mlo3', '2015-11-10', '2015-11-10 15:07:33', 'Logged In'),
(224, 'RAUU-1599', '66.49.237.16', 'j97fdhr68u0si6214763h6mlo3', '2015-11-10', '2015-11-10 15:07:48', 'Logged Out'),
(225, 'RAUU-1599', '66.49.237.16', 'p0eg98a5vbd0ttl0pp05jeq1v4', '2015-11-10', '2015-11-10 15:07:52', 'Logged In'),
(226, 'admin', '66.49.237.16', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', '2015-11-10 15:07:57', 'Logged Out'),
(227, 'RAUU-1599', '66.49.237.16', 'rkvacndllkpfff0vsaofh10pa4', '2015-11-10', '2015-11-10 15:08:00', 'Logged In'),
(228, 'RAUU-1599', '66.49.237.16', 'rkvacndllkpfff0vsaofh10pa4', '2015-11-10', '2015-11-10 15:08:03', 'Logged Out'),
(229, 'admin', '66.49.237.16', 'kgp5hukf1o09e26tts3v1q1c04', '2015-11-10', '2015-11-10 15:08:09', 'Logged In'),
(230, 'admin', '66.49.237.16', 'bttsits146cf9id7dldlaghc73', '2015-11-10', '2015-11-10 16:05:44', 'Logged In'),
(231, 'admin', '66.49.237.16', '4g2pevlpcgtcc3gjm47mojsqm1', '2015-11-10', '2015-11-10 16:06:14', 'Logged In'),
(232, 'admin', '112.196.136.78', '1235kmrko3ombedbcubd2tn2s6', '2015-11-11', '2015-11-11 01:35:26', 'Logged In'),
(233, 'admin', '112.196.136.78', '1235kmrko3ombedbcubd2tn2s6', '2015-11-11', '2015-11-11 01:38:18', 'Logged Out'),
(234, 'admin', '112.196.136.78', 'bb5f9m1m8hhfpf2spbdm59ehb5', '2015-11-11', '2015-11-11 01:38:22', 'Logged In'),
(235, 'admin', '112.196.136.78', 'bb5f9m1m8hhfpf2spbdm59ehb5', '2015-11-11', '2015-11-11 01:38:26', 'Logged Out'),
(236, 'admin', '112.196.136.78', 'ls7e06h52k43sikbv4mlndlf52', '2015-11-11', '2015-11-11 01:55:10', 'Logged In'),
(237, 'admin', '66.49.237.16', 'bfdjtj23ruql45j066emehp026', '2015-11-11', '2015-11-11 10:56:13', 'Logged In'),
(238, 'admin', '66.49.237.16', 'r6c39nvq98aegd2s1f04djr2i3', '2015-11-11', '2015-11-11 13:16:04', 'Logged In'),
(239, 'FXIO-8905', '66.49.237.16', 'i0fesmvivfvt3bs7e3sneod452', '2015-11-11', '2015-11-11 13:16:25', 'Logged In'),
(240, 'FXIO-8905', '66.49.237.16', 'i0fesmvivfvt3bs7e3sneod452', '2015-11-11', '2015-11-11 13:24:21', 'Added Posting'),
(241, 'FXIO-8905', '66.49.237.16', 'i0fesmvivfvt3bs7e3sneod452', '2015-11-11', '2015-11-11 13:25:44', 'Updated User Settings'),
(242, 'FXIO-8905', '66.49.237.16', 'i0fesmvivfvt3bs7e3sneod452', '2015-11-11', '2015-11-11 13:25:55', 'Logged Out'),
(243, 'FXIO-8905', '66.49.237.16', 'efcuqcbsr8ocorhneq9rqohhp3', '2015-11-11', '2015-11-11 13:26:07', 'Logged In'),
(244, 'FXIO-8905', '66.49.237.16', 'efcuqcbsr8ocorhneq9rqohhp3', '2015-11-11', '2015-11-11 13:26:32', 'Logged Out'),
(245, 'FXIO-8905', '66.49.237.16', 'o6q56paq2fe55qgvu1eon509e5', '2015-11-11', '2015-11-11 13:26:37', 'Logged In'),
(246, 'admin', '66.49.237.16', 'k04c0pmbt9dich8btsdimdssb3', '2015-11-12', '2015-11-12 12:47:56', 'Logged In'),
(247, 'admin', '59.161.188.117', 'jr51mje4ntp2ok0r4sefpfnv81', '2015-11-12', '2015-11-12 13:43:22', 'Logged In'),
(248, 'admin', '112.196.136.78', '9dc3abfa1ehjnek6fuel4ael27', '2015-11-14', '2015-11-14 01:06:41', 'Logged In'),
(249, 'admin', '112.196.136.78', 'vfetm6r3b2jh2iodo517mikts2', '2015-11-15', '2015-11-15 22:52:18', 'Logged In'),
(250, 'CXBZ-9999', '112.196.136.78', 'qdph3l3an4dko9vpbj440fdf77', '2015-11-15', '2015-11-15 23:16:17', 'Logged In'),
(251, 'CXBZ-9999', '112.196.136.78', 'qdph3l3an4dko9vpbj440fdf77', '2015-11-15', '2015-11-15 23:16:49', 'Logged Out'),
(252, 'FXIO-8905', '112.196.136.78', 'h04hspagligs15svcqulsmt0p4', '2015-11-15', '2015-11-15 23:17:07', 'Logged In'),
(253, 'FXIO-8905', '112.196.136.78', 'h04hspagligs15svcqulsmt0p4', '2015-11-15', '2015-11-15 23:20:02', 'Logged Out'),
(254, 'RAUU-1599', '112.196.136.78', 'k58401043c59cbipfal8433j85', '2015-11-15', '2015-11-15 23:20:19', 'Logged In'),
(255, 'admin', '112.196.136.78', 'vfetm6r3b2jh2iodo517mikts2', '2015-11-15', '2015-11-15 23:24:28', 'Logged Out'),
(256, 'admin', '112.196.136.78', '7n1i9s54430um74ji804tevq06', '2015-11-15', '2015-11-15 23:24:31', 'Logged In'),
(257, 'RAUU-1599', '112.196.136.78', 'k58401043c59cbipfal8433j85', '2015-11-15', '2015-11-15 23:46:05', 'Added Posting'),
(258, 'admin', '112.196.136.78', 'ukafcgmlai4jsofdhbdsb595l0', '2015-11-16', '2015-11-16 00:45:47', 'Logged In'),
(259, 'CXBZ-9999', '112.196.136.78', 'm671va9963d7jhb9hlj4v8q4h6', '2015-11-16', '2015-11-16 00:46:45', 'Logged In'),
(260, 'CXBZ-9999', '112.196.136.78', 'm671va9963d7jhb9hlj4v8q4h6', '2015-11-16', '2015-11-16 00:47:33', 'Logged Out'),
(261, 'VUQQ-3341', '112.196.136.78', 'vdfa54bcbf8sc8ne6s1vge0j96', '2015-11-16', '2015-11-16 00:47:40', 'Logged In'),
(262, 'admin', '112.196.136.78', '56qehkq203d0i4supmo4i6m525', '2015-11-16', '2015-11-16 03:12:34', 'Logged In'),
(263, 'admin', '112.196.136.78', '56qehkq203d0i4supmo4i6m525', '2015-11-16', '2015-11-16 03:47:00', 'Logged Out'),
(264, 'FXIO-8905', '112.196.136.78', '1rsveijddsnh74u9tfnjstljg4', '2015-11-16', '2015-11-16 03:47:12', 'Logged In'),
(265, 'RAUU-1599', '112.196.136.78', 'k58401043c59cbipfal8433j85', '2015-11-16', '2015-11-16 03:54:50', 'Logged Out'),
(266, 'admin', '112.196.136.78', '7cglicipko2l31adjons0q4td6', '2015-11-16', '2015-11-16 03:54:54', 'Logged In'),
(267, 'admin', '112.196.136.78', 'h3qeu0un4o3hlhgijiomjof6p4', '2015-11-16', '2015-11-16 04:11:19', 'Logged In'),
(268, 'admin', '112.196.136.78', 'kk94lf3niifr3updldiaooa130', '2015-11-16', '2015-11-16 05:22:31', 'Logged In'),
(269, 'admin', '112.196.136.78', '8jfuoq66e34434k5j4u2e5kbt0', '2015-11-16', '2015-11-16 06:24:57', 'Logged In'),
(270, 'admin', '112.196.136.78', '7cglicipko2l31adjons0q4td6', '2015-11-16', '2015-11-16 06:25:33', 'Logged Out'),
(271, 'ZJBX-0533', '112.196.136.78', '6hkpdr7arusjgeftrau4j1cft1', '2015-11-16', '2015-11-16 06:25:42', 'Logged In'),
(272, 'ZJBX-0533', '112.196.136.78', '0i01db9loqrsmujjr1smkiblm5', '2015-11-16', '2015-11-16 06:25:50', 'Logged In'),
(273, 'ZJBX-0533', '112.196.136.78', 't4vaa53uf0rjvmhu4vpp3fgfc0', '2015-11-16', '2015-11-16 06:26:08', 'Logged In'),
(274, 'admin', '112.196.136.78', '0oo84j98g81mq9pgtdhchasd13', '2015-11-16', '2015-11-16 06:26:56', 'Logged In'),
(275, 'admin', '112.196.136.78', '0oo84j98g81mq9pgtdhchasd13', '2015-11-16', '2015-11-16 06:27:26', 'Logged Out'),
(276, 'CXBZ-9999', '112.196.136.78', 'nop7kllulo89bi8vks2542fr06', '2015-11-16', '2015-11-16 06:27:34', 'Logged In'),
(277, 'admin', '112.196.136.78', '8jfuoq66e34434k5j4u2e5kbt0', '2015-11-16', '2015-11-16 06:33:54', 'Logged Out'),
(278, 'ZJBX-0533', '112.196.136.78', 'koqehudbc272jvaojgoel6rm84', '2015-11-16', '2015-11-16 06:34:02', 'Logged In'),
(279, 'admin', '112.196.136.78', '4vu049g0ejg66m97ngo5ilb6a1', '2015-11-16', '2015-11-16 07:05:58', 'Logged In'),
(280, 'CXBZ-9999', '112.196.136.78', 'h03o91njehip461pv3n76pqk05', '2015-11-16', '2015-11-16 07:06:48', 'Logged In'),
(281, 'CXBZ-9999', '112.196.136.78', 'h03o91njehip461pv3n76pqk05', '2015-11-16', '2015-11-16 07:07:18', 'Logged Out'),
(282, 'FTTP-8399', '112.196.136.78', 'gogthrs1snlaacgmmasrf25tc4', '2015-11-16', '2015-11-16 07:07:27', 'Logged In'),
(283, 'FTTP-8399', '112.196.136.78', 'gogthrs1snlaacgmmasrf25tc4', '2015-11-16', '2015-11-16 07:07:56', 'Logged Out'),
(284, 'ZJBX-0533', '112.196.136.78', 'cme3ai3ei712qd1uqmq01f8aa3', '2015-11-16', '2015-11-16 07:08:05', 'Logged In'),
(285, 'ZJBX-0533', '112.196.136.78', '0ggsunn4d4usr160ic612uk7g4', '2015-11-16', '2015-11-16 07:09:43', 'Logged In'),
(286, 'admin', '112.196.136.78', '4vu049g0ejg66m97ngo5ilb6a1', '2015-11-16', '2015-11-16 07:11:27', 'Logged Out'),
(287, 'ZJBX-0533', '112.196.136.78', '0ggsunn4d4usr160ic612uk7g4', '2015-11-16', '2015-11-16 07:11:38', 'Logged Out'),
(288, 'admin', '1.22.133.140', 'gd8p5qo6eon292jrs2e3qmola1', '2015-11-16', '2015-11-16 09:10:56', 'Logged In'),
(289, 'admin', '1.22.133.140', 'gd8p5qo6eon292jrs2e3qmola1', '2015-11-16', '2015-11-16 09:16:53', 'Logged Out'),
(290, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:28:28', 'Logged In'),
(291, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:36:24', 'Imported Industries'),
(292, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:36:33', 'Imported Categories'),
(293, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:36:40', 'Imported SubCategories'),
(294, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:36:50', 'Imported Brands'),
(295, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:40:58', 'Imported Products'),
(296, 'FXIO-8905', '66.49.237.16', 'the9315kk1tkk88odt6c0q9k81', '2015-11-16', '2015-11-16 11:44:19', 'Logged In'),
(297, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:44:40', 'Imported Customers'),
(298, 'FXIO-8905', '66.49.237.16', 'the9315kk1tkk88odt6c0q9k81', '2015-11-16', '2015-11-16 11:49:39', 'Added Posting'),
(299, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 11:52:27', 'Edited Product'),
(300, 'FXIO-8905', '66.49.237.16', 'the9315kk1tkk88odt6c0q9k81', '2015-11-16', '2015-11-16 11:55:49', 'Logged Out'),
(301, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:56:00', 'Logged In'),
(302, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:56:12', 'Updated User Settings'),
(303, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:56:26', 'Added Customer'),
(304, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:56:38', 'Updated Customer'),
(305, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:57:51', 'Updated Posting'),
(306, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:58:23', 'Cancelled Posting'),
(307, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:58:35', 'Reposted Posting'),
(308, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 11:59:31', 'Added Product Code'),
(309, 'FXIO-8905', '66.49.237.16', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', '2015-11-16 12:00:58', 'Reported Product'),
(310, 'CXBZ-9999', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:01:42', 'Updated User'),
(311, 'CXBZ-9999', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:01:54', 'Deleted User'),
(312, 'FMVI-7264', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:02:18', 'Added User'),
(313, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:02:53', 'Added Industry'),
(314, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:03:11', 'Updated Industry'),
(315, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:03:19', 'Deleted Industry'),
(316, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:03:40', 'Added Category'),
(317, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:03:46', 'Updated Category'),
(318, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:03:48', 'Deleted Category'),
(319, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:04:07', 'Added SubCategory'),
(320, 'admin', '66.49.237.16', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', '2015-11-16 12:04:13', 'Updated SubCategory'),
(321, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 02:32:24', 'Deleted User Activity'),
(322, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 03:11:21', 'Deleted User Activity'),
(323, 'admin', '104.236.57.54', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', '2015-11-20 03:11:54', 'Logged Out'),
(324, 'FXIO-8905', '104.236.57.54', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', '2015-11-20 03:12:07', 'Logged In'),
(325, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 03:12:34', 'Deleted User Activity'),
(326, 'FXIO-8905', '104.236.57.54', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', '2015-11-20 03:12:53', 'Added Product Code'),
(327, 'FXIO-8905', '104.236.57.54', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', '2015-11-20 03:23:03', 'Suggested Product Code'),
(328, 'admin', '112.196.136.78', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-20', '2015-11-20 03:24:11', 'WAITING New Product Code');

-- --------------------------------------------------------

--
-- Stand-in structure for view `oredr_products`
--
CREATE TABLE IF NOT EXISTS `oredr_products` (
`quotationno` varchar(11)
,`postno` varchar(6)
,`productid` int(11)
,`product_name` varchar(250)
,`productno` varchar(25)
,`industryid` int(11)
,`catid` int(11)
,`subcatid` int(11)
,`brandid` int(11)
,`industry` varchar(100)
,`category` varchar(100)
,`subcategory` varchar(100)
,`brand` varchar(100)
,`manufacture` varchar(100)
,`caseweight` varchar(100)
,`qtypercase` int(11)
,`pakaging` varchar(100)
,`shipingcondition` varchar(100)
,`code1` int(11)
,`codevalue1` varchar(100)
,`code2` int(11)
,`codevalue2` varchar(100)
,`code3` int(11)
,`codevalue3` varchar(100)
,`description` text
,`img0` varchar(100)
,`img1` varchar(100)
,`img2` varchar(100)
,`img3` varchar(100)
,`img4` varchar(100)
,`img5` varchar(100)
,`img6` varchar(100)
,`img7` varchar(100)
,`ptype` varchar(5)
,`targetprice` int(11)
,`currency` varchar(10)
,`uom` varchar(50)
,`quantity` int(11)
,`location` varchar(50)
,`expdate` varchar(50)
,`expirydate` varchar(50)
,`customerrefno` varchar(50)
,`post_advertisment_pakaging` text
,`post_advertisment_language` varchar(200)
,`post_advertisment_timeframe` varchar(50)
,`post_advertisment_country` varchar(200)
,`post_advertisment_userid` varchar(50)
,`post_advertisment_pdate` datetime
,`quoteid` int(11)
,`pdate` date
,`counter_date` datetime
,`offerstatus` varchar(20)
,`buyer` varchar(100)
,`seller` varchar(100)
,`quotefacility` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `page_master`
--

CREATE TABLE IF NOT EXISTS `page_master` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(50) NOT NULL,
  `p_show_name` varchar(100) NOT NULL,
  `p_path` varchar(100) NOT NULL,
  `p_key` varchar(100) NOT NULL,
  `p_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`p_id`),
  KEY `p_id` (`p_id`,`p_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `page_master`
--

INSERT INTO `page_master` (`p_id`, `p_name`, `p_show_name`, `p_path`, `p_key`, `p_status`) VALUES
(1, 'dashboard.php', 'Log In\n', 'dashboard.php', 'ndashboard', 1),
(2, 'logout.php', 'Log Out\r\n', 'logout.php', 'nlogout', 1),
(3, 'my-settings.php', 'Changed Settings', 'my-settings.php', 'nsetting', 1),
(4, 'mstcustomer.php', 'Added New Customer', 'mstcustomer.php', 'ncustomer', 1),
(5, 'mstcustomer.php', 'Edited Customer', 'mstcustomer.php', 'upcustomer', 1),
(6, 'mstcustomer.php', 'Deleted Customer\r\n', 'mstcustomer.php', 'dlcustomer', 1),
(7, 'create-add.php', 'Added New Posting\r\n', 'create-add.php', 'addpost', 1),
(8, 'edit-add.php', 'Edited Posting\r\n', 'edit-add.php', 'upposting', 1),
(9, 'my-posting.php', 'Canceled Posting\r\n', 'my-posting.php', 'upposting', 1),
(10, 'myposting-detail-view.php', 'Reposted Posting\r\n', 'myposting-detail-view.php', 'reposting', 1),
(11, 'myposting-detail-view.php', 'Accepted Offer', 'myposting-detail-view.php', 'accoffer', 1),
(12, 'myposting-detail-view.php', 'Declined Offer\r\n', 'myposting-detail-view.php', 'decoffer', 1),
(13, 'myposting-detail-view.php', 'Submitted Counter Offer\r\n', 'myposting-detail-view.php', 'countoffer', 1),
(14, 'posting-detail-view.php', 'Submitted Quote\r\n', 'posting-detail-view.php', 'subquots', 1),
(15, 'import-data.php', 'Imported New Products\r\n', 'import-data.php', 'impnewproduct', 1),
(16, 'detail-product-view.php', 'Reported Product\r\n', 'detail-product-view.php', 'reportpro', 1),
(17, 'detail-product-view.php', 'Suggested New Product Code\r\n', 'detail-product-view.php', 'newprocode', 1),
(18, 'suggest-product-review.php', 'Approved New Product\r\n', 'suggest-product-review.php', 'approvepro', 1),
(19, 'suggest-product-review.php', 'Rejected New Product\r\n', 'suggest-product-review.php', 'rejnewpro', 1),
(20, 'product-codes.php', 'Approved New Product Code\r\n', 'product-codes.php', 'apprprocode', 1),
(21, 'product-codes.php', 'Rejected New Product Code\r\n', 'product-codes.php', 'rejnewprocode', 1),
(22, 'product-listdownload.php', 'Downloaded Products List\r\n', 'product-listdownload.php', 'prolistdownld', 1),
(23, 'user-setting.php', 'Created New User\r\n', 'user-setting.php', 'createuser', 1),
(24, 'edit-user.php', 'Edited User\r\n', 'edit-user.php', 'edtuser', 1),
(25, 'userdetail.php', 'Deleted User\r\n', 'userdetail.php', 'dltuser', 1),
(26, 'useractivity.php', 'Purged User Activity\r\n', 'useractivity.php', 'prguactivity', 1),
(27, 'useractivity.php', 'Exported User Activity\r\n', 'useractivity.php', 'expuactivity', 1),
(29, 'import-data.php', 'Imported Products\r\n', 'import-data.php', 'imppro', 1),
(30, 'import-data.php', 'Imported Industries\r\n', 'import-data.php', 'impind', 1),
(31, 'import-data.php', 'Imported Categories\r\n', 'import-data.php', 'impcat', 1),
(32, 'import-data.php', 'Imported Sub-Categories\r\n', 'import-data.php', 'impsubcat', 1),
(33, 'import-data.php', 'Imported Brands\r\n', 'import-data.php', 'impbrnd', 1),
(34, 'import-data.php', 'Imported Customers\r\n', 'import-data.php', 'impcust', 1),
(35, 'mstindustry.php', 'Added New Industry\r\n', 'mstindustry.php', 'addnindustry', 1),
(36, 'mstindustry.php', 'Edited Industry\r\n', 'mstindustry.php', 'edtindustry', 1),
(37, 'mstindustry.php', 'Deleted Industry\r\n', 'mstindustry.php', 'dltindustry', 1),
(38, 'mstcategory.php', 'Added New Category\r\n', 'mstcategory.php', 'addnewcat', 1),
(39, 'mstcategory.php', 'Edited Category\r\n', 'mstcategory.php', 'upcat', 1),
(40, 'mstsubcategory.php', 'Added New Sub-Category\r\n', 'mstsubcategory.php', 'addsubcat', 1),
(41, 'mstcategory.php', 'Deleted Category\r\n', 'mstcategory.php', 'delcat', 1),
(42, 'mstsubcategory.php', 'Edited Sub-Category\r\n', 'mstsubcategory.php', 'editsubcat', 1),
(43, 'mstsubcategory.php', 'Deleted Sub-Category\r\n', 'mstsubcategory.php', 'dltsubcat', 1),
(44, 'mstbrand.php', 'Added New Brand\r\n', 'mstbrand.php', 'addnbrnd', 1),
(45, 'mstbrand.php', 'Edited Brand\r\n', 'mstbrand.php', 'updbrnd', 1),
(46, 'mstbrand.php', 'Deleted Brand\r\n', 'mstbrand.php', 'delbrnd', 1),
(47, 'my-settings.php', 'Log In', 'my-settings.php', 'ndashboard', 1),
(48, 'quotepost-detail-view.php', 'Quote Submit By QF', 'quotepost-detail-view.php', 'quotesubbyqf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_notification`
--

CREATE TABLE IF NOT EXISTS `page_notification` (
  `n_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `p_key` varchar(50) NOT NULL,
  `n_type` varchar(50) NOT NULL,
  `n_msg` varchar(200) NOT NULL,
  `n_variable` varchar(50) NOT NULL,
  `final_noti` varchar(200) NOT NULL,
  `roletype` varchar(20) NOT NULL,
  `n_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`n_id`),
  KEY `user_role` (`roletype`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `page_notification`
--

INSERT INTO `page_notification` (`n_id`, `p_id`, `p_key`, `n_type`, `n_msg`, `n_variable`, `final_noti`, `roletype`, `n_status`) VALUES
(15, 1, 'ndashboard', 'visual', 'Logged in successfully', '', '', 'AM', 1),
(2, 1, 'ndashboard', 'audio', 'Logged in successfully', '', '', 'QF', 1),
(3, 1, 'ndashboard', 'visual', 'Logged in successfully', '', '', 'TM', 1),
(4, 9, 'upposting', 'visual', 'Post Edited Successfully BY QF', '', '', 'QF', 1),
(14, 9, 'upposting', 'visual', 'Posting Update successfully By Admin', '', '', 'AD', 1),
(7, 7, 'addpost', 'audio', 'Ad Created  Successfully', '', '', 'AM', 1),
(10, 47, 'ndashboard', 'visual', 'Logged in successfully', '', '', 'AD', 1),
(12, 3, 'nsetting', 'visual', 'Successfully updated User settings', '', '', 'AM', 1),
(13, 3, 'nsetting', 'visual', 'Successfully updated User settings', '', '', 'AD', 1),
(16, 7, 'addpost', 'visual', 'Posting added by admin successfully', '', '', 'AD', 1),
(17, 9, 'upposting', 'visual', 'Posting Updated Successfully', '', '', 'AM', 1),
(18, 4, 'ncustomer', 'visual', 'Customer Added successfully', '', '', 'AM', 1),
(19, 4, 'ncustomer', 'visual', 'Customer added successfully', '', '', 'AD', 1),
(20, 5, 'upcustomer', 'visual', 'Customer status has successfully been updated.', '', '', 'AD', 1),
(21, 5, 'upcustomer', 'visual', 'Customer status has successfully been updated.', '', '', 'AM', 1),
(22, 18, 'approvepro', 'visual', 'Product approved successfully', '', '', 'AD', 1),
(23, 19, 'rejnewpro', 'visual', 'Product rejected Successfully', '', '', 'AD', 1),
(24, 48, 'quotesubbyqf', 'visual', 'Successfully Submitted Initial Offer', '', '', 'QF', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_advertisment`
--

CREATE TABLE IF NOT EXISTS `post_advertisment` (
  `postid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) NOT NULL,
  `pdate` datetime NOT NULL,
  `productno` varchar(25) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `subcategory` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `ptype` varchar(5) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `uom` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `expdate` varchar(50) NOT NULL,
  `expirydate` varchar(50) DEFAULT NULL,
  `customerrefno` varchar(50) NOT NULL,
  `pakaging` text NOT NULL,
  `language` varchar(200) NOT NULL,
  `timeframe` varchar(50) NOT NULL,
  `country` varchar(200) NOT NULL,
  `pstatus` varchar(25) NOT NULL,
  `targetprice` int(11) NOT NULL,
  `postno` varchar(6) NOT NULL,
  `tempid` varchar(100) NOT NULL,
  `archivepost` varchar(10) NOT NULL DEFAULT '0',
  `buyer` varchar(100) DEFAULT NULL,
  `seller` varchar(100) DEFAULT NULL,
  `quotefacility` varchar(100) NOT NULL,
  `act_inactive` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=active,1=inactive',
  PRIMARY KEY (`postid`),
  KEY `pdate` (`pdate`,`productno`,`ptype`,`location`,`expdate`,`expirydate`,`customerrefno`,`language`,`timeframe`,`country`,`postno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_customer`
--

CREATE TABLE IF NOT EXISTS `post_customer` (
  `custid` int(11) NOT NULL AUTO_INCREMENT,
  `refno` varchar(50) NOT NULL,
  `edate` date NOT NULL,
  `refuserid` varchar(25) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT '1',
  `roletype` varchar(10) NOT NULL,
  `adduserid` varchar(10) NOT NULL,
  PRIMARY KEY (`custid`),
  KEY `refuserid` (`refuserid`),
  KEY `refno` (`refno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `post_products`
--
CREATE TABLE IF NOT EXISTS `post_products` (
`postno` varchar(6)
,`productid` int(11)
,`product_name` varchar(250)
,`productno` varchar(25)
,`industryid` int(11)
,`catid` int(11)
,`subcatid` int(11)
,`brandid` int(11)
,`industry_name` varchar(100)
,`category_name` varchar(100)
,`subcategory_name` varchar(100)
,`brand_name` varchar(100)
,`manufacture` varchar(100)
,`caseweight` varchar(100)
,`qtypercase` int(11)
,`pakaging` varchar(100)
,`shipingcondition` varchar(100)
,`code1` int(11)
,`codevalue1` varchar(100)
,`code2` int(11)
,`codevalue2` varchar(100)
,`code3` int(11)
,`codevalue3` varchar(100)
,`description` text
,`img0` varchar(100)
,`img1` varchar(100)
,`img2` varchar(100)
,`img3` varchar(100)
,`img4` varchar(100)
,`img5` varchar(100)
,`img6` varchar(100)
,`img7` varchar(100)
,`ptype` varchar(5)
,`targetprice` int(11)
,`currency` varchar(10)
,`uom` varchar(50)
,`quantity` int(11)
,`location` varchar(50)
,`expdate` varchar(50)
,`expirydate` varchar(50)
,`customerrefno` varchar(50)
,`post_advertisment_pakaging` text
,`post_advertisment_language` varchar(200)
,`post_advertisment_timeframe` varchar(50)
,`post_advertisment_country` varchar(200)
,`post_advertisment_userid` varchar(50)
,`pdate` date
,`post_advertisment_pstatus` varchar(25)
);
-- --------------------------------------------------------

--
-- Table structure for table `post_quotation`
--

CREATE TABLE IF NOT EXISTS `post_quotation` (
  `quoteid` int(11) NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(11) NOT NULL,
  `postno` varchar(6) NOT NULL,
  `productno` varchar(25) NOT NULL,
  `price` bigint(14) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `uom` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `expdate` varchar(50) NOT NULL,
  `expirydate` datetime NOT NULL,
  `language` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `timeframe` varchar(50) NOT NULL,
  `detail` varchar(100) NOT NULL,
  `declinemsg` varchar(100) NOT NULL,
  `offerdeclinemsg` varchar(50) NOT NULL,
  `acceptmsg` varchar(100) NOT NULL,
  `offeracceptmsg` varchar(50) NOT NULL,
  `offercrdate` datetime NOT NULL,
  `userid` varchar(50) NOT NULL,
  `quationstatus` varchar(25) NOT NULL,
  `offerstatus` varchar(20) NOT NULL,
  `accdate` date DEFAULT NULL,
  `rejdate` date DEFAULT NULL,
  `lastcntdate` date DEFAULT NULL,
  `isactive` varchar(2) NOT NULL DEFAULT '1',
  `counetr_status` varchar(50) NOT NULL,
  `offerCounterStatus` varchar(50) NOT NULL,
  `postuserid` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `readunreadst` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`quoteid`),
  KEY `quoteid` (`quoteid`,`quotationno`,`postno`,`productno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productimport`
--

CREATE TABLE IF NOT EXISTS `productimport` (
  `productid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `industry` varchar(200) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `subcat` varchar(200) DEFAULT NULL,
  `brand` varchar(150) DEFAULT NULL,
  `manufacturer` varchar(100) DEFAULT NULL,
  `caseweight` text,
  `packaging` text,
  `shippingcond` text,
  `qtypercase` bigint(20) DEFAULT NULL,
  `des` text,
  `mpm` float DEFAULT NULL,
  `prefixone` text,
  `codeone` varchar(50) DEFAULT NULL,
  `prefixtwo` bit(1) DEFAULT NULL,
  `codetwo` varchar(50) DEFAULT NULL,
  `prefixthree` varchar(50) DEFAULT NULL,
  `codethree` text,
  `img1` text,
  `img2` text,
  `img3` text,
  `img4` text,
  `img5` text,
  `img6` text,
  `img7` text,
  `img8` text,
  `addedby` varchar(200) NOT NULL,
  `roletype` varchar(200) NOT NULL,
  `pstatus` varchar(200) NOT NULL,
  PRIMARY KEY (`productid`),
  KEY `name` (`name`(255),`industry`,`category`,`subcat`,`brand`),
  KEY `brandid` (`category`),
  KEY `subcatid` (`brand`),
  KEY `industryid` (`industry`),
  KEY `name_2` (`name`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=331 ;

--
-- Dumping data for table `productimport`
--

INSERT INTO `productimport` (`productid`, `productno`, `date`, `name`, `industry`, `category`, `subcat`, `brand`, `manufacturer`, `caseweight`, `packaging`, `shippingcond`, `qtypercase`, `des`, `mpm`, `prefixone`, `codeone`, `prefixtwo`, `codetwo`, `prefixthree`, `codethree`, `img1`, `img2`, `img3`, `img4`, `img5`, `img6`, `img7`, `img8`, `addedby`, `roletype`, `pstatus`) VALUES
(1, 'OOM-879-322', '2016-02-11', 'Juvederm Hydrate', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '1 x 1,0 ml', '', 0, 'Juvederm Hydrate', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(2, 'RLR-616-773', '2016-02-11', 'Juvederm Ultra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0.8ml', '', 0, 'Juvederm Ultra', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(3, 'SFF-244-983', '2016-02-11', 'Juvederm Ultra 2', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,55 ml', '', 0, 'Juvederm Ultra 2', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(4, 'UQX-055-188', '2016-02-11', 'Juvederm Ultra 3', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Ultra 3', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(5, 'FSV-428-527', '2016-02-11', 'Juvederm Ultra 4', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Ultra 4', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(6, 'TNL-356-586', '2016-02-11', 'Juvederm Ultra Smile', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,55 ml', '', 0, 'Juvederm Ultra Smile', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(7, 'KUE-909-603', '2016-02-11', 'Juvederm Ultra XC', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1 ml', '', 0, 'Juvederm Ultra XC', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(8, 'TJU-458-424', '2016-02-11', 'Juvederm Ultra XC Plus', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1 ml', '', 0, 'Juvederm Ultra XC Plus', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(9, 'RWB-455-136', '2016-02-11', 'Juvederm Ultra+', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0.8ml', '', 0, 'Juvederm Ultra+', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(10, 'SCZ-306-244', '2016-02-11', 'Juvederm Volbella w. Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '1 x 1,0ml', '', 0, 'Juvederm Volbella w. Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(11, 'BGO-146-666', '2016-02-11', 'Juvederm Volift w. lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Volift w. lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(12, 'QZV-959-313', '2016-02-11', 'Juvederm Voluma w. Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Voluma w. Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(13, 'AWT-377-729', '2016-02-11', 'Surgiderm 18', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 18', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(14, 'SXL-560-939', '2016-02-11', 'Surgiderm 24 XP', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 24 XP', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(15, 'SKU-759-260', '2016-02-11', 'Surgiderm 30', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 30', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(16, 'HOF-802-922', '2016-02-11', 'Surgiderm 30 XP', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 30 XP', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(17, 'REB-564-344', '2016-02-11', 'Orthovisc', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Anika Therapeutics', 'Anika Therapeutics', '', '1 x 2,0ml', '', 0, 'Orthovisc', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(18, 'UQB-479-544', '2016-02-11', 'Ellanse-E', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-E', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(19, 'DFF-857-030', '2016-02-11', 'Ellanse-L', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-L', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(20, 'TXX-905-045', '2016-02-11', 'Ellanse-M', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-M', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(21, 'CQZ-007-410', '2016-02-11', 'Ellanse-M', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 0,5 ml', '', 0, 'Ellanse-M', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(22, 'WVZ-276-605', '2016-02-11', 'Ellanse-M Hands', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-M Hands', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(23, 'KWK-668-039', '2016-02-11', 'Ellanse-S', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-S', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(24, 'BFY-088-843', '2016-02-11', 'Ellanse-S', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 0,5 ml', '', 0, 'Ellanse-S', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(25, 'LEF-657-181', '2016-02-11', 'Ellanse-S Hands', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-S Hands', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(26, 'IAC-777-114', '2016-02-11', 'Biodermis  PRO SIL 17 GR', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '17 gr', '', 0, 'Biodermis  PRO SIL 17 GR', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(27, 'WIJ-232-669', '2016-02-11', 'Biodermis  PRO SIL 4.25 GR', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '4.25 gr', '', 0, 'Biodermis  PRO SIL 4.25 GR', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(28, 'GCN-538-723', '2016-02-11', 'Biodermis 1.25 x 1.25 '''' ( 3.17 x 3.17 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis 1.25 x 1.25 '''' ( 3.17 x 3.17 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(29, 'AAV-593-036', '2016-02-11', 'Biodermis 12.6'''' x 3.8'''' (32 x 9.63 cm) Mastopexy', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis 12.6'''' x 3.8'''' (32 x 9.63 cm) Mastopexy', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(30, 'SQL-503-971', '2016-02-11', 'Biodermis 2.75'''' round  (6.98 x 1.90 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis 2.75'''' round  (6.98 x 1.90 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(31, 'KZJ-193-421', '2016-02-11', 'Biodermis 3'''' round  (7.62 x 1.90 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis 3'''' round  (7.62 x 1.90 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(32, 'ODM-252-048', '2016-02-11', 'Biodermis EPI NET 2 X 60''''', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis EPI NET 2 X 60''''', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(33, 'KGB-094-844', '2016-02-11', 'Biodermis EPI TAPE 2 X 10', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis EPI TAPE 2 X 10', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(34, 'USK-191-811', '2016-02-11', 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14,5 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 5', '', 0, 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14,5 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(35, 'EET-745-375', '2016-02-11', 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14.5 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14.5 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(36, 'TDO-284-225', '2016-02-11', 'Biodermis silicone 1.4 x 12.0'''' (3.55 x 30.48 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'box of 2', '', 0, 'Biodermis silicone 1.4 x 12.0'''' (3.55 x 30.48 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(37, 'GFR-175-797', '2016-02-11', 'Biodermis silicone 1.4 x 6.0'''' ( 3.6 x 15 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis silicone 1.4 x 6.0'''' ( 3.6 x 15 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(38, 'IYR-458-076', '2016-02-11', 'Biodermis silicone 11 x 16'''' (27.94 x 40.64 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis silicone 11 x 16'''' (27.94 x 40.64 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(39, 'NUC-734-665', '2016-02-11', 'Biodermis silicone 2 x 2.5 '''' (5 x 6 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis silicone 2 x 2.5 '''' (5 x 6 cm)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(40, 'BZS-662-164', '2016-02-11', 'Biodermis SILQUE CLENZ', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '28.4 ml', '', 0, 'Biodermis SILQUE CLENZ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(41, 'OZX-307-872', '2016-02-11', 'Biodermis XERAGEL', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '10 ML', '', 0, 'Biodermis XERAGEL', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(42, 'JJZ-718-969', '2016-02-11', 'Princess Filler', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Croma-Pharma GmbH', 'Croma-Pharma GmbH', '', '1 x 1,0 ml', '', 0, 'Princess Filler', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(43, 'GVL-655-507', '2016-02-11', 'Princess Rich', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Croma-Pharma GmbH', 'Croma-Pharma GmbH', '', '1 x 1,0 ml', '', 0, 'Princess Rich', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(44, 'MRG-576-045', '2016-02-11', 'Princess Volume', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Croma-Pharma GmbH', 'Croma-Pharma GmbH', '', '1 x 1,0 ml', '', 0, 'Princess Volume', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(45, 'LEF-531-347', '2016-02-11', 'Euflexxa', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Ferring', 'Ferring', '', '3 x 2,0ml', '', 0, 'Euflexxa', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(46, 'FZG-820-199', '2016-02-11', 'Hyalgan', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Fidia', 'Fidia', '', '1 x 2,0ml', '', 0, 'Hyalgan', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(47, 'TIU-450-815', '2016-02-11', 'Filorga M-HA 10', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '3', '', 0, 'Filorga M-HA 10', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(48, 'ELG-926-547', '2016-02-11', 'Filorga M-HA 18', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '2', '', 0, 'Filorga M-HA 18', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(49, 'OQL-430-845', '2016-02-11', 'Filorga NCTF 135', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '5', '', 0, 'Filorga NCTF 135', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(50, 'QMK-321-595', '2016-02-11', 'Filorga NCTF 135HA ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '5', '', 0, 'Filorga NCTF 135HA ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(51, 'HUE-442-246', '2016-02-11', 'Filorga X-HA 3', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '2', '', 0, 'Filorga X-HA 3', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(52, 'ZCA-136-365', '2016-02-11', 'Filorga X-HA Volume, 1ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '2', '', 0, 'Filorga X-HA Volume, 1ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(53, 'GKE-600-414', '2016-02-11', 'Durolane', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Galderma', 'Galderma', '', '1 x 3,0ml', '', 0, 'Durolane', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(54, 'TMI-573-341', '2016-02-11', 'Macrolane VRF20', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 10ml', '', 0, 'Macrolane VRF20', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(55, 'AMQ-433-861', '2016-02-11', 'Macrolane VRF20 ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 20ml', '', 0, 'Macrolane VRF20 ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(56, 'QEM-487-805', '2016-02-11', 'Macrolane VRF30', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 10ml', '', 0, 'Macrolane VRF30', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(57, 'DHS-219-069', '2016-02-11', 'Macrolane VRF30 ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 20ml', '', 0, 'Macrolane VRF30 ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(58, 'ZTA-188-879', '2016-02-11', 'Restylane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(59, 'BWM-018-195', '2016-02-11', 'Restylane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(60, 'MGA-339-121', '2016-02-11', 'Restylane Day Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Day Cream', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(61, 'NUU-999-270', '2016-02-11', 'Restylane Day Creme SPF 15', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Day Creme SPF 15', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(62, 'KWV-970-897', '2016-02-11', 'Restylane Eye Serum', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 15 ml', '', 0, 'Restylane Eye Serum', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(63, 'DXX-940-857', '2016-02-11', 'Restylane Facial Cleanser', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 100 ml', '', 0, 'Restylane Facial Cleanser', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(64, 'ECL-564-002', '2016-02-11', 'Restylane Hand Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Hand Cream', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(65, 'FGC-327-920', '2016-02-11', 'Restylane Lipp Refresh with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Lipp Refresh with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(66, 'RHB-859-197', '2016-02-11', 'Restylane Lipp Volume with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Lipp Volume with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(67, 'RII-420-677', '2016-02-11', 'Restylane Night Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Night Cream', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(68, 'HPF-390-565', '2016-02-11', 'Restylane Night Serum', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 15 ml', '', 0, 'Restylane Night Serum', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(69, 'RZK-795-120', '2016-02-11', 'Restylane Perlane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Perlane', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(70, 'WJR-061-459', '2016-02-11', 'Restylane Perlane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane Perlane', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(71, 'SMW-595-053', '2016-02-11', 'Restylane Perlane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Perlane with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(72, 'BLR-091-849', '2016-02-11', 'Restylane Perlane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane Perlane with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(73, 'UOG-948-796', '2016-02-11', 'Restylane Recover Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 25 ml', '', 0, 'Restylane Recover Cream', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(74, 'NUH-603-782', '2016-02-11', 'Restylane Sub-Q with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Sub-Q with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(75, 'BHU-325-092', '2016-02-11', 'Restylane Vital', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(76, 'OWX-558-313', '2016-02-11', 'Restylane Vital injector', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital injector', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(77, 'AAN-307-686', '2016-02-11', 'Restylane Vital injector with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital injector with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(78, 'DVZ-606-400', '2016-02-11', 'Restylane Vital Light', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital Light', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(79, 'JBZ-423-904', '2016-02-11', 'Restylane Vital light injector', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital light injector', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(80, 'EYU-449-536', '2016-02-11', 'Restylane Vital Light injector with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital Light injector with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(81, 'MQY-935-157', '2016-02-11', 'Restylane Vital Light with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital Light with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(82, 'HME-900-292', '2016-02-11', 'Restylane Vital w. Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital w. Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(83, 'GSH-903-195', '2016-02-11', 'Restylane Whitening Day Creme', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Whitening Day Creme', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(84, 'SHA-885-915', '2016-02-11', 'Restylane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(85, 'GIB-041-839', '2016-02-11', 'Restylane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane with Lidocaine', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(86, 'QVP-194-484', '2016-02-11', 'Synvisc Classic', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Genzyme', 'Genzyme', '', '3 x 2,0ml', '', 0, 'Synvisc Classic', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(87, 'NZO-999-535', '2016-02-11', 'Synvisc One', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Genzyme', 'Genzyme', '', '1 x 6,0ml', '', 0, 'Synvisc One', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(88, 'FFX-904-765', '2016-02-11', 'Perfectha Derm', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 1,0ml', '', 0, 'Perfectha Derm', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(89, 'YDC-730-801', '2016-02-11', 'Perfectha Derm Deep ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 1,0ml', '', 0, 'Perfectha Derm Deep ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(90, 'QSE-590-371', '2016-02-11', 'Perfectha Derm FineLines', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 0,5ml', '', 0, 'Perfectha Derm FineLines', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(91, 'PBO-849-940', '2016-02-11', 'Perfectha Derm Subskin', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 3,0ml', '', 0, 'Perfectha Derm Subskin', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(92, 'TZJ-088-896', '2016-02-11', 'Belotero Basic', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,0 ml', '', 0, 'Belotero Basic', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(93, 'VQX-055-560', '2016-02-11', 'Belotero Intense', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,0 ml', '', 0, 'Belotero Intense', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(94, 'FVU-400-551', '2016-02-11', 'Belotero Soft', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,0 ml', '', 0, 'Belotero Soft', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(95, 'QMC-046-899', '2016-02-11', 'Radiesse ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 0,8 ml', '', 0, 'Radiesse ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(96, 'LTI-947-920', '2016-02-11', 'Radiesse ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,5 ml', '', 0, 'Radiesse ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(97, 'OYQ-245-219', '2016-02-11', 'Redexis', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '1 x 1,0 ml', '', 0, 'Redexis', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(98, 'PMN-679-191', '2016-02-11', 'Redexis Ultra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '1 x 1,0 ml', '', 0, 'Redexis Ultra', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(99, 'RWI-154-699', '2016-02-11', 'Revanesse', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '2 x 1,0 ml', '', 0, 'Revanesse', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(100, 'ODC-944-670', '2016-02-11', 'Revanesse Lips', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '2 x 1,0 ml', '', 0, 'Revanesse Lips', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(101, 'OAT-640-361', '2016-02-11', 'Revanesse Ultra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '2 x 1,0 ml', '', 0, 'Revanesse Ultra', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(102, 'WKL-758-054', '2016-02-11', 'REVITACARE Bio-Revitalisation', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '4 ml + 10 ml', '', 0, 'REVITACARE Bio-Revitalisation', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(103, 'MIR-970-278', '2016-02-11', 'REVITACARE B-Plus Mask', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', 'x 7', '', 0, 'REVITACARE B-Plus Mask', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(104, 'PSF-279-780', '2016-02-11', 'REVITACARE CytoCare 502', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE CytoCare 502', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(105, 'RIW-710-269', '2016-02-11', 'REVITACARE CytoCare 502', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '5 x 5 ml', '', 0, 'REVITACARE CytoCare 502', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(106, 'VIE-272-840', '2016-02-11', 'REVITACARE CytoCare 516', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE CytoCare 516', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(107, 'UKZ-405-883', '2016-02-11', 'REVITACARE CytoCare 516', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '5 x 5 ml', '', 0, 'REVITACARE CytoCare 516', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(108, 'HUM-921-169', '2016-02-11', 'REVITACARE CytoCare 532', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE CytoCare 532', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(109, 'ZXB-217-619', '2016-02-11', 'REVITACARE CytoCare 532', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '5 x 5 ml', '', 0, 'REVITACARE CytoCare 532', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(110, 'UKJ-954-469', '2016-02-11', 'REVITACARE HairCare', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE HairCare', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(111, 'BAE-058-642', '2016-02-11', 'REVITACARE Hydraplus Gel', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '', '', 0, 'REVITACARE Hydraplus Gel', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(112, 'OJJ-125-369', '2016-02-11', 'REVITACARE StretchCare', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE StretchCare', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(113, 'AZT-871-116', '2016-02-11', 'Sculptra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Sanofi-Aventis', 'Sanofi-Aventis', '', '2', '', 0, 'Sculptra', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(114, 'MHZ-334-628', '2016-02-11', 'Artzal', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Seikagaku Corporation', 'Seikagaku Corporation', '', '5 x 2,5ml', '', 0, 'Artzal', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(115, 'BFQ-264-719', '2016-02-11', 'Supartz', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Seikagaku Corporation', 'Seikagaku Corporation', '', '5 x 2,5ml', '', 0, 'Supartz', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(116, 'ERG-134-555', '2016-02-11', 'Skin Tech -  Blending - Bleeching 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech -  Blending - Bleeching 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(117, 'JHT-310-533', '2016-02-11', 'Skin Tech - ACTILIFT 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - ACTILIFT 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(118, 'QZP-861-708', '2016-02-11', 'Skin Tech - ATROFILIN 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - ATROFILIN 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(119, 'PLH-360-760', '2016-02-11', 'Skin Tech - CLEANSER', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - CLEANSER', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(120, 'LHK-872-560', '2016-02-11', 'Skin Tech - DHEA PHYTO  50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - DHEA PHYTO  50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(121, 'ZYL-555-644', '2016-02-11', 'Skin Tech - EASY PEN LIGHT', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - EASY PEN LIGHT', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(122, 'MBO-817-631', '2016-02-11', 'Skin Tech - Easy Phutic solution 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - Easy Phutic solution 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(123, 'CSS-831-168', '2016-02-11', 'Skin Tech - LIP & EYLID FORMULA', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - LIP & EYLID FORMULA', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(124, 'BYO-236-412', '2016-02-11', 'Skin Tech - MELABLOCK HSP - SPF 30 - 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - MELABLOCK HSP - SPF 30 - 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(125, 'UVZ-912-542', '2016-02-11', 'Skin Tech - MELABLOCK HSP - SPF 50 - 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - MELABLOCK HSP - SPF 50 - 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(126, 'CWF-494-848', '2016-02-11', 'Skin Tech - New Easy TCA Classic Kit ( 12 peels + 2 cremes)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '12+2', '', 0, 'Skin Tech - New Easy TCA Classic Kit ( 12 peels + 2 cremes)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(127, 'GYO-631-546', '2016-02-11', 'Skin Tech - NUTRITIVE ACE LIPOIC COMPLEX', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - NUTRITIVE ACE LIPOIC COMPLEX', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(128, 'VZK-607-040', '2016-02-11', 'Skin Tech - ONLY TOUCH', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - ONLY TOUCH', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(129, 'KQS-502-077', '2016-02-11', 'Skin Tech - OPLASE MASK', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - OPLASE MASK', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(130, 'RVN-365-113', '2016-02-11', 'Skin Tech - PURFYING 50 ML', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - PURFYING 50 ML', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(131, 'DSB-153-808', '2016-02-11', 'Skin Tech - SET SANDBRASION', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - SET SANDBRASION', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(132, 'GFE-074-027', '2016-02-11', 'Skin Tech - TCA STARTER KID  ( 4 peels + 1 crème)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '4+ 1', '', 0, 'Skin Tech - TCA STARTER KID  ( 4 peels + 1 crème)', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(133, 'MGC-810-129', '2016-02-11', 'Skin Tech - UNIDEEP', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - UNIDEEP', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(134, 'JUN-041-670', '2016-02-11', 'Skin Tech - VIT. E ANTI OXIDANT 50 ML', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - VIT. E ANTI OXIDANT 50 ML', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(135, 'HCI-915-096', '2016-02-11', 'Skin Tech -PUREGEL 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech -PUREGEL 50 ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(136, 'CNG-710-837', '2016-02-11', 'Teosyal 27G Deep Lines ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal 27G Deep Lines ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(137, 'BDI-910-318', '2016-02-11', 'Teosyal 27G Deep Lines Puresense ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal 27G Deep Lines Puresense ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(138, 'HQU-155-031', '2016-02-11', 'Teosyal 27G Kiss Puresense ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal 27G Kiss Puresense ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(139, 'EYI-361-214', '2016-02-11', 'Teosyal 30 G Touch UP ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,5 ml', '', 0, 'Teosyal 30 G Touch UP ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(140, 'YEI-249-402', '2016-02-11', 'Teosyal 30 G Touch UP 0,5ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,5 ml', '', 0, 'Teosyal 30 G Touch UP 0,5ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(141, 'BRH-571-403', '2016-02-11', 'Teosyal 30G First Lines Puresense', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,7ml', '', 0, 'Teosyal 30G First Lines Puresense', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(142, 'CKC-196-378', '2016-02-11', 'Teosyal 30G Global Action ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal 30G Global Action ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(143, 'VJE-935-897', '2016-02-11', 'Teosyal 30G Global Action Puresense ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal 30G Global Action Puresense ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(144, 'XQH-238-426', '2016-02-11', 'Teosyal First Lines ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,7 ml', '', 0, 'Teosyal First Lines ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(145, 'MAT-064-445', '2016-02-11', 'Teosyal Kiss ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Kiss ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(146, 'NJY-822-145', '2016-02-11', 'Teosyal Meso ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Meso ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(147, 'BAM-860-917', '2016-02-11', 'Teosyal Redensity I Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Redensity I Puresense 1,0ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(148, 'GYG-318-283', '2016-02-11', 'Teosyal Redensity I Puresense 3,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '1 x 3,0ml', '', 0, 'Teosyal Redensity I Puresense 3,0ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(149, 'UMC-035-221', '2016-02-11', 'Teosyal Redensity II Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Redensity II Puresense 1,0ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(150, 'KLN-772-265', '2016-02-11', 'Teosyal Ultimate', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Ultimate', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(151, 'SIK-322-464', '2016-02-11', 'Teosyal Ultimate ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '1 x 3,0ml', '', 0, 'Teosyal Ultimate ', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(152, 'KEK-089-324', '2016-02-11', 'Teosyal Ultimate Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Ultimate Puresense 1,0ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(153, 'UCF-311-675', '2016-02-11', 'Teosyal Ultimate Puresense 3,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '1 x 3,0ml', '', 0, 'Teosyal Ultimate Puresense 3,0ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(154, 'ZXF-888-379', '2016-02-11', 'Teosyal Ultra Deep', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,2ml', '', 0, 'Teosyal Ultra Deep', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(155, 'ABW-335-287', '2016-02-11', 'Teosyal Ultra Deep', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Ultra Deep', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(156, 'WYN-106-166', '2016-02-11', 'Teosyal Ultra Deep Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Ultra Deep Puresense 1,0ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(157, 'MQC-516-661', '2016-02-11', 'Teosyal Ultra Deep Puresense 1,2ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,2ml', '', 0, 'Teosyal Ultra Deep Puresense 1,2ml', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(158, 'AKF-794-426', '2016-02-11', 'Stylage Hydro', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '1 x 1,0ml', '', 0, 'Stylage Hydro', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(159, 'UEU-920-103', '2016-02-11', 'Stylage Hydro Max', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '1 x 1,0ml', '', 0, 'Stylage Hydro Max', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(160, 'LUD-544-854', '2016-02-11', 'Stylage L', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage L', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED');
INSERT INTO `productimport` (`productid`, `productno`, `date`, `name`, `industry`, `category`, `subcat`, `brand`, `manufacturer`, `caseweight`, `packaging`, `shippingcond`, `qtypercase`, `des`, `mpm`, `prefixone`, `codeone`, `prefixtwo`, `codetwo`, `prefixthree`, `codethree`, `img1`, `img2`, `img3`, `img4`, `img5`, `img6`, `img7`, `img8`, `addedby`, `roletype`, `pstatus`) VALUES
(161, 'MHM-970-248', '2016-02-11', 'Stylage Lips', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '1 x 0,8ml', '', 0, 'Stylage Lips', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(162, 'XMN-350-077', '2016-02-11', 'Stylage M', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage M', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(163, 'YDS-045-083', '2016-02-11', 'Stylage S', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 0,8ml', '', 0, 'Stylage S', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(164, 'FIW-858-425', '2016-02-11', 'Stylage XL', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage XL', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(165, 'VUG-166-373', '2016-02-11', 'Stylage XXL', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage XXL', 0, '', 'Product Code', b'1', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(166, 'PEE-707-991', '2016-02-11', 'Juvederm Hydrate', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '1 x 1,0 ml', '', 0, 'Juvederm Hydrate', 0, 'Product Code', 'Juvederm Hydrate', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(167, 'GQM-989-883', '2016-02-11', 'Juvederm Ultra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0.8ml', '', 0, 'Juvederm Ultra', 0, 'Product Code', 'Juvederm Ultra', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(168, 'PWJ-661-557', '2016-02-11', 'Juvederm Ultra 2', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,55 ml', '', 0, 'Juvederm Ultra 2', 0, 'Product Code', 'Juvederm Ultra 2', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(169, 'ETZ-437-989', '2016-02-11', 'Juvederm Ultra 3', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Ultra 3', 0, 'Product Code', 'Juvederm Ultra 3', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(170, 'MYO-974-905', '2016-02-11', 'Juvederm Ultra 4', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Ultra 4', 0, 'Product Code', 'Juvederm Ultra 4', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(171, 'TEO-092-728', '2016-02-11', 'Juvederm Ultra Smile', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,55 ml', '', 0, 'Juvederm Ultra Smile', 0, 'Product Code', 'Juvederm Ultra Smile', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(172, 'VKZ-724-361', '2016-02-11', 'Juvederm Ultra XC', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1 ml', '', 0, 'Juvederm Ultra XC', 0, 'Product Code', 'Juvederm Ultra XC', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(173, 'GMA-991-183', '2016-02-11', 'Juvederm Ultra XC Plus', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1 ml', '', 0, 'Juvederm Ultra XC Plus', 0, 'Product Code', 'Juvederm Ultra XC Plus', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(174, 'ITZ-280-171', '2016-02-11', 'Juvederm Ultra+', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0.8ml', '', 0, 'Juvederm Ultra+', 0, 'Product Code', 'Juvederm Ultra+', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(175, 'HDJ-114-031', '2016-02-11', 'Juvederm Volbella w. Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '1 x 1,0ml', '', 0, 'Juvederm Volbella w. Lidocaine', 0, 'Product Code', 'Juvederm Volbella w. Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(176, 'XCL-808-029', '2016-02-11', 'Juvederm Volift w. lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Volift w. lidocaine', 0, 'Product Code', 'Juvederm Volift w. lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(177, 'WGS-195-347', '2016-02-11', 'Juvederm Voluma w. Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 1,0 ml', '', 0, 'Juvederm Voluma w. Lidocaine', 0, 'Product Code', 'Juvederm Voluma w. Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(178, 'EHZ-636-424', '2016-02-11', 'Surgiderm 18', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 18', 0, 'Product Code', 'Surgiderm 18', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(179, 'CQJ-647-000', '2016-02-11', 'Surgiderm 24 XP', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 24 XP', 0, 'Product Code', 'Surgiderm 24 XP', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(180, 'VPL-488-790', '2016-02-11', 'Surgiderm 30', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 30', 0, 'Product Code', 'Surgiderm 30', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(181, 'ADW-443-243', '2016-02-11', 'Surgiderm 30 XP', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Allergan', 'Allergan', '', '2 x 0,8 ml', '', 0, 'Surgiderm 30 XP', 0, 'Product Code', 'Surgiderm 30 XP', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(182, 'PLJ-350-064', '2016-02-11', 'Orthovisc', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Anika Therapeutics', 'Anika Therapeutics', '', '1 x 2,0ml', '', 0, 'Orthovisc', 0, 'Product Code', 'Orthovisc', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(183, 'JGI-892-181', '2016-02-11', 'Ellanse-E', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-E', 0, 'Product Code', 'Ellanse-E', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(184, 'LKE-311-204', '2016-02-11', 'Ellanse-L', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-L', 0, 'Product Code', 'Ellanse-L', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(185, 'ABI-130-712', '2016-02-11', 'Ellanse-M', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-M', 0, 'Product Code', 'Ellanse-M', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(186, 'WES-974-922', '2016-02-11', 'Ellanse-M', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 0,5 ml', '', 0, 'Ellanse-M', 0, 'Product Code', 'Ellanse-M', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(187, 'CJA-931-838', '2016-02-11', 'Ellanse-M Hands', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-M Hands', 0, 'Product Code', 'Ellanse-M Hands', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(188, 'XKA-317-016', '2016-02-11', 'Ellanse-S', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-S', 0, 'Product Code', 'Ellanse-S', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(189, 'ELY-678-272', '2016-02-11', 'Ellanse-S', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 0,5 ml', '', 0, 'Ellanse-S', 0, 'Product Code', 'Ellanse-S', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(190, 'XNW-341-886', '2016-02-11', 'Ellanse-S Hands', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'AQTIS Medical BV', 'AQTIS Medical BV', '', '2 x 1,0 ml', '', 0, 'Ellanse-S Hands', 0, 'Product Code', 'Ellanse-S Hands', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(191, 'UMO-711-562', '2016-02-11', 'Biodermis  PRO SIL 17 GR', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '17 gr', '', 0, 'Biodermis  PRO SIL 17 GR', 0, 'Product Code', 'Biodermis  PRO SIL 17 GR', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(192, 'BOT-487-608', '2016-02-11', 'Biodermis  PRO SIL 4.25 GR', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '4.25 gr', '', 0, 'Biodermis  PRO SIL 4.25 GR', 0, 'Product Code', 'Biodermis  PRO SIL 4.25 GR', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(193, 'XZK-732-274', '2016-02-11', 'Biodermis 1.25 x 1.25 '''' ( 3.17 x 3.17 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis 1.25 x 1.25 '''' ( 3.17 x 3.17 cm)', 0, 'Product Code', 'Biodermis 1.25 x 1.25 '''' ( 3.17 x 3.17 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(194, 'FSM-491-680', '2016-02-11', 'Biodermis 12.6'''' x 3.8'''' (32 x 9.63 cm) Mastopexy', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis 12.6'''' x 3.8'''' (32 x 9.63 cm) Mastopexy', 0, 'Product Code', 'Biodermis 12.6'''' x 3.8'''' (32 x 9.63 cm) Mastopexy', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(195, 'KWJ-664-455', '2016-02-11', 'Biodermis 2.75'''' round  (6.98 x 1.90 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis 2.75'''' round  (6.98 x 1.90 cm)', 0, 'Product Code', 'Biodermis 2.75'''' round  (6.98 x 1.90 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(196, 'BGU-978-923', '2016-02-11', 'Biodermis 3'''' round  (7.62 x 1.90 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis 3'''' round  (7.62 x 1.90 cm)', 0, 'Product Code', 'Biodermis 3'''' round  (7.62 x 1.90 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(197, 'QQI-352-407', '2016-02-11', 'Biodermis EPI NET 2 X 60''''', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis EPI NET 2 X 60''''', 0, 'Product Code', 'Biodermis EPI NET 2 X 60''''', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(198, 'NTH-365-657', '2016-02-11', 'Biodermis EPI TAPE 2 X 10', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis EPI TAPE 2 X 10', 0, 'Product Code', 'Biodermis EPI TAPE 2 X 10', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(199, 'ZUO-840-481', '2016-02-11', 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14,5 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 5', '', 0, 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14,5 cm)', 0, 'Product Code', 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14,5 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(200, 'AJB-594-700', '2016-02-11', 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14.5 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14.5 cm)', 0, 'Product Code', 'Biodermis silicone  4.7 x 5.7 '''' ( 12 x 14.5 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(201, 'OMO-319-587', '2016-02-11', 'Biodermis silicone 1.4 x 12.0'''' (3.55 x 30.48 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'box of 2', '', 0, 'Biodermis silicone 1.4 x 12.0'''' (3.55 x 30.48 cm)', 0, 'Product Code', 'Biodermis silicone 1.4 x 12.0'''' (3.55 x 30.48 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(202, 'TMT-590-033', '2016-02-11', 'Biodermis silicone 1.4 x 6.0'''' ( 3.6 x 15 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis silicone 1.4 x 6.0'''' ( 3.6 x 15 cm)', 0, 'Product Code', 'Biodermis silicone 1.4 x 6.0'''' ( 3.6 x 15 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(203, 'MJD-840-800', '2016-02-11', 'Biodermis silicone 11 x 16'''' (27.94 x 40.64 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 1', '', 0, 'Biodermis silicone 11 x 16'''' (27.94 x 40.64 cm)', 0, 'Product Code', 'Biodermis silicone 11 x 16'''' (27.94 x 40.64 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(204, 'TMB-650-781', '2016-02-11', 'Biodermis silicone 2 x 2.5 '''' (5 x 6 cm)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', 'Box of 2', '', 0, 'Biodermis silicone 2 x 2.5 '''' (5 x 6 cm)', 0, 'Product Code', 'Biodermis silicone 2 x 2.5 '''' (5 x 6 cm)', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(205, 'ZUP-709-137', '2016-02-11', 'Biodermis SILQUE CLENZ', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '28.4 ml', '', 0, 'Biodermis SILQUE CLENZ', 0, 'Product Code', 'Biodermis SILQUE CLENZ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(206, 'MGP-878-119', '2016-02-11', 'Biodermis XERAGEL', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Biodermis', 'Biodermis', '', '10 ML', '', 0, 'Biodermis XERAGEL', 0, 'Product Code', 'Biodermis XERAGEL', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(207, 'QDM-190-245', '2016-02-11', 'Princess Filler', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Croma-Pharma GmbH', 'Croma-Pharma GmbH', '', '1 x 1,0 ml', '', 0, 'Princess Filler', 0, 'Product Code', 'Princess Filler', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(208, 'RFL-649-979', '2016-02-11', 'Princess Rich', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Croma-Pharma GmbH', 'Croma-Pharma GmbH', '', '1 x 1,0 ml', '', 0, 'Princess Rich', 0, 'Product Code', 'Princess Rich', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(209, 'WDM-515-961', '2016-02-11', 'Princess Volume', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Croma-Pharma GmbH', 'Croma-Pharma GmbH', '', '1 x 1,0 ml', '', 0, 'Princess Volume', 0, 'Product Code', 'Princess Volume', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(210, 'VSE-103-420', '2016-02-11', 'Euflexxa', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Ferring', 'Ferring', '', '3 x 2,0ml', '', 0, 'Euflexxa', 0, 'Product Code', 'Euflexxa', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(211, 'SGE-477-617', '2016-02-11', 'Hyalgan', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Fidia', 'Fidia', '', '1 x 2,0ml', '', 0, 'Hyalgan', 0, 'Product Code', 'Hyalgan', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(212, 'SAV-577-997', '2016-02-11', 'Filorga M-HA 10', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '3', '', 0, 'Filorga M-HA 10', 0, 'Product Code', 'Filorga M-HA 10', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(213, 'AHA-524-245', '2016-02-11', 'Filorga M-HA 18', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '2', '', 0, 'Filorga M-HA 18', 0, 'Product Code', 'Filorga M-HA 18', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(214, 'KCO-261-768', '2016-02-11', 'Filorga NCTF 135', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '5', '', 0, 'Filorga NCTF 135', 0, 'Product Code', 'Filorga NCTF 135', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(215, 'HUJ-244-302', '2016-02-11', 'Filorga NCTF 135HA ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '5', '', 0, 'Filorga NCTF 135HA ', 0, 'Product Code', 'Filorga NCTF 135HA ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(216, 'CCD-265-460', '2016-02-11', 'Filorga X-HA 3', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '2', '', 0, 'Filorga X-HA 3', 0, 'Product Code', 'Filorga X-HA 3', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(217, 'BXY-889-148', '2016-02-11', 'Filorga X-HA Volume, 1ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Filorga', 'Filorga', '', '2', '', 0, 'Filorga X-HA Volume, 1ml', 0, 'Product Code', 'Filorga X-HA Volume, 1ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(218, 'LBT-482-759', '2016-02-11', 'Durolane', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Galderma', 'Galderma', '', '1 x 3,0ml', '', 0, 'Durolane', 0, 'Product Code', 'Durolane', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(219, 'KNV-408-772', '2016-02-11', 'Macrolane VRF20', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 10ml', '', 0, 'Macrolane VRF20', 0, 'Product Code', 'Macrolane VRF20', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(220, 'JZV-993-734', '2016-02-11', 'Macrolane VRF20 ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 20ml', '', 0, 'Macrolane VRF20 ', 0, 'Product Code', 'Macrolane VRF20 ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(221, 'OKT-835-606', '2016-02-11', 'Macrolane VRF30', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 10ml', '', 0, 'Macrolane VRF30', 0, 'Product Code', 'Macrolane VRF30', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(222, 'UVF-108-233', '2016-02-11', 'Macrolane VRF30 ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 20ml', '', 0, 'Macrolane VRF30 ', 0, 'Product Code', 'Macrolane VRF30 ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(223, 'UXM-869-601', '2016-02-11', 'Restylane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane', 0, 'Product Code', 'Restylane', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(224, 'YRY-568-510', '2016-02-11', 'Restylane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane', 0, 'Product Code', 'Restylane', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(225, 'ODG-776-656', '2016-02-11', 'Restylane Day Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Day Cream', 0, 'Product Code', 'Restylane Day Cream', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(226, 'JKO-918-949', '2016-02-11', 'Restylane Day Creme SPF 15', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Day Creme SPF 15', 0, 'Product Code', 'Restylane Day Creme SPF 15', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(227, 'KQN-754-450', '2016-02-11', 'Restylane Eye Serum', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 15 ml', '', 0, 'Restylane Eye Serum', 0, 'Product Code', 'Restylane Eye Serum', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(228, 'ZEW-017-664', '2016-02-11', 'Restylane Facial Cleanser', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 100 ml', '', 0, 'Restylane Facial Cleanser', 0, 'Product Code', 'Restylane Facial Cleanser', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(229, 'VVU-765-827', '2016-02-11', 'Restylane Hand Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Hand Cream', 0, 'Product Code', 'Restylane Hand Cream', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(230, 'ECS-709-047', '2016-02-11', 'Restylane Lipp Refresh with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Lipp Refresh with Lidocaine', 0, 'Product Code', 'Restylane Lipp Refresh with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(231, 'IZN-130-732', '2016-02-11', 'Restylane Lipp Volume with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Lipp Volume with Lidocaine', 0, 'Product Code', 'Restylane Lipp Volume with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(232, 'RKL-515-799', '2016-02-11', 'Restylane Night Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Night Cream', 0, 'Product Code', 'Restylane Night Cream', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(233, 'DEX-764-405', '2016-02-11', 'Restylane Night Serum', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 15 ml', '', 0, 'Restylane Night Serum', 0, 'Product Code', 'Restylane Night Serum', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(234, 'AEI-838-297', '2016-02-11', 'Restylane Perlane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Perlane', 0, 'Product Code', 'Restylane Perlane', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(235, 'ZSX-935-514', '2016-02-11', 'Restylane Perlane', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane Perlane', 0, 'Product Code', 'Restylane Perlane', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(236, 'ZTD-773-598', '2016-02-11', 'Restylane Perlane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Perlane with Lidocaine', 0, 'Product Code', 'Restylane Perlane with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(237, 'KJJ-582-257', '2016-02-11', 'Restylane Perlane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane Perlane with Lidocaine', 0, 'Product Code', 'Restylane Perlane with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(238, 'ABM-711-863', '2016-02-11', 'Restylane Recover Cream', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 25 ml', '', 0, 'Restylane Recover Cream', 0, 'Product Code', 'Restylane Recover Cream', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(239, 'IVD-793-032', '2016-02-11', 'Restylane Sub-Q with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Sub-Q with Lidocaine', 0, 'Product Code', 'Restylane Sub-Q with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(240, 'DMI-986-141', '2016-02-11', 'Restylane Vital', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital', 0, 'Product Code', 'Restylane Vital', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(241, 'YRP-730-592', '2016-02-11', 'Restylane Vital injector', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital injector', 0, 'Product Code', 'Restylane Vital injector', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(242, 'EBJ-635-183', '2016-02-11', 'Restylane Vital injector with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital injector with Lidocaine', 0, 'Product Code', 'Restylane Vital injector with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(243, 'YEQ-973-775', '2016-02-11', 'Restylane Vital Light', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital Light', 0, 'Product Code', 'Restylane Vital Light', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(244, 'RHV-497-475', '2016-02-11', 'Restylane Vital light injector', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital light injector', 0, 'Product Code', 'Restylane Vital light injector', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(245, 'HFL-242-696', '2016-02-11', 'Restylane Vital Light injector with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 2,0 ml', '', 0, 'Restylane Vital Light injector with Lidocaine', 0, 'Product Code', 'Restylane Vital Light injector with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(246, 'WQG-250-557', '2016-02-11', 'Restylane Vital Light with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital Light with Lidocaine', 0, 'Product Code', 'Restylane Vital Light with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(247, 'PYI-660-441', '2016-02-11', 'Restylane Vital w. Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane Vital w. Lidocaine', 0, 'Product Code', 'Restylane Vital w. Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(248, 'CUQ-039-083', '2016-02-11', 'Restylane Whitening Day Creme', 'Medical', 'Cosmetic & Plastic Surgery', 'Facial Cleanser & Creams', 'Galderma', 'Galderma', '', '1 x 50 ml', '', 0, 'Restylane Whitening Day Creme', 0, 'Product Code', 'Restylane Whitening Day Creme', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(249, 'AIM-948-995', '2016-02-11', 'Restylane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 1,0 ml', '', 0, 'Restylane with Lidocaine', 0, 'Product Code', 'Restylane with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(250, 'RUY-338-172', '2016-02-11', 'Restylane with Lidocaine', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Galderma', 'Galderma', '', '1 x 0,5 ml', '', 0, 'Restylane with Lidocaine', 0, 'Product Code', 'Restylane with Lidocaine', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(251, 'RTE-576-781', '2016-02-11', 'Synvisc Classic', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Genzyme', 'Genzyme', '', '3 x 2,0ml', '', 0, 'Synvisc Classic', 0, 'Product Code', 'Synvisc Classic', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(252, 'ODD-225-150', '2016-02-11', 'Synvisc One', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Genzyme', 'Genzyme', '', '1 x 6,0ml', '', 0, 'Synvisc One', 0, 'Product Code', 'Synvisc One', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(253, 'EZV-662-400', '2016-02-11', 'Perfectha Derm', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 1,0ml', '', 0, 'Perfectha Derm', 0, 'Product Code', 'Perfectha Derm', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(254, 'WEW-040-160', '2016-02-11', 'Perfectha Derm Deep ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 1,0ml', '', 0, 'Perfectha Derm Deep ', 0, 'Product Code', 'Perfectha Derm Deep ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(255, 'TMH-408-849', '2016-02-11', 'Perfectha Derm FineLines', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 0,5ml', '', 0, 'Perfectha Derm FineLines', 0, 'Product Code', 'Perfectha Derm FineLines', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(256, 'ZCD-870-686', '2016-02-11', 'Perfectha Derm Subskin', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Laboratoire ObvieLine', 'Laboratoire ObvieLine', '', '1 x 3,0ml', '', 0, 'Perfectha Derm Subskin', 0, 'Product Code', 'Perfectha Derm Subskin', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(257, 'XWG-000-055', '2016-02-11', 'Belotero Basic', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,0 ml', '', 0, 'Belotero Basic', 0, 'Product Code', 'Belotero Basic', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(258, 'YTU-391-315', '2016-02-11', 'Belotero Intense', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,0 ml', '', 0, 'Belotero Intense', 0, 'Product Code', 'Belotero Intense', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(259, 'SVO-260-901', '2016-02-11', 'Belotero Soft', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,0 ml', '', 0, 'Belotero Soft', 0, 'Product Code', 'Belotero Soft', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(260, 'AVM-779-804', '2016-02-11', 'Radiesse ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 0,8 ml', '', 0, 'Radiesse ', 0, 'Product Code', 'Radiesse ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(261, 'ADR-887-639', '2016-02-11', 'Radiesse ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Merz', 'Merz', '', '1 x 1,5 ml', '', 0, 'Radiesse ', 0, 'Product Code', 'Radiesse ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(262, 'OBK-620-691', '2016-02-11', 'Redexis', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '1 x 1,0 ml', '', 0, 'Redexis', 0, 'Product Code', 'Redexis', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(263, 'WCA-896-219', '2016-02-11', 'Redexis Ultra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '1 x 1,0 ml', '', 0, 'Redexis Ultra', 0, 'Product Code', 'Redexis Ultra', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(264, 'HYK-979-680', '2016-02-11', 'Revanesse', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '2 x 1,0 ml', '', 0, 'Revanesse', 0, 'Product Code', 'Revanesse', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(265, 'UXI-437-516', '2016-02-11', 'Revanesse Lips', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '2 x 1,0 ml', '', 0, 'Revanesse Lips', 0, 'Product Code', 'Revanesse Lips', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(266, 'ZVH-978-008', '2016-02-11', 'Revanesse Ultra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Prollenium Medical Technologies Inc', 'Prollenium Medical Technologies Inc', '', '2 x 1,0 ml', '', 0, 'Revanesse Ultra', 0, 'Product Code', 'Revanesse Ultra', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(267, 'FQP-864-927', '2016-02-11', 'REVITACARE Bio-Revitalisation', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '4 ml + 10 ml', '', 0, 'REVITACARE Bio-Revitalisation', 0, 'Product Code', 'REVITACARE Bio-Revitalisation', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(268, 'OQT-113-842', '2016-02-11', 'REVITACARE B-Plus Mask', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', 'x 7', '', 0, 'REVITACARE B-Plus Mask', 0, 'Product Code', 'REVITACARE B-Plus Mask', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(269, 'DXC-357-528', '2016-02-11', 'REVITACARE CytoCare 502', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE CytoCare 502', 0, 'Product Code', 'REVITACARE CytoCare 502', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(270, 'QRN-515-837', '2016-02-11', 'REVITACARE CytoCare 502', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '5 x 5 ml', '', 0, 'REVITACARE CytoCare 502', 0, 'Product Code', 'REVITACARE CytoCare 502', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(271, 'IIG-228-654', '2016-02-11', 'REVITACARE CytoCare 516', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE CytoCare 516', 0, 'Product Code', 'REVITACARE CytoCare 516', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(272, 'OWD-526-317', '2016-02-11', 'REVITACARE CytoCare 516', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '5 x 5 ml', '', 0, 'REVITACARE CytoCare 516', 0, 'Product Code', 'REVITACARE CytoCare 516', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(273, 'RCV-827-354', '2016-02-11', 'REVITACARE CytoCare 532', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE CytoCare 532', 0, 'Product Code', 'REVITACARE CytoCare 532', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(274, 'KOZ-530-630', '2016-02-11', 'REVITACARE CytoCare 532', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '5 x 5 ml', '', 0, 'REVITACARE CytoCare 532', 0, 'Product Code', 'REVITACARE CytoCare 532', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(275, 'VIH-365-208', '2016-02-11', 'REVITACARE HairCare', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE HairCare', 0, 'Product Code', 'REVITACARE HairCare', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(276, 'CBL-639-165', '2016-02-11', 'REVITACARE Hydraplus Gel', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '', '', 0, 'REVITACARE Hydraplus Gel', 0, 'Product Code', 'REVITACARE Hydraplus Gel', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(277, 'WOW-524-953', '2016-02-11', 'REVITACARE StretchCare', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Revitacare', 'Revitacare', '', '10 x 5 ml', '', 0, 'REVITACARE StretchCare', 0, 'Product Code', 'REVITACARE StretchCare', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(278, 'MXZ-603-430', '2016-02-11', 'Sculptra', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Sanofi-Aventis', 'Sanofi-Aventis', '', '2', '', 0, 'Sculptra', 0, 'Product Code', 'Sculptra', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(279, 'DKR-551-171', '2016-02-11', 'Artzal', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Seikagaku Corporation', 'Seikagaku Corporation', '', '5 x 2,5ml', '', 0, 'Artzal', 0, 'Product Code', 'Artzal', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(280, 'AYS-714-347', '2016-02-11', 'Supartz', 'Medical', 'Orthopaedic Surgery', 'Knee Injections', 'Seikagaku Corporation', 'Seikagaku Corporation', '', '5 x 2,5ml', '', 0, 'Supartz', 0, 'Product Code', 'Supartz', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(281, 'EMH-621-150', '2016-02-11', 'Skin Tech -  Blending - Bleeching 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech -  Blending - Bleeching 50 ml', 0, 'Product Code', 'Skin Tech  Blending Bleeching 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(282, 'SSG-564-951', '2016-02-11', 'Skin Tech - ACTILIFT 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - ACTILIFT 50 ml', 0, 'Product Code', 'Skin Tech ACTILIFT 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(283, 'QXU-297-381', '2016-02-11', 'Skin Tech - ATROFILIN 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - ATROFILIN 50 ml', 0, 'Product Code', 'Skin Tech ATROFILIN 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(284, 'GYM-289-705', '2016-02-11', 'Skin Tech - CLEANSER', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - CLEANSER', 0, 'Product Code', 'Skin Tech CLEANSER', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(285, 'DRU-590-138', '2016-02-11', 'Skin Tech - DHEA PHYTO  50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - DHEA PHYTO  50 ml', 0, 'Product Code', 'Skin Tech DHEA PHYTO  50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(286, 'MES-008-130', '2016-02-11', 'Skin Tech - EASY PEN LIGHT', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - EASY PEN LIGHT', 0, 'Product Code', 'Skin Tech EASY PEN LIGHT', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(287, 'DAI-114-223', '2016-02-11', 'Skin Tech - Easy Phutic solution 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - Easy Phutic solution 50 ml', 0, 'Product Code', 'Skin Tech Easy Phutic solution 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(288, 'SSS-748-493', '2016-02-11', 'Skin Tech - LIP & EYLID FORMULA', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - LIP & EYLID FORMULA', 0, 'Product Code', 'Skin Tech LIP & EYLID FORMULA', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(289, 'IZU-943-045', '2016-02-11', 'Skin Tech - MELABLOCK HSP - SPF 30 - 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - MELABLOCK HSP - SPF 30 - 50 ml', 0, 'Product Code', 'Skin Tech MELABLOCK HSP SPF 30 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(290, 'PQZ-921-621', '2016-02-11', 'Skin Tech - MELABLOCK HSP - SPF 50 - 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - MELABLOCK HSP - SPF 50 - 50 ml', 0, 'Product Code', 'Skin Tech MELABLOCK HSP SPF 50 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(291, 'QFL-495-572', '2016-02-11', 'Skin Tech - New Easy TCA Classic Kit ( 12 peels + 2 cremes)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '12+2', '', 0, 'Skin Tech - New Easy TCA Classic Kit ( 12 peels + 2 cremes)', 0, 'Product Code', 'Skin Tech New Easy TCA Classic Kit', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(292, 'LMA-764-401', '2016-02-11', 'Skin Tech - NUTRITIVE ACE LIPOIC COMPLEX', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - NUTRITIVE ACE LIPOIC COMPLEX', 0, 'Product Code', 'Skin Tech NUTRITIVE ACE LIPOIC COMPLEX', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(293, 'ISO-932-905', '2016-02-11', 'Skin Tech - ONLY TOUCH', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - ONLY TOUCH', 0, 'Product Code', 'Skin Tech ONLY TOUCH', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(294, 'KFU-013-294', '2016-02-11', 'Skin Tech - OPLASE MASK', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - OPLASE MASK', 0, 'Product Code', 'Skin Tech OPLASE MASK', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(295, 'GAA-227-509', '2016-02-11', 'Skin Tech - PURFYING 50 ML', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - PURFYING 50 ML', 0, 'Product Code', 'Skin Tech PURFYING 50 ML', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(296, 'JKI-555-599', '2016-02-11', 'Skin Tech - SET SANDBRASION', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - SET SANDBRASION', 0, 'Product Code', 'Skin Tech SET SANDBRASION', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(297, 'MBK-366-755', '2016-02-11', 'Skin Tech - TCA STARTER KID  ( 4 peels + 1 crème)', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '4+ 1', '', 0, 'Skin Tech - TCA STARTER KID  ( 4 peels + 1 crème)', 0, 'Product Code', 'Skin Tech TCA STARTER KID ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(298, 'CJW-369-464', '2016-02-11', 'Skin Tech - UNIDEEP', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '1 Box', '', 0, 'Skin Tech - UNIDEEP', 0, 'Product Code', 'Skin Tech UNIDEEP', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(299, 'DSZ-214-527', '2016-02-11', 'Skin Tech - VIT. E ANTI OXIDANT 50 ML', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech - VIT. E ANTI OXIDANT 50 ML', 0, 'Product Code', 'Skin Tech VIT. E ANTI OXIDANT 50 ML', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(300, 'GIS-613-999', '2016-02-11', 'Skin Tech -PUREGEL 50 ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Skin Care', 'Skin Tech', 'Skin Tech', '', '50 ml', '', 0, 'Skin Tech -PUREGEL 50 ml', 0, 'Product Code', 'Skin Tech -PUREGEL 50 ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(301, 'BUU-511-662', '2016-02-11', 'Teosyal 27G Deep Lines ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal 27G Deep Lines ', 0, 'Product Code', 'Teosyal 27G Deep Lines ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(302, 'BBC-000-674', '2016-02-11', 'Teosyal 27G Deep Lines Puresense ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal 27G Deep Lines Puresense ', 0, 'Product Code', 'Teosyal 27G Deep Lines Puresense ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(303, 'RMH-889-336', '2016-02-11', 'Teosyal 27G Kiss Puresense ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal 27G Kiss Puresense ', 0, 'Product Code', 'Teosyal 27G Kiss Puresense ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(304, 'ZYY-699-445', '2016-02-11', 'Teosyal 30 G Touch UP ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,5 ml', '', 0, 'Teosyal 30 G Touch UP ', 0, 'Product Code', 'Teosyal 30 G Touch UP ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(305, 'KDW-853-377', '2016-02-11', 'Teosyal 30 G Touch UP 0,5ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,5 ml', '', 0, 'Teosyal 30 G Touch UP 0,5ml', 0, 'Product Code', 'Teosyal 30 G Touch UP 0,5ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(306, 'RWM-345-828', '2016-02-11', 'Teosyal 30G First Lines Puresense', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,7ml', '', 0, 'Teosyal 30G First Lines Puresense', 0, 'Product Code', 'Teosyal 30G First Lines Puresense', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(307, 'WMV-223-959', '2016-02-11', 'Teosyal 30G Global Action ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal 30G Global Action ', 0, 'Product Code', 'Teosyal 30G Global Action ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(308, 'EAV-584-761', '2016-02-11', 'Teosyal 30G Global Action Puresense ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal 30G Global Action Puresense ', 0, 'Product Code', 'Teosyal 30G Global Action Puresense ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(309, 'XZW-073-373', '2016-02-11', 'Teosyal First Lines ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 0,7 ml', '', 0, 'Teosyal First Lines ', 0, 'Product Code', 'Teosyal First Lines ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED');
INSERT INTO `productimport` (`productid`, `productno`, `date`, `name`, `industry`, `category`, `subcat`, `brand`, `manufacturer`, `caseweight`, `packaging`, `shippingcond`, `qtypercase`, `des`, `mpm`, `prefixone`, `codeone`, `prefixtwo`, `codetwo`, `prefixthree`, `codethree`, `img1`, `img2`, `img3`, `img4`, `img5`, `img6`, `img7`, `img8`, `addedby`, `roletype`, `pstatus`) VALUES
(310, 'ENK-041-580', '2016-02-11', 'Teosyal Kiss ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Kiss ', 0, 'Product Code', 'Teosyal Kiss ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(311, 'ZTN-482-406', '2016-02-11', 'Teosyal Meso ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Meso ', 0, 'Product Code', 'Teosyal Meso ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(312, 'LUE-549-841', '2016-02-11', 'Teosyal Redensity I Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Redensity I Puresense 1,0ml', 0, 'Product Code', 'Teosyal Redensity I Puresense 1,0ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(313, 'OMW-058-298', '2016-02-11', 'Teosyal Redensity I Puresense 3,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '1 x 3,0ml', '', 0, 'Teosyal Redensity I Puresense 3,0ml', 0, 'Product Code', 'Teosyal Redensity I Puresense 3,0ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(314, 'ISE-752-540', '2016-02-11', 'Teosyal Redensity II Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Redensity II Puresense 1,0ml', 0, 'Product Code', 'Teosyal Redensity II Puresense 1,0ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(315, 'IGJ-137-974', '2016-02-11', 'Teosyal Ultimate', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Ultimate', 0, 'Product Code', 'Teosyal Ultimate', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(316, 'VPI-284-757', '2016-02-11', 'Teosyal Ultimate ', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '1 x 3,0ml', '', 0, 'Teosyal Ultimate ', 0, 'Product Code', 'Teosyal Ultimate ', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(317, 'EOY-391-949', '2016-02-11', 'Teosyal Ultimate Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Ultimate Puresense 1,0ml', 0, 'Product Code', 'Teosyal Ultimate Puresense 1,0ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(318, 'QSP-771-978', '2016-02-11', 'Teosyal Ultimate Puresense 3,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '1 x 3,0ml', '', 0, 'Teosyal Ultimate Puresense 3,0ml', 0, 'Product Code', 'Teosyal Ultimate Puresense 3,0ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(319, 'IXM-483-473', '2016-02-11', 'Teosyal Ultra Deep', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,2ml', '', 0, 'Teosyal Ultra Deep', 0, 'Product Code', 'Teosyal Ultra Deep', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(320, 'WQW-388-584', '2016-02-11', 'Teosyal Ultra Deep', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0 ml', '', 0, 'Teosyal Ultra Deep', 0, 'Product Code', 'Teosyal Ultra Deep', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(321, 'NAD-897-677', '2016-02-11', 'Teosyal Ultra Deep Puresense 1,0ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,0ml', '', 0, 'Teosyal Ultra Deep Puresense 1,0ml', 0, 'Product Code', 'Teosyal Ultra Deep Puresense 1,0ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(322, 'VZR-448-377', '2016-02-11', 'Teosyal Ultra Deep Puresense 1,2ml', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Teoxane', 'Teoxane', '', '2 x 1,2ml', '', 0, 'Teosyal Ultra Deep Puresense 1,2ml', 0, 'Product Code', 'Teosyal Ultra Deep Puresense 1,2ml', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(323, 'XUS-683-900', '2016-02-11', 'Stylage Hydro', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '1 x 1,0ml', '', 0, 'Stylage Hydro', 0, 'Product Code', 'Stylage Hydro', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(324, 'FHL-243-693', '2016-02-11', 'Stylage Hydro Max', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '1 x 1,0ml', '', 0, 'Stylage Hydro Max', 0, 'Product Code', 'Stylage Hydro Max', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(325, 'ZZZ-292-458', '2016-02-11', 'Stylage L', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage L', 0, 'Product Code', 'Stylage L', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(326, 'RKO-468-663', '2016-02-11', 'Stylage Lips', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '1 x 0,8ml', '', 0, 'Stylage Lips', 0, 'Product Code', 'Stylage Lips', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(327, 'JCA-898-089', '2016-02-11', 'Stylage M', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage M', 0, 'Product Code', 'Stylage M', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(328, 'OPR-565-864', '2016-02-11', 'Stylage S', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 0,8ml', '', 0, 'Stylage S', 0, 'Product Code', 'Stylage S', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(329, 'AOH-759-377', '2016-02-11', 'Stylage XL', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage XL', 0, 'Product Code', 'Stylage XL', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED'),
(330, 'FSM-393-274', '2016-02-11', 'Stylage XXL', 'Medical', 'Cosmetic & Plastic Surgery', 'Dermal Fillers', 'Vivacy Laboratories', 'Vivacy Laboratories', '', '2 x 1,0ml', '', 0, 'Stylage XXL', 0, 'Product Code', 'Stylage XXL', b'0', '', '', '', 'no-image.jpg', '', '', '', '', '', '', '', 'admin', 'AD', 'APPROVED');

-- --------------------------------------------------------

--
-- Stand-in structure for view `pro_products`
--
CREATE TABLE IF NOT EXISTS `pro_products` (
`productid` int(11)
,`product_name` varchar(250)
,`productno` varchar(25)
,`industryid` int(11)
,`catid` int(11)
,`subcatid` int(11)
,`brandid` int(11)
,`industry_name` varchar(100)
,`category_name` varchar(100)
,`subcategory_name` varchar(100)
,`brand_name` varchar(100)
,`manufacture` varchar(100)
,`caseweight` varchar(100)
,`qtypercase` int(11)
,`pakaging` varchar(100)
,`shipingcondition` varchar(100)
,`code1` int(11)
,`codevalue1` varchar(100)
,`code2` int(11)
,`codevalue2` varchar(100)
,`code3` int(11)
,`codevalue3` varchar(100)
,`description` text
,`img0` varchar(100)
,`img1` varchar(100)
,`img2` varchar(100)
,`img3` varchar(100)
,`img4` varchar(100)
,`img5` varchar(100)
,`img6` varchar(100)
,`img7` varchar(100)
,`isactive` bit(1)
,`product_status` varchar(25)
,`mpm` int(11)
,`product_userid` varchar(25)
,`pdate` date
);
-- --------------------------------------------------------

--
-- Table structure for table `quotation_counter`
--

CREATE TABLE IF NOT EXISTS `quotation_counter` (
  `counterid` int(11) NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(50) NOT NULL,
  `quoteid` int(5) NOT NULL,
  `postno` varchar(50) NOT NULL,
  `productno` varchar(50) NOT NULL,
  `acceptedBy` varchar(50) NOT NULL,
  `counter_date` datetime NOT NULL,
  `counter_by` varchar(50) NOT NULL,
  `counter_price` varchar(200) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `counter_quantity` int(11) NOT NULL,
  `counter_timeframe` varchar(200) NOT NULL,
  `counter_expdate` varchar(200) NOT NULL,
  `counter_msg` varchar(200) NOT NULL,
  `offeracceptmsg` varchar(50) NOT NULL,
  `counetr_status` varchar(200) NOT NULL,
  `offerCounterStatus` varchar(50) NOT NULL,
  `roletype` varchar(20) NOT NULL,
  PRIMARY KEY (`counterid`),
  KEY `counterid` (`counterid`,`quotationno`,`postno`,`productno`,`acceptedBy`,`counter_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `quotation_counter`
--

INSERT INTO `quotation_counter` (`counterid`, `quotationno`, `quoteid`, `postno`, `productno`, `acceptedBy`, `counter_date`, `counter_by`, `counter_price`, `currency`, `counter_quantity`, `counter_timeframe`, `counter_expdate`, `counter_msg`, `offeracceptmsg`, `counetr_status`, `offerCounterStatus`, `roletype`) VALUES
(1, '633889-O', 1, '710288', 'ZBQ-800-883', 'IXBB-4438', '2016-01-14 01:44:09', 'IXBB-4438', '200', '', 10, '3 - 4 Weeks', '3 - 6 Months', 'Countered By User', '', 'Counter', '', 'AM'),
(2, '633889-O', 1, '710288', 'ZBQ-800-883', 'YSZJ-4486', '2016-01-14 01:47:02', 'YSZJ-4486', '300', 'GBP', 10, '3 - 4 Weeks', '3 - 6 Months', 'Offer Accepted by User', '', 'Accept', '', 'QF'),
(3, '216588-H', 5, '343983', 'VEQ-997-719', 'OORQ-0229', '2016-01-19 09:57:32', 'OORQ-0229', '750.00', '', 5000, '1 - 2 Weeks', '12 - 18 Months', 'Countered By User', '', 'Counter', '', 'AM'),
(4, '216588-H', 5, '343983', 'VEQ-997-719', 'admin', '2016-01-19 09:59:07', 'admin', '700.00', 'GBP', 5000, '1 - 2 Weeks', '12 - 18 Months', 'Offer Accepted by User', '', 'Accept', '', 'AD');

-- --------------------------------------------------------

--
-- Stand-in structure for view `qutation_products`
--
CREATE TABLE IF NOT EXISTS `qutation_products` (
`quotationno` varchar(11)
,`postno` varchar(6)
,`productid` int(11)
,`product_name` varchar(250)
,`productno` varchar(25)
,`industryid` int(11)
,`catid` int(11)
,`subcatid` int(11)
,`brandid` int(11)
,`industry` varchar(100)
,`category` varchar(100)
,`subcategory` varchar(100)
,`brand` varchar(100)
,`manufacture` varchar(100)
,`caseweight` varchar(100)
,`qtypercase` int(11)
,`pakaging` varchar(100)
,`shipingcondition` varchar(100)
,`code1` int(11)
,`codevalue1` varchar(100)
,`code2` int(11)
,`codevalue2` varchar(100)
,`code3` int(11)
,`codevalue3` varchar(100)
,`description` text
,`img0` varchar(100)
,`img1` varchar(100)
,`img2` varchar(100)
,`img3` varchar(100)
,`img4` varchar(100)
,`img5` varchar(100)
,`img6` varchar(100)
,`img7` varchar(100)
,`ptype` varchar(5)
,`targetprice` int(11)
,`currency` varchar(10)
,`uom` varchar(50)
,`quantity` int(11)
,`location` varchar(50)
,`expdate` varchar(50)
,`expirydate` varchar(50)
,`customerrefno` varchar(50)
,`post_advertisment_pakaging` text
,`post_advertisment_language` varchar(200)
,`post_advertisment_timeframe` varchar(50)
,`post_advertisment_country` varchar(200)
,`post_advertisment_userid` varchar(50)
,`post_advertisment_pdate` datetime
,`quoteid` int(11)
,`pdate` date
,`counter_date` date
,`offerstatus` varchar(20)
,`quationstatus` varchar(25)
,`quationtype` varchar(50)
,`quationuserid` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ind_type` varchar(50) NOT NULL,
  `Index_val` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test1`
--

CREATE TABLE IF NOT EXISTS `test1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `industry` varchar(800) NOT NULL,
  `category` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tm_accpted_offers`
--

CREATE TABLE IF NOT EXISTS `tm_accpted_offers` (
  `acc_offr_id` int(11) NOT NULL AUTO_INCREMENT,
  `counterid` int(11) NOT NULL,
  `quotationno` varchar(50) NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) NOT NULL,
  `productno` varchar(50) NOT NULL,
  `acceptedBy` varchar(50) NOT NULL,
  `acc_date` datetime NOT NULL,
  `acc_price` varchar(50) NOT NULL,
  `acc_quantity` int(11) NOT NULL,
  `acc_timeframe` varchar(200) NOT NULL,
  `acc_expdate` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `noti_status` varchar(20) NOT NULL DEFAULT 'Unread',
  `tm_userID` varchar(20) NOT NULL,
  `offerType` varchar(20) NOT NULL,
  PRIMARY KEY (`acc_offr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trem_condition`
--

CREATE TABLE IF NOT EXISTS `trem_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trems_condition` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trem_condition`
--

INSERT INTO `trem_condition` (`id`, `trems_condition`) VALUES
(1, '<h1><u>Terms &amp; Conditions</u></h1><br><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim pellentesque est, non feugiat lectus. Etiam suscipit purus auctor leo gravida cursus. Praesent diam erat, varius non dolor in, finibus vestibulum ex. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus accumsan sollicitudin arcu, a condimentum turpis eleifend nec. Maecenas sollicitudin justo id auctor pretium. Nunc id ante nec urna placerat hendrerit tempor sed justo. </i><br><br>Praesent purus erat, sollicitudin sed odio sed, scelerisque iaculis leo. Aliquam mauris metus, venenatis eget lacus vel, molestie lobortis leo. Quisque mollis ut lacus in pulvinar.In vehicula, ligula a ultrices aliquam, sem nibh imperdiet elit, id efficitur sem augue non leo. Ut sed massa neque. Sed sagittis, nisi id pretium suscipit, velit nulla venenatis enim, vel convallis lacus sapien at magna. Curabitur sed eros magna. Vestibulum aliquet justo enim. Suspendisse efficitur consectetur ante ut interdum. Phasellus cursus dapibus ligula.Vestibulum id luctus nibh, id rhoncus sem. Etiam eleifend volutpat sapien eu ultricies. <br><br>Nunc cursus nisl accumsan mi aliquam, quis placerat augue fringilla. Integer interdum, felis eu varius sodales, dui ligula dignissim lacus, quis tincidunt tortor orci et risus. Sed interdum in sem quis tempus. Vivamus nec lorem in augue euismod gravida. Donec eleifend nibh at dapibus ultricies. Suspendisse suscipit condimentum odio, sit amet interdum nunc condimentum id. Donec tristique est nec orci semper scelerisque. Ut neque arcu, pulvinar id tellus ut, mollis scelerisque sapien. Maecenas a auctor leo. Aliquam et risus purus. Nam et leo vitae lectus mattis imperdiet ut sed nisi.Mauris blandit leo sit amet tortor laoreet, at placerat dolor aliquam. Quisque sed aliquam elit. Pellentesque in tellus nec augue egestas bibendum at sit amet orci. Pellentesque bibendum ante ex, eu tristique augue dapibus ac. Nam non ullamcorper dui. <br><br>Aliquam fringilla ligula neque, vel molestie lectus tincidunt vitae. Maecenas egestas, dolor vel tristique aliquam, quam neque aliquet velit, ut pellentesque elit lorem at velit.Ut fringilla justo id porta elementum. Cras orci mauris, gravida eget elit sed, viverra tincidunt neque. Sed dignissim semper libero, vitae feugiat leo tempus volutpat. Nullam tempus massa eu magna finibus lobortis. Sed molestie enim a nisl vehicula suscipit. Etiam vel orci ultrices, pulvinar odio lobortis, commodo lacus. Nam viverra faucibus lacus ut malesuada. Nam sem eros, luctus ut magna at, porta pulvinar lacus.');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwcustomer1`
--
CREATE TABLE IF NOT EXISTS `vwcustomer1` (
`productno` int(1)
,`pdate` int(1)
,`name` int(1)
,`productid` int(1)
,`industryid` int(1)
,`industryname` int(1)
,`brandid` int(1)
,`brandname` int(1)
,`catid` int(1)
,`categoryname` int(1)
,`subcatid` int(1)
,`subcategoryname` int(1)
,`pakaging` int(1)
,`shipingcondition` int(1)
,`caseweight` int(1)
,`qtypercase` int(1)
,`description` int(1)
,`manufacture` int(1)
,`pstatus` int(1)
,`mpm` int(1)
,`addedby` int(1)
,`isactive` int(1)
,`roletype` int(1)
,`img0` int(1)
,`img1` int(1)
,`img2` int(1)
,`img3` int(1)
,`img4` int(1)
,`img5` int(1)
,`img6` int(1)
,`img7` int(1)
,`code1` int(1)
,`codevalue1` int(1)
,`code2` int(1)
,`codevalue2` int(1)
,`code3` int(1)
,`codevalue3` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwoffers`
--
CREATE TABLE IF NOT EXISTS `vwoffers` (
`pdate` int(1)
,`quotationno` int(1)
,`postno` int(1)
,`productno` int(1)
,`price` int(1)
,`name` int(1)
,`productid` int(1)
,`addedby` int(1)
,`targetprice` int(1)
,`currency` int(1)
,`uom` int(1)
,`quantity` int(1)
,`location` int(1)
,`expdate` int(1)
,`expirydate` int(1)
,`language` int(1)
,`country` int(1)
,`timeframe` int(1)
,`detail` int(1)
,`declinemsg` int(1)
,`offerdeclinemsg` int(1)
,`acceptmsg` int(1)
,`offeracceptmsg` int(1)
,`offercrdate` int(1)
,`userid` int(1)
,`quationstatus` int(1)
,`offerstatus` int(1)
,`accdate` int(1)
,`rejdate` int(1)
,`lastcntdate` int(1)
,`isactive` int(1)
,`counetr_status` int(1)
,`offerCounterStatus` int(1)
,`postuserid` int(1)
,`type` int(1)
,`industryname` int(1)
,`industryid` int(1)
,`catid` int(1)
,`subcatid` int(1)
,`brandid` int(1)
,`brandname` int(1)
,`categoryname` int(1)
,`subcategoryname` int(1)
,`customerrefno` int(1)
,`ptype` int(1)
,`postadvuserid` int(1)
,`pstatus` int(1)
,`advlocation` int(1)
,`postdate` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vworder`
--
CREATE TABLE IF NOT EXISTS `vworder` (
`pdate` int(1)
,`acc_offr_id` int(1)
,`counterid` int(1)
,`quotationno` int(1)
,`quoteid` int(1)
,`postno` int(1)
,`productno` int(1)
,`acceptedBy` int(1)
,`acc_date` int(1)
,`acc_price` int(1)
,`ptype` int(1)
,`acc_quantity` int(1)
,`acc_timeframe` int(1)
,`acc_expdate` int(1)
,`pstatus` int(1)
,`status` int(1)
,`noti_status` int(1)
,`tm_userID` int(1)
,`offerType` int(1)
,`industryname` int(1)
,`industryid` int(1)
,`addedby` int(1)
,`productname` int(1)
,`productid` int(1)
,`brandname` int(1)
,`categoryname` int(1)
,`subcategoryname` int(1)
,`targetprice` int(1)
,`price` int(1)
,`currency` int(1)
,`uom` int(1)
,`quantity` int(1)
,`location` int(1)
,`expdate` int(1)
,`expirydate` int(1)
,`language` int(1)
,`country` int(1)
,`timeframe` int(1)
,`quoteuserid` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwpost`
--
CREATE TABLE IF NOT EXISTS `vwpost` (
`pdate` int(1)
,`industryid` int(1)
,`industryname` int(1)
,`catid` int(1)
,`productid` int(1)
,`addedby` int(1)
,`productno` int(1)
,`subcatid` int(1)
,`brandid` int(1)
,`name` int(1)
,`customerrefno` int(1)
,`ptype` int(1)
,`userid` int(1)
,`pstatus` int(1)
,`categoryname` int(1)
,`subcategoryname` int(1)
,`brandname` int(1)
,`location` int(1)
,`currency` int(1)
,`uom` int(1)
,`quantity` int(1)
,`expdate` int(1)
,`expirydate` int(1)
,`pakaging` int(1)
,`language` int(1)
,`timeframe` int(1)
,`country` int(1)
,`targetprice` int(1)
,`postno` int(1)
,`buyer` int(1)
,`seller` int(1)
,`quotefacility` int(1)
,`productpacking` int(1)
,`shipingcondition` int(1)
,`caseweight` int(1)
,`qtypercase` int(1)
,`description` int(1)
,`manufacture` int(1)
,`productstatus` int(1)
,`mpm` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwpost1`
--
CREATE TABLE IF NOT EXISTS `vwpost1` (
`pdate` int(1)
,`industryid` int(1)
,`industryname` int(1)
,`productid` int(1)
,`catid` int(1)
,`productno` int(1)
,`subcatid` int(1)
,`brandid` int(1)
,`name` int(1)
,`customerrefno` int(1)
,`ptype` int(1)
,`userid` int(1)
,`pstatus` int(1)
,`categoryname` int(1)
,`subcategoryname` int(1)
,`brandname` int(1)
,`location` int(1)
,`currency` int(1)
,`uom` int(1)
,`quantity` int(1)
,`expdate` int(1)
,`expirydate` int(1)
,`pakaging` int(1)
,`language` int(1)
,`timeframe` int(1)
,`country` int(1)
,`targetprice` int(1)
,`postno` int(1)
,`buyer` int(1)
,`seller` int(1)
,`quotefacility` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwproduct`
--
CREATE TABLE IF NOT EXISTS `vwproduct` (
`productno` int(1)
,`pdate` int(1)
,`name` int(1)
,`productid` int(1)
,`industryid` int(1)
,`industryname` int(1)
,`brandid` int(1)
,`brandname` int(1)
,`catid` int(1)
,`categoryname` int(1)
,`subcatid` int(1)
,`subcategoryname` int(1)
,`pakaging` int(1)
,`shipingcondition` int(1)
,`caseweight` int(1)
,`qtypercase` int(1)
,`description` int(1)
,`manufacture` int(1)
,`pstatus` int(1)
,`mpm` int(1)
,`addedby` int(1)
,`isactive` int(1)
,`roletype` int(1)
,`img0` int(1)
,`img1` int(1)
,`img2` int(1)
,`img3` int(1)
,`img4` int(1)
,`img5` int(1)
,`img6` int(1)
,`img7` int(1)
,`code1` int(1)
,`codevalue1` int(1)
,`code2` int(1)
,`codevalue2` int(1)
,`code3` int(1)
,`codevalue3` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwproduct1`
--
CREATE TABLE IF NOT EXISTS `vwproduct1` (
`productno` int(1)
,`pdate` int(1)
,`productid` int(1)
,`name` int(1)
,`industryid` int(1)
,`industryname` int(1)
,`brandid` int(1)
,`brandname` int(1)
,`catid` int(1)
,`categoryname` int(1)
,`subcatid` int(1)
,`subcategoryname` int(1)
,`pakaging` int(1)
,`shipingcondition` int(1)
,`caseweight` int(1)
,`qtypercase` int(1)
,`description` int(1)
,`manufacture` int(1)
,`pstatus` int(1)
,`mpm` int(1)
,`addedby` int(1)
,`isactive` int(1)
,`roletype` int(1)
,`img0` int(1)
,`img1` int(1)
,`img2` int(1)
,`img3` int(1)
,`img4` int(1)
,`img5` int(1)
,`img6` int(1)
,`img7` int(1)
,`code1` int(1)
,`codevalue1` int(1)
,`code2` int(1)
,`codevalue2` int(1)
,`code3` int(1)
,`codevalue3` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwquotation`
--
CREATE TABLE IF NOT EXISTS `vwquotation` (
`pdate` int(1)
,`quotationno` int(1)
,`postno` int(1)
,`productno` int(1)
,`price` int(1)
,`currency` int(1)
,`uom` int(1)
,`quantity` int(1)
,`location` int(1)
,`expdate` int(1)
,`expirydate` int(1)
,`language` int(1)
,`country` int(1)
,`timeframe` int(1)
,`detail` int(1)
,`declinemsg` int(1)
,`offerdeclinemsg` int(1)
,`acceptmsg` int(1)
,`offeracceptmsg` int(1)
,`pdate1` int(1)
,`userid` int(1)
,`quationstatus` int(1)
,`offerstatus` int(1)
,`accdate` int(1)
,`rejdate` int(1)
,`lastcntdate` int(1)
,`isactive` int(1)
,`counetr_status` int(1)
,`offerCounterStatus` int(1)
,`postuserid` int(1)
,`type` int(1)
,`industryname` int(1)
,`industryid` int(1)
,`catid` int(1)
,`subcatid` int(1)
,`brandid` int(1)
,`brandname` int(1)
,`categoryname` int(1)
,`subcategoryname` int(1)
,`customerrefno` int(1)
,`postingID` int(1)
,`ptype` int(1)
,`postadvuserid` int(1)
,`pstatus` int(1)
,`advlocation` int(1)
,`postdate` int(1)
,`product_name` int(1)
,`addedby` int(1)
,`manufacture` int(1)
,`caseweight` int(1)
,`targetprice` int(1)
);
-- --------------------------------------------------------

--
-- Structure for view `customer_products`
--
DROP TABLE IF EXISTS `customer_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`184.107.95.%` SQL SECURITY DEFINER VIEW `customer_products` AS select `post_customer`.`refno` AS `refno`,`mst_product`.`productid` AS `productid`,`mst_product`.`name` AS `product_name`,`mst_product`.`productno` AS `productno`,`mst_product`.`industryid` AS `industryid`,`mst_product`.`brandid` AS `brandid`,`mst_product`.`catid` AS `catid`,`mst_product`.`subcatid` AS `subcatid`,`mst_product`.`caseweight` AS `caseweight`,`mst_product`.`description` AS `description`,`mst_product`.`manufacture` AS `manufacture`,`post_customer`.`custid` AS `custid`,`mst_industry`.`name` AS `industry_name`,`mst_category`.`name` AS `category_name`,`mst_subcategory`.`name` AS `subcategory_name`,`mst_brand`.`name` AS `brand_name`,`mst_product`.`qtypercase` AS `qtypercase`,`mst_product`.`pakaging` AS `pakaging`,`mst_product`.`shipingcondition` AS `shipingcondition`,`mst_product`.`code1` AS `code1`,`mst_product`.`codevalue1` AS `codevalue1`,`mst_product`.`code2` AS `code2`,`mst_product`.`codevalue2` AS `codevalue2`,`mst_product`.`code3` AS `code3`,`mst_product`.`codevalue3` AS `codevalue3`,`post_advertisment`.`ptype` AS `ptype`,`post_advertisment`.`targetprice` AS `targetprice`,`post_advertisment`.`currency` AS `currency`,`post_advertisment`.`uom` AS `uom`,`post_advertisment`.`quantity` AS `quantity`,`post_advertisment`.`location` AS `location`,`post_advertisment`.`expdate` AS `expdate`,`post_advertisment`.`expirydate` AS `expirydate`,`post_advertisment`.`customerrefno` AS `customerrefno`,`post_advertisment`.`pakaging` AS `post_advertisment_pakaging`,`post_advertisment`.`language` AS `post_advertisment_language`,`post_advertisment`.`timeframe` AS `post_advertisment_timeframe`,`post_advertisment`.`country` AS `post_advertisment_country`,`post_advertisment`.`userid` AS `post_advertisment_userid`,cast(`post_advertisment`.`pdate` as date) AS `pdate`,`post_advertisment`.`pstatus` AS `post_advertisment_pstatus`,`post_advertisment`.`postno` AS `post_advertisment_postno` from ((((((`mst_product` join `post_advertisment` on((`post_advertisment`.`productno` = `mst_product`.`productno`))) join `post_customer` on((`post_customer`.`refno` = `post_advertisment`.`customerrefno`))) join `mst_industry` on((`mst_industry`.`industryid` = `mst_product`.`industryid`))) join `mst_category` on((`mst_category`.`catid` = `mst_product`.`catid`))) join `mst_subcategory` on((`mst_subcategory`.`subcatid` = `mst_product`.`subcatid`))) join `mst_brand` on((`mst_brand`.`brandid` = `mst_product`.`brandid`)));

-- --------------------------------------------------------

--
-- Structure for view `offer_products`
--
DROP TABLE IF EXISTS `offer_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`184.107.95.%` SQL SECURITY DEFINER VIEW `offer_products` AS select `pq`.`quotationno` AS `quotationno`,`pst`.`postno` AS `postno`,`pro`.`productid` AS `productid`,`pro`.`name` AS `product_name`,`pro`.`productno` AS `productno`,`pro`.`industryid` AS `industryid`,`pro`.`catid` AS `catid`,`pro`.`subcatid` AS `subcatid`,`pro`.`brandid` AS `brandid`,ifnull(`i`.`name`,'Not Applicable') AS `industry`,ifnull(`c`.`name`,'Not Applicable') AS `category`,ifnull(`sc`.`name`,'Not Applicable') AS `subcategory`,ifnull(`b`.`name`,'Not Applicable') AS `brand`,`pro`.`manufacture` AS `manufacture`,`pro`.`caseweight` AS `caseweight`,`pro`.`qtypercase` AS `qtypercase`,`pro`.`pakaging` AS `pakaging`,`pro`.`shipingcondition` AS `shipingcondition`,`pro`.`code1` AS `code1`,`pro`.`codevalue1` AS `codevalue1`,`pro`.`code2` AS `code2`,`pro`.`codevalue2` AS `codevalue2`,`pro`.`code3` AS `code3`,`pro`.`codevalue3` AS `codevalue3`,`pro`.`description` AS `description`,`pro`.`img0` AS `img0`,`pro`.`img1` AS `img1`,`pro`.`img2` AS `img2`,`pro`.`img3` AS `img3`,`pro`.`img4` AS `img4`,`pro`.`img5` AS `img5`,`pro`.`img6` AS `img6`,`pro`.`img7` AS `img7`,`pst`.`ptype` AS `ptype`,`pst`.`targetprice` AS `targetprice`,`pst`.`currency` AS `currency`,`pst`.`uom` AS `uom`,`pst`.`quantity` AS `quantity`,`pst`.`location` AS `location`,`pst`.`expdate` AS `expdate`,`pst`.`expirydate` AS `expirydate`,`pst`.`customerrefno` AS `customerrefno`,`pst`.`pakaging` AS `post_advertisment_pakaging`,`pst`.`language` AS `post_advertisment_language`,`pst`.`timeframe` AS `post_advertisment_timeframe`,`pst`.`country` AS `post_advertisment_country`,`pst`.`userid` AS `post_advertisment_userid`,`pst`.`pdate` AS `post_advertisment_pdate`,`pq`.`quoteid` AS `quoteid`,cast(`pq`.`offercrdate` as date) AS `pdate`,`qc`.`counter_date` AS `counter_date`,`pq`.`offerstatus` AS `offerstatus` from (((((((`post_quotation` `pq` left join `post_advertisment` `pst` on((convert(`pq`.`postno` using utf8) = `pst`.`postno`))) left join `mst_product` `pro` on((convert(`pq`.`productno` using utf8) = `pro`.`productno`))) left join `mst_industry` `i` on((`pro`.`industryid` = `i`.`industryid`))) left join `mst_category` `c` on((`pro`.`catid` = `c`.`catid`))) left join `mst_subcategory` `sc` on((`pro`.`subcatid` = `sc`.`subcatid`))) left join `mst_brand` `b` on((`pro`.`brandid` = `b`.`brandid`))) left join `quotation_counter` `qc` on((`pq`.`quotationno` = `qc`.`quotationno`)));

-- --------------------------------------------------------

--
-- Structure for view `oredr_products`
--
DROP TABLE IF EXISTS `oredr_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`184.107.95.%` SQL SECURITY DEFINER VIEW `oredr_products` AS select `pq`.`quotationno` AS `quotationno`,`pst`.`postno` AS `postno`,`pro`.`productid` AS `productid`,`pro`.`name` AS `product_name`,`pro`.`productno` AS `productno`,`pro`.`industryid` AS `industryid`,`pro`.`catid` AS `catid`,`pro`.`subcatid` AS `subcatid`,`pro`.`brandid` AS `brandid`,ifnull(`i`.`name`,'Not Applicable') AS `industry`,ifnull(`c`.`name`,'Not Applicable') AS `category`,ifnull(`sc`.`name`,'Not Applicable') AS `subcategory`,ifnull(`b`.`name`,'Not Applicable') AS `brand`,`pro`.`manufacture` AS `manufacture`,`pro`.`caseweight` AS `caseweight`,`pro`.`qtypercase` AS `qtypercase`,`pro`.`pakaging` AS `pakaging`,`pro`.`shipingcondition` AS `shipingcondition`,`pro`.`code1` AS `code1`,`pro`.`codevalue1` AS `codevalue1`,`pro`.`code2` AS `code2`,`pro`.`codevalue2` AS `codevalue2`,`pro`.`code3` AS `code3`,`pro`.`codevalue3` AS `codevalue3`,`pro`.`description` AS `description`,`pro`.`img0` AS `img0`,`pro`.`img1` AS `img1`,`pro`.`img2` AS `img2`,`pro`.`img3` AS `img3`,`pro`.`img4` AS `img4`,`pro`.`img5` AS `img5`,`pro`.`img6` AS `img6`,`pro`.`img7` AS `img7`,`pst`.`ptype` AS `ptype`,`pst`.`targetprice` AS `targetprice`,`pst`.`currency` AS `currency`,`pst`.`uom` AS `uom`,`pst`.`quantity` AS `quantity`,`pst`.`location` AS `location`,`pst`.`expdate` AS `expdate`,`pst`.`expirydate` AS `expirydate`,`pst`.`customerrefno` AS `customerrefno`,`pst`.`pakaging` AS `post_advertisment_pakaging`,`pst`.`language` AS `post_advertisment_language`,`pst`.`timeframe` AS `post_advertisment_timeframe`,`pst`.`country` AS `post_advertisment_country`,`pst`.`userid` AS `post_advertisment_userid`,`pst`.`pdate` AS `post_advertisment_pdate`,`pq`.`quoteid` AS `quoteid`,cast(`pq`.`offercrdate` as date) AS `pdate`,`qc`.`counter_date` AS `counter_date`,`pq`.`offerstatus` AS `offerstatus`,`pst`.`buyer` AS `buyer`,`pst`.`seller` AS `seller`,`pst`.`quotefacility` AS `quotefacility` from (((((((`post_quotation` `pq` left join `post_advertisment` `pst` on((convert(`pq`.`postno` using utf8) = `pst`.`postno`))) left join `mst_product` `pro` on((convert(`pq`.`productno` using utf8) = `pro`.`productno`))) left join `mst_industry` `i` on((`pro`.`industryid` = `i`.`industryid`))) left join `mst_category` `c` on((`pro`.`catid` = `c`.`catid`))) left join `mst_subcategory` `sc` on((`pro`.`subcatid` = `sc`.`subcatid`))) left join `mst_brand` `b` on((`pro`.`brandid` = `b`.`brandid`))) left join `quotation_counter` `qc` on((`pq`.`quotationno` = `qc`.`quotationno`)));

-- --------------------------------------------------------

--
-- Structure for view `post_products`
--
DROP TABLE IF EXISTS `post_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`184.107.95.%` SQL SECURITY DEFINER VIEW `post_products` AS select `post_advertisment`.`postno` AS `postno`,`mst_product`.`productid` AS `productid`,`mst_product`.`name` AS `product_name`,`mst_product`.`productno` AS `productno`,`mst_product`.`industryid` AS `industryid`,`mst_product`.`catid` AS `catid`,`mst_product`.`subcatid` AS `subcatid`,`mst_product`.`brandid` AS `brandid`,`mst_industry`.`name` AS `industry_name`,`mst_category`.`name` AS `category_name`,`mst_subcategory`.`name` AS `subcategory_name`,`mst_brand`.`name` AS `brand_name`,`mst_product`.`manufacture` AS `manufacture`,`mst_product`.`caseweight` AS `caseweight`,`mst_product`.`qtypercase` AS `qtypercase`,`mst_product`.`pakaging` AS `pakaging`,`mst_product`.`shipingcondition` AS `shipingcondition`,`mst_product`.`code1` AS `code1`,`mst_product`.`codevalue1` AS `codevalue1`,`mst_product`.`code2` AS `code2`,`mst_product`.`codevalue2` AS `codevalue2`,`mst_product`.`code3` AS `code3`,`mst_product`.`codevalue3` AS `codevalue3`,`mst_product`.`description` AS `description`,`mst_product`.`img0` AS `img0`,`mst_product`.`img1` AS `img1`,`mst_product`.`img2` AS `img2`,`mst_product`.`img3` AS `img3`,`mst_product`.`img4` AS `img4`,`mst_product`.`img5` AS `img5`,`mst_product`.`img6` AS `img6`,`mst_product`.`img7` AS `img7`,`post_advertisment`.`ptype` AS `ptype`,`post_advertisment`.`targetprice` AS `targetprice`,`post_advertisment`.`currency` AS `currency`,`post_advertisment`.`uom` AS `uom`,`post_advertisment`.`quantity` AS `quantity`,`post_advertisment`.`location` AS `location`,`post_advertisment`.`expdate` AS `expdate`,`post_advertisment`.`expirydate` AS `expirydate`,`post_advertisment`.`customerrefno` AS `customerrefno`,`post_advertisment`.`pakaging` AS `post_advertisment_pakaging`,`post_advertisment`.`language` AS `post_advertisment_language`,`post_advertisment`.`timeframe` AS `post_advertisment_timeframe`,`post_advertisment`.`country` AS `post_advertisment_country`,`post_advertisment`.`userid` AS `post_advertisment_userid`,cast(`post_advertisment`.`pdate` as date) AS `pdate`,`post_advertisment`.`pstatus` AS `post_advertisment_pstatus` from (((((`post_advertisment` left join `mst_product` on((`post_advertisment`.`productno` = `mst_product`.`productno`))) left join `mst_industry` on((`mst_product`.`industryid` = `mst_industry`.`industryid`))) left join `mst_category` on((`mst_product`.`catid` = `mst_category`.`catid`))) left join `mst_subcategory` on((`mst_product`.`subcatid` = `mst_subcategory`.`subcatid`))) left join `mst_brand` on((`mst_product`.`brandid` = `mst_brand`.`brandid`)));

-- --------------------------------------------------------

--
-- Structure for view `pro_products`
--
DROP TABLE IF EXISTS `pro_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`184.107.95.%` SQL SECURITY DEFINER VIEW `pro_products` AS select `mst_product`.`productid` AS `productid`,`mst_product`.`name` AS `product_name`,`mst_product`.`productno` AS `productno`,`mst_product`.`industryid` AS `industryid`,`mst_product`.`catid` AS `catid`,`mst_product`.`subcatid` AS `subcatid`,`mst_product`.`brandid` AS `brandid`,`mst_industry`.`name` AS `industry_name`,`mst_category`.`name` AS `category_name`,`mst_subcategory`.`name` AS `subcategory_name`,`mst_brand`.`name` AS `brand_name`,`mst_product`.`manufacture` AS `manufacture`,`mst_product`.`caseweight` AS `caseweight`,`mst_product`.`qtypercase` AS `qtypercase`,`mst_product`.`pakaging` AS `pakaging`,`mst_product`.`shipingcondition` AS `shipingcondition`,`mst_product`.`code1` AS `code1`,`mst_product`.`codevalue1` AS `codevalue1`,`mst_product`.`code2` AS `code2`,`mst_product`.`codevalue2` AS `codevalue2`,`mst_product`.`code3` AS `code3`,`mst_product`.`codevalue3` AS `codevalue3`,`mst_product`.`description` AS `description`,`mst_product`.`img0` AS `img0`,`mst_product`.`img1` AS `img1`,`mst_product`.`img2` AS `img2`,`mst_product`.`img3` AS `img3`,`mst_product`.`img4` AS `img4`,`mst_product`.`img5` AS `img5`,`mst_product`.`img6` AS `img6`,`mst_product`.`img7` AS `img7`,`mst_product`.`isactive` AS `isactive`,`mst_product`.`pstatus` AS `product_status`,`mst_product`.`mpm` AS `mpm`,`mst_product`.`addedby` AS `product_userid`,cast(`mst_product`.`date` as date) AS `pdate` from ((((`mst_product` join `mst_industry` on((`mst_industry`.`industryid` = `mst_product`.`industryid`))) join `mst_category` on((`mst_category`.`catid` = `mst_product`.`catid`))) join `mst_subcategory` on((`mst_subcategory`.`subcatid` = `mst_product`.`subcatid`))) join `mst_brand` on((`mst_brand`.`brandid` = `mst_product`.`brandid`)));

-- --------------------------------------------------------

--
-- Structure for view `qutation_products`
--
DROP TABLE IF EXISTS `qutation_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`184.107.95.%` SQL SECURITY DEFINER VIEW `qutation_products` AS select `pq`.`quotationno` AS `quotationno`,`pst`.`postno` AS `postno`,`pro`.`productid` AS `productid`,`pro`.`name` AS `product_name`,`pro`.`productno` AS `productno`,`pro`.`industryid` AS `industryid`,`pro`.`catid` AS `catid`,`pro`.`subcatid` AS `subcatid`,`pro`.`brandid` AS `brandid`,ifnull(`i`.`name`,'Not Applicable') AS `industry`,ifnull(`c`.`name`,'Not Applicable') AS `category`,ifnull(`sc`.`name`,'Not Applicable') AS `subcategory`,ifnull(`b`.`name`,'Not Applicable') AS `brand`,`pro`.`manufacture` AS `manufacture`,`pro`.`caseweight` AS `caseweight`,`pro`.`qtypercase` AS `qtypercase`,`pro`.`pakaging` AS `pakaging`,`pro`.`shipingcondition` AS `shipingcondition`,`pro`.`code1` AS `code1`,`pro`.`codevalue1` AS `codevalue1`,`pro`.`code2` AS `code2`,`pro`.`codevalue2` AS `codevalue2`,`pro`.`code3` AS `code3`,`pro`.`codevalue3` AS `codevalue3`,`pro`.`description` AS `description`,`pro`.`img0` AS `img0`,`pro`.`img1` AS `img1`,`pro`.`img2` AS `img2`,`pro`.`img3` AS `img3`,`pro`.`img4` AS `img4`,`pro`.`img5` AS `img5`,`pro`.`img6` AS `img6`,`pro`.`img7` AS `img7`,`pst`.`ptype` AS `ptype`,`pst`.`targetprice` AS `targetprice`,`pst`.`currency` AS `currency`,`pst`.`uom` AS `uom`,`pst`.`quantity` AS `quantity`,`pst`.`location` AS `location`,`pst`.`expdate` AS `expdate`,`pst`.`expirydate` AS `expirydate`,`pst`.`customerrefno` AS `customerrefno`,`pst`.`pakaging` AS `post_advertisment_pakaging`,`pst`.`language` AS `post_advertisment_language`,`pst`.`timeframe` AS `post_advertisment_timeframe`,`pst`.`country` AS `post_advertisment_country`,`pst`.`userid` AS `post_advertisment_userid`,`pst`.`pdate` AS `post_advertisment_pdate`,`pq`.`quoteid` AS `quoteid`,cast(`pq`.`offercrdate` as date) AS `pdate`,cast(`qc`.`counter_date` as date) AS `counter_date`,`pq`.`offerstatus` AS `offerstatus`,`pq`.`quationstatus` AS `quationstatus`,`pq`.`type` AS `quationtype`,`pq`.`userid` AS `quationuserid` from (((((((`post_quotation` `pq` left join `post_advertisment` `pst` on((convert(`pq`.`postno` using utf8) = `pst`.`postno`))) left join `mst_product` `pro` on((convert(`pq`.`productno` using utf8) = `pro`.`productno`))) left join `mst_industry` `i` on((`pro`.`industryid` = `i`.`industryid`))) left join `mst_category` `c` on((`pro`.`catid` = `c`.`catid`))) left join `mst_subcategory` `sc` on((`pro`.`subcatid` = `sc`.`subcatid`))) left join `mst_brand` `b` on((`pro`.`brandid` = `b`.`brandid`))) left join `quotation_counter` `qc` on((`pq`.`quotationno` = `qc`.`quotationno`)));

-- --------------------------------------------------------

--
-- Structure for view `vwcustomer1`
--
DROP TABLE IF EXISTS `vwcustomer1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwcustomer1` AS select 1 AS `productno`,1 AS `pdate`,1 AS `name`,1 AS `productid`,1 AS `industryid`,1 AS `industryname`,1 AS `brandid`,1 AS `brandname`,1 AS `catid`,1 AS `categoryname`,1 AS `subcatid`,1 AS `subcategoryname`,1 AS `pakaging`,1 AS `shipingcondition`,1 AS `caseweight`,1 AS `qtypercase`,1 AS `description`,1 AS `manufacture`,1 AS `pstatus`,1 AS `mpm`,1 AS `addedby`,1 AS `isactive`,1 AS `roletype`,1 AS `img0`,1 AS `img1`,1 AS `img2`,1 AS `img3`,1 AS `img4`,1 AS `img5`,1 AS `img6`,1 AS `img7`,1 AS `code1`,1 AS `codevalue1`,1 AS `code2`,1 AS `codevalue2`,1 AS `code3`,1 AS `codevalue3`;

-- --------------------------------------------------------

--
-- Structure for view `vwoffers`
--
DROP TABLE IF EXISTS `vwoffers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwoffers` AS select 1 AS `pdate`,1 AS `quotationno`,1 AS `postno`,1 AS `productno`,1 AS `price`,1 AS `name`,1 AS `productid`,1 AS `addedby`,1 AS `targetprice`,1 AS `currency`,1 AS `uom`,1 AS `quantity`,1 AS `location`,1 AS `expdate`,1 AS `expirydate`,1 AS `language`,1 AS `country`,1 AS `timeframe`,1 AS `detail`,1 AS `declinemsg`,1 AS `offerdeclinemsg`,1 AS `acceptmsg`,1 AS `offeracceptmsg`,1 AS `offercrdate`,1 AS `userid`,1 AS `quationstatus`,1 AS `offerstatus`,1 AS `accdate`,1 AS `rejdate`,1 AS `lastcntdate`,1 AS `isactive`,1 AS `counetr_status`,1 AS `offerCounterStatus`,1 AS `postuserid`,1 AS `type`,1 AS `industryname`,1 AS `industryid`,1 AS `catid`,1 AS `subcatid`,1 AS `brandid`,1 AS `brandname`,1 AS `categoryname`,1 AS `subcategoryname`,1 AS `customerrefno`,1 AS `ptype`,1 AS `postadvuserid`,1 AS `pstatus`,1 AS `advlocation`,1 AS `postdate`;

-- --------------------------------------------------------

--
-- Structure for view `vworder`
--
DROP TABLE IF EXISTS `vworder`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vworder` AS select 1 AS `pdate`,1 AS `acc_offr_id`,1 AS `counterid`,1 AS `quotationno`,1 AS `quoteid`,1 AS `postno`,1 AS `productno`,1 AS `acceptedBy`,1 AS `acc_date`,1 AS `acc_price`,1 AS `ptype`,1 AS `acc_quantity`,1 AS `acc_timeframe`,1 AS `acc_expdate`,1 AS `pstatus`,1 AS `status`,1 AS `noti_status`,1 AS `tm_userID`,1 AS `offerType`,1 AS `industryname`,1 AS `industryid`,1 AS `addedby`,1 AS `productname`,1 AS `productid`,1 AS `brandname`,1 AS `categoryname`,1 AS `subcategoryname`,1 AS `targetprice`,1 AS `price`,1 AS `currency`,1 AS `uom`,1 AS `quantity`,1 AS `location`,1 AS `expdate`,1 AS `expirydate`,1 AS `language`,1 AS `country`,1 AS `timeframe`,1 AS `quoteuserid`;

-- --------------------------------------------------------

--
-- Structure for view `vwpost`
--
DROP TABLE IF EXISTS `vwpost`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwpost` AS select 1 AS `pdate`,1 AS `industryid`,1 AS `industryname`,1 AS `catid`,1 AS `productid`,1 AS `addedby`,1 AS `productno`,1 AS `subcatid`,1 AS `brandid`,1 AS `name`,1 AS `customerrefno`,1 AS `ptype`,1 AS `userid`,1 AS `pstatus`,1 AS `categoryname`,1 AS `subcategoryname`,1 AS `brandname`,1 AS `location`,1 AS `currency`,1 AS `uom`,1 AS `quantity`,1 AS `expdate`,1 AS `expirydate`,1 AS `pakaging`,1 AS `language`,1 AS `timeframe`,1 AS `country`,1 AS `targetprice`,1 AS `postno`,1 AS `buyer`,1 AS `seller`,1 AS `quotefacility`,1 AS `productpacking`,1 AS `shipingcondition`,1 AS `caseweight`,1 AS `qtypercase`,1 AS `description`,1 AS `manufacture`,1 AS `productstatus`,1 AS `mpm`;

-- --------------------------------------------------------

--
-- Structure for view `vwpost1`
--
DROP TABLE IF EXISTS `vwpost1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwpost1` AS select 1 AS `pdate`,1 AS `industryid`,1 AS `industryname`,1 AS `productid`,1 AS `catid`,1 AS `productno`,1 AS `subcatid`,1 AS `brandid`,1 AS `name`,1 AS `customerrefno`,1 AS `ptype`,1 AS `userid`,1 AS `pstatus`,1 AS `categoryname`,1 AS `subcategoryname`,1 AS `brandname`,1 AS `location`,1 AS `currency`,1 AS `uom`,1 AS `quantity`,1 AS `expdate`,1 AS `expirydate`,1 AS `pakaging`,1 AS `language`,1 AS `timeframe`,1 AS `country`,1 AS `targetprice`,1 AS `postno`,1 AS `buyer`,1 AS `seller`,1 AS `quotefacility`;

-- --------------------------------------------------------

--
-- Structure for view `vwproduct`
--
DROP TABLE IF EXISTS `vwproduct`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwproduct` AS select 1 AS `productno`,1 AS `pdate`,1 AS `name`,1 AS `productid`,1 AS `industryid`,1 AS `industryname`,1 AS `brandid`,1 AS `brandname`,1 AS `catid`,1 AS `categoryname`,1 AS `subcatid`,1 AS `subcategoryname`,1 AS `pakaging`,1 AS `shipingcondition`,1 AS `caseweight`,1 AS `qtypercase`,1 AS `description`,1 AS `manufacture`,1 AS `pstatus`,1 AS `mpm`,1 AS `addedby`,1 AS `isactive`,1 AS `roletype`,1 AS `img0`,1 AS `img1`,1 AS `img2`,1 AS `img3`,1 AS `img4`,1 AS `img5`,1 AS `img6`,1 AS `img7`,1 AS `code1`,1 AS `codevalue1`,1 AS `code2`,1 AS `codevalue2`,1 AS `code3`,1 AS `codevalue3`;

-- --------------------------------------------------------

--
-- Structure for view `vwproduct1`
--
DROP TABLE IF EXISTS `vwproduct1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwproduct1` AS select 1 AS `productno`,1 AS `pdate`,1 AS `productid`,1 AS `name`,1 AS `industryid`,1 AS `industryname`,1 AS `brandid`,1 AS `brandname`,1 AS `catid`,1 AS `categoryname`,1 AS `subcatid`,1 AS `subcategoryname`,1 AS `pakaging`,1 AS `shipingcondition`,1 AS `caseweight`,1 AS `qtypercase`,1 AS `description`,1 AS `manufacture`,1 AS `pstatus`,1 AS `mpm`,1 AS `addedby`,1 AS `isactive`,1 AS `roletype`,1 AS `img0`,1 AS `img1`,1 AS `img2`,1 AS `img3`,1 AS `img4`,1 AS `img5`,1 AS `img6`,1 AS `img7`,1 AS `code1`,1 AS `codevalue1`,1 AS `code2`,1 AS `codevalue2`,1 AS `code3`,1 AS `codevalue3`;

-- --------------------------------------------------------

--
-- Structure for view `vwquotation`
--
DROP TABLE IF EXISTS `vwquotation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`zeesoft_zeesoft`@`localhost` SQL SECURITY DEFINER VIEW `vwquotation` AS select 1 AS `pdate`,1 AS `quotationno`,1 AS `postno`,1 AS `productno`,1 AS `price`,1 AS `currency`,1 AS `uom`,1 AS `quantity`,1 AS `location`,1 AS `expdate`,1 AS `expirydate`,1 AS `language`,1 AS `country`,1 AS `timeframe`,1 AS `detail`,1 AS `declinemsg`,1 AS `offerdeclinemsg`,1 AS `acceptmsg`,1 AS `offeracceptmsg`,1 AS `pdate1`,1 AS `userid`,1 AS `quationstatus`,1 AS `offerstatus`,1 AS `accdate`,1 AS `rejdate`,1 AS `lastcntdate`,1 AS `isactive`,1 AS `counetr_status`,1 AS `offerCounterStatus`,1 AS `postuserid`,1 AS `type`,1 AS `industryname`,1 AS `industryid`,1 AS `catid`,1 AS `subcatid`,1 AS `brandid`,1 AS `brandname`,1 AS `categoryname`,1 AS `subcategoryname`,1 AS `customerrefno`,1 AS `postingID`,1 AS `ptype`,1 AS `postadvuserid`,1 AS `pstatus`,1 AS `advlocation`,1 AS `postdate`,1 AS `product_name`,1 AS `addedby`,1 AS `manufacture`,1 AS `caseweight`,1 AS `targetprice`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
