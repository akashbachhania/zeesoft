<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImport extends Model
{
    protected $table = 'productimport';
    protected $primaryKey = 'productid';

}
