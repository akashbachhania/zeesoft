<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstPageName extends Model
{
    protected $table = 'mst_pagename';
    protected $primaryKey = 'rowid';
}
