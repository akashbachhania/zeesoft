<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    protected $table = 'mst_userrole';

    public function users()
    {
        return $this->hasMany('App\User', 'role_id', 'role_id');
    }

}
