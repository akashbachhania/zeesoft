<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProductReport extends Model
{
    protected $table = 'mst_productreport';
    protected $primaryKey = 'preportid';

   public function product(){
        return $this->belongsTo('App\MstProduct','productid','productid');
    }    
}
