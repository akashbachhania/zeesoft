<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class QuoteQueueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function quoteandpost(){

    	$checkMaskVal = \App\MaskProname::where('id','1')->get();

    	return View('quoteQueue.index',['checkMaskVal'=>$checkMaskVal])->with('title','TradingSoft');

    }

    public function quoteandpost_ajax(Request $request){
    	
		if ( !empty( $request->userId ) ) { 

        	$group = "AND pa.userid='".$request->userId."'";
    	}
		if (empty(Auth::user()->industry) == false) {
	            $industry1  = Auth::user()->industry;
	            $territory1 = ' Where mp.industryid IN (' . $industry1 . ') and pa.pstatus<>"COMPLETED"' ;
        }
		else
		{
		$territory1 = "WHERE pa.pstatus='ACTIVE'";
		}
		
        
        if (empty(Auth::user()->category) == false) {
            $category1  = Auth::user()->category;
            $territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
        }
        
        if (empty(Auth::user()->subcategory) == false) {
            $subcategory1 = Auth::user()->subcategory;
            $territory1   = $territory1 . ' AND mp.subcatid IN (' . $subcategory1 . ')';
        }
        
        if (empty(Auth::user()->brand) == false) {
            $brand1     = Auth::user()->brand;
            $territory1 = $territory1 . ' AND mp.brandid IN (' . $brand1 . ')';
        }
	
    if (isset($_COOKIE["productActionType"])) {
        $customAction = $_COOKIE['customActionName'];
        $exploadData  = explode('-', $customAction);
        $status       = (isset($_COOKIE['status']) && $_COOKIE['status']!="undefined") ? $_REQUEST['status'] : '';
        $radioread    = (isset($_COOKIE['radioread']) && $_COOKIE['radioread']!="undefined") ? $_REQUEST['radioread'] : '';
        $orderbypost  = (isset($_COOKIE['customActionName']) && $_COOKIE['customActionName']!="undefined") ? $_REQUEST['customActionName'] : '';
       
	    if (empty($orderbypost) == false && $exploadData[1] == 'ASC')
			{
			$orderby = " ORDER BY " . $exploadData[0] . " ASC";
			}

		if (empty($orderbypost) == false && $exploadData[1] == 'DESC')
			{
			$orderby = " ORDER BY " . $exploadData[0] . " DESC";
			}

        
        if (empty($orderbypost) == false && $exploadData[1] == 'Buy') {
            $filter = "AND ptype='Buy'";
        }
        
        if (empty($orderbypost) == false && $exploadData[1] == 'Sell') {
            $filter = "AND ptype='Sell'";
        }
        if (empty($orderbypost) == false && $exploadData[0] == 'status') {
            $filter = "AND quationstatus='" . $exploadData[1] . "'";
        }
        
        
        if (empty($orderbypost) == false && $exploadData[1] == 'Buy') { 
            $group = "AND ptype='Buy'";
        }
        
        if (empty($orderbypost) == false && $exploadData[1] == 'Sell') {
            $group = "AND ptype='Sell'";
        }
        if (empty($orderbypost) == false && $exploadData[0] == 'status') {
            $group = "AND quationstatus='" . $exploadData[1] . "'";
        }
        
        $sqlcnt = "select count(*) as totalCount from post_advertisment pa left join mst_product mp On pa.productno=mp.productno 
		 LEFT JOIN mst_industry i ON mp.industryid=i.industryid LEFT JOIN mst_category c ON mp.catid=c.catid LEFT JOIN post_customer cu ON pa.customerrefno=cu.refno
		 LEFT JOIN mst_userlogin mu ON pa.userid=mu.userid
		 LEFT JOIN mst_brand b ON mp.brandid = b.brandid WHERE pa.pstatus not in('Accepted','COMPLETED','CANCELLED','EXPIRED') AND mp.pstatus = 'APPROVED' AND pa.act_inactive=0 AND cu.isactive=1 AND mu.isactive=1 " . $filter . "";
		 
		 $sqlto="SELECT count(*) as totaltoCount FROM post_advertisment where postno in (select postno from post_quotation where counetr_status in ('Accepted'))";
		 $resultto = $conn->query($sqlto);
        $result = $conn->query($sqlcnt);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
				$itoRecords = $row["totalCount"];
				$ito1Records = $rowto["totaltoCount"];
				//$iTotalRecords = ($itoRecords-$ito1Records);
				$iTotalRecords = $row["totalCount"];
            }
        }
    } else {
      }
   
    
    if (isset($_COOKIE["productActionType"]) && $_COOKIE["productActionType"] == "group_action") {
        
    	$sql_result =  \App\PostAdvertisement::whereNotIn('pstatus',['Accepted','COMPLETED','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($query){
		            $query->where('pstatus','APPROVED');
		        })->whereHas('postCustomer',function($query){
		            $query->where('isactive','1');
		        })->whereHas('user',function($query){
		            $query->where('isactive','1');
		        })->get();
       
        
    } else {

    	 $sql_result =  \App\PostAdverstisement::whereNotIn('pstatus',['Accepted','COMPLETED','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($query){
		            $query->where('pstatus','APPROVED');
		        })->whereHas('postCustomer',function($query){
		            $query->where('isactive','1');
		        })->whereHas('user',function($query){
		            $query->where('isactive','1');
		        })->orderBy('pdate','DESC')->get();
        

    }
    	$record = array();
		$i = 0;		
        foreach ($sql_result as $value) {
     
            $sdate      = date('F d Y - h:i A',strtotime($value->pdate));
            $productno 	   = isset($value->productno)?$value->productno:'';
			$postnoid 	   = isset($value->postno)?$value->postno:'';
			// $quotationno   = $row["quotationno"];
            $postid        = isset($value->postid)?$value->postid:'';
            $postno        = isset($value->postno)?$value->postno:'';
            $brand         = isset($value->product->brand->name)?$value->product->brand->name:'';
            $ptype         = ucwords(strtolower(isset($value->ptype)?$value->ptype:''));
            $quantity      = isset($value->quantity)?$value->quantity:'';
            $productName   = isset($value->product->name)?$value->product->name:'';
            $price         = isset($value->targetprice)?$value->targetprice:'';
            $currency      = isset($value->currency)?$value->currency:'';
			$timeframe      = isset($value->timeframe)?$value->timeframe:'';
			$expdate      = isset($value->expdate)?$value->expdate:'';
            $customerrefno = isset($value->customerrefno)?$value->customerrefno:'';
			$poststatus		= isset($value->pstatus)?$value->pstatus:'';
            
			// $countid       = "select count(*) as rowcount from post_quotation Where postno='" .$row["postno"]. "'";
   //          $countidresult = $conn->query($countid);
   //          $countrows     = $countidresult->fetch_assoc();
   //          $countro       = $countrows['rowcount'];
			
			// $sqlqu      = "select quotationno from post_quotation Where postno='" .$postno. "' and type in ('QUOTATION','OFFER')";
   //          $resultqu 	= $conn->query($sqlqu);
   //          $rowsqu  	= $resultqu->fetch_assoc();
   //          $quotationno    = $rowsqu['quotationno'];
			
			if($poststatus=='NEW')
			{
			 $poststatus  = 'NEW'.'<a href="quotepostDetailView/' . $postnoid . '/'.$productno.'/'.$ptype .'" style="display:none;" id="quoest"><i class="fa fa-trash-o"></i></a>';
			}
			else
			{
			$poststatus = $poststatus.'<a href="quotepostDetailView/' . $postnoid . '/'.$productno.'/'.$ptype .'" style="display:none;" id="quoest"><i class="fa fa-trash-o"></i></a>';
			}
	
	    	$checkMaskVal = \App\MaskProname::where('id','1')->get();			
			
			$maskstatus=$checkMaskVal[0]->maskstatus;
			
			if(Auth::user()->roletype != 'AD'){
				if($maskstatus=='1'){
					$productNameshow=$productName;
				}else{
					$productNameshow=$productno;
				}
				
			}else{
				$productNameshow=$productName;
			}
			
			
            $record[$i]['postno'] = $postno;
            $record[$i]['sdate'] = $sdate;
            $record[$i]['ptype'] = $ptype;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['productNameshow'] = $productNameshow;
            $record[$i]['price'] = $price;
            $record[$i]['currency'] = $currency;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;
            $record[$i]['poststatus'] = $poststatus;
            
            $i++;
        }
        return \Response::json($record);

// echo "<center><img src='image/error.png'></center>";


    }

    public function quoteandpost1_ajax(Request $request){

    	if (isset($reuest->Type)) {
		    if ($reuest->Type == 'add') {
		        $name   = $reuest->name;
		        $status = $request->status;
		        $sql    = "insert into mst_brand (name,isactive) values('$name','$status')";
		     
		        exit();
		    } else if ($request->Type == 'update') {
		        $name   = $request->name;
		        $status = $request->status;
		        $id     = $request->id;
		        $sql    = "update mst_brand set name='$name',isactive='$status' where  brandid='$id'";
		     
		        exit();
		    } else if ($request->Type == 'delete') {
		        $productid     = $request->productid;
		        $sql           = "delete from mst_product where productid='$productid'";
		        $result        = $conn->query($sql);
		        $sql_action    = "INSERT INTO mst_useraction (userid,ipaddress,sessionid,actiondate,actiontime,actionname) VALUES ('$userid','$ipaddress','$sessionid','$actiondate','$finishtime','$action')";
		     
		        exit();
		    }

		}


    	$productActionType = $request->productActionType;
    	
	    if (isset($productActionType)) {
	        $customAction = $_REQUEST['customActionName'];
	        $exploadData  = explode('-', $customAction);
	        $status       = (isset($_REQUEST['status'])) ? $_REQUEST['status'] : '';
	        $radioread    = (isset($_REQUEST['radioread'])) ? $_REQUEST['radioread'] : '';
	        $orderbypost  = (isset($_REQUEST['customActionName'])) ? $_REQUEST['customActionName'] : '';
	        if (empty($orderbypost) == false && $exploadData[1] == 'ASC') {
	            $orderby = " ORDER BY " . $exploadData[0] . " ASC";
	        }
	        
	        if (empty($orderbypost) == false && $exploadData[1] == 'DESC') {
	            $orderby = " ORDER BY " . $exploadData[0] . " DESC";
	        }
	        
	        if (empty($orderbypost) == false && $exploadData[1] == 'Buy') {
	            $filter = "AND ptype='Buy'";
	        }
	        
	        if (empty($orderbypost) == false && $exploadData[1] == 'Sell') {
	            $filter = "AND ptype='Sell'";
	        }
	        if (empty($orderbypost) == false && $exploadData[0] == 'status') {
	            $filter = "AND quationstatus='" . $exploadData[1] . "'";
	        }
	        
	        
	        if (empty($orderbypost) == false && $exploadData[1] == 'Buy') {
	            $group = "AND ptype='Buy'";
	        }
	        
	        if (empty($orderbypost) == false && $exploadData[1] == 'Sell') {
	            $group = "AND ptype='Sell'";
	        }
	        if (empty($orderbypost) == false && $exploadData[0] == 'status') {
	            $group = "AND quationstatus='" . $exploadData[1] . "'";
	        }
	        
	        
	        $sqlcnt = "select count(*) as totalCount from post_quotation pq left join mst_product pro On pq.productno=pro.productno 
			 LEFT JOIN post_advertisment po ON pq.postno = po.postno where po.pstatus NOT IN ('Accepted',EXPIRED',  'Canceled',  'Completed')" . $filter;
	        $result = $conn->query($sqlcnt);
			
	/*		  $myFile = "ramafilequote3.txt";
	        $fh = fopen($myFile, 'w') or die("can't open file");
	        fwrite($fh, $sqlcnt);
	        fclose($fh);
	*/		
	        if ($result->num_rows > 0) {
	            while ($row = $result->fetch_assoc()) {
	                $iTotalRecords = $row["totalCount"];
	            }
	        }
	    } 
	    else {
	        $A1     = "";
	        $A2     = "";
	        $sqlcnt = "SELECT COUNT( * ) AS totalCount FROM post_quotation pq
	                   LEFT JOIN post_advertisment po ON pq.postno = po.postno WHERE po.pstatus NOT IN ('Accepted','EXPIRED',  'Canceled',  'Completed')";

	    }
    
    // if (isset($productActionType)) {
    //     if (isset($_REQUEST["industryData"]))
    //         $industryData = $_REQUEST["industryData"];
    //     $iDisplayLength = intval($_REQUEST['length']);
    //     $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
    //     $iDisplayStart  = intval($_REQUEST['start']);
    //     $sEcho          = intval($_REQUEST['draw']);
    //     if (isset($_REQUEST["brandData"]))
    //         $brandData = $_REQUEST["brandData"];
    //     $records         = array();
    //     $records["data"] = array();
    //     $end             = $iDisplayStart + $iDisplayLength;
    //     $end             = $end > $iTotalRecords ? $iTotalRecords : $end;
    //     if (isset($_REQUEST["categoryData"]))
    //         $categoryData = $_REQUEST["categoryData"];
    //     $records         = array();
    //     $records["data"] = array();
    //     $end             = $iDisplayStart + $iDisplayLength;
    //     $end             = $end > $iTotalRecords ? $iTotalRecords : $end;
    //     if (isset($_REQUEST["scategoryData"]))
    //         $categoryData = $_REQUEST["scategoryData"];
    //     $records         = array();
    //     $records["data"] = array();
    //     $end             = $iDisplayStart + $iDisplayLength;
    //     $end             = $end > $iTotalRecords ? $iTotalRecords : $end;
    //     if (isset($_REQUEST["brandData"]))
    //         $categoryData = $_REQUEST["brandData"];
    //     $records         = array();
    //     $records["data"] = array();
    //     $end             = $iDisplayStart + $iDisplayLength;
    //     $end             = $end > $iTotalRecords ? $iTotalRecords : $end;
    // } else {
    //     $A1             = $_REQUEST["A1"];
    //     $iDisplayLength = intval($_REQUEST['length']);
    //     $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
    //     $iDisplayStart  = intval($_REQUEST['start']);
    //     $sEcho          = intval($_REQUEST['draw']);
    //     if (isset($_REQUEST["A2"]))
    //         $A2 = $_REQUEST["A2"];
    //     $records         = array();
    //     $records["data"] = array();
    //     $end             = $iDisplayStart + $iDisplayLength;
    //     $end             = $end > $iTotalRecords ? $iTotalRecords : $end;
    // }
    
    if (isset($request->productActionType ) && $request->productActionType  == "group_action") {
//         $sql = "select pq.quoteid,pq.quotationno,pq.postno,pq.productno,pq.price,pq.currency,pq.uom,pq.quantity,pq.location,
// pq.expdate ,pq.language,pq.timeframe, pq.offercrdate,pq.userid, pq.quationstatus, pq.accdate,  pq.expirydate, pq.rejdate, pq.lastcntdate, pq.isactive,po.ptype,pro.name as productName
// from post_quotation pq left join mst_product pro On pq.productno=pro.productno 
// 		 LEFT JOIN post_advertisment po  ON pq.postno=po.postno where po.pstatus NOT IN ('Accepted','EXPIRED','Canceled','Completed') " . $group . $orderby . " limit $iDisplayStart ,$iDisplayLength ";
    } else {
        
        $sql_result = \App\PostQuotation::whereHas('postAdverstisment',function($query){
		            $query->whereNotIn('pstatus',['Accepted','EXPIRED','Canceled','Completed']);
		        })->orderBy('offercrdate','DESC')->get();
        
    }

    	$record = array();
    	$i = 0;
        foreach ($sql_result as $value) {
        	
            $offerdate = date('F d Y - h:i A',strtotime($value->offercrdate));
			$postno = $value->postno;
            $productno = $value->productno;
			 $qutationtype = $value->type;

			if($qutationtype=="QUOTATION"){
			 
			 $quoteid   = '<a href="currentOfferCounter?postno='.$postno.'&productno='. $productno.'&quotationno='.$value->quotationno.'&quoteid='.$value->quoteid.'" id="quoest">'.$value->quotationno.'</a>';
			 
			 }
			 else{

			 	$quoteid   = '<a href="currentNewofferCounter?postno='.$postno.'&productno='. $productno.'&quotationno='.$value->quotationno.'&quoteid='.$value->quoteid.'" id="quoest">'.$value->quotationno.'</a>';

			 }
			
			$ptype             = ucwords(strtolower($value->ptype));
            $quantity          = $value->quantity;

            $name              = isset($value->product->name)?$value->product->name:'';
            $price             = $value->price;
            $currency          = $value->currency;            
            $timeframe         = $value->timeframe;                                                                       
            $expdate           = $value->expdate;
            $expirydate        = $value->expirydate;
            $quationstatus     = $value->quationstatus .'<a href="quoteDetailView/' . $value->quotationno. '/' . $productno . '/' . $postno . '" style="display:none;" id="quoest"><i class="fa fa-trash-o"></i></a>';
            
            
	    	$checkMaskVal = \App\MaskProname::where('id','1')->get();			
			
			$maskstatus=$checkMaskVal[0]->maskstatus;
			
			if(Auth::user()->roletype != 'AD'){
				if($maskstatus=='1'){
					$productNameshow=$name;
				}else{
					$productNameshow=$productno;
				}
				
			}else{
				$productNameshow=$name;
			}

			$record[$i]['quoteid'] = $quoteid;
            $record[$i]['offerdate'] = $offerdate;
            $record[$i]['ptype'] = $ptype;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['productNameshow'] = $productNameshow;
            $record[$i]['price'] = $price;
            $record[$i]['currency'] = $currency;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;
            $record[$i]['quationstatus'] = $quationstatus;
                
 			$i++;
        }
    
        return \Response::json($record);
    }

    public function quotepostDetailView($postno,$productno,$ptype){

    	$mypoststatus = $this->mypoststatus($postno);

    	$currencies = \App\MstCurrency::orderBy('name')->get();

    	$postDetails = \App\PostAdverstisement::where('postno',$postno)->get();

    	$timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

    	$expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

    	$checkVal = $this->checkPermission();

    	$data = array(
    			'currencies' => $currencies,
    			'postDetails' => $postDetails,
    			'timeframes' => $timeframes,
    			'expdateranges' => $expdateranges,
    			'checkVal' => $checkVal,
    			'mypoststatus' => $mypoststatus
    		);

    	return View('quoteQueue.quotepostDetailView',$data)->with('title','Trading Soft::Quote Post Detail View');
    }

    public function checkPermission(){

        $data = \App\MastRolePermission::select('RoleId','ViewP','AddP','EditP','DeleteP')->where('RoleId',Auth::user()->roleid)->where('PageId','12')->get();
        return $data;
    }

    public function quoteDetailViewFunction(Request $request){
    	if ($_POST['Type'] == "offertypeuser") {
 
		    $userpostId = $request->userpostId;
		    $usertype   = $request->usertype;
		    $userid     = $request->userid;
			$userpost   = $request->userpost;
			$userptype  = $request->userptype;

			$result = \App\PostAdverstisement::where('postno',$userpostId)->get();
			
			foreach ($result as $value) {

				if(empty($value->buyer))
				{
			    	if ($userptype == 'Sell') {
						
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['buyer'=>$userpost]);

						return '1';
					}
				}
				else
				{
				
					if ($userptype == 'Sell') {
					  	
					  	$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['buyer'=>'']);

						return '2';
					}
				}	
			
				if(empty($value->Seller))
				{
				    if ($userptype == 'Buy') {
						
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['seller'=>$userpost]);

						return '1';
					}
				}
				else
				{
				
					if ($userptype == 'Buy') {
						
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['seller'=>'']);

						return '2';
					}
				}
			}
			
		}

		if ($request->Type == "typeuser") {
 
		    $userpostId = $request->userpostId;
		    $usertype   = $request->usertype;
		    $userid     = $request->userid;
			$userpost   = $request->userpost;
				
		    if ($usertype == 'Seller') {
				
				$result = \App\PostAdverstisement::where('postno',$userpostId)->get();

				foreach ($result as $value) {
					
					if(!empty($value->seller))
					{	
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['seller'=>'']);

						return '1';
					}
					else
					{
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['seller'=>$userpost]);

						return '2';
					}		
				}
				
		    }
		    if ($usertype == 'Buyer') {
				
				$result = \App\PostAdverstisement::where('postno',$userpostId)->get();

				foreach ($result as $value) {

					if(!empty($value->buyer))
					{	
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['buyer'=>'']);

						return '1';
					}
					else
					{	
						$statement1 = \App\PostAdverstisement::where('postno',$userpostId)->update(['buyer'=>$userpost]);

						return '2';
					}
				}

		    }
		}




    }

    function mypoststatus($id)
    {
    
        $r1=0;  $r2=0;
        $r3=0;  $r4=0;
        $r5=0;  $r6=0;

        $r1 = \App\QuotationCounter::where('postno',$id)->count();

        $r2 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->count();

        $r3 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->where(function($query){
                $query->where('counetr_status','Accepted')->orWhere('offerCounterStatus','Accepted');
                })->count();

        $r4 = \App\QuotationCounter::where('postno',$id)->where(function($query){
                    $query->where('counetr_status','Accepted')->orWhere('offerCounterStatus','Accepted');
                })->count();
        
        $r5 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->where(        function($query){
                        $query->where('counetr_status','Declined')->orWhere('offerCounterStatus','Declined');
                    })->count();
        
        $r6 = \App\QuotationCounter::where('postno',$id)->where(function($query){
                    $query->where('counetr_status','Declined')->orWhere('offerCounterStatus','Declined');
                })->count();
        
        if($r1 > 0 && $r2>0)
        {
            if($r3>0 || $r4 >0)
            {
            return 'ACCEPTED';
            }
            elseif($r5>0 || $r6 >0)
            {
            return 'DECLINED';
            }
            else
            {
            return 'COUNTERED';
            }
        
        }
        elseif($r1<=0 && $r2>0)
        {   if($r3>0 || $r4 >0)
            {
            return 'ACCEPTED';
            }
            elseif($r5>0 || $r6 >0)
            {
            return 'DECLINED';
            }
            else
            {
            return 'SUBMITTED';
            }
        }
        else
        { 
        return 'NEW';
        }
    }

    public function fetchFunction(Request $request){
    	if ($request->type == 'fetchquotepostdata') {
    		
    		$postno = $request->postno;

    		$result = \App\PostAdverstisement::where('postno',$postno)->get();

    		foreach ($result as $value) {
    			$ptype = ucwords(strtolower(isset($value->ptype)?$value->ptype:''));               
                $productName = mb_convert_encoding(isset($value->product->name)?$value->product->name:'', "HTML-ENTITIES", "utf8");
                $mpm = isset($value->product->mpm)?$value->product->mpm:'';
                $userid = isset($value->userid)?$value->userid:'';
                $postid = isset($value->postid)?$value->postid:'';               
                $pdate = isset($value->pdate)?$value->pdate:'';
                $productno = isset($value->productno)?$value->productno:'';            
                $industry = mb_convert_encoding(isset($value->industry)?$value->industry:'', "HTML-ENTITIES", "utf8");
                $category = mb_convert_encoding(isset($value->category)?$value->category:'', "HTML-ENTITIES", "utf8");
                $subcategory = mb_convert_encoding(isset($value->subcategory)?$value->subcategory:'', "HTML-ENTITIES", "utf8");
                $brand = mb_convert_encoding(isset($value->brand)?$value->brand:'', "HTML-ENTITIES", "utf8");              
                $currency = isset($value->currency)?$value->currency:'';
                $uom = isset($value->uom)?$value->uom:'';                  
                $quantity = isset($value->quantity)?$value->quantity:'';
                $location = isset($value->location)?$value->location:'';             
                $expdate = isset($value->expdate)?$value->expdate:'';
                $expirydate = isset($value->expirydate)?$value->expirydate:'';           
                $customerrefno = isset($value->customerrefno)?$value->customerrefno:'';
                $pakaging = isset($value->pakaging)?$value->pakaging:'';             
                $language = isset($value->language)?$value->language:'';
                $timeframe = isset($value->timeframe)?$value->timeframe:'';            
                $country = isset($value->country)?$value->country:'';
                $pstatus = isset($value->pstatus)?$value->pstatus:'';              
                $targetprice = isset($value->targetprice)?$value->targetprice:'';
                $postno = isset($value->postno)?$value->postno:'';               
                $shipingcondition = isset($value->shipingcondition)?$value->shipingcondition:'';
                $buyer = isset($value->buyer)?$value->buyer:'';                
                $seller = isset($value->seller)?$value->seller:'';
    		}

    		$arr = array(
                'ptype' => $ptype,                  
                'productName' => $productName,
                'mpm' => $mpm,                      
                'userid' => $userid,
                'postid' => $postid,                
                'pdate' => $pdate,
                'productno' => $productno,          
                'industry' => $industry,
                'category' => $category,            
                'subcategory' => $subcategory,
                'brand' => $brand,                  
                'currency' => $currency,
                'uom' => $uom,                      
                'quantity' => $quantity,
                'location' => $location,            
                'expdate' => $expdate,
                'expirydate' => $expirydate,        
                'customerrefno' => $customerrefno,
                'pakaging' => $pakaging,            
                'language' => $language,
                'timeframe' => $timeframe,          
                'country' => $country,
                'pstatus' => $pstatus,              
                'targetprice' => $targetprice,
                'postno' => $postno,                
                'shipingcondition' => $shipingcondition,
                'buyer' => $buyer,                  
                'seller' => $seller
            );
    		
    		return $arr;
    	}
	
    	if ($request->type == 'fetchcurrentoffers') {
		    
		    $postno = $request->postno;
		
		    $json   = '';
		    
		    $key = "ChangedSettings";
		    
		    $result = \App\PostQuotation::where('postno',$postno)->where('userid','!=',Auth::user()->userid)->where('type','OFFER')->orderBy('offercrdate','DESC')->get();

		    $row_count = count($result);
			
			$result1 = \App\PostAdverstisement::where('postno',$postno)->whereIn('pstatus',['Accepted','Declined'])->get();

		    $row_count1 = count($result1);
			$record = array();		
		    if ($row_count != 0) {
			    
			    if($row_count1 ==0){
                    
        			$record = array();            
                    $i = 0;
                    foreach ($result as $value) {
                    	
                    	$record[$i]['quotationno'] = '<a href="current_newoffer_counter/'.$postno.'/'.$value->productno.'/'.$value->quotationno.'/'.$value->quoteid.'">'.$value->quotationno.'</a>&nbsp;&nbsp;<a href="javascript:offeradduser(\'' . $value->postno . '\',\'Seller\',\''.$value->userid.'\');" class="userplus"><i class="fa fa-user-plus"></i></a>';

                    	$record[$i]['dateOffered'] = $value->offercrdate .' EST';
                    	$record[$i]['user'] = $value->userid;
                    	$record[$i]['price'] = $value->price;
                    	$record[$i]['curr'] = $value->currency;
                    	$record[$i]['uom'] = $value->uom;
                    	$record[$i]['quantity'] = $value->quantity;
                    	$record[$i]['timeframe'] = $value->timeframe;
                    	$record[$i]['expdaterange'] = $value->expdate . '<input type="hidden" name="qutationstatus" id="qutationstatus" value="' . $value->quationstatus . '">';

                    	$i++;
                    }
					           
				}
				
		    }
		     
		    return \Response::json($record);
		}

		if ($request->typeMode == 'checkQuotation') {
			
			$post_id  = $request->post_id;
			$productno  = $request->productno;
			
			$sth = \App\PostQuotation::where('postno',$post_id)->where('productno',$productno)->whereIn('quationstatus',['Submitted','Counter'])->where('type','QUOTATION')->get();
			$arr = array();
			foreach ($sth as $value) {
				
				$id=$value->quoteid;
				$userid=$value->userid;
				$arr = array('id'=>$id,	'userid'=>$userid);
			}
			 return $arr;
	    
	    }

    }

    public function offerHistory(Request $request){

    	$sql = \App\PostQuotation::where('postno',$request->post_id)->where('type','QUOTATION')->orderBy('quoteid','DESC')->get();

    	$record = array();
    	$i = 0;
    	foreach ($sql as $value) {
			$sdate = date('Y-m-d H:i A',strtotime($value->offercrdate)) . ' EST';
			$quoteid        = $value->quoteid;
            $quotationno    = $value->quotationno;
            $price          = $value->price;
			$userid         = $value->userid;
            $currency       = $value->currency;
            $uom            = $value->uom;
            $quantity       = $value->quantity.'<a href="current_offer_counter/'.$value->postno.'/'.$value->productno.'/'.$value->quotationno.'/'.$value->quoteid .'" style="display:none;" id="quoest"></a>';
            $timeframe      = $value->timeframe;
            $expdate        = $value->expdate;
            $language       = $value->language;
            $detail         = $value->details;
			$declinemsg     = $value->declinemsg;
			$acceptmsg     	= $value->acceptmsg;
            $isactive       = $value->isactive;
            $counetr_status = $value->counetr_status;
            
            $countro       = \App\QuotationCounter::where('quotationno',$quotationno)->count();

            /**********************Counter Offer*******************/

            $countersql = \App\QuotationCounter::where('quotationno',$quotationno)->where('postno',$request->post_id)->orderBy('counterid','DESC')->get();
            $j = 0;
            foreach ($countersql as $values) {
            	$sdate1 = date('Y-m-d H:i A',strtotime($values->counter_date)) . ' EST';
            	$quoteid1 = $values->quoteid;
                $coundID = $values->coundID;
                $quotationno1 = $values->quotationno;
                $price1 = $values->counter_price;
				$counter_by1 = $values->counter_by;
                $currency1 = $values->currency;
                $uom1 = $values->uom;
                $quantity1 = $values->counter_quantity;
                $timeframe1 = $values->counter_timeframe;
                
                $expdates1 = $values->counter_expdate.'<a href="current_offer_counter/'.$values->postno.'/'.$values->productno.'/'.$values->quotationno.'/'.$values->quoteid .'" style="display:none;" id="quoest"></a>';
                
                $detailstatus   	= $values->counter_msg;
                $counetr_status 	= $values->counetr_status;
                if($detailstatus=='Countered By User')
				{
                	$detail1="Counter Offer Received";
				}
				else
				{
				 	$detail1=$detailstatus;
				}

				$record[$j]['quotationno1'] = $quotationno1;
				$record[$j]['sdate1'] = $sdate1;
				$record[$j]['price1'] = $price1;
				$record[$j]['currency1'] = $currency1;
				$record[$j]['uom1'] = $uom1;
				$record[$j]['quantity1'] = $quantity1;
				$Record[$j]['timeframe1'] = $timeframe1;
				$record[$j]['expdates1'] = $expdates1;
				$record[$j]['detail1'] = $detail1;
				$record[$i]['counter_by1'] = $counter_by1;
				
				$j++;	
            }

            if(!empty($acceptmsg)){
            	$record[$i]['quotationno'] = $quotationno;
            	$record[$i]['sdate'] = $sdate;
            	$record[$i]['price'] = $price;
            	$record[$i]['currency'] = $currency;
            	$record[$i]['uom'] = $uom;
            	$record[$i]['quantity'] = $quantity;
            	$record[$i]['timeframe'] = $timeframe;
            	$record[$i]['expdate'] = $expdate;
            	$record[$i]['acceptmsg'] = $acceptmsg;
            	$record[$i]['userid'] = $userid;	
            }
            if(!empty($declinemsg)){
            	$record[$i]['quotationno'] = $quotationno;
            	$record[$i]['sdate'] = $sdate;
            	$record[$i]['price'] = $price;
            	$record[$i]['currency'] = $currency;
            	$record[$i]['uom'] = $uom;
            	$record[$i]['quantity'] = $quantity;
            	$record[$i]['timeframe'] = $timeframe;
            	$record[$i]['expdate'] = $expdate;
            	$record[$i]['declinemsg'] = $declinemsg;
            	$record[$i]['userid'] = $userid;		
            }

            $record[$i]['quotationno'] = $quotationno;
        	$record[$i]['sdate'] = $sdate;
        	$record[$i]['price'] = $price;
        	$record[$i]['currency'] = $currency;
        	$record[$i]['uom'] = $uom;
        	$record[$i]['quantity'] = $quantity;
        	$record[$i]['timeframe'] = $timeframe;
        	$record[$i]['expdate'] = $expdate;
        	$record[$i]['detail'] = $detail;
        	$record[$i]['userid'] = $usereid;

			$i++;
    	}
    	return \Response::json($record);
    }

    public function activeSellPosting(Request $request){

    	$sql = \App\PostAdverstisement::where('ptype','SELL')->whereHas('product',function($query) use ($request){
			            $query->where('productno',$request->prodno);
			        })->get();
    	$record = array();
    	$i = 0;
    	foreach ($sql as $value) {
			
			$sdate = date('F d Y - h:i A',strtotime($value->pdate));
			$pn = $value->postno;
			$postuserid = $value->userid;
			$postid = '<a href="posting-detail-view/'.$pn.'" style="display:none;" id="postrow">rama</a>';
			if(($request->ptype=='BUY') || ($request->ptype=='Buy')){
		         $postno = $pn . '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:adduser(\''.$pn . '\',\'Seller\',\''.$postuserid.'\')" class="userplus"><i class="fa fa-user-plus"></i></a>';
			}
			else
			{
		 		$postno = $pn . '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:adduser(\''.$pn . '\',\'Seller\',\''.$postuserid.'\')" class="userplus"></a>';
		 
			}

			$targetprice = $value->targetprice;
            $currency = $value->currency;
            $uom = $value->uom;
			$productno = $value->productno;
			
            $ptype = $postid.$value->ptype;
            $quantity = $value->quantity;
            $expdate = $value->expdate;
            $timeframe = $value->timeframe;
            $deleteid = '<a  href="javascript:deleteme(\'' . $pn . '\')"><i class="fa fa-trash-o"></i></a>';

            $record[$i]['sdate'] = $sdate;
            $record[$i]['productno'] = $productno;
            $record[$i]['postno'] = $postno;
            $record[$i]['ptype'] = $ptype;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['targetprice'] = $targetprice;
            $record[$i]['currency'] = $currency;
            $record[$i]['uom'] = $uom;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;

            $i++;
    	}

    	return \Response::json($record);
    }

    public function activeBuyPosting(Request $request){
    	
    	$sql = \App\PostAdverstisement::where('ptype','BUY')->whereHas('product',function($query) use ($request){
			            $query->where('productno',$request->prodno);
			        })->get();

    	$record = array();
    	$i = 0;
    	foreach ($sql as $value) {
    		
    		$sdate  = date('F d Y - h:i A',strtotime($value->pdate));
            $pn     = $value->postno;
			$postuserid = $value->postuserid;
            $postid = '<a href="posting-detail-view/' . $pn . '" style="display:none;" id="postrow">rama</a>';
            if(($request->ptype=='SELL') || ($request->ptype=='Sell')){
			
			// $myFile = "ramafile.txt";
			// $fh = fopen($myFile, 'w') or die("can't open file");
			// fwrite($fh, $orgiptype);
			// fclose($fh);
            
	            $postno  = $value->postno.' &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:adduser(\'' . $pn . '\',\'Buyer\',\''.$postuserid.'\')" class="userplus"><i class="fa fa-user-plus"></i></a>';
			}
			else
			{
				$postno  = $value->postno.' &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:adduser(\'' . $pn . '\',\'Buyer\',\''.$postuserid.'\')" class="userplus"></a>';

			}
            $targetprice = $value->targetprice;
            $currency    = $value->currency;
            $uom         = $value->uom;
            $productno   = $value->productno;
            
            $ptype     = $postid .$value->ptype;
            $quantity  = $value->quantity;
            $expdate   = $value->expdate;
            $timeframe = $value->timeframe;
            $deleteid  = '<a  href="javascript:deleteme(\'' . $pn . '\')"><i class="fa fa-trash-o"></i></a>';

            $record[$i]['sdate'] = $sdate;
            $record[$i]['productno'] = $productno;
            $record[$i]['postno'] = $postno;
            $record[$i]['ptype'] = $ptype;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['targetprice'] = $targetprice;
            $record[$i]['currency'] = $currency;
            $record[$i]['uom'] = $uom;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;
            
            $i++;
    	}

    	return \Response::json($record);
    }

    public function historicalTransaction(Request $request){

    	$sql = \App\TMAcceptedOffers::where('status','Completed')->where('productno',$request->prodno)->get();

    	$record = array();
    	$i = 0;
    	foreach ($sql as $value) {
    		
            $sdate = date('F d Y - h:i A',strtotime($value->acc_date));
            $pn = $value->postno;
            $postid = '<a href="posting-detail-view/' . $pn . '" style="display:none;">rama</a>';

            $postno = $pn . $postid;
            $targetprice = $value->acc_price;
            $currency = $value->adverstisement->currency;
            $uom = $value->adverstisement->uom;
			$productno = $value->productno;
            $ptype = $value->adverstisement->ptype;
            $quantity = $value->acc_quantity;
            $expdate = $value->acc_expdate;
            $timeframe = $value->acc_timeframe;
            $deleteid = '<a  href="javascript:deleteme(\'' . $pn . '\')"><i class="fa fa-trash-o"></i></a>';
           	$status1=$value->status;

           	$record[$i]['sdate'] = $sdate;
			$record[$i]['productno'] = $productno;
			$record[$i]['postno'] = $postno;
			$record[$i]['ptype'] = $ptype;
			$record[$i]['quantity'] = $quantity;
            $record[$i]['targetprice'] = $targetprice;
            $record[$i]['currency'] = $currency;
            $record[$i]['uom'] = $uom;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;
			$record[$i]['status1'] = $status1;

           	$i++;
    	}

    	return \Response::json($record);
    }

    public function addofferbyqf(Request $request){

		if ($_POST['type'] == 'addofferbyqf') {
   
		    if (empty($request->datepicker)) {
		        $datepicker = "";
		    } else {
		        $formdatepicker = $request->datepicker;
		        $datepicker     = date("Y-m-d", strtotime($formdatepicker));
		    }
		    
		    $quotationno = $this->randStringNumeric222(6) . "-" . $this->randStringAlpha2();
		    
		    $postno         = $request->postno;
		    $pro_id         = $request->productno;
		    $price          = $request->price;
		    $offerCurrency  = $request->currency;
		    $offerUoM       = $request->uom;
		    $offerqty       = $request->quantity;
		    $offerlocation  = $request->location;
		    $offerdaterange = $request->expdate;
		    //$datepicker = $request->datepicker'];
		    $offerlanguage  = $request->language;
		    $offertimeframe = $request->timeframe;
		    $puserid        = $request->puserid;
		    $detail         = "New Offer By Company";
		    $offercrdate    = date('Y-m-d H:i:sa');
		    $userid         = Auth::user()->userid;
		    $quationstatus  = "Submitted";
		    $type           = "QUOTATION";
			
		    $statement1 = new \App\PostQuotation;

		    $statement1->quotationno = $quotationno;
		    $statement1->postno = $postno;
		    $statement1->productno = $pro_id;
		    $statement1->price = $price;
		    $statement1->currency = $offerCurrency;
		    $statement1->uom = $offerUoM;
		    $statement1->quantity = $offerqty;
		    $statement1->location = $offerlocation;
		    $statement1->expdate = $offerdaterange;
		    $statement1->expirydate = $datepicker;
		    $statement1->language = $offerlanguage;
		    $statement1->timeframe = $offertimeframe;
		    $statement1->details = $detail;
		    $statement1->offercrdate = $offercrdate;
		    $statement1->userid = $userid;
		    $statement1->quationstatus = $quationstatus;
		    $statement1->postuserid = $puserid;
		    $statement1->type = $type;

		    $statement1->save();

		    $statement3 = \App\PostAdverstisement::where('postno',$postno)->update(['pstatus'=>$quationstatus]);
			
			$statement2 = new \App\UserActivity;

			$statement2->userid = Auth::user()->userid;
            $statement2->ipaddress = $request->ip();
            $statement2->sessionid = \Session::get('_token');
            $statement2->actiondate = date('Y-m-d H:i:sa');
            $statement2->actiontime = date('Y-m-d H:i:sa');
            $statement2->actionname = 'Submitted Quote';

            $statement2->save();
			
		    return '1';
		}    	
    }

	function randStringNumeric222($len)
    {
        $str   = "";
        $chars = array_merge(range(0, 9));
        for ($i = 0; $i < $len; $i++) {
            //reseed mt_rand
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            //append to string
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
    
    function randStringAlpha2($len = 1)
    {
        $str   = "";
        $chars = array_merge(range('A', 'Z'));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }

    public function currentNewofferCounter(Request $request){
    	$mypoststatus = $this->mypoststatus($request->input('postno'));

    	$currencies = \App\MstCurrency::orderBy('name')->get();

    	$postDetails = \App\PostAdverstisement::where('postno',$request->input('postno'))->get();

    	$timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

    	$expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

    	$checkVal = $this->checkPermission();

    	$data = array(
			'currencies' => $currencies,
			'postDetails' => $postDetails,
			'timeframes' => $timeframes,
			'expdateranges' => $expdateranges,
			'checkVal' => $checkVal,
			'mypoststatus' => $mypoststatus
		);

    	return View('quoteQueue.currentNewofferCounter',$data)->with('title','Ziki Trade');
    }
}
