<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\userLogon;
use App\UserRole;
use App\UserActivity;
use App\Timezone;
use App\Industry;
use App\Category;
use App\SubCategory;
use App\Brand;
use App\MastRolePermission;
use App\PostAdverstisement;
use DB;
use Crypt;
use View;
use Session;
use App\MastPage;
use Illuminate\Support\Facades\Redirect;
use DateTimeZone;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function testingUser(Request $request){
      die('llll');
    }

    public function index()
    {   
        return View::make('user.index')->with('title','Users');

    }

    public function userdetail_ajax(){

        $users = User::with(['userLogon'=>function($query){
            $query->orderBy('logonid','desc')->get();
        }])->get();

        $checkVal = $this->checkPermission();
        $action = '';
        $i = 0;
        foreach ($users as $value) {
            $record[$i]['userid'] = $value->userid;
            $record[$i]['userrole'] = $value->userRole->rolename;
            $record[$i]['creationdate'] = date('M d Y', strtotime($value->creationdate) );
            $record[$i]['lastlogin'] = isset($value->userLogon[0]) ? date('F d Y - h:i A',strtotime(str_ireplace('pm','',str_ireplace('am','',$value->userLogon[0]->starttime) ) ) ) .' EST' : 'NA';
            $record[$i]['status'] = ($value->isactive == 1) ? "Active" : "Inactive";
            


            if ($value->roletype == 'AM'){
                if ( Auth::user()->roletype =='AD'){
                    $action = '<a href="edit/'. Crypt::encrypt($value->userid) .'">EDIT</a>&nbsp;|&nbsp;
                      <a href="activity/'.Crypt::encrypt($value->userid).'">ACTIVITY</a>&nbsp;|&nbsp;
                      <a href="'.url("user/reports").'?q='.$value->userid.'&view_type=Postings&clauseby=post_advertisment_userid&reportbyad=reportbyad">REPORTS</a>&nbsp;|&nbsp;
                      <a href="customers/'.Crypt::encrypt($value->userid).'">CUSTOMERS</a>&nbsp;|&nbsp;
                      <a href="posting/'.Crypt::encrypt($value->userid).'">POSTINGS</a>&nbsp;|&nbsp;
                      <a href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';
                }
                else{

                  if($checkVal[0]['EditP']=='1'){
                    $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp';
                  }
                  
                  $action .= '<a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;
                  <a href="javascript:;">REPORTS</a>&nbsp;|&nbsp;
                  <a href="customers/'.Crypt::encrypt($value->userid).'">CUSTOMERS</a>&nbsp;|&nbsp;
                  <a href="posting/'.Crypt::encrypt($value->userid).'">POSTINGS</a>&nbsp;|&nbsp;';
                  
                  if($checkVal[0]['DeleteP']=='1'){
                    $action .= '<a href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';
                  }

                }
            }
            
            if ($value->roletype == 'AD'){

                if ( Auth::user()->roletype=='AD'){
                    $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;
                    <a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;';
                }
                else{

                  if($checkVal[0]['EditP']=='1'){

                    $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;';

                  }

                    $action .= '<a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;';

                }

            }


            if ($value->roletype == 'SA'){

                if ( Auth::user()->roletype == 'AD'){
                    
                    $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;
                      <a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;
                      <a href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';
                }
                else{

                    if($checkVal[0]['EditP']=='1'){

                      $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;';

                    }

                      $action .= '<a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;';

                    if($checkVal[0]['DeleteP']=='1'){

                      $action .= '<a  href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';
                    }
                }

            }


            if ($value->roletype == 'QF'){

                if ( Auth::user()->roletype=='AD'){

                  $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;
                      <a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;
                      <a href="'.url("user/reports").'?q='.$value->userid.'&view_type=Postings&clauseby=post_advertisment_userid&reportbyad=reportbyad">REPORTS</a>&nbsp;|&nbsp;
                      <a href="'.url("quoteQueue/quoteandpost").'">QUOTES</a>&nbsp;|&nbsp;
                      <a href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';

                }
                else{

                    if($checkVal[0]['EditP']=='1'){

                      $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;';

                    }

                      $action .= '<a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;
                              <a href="'.url("report/posting_pie_chart").'">REPORTS</a>&nbsp;|&nbsp;
                              <a href="'.url("quoteQueue/quoteandpost").'">QUOTES</a>&nbsp;|&nbsp;';
                    
                    if($checkVal[0]['DeleteP']=='1'){

                      $action .= '<a  href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';

                    }

                }

            }


            if ($value->roletype == 'TM'){

                if ( Auth::user()->roletype=='AD'){

                    $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;
                      <a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;
                      <a href="'.url("user/reports").'?q='.$value->userid.'&view_type=Postings&clauseby=post_advertisment_userid&reportbyad=reportbyad">REPORTS</a>&nbsp;|&nbsp;
                      <a href="order/acceptedoffers">ACCEPTED OFFERS</a>&nbsp;|&nbsp;
                      <a  href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';
                }
                else{

                    if($checkVal[0]['EditP']=='1'){

                      $action = '<a href="edit/'.Crypt::encrypt($value->userid).'">EDIT</a>&nbsp;|&nbsp;';

                    }

                    $action .= '<a href="activity/'.Crypt::encrypt($value->userid).'">Activity</a>&nbsp;|&nbsp;
                      <a href="javascript:void(0);">REPORTS</a>&nbsp;|&nbsp;
                      <a href="order/acceptedoffers">ACCEPTED OFFERS</a>&nbsp;|&nbsp;';
                    
                    if($checkVal[0]['DeleteP']=='1'){

                      $action .= '<a href="delete/'.Crypt::encrypt($value->userid).'">DELETE</a>';

                    }

                }

            }



            $record[$i]['action'] = $action;
        
            $i++;
        }

        return \Response::json($record);        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $userRole = UserRole::select('rolename','roleid')->orderBy('rolename')->get();

        $sysgenerateid = $this->sysgenerateid();

        $timezone = Timezone::select('name','timezonevalue')->where('isactive','1')->orderby('timezonevalue')->get();

        $data = array(
            'sysgenerateid'=> $sysgenerateid,
            'userrole'     => $userRole,
            'timezone'     => $timezone
            );

        return View('user.create',$data)->with('title','Create User');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        if ( $request->input('adduser') != null ) {
            // echo "<pre>"; print_r($request->input());die;            
            $indtemp    = '';
            $industries = $request->input('selectedindustry');
            if(count($industries) > 0){
                while (list($key, $val) = each($industries)) {
                    if (empty($indtemp)) {
                        $indtemp = $val;
                    } else {
                        $indtemp = $indtemp . ',' . $val;
                    }
                }
            }


            $catetemp   = '';
            $categories = $request->input('selectcategory');
            if(count($categories) > 0){
                while (list($key, $val) = each($categories)) {
                    if (empty($catetemp)) {
                        $catetemp = $val;
                    } else {
                        $catetemp = $catetemp . ',' . $val;
                    }
                }
            }

            $subcatetemp   = '';
            $subcategories = $request->input('selectsubcat');
            if(count($subcategories) > 0){
                while (list($key, $val) = each($subcategories)) {
                    if (empty($subcatetemp)) {
                        $subcatetemp = $val;
                    } else {
                        $subcatetemp = $subcatetemp . ',' . $val;
                    }
                }
            }

            $brandtemp = '';
            $brands = $request->input('selectbrands');
            if(count($brands) > 0){
                while (list($key, $val) = each($brands)) {
                    if (empty($brandtemp)) {
                        $brandtemp = $val;
                    } else {
                        $brandtemp = $brandtemp . ',' . $val;
                    }
                }
            }
            
            $rolecode = UserRole::select('code')->where('roleid',$request->input('role'))->get();
            
            $user = new User;

            $user->roleid = $request->input('role');
            $user->roletype = $rolecode[0]->code;
            $user->userid = $request->input('userid');
            $user->password = bcrypt($request->input('pass'));
            $user->authorizedpass = bcrypt($request->input('authpass'));
            $user->timezoneid = $request->input('timezone');
            $user->isactive = $request->input('status');
            
            date_default_timezone_set('EST');
            
            $user->creationdate = date('Y-m-d H:i:sa');
            $user->industry = $indtemp;
            $user->category = $catetemp;
            $user->subcategory = $subcatetemp;
            $user->brand = $brandtemp;
            $user->save();


            $useraction = new UserActivity;

            $useraction->userid = $request->input('userid');
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = Session::get('_token');
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Added User';

            $useraction->save();
            if( Auth::user()->roleid == '1' ){
              return redirect('create?add=true');
            }
            else{
              return redirect('userdetails');  
            }
            

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {      

      $user = User::find(Crypt::decrypt($id));
      $usercat = \App\Category::where('isactive','1')->whereIn('industryid',explode(',',$user->industry))->get();

      $usersubcat = \App\SubCategory::where('isactive','1')->whereIn('catid',explode(',', $user->category))->get();

      $userbrand = \App\Brand::where('isactive','1')->whereIn('industryid',explode(',',$user->industry))->get();

      $userRole = UserRole::select('rolename','roleid')->get();

      $timezone = Timezone::select('name','timezonevalue')->where('isactive','1')->orderby('timezonevalue')->get();


      $roletype = Auth::user()->roletype;
      $userid   = Auth::user()->userid;
      $status   = 1;
      $iid      = explode(',', \App\User::find($userid)->industry);

      if ($roletype == 'AM') {

          if (empty($iid) == false) {
              
             $result = \App\Industry::where('isactive',$status)->whereIn('industryid',$iid)->orderBy('name')->get();

              
          } else if (empty($iid) == true) {
             
              $result = \App\Industry::where('isactive',$status)->orderBy('name')->get();

          }
      }
      if ($roletype == 'AD') {
          
          $result = \App\Industry::where('isactive',$status)->orderBy('name')->get();

      }
      if ($roletype == 'SA') {
          
          $result = \App\Industry::where('isactive',$status)->orderBy('name')->get();

      }

      $data = array(
            'user'         => $user,
            'userrole'     => $userRole,
            'timezone'     => $timezone,
            'result'       => $result,
            'usercat'      => $usercat,
            'usersubcat'   => $usersubcat,
            'userbrand'    => $userbrand
            );

      return View::make('user.edit',$data)->with('title','Edit User');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ( $request->input('adduser') != null ) {
            // echo "<pre>"; print_r($request->input());die;            
            $indtemp = $catetemp = $subcatetemp = '';
            $industries = $request->input('selectedindustry');
            if(count($industries) > 0){
                while (list($key, $val) = each($industries)) {
                    if (empty($indtemp)) {
                        $indtemp = $val;
                    } else {
                        $indtemp = $indtemp . ',' . $val;
                    }
                }
            }


            $categories = $request->input('selectcategory');
            if(count($categories) > 0){
                while (list($key, $val) = each($categories)) {
                    if (empty($catetemp)) {
                        $catetemp = $val;
                    } else {
                        $catetemp = $catetemp . ',' . $val;
                    }
                }
            }

            $subcategories = $request->input('selectsubcat');
            if(count($subcategories) > 0){
                while (list($key, $val) = each($subcategories)) {
                    if (empty($subcatetemp)) {
                        $subcatetemp = $val;
                    } else {
                        $subcatetemp = $subcatetemp . ',' . $val;
                    }
                }
            }

            $brandtemp = '';
            $brands = $request->input('selectbrands');
            if(count($brands) > 0){
                while (list($key, $val) = each($brands)) {
                    if (empty($brandtemp)) {
                        $brandtemp = $val;
                    } else {
                        $brandtemp = $brandtemp . ',' . $val;
                    }
                }
            }
            
            $rolecode = UserRole::select('code')->where('roleid',$request->input('role'))->get();
            
            $user = User::find($request->input('u_id'));

            $user->roleid = $request->input('role');
            $user->roletype = $rolecode[0]->code;
            $user->password = bcrypt($request->input('pass'));
            $user->authorizedpass = bcrypt($request->input('authpass'));
            $user->timezoneid = $request->input('timezone');
            $user->isactive = $request->input('status');
            
            date_default_timezone_set('EST');
            
            $user->creationdate = date('Y-m-d H:i:sa');
            $user->industry = $indtemp;
            $user->category = $catetemp;
            $user->subcategory = $subcatetemp;
            $user->brand = $brandtemp;
            $user->save();


            $useraction = new UserActivity;

            $useraction->userid = $request->input('userid');
            $useraction->ipaddress = \Request::ip();
            $useraction->sessionid = Session::get('_token');
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Added User';

            $useraction->save();
            return redirect('userdetails');
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null){

        $userid = Crypt::decrypt($id);
        
        $post_advertisement = PostAdverstisement::where('userid',$userid)->count();
        
        if( $post_advertisement > 0 ){

            Session::flash('alert-danger', 'Cannot delete User, active data exists.');
            
            return Redirect::to(url('userdetails'));
        
        }
        else{

            $user = User::find($userid);
            $user->delete();

            $insertAction = new UserActivity;
            $insertAction->userid = $userid;
            $insertAction->ipaddress = \Request::ip();
            $insertAction->sessionid = Session::get('_token');
            date_default_timezone_set('EST');
  
            $insertAction->actiondate = date('Y-m-d H:i:sa');
            $insertAction->actiontime = date('Y-m-d H:i:sa');
            $insertAction->actionname = 'Deleted User';
            $insertAction->save();

            Session::flash('alert-danger', 'Related Record Delete.');
            
            return Redirect::to(url('userdetails'));

        }

    }

    public function activity($id = null){
            
        return View('user.activity')->with('title','Users Activity');        
    }
    
    public function activity_ajax( $id = null){
        
        if($id != null){
            $user_activity = UserActivity::where('userid',Crypt::decrypt($id))->get();
        }
        else{
            $user_activity = UserActivity::all();    
        }
        $i = 0;
        foreach ($user_activity as $key => $value) {
            $record[$i]['userid'] = $value->userid;
            $record[$i]['lastlogin'] = $value->actiontime;
            $record[$i]['ipaddress'] = $value->ipaddress;
            $record[$i]['activity'] = $value->actionname;    
        $i++;
        }
        return \Response::json($record);

    }


    public function exportActivity($id = null){
        if($id != null){
            $users = UserActivity::select('userid', 'actiontime', 'ipaddress', 'actionname')->where('userid',Crypt::decrypt($id))->get();
        }
        else{
            $users = UserActivity::select('userid', 'actiontime', 'ipaddress', 'actionname')->get();    
        }
        

        header('Content-Disposition: attachment; filename="export.csv"');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary\n");

        \Excel::create('users', function($excel) use($users) {
            $excel->sheet('Sheet 1', function($sheet) use($users) {
                $sheet->fromArray($users);
            });
        })->export('csv');
    }

    public function randStringAlpha($len = 4){
        $str   = "";
        $chars = array_merge(range('A', 'Z'));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
    public function randStringNumeric($len = 4){
        $str   = "";
        $chars = array_merge(range(0, 9));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
    public function sysgenerateid(){
        
        $userid = $this->randStringAlpha() . "-" . $this->randStringNumeric();
        return $userid;
    }

    public function checkAuthPass(Request $request){
        $authpass_input = $request->input('authpass');
        $authpass = User::select('authorizedpass')->where('userid',Auth::user()->userid)->get();
       
        if( \Hash::check($authpass_input,$authpass[0]->authorizedpass) ){
            echo 'success';
        }else{
            echo 'failure';
        }
    }

    public function checkPermission(){

        $data = MastRolePermission::select('RoleId','ViewP','AddP','EditP','DeleteP')->where('RoleId',Auth::user()->roleid)->where('PageId','42')->get();
        return $data;
    }

    public function multiselect(Request $request){

        switch ($request->actionmode) {
          case 'selectindustry':

              $industry = array(
                  $request->ind_id
              );

              $ind = explode(',', $request->ind_id);

              $userid      = Auth::user()->userid;
              $cids         = \App\User::where('userid',$userid)->lists('category')->toArray();
              $bids     = \App\User::where('userid',$userid)->lists('brand')->toArray();
              
              if( !empty($cids[0]) ){
                $cid = explode(',', $cids[0]);
              }
              else{
                $cid = array();
              }
              
              if( !empty($bids[0]) ){
                $bid = explode(',', $bids[0]);
              }
              else{
                $bid = array();
              }

              $status = 1;
              $json = array();
              if (empty($cid)) {

                $stmt = \App\Category::where('isactive',$status)->whereIn('industryid',$ind)->orderBy('name')->get();
                
                foreach ($stmt as $result) {
                  $catid = $result->catid;
                  $industryid = $result->industryid;
                  $isactive = $result->isactive;
                  $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                  $json['itemData'][] = array('catid'=>$catid,  'name'=>$name,
                                'industryid'=>$industryid,  'isactive'=>$isactive);
                }

              }
              else
              {
                
                $stmt = \App\Category::where('isactive',$status)->whereIn('industryid',$ind)->whereIn('catid',$cid)->orderBy('name')->get();

                foreach ($stmt as $result) {
                  $catid = $result->catid;
                  $industryid = $result->industryid;
                  $isactive = $result->isactive;
                  $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                  $json['itemData'][] = array('catid'=>$catid,  'name'=>$name,
                                'industryid'=>$industryid,  'isactive'=>$isactive);
                }

              }
              //select brands
              if (empty($bid)) {
                
                $stmt2 = \App\Brand::where('isactive',$status)->whereIn('industryid',$ind)->orderBy('name')->get();

                foreach ($stmt2 as $result2) {

                  $brandid = $result2->brandid;
                  
                  $industryid = $result2->industryid;
                  
                  $isactive = $result2->isactive;
                  
                  $name = mb_convert_encoding($result2->name, "HTML-ENTITIES", "ISO-8859-1");
                  
                  $json['brandData'][] =array('brandid'=>$brandid,  'name'=>$name,
                                'industryid'=>$industryid,  'isactive'=>$isactive);
                }

                return \Response::json($json);

              }
              else
              {

                $stmt2 = \App\Brand::where('isactive',$status)->whereIn('industryid',$ind)->whereIn('brandid',$bid)->orderBy('name')->get();
                // echo $stmt2;die;
                foreach ($stmt2 as $result2) {
                  $brandid = $result2->brandid;
                  $industryid = $result2->industryid;
                  $isactive = $result2->isactive;
                  $name = mb_convert_encoding($result2->name, "HTML-ENTITIES", "ISO-8859-1");
                  $json['brandData'][] = array('brandid'=>$brandid,  'name'=>$name,
                                'industryid'=>$industryid,  'isactive'=>$isactive);
                }

                return \Response::json($json);
               
              }

            break;

          case 'selectsubcategory':
              $industry = array(
                  $request->cat_id
              );
              
              $ind = explode(',', $request->cat_id);

              $status = 1;
              
              $userid   = Auth::user()->userid;
              
              $subcatids = \App\User::where('userid',$userid)->lists('subcategory')->toArray();

              if( !empty($subcatids[0]) ){
                $subcatid = explode(',', $subcatids[0]);
              }
              else{
                $subcatid = array();
              }

              $roletype = Auth::user()->roletype;
              $json = array();
              if (empty($subcatid)) {
                            
                $stmt2 = \App\SubCategory::where('isactive',$status)->whereIn('catid',$ind)->orderBy('name')->get();

                foreach ($stmt2 as $result2) {
                  
                  $json['itemData'][] = $result2;
                }

                return \Response::json($json);

                
              }
              else
              {

                $stmt2 = \App\SubCategory::where('isactive',$status)->whereIn('catid',$ind)->whereIn('subcatid',$subcatid)->orderBy('name')->get();

                foreach ($stmt2 as $result2) {
                  
                  $json['itemData'][] = $result2;
                }

                return \Response::json($json);
              }
            break;
          
          default:
            # code...
            break;
        }
    }

    public function userreports(Request $request){
        $reuserid = $request->reuserid;
        $showlaue = $request->showlaue;
        $fromdate1 = $request->fromdate;
        $todate1 = $request->todate;
        $fromdate2 = strtotime($fromdate1);
        $fromdate = date("Y-m-d", $fromdate2);


        $todate2 = strtotime($todate1);
        $todate = date("Y-m-d", $todate2);
        $pdate = '';
        $accDate = '';
        $eDate = '';
        $offerDate = '';
        if($fromdate1!="" && $todate1!=""){
        $pdate=" AND (pdate BETWEEN '".$fromdate."' AND '".$todate."')";
        $accDate=" AND (tmacc.acc_date BETWEEN '".$fromdate."' AND '".$todate."')";
        $eDate="AND (edate BETWEEN '".$fromdate."' AND '".$todate."')";
        $offerDate=" AND (pq.offercrdate BETWEEN '".$fromdate."' AND '".$todate."')";
          
        }else{
          
        }

         if($showlaue =='all')
         {
         
          $row1 = DB::select("SELECT count(*) AS totalactivep FROM  post_products WHERE  post_advertisment_pstatus NOT IN('Accepted','Completed','CANCELLED','EXPIRED') AND post_advertisment_userid='".$reuserid."'".$pdate."");
          
          foreach ($row1 as $value1) {
            $count1 =$value1->totalactivep;  
          }
          
          
          $type2='CANCELLED';
          $row2 = DB::select("SELECT count(*) AS totalcancelp FROM  post_products WHERE  post_advertisment_pstatus='". $type2."' AND 
          post_advertisment_userid='".$reuserid."'".$pdate."");
          
          foreach ($row2 as $value2) {
            $count2 =$value2->totalcancelp;  
          }
          
          $type3='Waiting';
          $qutype3='QUOTATION';
          
          $result3 = DB::select("SELECT count(*) AS totalrecquote FROM  qutation_products WHERE  quationstatus='". $type3."' AND 
          post_advertisment_userid='".$reuserid."' AND quationtype='".$qutype3."'".$pdate."");
          
          foreach ($result3 as $row3) {
            $count3 =$row3->totalrecquote;
          }
          
          
          $type4='Processing';
          $qutype4='QUOTATION';
          $result4 = DB::select("SELECT count(*) AS activecounter FROM  qutation_products WHERE  quationstatus='". $type4."' AND 
          post_advertisment_userid='".$reuserid."' AND quationtype='".$qutype4."'".$pdate."");
          
          foreach ($result4 as $row4) {
            $count4 =$row4->activecounter;  
          }
          
          
          $type5='Completed';
          $result5 = DB::select("SELECT count(*) AS completeorder, tmacc.quotationno, tmacc.status, CAST( tmacc.acc_date AS DATE ) AS  `pdate`, pq.postuserid 
              FROM tm_accpted_offers tmacc
              LEFT JOIN post_quotation pq ON tmacc.quotationno = pq.quotationno
              WHERE tmacc.status='". $type5."' AND 
              pq.postuserid='".$reuserid."'".$accDate."");
          
          foreach ($result5 as $row5) {
            $count5 =$row5->completeorder;
          }
          
          
          $result6 = DB::select("SELECT count(*) AS allcustomers FROM post_customer 
              WHERE  adduserid='".$reuserid."'".$eDate."");
          
          foreach ($result6 as $row6) {
            $count6 =$row6->allcustomers;
          }
          
          
           $st7='1';
          $result7 = DB::select("SELECT count(*) AS allcustomers FROM post_customer 
              WHERE  adduserid='".$reuserid."' AND isactive='". $st7."'".$eDate."");
          
          foreach ($result7 as $row7) {
            $count7 =$row7->allcustomers;  
          }

          
            $st8='0';
          $result8 = DB::select("SELECT count(*) AS allcustomers FROM post_customer 
              WHERE  adduserid='".$reuserid."' AND isactive='". $st8."'".$eDate."");
          
          foreach ($result8 as $row8) {
            $count8 =$row8->allcustomers;  
          }
          
          
          $result9 = DB::select("SELECT COUNT( * ) AS acceptcounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Accepted')".$offerDate." and pq.postuserid='".$reuserid."'");
          
          foreach ($result9 as $row9) {
            $count9 =$row9->acceptcounter;  
          }
          
          
          $result10 = DB::select("SELECT COUNT( * ) AS declinecounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Declined') and pq.type='QUOTATION'".$offerDate." and pq.postuserid='".$reuserid."'");
          
          foreach ($result10 as $row10) {
            $count10 =$row10->declinecounter;  
          }
          
          
          $result11 = DB::select("SELECT count(*) as cancelcounter  FROM  tm_accpted_offers tmacc left join post_quotation as pq on pq.quotationno=tmacc.quotationno WHERE tmacc.status IN ('Cancelled')".$accDate." and pq.postuserid='".$reuserid."'");
          

          foreach ($result11 as $row11) {
            $count11 =$row11->cancelcounter;  
          }
          
          
          
          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Active Postings</td><td width="20%">'.$count1.'</td></tr>';
          echo '<tr class="danger"><td width="10%"> 2</td><td width="70%"> Cancelled Postings</td><td width="20%">'.$count2.'</td></tr>';
          echo '<tr class="success"><td width="10%"> 3</td><td width="70%"> Received Quotes</td><td width="20%">'.$count3.'</td></tr>';
          echo '<tr class="danger"><td width="10%">4</td><td width="70%"> Active counters</td><td width="20%">'.$count4.'</td></tr>';
          echo '<tr class="success"><td width="10%">5</td><td width="70%"> Completed orders</td><td width="20%">'.$count5.'</td></tr>';
          echo '<tr class="danger"><td width="10%">6</td><td width="70%"> Cancelled Orders</td><td width="20%">'.$count11.'</td></tr>';
          echo '<tr class="success"><td width="10%"> 7</td><td width="70%"> Customers</td><td width="20%">'.$count6.'</td></tr>';
          echo '<tr class="danger"><td width="10%"> 8</td><td width="70%">Active Customers</td><td width="20%">'.$count7.'</td></tr>';
          echo '<tr class="success"><td width="10%"> 9</td><td width="70%">Inactive Customers</td><td width="20%">'.$count8.'</td></tr>';
          echo '<tr class="danger"><td width="10%">10</td><td width="70%"> Accepted Quotes</td><td width="20%">'.$count9.'</td></tr>';
          echo '<tr class="success"><td width="10%">11</td><td width="70%"> Declined Quotes</td><td width="20%">'.$count10.'</td></tr>';


        }

        if($showlaue =='allqf')
         {
         $type1='Completed';
          $result1 = DB::select("SELECT count(*) AS completeorder, tmacc.quotationno, tmacc.status, CAST( tmacc.acc_date AS DATE ) AS  `pdate`, pq.postuserid 
              FROM tm_accpted_offers tmacc
              LEFT JOIN post_quotation pq ON tmacc.quotationno = pq.quotationno
              WHERE tmacc.status='". $type1."' AND 
              tmacc.tm_userID='".$reuserid."'".$accDate."");

          foreach ($result1 as $row1) {
            $count1 =$row1->completeorder;
          }
          
          $type2='Waitting For Both';
          $result2 = DB::select("SELECT count(*) AS incompleteorder, tmacc.quotationno, tmacc.status, CAST( tmacc.acc_date AS DATE ) AS  `pdate`, pq.postuserid 
              FROM tm_accpted_offers tmacc
              LEFT JOIN post_quotation pq ON tmacc.quotationno = pq.quotationno
              WHERE tmacc.status='". $type2."' AND 
              tmacc.acceptedBy='".$reuserid."'".$accDate."");
          
          foreach ($result2 as $row2) {
            $count2 =$row2->incompleteorder;
          }

          
          $type3='Waiting';
          $qutype3='QUOTATION';
          $result3 = DB::select("SELECT count(*) AS totalrecquote FROM  qutation_products WHERE  quationstatus='". $type3."' AND 
          quationuserid='".$reuserid."' AND quationtype='".$qutype3."'".$pdate."");
          
          foreach ($result3 as $row3) {
            $count3 =$row3->totalrecquote;
          }

          
      $result4 = DB::select("SELECT count(*) AS activecounter FROM quotation_counter qc LEFT JOIN qutation_products qp ON qc.quotationno = qp.quotationno WHERE qc.counter_by ='".$reuserid."'".$pdate."");
          
          foreach ($result4 as $row4) {
            $count4 =$row4->activecounter;
          }


          $result5 = DB::select("SELECT COUNT( * ) AS acceptcounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Accepted')".$accDate." and pq.userid='".$reuserid."'");
          
          foreach ($result5 as $row5) {
            $count5 =$row5->acceptcounter;
          }

          
          $result6 = DB::select("SELECT COUNT( * ) AS declinecounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Declined') and pq.type='QUOTATION'".$offerDate." and pq.userid='".$reuserid."'");
          
          foreach ($result6 as $row6) {
            $count6 =$row6->declinecounter;
          }

          
          $result7 = DB::select("SELECT count(*) as cancelcounter  FROM  tm_accpted_offers tmacc left join post_quotation as pq on pq.quotationno=tmacc.quotationno WHERE tmacc.status IN ('Cancelled')".$accDate." and pq.userid='".$reuserid."'");
          
          foreach ($result7 as $row7) {
            $count7 =$row7->cancelcounter;
          }
          
            echo '<tr class="success"><td width="10%">1</td><td width="70%"> Completed orders</td><td width="20%">'.$count1.'</td></tr>';
          echo '<tr class="danger"><td width="10%">2</td><td width="70%"> InCompleted orders</td><td width="20%">'.$count2.'</td></tr>';
          echo '<tr class="success"><td width="10%">3</td><td width="70%"> Cancelled Orders</td><td width="20%">'.$count7.'</td></tr>';
          echo '<tr class="danger"><td width="10%">4</td><td width="70%"> Active Quotes</td><td width="20%">'.$count3.'</td></tr>';
          echo '<tr class="success"><td width="10%">5</td><td width="70%"> Active Counter</td><td width="20%">'.$count4.'</td></tr>';
          echo '<tr class="danger"><td width="10%">6</td><td width="70%"> Accepted Quotes</td><td width="20%">'.$count5.'</td></tr>';
          echo '<tr class="success"><td width="10%">7</td><td width="70%"> Declined Quotes</td><td width="20%">'.$count6.'</td></tr>';


         
         }  

         if($showlaue =='activepost')
         {
         
          $result = DB::select("SELECT count(*) AS totalactivep FROM  post_products WHERE  post_advertisment_pstatus NOT IN('Accepted','Completed','CANCELLED','EXPIRED') AND post_advertisment_userid='".$reuserid."'".$pdate."");
          
          foreach ($result as $row) {
            $count =$row->totalactivep;  
          }
          
          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Active Postings</td><td width="20%">'.$count.'</td></tr>';
        }

        if($showlaue =='cancelpost')
         {
          $type='CANCELLED';
          $result = DB::select("SELECT count(*) AS totalcancelp FROM  post_products WHERE  post_advertisment_pstatus='". $type."' AND 
          post_advertisment_userid='".$reuserid."'".$pdate."");
          
          foreach ($result as $row) {
            $count =$row->totalcancelp;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Cancelled Postings</td><td width="20%">'.$count.'</td></tr>';
         }
         
    if($showlaue =='recvedquote')
         {
          $type='Waiting';
          $qutype='QUOTATION';
          $result = DB::select("SELECT count(*) AS totalrecquote FROM  qutation_products WHERE  quationstatus='". $type."' AND 
          post_advertisment_userid='".$reuserid."' AND quationtype='".$qutype."'".$pdate."");
          
          foreach ($result as $row) {
            $count =$row->totalrecquote;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Received Quotes</td><td width="20%">'.$count.'</td></tr>';
         }  
      
    if($showlaue =='activecounter')
         {
          $type='Processing';
          $qutype='QUOTATION';
          $result = DB::select("SELECT count(*) AS activecounter FROM  qutation_products WHERE  quationstatus='". $type."' AND 
          post_advertisment_userid='".$reuserid."' AND quationtype='".$qutype."'".$pdate."");
          
          foreach ($result as $row) {
            $count =$row->activecounter;  
          }


          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Active counters</td><td width="20%">'.$count.'</td></tr>';
         }
         
    if($showlaue =='completedorder')
         {
          $type='Completed';
          $result = DB::select("SELECT count(*) AS completeorder, tmacc.quotationno, tmacc.status, CAST( tmacc.acc_date AS DATE ) AS  `pdate`, pq.postuserid 
              FROM tm_accpted_offers tmacc
              LEFT JOIN post_quotation pq ON tmacc.quotationno = pq.quotationno
              WHERE tmacc.status='". $type."' AND 
              pq.postuserid='".$reuserid."'".$accDate."");
          
          foreach ($result as $row) {
            $count =$row->completeorder;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Completed orders</td><td width="20%">'.$count.'</td></tr>';
         }    


    if($showlaue =='allcustomers')
         {
          $result = DB::select("SELECT count(*) AS allcustomers FROM post_customer 
              WHERE  adduserid='".$reuserid."'".$eDate."");
          
          foreach ($result as $row) {
            $count =$row->allcustomers;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%">All Customers</td><td width="20%">'.$count.'</td></tr>';
         }  
         
    if($showlaue =='activecustomer')
         {
          $st='1';
          $result = DB::select("SELECT count(*) AS allcustomers FROM post_customer 
              WHERE  adduserid='".$reuserid."' AND isactive='". $st."'".$eDate."");
          
          foreach ($result as $row) {
              $count =$row->allcustomers;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%">Active Customers</td><td width="20%">'.$count.'</td></tr>';
         }
         
    if($showlaue =='inactivecustomer')
         {
          $st='0';
          $result = DB::select("SELECT count(*) AS allcustomers FROM post_customer 
              WHERE  adduserid='".$reuserid."' AND isactive='". $st."'".$eDate."");
          
          foreach ($result as $row) {
              $count =$row->allcustomers;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%">Inactive Customers</td><td width="20%">'.$count.'</td></tr>';
         }
         
     if($showlaue =='completeorderqf')
         {
          $type='Completed';
          $result = DB::select("SELECT count(*) AS completeorder, tmacc.quotationno, tmacc.status, CAST( tmacc.acc_date AS DATE ) AS  `pdate`, pq.postuserid 
              FROM tm_accpted_offers tmacc
              LEFT JOIN post_quotation pq ON tmacc.quotationno = pq.quotationno
              WHERE tmacc.status='". $type."' AND 
              tmacc.tm_userID='".$reuserid."'".$accDate."");
          
          foreach ($result as $row) {
              $count =$row->completeorder;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Completed orders</td><td width="20%">'.$count.'</td></tr>';
         }  
         
    if($showlaue =='incompleteorder')
         {
          $type='Waitting For Both';
          $result = DB::select("SELECT count(*) AS incompleteorder, tmacc.quotationno, tmacc.status, CAST( tmacc.acc_date AS DATE ) AS  `pdate`, pq.postuserid 
              FROM tm_accpted_offers tmacc
              LEFT JOIN post_quotation pq ON tmacc.quotationno = pq.quotationno
              WHERE tmacc.status='". $type."' AND 
              tmacc.acceptedBy='".$reuserid."'".$accDate."");
          
          foreach ($result as $row) {
              $count =$row->incompleteorder;  
          }
          
          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> InCompleted orders</td><td width="20%">'.$count.'</td></tr>';
         }
         
    if($showlaue =='avtivequote')
         {
          $type='Waiting';
          $qutype='QUOTATION';
          $result = DB::select("SELECT count(*) AS totalrecquote FROM  qutation_products WHERE  quationstatus='". $type."' AND 
          quationuserid='".$reuserid."' AND quationtype='".$qutype."'".$pdate."");
          
          foreach ($result as $row) {
              $count =$row->totalrecquote;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Active Quotes</td><td width="20%">'.$count.'</td></tr>';
         }
         
    if($showlaue =='activecounterqf')
         {
          $result = DB::select("SELECT count(*) AS activecounter FROM quotation_counter qc LEFT JOIN qutation_products qp ON qc.quotationno = qp.quotationno WHERE qc.counter_by ='".$reuserid."'".$pdate."");
          
          foreach ($result as $row) {
              $count =$row->activecounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Active Counter</td><td width="20%">'.$count.'</td></tr>';
         }

    if($showlaue =='cancelledorder')
         {
          $result = DB::select("SELECT count(*) as cancelcounter  FROM  tm_accpted_offers tmacc left join post_quotation as pq on pq.quotationno=tmacc.quotationno WHERE tmacc.status IN ('Cancelled')".$accDate." and pq.userid='".$reuserid."'");
          
          foreach ($result as $row) {
              $count =$row->cancelcounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Cancelled Orders</td><td width="20%">'.$count.'</td></tr>';
         }
    if($showlaue =='acceptedquotes')
         {
          $result = DB::select("SELECT COUNT( * ) AS acceptcounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Accepted')".$accDate." and pq.userid='".$reuserid."'");
          
          foreach ($result as $row) {
              $count =$row->acceptcounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Accepted Quotes</td><td width="20%">'.$count.'</td></tr>';
         }
    if($showlaue =='declinedquotes')
         {
          $result = DB::select("SELECT COUNT( * ) AS declinecounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Declined') and pq.type='QUOTATION'".$offerDate." and pq.userid='".$reuserid."'");
          
          foreach ($result as $row) {
              $count =$row->declinecounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Declined Quotes</td><td width="20%">'.$count.'</td></tr>';
         }
         if($showlaue =='cancelledorderAm')
         {
          $result = DB::select("SELECT count(*) as cancelcounter  FROM  tm_accpted_offers tmacc left join post_quotation as pq on pq.quotationno=tmacc.quotationno WHERE tmacc.status IN ('Cancelled')".$accDate." and pq.postuserid='".$reuserid."'");
          
          foreach ($result as $row) {
              $count =$row->cancelcounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Cancelled Orders</td><td width="20%">'.$count.'</td></tr>';
         }
    if($showlaue =='acceptedquotesAm')
         {
          $result = DB::select("SELECT COUNT( * ) AS acceptcounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Accepted')".$accDate." and pq.postuserid='".$reuserid."'");
          
          foreach ($result as $row) {
              $count =$row->acceptcounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Accepted Quotes</td><td width="20%">'.$count.'</td></tr>';
         }
    if($showlaue =='declinedquotesAm')
         {
          $result = DB::select("SELECT COUNT( * ) AS declinecounter FROM post_quotation pq 
          WHERE pq.quationstatus IN ('Declined') and pq.type='QUOTATION'".$offerDate." and pq.postuserid='".$reuserid."'");
          
          foreach ($result as $row) {
              $count =$row->declinecounter;  
          }

          echo '<tr class="success"><td width="10%"> 1</td><td width="70%"> Declined Quotes</td><td width="20%">'.$count.'</td></tr>';
         }

    }

    public function reports(){

      return View('user.report')->with('title','Ziki Trade');
    }

    public function reports_ajax(Request $request){

      $industry_id = $request->input('industry_id');
      $sec_name = $request->input('sec_name');
      $secname = $request->input('secname');
     
      $productno =$request->input('productno');
      $industryid =$request->input('industryid');
      $postno =$request->input('postno');
      $refno =$request->input('refno'); 
      $clauseby =$request->input('dataClauseby'); 
      $lastfilter =urldecode($request->input('lastfilter')); 
      $view_type =$request->input('view_type');

      foreach($request->input() as $lo=>$do){

          if($do!="") {
              $lastvisited=$lo;
              $lastvisitedvalue = $do;
          }
      }

      if ($sec_name=='Postings' && $request->input('view_type')=="detailed") {

          if($lastfilter !="All"){

            if($lastvisitedvalue!="Postings"){
              
              $sql = DB::select("SELECT *  FROM post_products where $clauseby='".$lastfilter."'");
            }
            else
            {
              $sql = DB::select("SELECT *  FROM post_products where 1=1");

            }
          }
          else{

            $sql = DB::select("SELECT *  FROM post_products where 1=1");
          }

          $record = array();
          $i = 0;

          foreach ($sql as $row) {
            $postno = '';
            $postno = isset($row->postno)?$row->postno:'';
            $url = '<a href="report_detailed.php?key=40&postno='.$postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
            $product_name = mb_convert_encoding(isset($row->product_name)?$row->product_name:'', "HTML-ENTITIES", "utf8");
            $productid = mb_convert_encoding(isset($row->productno)?$row->productno:'', "HTML-ENTITIES", "utf8");
            $proid = mb_convert_encoding(isset($row->productid)?$row->productid:'', "HTML-ENTITIES", "utf8");
            $industryname = mb_convert_encoding(isset($row->industry_name)?$row->industry_name:'', "HTML-ENTITIES", "utf8");
            $subcategoryname = mb_convert_encoding(isset($row->subcategory_name)?$row->subcategory_name:'', "HTML-ENTITIES", "utf8");
            $categoryname = mb_convert_encoding(isset($row->category_name)?$row->category_name:'', "HTML-ENTITIES", "utf8");
            $brandname = mb_convert_encoding(isset($row->brand_name)?$row->brand_name:'', "HTML-ENTITIES", "utf8");
            $manufacture = mb_convert_encoding(isset($row->manufacture)?$row->manufacture:'', "HTML-ENTITIES", "utf8");
            $caseweight = mb_convert_encoding(isset($row->caseweight)?$row->caseweight:'', "HTML-ENTITIES", "utf8");
            $qtypercase = mb_convert_encoding(isset($row->qtypercase)?$row->qtypercase:'', "HTML-ENTITIES", "utf8");
            $pakaging = mb_convert_encoding(isset($row->pakaging)?$row->pakaging:'', "HTML-ENTITIES", "utf8");
            $shipingcondition =mb_convert_encoding(isset($row->shipingcondition)?$row->shipingcondition:'', "HTML-ENTITIES", "utf8");
            $description =mb_convert_encoding(isset($row->description)?$row->description:'', "HTML-ENTITIES", "utf8");
            $ptype = mb_convert_encoding(ucwords(strtolower(isset($row->ptype)?$row->ptype:'')), "HTML-ENTITIES", "utf8");
            $targetprice = mb_convert_encoding(isset($row->targetprice)?$row->targetprice:'', "HTML-ENTITIES", "utf8");
            $currency = mb_convert_encoding(isset($row->currency)?$row->currency:'', "HTML-ENTITIES", "utf8");
            $uom =mb_convert_encoding(isset($row->uom)?$row->uom:'', "HTML-ENTITIES", "utf8");
            $quantity =mb_convert_encoding(isset($row->quantity)?$row->quantity:'', "HTML-ENTITIES", "utf8");
            $location = mb_convert_encoding(isset($row->location)?$row->location:'', "HTML-ENTITIES", "utf8");
            $expdate = mb_convert_encoding(isset($row->expdate)?$row->expdate:'', "HTML-ENTITIES", "utf8");
            $expirydate = mb_convert_encoding(isset($row->expirydate)?$row->expirydate:'', "HTML-ENTITIES", "utf8");
            $customerrefno =mb_convert_encoding(isset($row->customerrefno)?$row->customerrefno:'', "HTML-ENTITIES", "utf8");
            $post_advertisment_pakaging =mb_convert_encoding(isset($row->post_advertisment_pakaging)?$row->post_advertisment_pakaging:'', "HTML-ENTITIES", "utf8");
            $post_advertisment_timeframe = mb_convert_encoding(isset($row->post_advertisment_timeframe)?$row->post_advertisment_timeframe:'', "HTML-ENTITIES", "utf8");
            $post_advertisment_country = mb_convert_encoding(isset($row->post_advertisment_country)?$row->post_advertisment_country:'', "HTML-ENTITIES", "utf8");
            $post_advertisment_userid =mb_convert_encoding(isset($row->post_advertisment_userid)?$row->post_advertisment_userid:'', "HTML-ENTITIES", "utf8");
            $pdate = strtotime(mb_convert_encoding(isset($row->pdate)?$row->pdate:'', "HTML-ENTITIES", "utf8"));
            $pdate = date("F d Y", $pdate);
            $post_advertisment_pstatus =mb_convert_encoding(isset($row->post_advertisment_pstatus)?$row->post_advertisment_pstatus:'', "HTML-ENTITIES", "utf8");
            $postno =(isset($row->postno)?$row->post_advertisment_pstatus:'').$url;

            $record[$i]['product_name'] = $product_name;
            $record[$i]['productid'] = $productid;
            $record[$i]['industryname'] = $industryname;
            $record[$i]['categoryname'] = $categoryname;
            $record[$i]['subcategoryname'] = $subcategoryname;
            $record[$i]['brandname'] = $brandname;
            $record[$i]['ptype'] = $ptype;
            $record[$i]['targetprice'] = $targetprice;
            $record[$i]['currency'] = $currency;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['location'] = $location;
            $record[$i]['post_advertisment_userid'] = $post_advertisment_userid;
            $record[$i]['pdate'] = $pdate;
            $record[$i]['post_advertisment_pstatus'] = $post_advertisment_pstatus;
            $record[$i]['postno'] = $postno;

            $i++;

          }

          return \Response::json($record);
      }


    }

 
}