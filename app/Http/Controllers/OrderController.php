<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MstProduct;
use App\PostQuotation;
use App\TMAcceptedOffers;
use DB;
class OrderController extends Controller
{
    
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$routeName = \Route::getCurrentRoute()->getPath();
       // echo \Request::segment(2);die;
       
        return View('order.acceptedoffer')->with('title','Accepted Offers');        
    }

    public function offers_ajax($type){

        if (isset($_COOKIE["productActionType"]) && $_COOKIE["productActionType"] == "group_filter") {

        }else{
            $query = DB::table('tm_accpted_offers as ta');
            
            $query->leftJoin('mst_product as mp','ta.productno','=','mp.productno')
                ->leftJoin('post_advertisment as pa','ta.postno', '=', 'pa.postno')
                ->leftJoin('post_quotation as pq','ta.quoteid', '=', 'pq.quoteid')
                ->select('ta.acc_offr_id', 'ta.counterid', 'ta.quotationno', 'mp.productno','mp.productid', 'ta.quoteid','pa.postno', 'ta.postno', 'ta.productno', 'ta.acceptedBy', 'ta.acc_date', 'ta.acc_price as price', 'ta.acc_quantity as quantity', 'ta.acc_timeframe as timeframe', 'ta.acc_expdate as expdate', 'ta.status', 'ta.noti_status','pa.ptype','mp.name as productName','pq.currency');
            
            if($type == 'accepted'){
                $query->where('status', '<>', 'Completed')
                ->where('status', '<>', 'Cancelled');
            }
            elseif($type == 'completed'){
                $query->where('status', '=', 'Completed');
            }

            $orders = $query->get();
                
        }

        $i = 0;
        $record = array();
        foreach($orders  as $order){
            $record[$i]['quotationno']  =   $order->quotationno;
            $record[$i]['acc_date']     =   date('F d Y - h:i A',strtotime($order->acc_date));
            $record[$i]['ptype']        =   ucwords(strtolower($order->ptype));
            $record[$i]['quantity']     =   $order->quantity;
            $record[$i]['productName']  =   mb_convert_encoding($order->productName, "HTML-ENTITIES", "utf8");
            $record[$i]['price']        =   $order->price;
            $record[$i]['currency']     =   $order->currency;
            $record[$i]['timeframe']    =   $order->timeframe;
            $record[$i]['expdate']      =   $order->expdate;
            $record[$i]['status']       =   $order->status;
        }

        return \Response::json($record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
