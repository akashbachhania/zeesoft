<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Industry;
use App\UserActivity;
use App\Category;
use App\SubCategory;
use App\Brand;
use Session;
use Crypt;
use SSH;
use DB;
use Illuminate\Support\Facades\Input;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function backupPage(){
    	return View('setting.backupPage')->with('title','Ziki Trade::Backup DB');
    }

    public function restorePage(){
    	return View('setting.restorePage')->with('title','Ziki Trade::Restore DB');
    }
    
    public function backup(){
    	$now            = str_replace(":", "", date("Y-m-d H: i: s"));
        $outputfilename = 'zeesoft-' . $now . '.sql';
        $outputfilename = str_replace(" ", "-", $outputfilename);
        
        $commands = ([
            'cd /var/www/html/public/sql',
            'mysqldump -uroot -plogin123#123# minialpha > '.$outputfilename,
        ]);
        SSH::run($commands);

        $file = public_path() .'/sql/' . $outputfilename;
        $header = array(
        	'Content-Type'=> 'force/download'
        	);
        return \Response::download($file,$outputfilename,$header);
    }

    public function restore(Request $request){
        
        $path = public_path() .'/sql/'; 

        $now            = str_replace(":", "", date("Y-m-d H: i: s"));
        $outputfilename = 'zeesoft-' . $now . '.sql';
        $outputfilename = str_replace(" ", "-", $outputfilename);
        Input::file('sql')->move($path, $outputfilename);        
        $commands = ([
            'cd /var/www/html/public/sql',
            'mysql -uroot -plogin123#123# minialpha < '.public_path() . '/sql/'.$outputfilename,
        ]);
        SSH::run($commands);
        \Session::flash('alert-success', 'Database Restored successfully'); 
            return \Redirect::to('setting/dbrestore');
    }

    public function industry(){
         
    	return View('setting.industry')->with('title','Ziki Trade::Industry');
    }

    public function industry_ajax(Request $request){
       
    	switch ($request->type) {

    		case 'exist':
    			$industry=Industry::where('name','=',$request->pname)->count();
    			if($industry>0)
    			{
    				echo 'exist';
    			}
    			else
				{
					echo '1';
				}
    		break;
    		
    		case 'add':
    			$industry=Industry::where('name','=',$request->name)->count();
    			if($industry==0)
    			{
	    			$industry=new Industry;

	    			$industry->name 		= 	$request->name;
	    			$industry->isactive		= 	$request->status;
	    			$industry->save();
	    			$arr = array(
							'name' => 'Record Saved Sucessfully',
							'msg' => 'SUCCESS'
						);
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Industry';

                    $useraction->save();
	    		}
	    		else{
	    			$arr = array(
						'msg' => 'ERROR'
					);
	    		}
    			return \Response::json($arr);

    		  break;

            case 'search':
                $industry=Industry::find($request->id);
                return \Response::json($industry);
                break;
            
            case 'update':
                
                $industry=Industry::where('name','=',$request->name)->count();
                if($industry==0)
                {
                    $industry=Industry::find($request->id);

                    $industry->name         =   $request->name;
                    $industry->isactive     =   $request->status;
                    $industry->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Industry';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $industry=Industry::find($request->id);
                if($industry->category->count()>0 || $industry->brand->count()>0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    
                }
                else{
                    $industry->delete();
                        $arr = array(
                        'name' => 'Related Record Delete',
                        'msg' => 'A1'
                        );
                        $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Industry';

                    $useraction->save();
                }
                return \Response::json($arr);
            break;
            case 'industaction':
                    
                $roletype = Auth::user()->roletype;
                $userid   = Auth::user()->userid;
                $status   = 1;
                $iid      = (Auth::user()->industry != '') ? explode(',',Auth::user()->industry) : array();

                $json = array();
                if ($roletype == 'AM') {
                    if (empty($iid) == false) {
                                            
                        $sth = \App\Industry::where('isactive',$status)->whereIn('industryid',$iid)->orderBy('name')->get();
                        
                        foreach ($sth as $value) {
                            $industryid = $value->industryid;
                            $name = mb_convert_encoding($value->name, "HTML-ENTITIES", "ISO-8859-1");
                            $json['industry'][] = array('industryid'=>$industryid,  'name'=>$name);    
                        }
                        return \Response::json($json);

                    } else if (empty($iid) == true) {
                    
                        $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();
                        
                        foreach ($sth as $value) {
                            $industryid = $value->industryid;
                            $name = mb_convert_encoding($value->name, "HTML-ENTITIES", "ISO-8859-1");
                            $json['industry'][] = array('industryid'=>$industryid,  'name'=>$name);    
                        }
                        return \Response::json($json);

                    }
                }
                if ($roletype == 'AD') {

                    $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();
                        
                    foreach ($sth as $value) {
                        $industryid = $value->industryid;
                        $name = mb_convert_encoding($value->name, "HTML-ENTITIES", "ISO-8859-1");
                        $json['industry'][] = array('industryid'=>$industryid,  'name'=>$name);    
                    }
                    return \Response::json($json);

                }    
                if ($roletype == 'SA') {
                    
                    $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();
                        
                    foreach ($sth as $value) {
                        $industryid = $value->industryid;
                        $name = mb_convert_encoding($value->name, "HTML-ENTITIES", "ISO-8859-1");
                        $json['industry'][] = array('industryid'=>$industryid,  'name'=>$name);    
                    }
                    return \Response::json($json);

                }
            
                // $industry=Industry::orderBy('name')->get();
                          
            break;
    		default:
                //echo $request->type;die(' ld');
                //break;
    		$industry=Industry::orderBy('name')->get();
            $i=0;
            $record=array();
            foreach ($industry as $row) {
                $record[$i]['name']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['industryid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['industryid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
    		break;
    	}
    }
    public function category(){
        $industries=Industry::orderBy('name')->get();
        return View('setting.category',compact('industries'))->with('title','Ziki Trade::Category');
    }
    public function category_ajax(Request $request){
         
        switch ($request->type) {
            case 'exist':
                $Category=Category::where('name','=',$request->pname)->where('industryid','=',$request->iid)->count();
                if($Category>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $Category=Category::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Category==0)
                {
                    $Category=new Category;

                    $Category->name           =   $request->name;
                    $Category->industryid     =   $request->iid;
                    $Category->isactive       =   $request->status;
                    $Category->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'ERROR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $Category=Category::find($request->id);
                return \Response::json($Category);
                break;
            
            case 'update':
                
                $Category=Category::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Category==0)
                {
                    $Category=Category::find($request->id);

                    $Category->name               =   $request->name;
                    $Category->industryid         =   $request->iid;
                    $Category->isactive           =   $request->status;
                    $Category->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $sqlPro = \App\MstProduct::where('catid',$request->id)->count();
                $sqlSub = \App\SubCategory::where('catid',$request->id)->count();
                
                if(($sqlSub > 0) || ($sqlPro > 0))
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    return \Response::json($arr);
                }
                else{
                    $category = \App\Category::find($request->id);
                    $category->delete();
                        $arr = array(
                        'name' => 'Related Record Delete',
                        'msg' => 'A1'
                        );
                        $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Industry';

                    $useraction->save();
                }
                return \Response::json($arr);
            break;

            case 'category':
                
                $roletype    = Auth::user()->roletype;
                $userid      = Auth::user()->userid;
                $industry_id = \Request::input('ind_id');
                $bid         = (Auth::user()->category != '') ? explode(',',Auth::user()->category) : array();

                if (empty($bid)) {
                    
                    $Category = Category::where('industryid','=',$industry_id)->orderBy('name')->get();    
                
                    return \Response::json(compact('Category'));
                
                } else {

                    $Category = Category::where('industryid','=',$industry_id)->whereIn('catid',$bid)->orderBy('name')->get();                 
                    return \Response::json(compact('Category'));

                }

            break;

            default:
            $Category=Category::orderBy('name')->get();
            $i=0;
            $record=array();
            foreach ($Category as $row) {
                
                $record[$i]['industry']=$row['industry']['name'];
                $record[$i]['category']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['catid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['catid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
            break;
               
        }
    }
    public function subcategory(){
        $industries=Industry::orderBy('name')->get();
        return View('setting.subcategory',compact('industries'))->with('title','Ziki Trade::Sub-Category');
    }
    public function subcategory_ajax(Request $request){
         
        switch ($request->type) {
            case 'exist':
                $Subcategory=SubCategory::where('name','=',$request->pname)
                                    ->where('catid','=',$request->iid)
                                    ->where('industryid','=',$request->industry)
                                    ->count();
                if($Subcategory>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            case 'category':
                $Category=Category::where('industryid','=',$request->ind_id)->get();
               
                    return \Response::json(compact('Category'));
                
               
            break;
            case 'add':
                $Subcategory=SubCategory::where('name','=',$request->name)
                                        ->where('catid','=',$request->iid)
                                        ->where('industryid','=',$request->industry)
                                        ->count();
                if($Subcategory==0)
                {
                    $Subcategory=new Subcategory;

                    $Subcategory->name           =   $request->name;
                    $Subcategory->industryid     =   $request->industry;
                    $Subcategory->catid          =   $request->iid;
                    $Subcategory->isactive       =   $request->status;
                    $Subcategory->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Sub Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $Subcategory=SubCategory::find($request->id);
                return \Response::json($Subcategory);
                break;
            
            case 'update':
                
               $Subcategory=SubCategory::where('name','=',$request->name)
                                        ->where('catid','=',$request->iid)
                                        ->where('industryid','=',$request->industry)
                                        ->count();
                if($Subcategory==0)
                {
                    $Subcategory=SubCategory::find($request->id);

                    $Subcategory->name           =   $request->name;
                    $Subcategory->industryid     =   $request->industry;
                    $Subcategory->catid          =   $request->iid;
                    $Subcategory->isactive       =   $request->status;
                    $Subcategory->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Sub Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;

            case 'subcat':
                
                $cat_id   = \Request::input('cat_id');
                $roletype = Auth::user()->roletype;
                $userid   = Auth::user()->userid;
                $subcatid = (Auth::user()->subcategory != '')?explode(',',Auth::user()->subcategory) : array();

                if (empty($subcatid)) {
                    
                    $Subcategory=SubCategory::where('catid','=',$request->cat_id)->orderBy('name')->get();
    
                    return \Response::json(compact('Subcategory'));

                } else {

                    $Subcategory=SubCategory::where('catid','=',$request->cat_id)->whereIn('subcatid',$subcatid)->orderBy('name')->get();
                
                    return \Response::json(compact('Subcategory'));
                }                        
                                        
            break;
            case 'delete':
                $sql = \App\MstProduct::where('subcatid',$request->id)->count();


                if($sql > 0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    return \Response::json($arr);
                }
                else{
                    $subcategor = \App\SubCategory::find($request->id);
                    $subcategor->delete();

                    $arr = array(
                        'name' => 'Related Record Deleted',
                        'msg' => 'ERROR'
                    );

                    return \Response::json($arr);
                }
            break;

            default:
            $Subcategory=SubCategory::orderBy('name')->get();

            $i=0;
            $record=array();
            foreach ($Subcategory as $row) {
                
                $record[$i]['industry']=$row['category']['industry']['name'];
                $record[$i]['category']=$row['category']['name'];
                $record[$i]['subcategory']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['subcatid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['subcatid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
            break;
               
        }
    }
     public function brand(){
        $industries=Industry::orderBy('name')->get();
        return View('setting.brand',compact('industries'))->with('title','Ziki Trade::Category');
    }
    public function brand_ajax(Request $request){
         
        switch ($request->type) {
            case 'exist':
                $Brand=Brand::where('name','=',$request->pname)->where('industryid','=',$request->iid)->count();
                if($Brand>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $Brand=Brand::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Brand==0)
                {
                    $Brand=new Brand;

                    $Brand->name           =   $request->name;
                    $Brand->industryid     =   $request->iid;
                    $Brand->isactive       =   $request->status;
                    $Brand->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Brand';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $Brand=Brand::find($request->id);
                return \Response::json($Brand);
                break;
            
            case 'update':
                
                $Brand=Brand::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Brand==0)
                {
                    $Brand=Brand::find($request->id);

                    $Brand->name               =   $request->name;
                    $Brand->industryid         =   $request->iid;
                    $Brand->isactive           =   $request->status;
                    $Brand->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $sql = \App\MstProduct::where('brandid',$request->input('id'))->count();

                if( $sql > 0 ){
                    
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    return \Response::json($arr);
                    
                }
                else{
                    $brand=\App\Brand::find($request->id);
                    $brand->delete();

                    $arr = array(
                        'name' => 'Record Delete Sucessfully',
                        'msg' => 'SUCCESS'
                    );

                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Brand';

                    $useraction->save();

                    return \Response::json($arr);
                }

            break;
            case 'brand':

                $roletype    = Auth::user()->roletype;
                $userid      = Auth::user()->userid;
                $industry_id = \Request::input('industry_id');

                $status = 1;    

                $bid = (Auth::user()->brand != '') ? explode(',',Auth::user()->brand) : array();

                if ($roletype == 'AM') {
                    if (empty($bid)) {
                        
                        $Brand = Brand::where('industryid','=',$industry_id)->orderBy('name')->get();
        
                        return \Response::json(compact('Brand'));

                    }
                    else
                    {
        
                        $Brand=Brand::where('industryid','=',$request->industry_id)->whereIn('brandid',$bid)->orderBy('name')->get();
        
                        return \Response::json(compact('Brand'));

                    }
                }
                if ($roletype == 'AD') {
                    
                    $Brand = Brand::where('industryid','=',$industry_id)->orderBy('name')->get();
        
                    return \Response::json(compact('Brand'));

                }
                if ($roletype == 'SA') {
                    
                    $Brand = Brand::where('industryid','=',$industry_id)->orderBy('name')->get();
        
                    return \Response::json(compact('Brand'));

                }
                 
            break;
            default:
            $Brand=Brand::orderBy('name')->get();
            $i=0;
            $record=array();
            foreach ($Brand as $row) {
                
                $record[$i]['industry']=$row['industry']['name'];
                $record[$i]['brand']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['brandid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['brandid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
            break;
               
        }
    }
    public function mstsecurity(){

        return View('setting.security')->with('title','Ziki Trade::Security');
    }
    public function mstsecurity_ajax(Request $request){
        switch ($request->Type) {
            case 'exist':
                $mstsecurity = \App\MstSecurity::where('name','=',$request->pname)->count();
                if($mstsecurity > 0)
                {
                    return 'exist';
                }
                else
                {
                    return '1';
                }
            break;

            case 'add':
                $mstsecurity = \App\MstSecurity::where('name','=',$request->name)->count();
                if($mstsecurity == 0)
                {
                    $squestion = new \App\MstSecurity;

                    $squestion->name       =   $request->name;
                    $squestion->isactive   =   '1';
                    $squestion->qtype      =   $request->iid;
                    
                    $squestion->save();

                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Question';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'name' => 'Record Already Exists',
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);

              break;            
            
            case 'search':
                $mstsecurity = \App\MstSecurity::find($request->id);

                if (count($mstsecurity) > 0) {
                    $name    = '';
                    $stautus = '';
                    $qtype   = '';

                     
                    $stautus = $mstsecurity->isactive;
                    $name    = $mstsecurity->name;
                    $qtype   = $mstsecurity->qtype;

                }
                
                $arr = array(
                    'name' => $name,
                    'status' => $stautus,
                    'qtype' => $qtype
                );

                return $arr;

                break;
            
            case 'update':
                
                $mstsecurity = \App\MstSecurity::where('name','=',$request->name)->where('securityid','=',$request->id)->count();
                if($mstsecurity == 0)
                {
                    $security = \App\MstSecurity::where('securityid',$request->id)->update(['name'=>$request->name,'isactive'=>$request->status,'qtype'=>$request->iid]);

                    $arr = array(
                            'name' => 'Record Updated Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Question';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $sql = \App\User::where('secquiz1',$request->id)->orWhere('secquiz2',$request->id)->orWhere('secquiz3',$request->id)->get();

                if( count($sql) > 0 ){
                    foreach ($sql as $value) {
                        if ($sql > 0) {
                            
                            $arr = array(
                                'name' => 'Related Record Exists',
                                'msg' => 'ERROR'
                            );
                            
                            return $arr;

                        } else {

                            $sql = \App\MstSecurity::where('securityid',$request->id)->delete();

                            $arr     = array(
                                'name' => 'Related Record Deleted',
                                'msg' => 'A2'
                            );

                            $useraction = new UserActivity;

                            $useraction->userid = Auth::user()->userid;
                            $useraction->ipaddress = $request->ip();
                            $useraction->sessionid = Session::get('_token');
                            $useraction->actiondate = date('Y-m-d');
                            $useraction->actiontime = date('Y-m-d H:i:sa');
                            $useraction->actionname = 'Deleted Question';

                            $useraction->save();

                            return $arr;
                        }
                    }
                }   
                else{
                    $sql = \App\MstSecurity::where('securityid',$request->id)->delete();

                    $arr     = array(
                        'name' => 'Related Record Deleted',
                        'msg' => 'A2'
                    );

                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Question';

                    $useraction->save();

                    return $arr; 
                }

            break;



            default:

                $security = \App\MstSecurity::where('isactive','1')->orderBy('name','qtype')->get();
                
                $i=0;
                $record=array();
                
                foreach ($security as $row) {
                    $record[$i]['name'] = $row->name;

                    if ($row->qtype == 1) {
                        $record[$i]['qtype']= "Question 1";
                    } elseif ($row->qtype == 2) {
                        $record[$i]['qtype']= "Question 2";
                    } else {
                        $record[$i]['qtype']= "Question 3";
                    }

                    if (Auth::user()->roletype == 'AD') {
                        $edit = '<a href="javascript:editme(\'' . $row->securityid . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;

                        <a  href="javascript:deleteme(\'' . $row->securityid . '\')"><i class="fa fa-trash-o"></i></a>';
                    } else {
                        if ($checkVal[0]['EditP'] == '1') {
                            $edit1 = '<a href="javascript:editme(\'' . $row->securityid . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;';
                        }
                        
                        if ($checkVal[0]['DeleteP'] == '1') {
                            $edit2 = '<a  href="javascript:deleteme(\'' . $row->securityid . '\')"><i class="fa fa-trash-o"></i></a>';
                        }
                        
                        $edit = $edit1 . $edit2;
                    }
                    
                    $record[$i]['edit']= $edit;

                    $i++;
                }
                
                return \Response::json($record);
                
                break;
        }
    }

    public function packaging(){
         
        return View('setting.packaging')->with('title','Ziki Trade::Packaging');
    }
    
    public function packaging_ajax(Request $request){
       
        switch ($request->type) {

            case 'exist':
                $packaging=\App\MstPackaging::where('name','=',$request->pname)->count();
                if($packaging>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $packaging = \App\MstPackaging::where('name','=',$request->name)->count();
                if($packaging==0)
                {
                    $packaging=new \App\MstPackaging;

                    $packaging->name = $request->name;
                    $packaging->isactive = $request->status;
                    $packaging->save();
                    
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Packaging';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'ERROR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $packaging=\App\MstPackaging::find($request->id);
                return \Response::json($packaging);
                break;
            
            case 'update':
                
                $packaging = \App\MstPackaging::where('name','=',$request->name)->count();
                if($packaging == 0)
                {
                    $packaging = \App\MstPackaging::find($request->id);

                    $packaging->name = $request->name;
                    // $packaging->isactive = $request->status;
                    $packaging->save();
                    
                    $arr = array(
                            'name' => 'Record Updated Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Packaging';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $packaging = \App\MstPackaging::find($request->id);
                
                $packaging->delete();

                $arr = array(
                    'name' => 'Related Record Delete',
                    'msg' => 'A1'
                );
                
                $useraction = new UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Deleted Packaging';

                $useraction->save();
            
                return \Response::json($arr);
  
            break;

            default:

                $packaging = \App\MstPackaging::where('isactive','1')->orderBy('name')->get();
                
                $i=0;
                $record=array();
                foreach ($packaging as $row) {
                    $record[$i]['srno'] = ($i+1);
                    $record[$i]['packaging']=$row['name'];
                    $record[$i]['action']='<a href="javascript:editme(\'' . $row['pkgid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['pkgid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                    $i++;
                }
                
                return \Response::json($record);
            break;
        }
    }


    public function shippingConditions(){
         
        return View('setting.shipping')->with('title','Ziki Trade::Shipping Conditions');
    }
    
    public function shippingConditions_ajax(Request $request){
       
        switch ($request->type) {

            case 'exist':
                $shipping = \App\MstShipping::where('name','=',$request->pname)->count();
                if($shipping > 0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $shipping = \App\MstShipping::where('name','=',$request->name)->count();
                if($shipping == 0)
                {
                    $shipping = new \App\MstShipping;

                    $shipping->name = $request->name;
                    $shipping->isactive = $request->status;
                    $shipping->save();
                    
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Shipping Conditions';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'ERROR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $shipping = \App\MstShipping::find($request->id);
                return \Response::json($shipping);
                break;
            
            case 'update':
                
                $shipping = \App\MstShipping::where('name','=',$request->name)->count();
                
                if($shipping == 0){

                    $shipping = \App\MstShipping::find($request->id);

                    $shipping->name = $request->name;
                    // $shipping->isactive = $request->status;
                    $shipping->save();
                    
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Shipping Conditions';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $shipping = \App\MstShipping::find($request->id);
                
                $shipping->delete();

                $arr = array(
                    'name' => 'Related Record Delete',
                    'msg' => 'A1'
                );
                
                $useraction = new UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Deleted Shipping Conditions';

                $useraction->save();
            
                return \Response::json($arr);
  
            break;

            default:

                $shipping = \App\MstShipping::where('isactive','1')->orderBy('name')->get();
                
                $i=0;
                $record=array();
                foreach ($shipping as $row) {
                    $record[$i]['srno'] = ($i+1);
                    $record[$i]['shipping']=$row['name'];
                    $record[$i]['action']='<a href="javascript:editme(\'' . $row['shipid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['shipid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                    $i++;
                }
                
                return \Response::json($record);
            break;
        }
    }

    public function mySetting(){

        $timezones = \App\Timezone::where('isactive','1')->get();
                
        $secque = \App\MstSecurity::where('isactive','1')->where('qtype','1')->orderBy('name')->get();

        $secque1 = \App\MstSecurity::where('isactive','1')->where('qtype','2')->orderBy('name')->get();

        $secque2 = \App\MstSecurity::where('isactive','1')->where('qtype','3')->orderBy('name')->get();

        $data = array(
                'timezones' => $timezones,
                'secque' => $secque,
                'secque1' => $secque1,
                'secque2' => $secque2
            );

        return View('setting.mySetting',$data)->with('title','Ziki Trade::My Setting');
    }

    public function updateMySetting(Request $request){

        if (isset($request->submit_msettings)) {

            $ftlogin  = "Y";
            
            $timezone = $request->timezone;
            
            $secquiz1 = $request->sque1;
            
            $secquiz2 = $request->sque2;
            
            $secquiz3 = $request->sque3;
            
            $secans1  = $request->sans1;
            
            $secans2  = $request->sans2;
            
            $secans3  = $request->sans3;
            
            $visual   = $request->vnotification;
            
            $audio    = $request->anotification;
            
            $userid   = $request->userid;
            
            if ($request->pwd == "") {
                
                $data = array(
                        'ftlogin' => $ftlogin,
                        'timezoneid' => $timezone,
                        'secquiz1' => $secquiz1,
                        'secquiz2' => $secquiz2,
                        'secquiz3' => $secquiz3,
                        'secans1' => $secans1,
                        'secans2' => $secans2,
                        'secans3' => $secans3,
                        'visualnoti' => $visual,
                        'audionoti' => $audio
                    );

                $sql = \App\User::where('userid',$userid)->update($data);
                
            } else {

                $data = array(
                        'password' => bcrypt($request->pwd),
                        'authorizedpass' => bcrypt($request->pwd),
                        'ftlogin' => $ftlogin,
                        'timezoneid' => $timezone,
                        'secquiz1' => $secquiz1,
                        'secquiz2' => $secquiz2,
                        'secquiz3' => $secquiz3,
                        'secans1' => $secans1,
                        'secans2' => $secans2,
                        'secans3' => $secans3,
                        'visualnoti' => $visual,
                        'audionoti' => $audio
                    );

                $sql = \App\User::where('userid',$userid)->update($data);
            }
            
            $useraction = new \App\UserActivity;

            $useraction->userid = $userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Updated User Settings';

            $useraction->save();
            
            return redirect('setting/mySetting?page=nsetting');

        }


        if ($request->lognoti == 'lognoti') {
            
            $key = $request->id;
            $roletype = Auth::user()->roletype;

            $sth = \App\PageNotification::where('p_key',$key)->where('roletype',$roletype)->get();

            foreach ($sth as $value) {
                
                if (count($sth) == 0) {
                    echo "No Notification";
                } else {
                    echo $value->n_msg;
                }
            
            }
        }
    }

    public function notificationSetting(){

        $checkVal = $this->checkPermission(25);

        return View('setting.notificationSetting',['checkVal'=>$checkVal])->with('title','Ziki Trade::Message Center');
        }

        public function notificationSetting_ajax(){

            $readnameforam = " status in('Read','Unread') and";
            $readnamevalam = " readunreadst in(0,1) and";
            $readnotivaltm = " where noti_status in ('Read','UnRead')";

            if ((Auth::user()->roletype == 'AD') || (Auth::user()->roletype == 'QF')) {

                $sql1 = \App\MstNotification::where('userid',Auth::user()->userid)->where('fromuserid','!=','UUZZ-9984')->whereIn('status',['Read','Unread'])->get();
            
            }
            if (Auth::user()->roletype == 'AM') {
                
                $sql1 = \DB::select("SELECT *FROM (SELECT notiid,msgdate,status,msg,pro_id, 0 as rdstatus FROM mst_notification left Join mst_product mp ON mp.productid=pro_id  where " . $readnameforam." userid='" . Auth::user()->userid . " ' UNION ALL SELECT postno as notiid,offercrdate as msgdate,0 as status,0 as msg,0 as pro_id,readunreadst as rdstatus FROM post_quotation where ".$readnamevalam." postuserid='" . Auth::user()->userid . "')  as a");

                // $sql = \App\MstNotification::whereIn('status',['Read','Unread'])->where('userid',Auth::user()->userid);

                // $sql1 = \App\PostQuotation::whereIn('readunreadst',['0','1'])->where('postuserid',Auth::user()->userid)->union($sql)->get();

            }
            if (Auth::user()->roletype == 'TM') {

                $sql1 = \DB::select("SELECT *FROM (SELECT notiid,msgdate,status,msg,pro_id,0 as acc_offr_id FROM mst_notification left Join mst_product mp ON mp.productid=pro_id  where ". $readnameforam ." userid='" . Auth::user()->userid . " ' UNION ALL SELECT postno as notiid,acc_date as msgdate,noti_status as status,0 as msg,quotationno as pro_id,acc_offr_id FROM tm_accpted_offers ".$readnotivaltm.")  as a");

                // $sql = \App\MstNotification::whereIn('status',['Read','Unread'])->where('userid',Auth::user()->userid);
                // $sql1 = \App\TMAcceptedOffers::whereIn('noti_status',['Read','Unread'])->union($sql)->get();

            }
            $record = array();
            $i = 0;
            foreach ($sql1 as $result) {
                
                $sdate = date('F d Y - h:i A',strtotime(isset($result->msgdate)?$result->msgdate:'') ) .' EST';

                $stnew = $result->rdstatus;

                if($stnew==0){ 
                    $stres= "Unread"; 
                }
                else if($stnew==1){ 
                    $stres=  "Read"; 
                }
                else{

                   $stres=  "Archived"; 
                }
                
                $accmsg = "Accepted Offer Recived";

                $notId = $result->notiid;

                $msg = $result->msg . '<input type="hidden" value="' . $notId . '" name="hiddennotiid" id="hiddennotiid"/>';

                $pro_id = $result->pro_id;

                if (Auth::user()->roletype == 'AD') {
                    
                    $status = $result->status . '<a href="suggest-product-review/' . $pro_id . '/' . $notId . '/'.$result->status.'"  style="display:none;"></a>';
                }
                if (Auth::user()->roletype == 'AM') {
                    if ($result->status != '0') {
                        $status = $result->status. '<a href="suggested_detail-product-view/' . $pro_id . '/' . $notId . '/'.$result->status.'" style="display:none;"></a>';
                    } 
                    else {
                           
                           $status = $stres. '<a href="myposting-detail-view/' . $pro_id . '/' . $notId . '/'.$stres.'" style="display:none;"></a>';
                    }
                }
                if (Auth::user()->roletype == 'QF') {
                    $status = $result->status . '<a href="#" style="display:none;"></a>';
                }
                if (Auth::user()->roletype == 'TM') {
                    if ($result->status != '0') {
                        $status = $result->status . '<a href="detail_accp_offer/' . $result->productno . '/' . $result->notiid . '/' . $result->pro_id . '/' . $result->productid . '/' . $result->acc_offr_id . '/'.$result->status.'" style="display:none;"></a>';
                    } else {
                        $status = $result->status . '<a href="detail_accp_offer/' . $result->productno . '/' . $result->notiid . '/' . $result->pro_id . '/' . $result->productid . '/' . $result->acc_offr_id . '/'.$result->status.'" style="display:none;"></a>';
                    }
                }

                if(isset($result->product->name))
                    $productName = $result->product->name;
                else
                    $productName = '';
                
                if (!empty($productName)) {
                    $addmsg = $msg . ' "' . $productName . '"';
                } else {
                    if ($result->msg != '0') {
                        $addmsg = $msg;
                    } else {
                        $addmsg = "New Offer Recived";
                    }
                    
                }

                $suggestdate = $sdate;
                if (Auth::user()->roletype != 'TM') {
                    $userCheck = '<input type="checkbox" class="" name="checkUser" id="checkUser" value="' . $notId . '" />';
                 
                    $record[$i]['userCheck'] = $userCheck;
                    $record[$i]['suggestdate'] = $suggestdate;
                    $record[$i]['addmsg'] = $addmsg;
                    $record[$i]['status'] = $status;

                } else {
                    $userCheck = '<input type="checkbox" class="" name="checkUser" id="checkUser" value="' . $notId . '" />';
     
                    $record[$i]['userCheck'] = $userCheck;
                    $record[$i]['sdate'] = $sdate;
                    $record[$i]['accmsg'] = $accmsg;
                    $record[$i]['status'] = $status;

                }
                $i++;             
            }

            return \Response::json($record);
        }

    public function checkPermission($key){

        $data = \App\MastRolePermission::where('RoleId',Auth::user()->roleid)->where('PageId',$key)->get();
        return $data;
    }

    public function importData(){

        return View('setting.importData')->with('title','Ziki Trade::Import Data');
    }
    public function importDataStore(Request $request){

        $realName=$request->file('uploadFile');

    
        $rand = time() . '_' . uniqid();
        
        $fileName = time().'.'.$realName->getClientOriginalExtension();
        
        $path=public_path('importData'). '/'.$fileName;
         
        $realName->move(base_path() . '/public/importData/', $fileName);
        $rpath = 'public/importData/'.$fileName;
 
         if( $request->itype == 'Brands'){
            try {
                ini_set('max_execution_time', 500000);
                \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {
                    $industry_id = '';
                    foreach ($reader as $row1) {

                        $a = \App\Industry::where('name',$row1['industry'])->get();
                        if( count($a) > 0 ){
                            $industry_id = $a[0]->industryid;
                        }
                        else{
                            $b = new \App\Industry;
                            $b->name = $row1['industry'];
                            $b->save();

                            $industry_id = $b->industryid;
                        }

                          \App\Brand::insert([
                            'name'    => trim($row1["brand"]),
                            'industryid'     => trim($industry_id),
                          ]);



                    }
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = \Request::ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Imported Brands';

                    $useraction->save();
                });
                \Session::flash('success', 'Data uploaded successfully.');
                return \Redirect::to(url('setting/importData'))->withInput()->withErrors(["msg"=>"Data data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('setting/importData'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }
         }
         elseif( $request->itype == 'Industries'){
            try {
                ini_set('max_execution_time', 500000); 
                \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {
                    $industry_id = '';

                    foreach ($reader as $row1) {

                        $a = \App\Industry::where('name',$row1['industry'])->get();
                        if( count($a) == 0 ){
                            $b = new \App\Industry;
                            $b->name = $row1['industry'];
                            $b->save();
                        }
                         
                    }
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = \Request::ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Imported Industries';

                    $useraction->save();
                });
                \Session::flash('success', 'Data uploaded successfully.');
                return \Redirect::to(url('setting/importData'))->withInput()->withErrors(["msg"=>"Data data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('setting/importData'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }
         }
         elseif( $request->itype == 'Categories'){
            try {
                ini_set('max_execution_time', 500000); 
                \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {
                    $industry_id = '';

                    foreach ($reader as $row1) {

                        $a = \App\Industry::where('name',$row1['industry'])->get();
                        if( count($a) > 0 ){
                            $industry_id = $a[0]->industryid;
                        }
                        else{
                            $b = new \App\Industry;
                            $b->name = $row1['industry'];
                            $b->save();

                            $industry_id = $b->industryid;
                        }

                           $c_y = new \App\Category;
                           $c_y->name       = trim($row1["category"]);
                           $c_y->industryid = trim($industry_id);
                           $c_y->save();
                         
                    }
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = \Request::ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Imported Categories';

                    $useraction->save();
                });
                \Session::flash('success', 'Data uploaded successfully.');
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&import=successful'))->withInput()->withErrors(["msg"=>"Data data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }
         }
         elseif( $request->itype == 'Subcategories'){
            try {
                ini_set('max_execution_time', 500000); 
                \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {
                    $industry_id = '';
// echo "<pre>"; print_r($reader);die;
                    foreach ($reader as $row1) {

                        $a = \App\Industry::where('name',$row1['industry'])->get();
                        if( count($a) > 0 ){
                            $industry_id = $a[0]->industryid;
                        }
                        else{
                            $b = new \App\Industry;
                            $b->name = $row1['industry'];
                            $b->save();

                            $industry_id = $b->industryid;
                        }
                        $c = \App\Category::where('name',$row1['category'])->get();
                        
                        if( count( $c ) ){
                            $cat_id = $c[0]->catid;
                        }else{
                            $c_i = new \App\Category;
                            $c_i->name = $row1['category'];
                            $c_i->save();

                            $cat_id = $c_i->catid;
                        }

                        $d = \App\SubCategory::where('name',$row1['sub_category'])->get();

                        if( count( $d ) ){
                            continue;
                        }
                        else{
                            $d_i = new \App\SubCategory;
                            $d_i->catid = $cat_id;
                            $d_i->name = $row1['sub_category'];
                            $d_i->industryid = $industry_id;
                            $d_i->save();
                        }
                         
                    }
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = \Request::ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Imported SubCategories';

                    $useraction->save();
                });
                \Session::flash('success', 'Data uploaded successfully.');
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&import=successful'))->withInput()->withErrors(["msg"=>"Data data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }
         }
        elseif( $request->itype == 'Customers'){

            $i = 0;

            if( Auth::user()->roletype == 'AD' ){
                try {
                    ini_set('max_execution_time', 500000);
                    \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) use ($i) {
                        
                        foreach ($reader as $row1) {
                            
                            $u =  \App\User::where( 'userid',$row1['userid'] )->count();

                            if( $u != 0 ){

                                $pc = new \App\PostCustomer;
                                $pc->refno = $row1['customerrefno'];
                                $pc->edate = date('Y-m-d');
                                $pc->refuserid = $row1['userid'];
                                $pc->isactive = $row1['status'];
                                $pc->roletype = Auth::user()->roletype;
                                $pc->adduserid = Auth::user()->userid;

                                $pc->save();
                                
                                $useraction = new UserActivity;

                                $useraction->userid = Auth::user()->userid;
                                $useraction->ipaddress = \Request::ip();
                                $useraction->sessionid = Session::get('_token');
                                $useraction->actiondate = date('Y-m-d');
                                $useraction->actiontime = date('Y-m-d H:i:sa');
                                $useraction->actionname = 'Imported Customers';
                            
                                $useraction->save();
                                $i++;
                            }

                        }
                        
                    });
                    // if( $i > 0 ){
                        return \Redirect::to(url('setting/importData?itype='.$request->itype.'&import=successful'));
                    // }
                    // else{
                        // return \Redirect::to(url('setting/importData?itype='.$request->itype.'&getfile=Failed'));
                    // }
                    
                }
                catch (Exception $e) {
                    \Session::flash('error', $e->getMessage());
                    return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
                }
            }
            if( Auth::user()->roletype == 'AM' ){
                try {
                    ini_set('max_execution_time', 500000); 
                    \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {
                        foreach ($reader as $row1) {
                            $pc = new \App\PostCustomer;
                            $pc->refno = $row1['customer_ref_no'];
                            $pc->edate = date('Y-m-d');
                            $pc->refuserid = Auth::user()->userid;
                            $pc->isactive = $row1['status'];
                            $pc->roletype = Auth::user()->roletype;
                            $pc->adduserid = Auth::user()->userid;

                            $pc->save();


                            $useraction = new UserActivity;

                            $useraction->userid = Auth::user()->userid;
                            $useraction->ipaddress = \Request::ip();
                            $useraction->sessionid = Session::get('_token');
                            $useraction->actiondate = date('Y-m-d');
                            $useraction->actiontime = date('Y-m-d H:i:sa');
                            $useraction->actionname = 'Imported Customers';

                            $useraction->save();
                        }
                    });
                    return \Redirect::to(url('setting/importData?itype='.$request->itype.'&import=successful'));
                }

                catch (Exception $e) {
                    \Session::flash('error', $e->getMessage());
                    return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
                }
            
            }
        }
         elseif( $request->itype == 'Products'){
            try {
                ini_set('max_execution_time', 500000); 
                \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {

                    $i = 0;

                    foreach ($reader as $row1) {
                                        
                        $date = date('Y-m-d h:i:s');

                        if((Auth::user()->roletype == "AD") || (Auth::user()->roletype == "SA"))
                        {
                            $pstatus = "APPROVED";
                        }
                        else
                        {
                            $pstatus = "WAITING";
                        }

                        $code = $this->randStringAlpha(3) . '-' . $this->randStringNumeric(3) . '-' . $this->randStringNumeric(3);
                        if( ($row1['productname'] != '') || ( $row1['productname'] != null ) ){
                            $pro_exists = \App\MstProduct::where('name',$row1['productname'])->count();

                            $industry__id = \App\Industry::where('name',$row1['industry'])->lists('industryid')->toArray();

                            if( $pro_exists == 0 ){
                                if( !empty($industry__id) ){
                                    $in_id = $industry__id[0];    

                                    $category__id = \App\Category::where('name',$row1['category'])->where('industryid',$in_id)->lists('catid')->toArray();
                                    
                                    if( !empty($category__id) ){
                                        $ca_id = $category__id[0];
                                    
                                        $brand__id = \App\Brand::where('name',ucfirst(strtolower($row1['brand'])))->where('industryid',$in_id)->lists('brandid')->toArray();

                                        if( !empty($brand__id) ){
                                            $br_id = $brand__id[0];

                                            $subcategory__id = \App\SubCategory::where('name',$row1['sub_category'])->lists('subcatid')->toArray();

                                            if( !empty( $subcategory__id ) ){
                                                $su_id = $subcategory__id[0];
                                            }
                                            else{
                                                $su_id = '';
                                            }


                                            $pro = new \App\MstProduct;

                                            $pro->productno = $code;
                                            $pro->date = $date;
                                            $pro->name = $row1['productname'];
                                            $pro->industryid = $in_id;
                                            $pro->brandid = $br_id;
                                            $pro->catid = $ca_id;
                                            $pro->subcatid = $su_id;
                                            $pro->pakaging = $row1['packaging'];
                                            $pro->mpm = $row1['minprofitmargin'];
                                            $pro->shipingcondition = $row1['shippingcondition'];
                                            $pro->caseweight = $row1['caseweight'];
                                            $pro->qtypercase = $row1['quantitypercase'];
                                            $pro->description = $row1['description'];
                                            $pro->manufacture = $row1['manufacturer'];
                                            $pro->addedby = Auth::user()->userid;
                                            $pro->roletype = Auth::user()->roletype;
                                            $pro->pstatus = $pstatus;
                                            if( $row1['image1'] != '' ){
                                                $pro->img0 = $row1['image1'];
                                            }
                                            else{
                                                $pro->img0 = '';
                                            }

                                            if( $row1['image2'] != '' ){
                                                $pro->img1 = $row1['image2'];
                                            }
                                            else{
                                                $pro->img1 = '';
                                            }
                                            if( $row1['image3'] != '' ){
                                                $pro->img2 = $row1['image3'];
                                            }
                                            else{
                                                $pro->img2 = '';
                                            }

                                            if( $row1['image4'] != '' ){
                                                $pro->img3 = $row1['image4'];
                                            }
                                            else{
                                                $pro->img3 = '';
                                            }
                                            if( $row1['image5'] != '' ){
                                                $pro->img4 = $row1['image5'];
                                            }
                                            else{
                                                $pro->img4 = '';
                                            }

                                            if( $row1['image6'] != '' ){
                                                $pro->img5 = $row1['image6'];
                                            }
                                            else{
                                                $pro->img5 = '';
                                            }
                                            if( $row1['image7'] != '' ){
                                                $pro->img6 = $row1['image7'];
                                            }
                                            else{
                                                $pro->img6 = '';
                                            }
                                            if( $row1['image8'] != '' ){
                                                $pro->img7 = $row1['image8'];
                                            }
                                            else{
                                                $pro->img7 = '';
                                            }
                                            
                                            $pro->code1 = $row1['prefix1'];
                                            $pro->codevalue1 = $row1['productcode1'];
                                            
                                            if( ($row1['prefix2'] != '') && ($row1['productcode2'] != '') ){
                                                $pro->code2 = $row1['prefix2'];
                                                $pro->codevalue2 = $row1['productcode2'];
                                            }
                                            else{
                                                $pro->code2 = '';
                                                $pro->codevalue2 = '';
                                            }
                                            if( ($row1['prefix3'] != '') && ($row1['productcode3'] != '') ){
                                                $pro->code3 = $row1['prefix3'];
                                                $pro->codevalue3 = $row1['productcode3'];
                                            }
                                            else{
                                                $pro->code3 = '';
                                                $pro->codevalue3 = '';
                                            }
                                            
                                            $pro->save();
                                            
                                            $last_pro_id = $pro->productid;


                        

                                            $useridn = 'UUZZ-9984';
                                            $msgn = 'Product Sent For Approval';
                                            $typen = 'Audio';
                                            $statusn = 'Unread';


                                            $mNotification = new \App\MstNotification;

                                            $mNotification->pro_id = $last_pro_id;
                                            $mNotification->userid = $useridn;
                                            $mNotification->msgdate = $date;
                                            $mNotification->msg = $msgn;
                                            $mNotification->type = $typen;
                                            $mNotification->status = $statusn;
                                            $mNotification->fromuserid = Auth::user()->userid;

                                            $mNotification->save();

                                            $pro_image = \App\MstProductImage::where('pid',$last_pro_id)->delete();

                                            $pro_code = \App\MstProductCode::where('pid',$last_pro_id)->delete();

                                            $pro_prefix_count = \App\ProductPrefix::where('name',$row1['prefix1'])->get();
                                            $pro_prefix_count1 = \App\ProductPrefix::where('name',$row1['prefix2'])->get();
                                            $pro_prefix_count2 = \App\ProductPrefix::where('name',$row1['prefix3'])->get();
                                            
                                            if( ($last_pro_id > 0) && (count($pro_prefix_count) > 0) && $row1['productcode1'] != '' ){
                                                
                                                $p_code = new \App\MstProductCode;

                                                $p_code->pid = $last_pro_id;
                                                $p_code->prefixid = $pro_prefix_count[0]->proprefixid;
                                                $p_code->code = $row1['productcode1'];
                                                $p_code->status = $pstatus;
                                                $p_code->productcode = $row1['prefix1'] . ':' . $row1['productcode1'];
                                                $p_code->refdate = $date;
                                                $p_code->refbyid = Auth::user()->userid;

                                                $p_code->save();
                                            
                                                $mNotification1 = new \App\MstNotification;

                                                $mNotification1->pro_id = $last_pro_id;
                                                $mNotification1->userid = $useridn;
                                                $mNotification1->msgdate = $date;
                                                $mNotification1->msg = 'Product Code Sent For Approval';
                                                $mNotification1->type = $typen;
                                                $mNotification1->status = $statusn;
                                                $mNotification1->fromuserid = Auth::user()->userid;

                                                $mNotification1->save();


                                            }
                                            if( ($last_pro_id > 0) && (count($pro_prefix_count1) > 0) && $row1['productcode2'] != '' ){
                                                
                                                $p_code = new \App\MstProductCode;

                                                $p_code2->pid = $last_pro_id;
                                                $p_code2->prefixid = $pro_prefix_count[0]->proprefixid;
                                                $p_code2->code = $row1['productcode2'];
                                                $p_code2->status = $pstatus;
                                                $p_code2->productcode = $row1['prefix2'] . ':' . $row1['productcode2'];
                                                $p_code2->refdate = $date;
                                                $p_code2->refbyid = Auth::user()->userid;

                                                $p_code2->save();
                                            
                                                $mNotification3 = new \App\MstNotification;

                                                $mNotification3->pro_id = $last_pro_id;
                                                $mNotification3->userid = $useridn;
                                                $mNotification3->msgdate = $date;
                                                $mNotification3->msg = 'Product Code Sent For Approval';
                                                $mNotification3->type = $typen;
                                                $mNotification3->status = $statusn;
                                                $mNotification3->fromuserid = Auth::user()->userid;

                                                $mNotification3->save();


                                            }if( ($last_pro_id > 0) && (count($pro_prefix_count2) > 0) && $row1['productcode3'] != '' ){
                                                
                                                $p_code1 = new \App\MstProductCode;

                                                $p_code1->pid = $last_pro_id;
                                                $p_code1->prefixid = $pro_prefix_count[0]->proprefixid;
                                                $p_code1->code = $row1['productcode3'];
                                                $p_code1->status = $pstatus;
                                                $p_code1->productcode = $row1['prefix3'] . ':' . $row1['productcode3'];
                                                $p_code1->refdate = $date;
                                                $p_code1->refbyid = Auth::user()->userid;

                                                $p_code1->save();
                                            
                                                $mNotification2 = new \App\MstNotification;

                                                $mNotification2->pro_id = $last_pro_id;
                                                $mNotification2->userid = $useridn;
                                                $mNotification2->msgdate = $date;
                                                $mNotification2->msg = 'Product Code Sent For Approval';
                                                $mNotification2->type = $typen;
                                                $mNotification2->status = $statusn;
                                                $mNotification2->fromuserid = Auth::user()->userid;

                                                $mNotification1->save();


                                            }


                                            if( $row1['image1'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image1'];
                                                $pr_image->srno = '0';

                                                $pr_image->save();
                                            }
                                            if( $row1['image2'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image2'];
                                                $pr_image->srno = '1';

                                                $pr_image->save();
                                            }
                                            if( $row1['image3'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image3'];
                                                $pr_image->srno = '2';

                                                $pr_image->save();
                                            }
                                            if( $row1['image4'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image4'];
                                                $pr_image->srno = '3';

                                                $pr_image->save();
                                            }
                                            if( $row1['image5'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image5'];
                                                $pr_image->srno = '4';

                                                $pr_image->save();
                                            }
                                            if( $row1['image6'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image6'];
                                                $pr_image->srno = '5';

                                                $pr_image->save();
                                            }
                                            if( $row1['image7'] != '' ){

                                                $pr_image = new \App\MstProductImage;

                                                $pr_image->pid = $last_pro_id;
                                                $pr_image->imageurl = $row1['image7'];
                                                $pr_image->srno = '6';

                                                $pr_image->save();
                                            }

                                            $i++;

                                        }
                                    }
                                    
                                }
                            }    
                        }
                    }

                    if( $i > 0 ){
                        $useraction = new UserActivity;

                        $useraction->userid = Auth::user()->userid;
                        $useraction->ipaddress = \Request::ip();
                        $useraction->sessionid = Session::get('_token');
                        $useraction->actiondate = date('Y-m-d');
                        $useraction->actiontime = date('Y-m-d H:i:sa');
                        $useraction->actionname = 'Imported Products';

                        $useraction->save();
                    }

                });
                \Session::flash('success', 'Data uploaded successfully.');
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&import=successful'))->withInput()->withErrors(["msg"=>"Data data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }
         }

         elseif( $request->itype == 'Users'){
            try {
                ini_set('max_execution_time', 500000); 
                \Excel::filter('chunk')->load($rpath)->chunk(10000, function($reader) {
                    // $industry_id = '';
// echo "<pre>"; print_r($reader);die;
                    $i = 0;
                    foreach ($reader as $row1) {
                        
                        

                    }
                    
                    // $useraction = new UserActivity;

                    // $useraction->userid = Auth::user()->userid;
                    // $useraction->ipaddress = \Request::ip();
                    // $useraction->sessionid = Session::get('_token');
                    // $useraction->actiondate = date('Y-m-d');
                    // $useraction->actiontime = date('Y-m-d H:i:sa');
                    // $useraction->actionname = 'Imported Products';

                    // $useraction->save();

                });
                \Session::flash('success', 'Data uploaded successfully.');
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&import=successful'))->withInput()->withErrors(["msg"=>"Data data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('setting/importData?itype='.$request->input('itype').'&getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }
         }



    }
    public function sample($sample){

        return \Response::download(public_path() .'/sample/'.$sample. '.csv');
    }

    public function location(){
        
        $checkVal = $this->checkPermission(32);

        return View('setting.locations',['checkVal'=>$checkVal])->with('title','Ziki Trade::Locations'); 
    }
    public function location_ajax(Request $request){
        
        $checkVal=$this->checkPermission(32);

        switch ($request->Type) {

            case 'exist':
                $location = \App\MstLocation::where('name','=',$request->pname)->count();
                if($location > 0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $location = \App\MstLocation::where('name','=',$request->name)->count();
                if($location == 0)
                {
                    $location = new \App\MstLocation;

                    $location->name = $request->name;
                    $location->isactive = $request->status;
                    $location->save();
                    
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Location';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'ERROR'
                    );
                }
                return $arr;

              break;

            case 'search':

                    $location = \App\MstLocation::find($request->id);
                    if( count($location) > 0 ){
                        
                        $name = $location->name;
                        $stautus = $location->isactive;

                    }
                    $arr = array(
                            'name' => $name,
                            'status' => $stautus
                            );
                    return $arr;
                break;
            
            case 'update':
                
                $location = \App\MstLocation::where('name','=',$request->name)->count();
                
                if($location == 0){

                    $location = \App\MstLocation::find($request->id);

                    $location->name = $request->name;
                    // $location->isactive = $request->status;
                    $location->save();
                    
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Location';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return $arr;
                break;
            case 'delete':

                $location = \App\MstLocation::find($request->id);
                
                $location->delete();

                $arr = array(
                    'name' => 'Related Record Delete',
                    'msg' => 'A1'
                );
                
                $useraction = new UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Deleted Location';

                $useraction->save();
            
                return \Response::json($arr);
  
            break;

            default:
                $sql = \App\MstLocation::orderBy('name')->get();

                $record = array();
                $i = 0;

                foreach ($sql as $value) {
                    
                    $record[$i]['srno'] = ($i + 1);
                    $record[$i]['location'] = isset($value->name)?$value->name:'';

                    if ( Auth::user()->roletype == 'AD') {
                        $edit = '<a href="javascript:editme(\'' . $value->locationid . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;

                        <a  href="javascript:deleteme(\'' . $value->locationid . '\')"><i class="fa fa-trash-o"></i></a>';
                    
                    }
                    else{
                        if($checkVal[0]['EditP']=='1')
                        {
                            $edit1 = '<a href="javascript:editme(\'' . $value->locationid . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;';
                        }
                        if($checkVal[0]['DeleteP']=='1'){
                            $edit2 = '<a  href="javascript:deleteme(\'' . $value->locationid . '\')"><i class="fa fa-trash-o"></i></a>';
                        }
                            $edit=$edit1.$edit2;
                    }

                    $record[$i]['action'] = $edit;

                    $i++;    
                }

                return \Response::json($record);
                
            break;
        }

        

    }


    public function termsAndConditions(){

        $terms = \App\TermsCondition::where('id','1')->get();

        return View('setting.termsAndConditions',['terms'=>$terms])->with('title','Terms And Conditions');
    }

    public function termsAndConditions_ajax(Request $request){

        $terms = \App\TermsCondition::where('id','1')->update(['trems_condition'=>$request->trems_condition]);
        if( $terms ){
            return '1';    
        }
        

    }

    public function maskProductName(){
        return View('setting.maskProductName')->with('title','Ziki Trade::Mask Product Name');
    }

    public function maskProductName_ajax(Request $request){
        if( $request->Type == 'checkMaskQuotes' ){
            $maskProname = \App\MaskProname::find(1);

            $arr = array(
                    'id'=>$maskProname->id, 
                    'maskstatus'=>$maskProname->maskstatus
                );
             
            return $arr;    
        }
        if( $request->Type == 'maskQuotes' ){

            $maskVal=$request->maskVal;
            
            if($maskVal == '1')
            {   
                $statement1 = \App\MaskProname::where('id','1')->update(['maskstatus'=>$maskVal]);
                
                return "1";
            }
            else
            {
                $statement1 = \App\MaskProname::where('id','1')->update(['maskstatus'=>$maskVal]);

                return "0";
            }
        }
        

    }

    public function roleList(){
        $checkVal = $this->checkPermission(45);

        return View('setting.roleList')->with('title','Ziki Trade::Role List');
    }

    public function roleList_ajax(Request $request){
        
        $checkVal = $this->checkPermission(45);

        switch ($request->Type) {
            case 'add':
                
                $bname = $request->baserole;

                if($bname=='AD'){
                    $basename = 'Administrator';
                }elseif($bname=='AM'){
                    $basename = 'Account Manager';
                }elseif($bname=='QF'){
                    $basename = 'Quote Facilitator';
                }else{
                    $basename = 'Trigger Man';
                }
                $status = 1;

                $sql = \App\UserRole::where('rolename',$request->name)->get();

                if( count($sql) == 0 ){

                    $role = new \App\UserRole;

                    $role->rolename = $request->name;
                    $role->name = $basename;
                    $role->code = $bname;
                    $role->isactive = $status;

                    $role->save();

                    $arr = array(
                        'name' => 'Record Saved Sucessfully',
                        'msg' => 'SUCCESS'
                    );

                    return $arr;

                }
                else{
                    $arr = array(
                        'name' => 'Record Already Exists',
                        'msg' => 'EROOR'
                    );
                    return $arr;
                }

                break;
            
            case 'update':

                $name = $request->name;
                $status = $request->status;
                $bname = $request->baserole;
                if($bname=='AD'){
                    $basename = 'Administrator';
                }elseif($bname=='AM'){
                    $basename = 'Account Manager';
                }elseif($bname=='QF'){
                    $basename = 'Quote Facilitator';
                }else{
                    $basename = 'Trigger Man';
                }
                $id = $request->id;

                $sql = \App\UserRole::where('roleid',$id)->update(['rolename'=>$name,'name'=>$basename,'code'=>$bname,'isactive'=>$status]);

                $sql1 = \App\User::where('roleid',$id)->update(['roletype'=>$bname]);

                
                $arr = array(
                        'name' => 'update  Sucessfully',
                        'msg' => 'SUCCESS'
                    );

                return $arr;
                
                break;

            case 'delete':

                $sql = \App\User::where('roleid',$request->id)->get();

                if( count($sql) > 0 ){

                    foreach ($sql as $value) {
                        if (count($sql) > 0)
                        {
                            $arr = array(
                                'name' => 'Related Record Exists',
                                'msg' => 'ERROR'
                            );
                            return $arr;
                            
                        }
                        else
                        {   
                            $sql = \App\UserRole::where('roleid',$request->id)->delete();

                            $arr = array(
                                'name' => 'Related Record Delete',
                                'msg' => 'A2'
                            );
                            
                            return $arr;
                        }
                    }

                }
                else{
                    $sql = \App\UserRole::where('roleid',$request->id)->delete();

                    $arr = array(
                        'name' => 'Related Record Delete',
                        'msg' => 'A2'
                    );

                    return $arr;
                }

                break;

            case 'exist':
                    $sql = \App\UserRole::where('rolename',$request->pname)->count();

                    if( $sql > 0){
                        return 'exist';
                    }   
                    else{
                        return '1';
                    }
                break;

            case 'search':

                $sql = \App\UserRole::find($request->id);

                $arr = array(
                    'name' => $sql->rolename,
                    'baserole' => $sql->code,
                    'status' => $sql->isactive
                );

                return $arr;

                break;


            default:
                
                $sql = \App\UserRole::orderBy('rolename')->get();

                $record = array();
                $i = 0;

                foreach ($sql as $value) {
                    
                    $record[$i]['name'] = $value->rolename;
                    $record[$i]['basename'] = $value->name;

                    if ( Auth::user()->roletype == 'AD') {

                        $edit = '<a href="javascript:editme(\'' . $value->roleid . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $value->roleid . '\')"><i class="fa fa-trash-o"></i></a>';
                    
                    }
                    else{
                        if($checkVal[0]['EditP']=='1'){

                            $edit1 = '<a href="javascript:editme(\'' . $value->roleid . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;';
                        
                        }
                        
                        if($checkVal[0]['DeleteP']=='1'){
                        
                            $edit2 = '<a  href="javascript:deleteme(\'' . $value->roleid . '\')"><i class="fa fa-trash-o"></i></a>';
                        
                        }
                        
                        $edit=$edit1.$edit2;
                    }

                    $record[$i]['edit'] = $edit;

                    $i++;
                }

                return \Response::json($record);

                break;
        }

        
    }

    public function menuPermission(){
        
        $roles = \App\UserRole::where('isactive','1')->get();

        $mPages = \App\MastPage::where('Parent_Id','0')->get();


        return View('setting.menuPermission',['roles'=>$roles,'mPages'=>$mPages])->with('title','Ziki Trade::Menu Permission');
    }

    public function menuPermission_ajax(Request $request){
        
        $userRole = (isset($request->userRole)) ? $request->userRole : '';
        $userPage = (isset($request->userPage)) ? $request->userPage : '';
        
        $record = array();

        if( $userRole != '' || $userPage != '' ){

            $sql = \DB::select("SELECT *FROM (SELECT DisplayName,ViewP, AddP, EditP, DeleteP,pagePrint,pageExport, mrp.PageId FROM MastRolePermission mrp
            INNER JOIN MastPage mp ON mrp.pageid = mp.pageid WHERE mrp.roleid = '".$userRole."' AND parent_id = '".$userPage."' UNION ALL
            SELECT DisplayName, 0 AS ViewP, 0 AS AddP, 0 AS EditP, 0 AS DeleteP,0 AS pageExport,0 AS pageExport, PageId FROM MastPage
            WHERE PageId NOT IN ( SELECT pageid FROM MastRolePermission WHERE RoleId = '".$userRole."') AND Parent_Id = '".$userPage."') a ORDER BY DisplayName");    
            
            $i = 0;
            $j = 0;
            foreach ($sql as $row) {

                $ViewP="";$AddP="";$EditP="";$pageDelete="";$pagePrint="";$pageExport="";
                
                if ($row->ViewP==1) {$viewp="checked";} else { $viewp="";}
                if ($row->AddP==1) {$addp="checked";} else { $addp="";}
                if ($row->EditP==1) {$editp="checked";} else { $editp="";}
                if ($row->DeleteP==1) {$deletep="checked";} else { $deletep="";}
                if ($row->pagePrint==1) {$printp="checked";} else { $printp="";}
                if ($row->pageExport==1) {$exportp="checked";} else { $exportp="";}

                $i++;
                // $id=$row->Id;
                $pagename = '<input type="hidden" value="'.$row->PageId.'" name="pageid-'.$i.'" id="pageid-'.$i.'" />';
                $userRole = '<input type="hidden" value="'.$request->userRole.'" name="roleid" id="roleid" />';
                $userPageId = '<input type="hidden" value="'.$request->userPage.'" name="pageID" id="pageID" />';
                $pageName =$row->DisplayName.$pagename.$userRole.$userPageId;
                $view ='<input type="checkbox"' .$viewp.' id="viewp-'.$i.'" name="viewp-'.$i.'"/>';
                $add ='<input type="checkbox"' .$addp.' id="viewp-'.$i.'" name="addp-'.$i.'"/>';
                $edit ='<input type="checkbox"' .$editp.' id="viewp-'.$i.'" name="editp-'.$i.'"/>';
                $delete ='<input type="checkbox"' .$deletep.' id="viewp-'.$i.'" name="deletep-'.$i.'"/>';
                $print ='<input type="checkbox"' .$printp.' id="viewp-'.$i.'" name="printp-'.$i.'"/>';
                $export ='<input type="checkbox"' .$exportp.' id="viewp-'.$i.'" name="exportp-'.$i.'"/>';

                $record[$j]['pageName'] = $pageName;
                $record[$j]['view'] = $view;
                $record[$j]['add'] = $add;
                $record[$j]['edit'] = $edit;
                $record[$j]['delete'] = $delete;
                $record[$j]['print'] = $print;
                $record[$j]['export'] = $export;

                $j++;
            }

        }
        
        return \Response::json($record);
    }

    public function rolePermissionUpdate(Request $request){
        // echo "<pre>"; print_r($_POST);die;
        $pageID=$request->pageID;
        $roleid=$request->roleid;

        $sqlcnt = \App\MastPage::where('Parent_Id',$pageID)->count();
        
        for ($i = 1; $i <=$sqlcnt; $i++) {
            
            $sqlq = \App\MastRolePermission::where('RoleId',$roleid)->where('PageId',$_POST["pageid-". $i])->delete();

            if ((isset($_POST['viewp-'.$i])==true) || (isset($_POST['addp-'.$i])==true) || (isset($_POST['editp-'.$i])==true) || (isset($_POST['deletep-'.$i])==true)|| (isset($_POST['printp-'.$i])==true) || (isset($_POST['exportp-'.$i])==true)){

               $sql = new \App\MastRolePermission;

               $sql->RoleId = $request->roleid;
               $sql->PageId = $_POST["pageid-" . $i];
               $sql->ViewP = ((isset($_POST['viewp-' . $i]) == true) ? 1 : 0);
               $sql->AddP = ((isset($_POST['addp-' . $i]) == true) ? 1 : 0);
               $sql->EditP = ((isset($_POST['editp-' . $i]) == true) ? 1 : 0);
               $sql->DeleteP = ((isset($_POST['deletep-' . $i]) == true) ? 1 : 0);
               $sql->pagePrint = ((isset($_POST['printp-' . $i]) == true) ? 1 : 0);
               $sql->pageExport = ((isset($_POST['exportp-' . $i]) == true) ? 1 : 0);

               $sql->save();                
                
            }
            
        }

        return redirect(url('setting/menuPermission?update=success&roleid='.$roleid.'&pageID='.$pageID.''));

    }

    public function changeBanner(Request $request){
        
        if( $request->type == "DELETE" ){

            $image = \App\MstBanner::find($request->id);

            if($image->delete()){
                
                $useraction = new \App\UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = \Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Deleted Banner';

                $useraction->save();

                return 'success';    
            }
            
        }


        $banner = \App\MstBanner::all();

        return View('setting.changeBanner',['banner'=>$banner])->with('title','Ziki Trade:: Change Banner');
    
    }

    public function changeBannerAjax( Request $request ){

        if( isset($request->upban) && ($request->upban == 'Upload Banner')  ){

            $file = $request->file('uploadBanner');
            
            if(!empty($file)){

                $rand = time() . '_' . uniqid();

                $imageName = $rand.'.'.$file->getClientOriginalExtension();
                
                $path=public_path('media/bg'). '/'.$imageName;
                
                $file->move(base_path() . '/public/media/bg/', $imageName);

                $image = new \App\MstBanner;

                $image->name = $imageName;

                $image->save();

                $useraction = new \App\UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = \Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Uploaded Banner';

                $useraction->save();

            }
            return redirect('setting/changeBanner');
        }
    }

    public function changeNotifiles(Request $request){

        $files = \File::allFiles(public_path() . '/assets/notifiles/3');
        
        $man = array();

        foreach ($files as $value) {
            $man[] = pathinfo( $value );
        }

        return View('setting.notifiles',['man'=>$man])->with(['title','Ziki Trade::Change Noti Files']);
    }

    public function changeNotifilesAjax( Request $request ){

        if( isset($request->upnoti) && ($request->upnoti == 'Upload Noti') ){

            $file = $request->file('uploadnoti');
            
            if(!empty($file)){

                if($request->type == 'Action'){
                    $imageName = '0396.'.$file->getClientOriginalExtension();
                }
                elseif($request->type == 'Sign'){
                    $imageName = 'Soft_Velvet_Logo.'.$file->getClientOriginalExtension();
                }
                
                $path=public_path('assets/notifiles/3'). '/'.$imageName;
                
                if($file->move(base_path() . '/public/assets/notifiles/3/', $imageName)){
                    return redirect('setting/changeNotifiles?upload=success');
                }else{
                    return redirect('setting/changeNotifiles?upload=fail');
                }

                $useraction = new \App\UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = \Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Uploaded Noti File';

                $useraction->save();

            }
        }

        return redirect('setting/changeNotifiles');
    }

    function randStringAlpha($len) 
    {
        $str = "";
        $chars = array_merge(range('A', 'Z'));
        for($i=0;$i<$len;$i++) 
        {
            //reseed mt_rand
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float)$sec+((float)$usec*100000);
            mt_srand($seed);
            //append to string
            $str .= $chars[mt_rand(0, (count($chars)-1))];
        }
        return $str;  
    }

    //Random Numeric Generator 
    function randStringNumeric($len) 
    {
        $str = "";
        $chars = array_merge(range(0, 9));
        for($i=0;$i<$len;$i++) 
        {
            //reseed mt_rand
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float)$sec+((float)$usec*100000);
            mt_srand($seed);
            //append to string
            $str .= $chars[mt_rand(0, (count($chars)-1))];
        }
        return $str;  
    }

    public function genFun(Request $request){
        if( $request->input('Mode') == 'showCategory_ByIndustry' ){
            
            $industry_id = $request->input('industry_id');

            $itemData = \App\Category::where('industryid',$industry_id)->orderBy('name')->get();

            return \Response::json(compact('itemData'));
        }
        if( $request->input('Mode') == 'showSubCategory_byCatid' ){
            
            $cat_id = $request->input('cat_id');

            $itemData = \App\SubCategory::where('catid',$cat_id)->orderBy('name')->get();

            return \Response::json(compact('itemData'));
        }
    }

    public function cleardatabase(){

         $a = DB::table('mst_industry')->truncate();
         $b = DB::table('mst_category')->truncate();
         $c = DB::table('mst_subcategory')->truncate();
         $d = DB::table('mst_brand')->truncate();
         $e = DB::table('mst_product')->truncate();
         $f = DB::table('mst_productcode')->truncate();
         $g = DB::table('mst_productimages')->truncate();
         $h = DB::table('mst_productreport')->truncate();
         $i = DB::table('post_advertisment')->truncate();
         $j = DB::table('post_quotation')->truncate();
         $k = DB::table('tm_accpted_offers')->truncate();
         $l = DB::table('post_customer')->truncate();
         $m = DB::table('mst_notification')->truncate();


         return redirect('setting/dbbackup?drop=1');

    }
}