<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use App\Http\Requests\Auth\LoginRequest;

use App\MastRolePermission;
use Session;
use App\UserLogon;
use App\UserActivity;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $redirectAfterLogout = '?logout=success';
    // protected $guard = 'users';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'userid' => $data['username'],
            'password' => ($data['password']),
        ]);
    }

    protected function showLoginForm(){
        $image = \App\MstBanner::all();
        return View('auth/login',['image'=>$image])->with('title','Ziki Trade | Log in');
    }

    protected function login(LoginRequest $request){
        
        if (Auth::attempt(['userid'=>$request->username, 'password'=>$request->password])) {

            $permission = MastRolePermission::select('PageId')->where('RoleId',Auth::user()->roleid)->with('mastpage')->get();
        
            foreach ($permission as $value) {
                $module[] = $value->mastpage->Module;
                $displayname[] = $value->mastpage->DisplayName;
                $pagename[] = $value->mastpage->PageName;
            }

            session(['module'=>$module,'displayname'=>$displayname,'pagename'=>$pagename]);             
            
            $firsttime = \App\UserLogon::where('userid',Auth::user()->userid)->count();

            $userlogon = new UserLogon;

            $userlogon->userid = Auth::user()->userid;
            $userlogon->ipaddress = \Request::ip();
            $userlogon->starttime = date('Y-m-d H:i:sa');
            
            $userlogon->sessionid = Session::get('_token');
            $userlogon->logondate = date('Y-m-d');
            $userlogon->roletype = Auth::user()->roletype;
            $userlogon->save();

            $useraction = new UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = \Request::ip();
            $useraction->sessionid = Session::get('_token');
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Logged In';

            $useraction->save();

            if( $firsttime == 0 ){
                return redirect()->intended('setting/mySetting?page=ndashboard&s=loggedin');    
            }
            else{
                if( Auth::user()->roletype == 'TM' ){
                    return redirect()->intended('order/acceptedoffers?s=loggedin');
                }
                elseif( Auth::user()->roletype == 'QF' ){
                    return redirect()->intended('quoteQueue/quoteandpost?s=loggedin');
                }
                else{
                    return redirect()->intended('dashboard?page=ndashboard&s=loggedin');    
                }
                
            }


        }
 
        return redirect('login')->withErrors([
            'username' => 'The Username or the password is invalid. Please try again.',
        ]);
    }
}
