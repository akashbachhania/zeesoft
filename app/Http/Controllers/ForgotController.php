<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Crypt;
class ForgotController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
	{
	  $secquiz1 = '';
	  $secquiz2 = '';
	  $q1 = '';
	  $q2 = '';  
	  $t = time();
	  $str = substr($t, -1);


	  if( $request->input('fsubmit') == 'Next' ){
	  	    $errMsg  = '';
		    $match   = "";
		    $userid  = trim($request->input('uname'));

		    $records = \App\User::where('userid',$userid)->where('isactive','1')->get();

		    if( count($records) > 0 ){

		    	foreach ($records as $results) {
					
		    		$results1 = \App\MstSecurity::where('securityid',$results->secquiz1)->get();
		    		

		    		$results2 = \App\MstSecurity::where('securityid',$results->secquiz2)->get();
		    		

		    		$results3 = \App\MstSecurity::where('securityid',$results->secquiz3)->get();

	    		    if ($str == '0' || $str == '1') {
				        $secquiz1 = isset($results1[0]->name)?$results1[0]->name:'';
				        $secquiz2 = isset($results2[0]->name)?$results2[0]->name:'';
				        $q1    =1;
				        $q2    = 2;
				    } else if ($str == '2' || $str == '7') {
				        $secquiz1 = isset($results1[0]->name)?$results1[0]->name:'';
				        $secquiz2 = isset($results3[0]->name)?$results3[0]->name:'';
				        $q1    = 1;
				        $q2    = 3;
				    } else if ($str == '3' || $str == '8') {
				        $secquiz1 = isset($results2[0]->name)?$results2[0]->name:'';
				        $secquiz2 = isset($results1[0]->name)?$results1[0]->name:'';
				        $q1    =2;
				        $q2    = 1;;
				    } else if ($str == '4' || $str == '9') {
				        $secquiz1 = isset($results2[0]->name)?$results2[0]->name:'';
				        $secquiz2 = isset($results3[0]->name)?$results3[0]->name:'';
				        $q1    = 2;
				        $q2    = 3;
				    } else if ($str == '5') {
				        $secquiz1 = isset($results3[0]->name)?$results3[0]->name:'';
				        $secquiz2 = isset($results1[0]->name)?$results1[0]->name:'';
				        $q1    = 3;
				        $q2    = 1;
				    } else if ($str == '6') {
				        $secquiz1 = isset($results3[0]->name)?$results3[0]->name:'';
				        $secquiz2 = isset($results2[0]->name)?$results2[0]->name:'';
				        $q1    =3;
				        $q2    = 2;
				    }

		    	}
		    	
		    }
		    else {
		        return redirect('login?username=NA');
		    }

	  }

	  $image = \App\MstBanner::all();

	  $data = array(
	  			'secquiz1' => $secquiz1,
	  			'secquiz2' => $secquiz2,
	  			'q1'       => $q1,
	  			'q2'       => $q2,
	  			'image'    => $image,
	  			'userid'   => $userid
	  		);

      return View('user.forgotpwd',$data)->with('title','Ziki Trade::Forgot Password');
    }

    public function resetpassword(Request $request){

    	if( !empty($request->input('userid')) ){

    		$results4 = \App\User::where('secans'.$request->input('q1'),$request->input('answer1'))->where('secans'.$request->input('q2'),$request->input('answer2'))->where('userid',$request->input('userid'))->where('isactive','1')->count();

    		$image = \App\MstBanner::all();

    		if( $results4 > 0 ){
    			return View('user.newpwd',['userid'=>$request->input('userid'),'image'=>$image])->with('title','Ziki Trade::Change Password');
    		}	
    		else{
    			return redirect('forgot?incorrect=true');
    		}

    	}

    }

    public function changepassword(Request $request){

    	if( !empty($request->input('userid')) && !empty($request->input('npwd')) && !empty($request->input('cnpwd')) ){

    		$newpwd = \App\User::where('userid',$request->input('userid'))->update(['password'=>bcrypt($request->input('npwd')),'authorizedpass'=>bcrypt($request->input('cnpwd'))]);
    		if( $newpwd == 1 ){
	    		return redirect('login?resetpassword=true');
	    	}
	    	else{
	    		return redirect('login?resetpassword=false');
	    	}
    	}
    }
}