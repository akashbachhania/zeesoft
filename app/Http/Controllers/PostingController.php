<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Crypt;
use Auth;
use SSH;
use Session;
use \App\UserActivity;
class PostingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        
        if($request->ajax()){
            
            $result = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED'])->where('customerrefno',$request->postuserid)->orderBy('industry')->get();

            $record = array();
            $i = 0;

            foreach ($result as $value) {
                
                $sdate = date('F d Y - h:i A',strtotime($value->pdate));
                $postid = isset($value->postid)?$value->postid:'';
                $postno = isset($value->postno)?$value->postno:'';
                $brand = (isset($value->product->brand->name)?$value->product->brand->name:"") .'<a href="'.url("/").'/posting/posting-detail-view/' . $postno . '" style=display:none>View</a>';
                $ptype = strtoupper(isset($value->ptype)?$value->ptype:'');
                $quantity = isset($value->quantity)?$value->quantity:'';
                $productName = isset($value->product->name)?$value->product->name:'';
                $price = isset($value->targetprice)?:'';
                $currency = isset($value->currency)?$value->currency:'';
                $customerrefno = isset($value->customerrefno)?$value->customerrefno:'';
                $poststatus = isset($value->pstatus)?$value->pstatus:'';
                
                $useridstataus = isset($value->user->isactive)?$value->user->isactive:'';
                $act_inactive = isset($value->act_inactive)?$value->act_inactive:'';
                $productstatus = isset($value->product->pstatus)?$value->product->pstatus:'';
                $customerstatus = isset($value->postCustomer->isactive)?$value->postCustomer->isactive:'';


                $productCheck = '<input type="checkbox" class="form-control checkProduct" name="checkProduct" id="checkProduct" value="' . $postid . '" />';
                
                if ($poststatus == 'NEW'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="'.url('posting/edit-add').'/' . $postno . '">Edit</a>&nbsp;|&nbsp;
                         <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                         
                }
                if ($poststatus == 'Submitted'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                        <a href="'.url('posting/edit-add').'/' . $postno . '&key=13">Edit</a>&nbsp;|&nbsp;
                        <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                         
                }


                if ($poststatus == 'Countered'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                         <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                }


                if ($poststatus == 'Completed'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="'.url('posting/repost').'/' . $postno . '">Repost</a>';
                }
                if ($poststatus == 'EXPIRED'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="'.url('posting/repost').'/' . $postno . '">Repost</a>';
                }
                if ($poststatus == 'Declined'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="'.url('posting/repost').'/' . $postno . '">Repost</a>';
                }
                if ($poststatus == 'Accepted'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="'.url('posting/repost').'/' . $postno . '">Repost</a>';
                }

                if ($poststatus == 'CANCELLED'){

                    $edit = '<a href="'.url('posting/myposting-detail-view/').'/' .$postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="'.url('posting/repost').'/' . $postno . '">Repost</a>';
                }
                
                if(Auth::user()->roletype == "AD"){
                
                     if ( ($useridstataus=='1') && ($act_inactive=='0') && ($productstatus!='REJECTED') && ($customerstatus=='1') && ($poststatus !='EXPIRED')) {
                        $edit = '<span class="label label-sm label-success"> Enable </span>';
                    
                     } else {
                        
                        if($poststatus =='EXPIRED'){

                            $edit = '<span class="label label-sm label-danger">EXPIRED</span>';
                        }
                        else{

                            $edit = '<span class="label label-sm label-danger">Disable</span>';
                        }
                     }
                }

                if( Auth::user()->roletype == 'AD' ){
                    $record[$i]['postno'] = $postno;
                    $record[$i]['sdate'] = $sdate;
                    $record[$i]['brand'] = $brand;
                    $record[$i]['ptype'] = $ptype;
                    $record[$i]['quantity'] = $quantity;
                    $record[$i]['productName'] = $productName;
                    $record[$i]['price'] = $price;
                    $record[$i]['currency'] = $currency;
                    $record[$i]['customerrefno'] = $customerrefno;
                    $record[$i]['edit1'] = $edit;
                }
                else{
                    $record[$i]['postno'] = $postno;
                    $record[$i]['sdate'] = $sdate;
                    $record[$i]['brand'] = $brand;
                    $record[$i]['ptype'] = $ptype;
                    $record[$i]['quantity'] = $quantity;
                    $record[$i]['productName'] = $productName;
                    $record[$i]['price'] = $price;
                    $record[$i]['currency'] = $currency;
                    $record[$i]['customerrefno'] = $customerrefno;
                    $record[$i]['poststatus'] = $poststatus;
                    $record[$i]['edit1'] = $edit;
                }

                $i++;
            }//foreach loop end
            return \Response::json($record);
        }//ajax if condition end

        return View('posting.userposting')->with('title','Ziki Trade::Customer Postings');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {   
        if((Auth::user()->roletype == "AD")||(Auth::user()->roletype == "SA")){ 
            $color='#ffffff';
        }
        else{

            $color='#f9f9f9';
        }
        $currencies = \App\MstCurrency::orderBy('name')->get();
        $uoms = \App\MstUom::orderBy('name')->get();
        $locations = \App\MstLocation::orderBy('name')->get();
        $expdateranges = \App\MstExpDaterange::orderBy('name')->get();

        if(Auth::user()->userid=="UUZZ-9984" || Auth::user()->roletype == "SA"){
            $cusrefnos = \App\PostCustomer::where('isactive','1')->orderBy('refno')->get();
        }
        else{
            $cusrefnos = \App\PostCustomer::where('isactive','1')->where('refuserid',Auth::user()->userid)->orderBy('refno')->get();
        }

        $languages = \App\MstLanguage::orderBy('name')->get();
        $timeframes = \App\MstTimeframe::orderBy('name')->get();
        $countries = \App\MstCountry::orderBy('name')->get();

        $checkVal = $this->checkPermission();

        $data = array(
                'x'=>$color,
                'currencies'=>$currencies,
                'uoms'=> $uoms,
                'locations'=>$locations,
                'expdateranges'=>$expdateranges,
                'cusrefnos'=>$cusrefnos,
                'languages'=>$languages,
                'timeframes'=>$timeframes,
                'countries'=>$countries,
                'checkVal'=>$checkVal
            );

        return View('posting.create',$data)->with('title','Ziki Trade');

    // {
        //
          // return View('posting.addposting')->with('title','User Postings');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $realName=$request->file('uploadFile');

    
        $rand = time() . '_' . uniqid();
        
        $fileName = time().'.'.$realName->getClientOriginalExtension();
        
        $path=public_path('postingImport'). '/'.$fileName;
         
        $realName->move(base_path() . '/public/postingImport/', $fileName);
        $rpath = 'public/postingImport/'.$fileName;
 
            try {
                ini_set('max_execution_time', 2000); 
                \Excel::filter('chunk')->load($rpath)->chunk(300, function($reader) {

                    foreach ($reader as $row1) {

                        $seller ='';
                        $buyer='';

                        $ind_1 = \App\Industry::where('name',trim($row1['industry']))->lists('industryid')->toArray();
                        
                        if( !empty($ind_1) ){
                            $ind = $ind_1[0];
                        }
                        else{
                            $ind = 0;
                        }

                        $sesInd = explode(',', Auth::user()->industry);

                        $pro = \App\MstProduct::where('productno',trim($row1['productid']))->count();

                        if( Auth::user()->roletype == 'AD' ){
                            $pc = \App\PostCustomer::where('refno',trim($row1["customerrefno"]))->get();

                        }
                        else{
                            $pc = \App\PostCustomer::where('refno',trim($row1["customerrefno"]))->where('refuserid',Auth::user()->userid)->get();
                        }

                        $lct = \App\MstLocation::where('name',trim($row1["location"]))->count();

                        $valexits=0;
                        $postno=0;

                        while($valexits==0)
                        {
                            
                            $str = "";
                            
                            $chars = array_merge(range(0, 9));
                            
                            for($i=0;$i<6;$i++) 
                            {
                                list($usec, $sec) = explode(' ', microtime());
                                $seed = (float)$sec+((float)$usec*100000);
                                mt_srand($seed);
                                $str .= $chars[mt_rand(0, (count($chars)-1))];
                            }

                            $postno = $str;

                            $ch_post = \App\PostAdverstisement::where('postno',$postno)->count();

                            if($ch_post > 0)
                            {
                                $valexits=0;
                            }
                            else
                            {
                                $valexits=1;
                            }
                        }

                        if ( ($pro > 0) && (count($pc) > 0) && ($lct > 0) && (in_array($ind,$sesInd) ) ){

                            $str = substr($row1['packaginglanguage'], 1);
                            $str1 = substr($row1['country'], 1);
                            $language = str_replace('#', ',', $str);
                            $country = str_replace('#', ',', $str1);

                            if((trim($row1["type"])=='BUY')||(trim($row1["type"])=='Buy'))
                            {
                               $buyer =$pc[0]->refuserid;
                            }
                            
                            if((trim($row1["type"])=='SELL')||(trim($row1["type"])=='Sell'))
                            {
                                $seller = $pc[0]->refuserid;
                            }

                            $po = new \App\PostAdverstisement;

                            $po->productno    = trim($row1['productid']);
                            $po->industry     = trim($row1["industry"]);
                            $po->ptype        = trim($row1["type"]);
                            $po->targetprice  = trim($row1["targetprice"]);
                            $po->currency     = trim($row1["currency"]);
                            $po->uom          = trim($row1["unitofmeasurement"]);
                            $po->quantity     = trim($row1["quantity"]);
                            $po->location     = trim($row1["location"]);
                            $po->expdate      = trim($row1["expirydaterange"]);
                            $po->expirydate   = trim($row1["exactexpirydate"]);
                            $po->customerrefno= trim($row1["customerrefno"]);
                            $po->language     = '';
                            $po->timeframe    = trim($row1["timeframe"]);
                            $po->country      = $country;
                            $po->userid       = Auth::user()->userid;
                            $po->pdate        = date('Y-m-d H:i:sa');
                            $po->postno       = $postno;
                            $po->pakaging     = $language;
                            $po->pstatus      = 'NEW';
                            $po->buyer        = $buyer;
                            $po->seller       = $seller;

                            $po->save();
                        }                       
                      

                    }
                });
                \Session::flash('success', 'Postings uploaded successfully.');
                return \Redirect::to(url('posting/import-post?import=successful'))->withInput()->withErrors(["msg"=>"Posting data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('posting/import-post?getfile=Failed'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if((Auth::user()->roletype == "AD")||(Auth::user()->roletype == "SA")){ 
            $color='#ffffff';
        }
        else{

            $color='#f9f9f9';
        }
        $currencies = \App\MstCurrency::orderBy('name')->get();
        $uoms = \App\MstUom::orderBy('name')->get();
        $locations = \App\MstLocation::orderBy('name')->get();
        $expdateranges = \App\MstExpDaterange::orderBy('name')->get();

        if(Auth::user()->userid=="UUZZ-9984" || Auth::user()->roletype == "SA"){
            $cusrefnos = \App\PostCustomer::where('isactive','1')->orderBy('refno')->get();
        }
        else{
            $cusrefnos = \App\PostCustomer::where('isactive','1')->where('refuserid',Auth::user()->userid)->orderBy('refno')->get();
        }

        $languages = \App\MstLanguage::orderBy('name')->get();
        $timeframes = \App\MstTimeframe::orderBy('name')->get();
        $countries = \App\MstCountry::orderBy('name')->get();

        $checkVal = $this->checkPermission();
        $pa = \App\PostAdverstisement::where('postno',$id)->get();
        $p_a = $pa->toArray();
        $data = array(
                'x'=>$color,
                'currencies'=>$currencies,
                'uoms'=> $uoms,
                'locations'=>$locations,
                'expdateranges'=>$expdateranges,
                'cusrefnos'=>$cusrefnos,
                'languages'=>$languages,
                'timeframes'=>$timeframes,
                'countries'=>$countries,
                'checkVal'=>$checkVal,
                'pa'=>$p_a[0],
                'id'=>$id
            );

        return View('posting.edit_add',$data)->with('title','Ziki Trade:: Edit Add');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        if ($request->updateadd) {

            if (empty($request->datepicker)) {
                $datepicker = "";
            } else {
                $formdatepicker = $request->datepicker;
                $datepicker     = date("Y-m-d", strtotime($formdatepicker));
            }

            $langtemp         = '';
            $langname         = '';
            $countname       = '';
            $counttemp       = '';

            foreach ($request->addcountry as $value) {
                
                $cnty = \App\MstCountry::where('name',$value)->get();

                if (empty($counttemp)) {
                    $counttemp = $value;
                    $countname = $cnty[0]->name;
                } else {
                    $counttemp = $langtemp . ',' . $value;
                    $countname = $countname . ',' . $cnty[0]->name;
                }
            }



            foreach ($request->selectedlanguage as $values) {
                $lang = \App\MstLanguage::where('languageid',$values)->get();

                if (empty($langtemp)) {
                    $langtemp = $values;
                    $langname = $lang[0]->name;
                } else {
                    $langtemp = $langtemp . ',' . $values;
                    $langname = $langname . ',' . $lang[0]->name;
                }
            }

            $data = array(
                    'productno' =>$request->addprono,
                    'ptype' =>$request->addtype,
                    'currency' =>$request->addcurrency,
                    'uom' => $request->adduom,
                    'quantity' => $request->addqty,
                    'location' => $request->addlocation,
                    'expdate' => $request->addexpdrange,
                    'expirydate' =>$datepicker,
                    'customerrefno' =>$request->addcurrefno,
                    'pakaging' => $langname,
                    'language' => $langtemp,
                    'timeframe' => $request->addtimeframe,
                    'country' => $countname,
                    'targetprice' => $request->addprice,
                );

            //echo Auth::user()->userid;echo "  ";echo $request->postno1;
            $statement1 = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('postno',$request->postno)->update($data);
            //echo "<pre>";print_r($statement1);die;

            $useraction = new UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = Session::get('_token');
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Updated Posting';

            $useraction->save();
            
            return redirect('posting/myPosting?page=upposting');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postingCatalog(){
        $checkVal = $this->checkPermission();
        $timeframes = \App\MstTimeframe::all();
        return View('posting.postingCatalog',['checkVal'=>$checkVal,'timeframes'=>$timeframes])->with('title','Ziki Trade::Posting Catalog');
    }

    public function postingCatalog_ajax(Request $request){

        if( isset($request->statusActionType) && ($request->statusActionType == 'shatusChange')){
    
            $checkUser  = explode(',', str_ireplace('\'', '', $request->checkUser));
            $statusData = $request->statusData;
        
            if ($statusData == 'ENABLE' && $checkUser != '') {

                $result = \App\PostAdverstisement::whereIn('postid',$checkUser)->update(['act_inactive'=>'0']);

                $useraction = new UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Updated Posting';

                $useraction->save();
            
            }
            if ($statusData == 'DISABLE' && $checkUser != '') {
                
                $result = \App\PostAdverstisement::whereIn('postid',$checkUser)->update(['act_inactive'=>'1']);
                
                $useraction = new UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Updated Posting';

                $useraction->save();
                
            }
            if ($statusData == 'DELETE' && $checkUser != '') {
                
                $result = \App\PostAdverstisement::whereIn('postid',$checkUser)->delete();
                
                $useraction = new UserActivity;

                $useraction->userid = Auth::user()->userid;
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Deleted Posting';

                $useraction->save();

            }

        }   
            $industryid = array();

            if( empty(Auth::user()->industry) ){
                $industryid = \App\Industry::where('isactive','1')->lists('industryid')->toArray();
            }
            else{
                $industryid = explode(',', Auth::user()->industry);
            }


        if (isset($request->productActionType) && $request->productActionType == "group_filter") {
            
            if (Auth::user()->roletype != "AD") {

                $result = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($q) use ($industryid){
                        $q->where('pstatus','APPROVED')->whereIn('industryid',$industryid);
                    })->whereHas('postCustomer',function($q){
                        $q->where('isactive','1');
                    })->whereHas('user',function($q){
                        $q->where('isactive','1');
                    });

                    if( empty($request->input('industryData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                               $q->whereIn( 'industryid', explode(',', $request->input('industryData')) );
                        });
                    }
                    if( empty($request->input('categoryData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                            $q->whereIn( 'catid', explode(',', $request->input('categoryData')) );
                        });
                    }
                    if( empty($request->input('scategoryData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                            $q->whereIn( 'subcatid', explode(',', $request->input('scategoryData')) );
                        });
                    }
                    if( empty($request->input('brandData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                            $q->whereIn( 'brandid', explode(',', $request->input('brandData')) );
                        });
                    }

                    if( empty($request->input('type')) == false ){
                        $result = $result->where('ptype',$request->input('type'));
                    }

                    if( empty($request->input('timeframe')) == false ){
                        $result = $result->where('timeframe',$request->input('timeframe'));
                    }

                    $result = $result->get();

            } else {

                $result = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED']);

                    if( empty($request->input('industryData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                               $q->whereIn( 'industryid', explode(',', $request->input('industryData')) );
                        });
                    }
                    if( empty($request->input('categoryData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                            $q->whereIn( 'catid', explode(',', $request->input('categoryData')) );
                        });
                    }
                    if( empty($request->input('scategoryData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                            $q->whereIn( 'subcatid', explode(',', $request->input('scategoryData')) );
                        });
                    }
                    if( empty($request->input('brandData')) == false ){
                        $result = $result->whereHas('product',function($q) use ($request){
                            $q->whereIn( 'brandid', explode(',', $request->input('brandData')) );
                        });
                    }

                    if( empty($request->input('type')) == false ){
                        $result = $result->where('ptype',$request->input('type'));
                    }

                    if( empty($request->input('timeframe')) == false ){
                        $result = $result->where('timeframe',$request->input('timeframe'));
                    }

                    $result = $result->get();

            }
            
        } else {

            if( Auth::user()->roletype != "AD" ){
                $result = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($q){
                        $q->where('pstatus','APPROVED');
                    })->whereHas('postCustomer',function($q){
                        $q->where('isactive','1');
                    })->whereHas('user',function($q){
                        $q->where('isactive','1');
                    });        
            
                if (empty(Auth::user()->industry) == false) {
                    $industry1  = explode(',', Auth::user()->industry);
                    
                   $result =  $result->whereHas('product',function($q) use ($industry1){
                            $q->whereIn('industryid',$industry1);
                        });
                    
                }
                
                if (empty(Auth::user()->category) == false) {
                    $category1  = explode(',', Auth::user()->category);
                    
                   $result =  $result->whereHas('product',function($q) use ($category1){
                            $q->whereIn('catid',$category1);
                        });
                }

                $result = $result->get();
            }
            else{

                $result = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED'])->whereHas('product',function($q) use ($industryid){
                        $q->whereIn('industryid',$industryid);
                    })->get();                

            }
        }
                

        
        
        $i = 0;

        $record = array();
        foreach ($result as $value) {

            $enableaction = '';
            if ( (isset($value->user->isactive) && ($value->user->isactive =='1')) && 
                 ( isset($value->act_inactive) && ($value->act_inactive=='0')) && 
                 ( isset($value->product->pstatus) && ($value->product->pstatus!='REJECTED')) && 
                 ( isset($value->postCustomer->isactive) && ($value->postCustomer->isactive=='1')) && 
                 ( isset($value->pstatus) && ($value->pstatus !='EXPIRED') ) ) {
                
                $enableaction .= '<span class="label label-sm label-success"> Enable </span>';
            
            } else {
                if(isset($value->pstatus) && ($value->pstatus =='EXPIRED'))
                {
                $enableaction .= '<span class="label label-sm label-danger">EXPIRED</span>';
                }
                else
                {
                $enableaction .= '<span class="label label-sm label-danger">Disable</span>';
                }
            }
            $postnumber = isset($value->postno)?$value->postno:"";
            if ($value->userid != Auth::user()->userid && Auth::user()->roletype == "AM") {

                if(( isset($value->product->pstatus) && ($value->product->pstatus!='Accepted')) && 
                    ( isset($value->product->pstatus) && ($value->product->pstatus!='Declined'))){

                    if (Auth::user()->roletype == "AD") {

                            $record[$i]['productCheck'] = '<input type="checkbox" class="checkProduct" name="checkProduct" id="checkProduct" value="' . isset($value->postid)?$value->postid:'' . '" />';

                            $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber.'">'.$postnumber.'</a>';

                            $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));

                            $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                            $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                            $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                            $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';

                            $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?:'', "HTML-ENTITIES", "ISO-8859-1");

                            $record[$i]['enableaction'] = $enableaction;

                    } else {
                        if ($value->act_inactive == 0) {

                            $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber. '">'.$postnumber.'</a>';

                            $record[$i]['sdate'] = date('F d Y - h:i A',strtotime( isset($value->pdate)?$value->pdate:'' ));

                            $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                            $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                            $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                            $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';

                            $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?$value->timeframe:'', "HTML-ENTITIES", "ISO-8859-1");;
                         }
                    }
                }
                
            }
             else {
                if (Auth::user()->roletype == "AD") {
                    $record[$i]['productCheck'] = '<input type="checkbox" class="checkProduct" name="checkProduct" id="checkProduct" value="' . $value->postid . '" />';

                    $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber. '">'.$postnumber.'</a>';

                    $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));

                    $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                    $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                    $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                    $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                    $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                    $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';

                    $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?$value->timeframe:'', "HTML-ENTITIES", "ISO-8859-1");

                    $record[$i]['enableaction'] = $enableaction;

                } else {
                    if ($value->act_inactive == 0) {

                        
                        $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber. '">'.$postnumber.'</a>';

                        $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));

                        $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                        $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                        $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                        $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                        $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                        $record[$i]['productName'] = isset($value->product->name)?$value->product->name:"";

                        $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?$value->timeframe:'', "HTML-ENTITIES", "ISO-8859-1");


                    }
                }
            }     
            
            $i++;
        }
        return \Response::json($record);
    }


    public function importPost(){

        return View('posting.importPost')->with('title','Ziki Trade::Import Post');
    }

    public function myOffers(){
        return View('posting.myOffers')->with('title','Ziki Trade::My Offers');
    }

    public function myOffersAjax(){
        $result = \App\PostQuotation::where('userid',Auth::user()->userid)->where('type','OFFER')->whereHas('product',function($query){
            $query->where('pstatus','APPROVED');
        })->get();

        $record = array();
        $i = 0;
        foreach ($result as $value) {

            $row = \App\QuotationCounter::where('quotationno',$value->quotationno)->get();

            if( count($row) == 0){


                $qtypercase    = isset($value->quantity)?$value->quantity:'';
                $offerPrice    = isset($value->price)?$value->price:'';
                $offertimeframe  = isset($value->timeframe)?$value->timeframe:'';
                $expdate      = isset($value->expdate)?$value->expdate:'';
                $quationstatus = isset($value->quationstatus)?$value->quationstatus:'';
            

            }
            else{
                foreach ($row as $values) {

                    if(isset($values->quotationno) && ($values->quotationno == '')){

                        $qtypercase    = isset($value->quantity)?$value->quantity:'';
                        $offerPrice    = isset($value->price)?$value->price:'';
                        $offertimeframe  = isset($value->timeframe)?$value->timeframe:'';
                        $expdate      = isset($value->expdate)?$value->expdate:'';
                        $quationstatus = isset($value->quationstatus)?$value->quationstatus:'';
                    
                    }
                    else{

                        $qtypercase    = isset($values->counter_quantity)?$values->counter_quantity:'';
                        $offerPrice    = isset($values->counter_price)?$values->counter_price:'';
                        $offertimeframe= isset($values->counter_timeframe)?$values->counter_timeframe:'';
                        $expdate       = isset($values->counter_expdate)?$values->counter_expdate:'';
                        $quationstatus = isset($values->counetr_status)?$values->counetr_status:'';
                    }

                }    
            }
                
                

            $record[$i]['postno'] = isset($value->postno)?$value->postno:'';
            
            $record[$i]['quotationno'] = '<a href="'.url("posting/myOfferDetail").'/' . (isset($value->postno)?$value->postno:'') . '/'.(isset($value->quotationno)?$value->quotationno:'').'" style="display:none;">' . (isset($value->quotationno)?$value->quotationno:''). '</a>'. (isset($value->quotationno)?$value->quotationno:'');
            
            $record[$i]['offerdate'] = date('F d Y - h:i A',strtotime(isset($value->offercrdate)?$value->offercrdate:'') ) .' EST';
            
            $record[$i]['IndustryName'] = isset($value->product->industry->name)?$value->product->industry->name:'';
            
            $record[$i]['brandName'] = isset($value->product->brand->name)?$value->product->brand->name:'';
            
            $record[$i]['producttype'] = isset($value->postAdverstisment->ptype) ? $value->postAdverstisment->ptype : '';
            
            $record[$i]['qtypercase'] = isset($qtypercase) ? $qtypercase : '';
            
            $record[$i]['productName'] = mb_convert_encoding(isset($value->product->name)?$value->product->name:'', "HTML-ENTITIES", "utf8");
            
            $record[$i]['offerPrice'] = isset($offerPrice) ? $offerPrice : '';
            
            $record[$i]['offercurrency'] = isset($value->currency)?$value->currency:'';
            
            $record[$i]['offertimeframe'] = isset($offertimeframe) ? $offertimeframe :'';
            
            $record[$i]['expdate'] = isset($expdate) ? $expdate : '';
            
            $record[$i]['quationstatus'] = isset($quationstatus) ? $quationstatus : '';         

            $i++;
        }

        return \Response::json($record);
    }

    public function myOfferDetailView(Request $request){
        $postStatus = $this->mypoststatus($request->post_id);

        $ml = \App\MstLanguage::orderBy('name')->get();

        $currencies = \App\MstCurrency::orderBy('name')->get();

        $uom = \App\MstUom::orderBy('name')->get();
        
        $locations = \App\MstLocation::orderBy('name')->get();
 
        $expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

        $timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

        $countries = \App\MstCountry::orderBy('name')->get();

        
        if((Auth::user()->roletype =="AD") || ( Auth::user()->roletype == "SA") ){
            $x='style="background:#ffffff"';
        }
        else{
            $x='style="background:#f9f9f9"';
        }

        $data = array(
            'postStatus' => $postStatus,
            'x' => $x,
            'ml' => $ml,
            'currencies'=>$currencies,
            'uom'=>$uom,
            'locations'=> $locations,
            'expdateranges' => $expdateranges,
            'timeframes' => $timeframes,
            'countries' => $countries
         );
        return View('posting.myOfferView',$data)->with('Ziki Trade::My Offer Views');
    }

    public function checkPermission(){

        $data = \App\MastRolePermission::select('RoleId','ViewP','AddP','EditP','DeleteP')->where('RoleId',Auth::user()->roleid)->where('PageId','12')->get();
        return $data;
    }    

    public function search( Request $request ){
            $q = $request->search;
            
            if (empty(Auth::user()->industry) == false) {
                $industry1  = explode(',', Auth::user()->industry);
            }
            
            if (empty(Auth::user()->category) == false) {
                $category1  = explode(',', Auth::user()->category);
            }

            $pc = \App\MstProductCode::where('productcode','LIKE','%'.$q.'%')->lists('pid')->toArray();
            
            if (!empty(Auth::user()->industry)) {
                  
              $sth = \App\MstProduct::where('pstatus','=','APPROVED')->whereIn('industryid',$industry1)->where('name','LIKE','%'.$q.'%')->orWhereIn('productid',$pc)->orderBy('productid')->limit('5')->get();                    

            } 
            else {
                  
               $sth = \App\MstProduct::where('pstatus','=','APPROVED')->where('name','LIKE','%'.$q.'%')->orWhereIn('productid',$pc)->orderBy('productid')->limit('5')->get();
            }

            foreach ($sth as $value) {
                $productid = $value->productid;
                
                $username = $value->name;
                
                $productcode = '';
                
                foreach ($value->productCode as $prc) {
                    $productcode .= $prc->productcode; 
                }



                 $username = str_replace('"', '', $username);
                $username = str_replace('\'', '', $username);
                $username = mb_convert_encoding($username, "HTML-ENTITIES", "ISO-8859-1");
                $final    = $productid . ':' . $username;
                $b_username = '<strong style="color:blue;">' . $q . '</strong>';
                $final_username = str_ireplace($q, $b_username, $username);
                $finalproductcode =  str_ireplace($q, $b_username, $productcode);
                ?>
                <div class="show" align="left" onclick="hello('<?php echo $final; ?>')";>
<?php
                echo '<span class="name">'. $final_username .'&nbsp;&nbsp;('. $finalproductcode .')</span>';

                echo '</div>'; 

            }


    }


    public function fetch_productdetail(Request $request){
        $id = $request->pro_id;
        
        $sql_result = \App\MstProduct::where('productid',$id)->get();

        foreach ($sql_result as $value) {

            $productno = $value->productno;
            $name = mb_convert_encoding($value->name, "HTML-ENTITIES", "utf8");
            $industryid = (isset($value->industry->name) && ($value->industry->name !='') )?$value->industry->name:'Not Applicable';
            $catid = (isset($value->category->name) && ($value->category->name !='') )?$value->category->name:'Not Applicable';
            $subcatid = (isset($value->subcategory->name) && ($value->subcategory->name !='') )?$value->subcategory->name:'Not Applicable';
            $brandid = (isset($value->brand->name) && ($value->brand->name !='') )?$value->brand->name:'Not Applicable';
            $manufacture = $value->manufacture;
            $caseweight = $value->caseweight;
            $qtypercase = $value->qtypercase;
            $pakaging = $value->pakaging;
            $shipingcondition = $value->shipingcondition;
            $description = mb_convert_encoding($value->description, "HTML-ENTITIES", "utf8");

            if((Auth::user()->roletype=="AD")||(Auth::user()->roletype=="SA")){

                $result = $value->productCode;

                $productcodes = '';
                
                foreach ($result as $values) {
                    
                    $productcodes .= $values->productcode;

                }
            }
            else{

                $result = $value->productCode;

                $productcodes = '';
                
                foreach ($result as $values) {
                    
                    $productcodes .= $values->productcode;

                }
            }

                $results=count($value->productImage);
                // echo "<pre>"; print_r($value->productImage);die;
                 if($results!=0)
                 {
                    if (strpos($value->productImage[0]->imageurl,'http://') !== false)
                    {
                       
                       $images = "<div class='BigImage'><img src='<?php echo $productimage[0]['imageurl'] ?>' alt='' width='258px'></div>";
                       
                    }
                    else
                    {
                        
                        $images = "<div class='BigImage'><div class='MagicZoom' id='image'><div class='MagicZoom'><a href='".url('/')."/productimg/".$value->productImage[0]->imageurl."' id='imageshref' class='MagicZoom' rel='zoom-position:inner;zoom-fade:true'><img id='bimg' src='".url('/')."/productimg/thumb/".$value->productImage[0]->imageurl ."' alt='' width='300'></a></div></div></div>";
                       
                    }
                  }
                  else
                  {
                      
                      $images = "<div class='BigImage'><img src=".public_path()."'/productimg/image4.jpg' alt='' width='258px'></div>";
                      
                  }
        }

        $arr = array(
            'productno' => $productno,
            'name' => $name,
            'industry' => $industryid,
            'category' => $catid,
            'subcategory' => $subcatid,
            'brand' => $brandid,
            'manufacture' => $manufacture,
            'caseweight' => $caseweight,
            'qtypercase' => $qtypercase,
            'pakaging' => $pakaging,
            'shipingcondition' => $shipingcondition,
            'productcodes' => $productcodes,
            'description' => $description,
            'image'=>$images
        );

        return $arr;
    }

    public function createPosting(Request $request){

        if ($request->createadd) {

            $postAdverstisement = new \App\PostAdverstisement;

            $postAdverstisement->userid = Auth::user()->userid;
            $postAdverstisement->pdate = date('Y-m-d H:i:sa');
            $postAdverstisement->productno = $request->addprono;
            $postAdverstisement->ptype = $request->addtype;
            $postAdverstisement->currency = $request->addcurrency;
            $postAdverstisement->uom = $request->adduom;
            $postAdverstisement->quantity = $request->addqty;
            $postAdverstisement->location = $request->addlocation;
            $postAdverstisement->expdate = $request->addexpdrange;
            
            if (empty($request->datepicker)) {
                $datepicker = "";
            } else {
                $formdatepicker = $request->datepicker;
                $datepicker     = date("Y-m-d", strtotime($formdatepicker));
            }

            $postAdverstisement->expirydate = $datepicker;
            $postAdverstisement->customerrefno = $request->addcurrefno;

            $postAdverstisement->timeframe = $request->addtimeframe;
            
            $langtemp         = '';
            $langname         = '';
            $countname       = '';
            $counttemp       = '';

            foreach ($request->addcountry as $value) {
                
                $cnty = \App\MstCountry::where('name',$value)->get();

                if (empty($counttemp)) {
                    $counttemp = $value;
                    $countname = isset($cnty[0])?$cnty[0]->name:'';
                } else {
                    $counttemp = $langtemp . ',' . $value;
                    $countname = $countname . ',' . isset($cnty[0])?$cnty[0]->name:'';
                }
            }



            foreach ($request->selectedlanguage as $values) {
                $lang = \App\MstLanguage::where('languageid',$values)->get();

                if (empty($langtemp)) {
                    $langtemp = $values;
                    $langname = $lang[0]->name;
                } else {
                    $langtemp = $langtemp . ',' . $values;
                    $langname = $langname . ',' . $lang[0]->name;
                }
            }
            $postAdverstisement->pakaging = $langname;
            $postAdverstisement->language = $langtemp;
            $postAdverstisement->country = $countname;
            $postAdverstisement->pstatus = "NEW";
            $postAdverstisement->targetprice = $request->addprice;
            $postAdverstisement->postno = $this->checkduplicy();
            
            $seller ='';
            $buyer='';
            if($request->addtype=='BUY'){

               $buyer = Auth::user()->userid;
            }
            else{

               $seller = Auth::user()->userid;
            }

            $postAdverstisement->buyer = $buyer;
            $postAdverstisement->seller = $seller; 
            

            $postAdverstisement->save();

            $useraction = new \App\UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Added Posting';

            $useraction->save();
            
            return redirect('posting/myPosting?page=addpost');            
            // header("location:my-posting.php?page=addpost&key=13");
        }
    }

    function randStringNumeric22($len=6){

        $str   = "";
        
        $chars = array_merge(range(0, 9));
        
        for ($i = 0; $i < $len; $i++) {
        
            list($usec, $sec) = explode(' ', microtime());
        
            $seed = (float) $sec + ((float) $usec * 100000);
        
            mt_srand($seed);
        
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        
        }
        
        return $str;
    
    }

    function randStringAlpha2($len = 1){

        $str   = "";
        $chars = array_merge(range('A', 'Z'));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
            
    function checkduplicy(){

        $postno= $this->randStringNumeric22();

        $sth = \App\PostAdverstisement::where('postno',$postno)->get();

        $r1 = count( $sth );

        if($r1>0)
        {
           $this->checkduplicy();
        }
        else
        {
          return $postno;
        }
    }

    public function postingDetailView($id){

        if((Auth::user()->roletype =="AD") || ( Auth::user()->roletype == "SA") ){
            $x='style="background:#ffffff"';
        }
        else{
            $x='style="background:#f9f9f9"';
        }

        $postStatus = \App\PostAdverstisement::where('postno',$id)->where('act_inactive','0')->whereNotIn('pstatus',['COMPLETED','CANCELLED','EXPIRED'])->whereHas('postCustomer',function($query){
                $query->where('isactive','1');
            })->whereHas('user',function($query){
                $query->where('isactive','1');
            })->whereHas('product',function($query){
                $query->whereIn('pstatus',['APPROVED','PROCESSING','WAITING']);
            })->count();

        $postingStatus = \App\PostAdverstisement::where('postno',$id)->whereIn('pstatus',['EXPIRED'])->count();


        $checkVal = $this->checkPermission();

        $ml = \App\MstLanguage::orderBy('name')->get();

        $currencies = \App\MstCurrency::orderBy('name')->get();

        $uom = \App\MstUom::orderBy('name')->get();
        
        $locations = \App\MstLocation::orderBy('name')->get();
 
        $expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

        $timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

        $countries = \App\MstCountry::orderBy('name')->get();

        $data = array(
                'postStatus' => $postStatus,
                'postingStatus'=> $postingStatus,
                'checkVal'=>$checkVal,
                'x' => $x,
                'ml' => $ml,
                'currencies'=>$currencies,
                'uom'=>$uom,
                'locations'=> $locations,
                'expdateranges' => $expdateranges,
                'timeframes' => $timeframes,
                'countries' => $countries
             );

        return View('posting.detailView',$data)->with('title','Ziki Trade:: Posting Detail View');
    }

    public function detailViewDelete(Request $request){

        if($request->action == 'delete'){

            $sqlcnt = \App\PostQuotation::where( 'postno',$request->postno )->count();
    
            if ($sqlcnt > 0) {

                echo "under process";
                // header("location:posting-catalog.php?error=Posting is under process.");
            } else {
                
                $pa = \App\PostAdverstisement::where('postno',$request->postno)->delete();
                echo 'done';
                // return redirect('posting/postingCatalog?v=deleted');
                // header("location:posting-catalog.php?success=Deleted successfully.");
            }
        }
        if($_REQUEST["action"]=='enableaction')
        {
            $pa = \App\PostAdverstisement::where('postno',$request->postno)->update(['act_inactive'=>0]);
 
            echo "1";

        }
        if($_REQUEST["action"]=='disableaction')
        {
            $pa = \App\PostAdverstisement::where('postno',$request->postno)->update(['act_inactive'=>1]);

            echo "1";

        }
    }

    public function fetchFunction(Request $request){

        if ($request->type == 'fetchallposts') {
            
            $postno = $request->postno;
            
            $result = \App\PostAdverstisement::where('postno',$postno)->get();

            foreach ($result as $value) {
                
                $postid           = isset($value->postid)?$value->postid:'';
                $userid           = isset($value->userid)?$value->userid:'';
                $ptype            = ucwords(strtolower(isset($value->ptype)?$value->ptype:''));  
                $currency         = isset($value->currency)?$value->currency:'';
                $productno        = isset($value->productno)?$value->productno:'';          
                $uom              = isset($value->uom)?$value->uom:'';
                $quantity         = isset($value->quantity)?$value->quantity:'';           
                $location         = isset($value->location)?$value->location:'';
                $expdate          = isset($value->expdate)?$value->expdate:'';            
                $expirydate       = isset($value->expirydate)?$value->expirydate:'';
                $customerrefno    = isset($value->customerrefno)?$value->customerrefno:'';      
                $pakaging         = isset($value->product->pakaging)?$value->product->pakaging:'';
                $language         = isset($value->language)?$value->language:'';           
                $name             = mb_convert_encoding(isset($value->product->name)?$value->product->name:'', "HTML-ENTITIES", "utf8");
                $industryid       = isset($value->product->industryid)?$value->product->industryid:'';         
                $brandid          = isset($value->product->brandid)?$value->product->brandid:'';
                $catid            = isset($value->product->catid)?$value->product->catid:'';              
                $subcatid         = isset($value->product->subcatid)?$value->product->subcatid:'';
                // $pakaging         = $value->pakaging;           
                $shipingcondition = isset($value->product->shipingcondition)?$value->product->shipingcondition:'';
                $caseweight       = isset($value->product->caseweight)?$value->product->caseweight:'';         
                $qtypercase       = isset($value->product->qtypercase)?$value->product->qtypercase:'';
                $description      = mb_convert_encoding(isset($value->product->description)?$value->product->description:'', "HTML-ENTITIES", "utf8");   
                $addedby          = isset($value->product->addedby)?$value->product->addedby:'';
                $isactive         = mb_convert_encoding(isset($value->product->isactive)?$value->product->isactive:'', "HTML-ENTITIES", "utf8");
                $roletype         = isset($value->product->roletype)?$value->product->roletype:'';
                $industry         = mb_convert_encoding(isset($value->product->industry->name) ? $value->product->industry->name : 'Not Applicable', "HTML-ENTITIES", "utf8");
                $category         = mb_convert_encoding(isset($value->product->category->name) ? $value->product->category->name : 'Not Applicable', "HTML-ENTITIES", "utf8");
                $subcategory      = mb_convert_encoding(isset($value->product->subcategory->name) ? $value->product->subcategory->name : 'Not Applicable', "HTML-ENTITIES", "utf8");         
                $brand            = mb_convert_encoding(isset($value->product->brand->name) ? $value->product->brand->name : 'Not Applicable', "HTML-ENTITIES", "utf8");
                $productid        = isset($value->product->productid)?$value->product->productid:'';          
                $targetprice      = isset($value->targetprice)?$value->targetprice:'';
                $timeframe        = isset($value->timeframe)?$value->timeframe:'';          
                $pstatus          = isset($value->pstatus)?$value->pstatus:'';
                $manufacture      = isset($value->product->manufacture)?$value->product->manufacture:'';        
                $packlang         = isset($value->pakaging)?$value->pakaging:'';
                $postno           = isset($value->postno)?$value->postno:'';             
                $pdate            = isset($value->pdate)?$value->pdate:'';
                $country          = isset($value->country)?$value->country:'';
                $productcodes = '';
                               
                if((Auth::user()->roletype=="AD")||(Auth::user()->roletype=="SA")){

                    $result1 = (count($value->product->productCode)!=0)?$value->product->productCode:array();

                    
                    $k_i = 0;
                    foreach ($result1 as $value_s) {

                        $productcodes .= str_ireplace(':',': ',str_ireplace(' ','',$value_s->productcode)) . '' .(($k_i == (count($result1) - 1))?'':';');

                        $k_i++;

                    }

                }
                else{

                    $result1 = (count($value->product->productCode)!=0)?$value->product->productCode:array();
                    $k_i = 0;
                    foreach ($result1 as $value_s) {
                        
                        $productcodes .= str_ireplace(':',': ',str_ireplace(' ','',$value_s->productcode))  . '' .(($k_i == (count($result1) - 1))?'':';');
                        $k_i++;
                    }
                }

                $results= empty($value->product->productImage) ? count($value->product->productImage) : 0;


                 if($results!=0)
                 {
                    if (strpos($value->product->productImage[0]->imageurl,'http://') !== false)
                    {
                       
                       $images = "<div class='BigImage'><img src='<?php echo $productimage[0]['imageurl'] ?>' alt='' width='258px'></div>";
                       
                    }
                    else
                    {
                        
                        $images = "<div class='BigImage'><div class='MagicZoom' id='image'><div class='MagicZoom'><a href='".url('/')."/productimg/".$value->product->productImage[0]->imageurl."' id='imageshref' class='MagicZoom' rel='zoom-position:inner;zoom-fade:true'><img id='bimg' src='".url('/')."/productimg/thumb/".$value->product->productImage[0]->imageurl ."' alt='' width='300'></a></div></div></div>";
                       
                    }
                  }
                  else
                  {
                      
                      $images = "<div class='BigImage'><img src='".url('/')."/productimg/image4.jpg' alt='' width='258px'></div>";
                      
                  }
            }

            $arr = array(
                    'postid' => $postid,                    
                    'userid' => $userid,
                    'ptype' => $ptype,                      
                    'currency' => $currency,
                    'productno' => $productno,              
                    'uom' => $uom,
                    'quantity' => $quantity,                
                    'location' => $location,
                    'expdate' => $expdate,                  
                    'expirydate' => $expirydate,
                    'customerrefno' => $customerrefno,      
                    'pakaging' => $pakaging,
                    'language' => $language,                
                    'name' => $name,
                    'industryid' => $industryid,            
                    'brandid' => $brandid,
                    'catid' => $catid,                      
                    'subcatid' => $subcatid,
                    'pakaging' => $pakaging,                
                    'shipingcondition' => $shipingcondition,
                    'caseweight' => $caseweight,            
                    'qtypercase' => $qtypercase,
                    'description' => $description,          
                    'addedby' => $addedby,
                    'isactive' => $isactive,                
                    'roletype' => $roletype,
                    'industry' => $industry,                
                    'category' => $category,
                    'subcategory' => $subcategory,          
                    'brand' => $brand,
                    'productid' => $productid,              
                    'targetprice' => $targetprice,
                    'timeframe' => $timeframe,              
                    'pstatus' => $pstatus,
                    'manufacture' => $manufacture,          
                    'packlang' => $packlang,
                    'postno' => $postno,                    
                    'pdate' => $pdate,
                    'country' => $country,
                    'image' => $images,
                    'productcodes' =>$productcodes
                );

                // $conn->close();
                return $arr;
        }
    }

    public function addOffer(Request $request){

        if (empty($request->datepicker)) {
            $datepicker = "";
        } else {
            $formdatepicker = $request->datepicker;
            $datepicker     = date("Y-m-d", strtotime($formdatepicker));
        }

        $lg = '';
        $cn = '';

        $statement1 = new \App\PostQuotation;

        $statement1->quotationno = $this->randStringNumeric22(6) . "-" . $this->randStringAlpha2();
        $statement1->postno = $request->postno;
        $statement1->productno = $request->pro_id;
        $statement1->price = $request->price;
        $statement1->currency = $request->offerCurrency;
        $statement1->uom = $request->offerUoM;
        $statement1->quantity = $request->offerqty;
        $statement1->location = $request->offerlocation;
        $statement1->expdate = $request->offerdaterange;
        $statement1->expirydate  = $datepicker;

        for ($i=0; $i < count($request->offerlanguage); $i++) { 
            if( $i == (count($request->offerlanguage) - 1) ){
                $lg .= $request->offerlanguage[$i];
            }else{
                $lg .= $request->offerlanguage[$i] . ',';    
            }
            
        }


        $statement1->language = $lg;
        
        for ($i=0; $i < count($request->addcountry); $i++) { 
            if( $i == (count($request->addcountry) - 1) ){
                $cn .= $request->addcountry[$i];
            }else{
                $cn .= $request->addcountry[$i] . ',';    
            }
            
        }

        $statement1->country = $cn;
        $statement1->timeframe = $request->offertimeframe;
        $statement1->details = "New Offer By Account Manager";
        $statement1->offercrdate = date('Y-m-d H:i:sa');
        $statement1->userid = Auth::user()->userid;
        $statement1->quationstatus = "Submitted";
        $statement1->postuserid = $request->puserid;
        $statement1->type = "OFFER";

        $statement1->save();
        
        $statement2 = new \App\UserActivity;

        $statement2->userid = Auth::user()->userid;
        $statement2->ipaddress = $request->ip();
        $statement2->sessionid = \Session::get('_token');
        $statement2->actiondate = date('Y-m-d');
        $statement2->actiontime = date('Y-m-d H:i:sa');
        $statement2->actionname = 'Submitted Offer';

        $statement2->save();

        return '1';
    }

    public function myPosting(){

        $checkVal = $this->checkPermission();       
        
        $timeframes = \App\MstTimeframe::all(); 
        
        return View('posting.myPosting',['checkVal'=>$checkVal,'timeframes'=>$timeframes])->with('title','Ziki Trade::My Postings');
    }

    public function myPosting_ajax(Request $request){

        $territory1 = array();
        $territory2 = array();
        if ( empty( Auth::user()->industry ) == false ) {
            $industry1  = Auth::user()->industry;
            $territory1 = explode(',', $industry1);
            // $territory1 = ' AND mp.industryid IN (' . $industry1 . ')';
        }
        
        if (empty( Auth::user()->category ) == false) {
            $category1  = Auth::user()->category;
            $territory2 = explode(',', $category1);
            // $territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
        }
        if( isset($request->productActionType) && ($request->productActionType == 'group_filter') ){
    // echo "<pre>"; print_r($request->input());die;         
            if (empty($request->radioread) == false) {
                if ($request->radioread == '1'){
                    
                    $sql = \App\PostAdverstisement::where( 'userid',Auth::user()->userid )->where( 'act_inactive','0' )->whereIn('pstatus',['Completed','CANCELLED','EXPIRED'])->whereHas('postCustomer',function($query){
                            $query->where('isactive','1');
                        })->whereHas('user',function($query){
                            $query->where('isactive','1');
                        })->whereHas('product',function($query) use ($territory1) {
                            $query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
                        })->whereHas('product',function($query) use ($territory2) {
                            $query->orWhere('pstatus','APPROVED')->orWhereIn('catid',$territory2);
                        })->get();

                    
                }
                else if ($request->radioread == '2'){
                    $sql = \App\PostAdverstisement::where( 'userid',Auth::user()->userid )->where( 'act_inactive','0' )->whereIn('pstatus',['New'])->whereHas('postCustomer',function($query){
                            $query->where('isactive','1');
                        })->whereHas('user',function($query){
                            $query->where('isactive','1');
                        })->whereHas('product',function($query) use ($territory1) {
                            $query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
                        })->whereHas('product',function($query) use ($territory2) {
                            $query->orWhere('pstatus','APPROVED')->orWhereIn('catid',$territory2);
                        })->get();
                }
                else if ($request->radioread == '3'){
                    $sql = \App\PostAdverstisement::where( 'userid',Auth::user()->userid )->where( 'act_inactive','0' )->whereHas('postCustomer',function($query){
                            $query->where('isactive','1');
                        })->whereHas('user',function($query){
                            $query->where('isactive','1');
                        })->whereHas('product',function($query) use ($territory1) {
                            $query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
                        })->whereHas('product',function($query) use ($territory2) {
                            $query->orWhere('pstatus','APPROVED')->orWhereIn('catid',$territory2);
                        })->get();
                }

                    
            }
            else{

                $sql = \App\PostAdverstisement::where( 'userid',Auth::user()->userid )->where( 'act_inactive','0' )
                        ->whereHas('user',function($q){
                            $q->where('isactive','1');
                        })->whereHas('postCustomer',function($q){
                            $q->where('isactive','1');
                        })->whereHas('product',function($q){
                            $q->where('pstatus','APPROVED');
                        });

                if( empty($request->input('industryData')) == false ){
                    $sql = $sql->whereHas('product',function($q) use ($request){
                        $q->whereHas('industry',function($q_i) use ($request){
                            $q_i->whereIn( 'industryid', explode(',', $request->input('industryData')) );
                        });
                    });
                }
                if( empty($request->input('categoryData')) == false ){
                    $sql = $sql->whereHas('product',function($q) use ($request){
                        $q->whereIn( 'catid', explode(',', $request->input('categoryData')) );
                    });
                }
                if( empty($request->input('scategoryData')) == false ){
                    $sql = $sql->whereHas('product',function($q) use ($request){
                        $q->whereIn( 'subcatid', explode(',', $request->input('scategoryData')) );
                    });
                }
                if( empty($request->input('brandData')) == false ){
                    $sql = $sql->whereHas('product',function($q) use ($request){
                        $q->whereIn( 'brandid', explode(',', $request->input('brandData')) );
                    });
                }

                if (empty($request->input('status')) == false) {
                    if ($request->input('status') == 'NEW'){
                        $sql = $sql->whereIn( 'pstatus',['NEW'] );
                    }
                    else if ($request->input('status') == 'Submitted'){
                        $sql = $sql->whereIn( 'pstatus',['Submitted'] );

                    }
                    else if ($request->input('status') == 'Completed'){
                        $sql = $sql->whereIn( 'pstatus',['Completed'] );
                    }
                    else if ($request->input('status') == 'CANCELLED'){
                        $sql = $sql->whereIn( 'pstatus',['CANCELLED'] );
                    }
                }

                if( empty($request->input('type')) == false ){
                    $sql = $sql->where('ptype',$request->input('type'));
                }

                if( empty($request->input('timeframe')) == false ){
                    $sql = $sql->where('timeframe',$request->input('timeframe'));
                }


                $sql = $sql->get(); 
            }
        
        }
        else{
            $sql = \App\PostAdverstisement::where( 'userid',Auth::user()->userid )->where( 'act_inactive','0' )->whereHas('postCustomer',function($query){
                    $query->where('isactive','1');
                })->whereHas('user',function($query){
                    $query->where('isactive','1');
                });  

            if ( empty( Auth::user()->industry ) == false ) {
                
                $industry1  = Auth::user()->industry;
                
                $territory1 = explode(',', $industry1);
                
                $sql = $sql->whereHas('product',function($query) use ($territory1) {
                    $query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
                });  
            }
            
            if (empty( Auth::user()->category ) == false) {
                
                $category1  = Auth::user()->category;
                
                $territory2 = explode(',', $category1);
                
                $sql = $sql->whereHas('product',function($query) use ($territory2) {
                            $query->orWhere('pstatus','APPROVED')->orWhereIn('catid',$territory2);
                        });
            }
            $sql = $sql->get();
        }

            

        

         $record = array();
         $i = 0;

         foreach ($sql as $value) {

            $countro = \App\PostQuotation::where('postno',$value->postno)->count();
            
            $poststatus = isset($value->pstatus)?$value->pstatus:'';

            $postno = $value->postno;

            if ($poststatus == 'NEW') {
                $newpost = $this->mypoststatus($postno);
                $edit1   = '<a href="myposting-detail-view/' . $postno .'">View</a>&nbsp;|&nbsp;';
                if ($newpost == 'NEW') {
                    if ($countro <= 0) {
                        $edit2 = '<a href="edit-add/' . $postno . '">Edit</a>&nbsp;|&nbsp;';
                    } else {
                        $edit2 = '';
                    }
                } else {
                    $edit2 = '';
                }
                $edit3 = '<a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                $edit  = $edit1 . $edit2 . $edit3;
            }

            if ($poststatus == 'Submitted') {
                $newpost = $this->mypoststatus($postno);
                $edit1   = '<a href="myposting-detail-view/' . $postno. '">View</a>&nbsp;|&nbsp;';
                if ($newpost == 'NEW') {
                    if ($countro <= 0) {
                        $edit2 = '<a href="edit-add/' . $postno. '">Edit</a>&nbsp;|&nbsp;';
                    } else {
                        $edit2 = '';
                    }
                } else {
                    $edit2 = '';
                }
                $edit3 = '<a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                $edit  = $edit1 . $edit2 . $edit3;
            }

            if ($poststatus == 'Countered') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
            }
            
            if ($poststatus == 'Completed') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
            if ($poststatus == 'EXPIRED') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
            
            if ($poststatus == 'CANCELLED') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
             if ($poststatus == 'Declined') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
            if ($poststatus == 'Accepted') {
                $edit = '<a href="myposting-detail-view' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '">Repost</a>';
            }
           
            if ($poststatus == 'CANCELLED') {
                $poststatus = "CANCELLED";
            } 
            else if($poststatus == 'Completed')
            {
                $poststatus = "COMPLETED";
            }
            else if($poststatus == 'EXPIRED')
            {
                $poststatus = "EXPIRED";
            }
            else if($poststatus == 'NEW')
            {
                $poststatus = "NEW";
            }
            else if($poststatus == 'Countered')
            {
                $poststatus = "Countered";
            }
            else if($poststatus == 'Declined')
            {
                $poststatus = "Declined";
            }
            else if($poststatus == 'Submitted')
            {
                $poststatus = "Submitted";
            }
            else if($poststatus == 'Accepted')
            {
                $poststatus = "Accepted";
            }
            else {
                $poststatus = $this->mypoststatus($postno);
            }

            $record[$i]['postno'] = $value->postno;
            $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));
            $record[$i]['brand'] = isset($value->product->brand->name) ? $value->product->brand->name : '';
            $record[$i]['ptype'] = ucwords(strtolower(isset($value->ptype)?$value->ptype:''));
            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';
            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';
            $record[$i]['price'] = isset($value->targetprice)?$value->targetprice:'';
            $record[$i]['currency'] = isset($value->currency)?$value->currency:'';
            $record[$i]['customerrefno'] = isset($value->customerrefno)?$value->customerrefno:'';
            $record[$i]['poststatus'] = $poststatus;
            $record[$i]['edit'] = $edit;

            $i++;   
         }

         return \Response::json($record);
    }

    function mypoststatus($id)
    {
    
        $r1=0;  $r2=0;
        $r3=0;  $r4=0;
        $r5=0;  $r6=0;

        $r1 = \App\QuotationCounter::where('postno',$id)->count();

        $r2 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->count();

        $r3 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->where(function($query){
                $query->where('counetr_status','Accepted')->orWhere('offerCounterStatus','Accepted');
                })->count();

        $r4 = \App\QuotationCounter::where('postno',$id)->where(function($query){
                    $query->where('counetr_status','Accepted')->orWhere('offerCounterStatus','Accepted');
                })->count();
        
        $r5 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->where(        function($query){
                        $query->where('counetr_status','Declined')->orWhere('offerCounterStatus','Declined');
                    })->count();
        
        $r6 = \App\QuotationCounter::where('postno',$id)->where(function($query){
                    $query->where('counetr_status','Declined')->orWhere('offerCounterStatus','Declined');
                })->count();
        
        if($r1 > 0 && $r2>0)
        {
            if($r3>0 || $r4 >0)
            {
            return 'ACCEPTED';
            }
            elseif($r5>0 || $r6 >0)
            {
            return 'DECLINED';
            }
            else
            {
            return 'COUNTERED';
            }
        
        }
        elseif($r1<=0 && $r2>0)
        {   if($r3>0 || $r4 >0)
            {
            return 'ACCEPTED';
            }
            elseif($r5>0 || $r6 >0)
            {
            return 'DECLINED';
            }
            else
            {
            return 'SUBMITTED';
            }
        }
        else
        { 
        return 'NEW';
        }
    }

    public function cancelPosting(Request $request){
        if ($request->Type == 'cancelpost') {
            
            $statement1 = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('postno',$request->postid)->update(['pstatus'=>'CANCELLED','archivepost'=>'1']);
            
            $useraction = new \App\UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Cancelled Posting';

            $useraction->save();

        }       
    }

    public function uPosting(){
        return View('posting.uPosting')->with('title','Ziki Trade::Users Posting');
    }
    public function uPosting_ajax(Request $request){

        $sql = \App\PostAdverstisement::where('userid',$request->postuserid)->whereNotIn('pstatus',['Completed','CANCELLED'])->orderBy('brand')->get();

        $record = array();
        $i = 0;

        foreach ($sql as $value) {
            
            $productstatus = isset($value->product->pstatus)?$value->product->pstatus:'';
            $poststatus    = isset($value->pstatus)?$value->pstatus:'';
            $customerstatus = isset($value->postCustomer->isactive)?$value->postCustomer->isactive:'';
            $act_inactive = isset($value->act_inactive)?$value->act_inactive:'';
            $useridstataus = isset($value->user->isactive)?$value->user->isactive:'';
            $productCheck = '<input type="checkbox" class="form-control checkProduct" name="checkProduct" id="checkProduct" value="' . $value->postid . '" />';

            if ( ($useridstataus=='1') && ($act_inactive=='0') && ($productstatus!='REJECTED') && ($customerstatus=='1') && ($poststatus !='EXPIRED')) {
                $enableaction = '<span class="label label-sm label-success"> Enable </span>';
            } else {
                if($poststatus =='EXPIRED')
                {
                   $enableaction = '<span class="label label-sm label-danger">EXPIRED</span>';
                }
                else
                {
                    $enableaction = '<span class="label label-sm label-danger">Disable</span>';
                }
            }
            $edit = '<a href="posting-detail-view/' . $value->postno . '">View</a>';

            $record[$i]['postno'] = isset($value->postno)?$value->postno:'';
            $record[$i]['sdate'] = date('F d Y',strtotime($value->pdate));
            $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';
            $record[$i]['ptype'] = isset($value->ptype)?$value->ptype:'';
            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';
            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';
            $record[$i]['price'] = isset($value->targetprice)?$value->targetprice:'';
            $record[$i]['currency'] = isset($value->currency)?$value->currency:'';
            $record[$i]['userid'] = isset($value->userid)?$value->userid:'';
            $record[$i]['customerrefno'] = isset($value->customerrefno)?$value->customerrefno:'';
            $record[$i]['enableaction'] = $enableaction;
            $record[$i]['edit'] = $edit;


            $i++;
        }

        return \Response::json($record);
    }

    public function mypostingDetailView(Request $request){

        $postStatus = $this->mypoststatus($request->post_id);

        $ml = \App\MstLanguage::orderBy('name')->get();

        $currencies = \App\MstCurrency::orderBy('name')->get();

        $uom = \App\MstUom::orderBy('name')->get();
        
        $locations = \App\MstLocation::orderBy('name')->get();
 
        $expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

        $timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

        $countries = \App\MstCountry::orderBy('name')->get();

        
        if((Auth::user()->roletype =="AD") || ( Auth::user()->roletype == "SA") ){
            $x='style="background:#ffffff"';
        }
        else{
            $x='style="background:#f9f9f9"';
        }

        $data = array(
            'postStatus' => $postStatus,
            'x' => $x,
            'ml' => $ml,
            'currencies'=>$currencies,
            'uom'=>$uom,
            'locations'=> $locations,
            'expdateranges' => $expdateranges,
            'timeframes' => $timeframes,
            'countries' => $countries
         );

        return View('posting.mypostingDetailView',$data)->with('title','Ziki Trade::My Posting Detail View');
    }


    public function mypostingoffer(Request $request){

        switch ($request->Type) {
            case 'counteradd':
                $counterprice       = $_REQUEST['counterprice'];
                $counterquantity    = $_REQUEST['counterquantity'];
                $countertimeframe   = $_REQUEST['countertimeframe'];
                $counterdaterange   = $_REQUEST['counterdaterange'];
                $counterpostno      = $_REQUEST['counterpostno'];
                $counterquotationno = $_REQUEST['counterquotationno'];
                $counterproductno   = $_REQUEST['counterproductno'];
                $counterquoteid     = $_REQUEST['counterquoteid'];
                $msg                = "Countered By User";
                $status             = "Counter";
                
                $qc = new \App\QuotationCounter;
                $qc->quotationno = $counterquotationno;
                $qc->quoteid = $counterquoteid;
                $qc->postno = $counterpostno;
                $qc->productno = $counterproductno;
                $qc->acceptedBy = Auth::user()->userid;
                $qc->counter_date = date('F d Y H:i:sa');
                $qc->counter_by = Auth::user()->userid;
                $qc->counter_price = $counterprice;
                $qc->counter_quantity = $counterquantity;
                $qc->counter_timeframe = $countertimeframe;
                $qc->counter_expdate = $counterdaterange;
                $qc->counter_msg = $msg;
                $qc->counetr_status = $status;
                $qc->roletype = Auth::user()->roletype;

                $qc->save();

                $uppq = \App\PostQuotation::where('quoteid',$counterquoteid)->update(['quationstatus'=>'Countered']);

                $uppa = \App\PostAdverstisement::where('postno',$counterpostno)->update(['pstatus'=>'Countered']);

                $useraction = new UserActivity;

                $useraction->userid = $request->input('userid');
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Account Manager submitted a counter offer';

                $useraction->save();
                break;
            

            case 'acceptupdate':
                $termcond        = $_REQUEST['termcond'];
                $acceptquoteid   = $_REQUEST['acceptquoteid'];
                $acceptcounterid = $_REQUEST['acceptcounterid'];
                $acceptpostno    = $_REQUEST['acceptpostno'];

                $datarow2 = \App\PostQuotation::where('quoteid',$acceptquoteid)->get();

                if (!empty($acceptcounterid)) {

                    $datarow = \App\QuotationCounter::where('counterid',$acceptcounterid)->get();
                    
                    $sql = \App\QuotationCounter::where('counterid',$acceptcounterid)->update(['counter_msg'=>'Offer Accepted by User','counetr_status'=>'Accept']);

                    $sqlq = \App\PostQuotation::where('quoteid',$datarow[0]->quoteid)->update(['quationstatus'=>'Accepted','counetr_status'=>'Accepted']);
                    
                    $sqldecline = \App\PostQuotation::where('postno',$datarow[0]->postno)->whereNotIn('quoteid',[$datarow[0]->quoteid])->update(['quationstatus'=>'Declined','counetr_status'=>'Decline','declinemsg'=>'Offer Declined By User']);

                    $sqlpa = \App\PostAdverstisement::where('postno',$datarow[0]->postno)->update(['pstatus'=>'Accepted','quotefacility'=>$datarow2[0]->userid]);

                    $sqltm = new \App\TMAcceptedOffers;

                    $sqltm->counterid  = $acceptcounterid;
                    $sqltm->quotationno  = $datarow[0]->quotationno;
                    $sqltm->quoteid  = $datarow[0]->quoteid;
                    $sqltm->postno  = $datarow[0]->postno;
                    $sqltm->productno  = $datarow[0]->productno;
                    $sqltm->acceptedBy  = Auth::user()->userid;
                    $sqltm->acc_date  = date('F d Y H:i:sa');
                    $sqltm->acc_price  = $datarow[0]->price;
                    $sqltm->acc_quantity  = $datarow[0]->quantity;
                    $sqltm->acc_timeframe  = $datarow[0]->timeframe;
                    $sqltm->acc_expdate  = $datarow[0]->expdate;
                    $sqltm->status  = 'Waiting For Both';

                    $sqltm->save();

                }
                else{
                    $datarow = \App\PostQuotation::where('quoteid',$acceptquoteid)->get();
                    
                    $sql = \App\PostQuotation::where('quoteid',$acceptquoteid)->update(['acceptmsg'=>'Offer Accepted By User','quationstatus'=>'Accepted','counetr_status'=>'Accepted']);

                    $sqlpa = \App\PostAdverstisement::where('postno',$datarow[0]->postno)->update(['pstatus'=>'Accepted','quotefacility'=>$datarow2[0]->userid]);
            

                   $sqldecline = \App\PostQuotation::where('postno',$datarow[0]->postno)->whereNotIn('quoteid',[$datarow[0]->quoteid])->update(['quationstatus'=>'Declined','counetr_status'=>'Decline','declinemsg'=>'Offer Declined By User']);

                    $sqltm = new \App\TMAcceptedOffers;

                    $sqltm->quotationno  = $datarow[0]->quotationno;
                    $sqltm->quoteid  = $datarow[0]->quoteid;
                    $sqltm->postno  = $datarow[0]->postno;
                    $sqltm->productno  = $datarow[0]->productno;
                    $sqltm->acceptedBy  = Auth::user()->userid;
                    $sqltm->acc_date  = date('F d Y H:i:sa');
                    $sqltm->acc_price  = $datarow[0]->price;
                    $sqltm->acc_quantity  = $datarow[0]->quantity;
                    $sqltm->acc_timeframe  = $datarow[0]->timeframe;
                    $sqltm->acc_expdate  = $datarow[0]->expdate;
                    $sqltm->status  = 'Waiting For Both';

                    $sqltm->save();                   
            
                }

                $useraction = new UserActivity;

                $useraction->userid = $request->input('userid');
                $useraction->ipaddress = $request->ip();
                $useraction->sessionid = Session::get('_token');
                $useraction->actiondate = date('Y-m-d');
                $useraction->actiontime = date('Y-m-d H:i:sa');
                $useraction->actionname = 'Offer accepted by Account Manager';

                $useraction->save();

                break;
            default:
                # code...
                break;
        }

        $sql = \App\PostQuotation::where('postno',$request->segment(3))->where('type','QUOTATION')->orderBy('offercrdate','DESC')->get();

        $record = array();
        foreach ($sql as $value) {

            $sdate = date('F d Y H:i:sa') . ' EST';
            $quoteid            = $value->quoteid;
            $quotationno        = $value->quotationno;
            $price              = $value->price;
            $currency           = $value->currency;
            $uom                = $value->uom;
            $quantity           = $value->quantity;
            $timeframe          = $value->timeframe;
            $expdate            = $value->expdate;
            $language           = $value->language;
            $detail             = $value->detail;
            $offertype          = $value->type;
            $declinemsg         = $value->declinemsg;
            $acceptmsg          = $value->acceptmsg;
            $isactive           = $value->isactive;
            $postcounetr_status = $value->counetr_status;
            
            $countro            = \App\QuotationCounter::where('quotationno',$quotationno)->count();

            if ($countro == '0') {
                if ($postcounetr_status == "") {
                    if($offertype !='OFFER')
                        {
                    if (Auth::user()->roletype != 'AD') {
                        $edit = '<a href="javascript:acceptnew(\'' . $quoteid . '\')">Accept</a>&nbsp;|&nbsp;
                         <a href="javascript:declineoffnew(\'' . $quoteid . '\')">Decline</a>&nbsp;|&nbsp;
                         <a href="javascript:counternew(\'' . $quoteid . '\')">Counter</a>';
                    }
                }
                else {
                 $edit = '';
                }
                }
                else {
                 $edit = '';
                }
                
            } else {
                $edit = '';
            }

            $countersql = \App\QuotationCounter::where('quotationno',$quotationno)->where('postno',$request->segment(3))->orderBy('counterid','DESC')->get();

            foreach ($countersql as $value1) {
                $sdate1         = date('F d Y H:i:sa',strtotime($value1->counter_date)) . " EST";
                $quoteid1       = $value1->postQuotation->quoteid;
                $coundID        = $value1->coundID;
                $quotationno1   = $value1->postQuotation->quotationno;
                $price1         = $value1->counter_price;
                $currency1      = $value1->postQuotation->currency;
                $uom1           = $value1->postQuotation->uom;
                $quantity1      = $value1->counter_quantity;
                $timeframe1     = $value1->counter_timeframe;
                $expdate1       = $value1->counter_expdate;
                $detailnew1     = $value1->counter_msg;
                $counterBy     = $value1->counterBy;
                if ($detailnew1 == 'Countered By User') {
                    $detail1 = "Countered By You";
                } elseif ($detailnew1 == 'Accepted by User') {
                    $detail1 = "Accepted By You";
                } elseif ($detailnew1 == 'Offer Declined By User') {
                    $detail1 = "Offer Declined By You";
                } else {
                    $detail1 = $detailnew1;
                }
                $max = \App\QuotationCounter::where('quotationno',$quotationno1)->max('counterid');

                if ( $max == $coundID ) {
                    if ( $counterBy != Auth::user()->userid ) {
                        if ($postcounetr_status == '') {
                            if ((Auth::user()->roletype != 'AD')) {
                                $edits1 = '<a href="javascript:acceptnew(\'' . $quoteid1 . '\')">Accept</a>&nbsp;|&nbsp;
                     <a href="javascript:declinenew(\'' . $coundID . '\')">Decline</a>&nbsp;|&nbsp;
                     <a href="javascript:counternewoff(\'' . $quoteid1 . '\')">Counter</a>';
                            }
                        } else {
                            $edits1 = '';
                            
                        }
                    } else {
                        $edits1 = '';
                        
                    }
                    
                } else {
                    $edits1 = '';
                    
                }

                $record[$i]['quotationno'] = $quotationno1;
                $record[$i]['sdate'] = $sdate1;
                $record[$i]['price'] = $price1;
                $record[$i]['currency'] = $currency1;
                $record[$i]['uom'] = $uom1;
                $record[$i]['quantity'] = $quantity1;
                $record[$i]['timeframe'] = $timeframe1;
                $record[$i]['expdate'] = $expdate1;
                $record[$i]['detail'] = $detail1;
                $record[$i]['edits'] = $edits1;
                    
            }

            if (!empty($acceptmsg)) {

                $record[$i]['quotationno'] = $quotationno;
                $record[$i]['sdate'] = $sdate;
                $record[$i]['price'] = $price;
                $record[$i]['currency'] = $currency;
                $record[$i]['uom'] = $uom;
                $record[$i]['quantity'] = $quantity;
                $record[$i]['timeframe'] = $timeframe;
                $record[$i]['expdate'] = $expdate;
                $record[$i]['detail'] = $acceptmsg;
                $record[$i]['edits'] = $edits;
            }

            if (!empty($declinemsg)) {

                $record[$i]['quotationno'] = $quotationno;
                $record[$i]['sdate'] = $sdate;
                $record[$i]['price'] = $price;
                $record[$i]['currency'] = $currency;
                $record[$i]['uom'] = $uom;
                $record[$i]['quantity'] = $quantity;
                $record[$i]['timeframe'] = $timeframe;
                $record[$i]['expdate'] = $expdate;
                $record[$i]['detail'] = $declinemsg;
                $record[$i]['edits'] = $edits;
            }

            $record[$i]['quotationno'] = $quotationno;
            $record[$i]['sdate'] = $sdate;
            $record[$i]['price'] = $price;
            $record[$i]['currency'] = $currency;
            $record[$i]['uom'] = $uom;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;
            $record[$i]['detail'] = $detail;
            $record[$i]['edits'] = $edits;

        }

        return \Response::json($record);
    }

    public function repost($id){
        
        if((Auth::user()->roletype == "AD")||(Auth::user()->roletype == "SA")){ 
            $color='#ffffff';
        }
        else{

            $color='#f9f9f9';
        }
        $currencies = \App\MstCurrency::orderBy('name')->get();
        $uoms = \App\MstUom::orderBy('name')->get();
        $locations = \App\MstLocation::orderBy('name')->get();
        $expdateranges = \App\MstExpDaterange::orderBy('name')->get();

        if(Auth::user()->userid=="UUZZ-9984" || Auth::user()->roletype == "SA"){
            $cusrefnos = \App\PostCustomer::where('isactive','1')->orderBy('refno')->get();
        }
        else{
            $cusrefnos = \App\PostCustomer::where('isactive','1')->where('refuserid',Auth::user()->userid)->orderBy('refno')->get();
        }

        $languages = \App\MstLanguage::orderBy('name')->get();
        $timeframes = \App\MstTimeframe::orderBy('name')->get();
        $countries = \App\MstCountry::orderBy('name')->get();

        $checkVal = $this->checkPermission();
        $pa = \App\PostAdverstisement::where('postno',$id)->get();
        $p_a = $pa->toArray();
        $data = array(
                'x'=>$color,
                'currencies'=>$currencies,
                'uoms'=> $uoms,
                'locations'=>$locations,
                'expdateranges'=>$expdateranges,
                'cusrefnos'=>$cusrefnos,
                'languages'=>$languages,
                'timeframes'=>$timeframes,
                'countries'=>$countries,
                'checkVal'=>$checkVal,
                'pa'=>$p_a[0],
                'id'=>$id
            );

        return View('posting.repost',$data)->with('title','Ziki Trade:: Repost Ad');       
    }

    public function repostPosting(Request $request,$id){

        if( isset($request->repostadd) && ($request->repostadd == 'Submit') ){
            
            $postAdverstisement = new \App\PostAdverstisement;

            $postAdverstisement->userid = Auth::user()->userid;
            $postAdverstisement->pdate = date('Y-m-d H:i:sa');
            $postAdverstisement->productno = $request->addprono;
            $postAdverstisement->ptype = $request->addtype;
            $postAdverstisement->currency = $request->addcurrency;
            $postAdverstisement->uom = $request->adduom;
            $postAdverstisement->quantity = $request->addqty;
            $postAdverstisement->location = $request->addlocation;
            $postAdverstisement->expdate = $request->addexpdrange;
            
            if (empty($request->datepicker)) {
                $datepicker = "";
            } else {
                $formdatepicker = $request->datepicker;
                $datepicker     = date("Y-m-d", strtotime($formdatepicker));
            }

            $postAdverstisement->expirydate = $datepicker;
            $postAdverstisement->customerrefno = $request->addcurrefno;

            $postAdverstisement->timeframe = $request->addtimeframe;
            
            $langtemp         = '';
            $langname         = '';
            $countname       = '';
            $counttemp       = '';

            foreach ($request->addcountry as $value) {
                
                $cnty = \App\MstCountry::where('name',$value)->get();

                if (empty($counttemp)) {
                    $counttemp = $value;
                    $countname = $cnty[0]->name;
                } else {
                    $counttemp = $langtemp . ',' . $value;
                    $countname = $countname . ',' . $cnty[0]->name;
                }
            }



            foreach ($request->selectedlanguage as $values) {
                $lang = \App\MstLanguage::where('languageid',$values)->get();

                if (empty($langtemp)) {
                    $langtemp = $values;
                    $langname = $lang[0]->name;
                } else {
                    $langtemp = $langtemp . ',' . $values;
                    $langname = $langname . ',' . $lang[0]->name;
                }
            }
            $postAdverstisement->pakaging = $langname;
            $postAdverstisement->language = $langtemp;
            $postAdverstisement->country = $countname;
            $postAdverstisement->pstatus = "NEW";
            $postAdverstisement->targetprice = $request->addprice;
            $postAdverstisement->postno = $this->checkduplicy();
            
            $seller ='';
            $buyer='';
            if($request->addtype=='BUY'){

               $buyer = Auth::user()->userid;
            }
            else{

               $seller = Auth::user()->userid;
            }

            $postAdverstisement->buyer = $buyer;
            $postAdverstisement->seller = $seller; 
            

            $postAdverstisement->save();

            $useraction = new \App\UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Reposted Posting';

            $useraction->save();
            
            return redirect('posting/myPosting?addpost=true&repost=success');           



        }
    }

    public function offerHistory(Request $request){

        $post_id = $request->input('postid');
        $quotationnonew = $request->input('quotationno');


        $sql = \DB::select("select pq.quoteid,pq.quotationno, pq.postno, pq.productno, pq.price, pq.currency, pq.uom, pq.quantity,pq.details,pq.offercrdate as offercrdatefor,pq.offerdeclinemsg,pq.offeracceptmsg, pq.location, 
            pq.expdate, pq.expirydate, pq.language, pq.timeframe, pq.userid, pq.quationstatus, pq.accdate, pq.rejdate, pq.lastcntdate, pq.isactive,pq.offerCounterStatus from post_quotation pq Where pq.postno='" . $post_id . "' AND pq.quotationno='".$quotationnonew."'  AND pq.type='OFFER' order by quoteid DESC");
        $edits = '';
        $record = array();
        $i = 0;
        foreach ($sql as $row) {

            $sdate = date('F d Y H:i:sa',strtotime($row->offercrdatefor)) . " EST";

            $quoteid        = $row->quoteid;
            $quotationno    = $row->quotationno;
            $price          = $row->price;
            $currency       = $row->currency;
            $uom            = $row->uom;
            $quantity       = $row->quantity;
            $timeframe      = $row->timeframe;
            $expdate        = $row->expdate;
            $language       = $row->language;
            $detailstatus   = $row->details;
            $declinemsg     = $row->offerdeclinemsg;
            $acceptmsg      = $row->offeracceptmsg;
            $isactive       = $row->isactive;
            $postcounetr_status = $row->offerCounterStatus;
            if($detailstatus =='New Offer By Account Manager')
            {
                $detail='New Offer By You';
            }

            $countid       = \DB::select("select count(*) as rowcount from quotation_counter Where quotationno='" . $quotationno . "'");

            $countro = !empty($countid) ? $countid[0]->rowcount : 0;

            $countersql = \DB::select("select pq.quoteid,qc.counterid as coundID,qc.counter_by,qc.counter_msg,pq.quotationno, pq.postno, 
                pq.productno, qc.counter_price, qc.currency, pq.uom,qc.counter_quantity,qc.counter_date, qc.counter_timeframe,qc.counter_expdate, qc.offeracceptmsg,qc.offerCounterStatus from quotation_counter qc LEFT JOIN 
                post_quotation pq ON qc.quotationno=pq.quotationno Where qc.quotationno='" . $quotationno . "' and qc.postno='" . $post_id . "' AND pq.type='OFFER' order by counterid DESC");

            foreach ($countersql as $value1) {
                $sdate1         = date('F d Y H:i:sa',strtotime($value1->counter_date)) . " EST";
                $quoteid1       = $value1->quoteid;
                $coundID        = $value1->coundID;
                $quotationno1   = $value1->quotationno;
                $price1         = $value1->counter_price;
                $currency1      = $value1->currency;
                $uom1           = $value1->uom;
                $quantity1      = $value1->counter_quantity;
                $timeframe1     = $value1->counter_timeframe;
                $expdate1       = $value1->counter_expdate;
                $detailnew1     = $value1->offeracceptmsg;
                $counterBy     = $value1->counter_by;
                $counetr_status1 = $value1->offerCounterStatus;
                if ($detailnew1 == 'Countered By User') {
                    $detail1 = "Countered By You";
                } elseif ($detailnew1 == 'Accepted by User') {
                    $detail1 = "Accepted By You";
                } elseif ($detailnew1 == 'Declined By User') {
                    $detail1 = "Offer Declined By You";
                } else {
                    $detail1 = $detailnew1;
                }
                $max = \App\QuotationCounter::where('quotationno',$quotationno1)->max('counterid');

                if ( $max == $coundID ) {
                    if ( $counterBy != Auth::user()->userid ) {
                        if ($counetr_status1 != '') {
                            if ($postcounetr_status == '') {
                                if ((Auth::user()->roletype != 'AD')) {
                                    $edits1 = '<a href="javascript:acceptnew(\'' . $quoteid1 . '\')">Accept</a>&nbsp;|&nbsp;
                         <a href="javascript:declinenew(\'' . $coundID . '\')">Decline</a>&nbsp;|&nbsp;
                         <a href="javascript:counternewoff(\'' . $quoteid1 . '\')">Counter</a>';
                                }
                            } else {
                                $edits1 = '';
                                
                            }
                        }
                        else{
                            $edits1 = '';    
                        }
                    } else {
                        $edits1 = '';
                        
                    }
                    
                } else {
                    $edits1 = '';
                    
                }

                $record[$i]['quotationno'] = $quotationno1;
                $record[$i]['sdate'] = $sdate1;
                $record[$i]['price'] = $price1;
                $record[$i]['currency'] = $currency1;
                $record[$i]['uom'] = $uom1;
                $record[$i]['quantity'] = $quantity1;
                $record[$i]['timeframe'] = $timeframe1;
                $record[$i]['expdate'] = $expdate1;
                $record[$i]['detail'] = $detail1;
                $record[$i]['edits'] = $edits1;
                    
            }         

            if (!empty($acceptmsg)) {

                $record[$i]['quotationno'] = $quotationno;
                $record[$i]['sdate'] = $sdate;
                $record[$i]['price'] = $price;
                $record[$i]['currency'] = $currency;
                $record[$i]['uom'] = $uom;
                $record[$i]['quantity'] = $quantity;
                $record[$i]['timeframe'] = $timeframe;
                $record[$i]['expdate'] = $expdate;
                $record[$i]['detail'] = $acceptmsg;
                $record[$i]['edits'] = $edits;
            }

            if (!empty($declinemsg)) {

                $record[$i]['quotationno'] = $quotationno;
                $record[$i]['sdate'] = $sdate;
                $record[$i]['price'] = $price;
                $record[$i]['currency'] = $currency;
                $record[$i]['uom'] = $uom;
                $record[$i]['quantity'] = $quantity;
                $record[$i]['timeframe'] = $timeframe;
                $record[$i]['expdate'] = $expdate;
                $record[$i]['detail'] = $declinemsg;
                $record[$i]['edits'] = $edits;
            }

            $record[$i]['quotationno'] = $quotationno;
            $record[$i]['sdate'] = $sdate;
            $record[$i]['price'] = $price;
            $record[$i]['currency'] = $currency;
            $record[$i]['uom'] = $uom;
            $record[$i]['quantity'] = $quantity;
            $record[$i]['timeframe'] = $timeframe;
            $record[$i]['expdate'] = $expdate;
            $record[$i]['detail'] = $detail;
            $record[$i]['edits'] = $edits;  
            $i++;
        }

        return \Response::json($record);

    }
}