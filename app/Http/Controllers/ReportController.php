<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('report.index')->with('title','Ziki Trade::Report');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function functions(Request $request){

        if ($request->actionmode == "industaction") {

            $roletype = Auth::user()->roletype;
            $userid   = Auth::user()->userid;
            $status   = 1;
            $iid      = explode(',', Auth::user()->industry);

            if ($roletype == 'AM') {
                if (empty($iid) == false) {

                    $sth = \App\Industry::where('isactive',$status)->whereIn('industry',$iid)->orderBy('name')->get();

                    foreach ($sth as $result) {

                        $industryid = $result->industryid;
                        
                        $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                        
                        $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);    

                    }

                    return \Response::json($json);

                } 
                else if (empty($iid) == true) {

                    $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();

                    foreach ($sth as $result) {

                        $industryid=$result->industryid;

                        $name=mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                        
                        $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);

                    }
                    
                    return \Response::json($json);
                }
            }
            if ($roletype == 'AD') {

                $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();

                foreach ($sth as $result) {

                    $industryid=$result->industryid;

                    $name=mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                    
                    $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);

                }
                
                return \Response::json($json);
            }

            if ($roletype == 'SA') {

                $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();

                foreach ($sth as $result) {

                    $industryid=$result->industryid;

                    $name=mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                    
                    $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);

                }
                
                return \Response::json($json);
            }
        }

        if ($request->actionmode == 'selectreportindustry') {

            $industry = array(
                $request->ind_id
                );

            $in   = implode(',', $_POST['ind_id']);
            $status = 1;

            $stmt = \App\Category::where('isactive',$status)->whereIn('industryid',$request->ind_id)->orderBy('name')->get();

            
            if( count($stmt) ){
                foreach ($stmt as $result) {
                    $catid = $result->catid;
                    $industryid = $result->industryid;
                    $isactive = $result->isactive;        
                    $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");        
                    
                    $json['itemData'][] = array(
                        'catid'=>$catid, 
                        'name'=>$name,
                        'industryid'=>$industryid,  
                        'isactive'=>$isactive
                        );
                }
            }
            else{
                $json['itemData'][] =array();    
            }
            //select brands
            $stmt2 = \App\Brand::where('isactive',$status)->whereIn('industryid',$request->ind_id)->orderBy('name')->get();

            if(count($stmt2) > 0) {
                foreach ($stmt2 as $result2) {

                    $brandid=$result2->brandid;
                    
                    $industryid=$result2->industryid;
                    
                    $isactive=$result2->isactive;
                    
                    $name=mb_convert_encoding($result2->name, "HTML-ENTITIES", "ISO-8859-1");

                    $json['brandData'][] =array(
                        'sql'=>'SELECT brandid, name, isactive, industryid FROM mst_brand WHERE isactive = :isactive AND industryid IN (' . $in . ') ORDER BY name',

                        'brandid'=>$brandid,    
                        'name'=>$name,
                        'industryid'=>$industryid,  
                        'isactive'=>$isactive
                        );
                }

            } 
            else {

                $json['brandData'][] = array(
                    'brandid'=>'0',
                    'sql'=>'SELECT brandid, name, isactive, industryid FROM mst_brand WHERE isactive = :isactive AND industryid IN (' . $in . ') ORDER BY name',
                    'count'=>count($stmt2));
            }
            return \Response::json($json);
        }

        if ($request->actionmode == 'selectsubcategory') {

            $industry = array(
                $request->cat_id
                );

            $status = 1;

            $userid   = Auth::user()->userid;

            $subcatid = explode(',', Auth::user()->subcategory); 

            $roletype = Auth::user()->roletype;

            if (empty($subcatid)) {

                $stmt = \App\SubCategory::where('isactive',$status)->whereIn('catid',$industry)->orderBy('name')->get();

                foreach ($stmt as $result) {
                    $json['itemData'][] = $result;
                }

                return \Response::json($json);
            }
            else
            {

                $stmt = \App\SubCategory::where('isactive',$status)->whereIn('catid',$industry)->whereIn('subcatid',$subcatid)->orderBy('name')->get();
                
                foreach ($stmt as $result2) {
                    $json['itemData'][] = $result2;
                }
                
                return \Response::json($json);
            }
        }

        if ($request->actionmode == 'customeraction') {

            $status= 1;
            
            $sth = \App\PostCustomer::where('isactive',$status)->orderBy('refno')->get();

            foreach ($sth as $result) {

                $custid = $result->custid;

                $name = mb_convert_encoding($result->refno, "HTML-ENTITIES", "ISO-8859-1");

                $json['itemData'][] = array(
                    'custid'=>$custid,  
                    'refno'=>$name
                    );
            }
            
            return \Response::json($json);
        }


        if ($_POST['actionmode'] == 'alluserid') {

            $status= 1;
            
            $sth = \App\User::where('isactive',$status)->orderBy('userid')->get();

            foreach ($sth as $result) {

                $userid = $result->userid;

                $name = mb_convert_encoding($result->userid, "HTML-ENTITIES", "ISO-8859-1");
                
                $json['itemData'][] = array(
                    'userid'=>$userid,  
                    'userid'=>$name
                    );   

            }
            
            return \Response::json($json);
        }


        if ($request->actionmode == 'alllocation') {

            $status= 1;
            
            $sth = \App\MstLocation::where('isactive',$status)->orderBy('name')->get();

            foreach ($sth as $result) {

                $locationid = $result->locationid;

                $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                
                $json['itemData'][] = array(
                    'locationid'=>$locationid,
                    'name'=>$name
                    );   

            }
            
            return \Response::json($json);
        }

    }

    public function posting_pie_chart(Request $request){

        $where = "1=1";
        if(!empty($request->to && $request->from))
        {
            $fromdate = strtotime($request->from);
            $fromdate = date("Y-m-d",$fromdate);
            $todate = strtotime($request->to);
            $todate = date("Y-m-d",$todate);
            $where= $where." && pdate between '".$fromdate."' AND  '".$todate." '";
        } else {
            $fromdate = "";
            $todate = "";
        }
        if(empty($request->selectedindustry))
        {
            $selectedindustry=array();
        }

        $selectedindustry = $this->unsetbyvalue($request->selectedindustry,'all');

        $selectcategory = $this->unsetbyvalue($request->selectcategory,'all');
        $selectbrands = $this->unsetbyvalue($request->selectbrands,'all');
        $selectsubcat = $this->unsetbyvalue($request->selectsubcat,'all');
        $alluserid = $this->unsetbyvalue($request->alluserid,'all');
        $customerdata = $this->unsetbyvalue($request->customerdata,'all');
        $type = $this->unsetbyvalue($request->type,'all');
        $alllocation = $this->unsetbyvalue($request->alllocation,'all');
        $status = $this->unsetbyvalue($request->status,'all');
        
        $sec = $request->Section;
        
        if(!empty($selectedindustry)){
            $selectedindustry = implode(',',$selectedindustry); 
        } else {
            $selectedindustry = "";
        }
        if(!empty($selectcategory)){
            $selectcategory = implode(',',$selectcategory); 
        } else {
            $selectcategory = "";   
        }
        if(!empty($selectbrands)){
            $selectbrands = implode(',',$selectbrands); 
        } else {
            $selectbrands = ""; 
        }
        if(!empty($selectsubcat)){
            $selectsubcat = implode(',',$selectsubcat); 
        } else {
            $selectsubcat = ""; 
        }
        if(!empty($alluserid)){
            $alluserid = implode(',',$alluserid);   
        } else {
            $alluserid = "";    
        }
        if(!empty($customerdata)){
            $customerdata = implode(',',$customerdata); 
        } else {
            $customerdata = ""; 
        }
        if(!empty($type)){
            $type = implode(',',$type); 
        } else {
            $type = ""; 
        }
        if(!empty($alllocation)){
            $alllocation = implode(',',$alllocation);   
        } else {
            $alllocation = "";  
        }
        if(!empty($status)){
            $status = implode(',',$status); 
        } else {
            $status = "";   
        }
        if (!empty($industry))
        {  

            $result = \App\Industry::where('industryid',$industry)->get();

            $indname=$result[0]->name;

        }

        if (!empty($cat_id))
        {

            $result1 = \App\Category::where('catid',$cat_id)->get(); 

            $catname=$result1[0]->name;
        }
        if (!empty($sub_cat))
        {
            $result2 = \App\SubCategory::where('subcatid',$sub_cat)->get();

            $subcatname=$result2[0]->name;
        }
        if (!empty($bname))
        {

            $result3 = \App\Brand::where('brandid',$bname)->get();

            $brandname=$result3[0]->name;
        }
        $headTitle = '';
        if(isset($filtername)=="all") 
        {

          $headTitle = 'All ' . $sec;

      }
      else
      {

          $headTitle = $sec; 

          if(!empty($indname))
          {
            if(!empty($catname))
            {
              if(!empty($subcatname))
              {
                  if(!empty($brandname))
                  {
                    $headTitle .= "By  Brands";
                }
                else
                {
                   $headTitle .= "By  Subcategory";
               }
           }
           else
           {
              $headTitle .= "By  category";
          }
      }
      else
      {
          $headTitle .= "By  industry";
      }
  } 

}       


$lastselected1 = '';
unset($_GET['addproduct']);
foreach($_GET as $key=>$value){
    if(!empty($_GET["$key"])){
        $lastfilledparam = $key;
    }
}
$renamearray = array(
   'Section'=>'All '.$request->Section,
   'from'=>$request->Section."  By Date",
   'to'=>$request->Section."  By Date",
   'selectedindustry'=>$request->Section." By Industry",
   'selectcategory'=>$request->Section." By Category",
   'selectsubcat'=>$request->Section." By Sub-Category",
   'selectbrands'=>$request->Section." By Brands",
   'searchid'=>$request->Section." By Product Name",
   'customerdata'=>$request->Section." By Customers",
   'status'=>$request->Section." By Status",
   'alluserid'=>$request->Section." By Users",
   'type'=>$request->Section." By Type",
   'alllocation'=>$request->Section." By Location"
   );
$ra = $renamearray[$lastfilledparam];
        // echo $renamearray[$lastfilledparam];die;
$tags = $this->tags();
$data = array(
    'sec'=>$sec,
    'headTitle'=>$headTitle,
    'ra'=>$ra,
    'filtername'=>isset($filtername)?$filtername:'',
    'tags'=>$tags,
    'fromdate'=>$fromdate,
    'todate'=>$todate,
    'selectedindustry'=>$selectedindustry,
    'selectcategory'=>$selectcategory,
    'selectsubcat'=>$selectsubcat,
    'selectbrands'=>$selectbrands,
    'customerdata' => $customerdata,
    'status'=>$status,
    'alluserid'=>$alluserid,
    'type' => $type,
    'alllocation'=>$alllocation        
    );
return View('report.pieChart',$data)->with('title','Ziki Trade::Posting Pie Chart');
}

function unsetbyvalue($array=array(),$value=''){
    if(empty($array))
    {
        $array=array();
    }
    else
    {
        $fields = array_flip($array);
        unset($fields[$value]);
        $fields = array_flip($fields);
        return $fields;
    }
}

public function metroniChartPro(Request $request){
    $result = array();
    $filter  = '';
    $group   = '';
    $orderby = 'ORDER BY brand';
    $sec = $request->section;
    $attribute = '';
    if($request->from != "" && $request->to != "") {
        $from= date("Y-m-d", strtotime($request->from ));;
        $to =  date("Y-m-d", strtotime($request->to ));;
    } else {
        $from = "";
        $to =  "";
    }
    
    $industry= $request->industry;
    $cat_id = $request->cat_id;
    $sub_cat= $request->sub_cat;
    $bname = $request->bname;
    $searchid= $request->searchid;
    $customerdata = $request->customerdata;
    $status= $request->status;
    $alluserid = $request->alluserid;

    if(!empty($request->type))
    {
      $type = $request->type;
  }
  $alllocation = $request->alllocation;


    //$productActionType = $_REQUEST["productActionType"];

  $sec  = (isset($request->section)) ? $request->section : '';
  $filtername  = (isset($request->filtername)) ? $request->filtername : '';
        //die();
  $industry   = (isset($request->industry)) ? $request->industry : '';
  $cat_id   = (isset($request->cat_id)) ? $request->cat_id : '';
  $sub_cat   = (isset($request->sub_cat)) ? $request->sub_cat : '';
  $bname   = (isset($request->bname)) ? $request->bname : '';
  $searchid   = (isset($request->searchid)) ? $request->searchid : '';

  $customerdata   = (isset($request->customerdata)) ? $request->customerdata : '';
  $status   = (isset($request->status)) ? $request->status : '';
  $alluserid   = (isset($request->alluserid)) ? $request->alluserid : '';
  $type   = (isset($request->type)) ? $request->type : '';
  $alllocation   = (isset($request->alllocation)) ? $request->alllocation : '';
  $territory='1=1';


  $territory = "1=1";
  if (empty($from) == false) 
  {
      $territory = $territory ." && pdate between '".$from ."'AND '". $to ." '";
  }
  if (empty($industry) == false )
  {
      $territory = $territory . ' AND industryid IN (' .$industry. ')';
  }
  if (empty($cat_id) == false )
  {
      $territory = $territory . ' AND catid IN ('.$cat_id.')';
  }
  if (empty($sub_cat) == false )
  {
      $territory = $territory . ' AND subcatid IN ('.$sub_cat.')';
  }
  if (empty($bname) == false )
  {
      $territory = $territory . ' AND brandid IN ('.$bname.')';
  }
  if (empty($searchid) == false )
  {
      $territory = $territory . " AND product_name ='".$searchid."'";
  }

  if (empty($customerdata) == false )
  {
      $wherepstatus='';
      $data = explode(',',$customerdata);
      $datacount = count($customerdata);
      $wherepstatus .= " && (";
      foreach($data as $d){
        $wherepstatus .= " customerrefno LIKE '%".$d."%' || ";  
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}

if ((!empty($status)) && ($sec == "Postings") )
{
   $wherepstatus='';
   $data = explode(',',$status);
   $datacount = count($status);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " post_advertisment_pstatus LIKE '%".$d."%' || ";  
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Quotes") )
{
   $wherepstatus='';
   $data = explode(',',$status);
   $datacount = count($status);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Offers") )
{
   $wherepstatus='';
   $data = explode(',',$status);
   $datacount = count($status);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Orders") )
{
   $wherepstatus='';
   $data = explode(',',$status);
   $datacount = count($status);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Customers") )
{
   $wherepstatus='';
   $data = explode(',',$status);
   $datacount = count($status);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " post_advertisment_pstatus LIKE '%".$d."%' || ";  
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 
}


if (empty($alluserid) == false )
{
   $wherepstatus1='';
   $data = explode(',',$alluserid);
   $datacount = count($alluserid);
   $wherepstatus1 .= " && (";
   foreach($data as $d){
    if($sec=="Products")
    {
        $wherepstatus1 .= " product_userid LIKE '%".$d."%' || ";    

    }
    else  
    {
        $wherepstatus1 .= " post_advertisment_userid LIKE '%".$d."%' || ";  
    }
}
$territory = $territory . substr($wherepstatus1,0,-4).")"; 
}



if ((empty($type) == false) && ($sec =='Postings'))
{
   $wherepstatus2='';
   $data = explode(',',$type);
   $datacount = count($type);
   $wherepstatus2 .= " && (";
   foreach($data as $d){
    $wherepstatus2 .= " ptype LIKE '%".$d."%' || "; 
}
$territory = $territory . substr($wherepstatus2,0,-4).")"; 
}




if (empty($alllocation) == false )
{
   $wherepstatus3='';
   $data = explode(',',$alllocation);
   $datacount = count($alllocation);
   $wherepstatus3 .= " && (";
   foreach($data as $d){
    $wherepstatus3 .= " location LIKE '%".$d."%' || ";  
}
$territory = $territory . substr($wherepstatus3,0,-4).")"; 
}

foreach($_POST as $lo=>$do)
{
    if($do!="") {
        $lastvisited=$lo;
        $lastvisitedvalue = $do;
    }
}

if($sec=="Postings") {
    if($lastvisited=='bname'){
        $attribute = " brand_name as productno ";
        $groupby = "brand_name";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry_name as productno ";
        $groupby = "industry_name";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category_name as productno ";
        $groupby = "category_name";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " post_advertisment_pstatus as productno ";
        $groupby = "post_advertisment_pstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }   

    $countpostno = DB::select("SELECT count(*) as postno,$attribute FROM post_products where ".$territory." GROUP BY $groupby");

    if($lastvisitedvalue!="Postings")
        $result = DB::select("SELECT count(*) as postno,$attribute,productno as productno1 FROM post_products where ".$territory." GROUP BY $groupby order by $groupby");

    else
        $result = DB::select("SELECT count(*) as postno FROM post_products order by $groupby ");
} else if($sec=="Products") {

 if($lastvisited=='bname'){
    $attribute = " brand_name as productno ";
    $groupby = "brand_name";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry_name as productno ";
    $groupby = "industry_name";
}
if($lastvisited=='cat_id'){
    $attribute = " category_name as productno ";
    $groupby = "category_name";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory_name as productno ";
    $groupby = "subcategory_name";
}
if($lastvisited=='searchid'){
    $attribute = " product_name as productno ";
    $groupby = "product_name";
}
if($lastvisited=='status'){
    $attribute = " isactive as productno ";
    $groupby = "isactive";
}
if($lastvisited=='alluserid'){
    $attribute = " product_userid as productno ";
    $groupby = "product_userid";
}
$countpostno = \DB::select("SELECT count(*) as postno,$attribute FROM pro_products where ".$territory);

if($lastvisitedvalue!="Products")
{
    $result = \DB::select("SELECT count(*) as postno,$attribute,productno as productno1 FROM pro_products where ".$territory." GROUP BY $groupby order by $groupby");

}
else {

 $result = \DB::select("SELECT count(*) as postno FROM pro_products order by $groupby");

}
} else if($sec=="Quotes"){
    if($lastvisited=='bname'){
        $attribute = " brand as productno ";
        $groupby = "brand";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry as productno ";
        $groupby = "industry";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category as productno ";
        $groupby = "category";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory as productno ";
        $groupby = "subcategory";
    }
    if($lastvisited=='searchid'){
        $attribute = " product as productno ";
        $groupby = "product";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " offerstatus as productno ";
        $groupby = "offerstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }   
    $countpostno = DB::select("SELECT count(*) as postno,$attribute FROM qutation_products where ".$territory);

    $result = DB::select("SELECT count(*) as postno,$attribute,productno as productno1 FROM qutation_products where ".$territory." GROUP BY $groupby order by $groupby");


} else if($sec=="Offers"){
    if($lastvisited=='bname'){
        $attribute = " brand as productno ";
        $groupby = "brand";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry as productno ";
        $groupby = "industry";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category as productno ";
        $groupby = "category";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " offerstatus as productno ";
        $groupby = "offerstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }  
    $countpostno = DB::select("SELECT count(*) as postno,$attribute FROM offer_products where ".$territory.""); 
    $result = DB::select("SELECT count(*) as postno,$attribute,productno as productno1 FROM offer_products where ".$territory." GROUP BY $groupby order by $groupby");

} else if($sec=="Orders"){
    if($lastvisited=='bname'){
        $attribute = " brand as productno ";
        $groupby = "brand";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry as productno ";
        $groupby = "industry";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category as productno ";
        $groupby = "category";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory as productno ";
        $groupby = "subcategory";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " offerstatus as productno ";
        $groupby = "offerstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }   

    $countpostno = DB::select("SELECT count(*) as postno,$attribute FROM oredr_products where ".$territory); 
    $result = DB::select("SELECT count(*) as postno,$attribute,productno as productno1 FROM oredr_products where ".$territory." GROUP BY $groupby order by $groupby");

} else if($sec=="Customers"){
    if($lastvisited=='bname'){
        $attribute = " brand_name as productno ";
        $groupby = "brand_name";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry_name as productno ";
        $groupby = "industry_name";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category_name as productno ";
        $groupby = "category_name";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " post_advertisment_pstatus as productno ";
        $groupby = "post_advertisment_pstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }   

    $countpostno = DB::select("SELECT count(*) as postno,$attribute FROM customer_products where ".$territory); 
    $result = DB::select("SELECT count(*) as postno,$attribute,productno as productno1  FROM customer_products where ".$territory." GROUP BY $groupby order by $groupby");
    
} else if($sec=="Users"){
    if($lastvisited=='bname'){
        $attribute = " brand_name as productno ";
        $groupby = "brand_name";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry as productno ";
        $groupby = "industry";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category_name as productno ";
        $groupby = "category_name";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " offerstatus as productno ";
        $groupby = "offerstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }   

    $countpostno = DB::select("SELECT count(*) as postno,$attribute FROM qutation_products where ".$territory); 
    $result = DB::select("SELECT count(*) as postno,$attribute,productno as productno1 FROM vwpost where ".$territory." GROUP BY $groupby order by $groupby");
}

$rows = array();
$run = array();

foreach($result as $r) {
  $rows = array();
  if($sec=="Products")
  {
    if(($r->productno !='1'))
       $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
   else
       $rows['label'] = (string) 'Active';
}
else if($sec=="Quotes")
   $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
else if($sec=="Offers")
   $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
else if($sec=="Orders")
   $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1"); 
else if($sec=="Postings")
   if($lastvisitedvalue!="Postings")
       $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
   else
       $rows['label'] = (string) 'All'; 
   else if($sec=="Customers")
       $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
   else 
       $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1"); 

   $rows['data'] =   $r->postno;
   $run[] = $rows; 
    // $rows[] = array('c' => $temp);
}
// convert data into JSON format
$jsonTable = json_encode($run);
echo $jsonTable;
}

public function barChartPro(Request $request){

    $result = $finalarray = array();  
    $filter  = '';
    $group   = '';
    $orderby = 'ORDER BY brand';
    $sec = $request->section;
    if($request->from!="" && $request->to!="") {
        $from= date("Y-m-d", strtotime($request->from));;
        $to =  date("Y-m-d", strtotime($request->to));;
    } else {
        $from = "";
        $to =  "";
    }



    $industry= $request->industry;
    $cat_id = $request->cat_id;
    $sub_cat= $request->sub_cat;
    $bname = $request->bname;
    $searchid= $request->searchid;
    $customerdata = $request->customerdata;
    $status= $request->status;
    $alluserid = $request->alluserid;

    if(!empty($request->type))
    {
      $type = $request->type;
  }
  $alllocation = $request->alllocation;


            //$productActionType = $_REQUEST["productActionType"];

  $sec  = (isset($request->section)) ? $request->section : '';
  $filtername  = (isset($request->filtername)) ? $request->filtername : '';
                //die();
  $industry   = (isset($request->industry)) ? $request->industry : '';
  $cat_id   = (isset($request->cat_id)) ? $request->cat_id : '';
  $sub_cat   = (isset($request->sub_cat)) ? $request->sub_cat : '';
  $bname   = (isset($request->bname)) ? $request->bname : '';
  $searchid   = (isset($request->searchid)) ? $request->searchid : '';

  $customerdata   = (isset($request->customerdata)) ? $request->customerdata : '';
  $status   = (isset($request->status)) ? $request->status : '';
  $alluserid   = (isset($request->alluserid)) ? $request->alluserid : '';
  $type   = (isset($request->type)) ? $request->type : '';
  $alllocation   = (isset($request->alllocation)) ? $request->alllocation : '';
  $territory='1=1';


  $territory = "1=1";
  if (empty($from) == false) 
  {
      $territory = $territory ." && pdate between '".$from ."'AND '". $to ." '";
  }
  if (empty($industry) == false )
  {
      $territory = $territory . ' AND industryid IN (' .$industry. ')';
  }
  if (empty($cat_id) == false )
  {
      $territory = $territory . ' AND catid IN ('.$cat_id.')';
  }
  if (empty($sub_cat) == false )
  {
      $territory = $territory . ' AND subcatid IN ('.$sub_cat.')';
  }
  if (empty($bname) == false )
  {
      $territory = $territory . ' AND brandid IN ('.$bname.')';
  }
  if (empty($searchid) == false )
  {
      $territory = $territory . " AND product_name ='".$searchid."'";
  }

  if (empty($customerdata) == false )
  {
                 // $territory = $territory . ' AND customerrefno IN (\''.$customerdata.'\')';
   $wherepstatus='';
   $data = explode(',',$customerdata);
   $datacount = count($customerdata);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " customerrefno LIKE '%".$d."%' || ";  
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 


}
if ((empty($status) == false) && ($sec!="Product") )
{
   $wherepstatus='';
   $data = explode(',',$status);
   $datacount = count($status);
   $wherepstatus .= " && (";
   foreach($data as $d){
    $wherepstatus .= " post_advertisment_pstatus LIKE '%".$d."%' || ";  
}
$territory = $territory . substr($wherepstatus,0,-4).")"; 
}


if (empty($alluserid) == false )
{
   $wherepstatus1='';
   $data = explode(',',$alluserid);
   $datacount = count($alluserid);
   $wherepstatus1 .= " && (";
   foreach($data as $d){

    if($sec=="Products")
    {
        $wherepstatus1 .= " product_userid LIKE '%".$d."%' || ";    

    }
    else  
    {
        $wherepstatus1 .= " post_advertisment_userid LIKE '%".$d."%' || ";  
    }

}
$territory = $territory . substr($wherepstatus1,0,-4).")"; 
}



if ((empty($type) == false) && ($sec =='Postings'))
{
   $wherepstatus2='';
   $data = explode(',',$type);
   $datacount = count($type);
   $wherepstatus2 .= " && (";
   foreach($data as $d){
    $wherepstatus2 .= " ptype LIKE '%".$d."%' || "; 
}
$territory = $territory . substr($wherepstatus2,0,-4).")"; 
}




if (empty($alllocation) == false )
{
   $wherepstatus3='';
   $data = explode(',',$alllocation);
   $datacount = count($alllocation);
   $wherepstatus3 .= " && (";
   foreach($data as $d){
    $wherepstatus3 .= " location LIKE '%".$d."%' || ";  
}
$territory = $territory . substr($wherepstatus3,0,-4).")"; 
}
unset($_POST['filtername']);
foreach($_POST as $lo=>$do)
{
    if($do!="") {
        $lastvisited = $lo;
        $lastvisitedvalue = $do;
    }
}


if($sec=="Postings") {
    if($lastvisited=='bname'){
        $attribute = " brand_name as productno ";
        $groupby = "brand_name";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry_name as productno ";
        $groupby = "industry_name";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category_name as productno ";
        $groupby = "category_name";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " post_advertisment_pstatus as productno ";
        $groupby = "post_advertisment_pstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }     
    if($lastvisitedvalue!="Postings")

        $result = DB::select("SELECT count(*) as postno,$attribute FROM post_products where ".$territory." GROUP BY $groupby order by $groupby LIMIT 0 , 5");

    else
    {
        $result = DB::select("SELECT count(*) as postno FROM post_products LIMIT 0 , 5");

    }

} else if($sec=="Products") {

   if ($lastvisited == 'bname') {
    $attribute = " brand_name as productno ";
    $groupby   = "brand_name";
}
if ($lastvisited == 'to') {
    $attribute = " pdate as productno ";
    $groupby   = "pdate";
}
if ($lastvisited == 'industry') {
    $attribute = " industry_name as productno ";
    $groupby   = "industry_name";
}
if ($lastvisited == 'cat_id') {
    $attribute = " category_name as productno ";
    $groupby   = "category_name";
}
if ($lastvisited == 'sub_cat') {
    $attribute = " subcategory_name as productno ";
    $groupby   = "subcategory_name";
}
if ($lastvisited == 'searchid') {
    $attribute = " product_name as productno ";
    $groupby   = "product_name";
}

if ($lastvisited == 'status') {
    $attribute = " isactive as productno ";
    $groupby   = "isactive";
}
if ($lastvisited == 'alluserid') {
    $attribute = " product_userid as productno ";
    $groupby   = "product_userid";
}

if($lastvisitedvalue!="Products")
    $result = DB::select("SELECT count(*) as postno,$attribute FROM pro_products where ".$territory." GROUP BY $groupby order by $groupby ASC LIMIT 0 , 5");

else
{
    $result = DB::select("SELECT count(*) as postno FROM pro_products LIMIT 0 , 5");

}

} else if($sec=="Quotes"){

    if($lastvisited=='bname'){

        $attribute = " brand_name as productno ";
        $groupby = "brand";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry as productno ";
        $groupby = "industry";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category as productno ";
        $groupby = "category";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory as productno ";
        $groupby = "subcategory";
    }
    if($lastvisited=='searchid'){
        $attribute = " brand as productno ";
        $groupby = "brand";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " offerstatus as productno ";
        $groupby = "offerstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }    

    if($lastvisitedvalue!="Quotes")

        $result = DB::select("SELECT count(*) as postno,$attribute FROM qutation_products where ".$territory." GROUP BY $groupby order by $groupby LIMIT 0 , 5");

    else
    {
        $result = DB::select("SELECT count(*) as postno FROM qutation_products LIMIT 0 , 5");

    }
} else if($sec=="Offers"){

  if($lastvisited=='bname'){
    $attribute = " brand as productno ";
    $groupby = "brand";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry as productno ";
    $groupby = "industry";
}
if($lastvisited=='cat_id'){
    $attribute = " category as productno ";
    $groupby = "category";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory as productno ";
    $groupby = "subcategory";
}
if($lastvisited=='searchid'){
    $attribute = " product_name as productno ";
    $groupby = "product_name";
}
if($lastvisited=='customerdata'){
    $attribute = " customerrefno as productno ";
    $groupby = "customerrefno";
}
if($lastvisited=='status'){
    $attribute = " offerstatus as productno ";
    $groupby = "offerstatus";
}
if($lastvisited=='alluserid'){
    $attribute = " post_advertisment_userid as productno ";
    $groupby = "post_advertisment_userid";
}
if($lastvisited=='type'){
    $attribute = " ptype as productno ";
    $groupby = "ptype";
}
if($lastvisited=='alllocation'){
    $attribute = " location as productno ";
    $groupby = "location";
}    
if($lastvisitedvalue!="Offers")
    $result = DB::select("SELECT count(*) as postno,$attribute FROM offer_products where ".$territory." GROUP BY $groupby order by $groupby LIMIT 0 , 5");

else
{
 $result = DB::select("SELECT count(*) as postno FROM offer_products LIMIT 0 , 5");

}

} else if($sec=="Orders"){
 if($lastvisited=='bname'){

    $attribute = " brand_name as productno ";
    $groupby = "brand_name";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry as productno ";
    $groupby = "industry";
}
if($lastvisited=='cat_id'){
    $attribute = " category as productno ";
    $groupby = "category";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory as productno ";
    $groupby = "subcategory";
}
if($lastvisited=='searchid'){
    $attribute = " brand as productno ";
    $groupby = "brand";
}
if($lastvisited=='customerdata'){
    $attribute = " customerrefno as productno ";
    $groupby = "customerrefno";
}
if($lastvisited=='status'){
    $attribute = " offerstatus as productno ";
    $groupby = "offerstatus";
}
if($lastvisited=='alluserid'){
    $attribute = " post_advertisment_userid as productno ";
    $groupby = "post_advertisment_userid";
}
if($lastvisited=='type'){
    $attribute = " ptype as productno ";
    $groupby = "ptype";
}
if($lastvisited=='alllocation'){
    $attribute = " location as productno ";
    $groupby = "location";
}    

if($lastvisitedvalue!="Orders")
    $result = DB::select("SELECT count(*) as postno,$attribute FROM oredr_products where ".$territory." GROUP BY $groupby order by $groupby LIMIT 0 , 5");

else
{
    $result = DB::select("SELECT count(*) as postno FROM oredr_products LIMIT 0 , 5");

}

} else if($sec=="Customers"){
 if($lastvisited=='bname'){

    $attribute = " brand_name as productno ";
    $groupby = "brand_name";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry_name as productno ";
    $groupby = "industry_name";
}
if($lastvisited=='cat_id'){
    $attribute = " category_name as productno ";
    $groupby = "category_name";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory_name as productno ";
    $groupby = "subcategory_name";
}
if($lastvisited=='searchid'){
    $attribute = " brand_name as productno ";
    $groupby = "brand";
}
if($lastvisited=='customerdata'){
    $attribute = " customerrefno as productno ";
    $groupby = "customerrefno";
}
if($lastvisited=='status'){
    $attribute = " offerstatus as productno ";
    $groupby = "offerstatus";
}
if($lastvisited=='alluserid'){
    $attribute = " post_advertisment_userid as productno ";
    $groupby = "post_advertisment_userid";
}
if($lastvisited=='type'){
    $attribute = " ptype as productno ";
    $groupby = "ptype";
}
if($lastvisited=='alllocation'){
    $attribute = " location as productno ";
    $groupby = "location";
}

if($lastvisitedvalue!="Customers")

    $result = DB::select("SELECT count(*) as postno,$attribute FROM customer_products where ".$territory." GROUP BY $groupby order by $groupby LIMIT 0 , 5");

else
{
    $result = DB::select("SELECT count(*) as postno FROM customer_products LIMIT 0 , 5");

}

} else if($sec=="Users"){

    $result = DB::select("SELECT count(*) as postno,userid FROM vwpost where ".$territory." group by userid LIMIT 0 , 5");

}
            //}
            /*$myFile = "ramaFile16.txt";
            $fh = fopen($myFile, 'w') or die("can't open file");
            fwrite($fh,"SELECT count(*) as postno,productno  FROM post_products where ".$territory." GROUP BY productno order by productno LIMIT 0 , 5");
            fclose($fh);*/
            
            $rows = array();
            $run = array();

            foreach($result as $r) {
              $rows = array();
              if($sec=="Products")
                 if($lastvisitedvalue!="Products")
                    $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1"); 
                else
                {
                  $rows['label'] = (string) 'All'; 
              }
              else if($sec=="Quotes")
              {
                  if(($lastvisitedvalue!="Quotes"))
                      $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1"); 
                  else
                  {
                   $rows['label'] = (string) 'All'; 
               }
           } 
           else if($sec=="Offers")
           {
              if(($lastvisitedvalue!="Offers"))
                  $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
              else
              {
               $rows['label'] = (string) 'All'; 
           }
       } 

       else if($sec=="Orders")
       {
          if(($lastvisitedvalue!="Orders"))
              $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
          else
          {
           $rows['label'] = (string) 'All'; 
       }
   } 
                        //mb_convert_encoding($result2['name'], "HTML-ENTITIES", "ISO-8859-1"); 
   else if($sec=="Postings") {
    if($lastvisitedvalue!="Postings")
        $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
    else
        $rows['label'] = (string) 'All'; 
}
else if($sec=="Customers")
{
 if($lastvisitedvalue!="Customers")
    $rows['label'] = (string) mb_convert_encoding($r->productno, "HTML-ENTITIES", "ISO-8859-1");
else
{
 $rows['label'] = (string) 'All'; 
}
}   
else 
    $rows['label'] = (string) $r['userid']; 

$rows['data'] =  (int) $r->postno;
$data['lable'] = $rows['label'];
$data['count'] = $r->postno;
$finalarray[] = $data;
}

$jsonTable = json_encode($finalarray);
echo $jsonTable;
}

function tags(){
    $tag = '';
    unset($_GET['addproduct']);
    foreach($_GET as $key=>$itesm){
        if($key=="Section"){
            $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$_GET['Section'].'<a href="'.$this->getURL('none').'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
        }
        if($key=="from" && !empty($_GET['from']) && !empty($_GET['to']) ){
            $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$_GET['from'].' - '.$_GET['to'].'<a href="'.$this->getURL('none').'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
        }
        if($key=="selectedindustry"){

            foreach($_GET['selectedindustry'] as $si){

                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$this->getIndustryname($si);
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'[]='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="selectcategory"){
            foreach($_GET['selectcategory'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$this->getCatname($si);
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'[]='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="selectsubcat"){
            foreach($_GET['selectsubcat'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$this->getSubCatname($si);
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'[]='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="selectbrands"){
            foreach($_GET['selectbrands'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$this->getBrandsname($si);
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'[]='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="customerdata"){
            foreach($_GET['customerdata'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$si;
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'%5B%5D='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="status"){
            foreach($_GET['status'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$si;
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'%5B%5D='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="alluserid"){
            foreach($_GET['alluserid'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$si;
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'%5B%5D='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="type"){
            foreach($_GET['type'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$si;
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'%5B%5D='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="alllocation"){
            foreach($_GET['alllocation'] as $si){
                $tag .= '<div style="background-color:#aaaaaa; color:#ffffff; padding:5px;float: left;margin: 5px;">'.$si;
                $tag .= '<a href="'.$this->getURL('selectedindustry',$key.'%5B%5D='.$si).'" style="color:#ffffff; text-decoration:none;"> X </a></div>';
            }
        }
        if($key=="searchid"){
                //$tag .= $_GET['searchid'];
                //$this->getURL('selectedindustry',$key.'='.$_GET['searchid']);
        }
    }
    return $tag;
}

function getURL($section,$substr=''){
    $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    if($section!='none'){
        $url = str_replace($substr,'',$url);
        return $url;
    }

    else{
        return "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'].'/report';
    }
}

public function pie_chart_csv(){
    $file = "postandindustry";
        // $exportData = $_REQUEST['exportData'];
        // $sortData = $_REQUEST['sortData'];
        // $categoryData = $_REQUEST['categoryData'];


        // $customAction = $_REQUEST['sortData'];
        // $exploadData = explode('-', $customAction);


    $output = '';
    $filter  = '';
    $group   = '';
    $orderby = 'ORDER BY brand';
    $sec = $_REQUEST['section'];
    if($_REQUEST['from']!="" && $_REQUEST['to']!="") {
        $from= date("Y-m-d", strtotime($_REQUEST['from']));;
        $to =  date("Y-m-d", strtotime($_REQUEST['to']));;
    } else {
        $from = "";
        $to =  "";
    }



    $industry= $_REQUEST['industry'];
    $cat_id = $_REQUEST['cat_id'];
    $sub_cat= $_REQUEST['sub_cat'];
    $bname = $_REQUEST['bname'];
    $searchid= $_REQUEST['searchid'];
    $customerdata = $_REQUEST['customerdata'];
    $status= $_REQUEST['status'];
    $alluserid = $_REQUEST['alluserid'];

    if(!empty($_REQUEST['type']))
    {
      $type = $_REQUEST['type'];
  }
  $alllocation = $_REQUEST['alllocation'];

  $sec  = (isset($_REQUEST['section'])) ? $_REQUEST['section'] : '';
  $filtername  = (isset($_REQUEST['filtername'])) ? $_REQUEST['filtername'] : '';
                //die();
  $industry   = (isset($_REQUEST['industry'])) ? $_REQUEST['industry'] : '';
  $cat_id   = (isset($_REQUEST['cat_id'])) ? $_REQUEST['cat_id'] : '';
  $sub_cat   = (isset($_REQUEST['sub_cat'])) ? $_REQUEST['sub_cat'] : '';
  $bname   = (isset($_REQUEST['bname'])) ? $_REQUEST['bname'] : '';
  $searchid   = (isset($_REQUEST['searchid'])) ? $_REQUEST['searchid'] : '';

  $customerdata   = (isset($_REQUEST['customerdata'])) ? $_REQUEST['customerdata'] : '';
  $status   = (isset($_REQUEST['status'])) ? $_REQUEST['status'] : '';
  $alluserid   = (isset($_REQUEST['alluserid'])) ? $_REQUEST['alluserid'] : '';
  $type   = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';
  $alllocation   = (isset($_REQUEST['alllocation'])) ? $_REQUEST['alllocation'] : '';
  $territory='1=1';


  unset($_GET['filtername']);
  foreach($_GET as $lo=>$do)
  {
    if($do!="") {
        $lastvisited=$lo;
        $lastvisitedvalue = $do;
    }
}

$territory = "1=1";
if (empty($from) == false) 
{
  $territory = $territory ." && pdate between '".$from ."'AND '". $to ." '";
}
if (empty($industry) == false )
{
  $territory = $territory . ' AND industryid IN (' .$industry. ')';
}
if (empty($cat_id) == false )
{
  $territory = $territory . ' AND catid IN ('.$cat_id.')';
}
if (empty($sub_cat) == false )
{
  $territory = $territory . ' AND subcatid IN ('.$sub_cat.')';
}
if (empty($bname) == false )
{
  $territory = $territory . ' AND brandid IN ('.$bname.')';
}
if (empty($searchid) == false )
{
  $territory = $territory . " AND product_name LIKE '%".$searchid."%'";
}

if (empty($customerdata) == false )
{
                 // $territory = $territory . ' AND customerrefno IN (\''.$customerdata.'\')';

    $data = explode(',',$customerdata);
    $datacount = count($customerdata);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " customerrefno LIKE '%".$d."%' || ";  
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 


}
if ((empty($status) == false) && ($sec=="Product") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " product_status LIKE '%".$d."%' || "; 
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((empty($status)== false) && ($sec =="Postings"))
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " post_advertisment_pstatus ='".$d."' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Quotes") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Offers") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Orders") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Customers") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " post_advertisment_pstatus LIKE '%".$d."%' || ";  
    }

}

if (empty($alluserid) == false )
{
    $data = explode(',',$alluserid);
    $datacount = count($alluserid);
    $wherepstatus1 .= " && (";
    foreach($data as $d){

        if($sec=="Products")
        {
            $wherepstatus1 .= " product_userid LIKE '%".$d."%' || ";    

        }
        else  
        {
            $wherepstatus1 .= " post_advertisment_userid LIKE '%".$d."%' || ";  
        }

    }
    $territory = $territory . substr($wherepstatus1,0,-4).")"; 
}



if ((empty($type) == false) && ($sec =='Postings'))
{
    $data = explode(',',$type);
    $datacount = count($type);
    $wherepstatus2 .= " && (";
    foreach($data as $d){
        $wherepstatus2 .= " ptype LIKE '%".$d."%' || "; 
    }
    $territory = $territory . substr($wherepstatus2,0,-4).")"; 
}




if (empty($alllocation) == false )
{
    $data = explode(',',$alllocation);
    $datacount = count($alllocation);
    $wherepstatus3 .= " && (";
    foreach($data as $d){
        $wherepstatus3 .= " location LIKE '%".$d."%' || ";  
    }
    $territory = $territory . substr($wherepstatus3,0,-4).")"; 
}


if($sec=="Postings") {
    if($lastvisited=='bname'){
        $attribute = " brand_name as productno ";
        $groupby = "brand_name";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry_name as productno ";
        $groupby = "industry_name";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category_name as productno ";
        $groupby = "category_name";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " post_advertisment_pstatus as productno ";
        $groupby = "post_advertisment_pstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }   

    $renamearray1 = array(
       'Section'=>$_GET['section'],
       'from'=>"Date",
       'to'=>"Date",
       'industry'=>"Industry",
       'cat_id'=>"Category",
       'sub_cat'=>"Sub-Category",
       'bname'=>"Brands",
       'searchid'=>"Product Name",
       'customerdata'=>"Customers",
       'status'=>"Status",
       'alluserid'=>"Users",
       'type'=>"Type",
       'alllocation'=>"Location"
       );



     if($lastvisitedvalue!="Postings"){
         $fquery = DB::select("SELECT $attribute, count(*) as  Totalposts,$attribute FROM post_products where ".$territory." GROUP BY  $groupby order by  $groupby");
     }

     else
     {
        $fquery = DB::select("SELECT  count(*) as  Totalposts FROM post_products");

    }
    if($lastvisitedvalue!="Postings")
    {   
        $output.= $renamearray1["$lastvisited"].',';
        $output.= '"Value (%)",';
        $output.= '"Value (#)",';
        $output.= "\n";
    }
    else
    {
        $output.= $_GET['section'].',';
        $output.= '"Value (%)",';
        $output.= '"Value (#)",';
        $output.= "\n";
    }
}
else if($sec=="Products") {

    if ($lastvisited == 'bname') {
        $attribute = " brand_name as productno ";
        $groupby   = "brand_name";
    }
    if ($lastvisited == 'to') {
        $attribute = " pdate as productno ";
        $groupby   = "pdate";
    }
    if ($lastvisited == 'industry') {
        $attribute = " industry_name as productno ";
        $groupby   = "industry_name";
    }
    if ($lastvisited == 'cat_id') {
        $attribute = " category_name as productno ";
        $groupby   = "category_name";
    }
    if ($lastvisited == 'sub_cat') {
        $attribute = " subcategory_name as productno ";
        $groupby   = "subcategory_name";
    }
    if ($lastvisited == 'searchid') {
        $attribute = " product_name as productno ";
        $groupby   = "product_name";
    }

    if ($lastvisited == 'status') {
        $attribute = " isactive as productno ";
        $groupby   = "isactive";
    }
    if ($lastvisited == 'alluserid') {
        $attribute = " product_userid as productno ";
        $groupby   = "product_userid";
    }


    foreach($_GET as $lo=>$do)
    {
        if($do!="")
            $lastvisited=$lo;
    }
    $renamearray1 = array(
       'Section'=>$_GET['section'],
       'from'=>"Date",
       'to'=>"Date",
       'industry'=>"Industry",
       'cat_id'=>"Category",
       'sub_cat'=>"Sub-Category",
       'bname'=>"Brands",
       'searchid'=>"Product Name",
       'customerdata'=>"Customers",
       'status'=>"Status",
       'alluserid'=>"Users",
       'type'=>"Type",
       'alllocation'=>"Location"
       );
        

        if($lastvisitedvalue!="Products")  

             $fquery = DB::select("SELECT $attribute, count(*) as Totalposts,$attribute FROM pro_products 
                where ".$territory." GROUP BY $groupby order by $groupby ASC");
        else
        {
            $fquery =DB::select("SELECT count(*) as Totalposts FROM pro_products"); 
            
        }

        if($lastvisitedvalue!="Products")
        {   
            $output.= $renamearray1["$lastvisited"].',';
            $output.= '"Value (%)",';
            $output.= '"Value (#)",';
            $output.= "\n";
        }
        else
        {
            $output.= $_GET['section'].',';
            $output.= '"Value (%)",';
            $output.= '"Value (#)",';
            $output.= "\n";
        }
    }
                    else if($sec=="Offers") {
                        if($lastvisited=='bname'){

                            $attribute = " brand as productno ";
                            $groupby = "brand";
                        }
                        if($lastvisited=='to'){
                            $attribute = " pdate as productno ";
                            $groupby = "pdate";
                        }
                        if($lastvisited=='industry'){
                            $attribute = " industry as productno ";
                            $groupby = "industry";
                        }
                        if($lastvisited=='cat_id'){
                            $attribute = " category as productno ";
                            $groupby = "category";
                        }
                        if($lastvisited=='sub_cat'){
                            $attribute = " subcategory as productno ";
                            $groupby = "subcategory";
                        }
                        if($lastvisited=='searchid'){
                            $attribute = " brand as productno ";
                            $groupby = "brand";
                        }
                        if($lastvisited=='customerdata'){
                            $attribute = " customerrefno as productno ";
                            $groupby = "customerrefno";
                        }
                        if($lastvisited=='status'){
                            $attribute = " offerstatus as productno ";
                            $groupby = "offerstatus";
                        }
                        if($lastvisited=='alluserid'){
                            $attribute = " post_advertisment_userid as productno ";
                            $groupby = "post_advertisment_userid";
                        }
                        if($lastvisited=='type'){
                            $attribute = " ptype as productno ";
                            $groupby = "ptype";
                        }
                        if($lastvisited=='alllocation'){
                            $attribute = " location as productno ";
                            $groupby = "location";
                        }


                        foreach($_GET as $lo=>$do)
                        {
                            if($do!="")
                                $lastvisited=$lo;
                        }
                        $renamearray1 = array(
                           'Section'=>$_GET['section'],
                           'from'=>"Date",
                           'to'=>"Date",
                           'industry'=>"Industry",
                           'cat_id'=>"Category",
                           'sub_cat'=>"Sub-Category",
                           'bname'=>"Brands",
                           'searchid'=>"Product Name",
                           'customerdata'=>"Customers",
                           'status'=>"Status",
                           'alluserid'=>"Users",
                           'type'=>"Type",
                           'alllocation'=>"Location"
                           );
                        if($lastvisitedvalue!="Offers") 
                        { 
                         $fquery =DB::select("SELECT $attribute, count(*) as Totalposts,$attribute FROM offer_products where ".$territory." GROUP BY $groupby order by $groupby ASC");
                         $output.= $renamearray1["$lastvisited"].',';
                         $output.= '"Value (%)",';
                         $output.= '"Value (#)",';
                         $output.= "\n";
                     }
                     else
                     {
                        $fquery =DB::select("SELECT count(*) as Totalposts  FROM offer_products");
                        $output.= $_GET['section'].',';
                        $output.= '"Value (%)",';
                        $output.= '"Value (#)",';
                        $output.= "\n";
                    }
                }
                else if($sec=="Quotes") {

                    if($lastvisited=='bname'){

                        $attribute = " brand_name as productno ";
                        $groupby = "brand_name";
                    }
                    if($lastvisited=='to'){
                        $attribute = " pdate as productno ";
                        $groupby = "pdate";
                    }
                    if($lastvisited=='industry'){
                        $attribute = " industry as productno ";
                        $groupby = "industry";
                    }
                    if($lastvisited=='cat_id'){
                        $attribute = " category as productno ";
                        $groupby = "category";
                    }
                    if($lastvisited=='sub_cat'){
                        $attribute = " subcategory as productno ";
                        $groupby = "subcategory";
                    }
                    if($lastvisited=='searchid'){
                        $attribute = " brand as productno ";
                        $groupby = "brand";
                    }
                    if($lastvisited=='customerdata'){
                        $attribute = " customerrefno as productno ";
                        $groupby = "customerrefno";
                    }
                    if($lastvisited=='status'){
                        $attribute = " offerstatus as productno ";
                        $groupby = "offerstatus";
                    }
                    if($lastvisited=='alluserid'){
                        $attribute = " post_advertisment_userid as productno ";
                        $groupby = "post_advertisment_userid";
                    }
                    if($lastvisited=='type'){
                        $attribute = " ptype as productno ";
                        $groupby = "ptype";
                    }
                    if($lastvisited=='alllocation'){
                        $attribute = " location as productno ";
                        $groupby = "location";
                    }


                    foreach($_GET as $lo=>$do)
                    {
                        if($do!="")
                            $lastvisited=$lo;
                    }
                    $renamearray1 = array(
                       'Section'=>$_GET['section'],
                       'from'=>"Date",
                       'to'=>"Date",
                       'industry'=>"Industry",
                       'cat_id'=>"Category",
                       'sub_cat'=>"Sub-Category",
                       'bname'=>"Brands",
                       'searchid'=>"Product Name",
                       'customerdata'=>"Customers",
                       'status'=>"Status",
                       'alluserid'=>"Users",
                       'type'=>"Type",
                       'alllocation'=>"Location"
                       );
                    if($lastvisitedvalue!="Quotes")  
                     $fquery =DB::select("SELECT $attribute,count(*) as Totalposts,$attribute FROM qutation_products where ".$territory." GROUP BY $groupby order by $groupby ASC");
                 else{
                     $fquery =DB::select("SELECT count(*) as Totalposts FROM qutation_products");
                 }
                 if($lastvisitedvalue!="Quotes")
                 {
                    $output.= $renamearray1["$lastvisited"].',';
                    $output.= '"Value (%)",';
                    $output.= '"Value (#)",';
                    $output.= "\n";
                }
                else
                {
                    $output.= $_GET['section'].',';
                    $output.= '"Value (%)",';
                    $output.= '"Value (#)",';
                    $output.= "\n";
                }
            }
            else if($sec=="Orders") {
                if($lastvisited=='bname'){

                    $attribute = " brand as productno ";
                    $groupby = "brand";
                }
                if($lastvisited=='to'){
                    $attribute = " pdate as productno ";
                    $groupby = "pdate";
                }
                if($lastvisited=='industry'){
                    $attribute = " industry as productno ";
                    $groupby = "industry";
                }
                if($lastvisited=='cat_id'){
                    $attribute = " category as productno ";
                    $groupby = "category";
                }
                if($lastvisited=='sub_cat'){
                    $attribute = " subcategory as productno ";
                    $groupby = "subcategory";
                }
                if($lastvisited=='searchid'){
                    $attribute = " brand as productno ";
                    $groupby = "brand";
                }
                if($lastvisited=='customerdata'){
                    $attribute = " customerrefno as productno ";
                    $groupby = "customerrefno";
                }
                if($lastvisited=='status'){
                    $attribute = " offerstatus as productno ";
                    $groupby = "offerstatus";
                }
                if($lastvisited=='alluserid'){
                    $attribute = " post_advertisment_userid as productno ";
                    $groupby = "post_advertisment_userid";
                }
                if($lastvisited=='type'){
                    $attribute = " ptype as productno ";
                    $groupby = "ptype";
                }
                if($lastvisited=='alllocation'){
                    $attribute = " location as productno ";
                    $groupby = "location";
                }

                
                foreach($_GET as $lo=>$do)
                {
                    if($do!="")
                        $lastvisited=$lo;
                }
                $renamearray1 = array(
                   'Section'=>$_GET['section'],
                   'from'=>"Date",
                   'to'=>"Date",
                   'industry'=>"Industry",
                   'cat_id'=>"Category",
                   'sub_cat'=>"Sub-Category",
                   'bname'=>"Brands",
                   'searchid'=>"Product Name",
                   'customerdata'=>"Customers",
                   'status'=>"Status",
                   'alluserid'=>"Users",
                   'type'=>"Type",
                   'alllocation'=>"Location"
                   );
                if($lastvisitedvalue!="Orders")
                {
                 $fquery =DB::select("SELECT $attribute,count(*) as Totalposts,$attribute  FROM oredr_products where ".$territory." GROUP BY $groupby order by $groupby ASC");
                 $output.= $renamearray1["$lastvisited"].',';
                 $output.= '"Value (%)",';
                 $output.= '"Value (#)",';
                 $output.= "\n";
             }
             else
             {
                $fquery =DB::select("SELECT count(*) as Totalposts  FROM oredr_products");
                $output.= $_GET['section'].',';
                $output.= '"Value (%)",';
                $output.= '"Value (#)",';
                $output.= "\n"; 
            }
        }
        else if($sec=="Customers") {
            if($lastvisited=='bname'){

                $attribute = " brand_name as productno ";
                $groupby = "brand_name";
            }
            if($lastvisited=='to'){
                $attribute = " pdate as productno ";
                $groupby = "pdate";
            }
            if($lastvisited=='industry'){
                $attribute = " industry_name as productno ";
                $groupby = "industry_name";
            }
            if($lastvisited=='cat_id'){
                $attribute = " category_name as productno ";
                $groupby = "category_name";
            }
            if($lastvisited=='sub_cat'){
                $attribute = " subcategory_name as productno ";
                $groupby = "subcategory_name";
            }
            if($lastvisited=='searchid'){
                $attribute = " brand_name as productno ";
                $groupby = "brand_name";
            }
            if($lastvisited=='customerdata'){
                $attribute = " customerrefno as productno ";
                $groupby = "customerrefno";
            }
            if($lastvisited=='status'){
                $attribute = " post_advertisment_pstatus as productno ";
                $groupby = "post_advertisment_pstatus";
            }
            if($lastvisited=='alluserid'){
                $attribute = " post_advertisment_userid as productno ";
                $groupby = "post_advertisment_userid";
            }
            if($lastvisited=='type'){
                $attribute = " ptype as productno ";
                $groupby = "ptype";
            }
            if($lastvisited=='alllocation'){
                $attribute = " location as productno ";
                $groupby = "location";
            }    


            foreach($_GET as $lo=>$do)
            {
                if($do!="")
                    $lastvisited=$lo;
            }
            $renamearray1 = array(
               'Section'=>$_GET['section'],
               'from'=>"Date",
               'to'=>"Date",
               'industry'=>"Industry",
               'cat_id'=>"Category",
               'sub_cat'=>"Sub-Category",
               'bname'=>"Brands",
               'searchid'=>"Product Name",
               'customerdata'=>"Customers",
               'status'=>"Status",
               'alluserid'=>"Users",
               'type'=>"Type",
               'alllocation'=>"Location"
               );
            if($lastvisitedvalue!="Customers")
            {
                $fquery = DB::select("SELECT $attribute,count(*) as Totalposts,$attribute FROM customer_products  where ".$territory." GROUP BY $groupby order by $groupby ASC");
                $output.= $renamearray1["$lastvisited"].',';
                $output.= '"Value (%)",';
                $output.= '"Value (#)",';
                $output.= "\n";
            }
            else
            {
                $fquery = DB::select("SELECT count(*) as Totalposts FROM customer_products");
                $output.= $_GET['section'].',';
                $output.= '"Value (%)",';
                $output.= '"Value (#)",';
                $output.= "\n"; 
            }
        }
        else if($sec=="Users") {
         $fquery =DB::select("SELECT userid as industryname,COUNT( * ) AS Totalposts FROM vwpost where ".$territory." group by userid order by userid");
         $output.= $renamearray1["$lastvisited"].',';
         $output.= '"Value (%)",';
         $output.= '"Value (#)",';
         $output.= "\n";

     }


     if (!empty($fquery))
     {
            //$output.= '"Product_Codes",';
        $temp = '';
        $count=0;
        $tnoofposts='';

        foreach ($fquery as $row1) {

            $count = $count + $row1->Totalposts;
        
        }

        foreach ($fquery as $row) {
            if($sec=="Postings")
            {
                if($lastvisitedvalue!="Postings")
                {
                    $output.= '"' . $row->productno . '",'; 
                    $output.= '"' . number_format((($row->Totalposts/$count)*100),2) . '%",';
                    $output.= '"' .$row->Totalposts. '",';
                }
                else
                {
                    $output.= '"'. All. '",';   
                    $output.= '"' . 100 . '%",';
                    $output.= '"' . $row->productno . '",';
                }
                $output.= "\n";
            }
            else if($sec=="Products")
            {
                if($lastvisitedvalue!="Products")
                {
                    $output.= '"' . $row->productno . '",'; 
                    $output.= '"' . number_format((($row->Totalposts/$count)*100),2) . '%",';
                    $output.= '"' .$row->Totalposts. '",';
                }
                else
                {
                    $output.= '"'. All. '",';   
                    $output.= '"' . 100 . '%",';
                    $output.= '"' . $row->productno . '",';
                }
                $output.= "\n";
            }
            else if($sec=="Quotes")
            {        
                if($lastvisitedvalue!="Quotes")
                {
                    $output.= '"' . $row->productno . '",'; 
                    $output.= '"' . number_format((($row->Totalposts/$count)*100),2) . '%",';
                    $output.= '"' .$row->Totalposts. '",';
                }
                else
                {
                    $output.= '"'. All. '",';   
                    $output.= '"' . 100 . '%",';
                    $output.= '"' . $row->productno . '",';
                }
                $output.= "\n";
            }
            else if($sec=="Offers")
            {
                if($lastvisitedvalue!="Offers")
                {
                    $output.= '"' . $row->productno . '",'; 
                    $output.= '"' . number_format((($row->Totalposts/$count)*100),2) . '%",';
                    $output.= '"' .$row->Totalposts. '",';
                }
                else
                {
                    $output.= '"'. All. '",';   
                    $output.= '"' . 100 . '%",';
                    $output.= '"' . $row->productno . '",';
                }
                $output.= "\n";
            }
            else if($sec=="Customers")
            {
                if($lastvisitedvalue!="Customers")
                {
                    $output.= '"' . $row->productno . '",'; 
                    $output.= '"' . number_format((($row->Totalposts/$count)*100),2) . '%",';
                    $output.= '"' .$row->Totalposts. '",';
                }
                else
                {
                    $output.= '"'. All. '",';   
                    $output.= '"' . 100 . '%",';
                    $output.= '"' . $row->productno . '",';
                }
                $output.= "\n";
            }
            else if($sec=="Orders")
            {
                if($lastvisitedvalue!="Orders")
                {
                    $output.= '"' . $row->productno . '",'; 
                    $output.= '"' . number_format((($row->Totalposts/$count)*100),2) . '%",';
                    $output.= '"' .$row->productno. '",';
                }
                else
                {
                    $output.= '"'. All. '",';   
                    $output.= '"' . 100 . '%",';
                    $output.= '"' . $row->productno . '",';
                }
                $output.= "\n";
            }
        }

        $filename = $file . "_" . date("Y-m-d_H-i", time()) . '.csv';
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: csv" . date("Y-m-d") . ".csv");
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        return $output;
        
    }
    else
    {
        echo '<script language="javascript">';
        echo 'alert("Data is not found");';

        echo '</script>';
        return redirect('/posting/myPosting');
    }
}

public function posting_pie_chart_ajax( Request $request ){

    $sec = $request->section;
    $wherepstatus = '';
    $wherepstatus1 = '';
    $wherepstatus2 = '';
    $wherepstatus3 = '';
    if($request->from != "" && $request->to != "") {
        $from= date("Y-m-d", strtotime($request->from));;
        $to =  date("Y-m-d", strtotime($request->to));;
    } else {
        $from = "";
        $to =  "";
    }

    $industry= $request->input('industry');
    $cat_id = $request->input('cat_id');
    $sub_cat= $request->input('sub_cat');
    $bname = $request->input('bname');
    $searchid= $request->input('searchid');
    $customerdata = $request->input('refno');
    $status= $request->input('status');
    $alluserid = $request->input('alluserid');

    if(!empty($request->input('type')))
    {
      $type = $request->input('type');
  }

  $alllocation = $request->input('alllocation');

  $productActionType = $request->input('productActionType');

  $sec  = (!empty($request->input('section'))) ? $request->input('section') : '';
  $filtername  = (!empty($request->input('filtername'))) ? $request->input('filtername') : '';
  $industry   = (!empty($request->input('industry'))) ? $request->input('industry') : '';
  $cat_id   = (!empty($request->input('cat_id'))) ? $request->input('cat_id') : '';
  $sub_cat   = (!empty($request->input('sub_cat'))) ? $request->input('sub_cat') : '';
  $bname   = (!empty($request->input('bname'))) ? $request->input('bname') : '';
  $searchid   = (!empty($request->input('searchid'))) ? $request->input('searchid') : '';

  $customerdata   = (!empty($request->input('customerdata'))) ? $request->input('customerdata') : '';
  $status   = (!empty($request->input('status'))) ? $request->input('status') : '';
  $alluserid   = (!empty($request->input('alluserid'))) ? $request->input('alluserid') : '';
  $type   = (!empty($request->input('type'))) ? $request->input('type') : '';
  $alllocation   = (!empty($request->input('alllocation'))) ? $request->input('alllocation') : '';
  $territory='1=1';

  if (empty($from) == false) 
  {
    $territory = $territory ." && pdate between '".$from ."'AND '". $to ." '";
}
if (empty($industry) == false )
{
    $territory = $territory . ' AND industryid IN (' .$industry. ')';
}
if (empty($cat_id) == false )
{
    $territory = $territory . ' AND catid IN ('.$cat_id.')';
}
if (empty($sub_cat) == false )
{
    $territory = $territory . ' AND subcatid IN ('.$sub_cat.')';
}
if (empty($bname) == false )
{
    $territory = $territory . ' AND brandid IN ('.$bname.')';
}
if (empty($searchid) == false )
{
    $territory = $territory . " AND product_name ='".$searchid."'";
}

if (!empty($customerdata))
{
    $data = explode(',',$customerdata);
    $datacount = count($customerdata);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " customerrefno = '".$d."' || ";   
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((empty($status)== false) && ($sec =="Postings"))
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " post_advertisment_pstatus ='".$d."' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Quotes") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Offers") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Orders") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " offerstatus LIKE '%".$d."%' || ";    
    }
    $territory = $territory . substr($wherepstatus,0,-4).")"; 
}
if ((!empty($status)) && ($sec == "Customers") )
{
    $data = explode(',',$status);
    $datacount = count($status);
    $wherepstatus .= " && (";
    foreach($data as $d){
        $wherepstatus .= " post_advertisment_pstatus LIKE '%".$d."%' || ";  
    }

}


if (empty($alluserid) == false )
{
    $data = explode(',',$alluserid);
    $datacount = count($alluserid);
    $wherepstatus1 .= " && (";
    foreach($data as $d){
        if($sec=="Products")
        {
            $wherepstatus1 .= " product_userid LIKE '%".$d."%' || ";    

        }
        else  
        {
            $wherepstatus1 .= " post_advertisment_userid LIKE '%".$d."%' || ";  
        }
    }
    $territory = $territory . substr($wherepstatus1,0,-4).")"; 
}



if ((empty($type) == false) && ($sec =='Postings'))
{
    $data = explode(',',$type);
    $datacount = count($type);
    $wherepstatus2 .= " && (";
    foreach($data as $d){
        $wherepstatus2 .= " ptype LIKE '%".$d."%' || "; 
    }
    $territory = $territory . substr($wherepstatus2,0,-4).")"; 
}




if (empty($alllocation) == false )
{
    $data3 = explode(',',$alllocation);
    $datacount = count($alllocation);
    $wherepstatus3 .= " && (";
    foreach($data3 as $d3){
        $wherepstatus3 .= " location LIKE '%".$d3."%' || "; 
    }
    $territory = $territory . substr($wherepstatus3,0,-4).")"; 
}

setcookie('territory', $territory, time() + (86400 * 30),"/");


$attribute = '';
unset($_GET['filtername']);
foreach($_GET as $lo=>$do)
{
    if($do!="") {
        $lastvisited=$lo;
        $lastvisitedvalue = $do;
    }
}

/*************************Posting report start*****************************************************/


if($sec=="Postings")
{
    if($lastvisited=='bname'){

        $attribute = " brand_name as productno ";
        $groupby = "brand_name";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry_name as productno ";
        $groupby = "industry_name";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category_name as productno ";
        $groupby = "category_name";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory_name as productno ";
        $groupby = "subcategory_name";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " post_advertisment_pstatus as productno ";
        $groupby = "post_advertisment_pstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }    

    if($lastvisitedvalue!="Postings"){
        $sql = \DB::select("SELECT count(*) as name ,$attribute,productno as productno1  FROM post_products where ".$territory." GROUP BY $groupby order by $groupby");
    }
    else
    {
      $sql = \DB::select("SELECT count(*) as name  FROM post_products");    
  }



  $sql1 = \DB::select("select  count(*) as name from post_products where ".$territory."");

  $temp=0;
  foreach( $sql1 as $row1 ){

    $industryname1 = $row1->name;
    $temp=$temp+$industryname1;

}

$records = array();
$i = 0;

foreach( $sql as $row )
{
    $industryname = $row->name;
    $industryid=isset($row->productno)?$row->productno:'';

    if($lastvisitedvalue!="Postings")
        $productno =isset($row->productno)?$row->productno:'';
    else 
        $productno ='All';
    $productno1 =isset($row->productno1)?$row->productno1:'';
    $name1 =$productno.'
    <a href="'.url('/').'/report/chartDetails?q=' .urlencode($productno). '&view_type='.$sec.'&clauseby='.(isset($groupby)?$groupby:"").'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
    $per = ($industryname/$temp)*100;

    if($lastvisitedvalue!="Postings")
        $postpercent = number_format($per,2).'%';
    else
        $postpercent = '100%';
    $records[$i]['name1'] = $name1;
    $records[$i]['postpercent'] = $postpercent;
    $records[$i]['industryname'] = $industryname;
    $i++;
}


return \Response::json($records);

}  
/***************************Posting report end******************************************/

/*************************Products report start*****************************************************/   
if ($sec == "Products") {

    if ($lastvisited == 'bname') {
        $attribute = " brand_name as productno ";
        $groupby   = "brand_name";
    }
    if ($lastvisited == 'to') {
        $attribute = " pdate as productno ";
        $groupby   = "pdate";
    }
    if ($lastvisited == 'industry') {
        $attribute = " industry_name as productno ";
        $groupby   = "industry_name";
    }
    if ($lastvisited == 'cat_id') {
        $attribute = " category_name as productno ";
        $groupby   = "category_name";
    }
    if ($lastvisited == 'sub_cat') {
        $attribute = " subcategory_name as productno ";
        $groupby   = "subcategory_name";
    }
    if ($lastvisited == 'searchid') {
        $attribute = " product_name as productno ";
        $groupby   = "product_name";
    }
    
    if ($lastvisited == 'status') {
        $attribute = " isactive as productno ";
        $groupby   = "isactive";
    }
    if ($lastvisited == 'alluserid') {
        $attribute = " product_userid as productno ";
        $groupby   = "product_userid";
    }
    
    if ($lastvisitedvalue != "Products"){
        $sql    = \DB::select("SELECT count(*) as name,product_name as pname,$attribute,productno as productno1,industryid,industry_name  FROM pro_products where " . $territory . " GROUP BY $groupby order by $groupby ASC");
    }
    else
    {
        $sql    = \DB::select("SELECT count(*) as name,product_name as pname,productno as productno1,industryid,industry_name FROM pro_products");
    }

    $sql2 = \DB::select("select  count(*) as name from pro_products where " . $territory . "");
    
    $temp = 0;

    foreach ($sql2 as $row1) {
        $industryname1 = $row1->name;
        $temp          = $temp + $industryname1;
    }
    $records = array();
    $i = 0;
    foreach ($sql as $row) {
        $industryname = isset($row->name)?$row->name:'';
        $industryid   = isset($row->industryid)?$row->industryid:'';
        $bhak         = '';
        if ($lastvisitedvalue != "Products"){
            $productno = isset($row->productno)?$row->productno:'';
        }
        else{
            $productno = 'All';
        }
        $productno1 = isset($row->productno1)?$row->productno1:'';
        
        $name1      = $productno. '<a href="'.url('/').'/report/chartDetails?q=' .urlencode($productno). '&view_type='.$sec.'&clauseby='.(isset($groupby)?$groupby:"").'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
        
        $per         = ($industryname / $temp) * 100;
        if ($lastvisitedvalue != "Products"){
            $postpercent = number_format($per, 2) . '%';    
        }
        
        else{
            $postpercent = '100%';    
        }
        
        
        $records[$i]['name1'] = $name1;
        $records[$i]['postpercent'] = $postpercent;
        $records[$i]['industryname'] = $industryname; 

        $i++; 
    }

    return \Response::json($records);
    
}
/***************************Products report end******************************************/







/*************************Quotes report start*****************************************************/   
if($sec=="Quotes"){ 


    if($lastvisited=='bname'){

        $attribute = " brand as productno ";
        $groupby = "brand";
    }
    if($lastvisited=='to'){
        $attribute = " pdate as productno ";
        $groupby = "pdate";
    }
    if($lastvisited=='industry'){
        $attribute = " industry as productno ";
        $groupby = "industry";
    }
    if($lastvisited=='cat_id'){
        $attribute = " category as productno ";
        $groupby = "category";
    }
    if($lastvisited=='sub_cat'){
        $attribute = " subcategory as productno ";
        $groupby = "subcategory";
    }
    if($lastvisited=='searchid'){
        $attribute = " product_name as productno ";
        $groupby = "product_name";
    }
    if($lastvisited=='customerdata'){
        $attribute = " customerrefno as productno ";
        $groupby = "customerrefno";
    }
    if($lastvisited=='status'){
        $attribute = " offerstatus as productno ";
        $groupby = "offerstatus";
    }
    if($lastvisited=='alluserid'){
        $attribute = " post_advertisment_userid as productno ";
        $groupby = "post_advertisment_userid";
    }
    if($lastvisited=='type'){
        $attribute = " ptype as productno ";
        $groupby = "ptype";
    }
    if($lastvisited=='alllocation'){
        $attribute = " location as productno ";
        $groupby = "location";
    }    
    

    if ($lastvisitedvalue !="Quotes"){
        $sql = \DB::select("SELECT count(*) as name,postno,$attribute,productno as productno1 FROM qutation_products where ".$territory." GROUP BY $groupby order by $groupby");
    }
    
    else
    {
        $sql = \DB::select("SELECT count(*) as name,postno,productno as productno1  FROM qutation_products");
    }

    $sql2 = \DB::select("select  count(*) as name FROM qutation_products where ".$territory."");

    $temp=0;

    foreach ($sql2 as $row1) {
      $industryname1 = isset($row1->name)?$row1->name:'';

      $temp=$temp+$industryname1;
  }

  $records = array();
  $i = 0;

  foreach ($sql as $row) {
    $industryname = $row->name;
    $industryid=isset($row->postno)?$row->postno:'';
    $bhak = '';
    $postno = isset($row->postno)?$row->postno:'';          

    if ($lastvisitedvalue != "Quotes"){
        $productno = isset($row->productno)?$row->productno:''; 
    }

    else
    {
        $productno = 'All';
    }

    $name1 = $productno.'<a href="'.url('/').'/report/chartDetails?q=' .urlencode($productno). '&view_type='.$sec.'&clauseby='.(isset($groupby)?$groupby:"").'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';

    $per = ($industryname/($temp == 0)?1:$temp)*100;

    if ($lastvisitedvalue != "Quotes")
        $postpercent = number_format($per,2).'%';
    else
    {
        $postpercent ='100%';
    }


    $records[$i]['name1'] = $name1;
    $records[$i]['postpercent'] = $postpercent;
    $records[$i]['industryname'] = $industryname;

    $i++;


}

return \Response::json($records);

}  
/***************************Quotes report end******************************************/







/*************************Offers report start*****************************************************/   
if($sec=="Offers")
{ 

   if($lastvisited=='bname'){

    $attribute = " brand as productno ";
    $groupby = "brand";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry as productno ";
    $groupby = "industry";
}
if($lastvisited=='cat_id'){
    $attribute = " category as productno ";
    $groupby = "category";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory as productno ";
    $groupby = "subcategory";
}
if($lastvisited=='searchid'){
    $attribute = " brand as productno ";
    $groupby = "brand";
}
if($lastvisited=='customerdata'){
    $attribute = " customerrefno as productno ";
    $groupby = "customerrefno";
}
if($lastvisited=='status'){
    $attribute = " offerstatus as productno ";
    $groupby = "offerstatus";
}
if($lastvisited=='alluserid'){
    $attribute = " post_advertisment_userid as productno ";
    $groupby = "post_advertisment_userid";
}
if($lastvisited=='type'){
    $attribute = " ptype as productno ";
    $groupby = "ptype";
}
if($lastvisited=='alllocation'){
    $attribute = " location as productno ";
    $groupby = "location";
}    

if ($lastvisitedvalue != "Offers")
    $sql = \DB::select("SELECT count(*) as name,postno,$attribute,productno as productno1  FROM offer_products where ".$territory." GROUP BY $groupby  order by $groupby");
else
{
    $sql = \DB::select("SELECT count(*) as name,postno,productno as productno1  FROM offer_products");
}


$sql2 = \DB::select("select  count(*) as name FROM offer_products where ".$territory."");


$temp=0;

foreach ($sql2 as $row1) {
    $industryname1 = isset($row1->name)?$row1->name:'';
    $temp=$temp+$industryname1;
}
$records = array();
$i = 0;
foreach ($sql as $row) {
    $industryname = $row->name;
    $industryid=isset($row->postno)?$row->postno:'';
    $postno = isset($row->postno)?$row->postno:'';

    if ($lastvisitedvalue != "Offers")
     $productno = isset($row->productno)?$row->productno:'';
 else
 {
    $productno = 'All';
}

$name1 =$productno.'<a href="'.url('/').'/report/chartDetails?q=' .urlencode($productno). '&view_type='.$sec.'&clauseby='.(isset($groupby)?$groupby:"").'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';

$per = ($industryname/(($temp==0)?1:$temp))*100;
if ($lastvisitedvalue != "Offers")
    $postpercent = number_format($per,2).'%';
else
{
  $postpercent = '100%';
}

$records[$i]['name1'] = $name1;
$records[$i]['postpercent'] = $postpercent;
$records[$i]['industryname'] = $industryname;
$i++;
}

return \Response::json($records);

}  
/***************************Offers report end******************************************/



/*************************Orders report start*****************************************************/   
if($sec=="Orders")
{ 

  if($lastvisited=='bname'){

    $attribute = " brand as productno ";
    $groupby = "brand";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry as productno ";
    $groupby = "industry";
}
if($lastvisited=='cat_id'){
    $attribute = " category as productno ";
    $groupby = "category";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory as productno ";
    $groupby = "subcategory";
}
if($lastvisited=='searchid'){
    $attribute = " brand as productno ";
    $groupby = "brand";
}
if($lastvisited=='customerdata'){
    $attribute = " customerrefno as productno ";
    $groupby = "customerrefno";
}
if($lastvisited=='status'){
    $attribute = " offerstatus as productno ";
    $groupby = "offerstatus";
}
if($lastvisited=='alluserid'){
    $attribute = " post_advertisment_userid as productno ";
    $groupby = "post_advertisment_userid";
}
if($lastvisited=='type'){
    $attribute = " ptype as productno ";
    $groupby = "ptype";
}
if($lastvisited=='alllocation'){
    $attribute = " location as productno ";
    $groupby = "location";
}    


if ($lastvisitedvalue != "Orders"){
    $sql = \DB::select("SELECT count(*) as name,postno,$attribute,productno as productno1   FROM oredr_products where ".$territory." GROUP BY $groupby order by $groupby");
}
else
{
    $sql = \DB::select("SELECT count(*) as name,postno,productno as productno1  FROM oredr_products");
}

$sql2 = \DB::select("select  count(*) as name  FROM oredr_products where ".$territory."");

$temp=0;

foreach ($sql2 as $row1) {        
    $industryname1 = isset($row1->name)?$row1->name:'';
    $temp=$temp+$industryname1;
}

$records = array();
$i = 0;

foreach ($sql as $row) {
    $industryname = isset($row->name)?$row->name:'';
    $industryid=isset($row->postno)?$row->postno:'';

    $postno = isset($row->postno)?$row->postno:'';

    if ($lastvisitedvalue != "Orders")
     $productno = isset($row->productno)?$row->productno:'';
 else
 {
    $productno = 'All';
}


$name1 =$productno.'
<a href="'.url('/').'/report/chartDetails?q=' .urlencode($productno). '&view_type='.$sec.'&clauseby='.(isset($groupby)?$groupby:"").'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';

$per = ($industryname/(($temp==0)?1:$temp))*100;
if ($lastvisitedvalue != "Orders")
    $postpercent = number_format($per,2).'%';
else
{
    $postpercent ='100%';
}

$records[$i]['name1'] = $name1;
$records[$i]['postpercent'] = $postpercent;
$records[$i]['industryname'] = $industryname;

$i++;

}

return \Response::json($records);

}  
/***************************Orders report end******************************************/



/*************************Customers report start*****************************************************/   
if($sec=="Customers")
{ 
  if($lastvisited=='bname'){

    $attribute = " brand_name as productno ";
    $groupby = "brand_name";
}
if($lastvisited=='to'){
    $attribute = " pdate as productno ";
    $groupby = "pdate";
}
if($lastvisited=='industry'){
    $attribute = " industry_name as productno ";
    $groupby = "industry_name";
}
if($lastvisited=='cat_id'){
    $attribute = " category_name as productno ";
    $groupby = "category_name";
}
if($lastvisited=='sub_cat'){
    $attribute = " subcategory_name as productno ";
    $groupby = "subcategory_name";
}
if($lastvisited=='searchid'){
    $attribute = " brand_name as productno ";
    $groupby = "brand_name";
}
if($lastvisited=='customerdata'){
    $attribute = " customerrefno as productno ";
    $groupby = "customerrefno";
}
if($lastvisited=='status'){
    $attribute = " post_advertisment_pstatus as productno ";
    $groupby = "post_advertisment_pstatus";

}
if($lastvisited=='alluserid'){
    $attribute = " post_advertisment_userid as productno ";
    $groupby = "post_advertisment_userid";
}
if($lastvisited=='type'){
    $attribute = " ptype as productno ";
    $groupby = "ptype";
}
if($lastvisited=='alllocation'){
    $attribute = " location as productno ";
    $groupby = "location";
}    

if ($lastvisitedvalue != "Customers")
 $sql = \DB::select("SELECT count(*) as name,refno,$attribute,productno as productno1  FROM customer_products where ".$territory." GROUP BY $groupby order by $groupby");
else
{
    $sql = \DB::select("SELECT count(*) as name,refno,productno as productno1  FROM customer_products");
}


$sql2 = \DB::select("SELECT count(*) as name FROM customer_products where ".$territory."");

$temp=0;

foreach ($sql2 as $row1) {
    $industryname1 = isset($row1->name)?$row1->name:'';
    $temp=$temp+$industryname1;
}

$records = array();
$i = 0;

foreach ($sql as $row) {
    $refno = isset($row->refno)?$row->refno:'';
    $industryname = isset($row->name)?$row->name:'';
    $industryid=isset($row->refno)?$row->refno:'';

    if ($lastvisitedvalue != "Customers"){
     $productno = isset($row->productno)?$row->productno:'';
 }
 else
 {
    $productno = 'All';
}

$name1 =$productno.'
<a href="'.url('/').'/report/chartDetails?q=' .urlencode($productno). '&view_type='.$sec.'&clauseby='.(isset($groupby)?$groupby:"").'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';

$per = ($industryname/(($temp == 0) ? 1 : $temp ))*100;
if ($lastvisitedvalue != "Customers")
    $postpercent = number_format($per,2).'%';
else
{
    $postpercent ='100%';
}

$records[$i]['name1'] = $name1;
$records[$i]['postpercent'] = $postpercent;
$records[$i]['industryname'] = $industryname;
$i++;
}
return \Response::json($records);

}  
/***************************Customers report end******************************************/

/*************************Users report start*****************************************************/   
if($sec=="Users")
{ 
    $sql = \DB::select("SELECT count(*) as name,userid,industryname,industryid  FROM vwpost where ".$territory." GROUP BY userid");

    $temp=0;

    foreach ($sql as $row1) {
        $industryname1 = isset($row1->name)?$row1->name:'';
        $temp=$temp+$industryname1;
    }

    $records = array();
    $i = 0;

    foreach ($sql as $row) {
        $industryname = isset($row->name)?$row->name:'';
        $industryid=isset($row->industryid)?$row->industryid:'';
        
        $name1 =(isset($row->userid)?$row->userid:'').'<a href="'.url('/').'/report/chartDetails?user_id='.(isset($row->userid)?$row->userid:'').'&view_type='.$sec.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';

        $per = ($industryname/(($temp == 0 ) ? 1 : $temp ))*100;
        $postpercent = number_format($per,2).'%';

        $records[$i]['name1'] = $name1;
        $records[$i]['postpercent'] = $postpercent;
        $records[$i]['industryname'] = $industryname;

        $i++;

    }

    return \Response::json($records);
    
}  


/***************************Users report end******************************************/
return \Response::json(array());

}

function getIndustryname($industryid=''){

    $result = \App\Industry::where('industryid',$industryid)->lists('name')->toArray();

    return $result[0];
    
}
function getCatname($catid=''){

    $result = \App\Category::where('catid',$catid)->lists('name')->toArray();

    return $result[0];

}
function getSubCatname($subcatid=''){

    $result = \App\SubCategory::where('subcatid',$subcatid)->lists('name')->toArray();

    return $result[0];

}
function getBrandsname($brandid=''){

    $result = \App\Brand::where('brandid',$brandid)->lists('name')->toArray();

    return $result[0];

}

    public function chartDetails(Request $request){

        if( empty($request->input('reportbyad')) ){

            $headTitle = $request->input('view_type') . ' By Industry';
        
        }
        else{

            $headTitle = $request->input('view_type') . ' By Users';

        }

        return View('report.chartdetails',['headTitle'=>$headTitle])->with('title','Ziki Trade::Chart Details');

    }

    public function chartDetailsTable_ajax(Request $request){

        $industry_id = $request->input('industry_id');
        $sec_name = $request->input('sec_name');
        $secname = $request->input('secname');
 
        $productno =$request->input('productno');
        $industryid =$request->input('industryid');
        $postno =$request->input('postno');
        $refno =$request->input('refno'); 
        $clauseby =$request->input('dataClauseby'); 
        $lastfilter =urldecode($request->input('lastfilter')); 
        $view_type =$request->input('view_type');
    
    
        foreach($_GET as $lo=>$do)
        {
                if($do!="") {
                    $lastvisited=$lo;
                    $lastvisitedvalue = $do;
                }
        }

        if ($sec_name=='Postings' && $request->input('view_type')=="detailed") {

            if($lastfilter !="All"){

                if($lastvisitedvalue!="Postings"){

                    $sql = \DB::select("SELECT * FROM post_products where $clauseby='$lastfilter'");
                }
                else
                {
                    $sql = \DB::select("SELECT * FROM post_products where 1=1");
            
                }
            }
            else
            {
              $sql = \DB::select("SELECT *  FROM post_products where 1=1");
            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                
                $postno = '';
                $postno = $row->postno;
                $url = '<a href="report/report_detailed?postno='.$postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry_name, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory_name, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category_name, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand_name, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location = mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate = mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate = mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_pakaging =mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe = mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country = mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date("F d Y", $pdate);
                $post_advertisment_pstatus =mb_convert_encoding($row->post_advertisment_pstatus, "HTML-ENTITIES", "utf8");
                $postno =$row->postno.$url;
            

                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['post_advertisment_pstatus'] = $post_advertisment_pstatus;
                $records[$i]['postno'] = $postno;

                $i++;
            }

            return \Response::json($records);

        }


        if ($sec_name=='Products' && $_REQUEST['view_type']=="detailed") {

            if($lastfilter !="All"){

                 if($lastvisitedvalue!="Products"){
                  
                    $sql = \DB::select("SELECT *  FROM pro_products where $clauseby='".$lastfilter."'");  
                  }
                  
                else
                {
                    $sql = \DB::select("SELECT *  FROM pro_products where 1=1");
                }
            }   
            else
            {
                 $sql = \DB::select("SELECT *  FROM pro_products where 1=1");
            }

            $i = 0;
            $records = array();
            foreach ($sql as $row) {
                
                $name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry_name, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory_name, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category_name, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand_name, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                $product_userid =mb_convert_encoding($row->product_userid, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                //$pdate = date('Y, M d', $pdate);
                 $pdate = date("F d Y", $pdate);
                
                $status = mb_convert_encoding($row->product_status, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&product_id='.$proid.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                // $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';
                $records[$i]['name'] = $name;
                $records[$i]['industryname'] =$industryname;
                $records[$i]['categoryname'] =$categoryname;
                $records[$i]['subcategoryname'] =$subcategoryname;
                $records[$i]['brandname'] =$brandname;
                $records[$i]['product_userid'] =$product_userid;
                $records[$i]['pdate'] =$pdate;
                $records[$i]['status'] =$status;
                
            }

            return \Response::json($records);

        }


        if ($sec_name=='Offers' && $request->input('view_type')=="detailed") {

            if($lastfilter !="All")
            { 
             if($lastvisitedvalue!="Offers")
                $sql = \DB::select("SELECT *  FROM offer_products where $clauseby='".$lastfilter."'");
                else
                {
                 $sql = \DB::select("SELECT *  FROM offer_products where 1=1");
                }
            }
            else
            {
                $sql = \DB::select("SELECT *  FROM offer_products where 1=1");
            }

            $i = 0;
            $records = array();


            foreach ($sql as $row) {
                
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                
                $ptype =mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice =mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency =mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate =mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate =mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_language =mb_convert_encoding($row->post_advertisment_language, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe =mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country =mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                $post_advertisment_pdate =mb_convert_encoding($row->post_advertisment_pdate, "HTML-ENTITIES", "utf8");
                
                $offerstatus =mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                //$postno =mb_convert_encoding($row->postno), "HTML-ENTITIES", "utf8");
                $quotationno =mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                $pdate =mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8");
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                 $pdate = date("F d Y", $pdate);
                $counter_date1 = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                if(!empty($counter_date1))
                {
                $counter_date =strtotime( mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8"));
                 $counter_date = date("F d Y", $counter_date);
                }
                else
                {
                $counter_date='Not Applicable';
                }
                
                $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['post_advertisment_pdate'] = $post_advertisment_pdate;
                $records[$i]['offerstatus'] = $offerstatus;
                $records[$i]['postno'] = $postno;
                $records[$i]['quotationno'] = $quotationno;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['counter_date'] = $counter_date;
                
                $i++;
            }

            return \Response::json($records);
        
        }

        if ($sec_name=='Quotes' && $_REQUEST['view_type']=="detailed") {

            if($lastfilter !="All")
            { 
                 if($lastvisitedvalue!="Quotes"){
                    $sql = \DB::select("SELECT *  FROM qutation_products where $clauseby='".$lastfilter."'");
                 }
                 else
                    {
                        $sql = \DB::select("SELECT * FROM qutation_products where 1=1");
                    }
            }
            else
            {
                $sql = \DB::select("SELECT * FROM qutation_products where 1=1");
            }

            $i = 0;
            $records = array();
            
            foreach ($sql as $row) {
                
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                $ptype =mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice =mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency =mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate =mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate =mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                
                
                $post_advertisment_pakaging =mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe =mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country =mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                $post_advertisment_pdate =mb_convert_encoding($row->post_advertisment_pdate, "HTML-ENTITIES", "utf8");
                $offerstatus =mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                //$postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                
                $quotationno =mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date('Y, M d', $pdate);
                $counter_date = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date("F d Y", $pdate);
                $counter_date1 = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                //$pdate = mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8");
                if(!empty($counter_date1))
                {
                $counter_date =strtotime( mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8"));
                 $counter_date = date("F d Y", $counter_date);
                }
                else
                {
                $counter_date='Not Applicable';
                }
                
                $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed?&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';

                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['post_advertisment_pdate'] = $post_advertisment_pdate;
                $records[$i]['offerstatus'] = $offerstatus;
                $records[$i]['postno'] = $postno;
                $records[$i]['quotationno'] = $quotationno;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['counter_date'] = $counter_date;
        

                $i++;    
            }
            
            return \Response::json($records);
        }


        if ($sec_name=='Orders' && $_REQUEST['view_type']=="detailed") {

            if($lastfilter !="All")
            { 
                if($lastvisitedvalue!="Orders")
                    $sql = \DB::select("SELECT *  FROM oredr_products where $clauseby='".$lastfilter."'");
                else
                {
                        $sql = \DB::select("SELECT *  FROM oredr_products where 1=1");
                }
            
            }
            else
            {
                    $sql = \DB::select("SELECT *  FROM oredr_products where 1=1");

            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom = mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity = mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate =mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                
                $expirydate = mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno = mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_pakaging = mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_language = mb_convert_encoding($row->post_advertisment_language, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe = mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country =mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                
                $post_advertisment_pdate = mb_convert_encoding($row->post_advertisment_pdate, "HTML-ENTITIES", "utf8");
                $offerstatus = mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                //$postno = mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $quotationno = mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                
                $buyer = mb_convert_encoding($row->buyer, "HTML-ENTITIES", "utf8");
                $seller = mb_convert_encoding($row->seller, "HTML-ENTITIES", "utf8");
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date('Y, M d', $pdate);
                $counter_date = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                //$pdate = date('Y, M d', $pdate);
                 $pdate = date("F d Y", $pdate);
                 // echo "".$php_timestamp_date.""; 
        $quotefacility1 =mb_convert_encoding($row->quotefacility, "HTML-ENTITIES", "utf8");
                 if(!empty($quotefacility1))
                {
                    $quotefacility =mb_convert_encoding($row->quotefacility, "HTML-ENTITIES", "utf8");
                }
                else
                {
                    $quotefacility ="Not Applicable";
                }
                
        $buyer1 = mb_convert_encoding($row->buyer, "HTML-ENTITIES", "utf8");
                 if(!empty($buyer1))
                {
                   $buyer = mb_convert_encoding($row->buyer, "HTML-ENTITIES", "utf8");
                }
                else
                {
                    $buyer ="Not Applicable";
                }
                
        $seller1 = mb_convert_encoding($row->seller, "HTML-ENTITIES", "utf8");
                 if(!empty($seller1))
                {
                        $seller = mb_convert_encoding($row->seller, "HTML-ENTITIES", "utf8");
                }
                else
                {
                    $seller ="Not Applicable";
                }
                
                $status =mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                $quotationno = mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                $counter_date1 = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                //$pdate = mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8");
                if(!empty($counter_date1))
                {
                $counter_date =strtotime( mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8"));
                 $counter_date = date("F d Y", $counter_date);
                }
                else
                {
                $counter_date='Not Applicable';
                }
                
                $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';
                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['post_advertisment_pdate'] = $post_advertisment_pdate;
                $records[$i]['offerstatus'] = $offerstatus;
                $records[$i]['postno'] = $postno;
                $records[$i]['quotationno'] = $quotationno;
                $records[$i]['quotefacility'] = $quotefacility;
                $records[$i]['buyer'] = $buyer;
                $records[$i]['seller'] = $seller;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['counter_date'] = $counter_date;
                
                $i++;

            }

            return \Response::json($records);
        }


        if ($sec_name=='Customers' && $_REQUEST['view_type']=="detailed"){

            if($lastfilter !="All")
            { 
                    if($lastvisitedvalue!="Customers")
                $sql = \DB::select("SELECT *  FROM customer_products where $clauseby='".$lastfilter."'");
                else
                {
                $sql = \DB::select("SELECT *  FROM customer_products where 1=1");
                }
            }
            else
            {
            $sql = \DB::select("SELECT *  FROM customer_products where 1=1");
            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry_name, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory_name, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category_name, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand_name, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                
                
                $description = mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                
                
                $location = mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate = mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate = mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno = mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_pakaging =mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_language =mb_convert_encoding($row->post_advertisment_language, "HTML-ENTITIES", "utf8");
                
                $post_advertisment_timeframe = mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country = mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid = mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                //$pdate = date('Y, M d', $pdate);
                 $pdate = date("F d Y", $pdate);
                $status =mb_convert_encoding($row->post_advertisment_pstatus, "HTML-ENTITIES", "utf8");
                $refno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $postno =mb_convert_encoding($row->post_advertisment_postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->post_advertisment_postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $edit='';
                
                $records["data"]['product_name'] = $product_name;
                $records["data"]['productid'] = $productid;
                $records["data"]['industryname'] = $industryname;
                $records["data"]['categoryname'] = $categoryname;
                $records["data"]['subcategoryname'] = $subcategoryname;
                $records["data"]['brandname'] = $brandname;
                $records["data"]['ptype'] = $ptype;
                $records["data"]['targetprice'] = $targetprice;
                $records["data"]['currency'] = $currency;
                $records["data"]['quantity'] = $quantity;
                $records["data"]['location'] = $location;
                $records["data"]['post_advertisment_userid'] = $post_advertisment_userid;
                $records["data"]['pdate'] = $pdate;
                $records["data"]['status'] = $status;
                $records["data"]['postno'] = $postno;
                $records["data"]['refno'] = $refno;
                
                $i++;
            }

            return \Response::json($records);
        }


        if ($sec_name=='Users' && $request->input('view_type')=="detailed") {

            $sql = \DB::select("SELECT *  FROM vwpost where userid = '".$industry_id."'");

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                
                $product_name = mb_convert_encoding($row->name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industryname, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategoryname, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->categoryname, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brandname, "HTML-ENTITIES", "utf8");
                $manufacturer = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $quantity = mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $addedby =mb_convert_encoding($row->userid, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date('Y, M d', $pdate);
                $status =mb_convert_encoding($row->pstatus, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';
                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['addedby'] = $addedby;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['status'] = $status;
                $records[$i]['postingID'] = $postingID;
            
                $i++;
            }

            return \Response::json($records);

        }

        /* Expand View Start */

        if ($sec_name=='Postings' && $request->input('view_type')=="expand") {
            if($lastfilter !="All")
            {
                if($lastvisitedvalue!="Postings")
                    $sql = \DB::select("SELECT *  FROM post_products where $clauseby='".$lastfilter."'");
                else
                {
                    $sql = \DB::select("SELECT *  FROM post_products where 1=1");
            
                }
            }
            else
            {
              $sql = \DB::select("SELECT *  FROM post_products where 1=1");
            }
            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                
                $postno = '';
                
                $productcodes2='';
                $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");


                foreach ($sqlcnt2 as $row2) {
                    $productcodes2 = $productcodes2 .','. $row2->productcode;
                }

                    
                $productcodes2=substr($productcodes2,1,500);
                $postno = $row->postno;
                $url = '<a href="report_detailed.php?key=40&postno='.$postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry_name, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory_name, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category_name, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand_name, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                
                //substr($row->description,0,100);
                
                $description =mb_convert_encoding(substr($row->description,0,100), "HTML-ENTITIES", "utf8");
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location = mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate = mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate = mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_pakaging =mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe = mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country = mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date("F d Y", $pdate);
                $post_advertisment_pstatus =mb_convert_encoding($row->post_advertisment_pstatus, "HTML-ENTITIES", "utf8");
                $postno =$row->postno.$url;
                
                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['manufacture'] = $manufacture;
                $records[$i]['caseweight'] = $caseweight;
                $records[$i]['qtypercase'] = $qtypercase;
                $records[$i]['pakaging'] = $pakaging;
                $records[$i]['shipingcondition'] = $shipingcondition;
                $records[$i]['productcodes2'] = $productcodes2;
                $records[$i]['description'] = $description;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['uom'] = $uom;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['expdate'] = $expdate;
                $records[$i]['expirydate'] = $expirydate;
                $records[$i]['customerrefno'] = $customerrefno;
                $records[$i]['post_advertisment_pakaging'] = $post_advertisment_pakaging;
                $records[$i]['post_advertisment_timeframe'] = $post_advertisment_timeframe;
                $records[$i]['post_advertisment_country'] = $post_advertisment_country;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['post_advertisment_pstatus'] = $post_advertisment_pstatus;
                $records[$i]['postno'] = $postno;
                
                $i++;
            }

            return \Response::json($records);
        }


        if ($sec_name=='Products' && $request->input('view_type')=="expand") {

            if($lastfilter !="All")
            {    
                 if($lastvisitedvalue!="Products")
                  $sql = \DB::select("SELECT *  FROM pro_products where $clauseby='".$lastfilter."'");
                else
                {
                    $sql = \DB::select("SELECT *  FROM pro_products where 1=1");
                }
            }   
            else
            {
                 $sql = \DB::select("SELECT *  FROM pro_products where 1=1");
            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                
                $productcodes2='';
                $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
                
                foreach ($sqlcnt2 as $row2) {
                    $productcodes2 = $productcodes2 .','. $row2->productcode;
                }
                    
                $productcodes2=substr($productcodes2,1,500);
            
                
                $name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry_name, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory_name, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category_name, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand_name, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding(substr($row->description,0,100), "HTML-ENTITIES", "utf8");
                $product_userid =mb_convert_encoding($row->product_userid, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                //$pdate = date('Y, M d', $pdate);
                 $pdate = date("F d Y", $pdate);
                
                $status = mb_convert_encoding($row->product_status, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&product_id='.$proid.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                // $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';
                $records[$i]['name'] = $name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['manufacture'] = $manufacture;
                $records[$i]['caseweight'] = $caseweight;
                $records[$i]['qtypercase'] = $qtypercase;
                $records[$i]['pakaging'] = $pakaging;
                $records[$i]['shipingcondition'] = $shipingcondition;
                $records[$i]['productcodes2'] = $productcodes2;
                $records[$i]['description'] = $description;
                $records[$i]['product_userid'] = $product_userid;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['status'] = $status;

                $i++;
            }

            return \Response::json($records);

        }


        if ($sec_name=='Offers' && $_REQUEST['view_type']=="expand") {
            if($lastfilter !="All")
            { 
                if($lastvisitedvalue!="Offers"){
                    $sql = \DB::select("SELECT *  FROM offer_products where $clauseby='".$lastfilter."'");
                }
                else
                {
                    $sql = \DB::select("SELECT *  FROM offer_products where 1=1");
                }
            }
            else
            {
                $sql = \DB::select("SELECT *  FROM offer_products where 1=1");
            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                
                $productcodes2='';
                $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
                foreach ($sqlcnt2 as $row2) {
                    $productcodes2 = $productcodes2 .','. $row2->productcode;
                }
                    
                $productcodes2=substr($productcodes2,1,500);
                
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                
                $ptype =mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice =mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency =mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate =mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate =mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_language =mb_convert_encoding($row->post_advertisment_language, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe =mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country =mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                $post_advertisment_pdate =mb_convert_encoding($row->post_advertisment_pdate, "HTML-ENTITIES", "utf8");
                
                $offerstatus =mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                //$postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $quotationno =mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                $pdate =mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8");
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                 $pdate = date("F d Y", $pdate);
                $counter_date1 = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                if(!empty($counter_date1))
                {
                $counter_date =strtotime( mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8"));
                 $counter_date = date("F d Y", $counter_date);
                }
                else
                {
                $counter_date='Not Applicable';
                }
                
                $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                
                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['manufacture'] = $manufacture;
                $records[$i]['caseweight'] = $caseweight;
                $records[$i]['qtypercase'] = $qtypercase;
                $records[$i]['pakaging'] = $pakaging;
                $records[$i]['shipingcondition'] = $shipingcondition;
                $records[$i]['productcodes2'] = $productcodes2;
                $records[$i]['description'] = $description;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['uom'] = $uom;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['expdate'] = $expdate;
                $records[$i]['expirydate'] = $expirydate;
                $records[$i]['customerrefno'] = $customerrefno;
                $records[$i]['post_advertisment_language'] = $post_advertisment_language;
                $records[$i]['post_advertisment_timeframe'] = $post_advertisment_timeframe;
                $records[$i]['post_advertisment_country'] = $post_advertisment_country;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['post_advertisment_pdate'] = $post_advertisment_pdate;
                $records[$i]['offerstatus'] = $offerstatus;
                $records[$i]['postno'] = $postno;
                $records[$i]['quotationno'] = $quotationno;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['counter_date'] = $counter_date;
                
                $i++;

            }

            return \Response::json($records);
        }

        if ($sec_name=='Quotes' && $_REQUEST['view_type']=="expand") {

            if($lastfilter !="All")
            { 
                if($lastvisitedvalue!="Quotes")
                    $sql = \DB::select("SELECT *  FROM qutation_products where $clauseby='".$lastfilter."'");
                else
                {
                    $sql = \DB::select("SELECT *  FROM qutation_products where 1=1");
                }
            }   
            else
            {
                $sql = \DB::select("SELECT *  FROM qutation_products where 1=1");
            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {
                $productcodes2='';
                $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
                foreach ($sqlcnt2 as $row2) {
                    $productcodes2 = $productcodes2 .','. $row2->productcode;
                }
                    
                $productcodes2=substr($productcodes2,1,500);
                
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                $ptype =mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice =mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency =mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate =mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate =mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno =mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                
                
                $post_advertisment_pakaging =mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe =mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country =mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                $post_advertisment_pdate =mb_convert_encoding($row->post_advertisment_pdate, "HTML-ENTITIES", "utf8");
                $offerstatus =mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                //$postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                
                $quotationno =mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date('Y, M d', $pdate);
                $counter_date = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date("F d Y", $pdate);
                $counter_date1 = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                //$pdate = mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8");
                if(!empty($counter_date1))
                {
                $counter_date =strtotime( mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8"));
                 $counter_date = date("F d Y", $counter_date);
                }
                else
                {
                $counter_date='Not Applicable';
                }
                
                $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';
                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['manufacture'] = $manufacture;
                $records[$i]['caseweight'] = $caseweight;
                $records[$i]['qtypercase'] = $qtypercase;
                $records[$i]['pakaging'] = $pakaging;
                $records[$i]['shipingcondition'] = $shipingcondition;
                $records[$i]['productcodes2'] = $productcodes2;
                $records[$i]['description'] = $description;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['uom'] = $uom;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['expdate'] = $expdate;
                $records[$i]['expirydate'] = $expirydate;
                $records[$i]['customerrefno'] = $customerrefno;
                $records[$i]['post_advertisment_pakaging'] = $post_advertisment_pakaging;
                $records[$i]['post_advertisment_timeframe'] = $post_advertisment_timeframe;
                $records[$i]['post_advertisment_country'] = $post_advertisment_country;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['post_advertisment_pdate'] = $post_advertisment_pdate;
                $records[$i]['offerstatus'] = $offerstatus;
                $records[$i]['postno'] = $postno;
                $records[$i]['quotationno'] = $quotationno;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['counter_date'] = $counter_date;
                
                $i++;
            }

            return \Response::json($records);

        }

        if ($sec_name=='Orders' && $_REQUEST['view_type']=="expand") {
            if($lastfilter !="All")
            { 
                 if($lastvisitedvalue!="Orders")
                $sql = \DB::select("SELECT *  FROM oredr_products where $clauseby='".$lastfilter."'");
                else
                {
                        $sql = \DB::select("SELECT *  FROM oredr_products where 1=1");

                }
            
            }
            else
            {
                    $sql = \DB::select("SELECT *  FROM oredr_products where 1=1");

            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {

                $productcodes2='';
                $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
                foreach ($sqlcnt2 as $row2) {
                    $productcodes2 = $productcodes2 .','. $row2->productcode;
                }
                    
                $productcodes2=substr($productcodes2,1,500);
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                $description =mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom = mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity = mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                $location =mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate =mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                
                $expirydate = mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno = mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_pakaging = mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_language = mb_convert_encoding($row->post_advertisment_language, "HTML-ENTITIES", "utf8");
                $post_advertisment_timeframe = mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country =mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid =mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                
                $post_advertisment_pdate = mb_convert_encoding($row->post_advertisment_pdate, "HTML-ENTITIES", "utf8");
                $offerstatus = mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                //$postno = mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $quotationno = mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                
                $buyer = mb_convert_encoding($row->buyer, "HTML-ENTITIES", "utf8");
                $seller = mb_convert_encoding($row->seller, "HTML-ENTITIES", "utf8");
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                $pdate = date('Y, M d', $pdate);
                $counter_date = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                //$pdate = date('Y, M d', $pdate);
                 $pdate = date("F d Y", $pdate);
                 // echo "".$php_timestamp_date.""; 
        $quotefacility1 =mb_convert_encoding($row->quotefacility, "HTML-ENTITIES", "utf8");
                 if(!empty($quotefacility1))
                {
                    $quotefacility =mb_convert_encoding($row->quotefacility, "HTML-ENTITIES", "utf8");
                }
                else
                {
                    $quotefacility ="Not Applicable";
                }
                
        $buyer1 = mb_convert_encoding($row->buyer, "HTML-ENTITIES", "utf8");
                 if(!empty($buyer1))
                {
                   $buyer = mb_convert_encoding($row->buyer, "HTML-ENTITIES", "utf8");
                }
                else
                {
                    $buyer ="Not Applicable";
                }
                
        $seller1 = mb_convert_encoding($row->seller, "HTML-ENTITIES", "utf8");
                 if(!empty($seller1))
                {
                        $seller = mb_convert_encoding($row->seller, "HTML-ENTITIES", "utf8");
                }
                else
                {
                    $seller ="Not Applicable";
                }
                
                $status =mb_convert_encoding($row->offerstatus, "HTML-ENTITIES", "utf8");
                $quotationno = mb_convert_encoding($row->quotationno, "HTML-ENTITIES", "utf8");
                $counter_date1 = mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8");
                //$pdate = mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8");
                if(!empty($counter_date1))
                {
                $counter_date =strtotime( mb_convert_encoding($row->counter_date, "HTML-ENTITIES", "utf8"));
                 $counter_date = date("F d Y", $counter_date);
                }
                else
                {
                $counter_date='Not Applicable';
                }
                
                $postno =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $postingID =mb_convert_encoding($row->postno, "HTML-ENTITIES", "utf8");
                $edit='';

                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['manufacture'] = $manufacture;
                $records[$i]['caseweight'] = $caseweight;
                $records[$i]['qtypercase'] = $qtypercase;
                $records[$i]['pakaging'] = $pakaging;
                $records[$i]['shipingcondition'] = $shipingcondition;
                $records[$i]['productcodes2'] = $productcodes2;
                $records[$i]['description'] = $description;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['uom'] = $uom;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['expdate'] = $expdate;
                $records[$i]['expirydate'] = $expirydate;
                $records[$i]['customerrefno'] = $customerrefno;
                $records[$i]['post_advertisment_pakaging'] = $post_advertisment_pakaging;
                $records[$i]['post_advertisment_language'] = $post_advertisment_language;
                $records[$i]['post_advertisment_timeframe'] = $post_advertisment_timeframe;
                $records[$i]['post_advertisment_country'] = $post_advertisment_country;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['post_advertisment_pdate'] = $post_advertisment_pdate;
                $records[$i]['offerstatus'] = $offerstatus;
                $records[$i]['postno'] = $postno;
                $records[$i]['quotationno'] = $quotationno;
                $records[$i]['buyer'] = $buyer;
                $records[$i]['seller'] = $seller;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['counter_date'] = $counter_date;
                
                $i++;
            }
            return \Response::json($records);
        }

        if ($sec_name=='Customers' && $_REQUEST['view_type']=="expand") {
            if($lastfilter !="All")
            { 
                    if($lastvisitedvalue!="Customers")
                $sql = \DB::select("SELECT *  FROM customer_products where $clauseby='".$lastfilter."'");
                else
                {
                $sql = \DB::select("SELECT *  FROM customer_products where 1=1");
                }
            }
            else
            {
            $sql = \DB::select("SELECT *  FROM customer_products where 1=1");
            }

            $i = 0;
            $records = array();

            foreach ($sql as $row) {

                $productcodes2='';
                $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");

                foreach ($sqlcnt2 as $row2) {
                    $productcodes2 = $productcodes2 .','. $row2->productcode;
                }
                    
                    
                $productcodes2=substr($productcodes2,1,500);
                $product_name = mb_convert_encoding($row->product_name, "HTML-ENTITIES", "utf8");
                $productid = mb_convert_encoding($row->productno, "HTML-ENTITIES", "utf8");
                $proid = mb_convert_encoding($row->productid, "HTML-ENTITIES", "utf8");
                $industryname = mb_convert_encoding($row->industry_name, "HTML-ENTITIES", "utf8");
                $subcategoryname = mb_convert_encoding($row->subcategory_name, "HTML-ENTITIES", "utf8");
                $categoryname = mb_convert_encoding($row->category_name, "HTML-ENTITIES", "utf8");
                $brandname = mb_convert_encoding($row->brand_name, "HTML-ENTITIES", "utf8");
                $manufacture = mb_convert_encoding($row->manufacture, "HTML-ENTITIES", "utf8");
                $caseweight = mb_convert_encoding($row->caseweight, "HTML-ENTITIES", "utf8");
                $qtypercase = mb_convert_encoding($row->qtypercase, "HTML-ENTITIES", "utf8");
                $pakaging = mb_convert_encoding($row->pakaging, "HTML-ENTITIES", "utf8");
                $shipingcondition =mb_convert_encoding($row->shipingcondition, "HTML-ENTITIES", "utf8");
                
                
                $description = mb_convert_encoding($row->description, "HTML-ENTITIES", "utf8");
                $ptype = mb_convert_encoding(ucwords(strtolower($row->ptype)), "HTML-ENTITIES", "utf8");
                $targetprice = mb_convert_encoding($row->targetprice, "HTML-ENTITIES", "utf8");
                $currency = mb_convert_encoding($row->currency, "HTML-ENTITIES", "utf8");
                $uom =mb_convert_encoding($row->uom, "HTML-ENTITIES", "utf8");
                $quantity =mb_convert_encoding($row->quantity, "HTML-ENTITIES", "utf8");
                
                
                $location = mb_convert_encoding($row->location, "HTML-ENTITIES", "utf8");
                $expdate = mb_convert_encoding($row->expdate, "HTML-ENTITIES", "utf8");
                $expirydate = mb_convert_encoding($row->expirydate, "HTML-ENTITIES", "utf8");
                $customerrefno = mb_convert_encoding($row->customerrefno, "HTML-ENTITIES", "utf8");
                $post_advertisment_pakaging =mb_convert_encoding($row->post_advertisment_pakaging, "HTML-ENTITIES", "utf8");
                $post_advertisment_language =mb_convert_encoding($row->post_advertisment_language, "HTML-ENTITIES", "utf8");
                
                $post_advertisment_timeframe = mb_convert_encoding($row->post_advertisment_timeframe, "HTML-ENTITIES", "utf8");
                $post_advertisment_country = mb_convert_encoding($row->post_advertisment_country, "HTML-ENTITIES", "utf8");
                $post_advertisment_userid = mb_convert_encoding($row->post_advertisment_userid, "HTML-ENTITIES", "utf8");
                
                $pdate = strtotime(mb_convert_encoding($row->pdate, "HTML-ENTITIES", "utf8"));
                //$pdate = date('Y, M d', $pdate);
                 $pdate = date("F d Y", $pdate);
                $status =mb_convert_encoding($row->post_advertisment_pstatus, "HTML-ENTITIES", "utf8");
                $refno =mb_convert_encoding($row->refno, "HTML-ENTITIES", "utf8");
                $postno =mb_convert_encoding($row->post_advertisment_postno, "HTML-ENTITIES", "utf8").'

                <a href="report_detailed.php?key=40&postno='.$row->post_advertisment_postno.'&secname='.$sec_name.'" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
                $edit='';

                $records[$i]['product_name'] = $product_name;
                $records[$i]['productid'] = $productid;
                $records[$i]['industryname'] = $industryname;
                $records[$i]['categoryname'] = $categoryname;
                $records[$i]['subcategoryname'] = $subcategoryname;
                $records[$i]['brandname'] = $brandname;
                $records[$i]['manufacture'] = $manufacture;
                $records[$i]['caseweight'] = $caseweight;
                $records[$i]['qtypercase'] = $qtypercase;
                $records[$i]['pakaging'] = $pakaging;
                $records[$i]['shipingcondition'] = $shipingcondition;
                $records[$i]['productcodes2'] = $productcodes2;
                $records[$i]['description'] = $description;
                $records[$i]['ptype'] = $ptype;
                $records[$i]['targetprice'] = $targetprice;
                $records[$i]['currency'] = $currency;
                $records[$i]['uom'] = $uom;
                $records[$i]['quantity'] = $quantity;
                $records[$i]['location'] = $location;
                $records[$i]['expdate'] = $expdate;
                $records[$i]['expirydate'] = $expirydate;
                $records[$i]['customerrefno'] = $customerrefno;
                $records[$i]['post_advertisment_pakaging'] = $post_advertisment_pakaging;
                $records[$i]['post_advertisment_timeframe'] = $post_advertisment_timeframe;
                $records[$i]['post_advertisment_country'] = $post_advertisment_country;
                $records[$i]['post_advertisment_userid'] = $post_advertisment_userid;
                $records[$i]['pdate'] = $pdate;
                $records[$i]['status'] = $status;
                $records[$i]['postno'] = $postno;
                $records[$i]['refno'] = $refno;
                
                $i++;

            }
            return \Response::json($records);
        }


        /* Expand View Ends */
    }

    public function posting_csv_detail(Request $request){

        $output = "";
        $file = "postandindustry";

        $industry_id = $request->input('industry_id');
        $sec_name = $request->input('sec_name');


        $user_id = $request->input('user_id');
        $productno =$request->input('productno');
        $industryid =$request->input('industryid');
        $postno =$request->input('postno');
        $refno =$request->input('refno');
        $view_type = $request->input('view_type');

        $filter  = '';
        $group   = '';
        $orderby = 'ORDER BY brand';
        $sec= $request->input('section');


        $clauseby =$request->input('dataClauseby'); 
        $lastfilter1 =urldecode($request->input('lastfilter')); 



        $lastfilter =mb_convert_encoding($lastfilter1,"HTML-ENTITIES","UTF-8");


        foreach($_GET as $lo=>$do)
        {
            if($do!="") {
                $lastvisited=$lo;
                $lastvisitedvalue = $do;
            }
        }
    
/***************************Postings code start************************************/
         if($sec_name=="Postings" && $view_type=='0') 
         {
             if($lastfilter !="All"){
                if($lastvisitedvalue!="Postings")
                $fquery = \DB::select("SELECT *  FROM post_products where $clauseby='".$lastfilter."'");
                else
                {
                    $fquery = \DB::select("SELECT *  FROM post_products where 1=1");
                }
             }
             else{
                $fquery = \DB::select("SELECT *  FROM post_products where 1=1"); 
             }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= "\n";
        }
        if($sec_name=="Postings" && $view_type=='1') 
         {
            if($lastfilter !="All"){
                if($lastvisitedvalue!="Postings")
                $fquery = \DB::select("SELECT *  FROM post_products where $clauseby='".$lastfilter."'");
                else
                {
                    $fquery = \DB::select("SELECT *  FROM post_products where 1=1");
                }
             }
             else{
                $fquery = \DB::select("SELECT *  FROM post_products where 1=1"); 
             }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Manufacturer",';
                $output.= '"Case Weight",';
                $output.= '"Quantity per Case",';
                $output.= '"Packaging",';
                $output.= '"Shipping Conditions",';
                $output.= '"Product Codes",';
                $output.= '"Description",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Unit of Measurement",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"Expiry Date Range",';
                $output.= '"Exact Expiry Date",';
                $output.= '"Customer Reference Number",';
                $output.= '"Packaging Languages",';
                $output.= '"Timeframe",';
                $output.= '"Country of Origin",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= "\n";
        }
    /***************************Postings code end************************************/

/***************************Products code start************************************/
         if($sec_name=="Products" && $view_type=='0') 
         {
         
            if($lastfilter !="All")
            {    
                 if($lastvisitedvalue!="Products")
                  $fquery = \DB::select("SELECT *  FROM pro_products where $clauseby='".$lastfilter."'");
                else
                {
                    $fquery = \DB::select("SELECT *  FROM pro_products where 1=1");
                }
            }   
            else
            {
                 $fquery = \DB::select("SELECT *  FROM pro_products where 1=1");
            }
                $output.= '"Product Name",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"User ID",';
                $output.= '"Suggestion Date",';
                $output.= '"Status",';
                $output.= "\n";
        }
        if($sec_name=="Products" && $view_type=='1') 
        {
            if($lastfilter !="All")
            {    
                 if($lastvisitedvalue!="Products")
                  $fquery = \DB::select("SELECT *  FROM pro_products where $clauseby='".$lastfilter."'");
                else
                {
                    $fquery = \DB::select("SELECT *  FROM pro_products where 1=1");
                }
            }   
            else
            {
                 $fquery = \DB::select("SELECT *  FROM pro_products where 1=1");
            }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Manufacturer",';
                $output.= '"Case Weight",';
                $output.= '"Quantity per Case",';
                $output.= '"Packaging",';
                $output.= '"Shipping Conditions",';
                $output.= '"Product Codes",';
                $output.= '"Description",';
                $output.= '"User ID",';
                $output.= '"Suggestion Date",';
                $output.= '"Status",';
                $output.= "\n";
        }
    /***************************Products code end************************************/

/***************************Quotes code start************************************/
         if($sec_name=="Quotes" && $view_type=='0') 
         {
         
        if($lastfilter !="All")
        { 
        if($lastvisitedvalue!="Quotes")
                $fquery = \DB::select("SELECT *  FROM qutation_products where $clauseby='".$lastfilter."'");
            else
            {
                $fquery = \DB::select("SELECT *  FROM qutation_products where 1=1");
            }
        }   
        else
        {
            $fquery = \DB::select("SELECT *  FROM qutation_products where 1=1");
        }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"QuoteID",';
                $output.= '"Creation Date",';
                $output.= '"Modified Date",';
                $output.= "\n";
        }
        if($sec_name=="Quotes" && $view_type=='1') 
         {
        if($lastfilter !="All")
        { 
        if($lastvisitedvalue!="Quotes")
                $fquery = \DB::select("SELECT *  FROM qutation_products where $clauseby='".$lastfilter."'");
            else
            {
                $fquery = \DB::select("SELECT *  FROM qutation_products where 1=1");
            }
        }   
        else
        {
            $fquery = \DB::select("SELECT *  FROM qutation_products where 1=1");
        }
         
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Manufacturer",';
                $output.= '"Case Weight",';
                $output.= '"Quantity per Case",';
                $output.= '"Packaging",';
                $output.= '"Shipping Conditions",';
                $output.= '"Product Codes",';
                $output.= '"Description",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Unit of Measurement",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"Expiry Date Range",';
                $output.= '"Exact Expiry Date",';
                $output.= '"Customer Reference Number",';
                $output.= '"Packaging Languages",';
                $output.= '"Timeframe",';
                $output.= '"Country of Origin",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"QuoteID",';
                $output.= '"Creation Date",';
                $output.= '"Modified Date",';
                $output.= "\n";
        }
    /***************************Quotes code end************************************/
    
    /***************************Offers code start************************************/
         if($sec_name=="Offers" && $view_type=='0') 
         {
         
         if($lastfilter !="All")
        { 
            if($lastvisitedvalue!="Offers")
                $fquery = \DB::select("SELECT *  FROM offer_products where $clauseby='".$lastfilter."'");
            else
            {
                $fquery = \DB::select("SELECT *  FROM offer_products where 1=1");
            }
        }
        else
        {
            $fquery = \DB::select("SELECT *  FROM offer_products where 1=1");
        }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"QuoteID",';
                $output.= '"Creation Date",';
                $output.= '"Modified Date",';
                $output.= "\n";
        }
        if($sec_name=="Offers" && $view_type=='1') 
         {
         if($lastfilter !="All")
        { 
            if($lastvisitedvalue!="Offers")
                $fquery = \DB::select("SELECT *  FROM offer_products where $clauseby='".$lastfilter."'");
            else
            {
                $fquery = \DB::select("SELECT *  FROM offer_products where 1=1");
            }
        }
        else
        {
            $fquery = \DB::select("SELECT *  FROM offer_products where 1=1");
        }
         
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Manufacturer",';
                $output.= '"Case Weight",';
                $output.= '"Quantity per Case",';
                $output.= '"Packaging",';
                $output.= '"Shipping Conditions",';
                $output.= '"Product Codes",';
                $output.= '"Description",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Unit of Measurement",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"Expiry Date Range",';
                $output.= '"Exact Expiry Date",';
                $output.= '"Customer Reference Number",';
                $output.= '"Packaging Languages",';
                $output.= '"Timeframe",';
                $output.= '"Country of Origin",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"QuoteID",';
                $output.= '"Creation Date",';
                $output.= '"Modified Date",';
                $output.= "\n";
        }
    /***************************Offers code end************************************/
    
    /***************************Orders code start************************************/
         if($sec_name=="Orders" && $view_type=='0') 
         {
         
         if($lastfilter !="All")
        { 
             if($lastvisitedvalue!="Orders")
            $fquery = \DB::select("SELECT *  FROM oredr_products where $clauseby='".$lastfilter."'");
            else
            {
                    $fquery = \DB::select("SELECT *  FROM oredr_products where 1=1");

            }
        
        }
        else
        {
                $fquery = \DB::select("SELECT *  FROM oredr_products where 1=1");

        }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"QuoteID",';
                $output.= '"Quote Facilitator",';
                $output.= '"Buyer",';
                $output.= '"Seller",';
                $output.= '"Creation Date",';
                $output.= '"Modified Date",';
                $output.= "\n";
        }
        if($sec_name=="Orders" && $view_type=='1') 
         {
         if($lastfilter !="All")
        { 
             if($lastvisitedvalue!="Orders")
            $fquery = \DB::select("SELECT *  FROM oredr_products where $clauseby='".$lastfilter."'");
            else
            {
                    $fquery = \DB::select("SELECT *  FROM oredr_products where 1=1");

            }
        
        }
        else
        {
                $fquery = \DB::select("SELECT *  FROM oredr_products where 1=1");

        }
         
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Manufacturer",';
                $output.= '"Case Weight",';
                $output.= '"Quantity per Case",';
                $output.= '"Packaging",';
                $output.= '"Shipping Conditions",';
                $output.= '"Product Codes",';
                $output.= '"Description",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Unit of Measurement",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"Expiry Date Range",';
                $output.= '"Exact Expiry Date",';
                $output.= '"Customer Reference Number",';
                $output.= '"Packaging Languages",';
                $output.= '"Timeframe",';
                $output.= '"Country of Origin",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"QuoteID",';
                $output.= '"Quote Facilitator",';
                $output.= '"Buyer",';
                $output.= '"Seller",';
                $output.= '"Creation Date",';
                $output.= '"Modified Date",';
                $output.= "\n";
        }
    /***************************Orders code end************************************/
    
    /***************************Customers code start************************************/
         if($sec_name=="Customers" && $view_type=='0') 
         {
         
         if($lastfilter !="All")
        { 
                if($lastvisitedvalue!="Customers")
            $fquery = \DB::select("SELECT *  FROM customer_products where $clauseby='".$lastfilter."'");
            else
            {
            $fquery = \DB::select("SELECT *  FROM customer_products where 1=1");
            }
        }
        else
        {
        $fquery = \DB::select("SELECT *  FROM customer_products where 1=1");
        }
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"Customer Ref Number",';
                $output.= "\n";
        }
        if($sec_name=="Customers" && $view_type=='1') 
         {
         if($lastfilter !="All")
        { 
                if($lastvisitedvalue!="Customers")
            $fquery = \DB::select("SELECT *  FROM customer_products where $clauseby='".$lastfilter."'");
            else
            {
            $fquery = \DB::select("SELECT *  FROM customer_products where 1=1");
            }
        }
        else
        {
        $fquery = \DB::select("SELECT *  FROM customer_products where 1=1");
        }
         
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Manufacturer",';
                $output.= '"Case Weight",';
                $output.= '"Quantity per Case",';
                $output.= '"Packaging",';
                $output.= '"Shipping Conditions",';
                $output.= '"Product Codes",';
                $output.= '"Description",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Unit of Measurement",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"Expiry Date Range",';
                $output.= '"Exact Expiry Date",';
                $output.= '"Customer Reference Number",';
                $output.= '"Packaging Languages",';
                $output.= '"Timeframe",';
                $output.= '"Country of Origin",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= '"Customer Ref Number",';
                $output.= "\n";
        }
    /***************************Customers code end************************************/
    
    /***************************Users code start************************************/
         if($sec_name=="Users" && $view_type=='0') 
         {
         $fquery = \DB::select("SELECT *  FROM vwpost where $clauseby='".$lastfilter."'");
         
                $output.= '"Product Name",';
                $output.= '"Product ID",';
                $output.= '"Industry",';
                $output.= '"Category",';
                $output.= '"Sub-Category",';
                $output.= '"Brand",';
                $output.= '"Type",';
                $output.= '"Target Price",';
                $output.= '"Currency",';
                $output.= '"Quantity",';
                $output.= '"Location",';
                $output.= '"User Id",';
                $output.= '"Posting Date",';
                $output.= '"Status",';
                $output.= '"Posting Id",';
                $output.= "\n";
        }
    /***************************Users code end************************************/     
// $sql = mysql_query($fquery);

// $herry = mysql_fetch_assoc($sql);
// $columns_total = mysql_num_fields($sql);

// $rowData = mysql_num_rows($sql);

if (!empty($fquery))
    {
    //$output.= '"Product_Codes",';
    $temp = '';
    $count=0;
    
    
/********************Postings csv start*********************************/   
if($sec_name=="Postings" && $view_type=='0') 
{   
    foreach ($fquery as $row) {
        
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category_name)))) .', ';
        if($row->subcategory_name!=0 || $row->subcategory_name!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory_name)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
        $output.= "\n";
    }
}

if($sec_name=="Postings" && $view_type=='1') 
{
    foreach ($fquery as $row) {

        $productcodes2='';
        $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");

        foreach ($sqlcnt2 as $row2) {
            $procode = trim($row2->productcode);
            $productcodes2 = $productcodes2 .','. $procode;
        }
            

        $productcodes2=substr(preg_replace('!\s+!', ' ',trim($productcodes2)),1,500);
        $description=substr(html_entity_decode(preg_replace('!\s+!', ' ',trim($row->description))),0,100);
        
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category_name)))) .', ';
        if($row->subcategory_name!=0 || $row->subcategory_name!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory_name)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->manufacture)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->caseweight))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->qtypercase))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->shipingcondition))) .', ';
        $output.= str_replace(","," ",$productcodes2) .', ';
        $output.= str_replace(","," ",$description) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->uom))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expdate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expirydate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->customerrefno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_timeframe))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_country))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
        $output.= "\n";
    }
}
/********************Postings csv end*********************************/ 
/*   ...................................                       */
/********************Products csv start*********************************/   
if($sec_name=="Products" && $view_type=='0') 
{
    foreach( $fquery as $row ){

        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category_name)))) .', ';
        if($row->subcategory_name!=0 || $row->subcategory_name!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory_name)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->product_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->product_status))) .', ';
        $output.= "\n";
    }
}
if($sec_name=="Products" && $view_type=='1') 
{
    foreach( $fquery as $row){

        $productcodes2='';
        $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
        foreach ($sqlcnt2 as $row2) {
            $procode = trim($row2->productcode);
            $productcodes2 = $productcodes2 .','. $procode;
        }
            

        $productcodes2=substr(preg_replace('!\s+!', ' ',trim($productcodes2)),1,500);
        $description=substr(html_entity_decode(preg_replace('!\s+!', ' ',trim($row->description))),0,100);
        
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category_name)))) .', ';
        if($row->subcategory_name!=0 || $row->subcategory_name!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory_name)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->manufacture)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->caseweight))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->qtypercase))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->shipingcondition))) .', ';
        $output.= str_replace(","," ",$productcodes2) .', ';
        $output.= str_replace(","," ",$description) .', ';      
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->product_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->product_status))) .', ';
        $output.= "\n";
    }
}
/********************Products csv end*********************************/ 

/********************Quotes csv start*********************************/ 
if($sec_name=="Quotes" && $view_type=='0') 
{
    foreach($fquery as $row){

        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category)))) .', ';
        if($row->subcategory!=0 || $row->subcategory!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand)))) .', ';
        //$output.=  mb_convert_encoding($fetchData["manufacture, "HTML-ENTITIES", "utf8"). ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->offerstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotationno))) .', ';
        $output .= date("F d Y", $pdate) . ',';
        if($row->counter_date!=0 || $row->counter_date!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->counter_date)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= "\n";
    }
}
if($sec_name=="Quotes" && $view_type=='1') 
{
    foreach ($fquery as $row) {
        
        $productcodes2='';
        $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");

        foreach ($sqlcnt2 as $row2) {
            $procode = trim($row2->productcode);
            $productcodes2 = $productcodes2 .','. $procode;
        }
        
        $productcodes2=substr(preg_replace('!\s+!', ' ',trim($productcodes2)),1,500);
        $description=substr(html_entity_decode(preg_replace('!\s+!', ' ',trim($row->description))),0,100);
        
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category)))) .', ';
        if($row->subcategory!=0 || $row->subcategory!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->manufacture)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->caseweight))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->qtypercase))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->shipingcondition))) .', ';
        $output.= str_replace(","," ",$productcodes2) .', ';
        $output.= str_replace(","," ",$description) .', ';      
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->uom))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expdate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expirydate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->customerrefno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_timeframe))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_country))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->offerstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotationno))) .', ';
        $output .= date("F d Y", $pdate) . ',';
        if($row->counter_date!=0 || $row->counter_date!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->counter_date)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= "\n";
    }
}
/********************Quotes csv end*********************************/   

/********************Offer csv start*********************************/  
if($sec_name=="Offers" && $view_type=='0') 
{
    foreach($fquery as $row){

        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category)))) .', ';
        if($row->subcategory!=0 || $row->subcategory!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->offerstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotationno))) .', ';
        $output .= date("F d Y", $pdate) . ',';
        if($row->counter_date!=0 || $row->counter_date!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->counter_date)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= "\n";
    }
}

if($sec_name=="Offers" && $view_type=='1') 
{
    foreach($fquery as $row)
     {
        $productcodes2='';
        $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
        foreach($sqlcnt2 as $row2){

            $procode = trim($row2->productcode);
            $productcodes2 = $productcodes2 .','. $procode;
        }

        $productcodes2=substr(preg_replace('!\s+!', ' ',trim($productcodes2)),1,500);
        $description=substr(html_entity_decode(preg_replace('!\s+!', ' ',trim($row->description))),0,100);
        
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category)))) .', ';
        if($row->subcategory!=0 || $row->subcategory!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->manufacture)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->caseweight))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->qtypercase))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->shipingcondition))) .', ';
        $output.= str_replace(","," ",$productcodes2) .', ';
        $output.= str_replace(","," ",$description) .', ';  
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->uom))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expdate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expirydate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->customerrefno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_language))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_timeframe))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_country))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->offerstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotationno))) .', ';
        $output .= date("F d Y", $pdate) . ',';
        if($row->counter_date!=0 || $row->counter_date!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->counter_date)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= "\n";
    }
}
/********************Offer csv end*********************************/    

/********************Orders csv start*********************************/ 
if($sec_name=="Orders" && $view_type=='0') 
{
    foreach( $fquery as $row){

            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry)))) .', ';
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category)))) .', ';
            if($row->subcategory!=0 || $row->subcategory!='')
            {
                $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory)))) .', ';
            }
            else
            {
                $output.="Not Applicable". ',';
            }
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand)))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
            $pdate = strtotime($row->pdate);
            $output .= date("F d Y", $pdate) . ',';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->offerstatus))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotationno))) .', ';
            if($row->quotefacility!='')
            {
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotefacility))) .', ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            if($row->buyer!='')
            {
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->buyer))) .', ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            if($row->seller!='')
            {
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->seller))) .', ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            $output .= date("F d Y", $pdate) . ',';
            if($row->counter_date!=0 || $row->counter_date!='')
            {
                $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->counter_date)))) .', ';
            }
            else
            {
                $output.="Not Applicable". ',';
            }
            $output.= "\n";
    }
}

if($sec_name=="Orders" && $view_type=='1') 
{
    foreach($fquery as $row)
    {
        $productcodes2='';
        
        $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
        
        foreach( $sqlcnt2 as $row2 ){
            $procode = trim($row2->productcode);
            $productcodes2 = $productcodes2 .','. $procode;
        }
        
        $productcodes2=substr(preg_replace('!\s+!', ' ',trim($productcodes2)),1,500);
        $description=substr(html_entity_decode(preg_replace('!\s+!', ' ',trim($row->description))),0,100);
        
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name  )))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry)))) .', ';
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category)))) .', ';
            if($row->subcategory!=0 || $row->subcategory!='')
            {
                $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory)))) .', ';
            }
            else
            {
                $output.="Not Applicable". ',';
            }
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand)))) .', ';
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->manufacture)))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->caseweight))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->qtypercase))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->pakaging))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->shipingcondition))) .', ';
            $output.= str_replace(","," ",$productcodes2) .', ';
            $output.= str_replace(","," ",$description) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->uom))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expdate))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expirydate))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->customerrefno))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pakaging))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_timeframe))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_country))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
            $pdate = strtotime($row->pdate);
            $output .= date("F d Y", $pdate) . ',';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->offerstatus))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->postno))) .', ';
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotationno))) .', ';
            if($row->quotefacility!='')
            {
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quotefacility))) .', ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            if($row->buyer!='')
            {
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->buyer))) .', ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            if($row->seller!='')
            {
            $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->seller))) .', ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            $output .= date("F d Y", $pdate) . ',';
            if($row->counter_date!=0 || $row->counter_date!='')
            {
                $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->counter_date)))) .', ';
            }
            else
            {
                $output.="Not Applicable". ',';
            }
            $output.= "\n";
    }
}
/********************Orders csv end*********************************/   

/********************Customers csv start*********************************/  
if($sec_name=="Customers" && $view_type=='0') 
{
    foreach($fquery as $row)
    {
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category_name)))) .', ';
        if($row->subcategory_name!=0 || $row->subcategory_name!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory_name)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_postno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->refno))) .', ';
        $output.= "\n";
    }
}
if($sec_name=="Customers" && $view_type=='1') 
{
    foreach( $fquery as $row )
    {
        $productcodes2='';
        $sqlcnt2 = \DB::select("SELECT productcode FROM mst_productcode where pid='".$row->productid."'");
        foreach ($sqlcnt2 as $row2) {

            $procode = trim($row2->productcode);
            $productcodes2 = $productcodes2 .','. $procode;
        
        }

        $productcodes2=substr(preg_replace('!\s+!', ' ',trim($productcodes2)),1,500);
        $description=substr(html_entity_decode(preg_replace('!\s+!', ' ',trim($row->description))),0,100);
        
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->product_name)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->productno))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->industry_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->category_name)))) .', ';
        if($row->subcategory_name!=0 || $row->subcategory_name!='')
        {
            $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->subcategory_name)))) .', ';
        }
        else
        {
            $output.="Not Applicable". ',';
        }
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->brand_name)))) .', ';
        $output.= str_replace(","," ",html_entity_decode(preg_replace('!\s+!', ' ',trim($row->manufacture)))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->caseweight))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->qtypercase))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->shipingcondition))) .', ';
        $output.= str_replace(","," ",$productcodes2) .', ';
        $output.= str_replace(","," ",$description) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->ptype))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->targetprice))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->currency))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->uom))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->quantity))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->location))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expdate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->expirydate))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->customerrefno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pakaging))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_timeframe))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_country))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_userid))) .', ';
        $pdate = strtotime($row->pdate);
        $output .= date("F d Y", $pdate) . ',';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_pstatus))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->post_advertisment_postno))) .', ';
        $output.= str_replace(","," ",preg_replace('!\s+!', ' ',trim($row->refno))) .', ';
        $output.= "\n";
    }
}
/********************Customers csv end*********************************/

/********************Users csv start*********************************/  
 if($sec_name=="Users") 
         {
    foreach($fquery as $row){
        
            $output.=  '"' . str_replace('"', '""',$row->name) . '", ';
            $output.=  '"' . str_replace('"', '""',$row->productno) . '", ';
            $output.=  '"' . str_replace('"', '""',$row->industryname) . '", ';
            $output.=  '"' . str_replace('"', '""',$row->categoryname) . '", ';
            
            
            if($row->subcategoryname!=0 || $row->subcategoryname!='')
            {
            $output.= '"' . str_replace('"', '""',$row->subcategoryname) . '", ';
            }
            else
            {
            $output.="Not Applicable". ',';
            }
            $output.=  '"' . str_replace('"', '""',$row->brandname) . '", ';
            $output.=  '"' . str_replace('"', '""',$row->ptype) . '", ';
            $output.= '"' . str_replace('"', '""',$row->targetprice) . '", ';
            $output.=  '"' . str_replace('"', '""',$row->currency) . '", ';
            $output.=  '"' . str_replace('"', '""',$row->quantity) . '", ';
            $output.= '"' . str_replace('"', '""',$row->location) . '", ';
            $output.= '"' . str_replace('"', '""',$row->addedby) . '", ';
            $pdate = '"' . str_replace('"', '""',$row->pdate) . '", ';
            $output.=  date('Y M d', $pdate). ',';
            
            $output.= '"' . str_replace('"', '""',$row->pstatus) . '", ';
            $output.= '"' . str_replace('"', '""',$row->postno) . '", ';
            $output.= "\n";
    }
}
/********************Users csv end*********************************/    
    

            //echo $output;

            //die;

    
    // Download the file

    $filename = $file . "_" . date("Y-m-d_H-i", time()) . '.csv';
    // header('Content-Encoding: UTF-8');
    header("Content-type: application/vnd.ms-excel");
    header("Content-disposition: csv" . date("Y-m-d") . ".csv");
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    return $output;
    
    }
  else
    {
    echo '<script language="javascript">';
    echo 'alert("Data is not found");';
    echo '</script>';

    return redirect('report');
    }


    }
}