<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;

class DashboardController extends Controller
{

    public function __construct(){

        $this->middleware('auth');
    }

    public function index(){

    	if( Auth::user()->roletype == 'AM' ){
    		
    		$sqlq = \App\PostQuotation::where( 'postuserid',Auth::user()->userid )->where( 'readunreadst','0' )->whereIn('type',['QUOTATION'])->whereHas('product',function($query){
    				$query->where( 'pstatus','APPROVED' );
    		})->orderBy('quoteid','DESC')->limit(15)->offset(0)->get();
    	}	

		else if (Auth::user()->roletype == 'TM') {

			$sqlq = \App\TMAcceptedOffers::where( 'noti_status','Unread' )->limit(15)->offset(0)->get();

		} else if (Auth::user()->roletype == 'SA') {

			$sqlq="";

		} else if (Auth::user()->roletype == 'AD') {

			$sqlq="";

		}    	
		$swlw = '';
		if( Auth::user()->roletype == 'QF' ){

			$sqlq = \App\PostQuotation::where('userid','!=',Auth::user()->userid)->where('readunreadst','0')->where('type','!=','QUOTATION')->get();

			if( !empty($sqlq)  && (count($sqlq) > 0) ){

				foreach( $sqlq as $value ){

					$swlw .= '<li>
								<span class="text">
									<a href="'.url('quoteQueue/quotepostDetailView').'/'. $value->postno  .'/'. $value->productno.'/'. $value->postAdverstisment->ptype .'?quotationno='. $value->quotationno.'" class="ofercss">
										'. $value->details .' - '. $value->postAdverstisment->ptype .'&nbsp;'. $value->quantity .'&nbsp;'. $value->product->name .'
									</a>
								</span>
								<small class="label label-default pull-right date">Just Now</small>
							</li>';
					$sqlqm = \App\QuotationCounter::where('quotationno',$value->quotationno)->where('acceptedBy',Auth::user()->userid)->get();

					if( !empty($sqlqm)  && (count($sqlqm) > 0) ){

						foreach ($sqlqm as $sqlqmresult) {
							
							$swlw .= '<li>
										 <span class="text>
										 '.$sqlqmresult->counter_msg.' - '.$sqlqmresult->postAdverstisement->ptype.'&nbsp;' . $sqlqmresult->counter_quantity . '&nbsp;'.$sqlqmresult->product->name.'
										 </span>
									  	 <small class="label label-default pull-right date">Just Now</small>
									  </li>';

						}

					}

				}

			}


		}


		$mypostings = \App\PostAdverstisement::where('userid',Auth::user()->userid)->whereNotIn('pstatus',['Accepted','Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($query){
				$query->where('pstatus','APPROVED');
			})->whereHas('postCustomer',function($query){
				$query->where('isactive','1');
			})->whereHas('user',function($query){
				$query->where('isactive','1');
			})->orderBy('pdate','DESC')->limit(10)->offset(0)->get();

		$industry='';
		
		

		$recentpostings = \App\PostAdverstisement::whereNotIn('pstatus',['Accepted','Completed','CANCELLED','EXPIRED'])->where('act_inactive','0');

			$recentpostings = $recentpostings->whereHas('product',function($query){
									$query->where('pstatus','APPROVED');
								});

			if (empty(Auth::user()->industry) == false) {

				$industry  = explode(',', Auth::user()->industry);
				$recentpostings = $recentpostings->whereHas('product',function($query) use ($industry){
									$query->whereHas('industry',function($query) use ($industry){
										$query->whereIn('industryid',$industry);
									});
								  });
			}

			$recentpostings = $recentpostings->whereHas('postCustomer',function($query){
				$query->where('isactive','1');
			})->whereHas('user',function($query){
				$query->where('isactive','1');
			})->limit(10)->offset(0)->orderBy('pdate','DESC')->get();
			// echo "<pre>"; print_r($recentpostings);die;
		$data = array(
				'mypostcount' => count($this->mypostcount()),
				'allpostcount' => count($this->allpostcount()),
				'allproductCount' => count($this->allproductCount()),
				'allofferCount' => count($this->allofferCount()),
				'allqouteCount' => count($this->allqoutCount()),
				'allofferacceptedCount' => count($this->allofferaccCount()),
				'sqlq' => $sqlq,
				'swlw' => $swlw,
				'mypostings'=>$mypostings,
				'recentpostings' => $recentpostings
			);

    	return view('dashboard.dashboard',$data)->with(['title'=>'Dashboard','page_title'=>'Dashboard','page_description'=>'Welcome '. Auth::user()->userid]);
    }


    function mypostcount(){
    
      $result = \App\MastRolePermission::where('PageId','13')->where('RoleId',Auth::user()->roleid)->get();


	  
		 if( !empty($result[0]) && ($result[0]['ViewP'] == '1') ){
		 	
		 	$sth = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('act_inactive','0')->whereHas('user',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('postCustomer',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('product',function($query){
		 			$query->where('pstatus','APPROVED');
		 		})->get();


		 }
		 else{
		 	$sth = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('act_inactive','0')->whereHas('user',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('postCustomer',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('product',function($query){
		 			$query->where('pstatus','APPROVED');
		 		})->get();    	 
    	}

	    return $sth;
	}

	function allpostcount(){

	    $territory1 = array();
        $territory2 = array();
	    $industryid  = isset(Auth::user()->industry)?explode(',', Auth::user()->industry):array();
	    $category2  = Auth::user()->category;
	    
	    if(Auth::user()->roletype != "AD"){

	    	$result1 = \App\MastRolePermission::where('PageId','12')->where('RoleId',Auth::user()->roleid)->get();
			
			if(!empty($result1[0]) && ($result1[0]['ViewP'] == '1')){

				$sth = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('user',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('postCustomer',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('product',function($query){
			 			$query->where('pstatus','APPROVED');
			 		});

			 	if (empty(Auth::user()->industry) == false) {
		            $industry1  = Auth::user()->industry;
		            $territory1 = explode(',', $industry1);

		            $sth = $sth->whereHas('product',function($query) use ($territory1){
					 			$query->whereIn('industryid',$territory1);
					 		});

		        }
		        
		        if (empty(Auth::user()->category) == false) {
		            $category1  = Auth::user()->category;
		            $territory2 = explode(',', $category1);
		            
		            $sth = $sth->whereHas('product',function($query) use ($territory2){
					 			$query->whereIn('catid',$territory2);
					 		});

		        }

		        $sth = $sth->get();

			}else{

				$sth = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('user',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('postCustomer',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('product',function($query){
			 			$query->where('pstatus','APPROVED');
			 		});


			 	if (empty(Auth::user()->industry) == false) {
		            $industry1  = Auth::user()->industry;
		            $territory1 = explode(',', $industry1);

		            $sth = $sth->whereHas('product',function($query) use ($territory1){
					 			$query->whereIn('industryid',$territory1);
					 		});

		        }
		        
		        if (empty(Auth::user()->category) == false) {
		            $category1  = Auth::user()->category;
		            $territory2 = explode(',', $category1);
		            
		            $sth = $sth->whereHas('product',function($query) use ($territory2){
					 			$query->whereIn('catid',$territory2);
					 		});

		        }

		        $sth = $sth->get();

			}
	   
		}
		else{

			$sth = \App\PostAdverstisement::whereNotIn('pstatus',['COMPLETED','CANCELLED']);

			if( empty(Auth::user()->industry) == false ){
				$industryid = explode(',', Auth::user()->industry);
				$sth = $sth->whereHas('product',function($query) use ($industryid){
							$query->whereIn('industryid',$industryid);
						});
			}

			$sth = $sth->get();


		}

	    return $sth;
		
	}	

	function allproductCount(){

		$territory = array();
		$territory1 = array();
		$territory2 = array();
		
		if (Auth::user()->roletype == "AD") {

			$sth = \App\MstProduct::where('pstatus','<>','1');

			if (empty(Auth::user()->industry) == false){

				$industry = Auth::user()->industry;
				$territory = explode(',', Auth::user()->industry);

				$sth = $sth->whereIn('industryid',$territory);
			
			}

			if (empty(Auth::user()->category) == false){

				$category = Auth::user()->category;
				$territory1 = explode(',', Auth::user()->category);
				$sth = $sth->whereIn('catid',$territory1);
			
			}
			if (empty(Auth::user()->brand) == false){

				$brand = Auth::user()->brand;
				$territory2 = explode(',', Auth::user()->brand);
				$sth = $sth->whereIn('brandid',$territory2);
			}

			$sth = $sth->get();

		}
		else
		{
			$result1 = \App\MastRolePermission::where('PageId','7')->where('RoleId',Auth::user()->roleid)->get();
			
			if(!empty($result[0]) && ($result1[0]['pageView']=='1'))
			{
				$sth = \App\MstProduct::where('pstatus','APPROVED');
			
				if (empty(Auth::user()->industry) == false){

					$industry = Auth::user()->industry;
					$territory = explode(',', Auth::user()->industry);

					$sth = $sth->whereIn('industryid',$territory);
				
				}

				if (empty(Auth::user()->category) == false){

					$category = Auth::user()->category;
					$territory1 = explode(',', Auth::user()->category);
					$sth = $sth->whereIn('catid',$territory1);
				
				}
				if (empty(Auth::user()->brand) == false){

					$brand = Auth::user()->brand;
					$territory2 = explode(',', Auth::user()->brand);
					$sth = $sth->whereIn('brandid',$territory2);
				}

				$sth = $sth->get();
			}
			else{

				$sth = \App\MstProduct::where('pstatus','APPROVED');
				if (empty(Auth::user()->industry) == false){

					$industry = Auth::user()->industry;
					$territory = explode(',', Auth::user()->industry);

					$sth = $sth->whereIn('industryid',$territory);
				
				}

				if (empty(Auth::user()->category) == false){

					$category = Auth::user()->category;
					$territory1 = explode(',', Auth::user()->category);
					$sth = $sth->whereIn('catid',$territory1);
				
				}
				if (empty(Auth::user()->brand) == false){

					$brand = Auth::user()->brand;
					$territory2 = explode(',', Auth::user()->brand);
					$sth = $sth->whereIn('brandid',$territory2);
				}

				$sth = $sth->get();

			}

		}
	    
	    return $sth;
	}

	function allofferCount(){
		
		$result1 = \App\MastRolePermission::where('PageId','47')->where('RoleId',Auth::user()->roleid)->get();
		$sth = array();
		if( count($result1) > 0 ){
			foreach ($result1 as $value) {
				if( !empty($result1[0]) && $value->ViewP == '1'){

					$sth = \App\PostQuotation::where('userid',Auth::user()->userid)->where('type','OFFER')->whereHas('product',function($query){
							$query->where('pstatus','APPROVED');
						})->get();

				}else{
					
					$sth = \App\PostQuotation::all();
				
				}
				
			}
		}
		else{
					
			$sth = \App\PostQuotation::all();
		
		}

	    return $sth;
	}

	function allqoutCount(){

		$sth = \App\PostQuotation::where('postuserid',Auth::user()->userid)->where('type','QUOTATION')->get();

	    return $sth;
	}

	function allofferaccCount(){

		$territory1 = array();
		$territory2 = array();

	 	if (empty(Auth::user()->industry) == false) {

            $industry1  = Auth::user()->industry;
            $territory1 = explode(',', Auth::user()->industry);
            // $territory1 = ' AND mp.industryid IN (' . $industry1 . ')';
        }
        
        if (empty(Auth::user()->category) == false) {

            $category1  = Auth::user()->category;
            $territory2 = explode(',', Auth::user()->category);
            // $territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
        
        }

        $result1 = \App\MastRolePermission::where('PageId','12')->where('RoleId',Auth::user()->roleid)->get();
			
		if(!empty($result[0]) && ($result1[0]['ViewP']=='1'))
		{	
			$sth = \App\PostAdverstisement::where('userid','<>',Auth::user()->userid)->whereIn('pstatus',['Accepted','Declined'])->whereHas('product',function($query) use ($territory1){
					$query->whereIn('industryid',$territory1);
			})->whereHas('product',function($query) use ($territory2){

					$query->whereIn('catid',$territory2);
			
			})->get();

		}
		else{
			
			$sth = \App\PostAdverstisement::where('userid','<>',Auth::user()->userid)->whereIn('pstatus',['Accepted','Declined'])->whereHas('product',function($query) use ($territory1){
					$query->whereIn('industryid',$territory1);
			})->whereHas('product',function($query) use ($territory2){

					$query->whereIn('catid',$territory2);
			
			})->get();

		}
		
    	return $sth;
	}

	public function search(Request $request){

		$q = $request->searchname;
		return View('dashboard.unisearchresult')->with(['title'=>'Trading Soft::Search Result','page_title'=>'Search Result For ' . $q .'  ........']);
	
	}

	public function searchProductCatalog( Request $request ){
		
		$i1 = \App\Industry::where('isactive','1')->where('name','LIKE','%'.$request->searchname.'%')->lists('industryid')->toArray();

		$c1 = \App\Category::where('isactive','1')->where('name','LIKE','%'.$request->searchname.'%')->lists('catid')->toArray();
		
		$sc1 = \App\SubCategory::where('isactive','1')->where('name','LIKE','%'.$request->searchname.'%')->lists('subcatid')->toArray();

		$b1 = \App\Brand::where('isactive','1')->where('name','LIKE','%'.$request->searchname.'%')->lists('brandid')->toArray();

		$pc1 = \App\MstProductCode::where('isactive','1')->where('productcode','LIKE','%'.$request->searchname.'%')->lists('pid')->toArray();

		$sql = \App\MstProduct::where('pstatus','APPROVED');

		if (empty(Auth::user()->industry) == false)
		{
			$sql = $sql->whereIn('industryid',explode(',', Auth::user()->industry));
		}

		if (empty(Auth::user()->category) == false)
		{
			$sql = $sql->whereIn('catid',explode(',', Auth::user()->category));
		}

		if (empty(Auth::user()->subcategory) == false)
		{
			$sql = $sql->whereIn('subcatid',explode(',', Auth::user()->subcategory));
		}

		if (empty(Auth::user()->brand) == false)
		{
			$sql = $sql->whereIn('brandid',explode(',', Auth::user()->brand));
		}

		$sql = $sql->where('name','LIKE','%'.$request->input('searchname').'%')
				->orWhere('productno','LIKE','%'.$request->input('searchname').'%')
				->orWhere(function($query) use ($i1){
					$query->whereIn('industryid',$i1)->whereHas('industry',function($q) use ($i1){
						$q->whereIn('industryid',$i1);
					});
				})
				->orWhere(function($query) use ($c1){
					$query->whereIn('catid',$c1)->whereHas('category',function($q) use ($c1){
						$q->whereIn('catid',$c1);
					});
				})
				->orWhere(function($query) use ($b1){
					$query->whereIn('brandid',$b1)->whereHas('brand',function($q) use ($b1){
						$q->whereIn('brandid',$b1);
					});
				})
				->orWhereIn('subcatid',$sc1)
				->orWhere('manufacture','LIKE','%'.$request->input('searchname').'%')
				->orWhereIn('productid',$pc1)->get();


			$record = array();
			$i = 0;

			foreach ($sql as $row) {
				$productid = $row->productid;								
				$productno = '<a href="'.url('product/detail-product-view').'/' . $row->productid . '">'.$row->productno. '</a>';
				$productName = mb_convert_encoding($row->name, "HTML-ENTITIES", "utf8");
				$industry = mb_convert_encoding($row->industry->name, "HTML-ENTITIES", "utf8");
				$category = mb_convert_encoding($row->category->name, "HTML-ENTITIES", "utf8");
				$brand = mb_convert_encoding($row->brand->name, "HTML-ENTITIES", "utf8");
				$productcode = $row->productCode->lists('productcode')->toArray();

				$code = implode('; ', str_ireplace(' ', '', str_ireplace(':',': ',$productcode)) );

				$edit = '<a href="'.url('product/detail-product-view').'/' . $row->productid . '" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
				if(Auth::user()->roletype == 'AD') 
				{
					$edit1 = '<a href="'.url('product/detail-product-view').'/' . $row->productid . '" style="display:none;"><i class="fa fa-pencil-square-o"></i></a>';
					
					 $edit2 = '<a href="'.url('product/edit-product').'/' . $row->productid . '&key=7"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;';
					
					$edit3 = '<a  href="javascript:deleteme(\'' . $row->productid . '\')"><i class="fa fa-trash-o"></i></a>';
						
					
					$edit=$edit2.$edit3;
				}

				$record[$i]['productnoData'] = $productno;
				$record[$i]['industry'] = $industry;
				$record[$i]['brand'] = $brand;
				$record[$i]['productName'] = $productName;
				$record[$i]['code'] = $code;
				
				if( Auth::user()->roletype == 'AD' ){

					$record[$i]['edit'] = $edit;

				}
				$i++;

			}

		return \Response::json($record);
	}

	public function searchPostingCatalog(Request $request){
		$territory1 = '';
		if (empty(Auth::user()->industry) == false)
		{
			$industry1 = Auth::user()->industry;
			$territory1 = ' AND mp.industryid IN (' . $industry1 . ')';
		}

		if (empty(Auth::user()->category) == false)
		{
			$category1 = Auth::user()->category;
			$territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
		}

		if( Auth::user()->roletype != 'AD' ){

			$sql = \DB::select("SELECT pa.postno,pa.postid,pa.pdate,pa.ptype,pa.quantity,pa.timeframe,mp.productid,mp.name as productName,pa.act_inactive, i.name AS industry,c.name AS category, b.name AS brand,cu.isactive as customerstatus,mu.isactive as useridstataus,mp.pstatus as productstatus,pa.pstatus as poststatus 
				 from post_advertisment pa 
				 left join mst_product mp On pa.productno=mp.productno 
		         LEFT JOIN mst_industry i ON mp.industryid=i.industryid 
				 LEFT JOIN mst_category c ON mp.catid=c.catid 
		         LEFT JOIN mst_brand b ON mp.brandid = b.brandid
				 LEFT JOIN mst_userlogin mu ON pa.userid=mu.userid 
				 LEFT JOIN post_customer cu ON pa.customerrefno=cu.refno where pa.pstatus NOT IN('Completed','CANCELLED','EXPIRED') AND mp.pstatus = 'APPROVED' AND pa.act_inactive=0 AND cu.isactive=1 AND mu.isactive=1 " .$territory1. " AND
		 (
		    pa.postno like '%".$_REQUEST['searchname']."%'
			OR  pa.ptype like '%".$_REQUEST['searchname']."%'
			OR mp.name like '%".$_REQUEST['searchname']."%' 
			OR pa.productno like '%".$_REQUEST['searchname']."%'   
			OR (mp.industryid IN (SELECT industryid from mst_industry where isactive=1  and name like '%".$_REQUEST['searchname']."%') AND i.industryid IN (SELECT industryid from mst_industry where isactive=1  and name like '%".$_REQUEST['searchname']."%')) 
			OR (mp.catid IN      (SELECT catid from mst_category where isactive=1       and name like '%".$_REQUEST['searchname']."%') AND c.catid IN (SELECT catid from mst_category where isactive=1 and name like '%".$_REQUEST['searchname']."%'))
			OR (mp.brandid IN    (SELECT brandid from mst_brand where isactive=1        and name like '%".$_REQUEST['searchname']."%') AND b.brandid IN  (SELECT brandid from mst_brand where isactive=1        and name like '%".$_REQUEST['searchname']."%'))
			OR (mp.subcatid IN   (SELECT subcatid from mst_subcategory where isactive=1 and name like '%".$_REQUEST['searchname']."%') )
		 ) order by industry");

			$record = array();
			$i = 0;

			foreach ($sql as $row) {
				
				$enableaction='';
		        $sdate       = $row->pdate;
		        $pn          = '<a href="'.url('/').'/posting/posting-detail-view/' .$row->postno. '">'.$row->postno.'</a>';
		        $postno      = $pn;
	            $industry    = $row->industry;
	            $category    = $row->category;
	            $brand       = $row->brand;
	            $ptype       = $row->ptype;
	            $quantity    = $row->quantity;
				$act_inactive = $row->act_inactive;
				$productstatus = $row->productstatus;
				$poststatus    = $row->poststatus;
				$useridstataus = $row->useridstataus;
				$customerstatus = $row->customerstatus;
	            $productName = mb_convert_encoding($row->productName, "HTML-ENTITIES", "ISO-8859-1");
	            $timeframe   = $row->timeframe;
	            $productCheck = '<input type="checkbox" class="form-control" name="checkProduct" id="checkProduct" value="' . $row->postid . '" />';
	            if ( ($useridstataus=='1') && ($act_inactive=='0') && ($productstatus!='REJECTED') && ($customerstatus=='1') && ($poststatus !='EXPIRED'))
				{
					$enableaction .= '<span class="label label-sm label-success"> Enable </span>';
				}
				else
				{
					if($poststatus =='EXPIRED')
					{
	                	$enableaction .= '<span class="label label-sm label-danger">EXPIRED</span>';
					}
					else
					{
						$enableaction .= '<span class="label label-sm label-danger">Disable</span>';
					}
				}
				
				$record[$i]['postno'] = $postno;
				$record[$i]['sdate'] = $sdate;
				$record[$i]['industry'] = $industry;
				$record[$i]['category'] = $category;
				$record[$i]['brand'] = $brand;
				$record[$i]['ptype'] = $ptype;
				$record[$i]['quantity'] = $quantity;
				$record[$i]['productName'] = $productName;
				$record[$i]['timeframe'] = $timeframe;

				$i++;
			}

		}
		else{

			$sql = \DB::select("SELECT pa.postno,pa.postid,pa.pdate,pa.ptype,pa.quantity,pa.timeframe,mp.productid,mp.name as productName,pa.act_inactive, i.name AS industry,
         c.name AS category, b.name AS brand,cu.isactive as customerstatus,mu.isactive as useridstataus,mp.pstatus as productstatus,pa.pstatus as poststatus 
		 from post_advertisment pa 
		 left join mst_product mp On pa.productno=mp.productno 
         LEFT JOIN mst_industry i ON mp.industryid=i.industryid 
		 LEFT JOIN mst_category c ON mp.catid=c.catid 
         LEFT JOIN mst_brand b ON mp.brandid = b.brandid
		 LEFT JOIN mst_userlogin mu ON pa.userid=mu.userid 
		 LEFT JOIN post_customer cu ON pa.customerrefno=cu.refno  
		 where
		 pa.pstatus NOT IN('Completed','CANCELLED') " .$territory1. " AND
		 (
		    pa.postno like '%".$_REQUEST['searchname']."%'
			OR  pa.ptype like '%".$_REQUEST['searchname']."%'
			OR mp.name like '%".$_REQUEST['searchname']."%' 
			OR pa.productno like '%".$_REQUEST['searchname']."%'   
			OR (mp.industryid IN (SELECT industryid from mst_industry where isactive=1  and name like '%".$_REQUEST['searchname']."%') AND i.industryid IN (SELECT industryid from mst_industry where isactive=1  and name like '%".$_REQUEST['searchname']."%')) 
			OR (mp.catid IN      (SELECT catid from mst_category where isactive=1       and name like '%".$_REQUEST['searchname']."%') AND c.catid IN (SELECT catid from mst_category where isactive=1 and name like '%".$_REQUEST['searchname']."%'))
			OR (mp.brandid IN    (SELECT brandid from mst_brand where isactive=1        and name like '%".$_REQUEST['searchname']."%') AND b.brandid IN  (SELECT brandid from mst_brand where isactive=1        and name like '%".$_REQUEST['searchname']."%'))
			OR (mp.subcatid IN   (SELECT subcatid from mst_subcategory where isactive=1 and name like '%".$_REQUEST['searchname']."%') )
		 ) order by industry");

			$record = array();
			$i = 0;

			foreach ($sql as $row) {
				
				$enableaction='';
		        $sdate       = $row->pdate;
		        $pn          = '<a href="'.url('/').'/posting/posting-detail-view/' .$row->postno. '">'.$row->postno.'</a>';
		        $postno      = $pn;
	            $industry    = $row->industry;
	            $category    = $row->category;
	            $brand       = $row->brand;
	            $ptype       = $row->ptype;
	            $quantity    = $row->quantity;
				$act_inactive = $row->act_inactive;
				$productstatus = $row->productstatus;
				$poststatus    = $row->poststatus;
				$useridstataus = $row->useridstataus;
				$customerstatus = $row->customerstatus;
	            $productName = mb_convert_encoding($row->productName, "HTML-ENTITIES", "ISO-8859-1");
	            $timeframe   = $row->timeframe;
	            $productCheck = '<input type="checkbox" class="form-control" name="checkProduct" id="checkProduct" value="' . $row->postid . '" />';
	            if ( ($useridstataus=='1') && ($act_inactive=='0') && ($productstatus!='REJECTED') && ($customerstatus=='1') && ($poststatus !='EXPIRED'))
				{
					$enableaction .= '<span class="label label-sm label-success"> Enable </span>';
				}
				else
				{
					if($poststatus =='EXPIRED')
					{
	                	$enableaction .= '<span class="label label-sm label-danger">EXPIRED</span>';
					}
					else
					{
						$enableaction .= '<span class="label label-sm label-danger">Disable</span>';
					}
				}
				
				$record[$i]['postno'] = $postno;
				$record[$i]['sdate'] = $sdate;
				$record[$i]['industry'] = $industry;
				$record[$i]['category'] = $category;
				$record[$i]['brand'] = $brand;
				$record[$i]['ptype'] = $ptype;
				$record[$i]['quantity'] = $quantity;
				$record[$i]['productName'] = $productName;
				$record[$i]['timeframe'] = $timeframe;
				$record[$i]['enableaction'] = $enableaction;
				$i++;
			}

		}

		return \Response::json($record);	
	}

	public function notidash(){

		if( Auth::user()->roletype != 'TM' ){
			$result = \App\MstNotification::where('userid',Auth::user()->userid)->where('status','Unread')->where('fromuserid','!=','UUZZ-9984')->orderBy('notiid','DESC')->get();
		}

		if( Auth::user()->roletype == 'TM' ){
			$result = \App\TMAcceptedOffers::where('noti_status','Unread')->orderBy('acc_offr_id','DESC')->limit(4)->get();
		}
		if( Auth::user()->roletype == 'AM' ){
			$result1 = \App\MstNotification::where('userid',Auth::user()->userid)->where('status','Unread')->orderBy('notiid')->count();

			$result2 = \App\PostQuotation::where('readunreadst','0')->where('postuserid',Auth::user()->userid)->where('type','!=','OFFER')->orderBy('offercrdate','ASC')->count();

			$total = $result1 + $result2;
		
			if( ( $result1 > 0 ) || ( $result2 > 0 ) ){

				if( $result1 > 0 ){
					foreach ($result1 as $value1) {
						$link = 'suggested_detail-product-view?product_id='. $value1->pro_id .'&notiid='.$value1->notiid;

						echo '<li>
								<span class="text">
									<a href="'.$link.'" class="ofercss">'.$value1->msg.'</a>
								</span>
							  </li>';

					}
				}
				if( $result2 > 0 ){
					foreach ($result2 as $value2) {
						$link = url('posting/myposting-detail-view').'/'.$value2->postno.'?quotationno='.$value2->quotationno;
						echo '<li>
								<span class="text">
									<a href="'.$link.'">New Offer Received</a>
								</span>
							  </li>';
					}
				}

			}

			else{
				echo '';
			}
		}
		elseif( count($result) != 0 ){
			foreach ($result as $value3) {
				if( Auth::user()->roletype == 'TM' ){
					$data = explode(' ',$value3->acc_date);	
				}
				else{
					$data = explode(' ',$value3->msgdate);
				}

				echo '<li>
						<span class="text">';
	            if( Auth::user()->roletype != 'TM' ){
	            	if($value3->msg == "Reported Product"){

						$link= url('product/update-reported-product').'/'.$value3->pro_id.'/'.$value3->preportid.'?notiid='.$value3->notiid;
					}
					if($value3->msg == "Product Code Sent For Approval"){

						$link='product-codes.php?notiid='.$value3->pro_id.'&product_id='.$value3->pro_id;
					}
					if($value3->msg=="Product Sent For Approval"){

						$link='suggest-product-review.php?product_id='.$value3->pro_id.'&notiid='.$value3->notiid;
					}
						
					echo '<a href="'.$link.'" class="linkcol"> '.$value3->msg.' </a>';	
	            }
	            else {
					echo "Accepted Offer Recived";
				}
				echo '</span>
					<small class="label label-default pull-right">'.$data[0].'</small>';
			}
		}

		else {
			echo '';
		}
	}

	public function notiAlert(Request $request){
		if ($request->lognoti == 'lognoti') {
		 
		    $key      = $request->id;
		    $roletype = Auth::user()->roletype;

		    $sth = \App\PageNotification::where('p_key',$key)->where('roletype',$roletype)->get();

		    foreach ($sth as $value) {
		    	if( count( $sth ) == 0 ){
		    		return "No Notification";
		    	}
		    	else{
		    		echo $value->n_msg;
		    	}
		    }
		}
	}

	public function noti6(){
		$latestmsg   = \App\User::find(Auth::user()->userid);
		
		if (($latestmsg->visualnoti == 0) && ($latestmsg->audionoti == 0)) {
						echo "bothclose";
		}
		if ($latestmsg->audionoti == 0) {
						echo "audioclose";
		}
		if ($latestmsg->visualnoti == 0) {
						echo "vedioclose";
		}
		if (($latestmsg->visualnoti == 1) && ($latestmsg->audionoti == 1)) {
						echo "bothstart";
		}
	}
}
