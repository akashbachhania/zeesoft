<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProductCode extends Model
{
    protected $table      = 'mst_productcode';
    protected $primaryKey = 'productcodeid';

    function product(){
        return $this->belongsTo('App\MstProduct','pid');
    }
}
