<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TMAcceptedOffers extends Model
{
    protected $table = 'tm_accpted_offers';

    public function mstProduct()
    {
        return $this->belongsTo('App\MstProduct', 'productno', 'productno');
    }

    public function adverstisement()
    {
        return $this->belongsTo('App\PostAdverstisement', 'postno', 'postno');
    }

    public function postQuotation()
    {
        return $this->belongsTo('App\PostQuotation', 'quoteid', 'quoteid');
    }


}
