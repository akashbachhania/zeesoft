<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstTimeframe extends Model
{
    protected $table = 'mst_timeframe';
    protected $primaryKey = 'timeframeid';
}
