<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationCounter extends Model
{
    protected $table = 'quotation_counter';
    protected $primaryKey = 'counterid';

    function postQuotation(){
    	return $this->belongsTo('App\QuotationCounter','quotationno','quotationno');
    }

    function postAdverstisement(){
    	return $this->belongsTo('\App\PostAdverstisement','postno','postno');
    }

    function product(){
    	return $this->belongsTo('\App\MstProduct','productno','productno');
    } 
}
