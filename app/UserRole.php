<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'mst_userrole';
    protected $primaryKey = 'roleid';
}
