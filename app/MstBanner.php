<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstBanner extends Model
{
    protected $table = 'mst_banner';
    protected $primaryKey = 'id';
    
}
