<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaskProname extends Model
{
    protected $table = 'maskproname';
    protected $primaryKey = 'id';
}
