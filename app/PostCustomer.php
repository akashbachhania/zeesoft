<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCustomer extends Model
{
    protected $table = 'post_customer';
    protected $primaryKey = 'custid';

    function userrole(){
    	return $this->belongsTo('\App\UserRole','roletype','code');
    }
}
