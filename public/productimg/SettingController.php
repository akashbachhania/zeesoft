<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Industry;
use App\UserActivity;
use App\Category;
use App\SubCategory;
use App\Brand;
use Session;

use SSH;
use Illuminate\Support\Facades\Input;

class SettingController extends Controller
{
    public function backupPage(){
    	return View('setting.backupPage')->with('title','Trading Soft::Backup DB');
    }

    public function restorePage(){
    	return View('setting.restorePage')->with('title','Trading Soft::Restore DB');
    }
    
    public function backup(){
    	$now            = str_replace(":", "", date("Y-m-d H: i: s"));
        $outputfilename = 'zeesoft-' . $now . '.sql';
        $outputfilename = str_replace(" ", "-", $outputfilename);
        
        $commands = ([
            'cd /var/www/html/public/sql',
            'mysqldump -uroot -plogin123#123# minialpha > '.$outputfilename,
        ]);
        SSH::run($commands);

        $file = public_path() .'/sql/' . $outputfilename;
        $header = array(
        	'Content-Type'=> 'force/download'
        	);
        return \Response::download($file,$outputfilename,$header);
    }

    public function restore(Request $request){
        
        $path = public_path() .'/sql/'; 

        $now            = str_replace(":", "", date("Y-m-d H: i: s"));
        $outputfilename = 'zeesoft-' . $now . '.sql';
        $outputfilename = str_replace(" ", "-", $outputfilename);
        Input::file('sql')->move($path, $outputfilename);        
        $commands = ([
            'cd /var/www/html/public/sql',
            'mysql -uroot -plogin123#123# minialpha < '.public_path() . '/sql/'.$outputfilename,
        ]);
        SSH::run($commands);
        \Session::flash('alert-success', 'Database Restored successfully'); 
            return \Redirect::to('dbrestore');
    }

    public function industry(){
         
    	return View('setting.industry')->with('title','Trading Soft::Industry');
    }

    public function industry_ajax(Request $request){
       
    	switch ($request->type) {

    		case 'exist':
    			$industry=Industry::where('name','=',$request->pname)->count();
    			if($industry>0)
    			{
    				echo 'exist';
    			}
    			else
				{
					echo '1';
				}
    		break;
    		
    		case 'add':
    			$industry=Industry::where('name','=',$request->name)->count();
    			if($industry==0)
    			{
	    			$industry=new Industry;

	    			$industry->name 		= 	$request->name;
	    			$industry->isactive		= 	$request->status;
	    			$industry->save();
	    			$arr = array(
							'name' => 'Record Saved Sucessfully',
							'msg' => 'SUCCESS'
						);
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Industry';

                     $useraction->save();
	    		}
	    		else{
	    			$arr = array(
						'msg' => 'ERROR'
					);
	    		}
    			return \Response::json($arr);

    		  break;

            case 'search':
                $industry=Industry::find($request->id);
                return \Response::json($industry);
                break;
            
            case 'update':
                
                $industry=Industry::where('name','=',$request->name)->count();
                if($industry==0)
                {
                    $industry=Industry::find($request->id);

                    $industry->name         =   $request->name;
                    $industry->isactive     =   $request->status;
                    $industry->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Industry';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $industry=Industry::find($request->id);
                if($industry->category->count()>0 || $industry->brand->count()>0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    
                }
                else{
                    $industry->delete();
                        $arr = array(
                        'name' => 'Related Record Delete',
                        'msg' => 'A1'
                        );
                        $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Industry';

                    $useraction->save();
                }
                return \Response::json($arr);
            break;
            case 'industaction':
                $industry=Industry::all();
                return \Response::json(compact('industry'));          
            break;
    		default:
                //echo $request->type;die(' ld');
                //break;
    		$industry=Industry::all();
            $i=0;
            $record=array();
            foreach ($industry as $row) {
                $record[$i]['name']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['industryid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['industryid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
    		break;
    	}
    }
    public function category(){
        $industries=Industry::all();
        return View('setting.category',compact('industries'))->with('title','Trading Soft::Category');
    }
    public function category_ajax(Request $request){
         
        switch ($request->type) {
            case 'exist':
                $Category=Category::where('name','=',$request->pname)->where('industryid','=',$request->iid)->count();
                if($Category>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $Category=Category::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Category==0)
                {
                    $Category=new Category;

                    $Category->name           =   $request->name;
                    $Category->industryid     =   $request->iid;
                    $Category->isactive       =   $request->status;
                    $Category->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'ERROR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $Category=Category::find($request->id);
                return \Response::json($Category);
                break;
            
            case 'update':
                
                $Category=Category::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Category==0)
                {
                    $Category=Category::find($request->id);

                    $Category->name               =   $request->name;
                    $Category->industryid         =   $request->iid;
                    $Category->isactive           =   $request->status;
                    $Category->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $Category=Category::find($request->id);
                if($Category->subcategory->count()>0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    return \Response::json($arr);
                }
                else{
                    $Category->delete();
                        $arr = array(
                        'name' => 'Related Record Delete',
                        'msg' => 'A1'
                        );
                        $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Industry';

                    $useraction->save();
                }
                return \Response::json($arr);
            break;

            case 'category':
                $Category=Category::where('industryid','=',$request->ind_id)->get();
                return \Response::json(compact('Category'));
            break;

            default:
            $Category=Category::all();
            $i=0;
            $record=array();
            foreach ($Category as $row) {
                
                $record[$i]['industry']=$row['industry']['name'];
                $record[$i]['category']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['catid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['catid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
            break;
               
        }
    }
    public function subcategory(){
        $industries=Industry::all();
        return View('setting.subcategory',compact('industries'))->with('title','Trading Soft::Sub-Category');
    }
    public function subcategory_ajax(Request $request){
         
        switch ($request->type) {
            case 'exist':
                $Subcategory=SubCategory::where('name','=',$request->pname)
                                    ->where('catid','=',$request->iid)
                                    ->where('industryid','=',$request->industry)
                                    ->count();
                if($Subcategory>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            case 'category':
                $Category=Category::where('industryid','=',$request->ind_id)->get();
               
                    return \Response::json(compact('Category'));
                
               
            break;
            case 'add':
                $Subcategory=SubCategory::where('name','=',$request->name)
                                        ->where('catid','=',$request->iid)
                                        ->where('industryid','=',$request->industry)
                                        ->count();
                if($Subcategory==0)
                {
                    $Subcategory=new Subcategory;

                    $Subcategory->name           =   $request->name;
                    $Subcategory->industryid     =   $request->industry;
                    $Subcategory->catid          =   $request->iid;
                    $Subcategory->isactive       =   $request->status;
                    $Subcategory->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Sub Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $Subcategory=SubCategory::find($request->id);
                return \Response::json($Subcategory);
                break;
            
            case 'update':
                
               $Subcategory=SubCategory::where('name','=',$request->name)
                                        ->where('catid','=',$request->iid)
                                        ->where('industryid','=',$request->industry)
                                        ->count();
                if($Subcategory==0)
                {
                    $Subcategory=SubCategory::find($request->id);

                    $Subcategory->name           =   $request->name;
                    $Subcategory->industryid     =   $request->industry;
                    $Subcategory->catid          =   $request->iid;
                    $Subcategory->isactive       =   $request->status;
                    $Subcategory->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Sub Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;

            case 'subcat':
                $Subcategory=SubCategory::where('catid','=',$request->cat_id)->get();
                return \Response::json(compact('Subcategory'));                        
                                        
            break;
            case 'delete':

                $Category=Category::find($request->id);
                if($Category->category->count()>0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    return \Response::json($arr);
                }
            break;

            default:
            $Subcategory=SubCategory::all();
            $i=0;
            $record=array();
            foreach ($Subcategory as $row) {
                
                $record[$i]['industry']=$row['category']['industry']['name'];
                $record[$i]['category']=$row['category']['name'];
                $record[$i]['subcategory']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['subcatid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['subcatid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
            break;
               
        }
    }
     public function brand(){
        $industries=Industry::all();
        return View('setting.brand',compact('industries'))->with('title','Trading Soft::Category');
    }
    public function brand_ajax(Request $request){

        switch ($request->type) {
            case 'exist':
                $Brand=Brand::where('name','=',$request->pname)->where('industryid','=',$request->iid)->count();
                if($Brand>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $Brand=Brand::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Brand==0)
                {
                    $Brand=new Brand;

                    $Brand->name           =   $request->name;
                    $Brand->industryid     =   $request->iid;
                    $Brand->isactive       =   $request->status;
                    $Brand->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Brand';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $Brand=Brand::find($request->id);
                return \Response::json($Brand);
                break;
            
            case 'update':
                
                $Brand=Brand::where('name','=',$request->name)->where('industryid','=',$request->iid)->count();
                if($Brand==0)
                {
                    $Brand=Brand::find($request->id);

                    $Brand->name               =   $request->name;
                    $Brand->industryid         =   $request->iid;
                    $Brand->isactive           =   $request->status;
                    $Brand->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Category';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $Category=Category::find($request->id);
                if($Category->category->count()>0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    return \Response::json($arr);
                }
            break;
            case 'brand':
                $Brand=Brand::where('industryid','=',$request->industry_id)->get();
                return \Response::json(compact('Brand')); 
            break;
            default:
            $Brand=Brand::all();
            $i=0;
            $record=array();
            foreach ($Brand as $row) {
                
                $record[$i]['industry']=$row['industry']['name'];
                $record[$i]['brand']=$row['name'];
                $record[$i]['action']='<a href="javascript:editme(\'' . $row['brandid'] . '\')"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $row['brandid'] . '\')"><i class="fa fa-trash-o"></i></a>';
                $i++;
            }
            
            return \Response::json($record);
            break;
               
        }
    }
    public function mstsecurity(){

        return View('setting.security')->with('title','Trading Soft::Security');//
    }
    public function mstsecurity_ajax(){
        switch ($request->type) {

            case 'exist':
                $industry=Industry::where('name','=',$request->pname)->count();
                if($industry>0)
                {
                    echo 'exist';
                }
                else
                {
                    echo '1';
                }
            break;
            
            case 'add':
                $industry=Industry::where('name','=',$request->name)->count();
                if($industry==0)
                {
                    $industry=new Industry;

                    $industry->name         =   $request->name;
                    $industry->isactive     =   $request->status;
                    $industry->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Added Industry';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'ERROR'
                    );
                }
                return \Response::json($arr);

              break;

            case 'search':
                $industry=Industry::find($request->id);
                return \Response::json($industry);
                break;
            
            case 'update':
                
                $industry=Industry::where('name','=',$request->name)->count();
                if($industry==0)
                {
                    $industry=Industry::find($request->id);

                    $industry->name         =   $request->name;
                    $industry->isactive     =   $request->status;
                    $industry->save();
                    $arr = array(
                            'name' => 'Record Saved Sucessfully',
                            'msg' => 'SUCCESS'
                        );
                    $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Industry';

                     $useraction->save();
                }
                else{
                    $arr = array(
                        'msg' => 'EROOR'
                    );
                }
                return \Response::json($arr);
                break;
            case 'delete':

                $industry=Industry::find($request->id);
                if($industry->category->count()>0 || $industry->brand->count()>0)
                {
                    $arr = array(
                        'name' => 'Related Record Exists',
                        'msg' => 'ERROR'
                    );
                    
                }
                else{
                    $industry->delete();
                        $arr = array(
                        'name' => 'Related Record Delete',
                        'msg' => 'A1'
                        );
                        $useraction = new UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Deleted Industry';

                    $useraction->save();
                }
                return \Response::json($arr);
        }
    }
    
}
