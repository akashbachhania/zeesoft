-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2016 at 03:38 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeesoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_productreport`
--

CREATE TABLE IF NOT EXISTS `mst_productreport` (
  `preportid` int(10) UNSIGNED NOT NULL,
  `reportid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `reviewbyid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `reviewdate` date NOT NULL,
  `reviewremark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `reviewstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approvedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approveddate` date NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_productreport`
--

INSERT INTO `mst_productreport` (`preportid`, `reportid`, `productid`, `reviewbyid`, `reviewdate`, `reviewremark`, `reviewstatus`, `approvedby`, `approveddate`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'UUZZ-9984', '2016-02-26', 'asdasd', 'asdasd', 'UUZZ-9984', '2016-02-26', '1', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_productreport`
--
ALTER TABLE `mst_productreport`
  ADD PRIMARY KEY (`preportid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_productreport`
--
ALTER TABLE `mst_productreport`
  MODIFY `preportid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
