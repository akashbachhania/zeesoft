-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2016 at 03:37 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeesoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_productcode`
--

CREATE TABLE IF NOT EXISTS `mst_productcode` (
  `productcodeid` int(10) UNSIGNED NOT NULL,
  `pid` int(11) NOT NULL,
  `prefixid` int(11) NOT NULL,
  `refbyid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `approveid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `refdate` datetime NOT NULL,
  `approvedate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_productcode`
--

INSERT INTO `mst_productcode` (`productcodeid`, `pid`, `prefixid`, `refbyid`, `approveid`, `code`, `status`, `productcode`, `isactive`, `refdate`, `approvedate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'UUZZ-9984', 'UUZZ-9984', '123', 'WAITING', 'DIN : 123', 1, '2016-02-23 05:15:09', '2016-02-23 05:15:09', NULL, NULL),
(2, 2, 1, 'UUZZ-9984', 'UUZZ-9984', '123', 'APPROVED', 'DIN : 123', 1, '2016-02-24 05:33:13', '2016-02-24 05:33:13', NULL, NULL),
(3, 3, 1, 'UUZZ-9984', 'UUZZ-9984', '123', 'APPROVED', 'DIN : 123', 1, '2016-02-24 05:34:41', '2016-02-24 05:34:41', NULL, NULL),
(4, 4, 1, 'UUZZ-9984', 'UUZZ-9984', '123', 'APPROVED', 'DIN : 123', 1, '2016-02-24 06:06:30', '2016-02-24 06:06:30', NULL, NULL),
(5, 4, 4, 'UUZZ-9984', 'UUZZ-9984', 'ddd', 'APPROVED', 'GTIN : ddd', 1, '2016-02-24 13:46:35', '2016-02-24 13:46:35', NULL, NULL),
(6, 4, 4, 'UUZZ-9984', 'UUZZ-9984', 'ddd', 'APPROVED', 'GTIN : ddd', 1, '2016-02-24 13:46:37', '2016-02-24 13:46:37', NULL, NULL),
(7, 1, 1, 'UUZZ-9984', 'UUZZ-9984', '124', 'WAITING', 'DIN : 124', 1, '2016-02-25 09:47:19', '2016-02-25 09:47:19', '2016-02-25 04:17:19', '2016-02-25 04:17:19'),
(8, 1, 3, 'UUZZ-9984', 'UUZZ-9984', '456', 'APPROVED', 'EAN : 456', 1, '2016-02-25 09:52:19', '2016-02-25 09:52:19', '2016-02-25 04:22:19', '2016-02-25 04:22:19'),
(9, 1, 5, 'UUZZ-9984', 'UUZZ-9984', '789', 'APPROVED', 'IBSN : 789', 1, '2016-02-25 09:55:07', '2016-02-25 09:55:07', '2016-02-25 04:25:07', '2016-02-25 04:25:07'),
(10, 1, 3, 'UUZZ-9984', 'UUZZ-9984', '485', 'APPROVED', 'EAN : 485', 1, '2016-02-25 09:55:46', '2016-02-25 09:55:46', '2016-02-25 04:25:46', '2016-02-25 04:25:46'),
(11, 2, 3, 'UUZZ-9984', 'UUZZ-9984', '145', 'APPROVED', 'EAN : 145', 1, '2016-02-25 09:57:18', '2016-02-25 09:57:18', '2016-02-25 04:27:18', '2016-02-25 04:27:18'),
(12, 3, 3, 'UUZZ-9984', 'UUZZ-9984', '152', 'APPROVED', 'EAN : 152', 1, '2016-02-25 10:01:45', '2016-02-25 10:01:45', '2016-02-25 04:31:45', '2016-02-25 04:31:45'),
(13, 1, 5, 'UUZZ-9984', 'UUZZ-9984', '256', 'APPROVED', 'IBSN : 256', 1, '2016-02-25 12:24:41', '2016-02-25 12:24:41', '2016-02-25 06:54:41', '2016-02-25 06:54:41'),
(14, 1, 9, 'UUZZ-9984', 'UUZZ-9984', '145', 'APPROVED', 'Other : 145', 1, '2016-02-25 12:25:20', '2016-02-25 12:25:20', '2016-02-25 06:55:20', '2016-02-25 06:55:20'),
(15, 1, 5, 'UUZZ-9984', 'UUZZ-9984', '485', 'APPROVED', 'IBSN : 485', 1, '2016-02-25 12:25:58', '2016-02-25 12:25:58', '2016-02-25 06:55:58', '2016-02-25 06:55:58'),
(16, 1, 7, 'UUZZ-9984', 'UUZZ-9984', '234', 'APPROVED', 'Model Number : 234', 1, '2016-02-25 12:26:44', '2016-02-25 12:26:44', '2016-02-25 06:56:44', '2016-02-25 06:56:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_productcode`
--
ALTER TABLE `mst_productcode`
  ADD PRIMARY KEY (`productcodeid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_productcode`
--
ALTER TABLE `mst_productcode`
  MODIFY `productcodeid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
