-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2016 at 05:11 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minialpha`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_useraction`
--
DROP TABLE `mst_useraction`;

CREATE TABLE `mst_useraction` (
  `actionid` int(10) UNSIGNED NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_useraction`
--

INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`, `created_at`, `updated_at`) VALUES
(1, 'LYSW-4335', '::1', 'pKy3LsftxNKKcaPFzUDyuX4zd6cWLURVuPGZRVb3', '2016-02-17', '2016-02-17 09:12:08', 'Added User', '2016-02-17 03:42:08', '2016-02-17 03:42:08'),
(2, 'MHSV-1760', '::1', 'pKy3LsftxNKKcaPFzUDyuX4zd6cWLURVuPGZRVb3', '2016-02-17', '2016-02-17 09:12:39', 'Added User', '2016-02-17 03:42:39', '2016-02-17 03:42:39'),
(3, 'WRGG-1154', '::1', 'pKy3LsftxNKKcaPFzUDyuX4zd6cWLURVuPGZRVb3', '2016-02-17', '2016-02-17 09:13:01', 'Added User', '2016-02-17 03:43:01', '2016-02-17 03:43:01'),
(4, 'UMQA-9384', '::1', 'pKy3LsftxNKKcaPFzUDyuX4zd6cWLURVuPGZRVb3', '2016-02-17', '2016-02-17 09:13:33', 'Added User', '2016-02-17 03:43:33', '2016-02-17 03:43:33'),
(5, 'MFRB-3510', '::1', 'pKy3LsftxNKKcaPFzUDyuX4zd6cWLURVuPGZRVb3', '2016-02-17', '2016-02-17 09:14:33', 'Added User', '2016-02-17 03:44:33', '2016-02-17 03:44:33'),
(6, 'RDZU-6861', '::1', 'sVeZ0H85I1xNLVQkae791oIH8sfCI67RnlCVVYts', '2016-02-17', '2016-02-17 15:53:36', 'Added User', '2016-02-17 10:23:36', '2016-02-17 10:23:36'),
(7, 'VDRS-1865', '::1', 'sVeZ0H85I1xNLVQkae791oIH8sfCI67RnlCVVYts', '2016-02-17', '2016-02-17 15:54:07', 'Added User', '2016-02-17 10:24:07', '2016-02-17 10:24:07'),
(8, 'IZZC-9355', '::1', 'sVeZ0H85I1xNLVQkae791oIH8sfCI67RnlCVVYts', '2016-02-17', '2016-02-17 15:54:35', 'Added User', '2016-02-17 10:24:35', '2016-02-17 10:24:35'),
(9, 'CFFX-7759', '::1', 'sVeZ0H85I1xNLVQkae791oIH8sfCI67RnlCVVYts', '2016-02-17', '2016-02-17 15:55:02', 'Added User', '2016-02-17 10:25:02', '2016-02-17 10:25:02'),
(10, 'FEEP-1338', '::1', 'sVeZ0H85I1xNLVQkae791oIH8sfCI67RnlCVVYts', '2016-02-17', '2016-02-17 15:55:35', 'Added User', '2016-02-17 10:25:35', '2016-02-17 10:25:35'),
(11, 'UUZZ-9984', '::1', 'oYKtK0digmSmfnZRFe6u4H7Cox8nKTFjF4bKHXVi', '2016-02-18', '2016-02-18 16:01:18', 'Logged In', '2016-02-18 10:31:18', '2016-02-18 10:31:18'),
(12, 'UUZZ-9984', '::1', 'oYKtK0digmSmfnZRFe6u4H7Cox8nKTFjF4bKHXVi', '2016-02-18', '2016-02-18 17:09:57', 'Logged In', '2016-02-18 11:39:57', '2016-02-18 11:39:57'),
(13, 'UUZZ-9984', '::1', 'oYKtK0digmSmfnZRFe6u4H7Cox8nKTFjF4bKHXVi', '2016-02-18', '2016-02-18 17:39:51', 'Logged In', '2016-02-18 12:09:51', '2016-02-18 12:09:51'),
(14, 'CFFX-7759', '::1', 'oYKtK0digmSmfnZRFe6u4H7Cox8nKTFjF4bKHXVi', '2016-02-18', '2016-02-18 13:09:18', 'Logged In', '2016-02-18 07:39:18', '2016-02-18 07:39:18'),
(15, 'UUZZ-9984', '::1', 'N0T5BfWNGM4TFKUIz9Ds8tAovqOivXmEWyqpJL4l', '2016-02-19', '2016-02-19 07:47:32', 'Logged In', '2016-02-19 02:17:32', '2016-02-19 02:17:32'),
(16, 'UUZZ-9984', '::1', 'BVhuD3uSyv59iAN3qDzohqjOWeIMS8GJsGcTYB0j', '2016-02-19', '2016-02-19 07:59:33', 'Logged In', '2016-02-19 02:29:33', '2016-02-19 02:29:33'),
(17, 'UCCA-7511', '::1', 'BVhuD3uSyv59iAN3qDzohqjOWeIMS8GJsGcTYB0j', '2016-02-19', '2016-02-19 03:41:58', 'Added User', '2016-02-18 22:11:58', '2016-02-18 22:11:58'),
(18, 'UUZZ-9984', '::1', 'v5zFLlxSqlSIVZEHyZXC6UJw4ZRIEc4fSaQGLYEI', '2016-02-19', '2016-02-19 08:48:02', 'Logged In', '2016-02-19 03:18:02', '2016-02-19 03:18:02'),
(19, 'ZBLL-0947', '::1', 'v5zFLlxSqlSIVZEHyZXC6UJw4ZRIEc4fSaQGLYEI', '2016-02-19', '2016-02-19 03:49:52', 'Added User', '2016-02-18 22:19:52', '2016-02-18 22:19:52'),
(20, 'TBIF-9422', '::1', 'v5zFLlxSqlSIVZEHyZXC6UJw4ZRIEc4fSaQGLYEI', '2016-02-19', '2016-02-19 03:52:28', 'Added User', '2016-02-18 22:22:28', '2016-02-18 22:22:28'),
(21, 'JSOL-6911', '::1', 'v5zFLlxSqlSIVZEHyZXC6UJw4ZRIEc4fSaQGLYEI', '2016-02-19', '2016-02-19 03:58:15', 'Added User', '2016-02-18 22:28:15', '2016-02-18 22:28:15'),
(22, 'UUZZ-9984', '::1', 'ZK4j8UtAkuUgq4PTmXsaAfvDGiecby3S4VIuodfc', '2016-02-20', '2016-02-20 07:49:03', 'Logged In', '2016-02-20 02:19:03', '2016-02-20 02:19:03'),
(23, 'UUZZ-9984', '::1', 'raxIxQZNilsuzVKywdINuJZc8rYTUANR7A6h0PZb', '2016-02-22', '2016-02-22 09:41:52', 'Logged In', '2016-02-22 04:11:52', '2016-02-22 04:11:52'),
(24, 'UUZZ-9984', '::1', 'J34T71YsFCiI3hQ415tnaABmO8PWFm4w7CGo4o7y', '2016-02-22', '2016-02-22 13:25:36', 'Logged In', '2016-02-22 07:55:36', '2016-02-22 07:55:36'),
(25, 'UUZZ-9984', '::1', 'sBDfIaY6jZNRG0HWf7a052wOwbWn8xrDgPtGkoB2', '2016-02-23', '2016-02-23 06:41:43', 'Logged In', '2016-02-23 01:11:43', '2016-02-23 01:11:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_useraction`
--
ALTER TABLE `mst_useraction`
  ADD PRIMARY KEY (`actionid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_useraction`
--
ALTER TABLE `mst_useraction`
  MODIFY `actionid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
