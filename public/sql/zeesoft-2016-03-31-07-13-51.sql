-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: minialpha
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MastPage`
--

DROP TABLE IF EXISTS `MastPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MastPage` (
  `PageId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PageName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DisplayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Parent_Id` int(11) NOT NULL,
  `Level_Idx` int(11) NOT NULL,
  `Idx` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PageId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MastPage`
--

LOCK TABLES `MastPage` WRITE;
/*!40000 ALTER TABLE `MastPage` DISABLE KEYS */;
INSERT INTO `MastPage` VALUES (3,'','Customers','Customers',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(4,'mstcustomer','Customers','Browse Customers',3,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(5,'','Product Catalog','Product Catalog',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(6,'add-product','Product Catalog','Add New Product',5,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(7,'product-catalog','Product Catalog','Browse Products',5,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(8,'suggested-product','Product Catalog','Suggested Product',5,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(9,'suggested-product-code','Product Catalog','Suggested Product Code',5,0,4,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(10,'reported-product','Product Catalog','Reported Product',5,0,5,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(11,'','Postings','Postings',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(12,'posting-catalog','Postings','Browse Postings',11,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(13,'my-posting','Postings','My Postings',11,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(14,'import-post','Postings','Import Postings',11,0,4,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(15,'','Product List Download','Products List Download',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(16,'product-listdownload','Product List Download','Products List Download',15,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(17,'','Quote Queue','Quote Queue',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(18,'quoteandpost_que','Quote Queue','Quote Queue',17,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(19,'','Orders','Orders',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(20,'tm-accepted-offers','Orders','Accepted Offers',19,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(21,'tm-completed-offers','Orders','Completed Offers',19,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(22,'tm-cancelled-order','Orders','Cancelled Orders',19,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(23,'','Settings','Settings',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(24,'my-setting','Settings','My Settings',23,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(25,'notification-setting','Settings','Notifications',23,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(26,'notification','Settings','Notifications Settings',23,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(27,'import-data','Settings','Import Data',23,0,4,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(28,'mstindustry','Settings','Industry List',23,0,5,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(29,'mstcategory','Settings','Category List',23,0,6,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(30,'mstsubcategory','Settings','Sub Category List',23,0,7,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(31,'mstbrand','Settings','Brands List',23,0,8,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(32,'mstlocation','Settings','Location',23,0,9,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(34,'mstsecurity-que','Settings','Security Question List',23,0,11,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(35,'mstterms-condition','Settings','Terms & Conditions',23,0,12,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(36,'databackup','Settings','Database Backup',23,0,13,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(37,'maskproductname','Settings','Website Settings',23,0,15,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(38,'menu_permission','Settings','Menu Permission',23,0,16,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(39,'','Reports','Reports',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(40,'report','Reports','Reports',39,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(41,'','Users','Users',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(42,'userdetails','Users','View Users',41,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(43,'activity','Users','Users Activity',41,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(44,'create','Users','Create New User',41,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(45,'mstrole','Settings','Role List',23,0,17,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(46,'databaserestore','Settings','Database Restore',23,0,14,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(47,'my-offer','Postings','My Offers',11,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58');
/*!40000 ALTER TABLE `MastPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MastRolePermission`
--

DROP TABLE IF EXISTS `MastRolePermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MastRolePermission` (
  `RoleId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `ViewP` int(11) NOT NULL,
  `AddP` int(11) NOT NULL,
  `EditP` int(11) NOT NULL,
  `DeleteP` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`RoleId`,`PageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MastRolePermission`
--

LOCK TABLES `MastRolePermission` WRITE;
/*!40000 ALTER TABLE `MastRolePermission` DISABLE KEYS */;
INSERT INTO `MastRolePermission` VALUES (1,4,1,1,1,1,1,0,NULL,NULL),(1,6,1,1,1,1,1,1,NULL,NULL),(1,7,1,1,1,1,1,1,NULL,NULL),(1,12,1,1,1,0,1,0,NULL,NULL),(1,13,1,1,1,0,0,0,NULL,NULL),(1,14,1,1,0,0,0,0,NULL,NULL),(1,16,1,1,1,0,1,1,NULL,NULL),(1,24,1,1,1,0,0,0,NULL,NULL),(1,25,1,1,1,0,0,0,NULL,NULL),(1,27,1,1,1,0,1,0,NULL,NULL),(1,47,1,1,1,0,0,0,NULL,NULL),(2,18,1,1,1,0,0,0,NULL,NULL),(2,24,1,1,1,1,0,0,NULL,NULL),(2,25,1,1,1,1,0,0,NULL,NULL),(3,20,1,1,1,1,0,0,NULL,NULL),(3,21,1,1,1,1,0,0,NULL,NULL),(3,22,1,1,1,1,0,0,NULL,NULL),(3,24,1,1,1,1,0,0,NULL,NULL),(3,25,1,1,1,1,0,0,NULL,NULL),(4,4,1,1,1,1,0,0,NULL,NULL),(4,6,1,1,1,1,1,0,NULL,NULL),(4,7,1,1,1,1,1,0,NULL,NULL),(4,8,1,1,1,1,1,0,NULL,NULL),(4,9,1,1,1,1,1,0,NULL,NULL),(4,10,1,1,1,1,1,0,NULL,NULL),(4,12,1,1,1,0,1,0,NULL,NULL),(4,13,1,1,1,0,1,0,NULL,NULL),(4,14,1,1,1,0,1,0,NULL,NULL),(4,16,1,0,0,0,0,0,NULL,NULL),(4,18,1,1,1,0,1,0,NULL,NULL),(4,20,1,1,1,0,1,0,NULL,NULL),(4,21,1,1,1,0,1,0,NULL,NULL),(4,22,1,1,1,0,1,0,NULL,NULL),(4,24,1,1,1,0,1,0,NULL,NULL),(4,25,1,1,1,0,1,0,NULL,NULL),(4,26,1,1,1,0,1,0,NULL,NULL),(4,27,1,1,1,0,1,0,NULL,NULL),(4,28,1,1,1,0,1,0,NULL,NULL),(4,29,1,1,1,0,1,0,NULL,NULL),(4,30,1,1,1,0,1,0,NULL,NULL),(4,31,1,1,1,0,1,0,NULL,NULL),(4,32,1,1,1,0,1,0,NULL,NULL),(4,34,1,1,1,0,1,0,NULL,NULL),(4,35,1,1,1,0,1,0,NULL,NULL),(4,36,1,1,1,0,1,0,NULL,NULL),(4,37,1,1,1,0,1,0,NULL,NULL),(4,38,1,1,1,0,1,0,NULL,NULL),(4,40,1,0,0,0,0,0,NULL,NULL),(4,42,1,1,1,0,1,0,NULL,NULL),(4,43,1,1,1,0,1,0,NULL,NULL),(4,44,1,1,1,0,1,0,NULL,NULL),(4,45,1,1,1,0,1,0,NULL,NULL),(4,46,1,1,1,0,1,0,NULL,NULL),(4,47,1,1,1,0,1,0,NULL,NULL),(5,4,1,1,1,1,0,0,NULL,NULL),(5,6,1,1,1,1,0,0,NULL,NULL),(5,7,1,1,1,1,0,0,NULL,NULL),(5,8,1,1,1,1,0,0,NULL,NULL),(5,9,1,1,1,1,0,0,NULL,NULL),(5,10,1,1,1,1,0,0,NULL,NULL),(5,12,1,0,1,1,0,0,NULL,NULL),(5,13,1,1,1,0,0,0,NULL,NULL),(5,14,1,1,1,1,0,0,NULL,NULL),(5,16,1,1,1,1,1,1,NULL,NULL),(5,18,1,1,1,1,0,0,NULL,NULL),(5,20,1,1,1,1,0,0,NULL,NULL),(5,21,1,1,1,1,0,0,NULL,NULL),(5,22,1,1,1,1,0,0,NULL,NULL),(5,24,1,1,1,1,0,0,NULL,NULL),(5,25,1,1,1,1,0,0,NULL,NULL),(5,27,1,1,1,1,0,0,NULL,NULL),(5,28,1,1,1,1,0,0,NULL,NULL),(5,29,1,1,1,1,0,0,NULL,NULL),(5,30,1,1,1,1,0,0,NULL,NULL),(5,31,1,1,1,1,0,0,NULL,NULL),(5,32,1,1,1,1,0,0,NULL,NULL),(5,34,1,1,1,1,0,0,NULL,NULL),(5,35,1,1,1,1,0,0,NULL,NULL),(5,36,1,1,1,1,0,0,NULL,NULL),(5,37,1,1,1,1,0,0,NULL,NULL),(5,38,1,1,1,1,0,0,NULL,NULL),(5,40,1,1,1,1,0,0,NULL,NULL),(5,42,1,1,1,1,0,0,NULL,NULL),(5,43,1,1,1,1,0,0,NULL,NULL),(5,44,1,1,1,1,0,0,NULL,NULL),(5,45,1,0,0,0,0,0,NULL,NULL),(5,46,1,1,0,0,0,0,NULL,NULL),(6,4,1,0,0,0,0,0,NULL,NULL),(14,20,1,0,0,0,0,0,NULL,NULL),(14,21,0,0,1,0,0,0,NULL,NULL),(14,22,0,1,0,1,0,0,NULL,NULL),(14,24,0,1,0,0,0,0,NULL,NULL),(14,25,0,0,1,0,0,0,NULL,NULL),(14,26,0,0,0,1,0,0,NULL,NULL),(14,27,0,0,0,1,0,0,NULL,NULL),(14,28,0,0,1,0,0,0,NULL,NULL),(14,29,0,1,0,0,0,0,NULL,NULL),(14,31,1,0,0,0,0,0,NULL,NULL),(14,32,0,1,0,0,0,0,NULL,NULL),(14,36,0,0,1,0,0,0,NULL,NULL),(14,38,1,0,0,0,0,0,NULL,NULL),(15,4,1,1,0,0,0,0,NULL,NULL),(15,6,1,1,1,0,0,0,NULL,NULL),(15,7,1,1,1,0,0,0,NULL,NULL),(17,4,1,1,1,0,0,0,NULL,NULL),(18,4,1,1,1,1,0,0,NULL,NULL),(18,6,1,1,0,0,0,0,NULL,NULL),(18,7,1,1,0,0,0,0,NULL,NULL),(18,12,1,1,0,0,0,0,NULL,NULL),(18,13,1,1,0,0,0,0,NULL,NULL),(18,14,1,1,0,0,0,0,NULL,NULL),(18,24,1,0,0,0,0,0,NULL,NULL),(18,25,1,0,0,0,0,0,NULL,NULL),(18,47,1,1,0,0,0,0,NULL,NULL);
/*!40000 ALTER TABLE `MastRolePermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `count_notification`
--

DROP TABLE IF EXISTS `count_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `count_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tmsg` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `count_notification`
--

LOCK TABLES `count_notification` WRITE;
/*!40000 ALTER TABLE `count_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `count_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countdata`
--

DROP TABLE IF EXISTS `countdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countdata` (
  `id` int(11) NOT NULL,
  `recordcount` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`recordcount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countdata`
--

LOCK TABLES `countdata` WRITE;
/*!40000 ALTER TABLE `countdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `countdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maskproname`
--

DROP TABLE IF EXISTS `maskproname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maskproname` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maskstatus` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maskproname`
--

LOCK TABLES `maskproname` WRITE;
/*!40000 ALTER TABLE `maskproname` DISABLE KEYS */;
INSERT INTO `maskproname` VALUES (1,'1',NULL,'2016-03-31 07:09:13');
/*!40000 ALTER TABLE `maskproname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_permission`
--

DROP TABLE IF EXISTS `menu_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_permission` (
  `rowId` int(11) NOT NULL,
  `pageView` int(11) NOT NULL,
  `pageAdd` int(11) NOT NULL,
  `pageEdit` int(11) NOT NULL,
  `pageDelete` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permission`
--

LOCK TABLES `menu_permission` WRITE;
/*!40000 ALTER TABLE `menu_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_admin_report`
--

DROP TABLE IF EXISTS `mst_admin_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_admin_report` (
  `reportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reportsql` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_admin_report`
--

LOCK TABLES `mst_admin_report` WRITE;
/*!40000 ALTER TABLE `mst_admin_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_admin_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_brand`
--

DROP TABLE IF EXISTS `mst_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_brand` (
  `brandid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `industryid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`brandid`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_brand`
--

LOCK TABLES `mst_brand` WRITE;
/*!40000 ALTER TABLE `mst_brand` DISABLE KEYS */;
INSERT INTO `mst_brand` VALUES (1,'Allergan','1',1,NULL,NULL),(2,'Anika Therapeutics','1',1,NULL,NULL),(3,'AQTIS Medical BV','1',1,NULL,NULL),(4,'Biodermis','1',1,NULL,NULL),(5,'Croma-Pharma GmbH','1',1,NULL,NULL),(6,'Ferring','1',1,NULL,NULL),(7,'Fidia','1',1,NULL,NULL),(8,'Filorga','1',1,NULL,NULL),(9,'Galderma','1',1,NULL,NULL),(10,'Genzyme','1',1,NULL,NULL),(11,'Laboratoire ObvieLine','1',1,NULL,NULL),(12,'Merz','1',1,NULL,NULL),(13,'Prollenium Medical Technologies Inc','1',1,NULL,NULL),(14,'Revitacare','1',1,NULL,NULL),(15,'Sanofi-Aventis','1',1,NULL,NULL),(16,'Seikagaku Corporation','1',1,NULL,NULL),(17,'Skin Tech','1',1,NULL,NULL),(18,'Teoxane','1',1,NULL,NULL),(19,'Vivacy Laboratories','1',1,NULL,NULL),(20,'brand1','',3,'2016-03-29 10:59:04','2016-03-29 12:49:48'),(21,'brands','1',3,'2016-03-29 12:49:33','2016-03-29 12:49:33'),(22,'test12333','1',3,'2016-03-30 07:07:24','2016-03-30 07:07:24'),(23,'testas asd asdfasd','1',3,'2016-03-30 07:07:38','2016-03-30 07:07:38'),(24,'automovie brand','1',6,'2016-03-30 20:07:48','2016-03-30 20:07:48'),(25,'Dental Brand','1',5,'2016-03-30 20:08:01','2016-03-30 20:08:01'),(26,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','',5,'2016-03-31 06:28:52','2016-03-31 06:29:20');
/*!40000 ALTER TABLE `mst_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_category`
--

DROP TABLE IF EXISTS `mst_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_category` (
  `catid` int(10) NOT NULL AUTO_INCREMENT,
  `industryid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `isactive` varchar(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_category`
--

LOCK TABLES `mst_category` WRITE;
/*!40000 ALTER TABLE `mst_category` DISABLE KEYS */;
INSERT INTO `mst_category` VALUES (1,1,'new','1','2016-03-29 12:30:32','2016-03-29 12:30:32'),(4,3,'cat2','1','2016-03-29 12:37:18','2016-03-29 12:37:18'),(5,6,'new automotive','1','2016-03-30 20:05:19','2016-03-30 20:05:19'),(6,5,'Dental Cat','1','2016-03-30 20:05:38','2016-03-30 20:05:38'),(7,4,'Health & Safety Cat','1','2016-03-30 20:05:56','2016-03-30 20:05:56');
/*!40000 ALTER TABLE `mst_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_country`
--

DROP TABLE IF EXISTS `mst_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_country` (
  `countryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`countryid`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_country`
--

LOCK TABLES `mst_country` WRITE;
/*!40000 ALTER TABLE `mst_country` DISABLE KEYS */;
INSERT INTO `mst_country` VALUES (1,'Afghanistan','1',NULL,NULL),(2,'Albania','1',NULL,NULL),(3,'Algeria','1',NULL,NULL),(4,'Andorra','1',NULL,NULL),(5,'Angola','1',NULL,NULL),(6,'Antigua and Barbuda','1',NULL,NULL),(7,'Argentina','1',NULL,NULL),(8,'Armenia','1',NULL,NULL),(9,'Aruba','1',NULL,NULL),(10,'Australia','1',NULL,NULL),(11,'Austria','1',NULL,NULL),(12,'Azerbaijan','1',NULL,NULL),(13,'Bahamas, The','1',NULL,NULL),(14,'Bahrain','1',NULL,NULL),(15,'Bangladesh','1',NULL,NULL),(16,'Barbados','1',NULL,NULL),(17,'Belarus','1',NULL,NULL),(18,'Belgium','1',NULL,NULL),(19,'Belize','1',NULL,NULL),(20,'Benin','1',NULL,NULL),(21,'Bhutan','1',NULL,NULL),(22,'Bolivia','1',NULL,NULL),(23,'Bosnia and Herzegovina','1',NULL,NULL),(24,'Botswana','1',NULL,NULL),(25,'Brazil','1',NULL,NULL),(26,'Brunei','1',NULL,NULL),(27,'Bulgaria','1',NULL,NULL),(28,'Burkina Faso','1',NULL,NULL),(29,'Burma','1',NULL,NULL),(30,'Burundi','1',NULL,NULL),(31,'Cambodia','1',NULL,NULL),(32,'Cameroon','1',NULL,NULL),(33,'Canada','1',NULL,NULL),(34,'Cape Verde','1',NULL,NULL),(35,'Central African Republic','1',NULL,NULL),(36,'Chad','1',NULL,NULL),(37,'Chile','1',NULL,NULL),(38,'China','1',NULL,NULL),(39,'Colombia','1',NULL,NULL),(40,'Comoros','1',NULL,NULL),(41,'Congo, Democratic Republic of the','1',NULL,NULL),(42,'Congo, Republic of the','1',NULL,NULL),(43,'Costa Rica','1',NULL,NULL),(44,'Cote d\'Ivoire','1',NULL,NULL),(45,'Croatia','1',NULL,NULL),(46,'Cuba','1',NULL,NULL),(47,'Curacao','1',NULL,NULL),(48,'Cyprus','1',NULL,NULL),(49,'Czech Republic','1',NULL,NULL),(50,'Denmark','1',NULL,NULL),(51,'Djibouti','1',NULL,NULL),(52,'Dominica','1',NULL,NULL),(53,'Dominican Republic','1',NULL,NULL),(54,'East Timor (see','1',NULL,NULL),(55,'Ecuador','1',NULL,NULL),(56,'Egypt','1',NULL,NULL),(57,'El Salvador','1',NULL,NULL),(58,'Equatorial Guinea','1',NULL,NULL),(59,'Eritrea','1',NULL,NULL),(60,'Estonia','1',NULL,NULL),(61,'Ethiopia','1',NULL,NULL),(62,'Fiji','1',NULL,NULL),(63,'Finland','1',NULL,NULL),(64,'France','1',NULL,NULL),(65,'Gabon','1',NULL,NULL),(66,'Gambia, The','1',NULL,NULL),(67,'Georgia','1',NULL,NULL),(68,'Germany','1',NULL,NULL),(69,'Ghana','1',NULL,NULL),(70,'Greece','1',NULL,NULL),(71,'Grenada','1',NULL,NULL),(72,'Guatemala','1',NULL,NULL),(73,'Guinea','1',NULL,NULL),(74,'Guinea-Bissau','1',NULL,NULL),(75,'Guyana','1',NULL,NULL),(76,'Haiti','1',NULL,NULL),(77,'Holy See','1',NULL,NULL),(78,'Honduras','1',NULL,NULL),(79,'Hong Kong','1',NULL,NULL),(80,'Hungary','1',NULL,NULL),(81,'Iceland','1',NULL,NULL),(82,'India','1',NULL,NULL),(83,'Indonesia','1',NULL,NULL),(84,'Iran','1',NULL,NULL),(85,'Iraq','1',NULL,NULL),(86,'Ireland','1',NULL,NULL),(87,'Israel','1',NULL,NULL),(88,'Italy','1',NULL,NULL),(89,'Jamaica','1',NULL,NULL),(90,'Japan','1',NULL,NULL),(91,'Jordan','1',NULL,NULL),(92,'Kazakhstan','1',NULL,NULL),(93,'Kenya','1',NULL,NULL),(94,'Kiribati','1',NULL,NULL),(95,'Korea, North','1',NULL,NULL),(96,'Korea, South','1',NULL,NULL),(97,'Kosovo','1',NULL,NULL),(98,'Kuwait','1',NULL,NULL),(99,'Kyrgyzstan','1',NULL,NULL),(100,'Laos','1',NULL,NULL),(101,'Latvia','1',NULL,NULL),(102,'Lebanon','1',NULL,NULL),(103,'Lesotho','1',NULL,NULL),(104,'Liberia','1',NULL,NULL),(105,'Libya','1',NULL,NULL),(106,'Liechtenstein','1',NULL,NULL),(107,'Lithuania','1',NULL,NULL),(108,'Luxembourg','1',NULL,NULL),(109,'Macau','1',NULL,NULL),(110,'Macedonia','1',NULL,NULL),(111,'Madagascar','1',NULL,NULL),(112,'Malawi','1',NULL,NULL),(113,'Malaysia','1',NULL,NULL),(114,'Maldives','1',NULL,NULL),(115,'Mali','1',NULL,NULL),(116,'Malta','1',NULL,NULL),(117,'Marshall Islands','1',NULL,NULL),(118,'Mauritania','1',NULL,NULL),(119,'Mauritius','1',NULL,NULL),(120,'Mexico','1',NULL,NULL),(121,'Micronesia','1',NULL,NULL),(122,'Moldova','1',NULL,NULL),(123,'Monaco','1',NULL,NULL),(124,'Mongolia','1',NULL,NULL),(125,'Montenegro','1',NULL,NULL),(126,'Morocco','1',NULL,NULL),(127,'Mozambique','1',NULL,NULL),(128,'Namibia','1',NULL,NULL),(129,'Nauru','1',NULL,NULL),(130,'Nepal','1',NULL,NULL),(131,'Netherlands','1',NULL,NULL),(132,'Netherlands Antilles','1',NULL,NULL),(133,'New Zealand','1',NULL,NULL),(134,'Nicaragua','1',NULL,NULL),(135,'Niger','1',NULL,NULL),(136,'Nigeria','1',NULL,NULL),(137,'North Korea','1',NULL,NULL),(138,'Norway','1',NULL,NULL),(139,'Oman','1',NULL,NULL),(140,'Pakistan','1',NULL,NULL),(141,'Palau','1',NULL,NULL),(142,'Palestinian Territories','1',NULL,NULL),(143,'Panama','1',NULL,NULL),(144,'Papua New Guinea','1',NULL,NULL),(145,'Paraguay','1',NULL,NULL),(146,'Peru','1',NULL,NULL),(147,'Philippines','1',NULL,NULL),(148,'Poland','1',NULL,NULL),(149,'Portugal','1',NULL,NULL),(150,'Qatar','1',NULL,NULL),(151,'Romania','1',NULL,NULL),(152,'Russia','1',NULL,NULL),(153,'Rwanda','1',NULL,NULL),(154,'Saint Kitts and Nevis','1',NULL,NULL),(155,'Saint Lucia','1',NULL,NULL),(156,'Saint Vincent and the Grenadines','1',NULL,NULL),(157,'Samoa','1',NULL,NULL),(158,'San Marino','1',NULL,NULL),(159,'Sao Tome and Principe','1',NULL,NULL),(160,'Saudi Arabia','1',NULL,NULL),(161,'Senegal','1',NULL,NULL),(162,'Serbia','1',NULL,NULL),(163,'Seychelles','1',NULL,NULL),(164,'Sierra Leone','1',NULL,NULL),(165,'Singapore','1',NULL,NULL),(166,'Sint Maarten','1',NULL,NULL),(167,'Slovakia','1',NULL,NULL),(168,'Slovenia','1',NULL,NULL),(169,'Solomon Islands','1',NULL,NULL),(170,'Somalia','1',NULL,NULL),(171,'South Africa','1',NULL,NULL),(172,'South Korea','1',NULL,NULL),(173,'South Sudan','1',NULL,NULL),(174,'Spain','1',NULL,NULL),(175,'Sri Lanka','1',NULL,NULL),(176,'Sudan','1',NULL,NULL),(177,'Suriname','1',NULL,NULL),(178,'Swaziland','1',NULL,NULL),(179,'Sweden','1',NULL,NULL),(180,'Switzerland','1',NULL,NULL),(181,'Syria','1',NULL,NULL),(182,'Taiwan','1',NULL,NULL),(183,'Tajikistan','1',NULL,NULL),(184,'Tanzania','1',NULL,NULL),(185,'Thailand','1',NULL,NULL),(186,'Timor-Leste','1',NULL,NULL),(187,'Togo','1',NULL,NULL),(188,'Tonga','1',NULL,NULL),(189,'Trinidad and Tobago','1',NULL,NULL),(190,'Tunisia','1',NULL,NULL),(191,'Turkey','1',NULL,NULL),(192,'Turkmenistan','1',NULL,NULL),(193,'Tuvalu','1',NULL,NULL),(194,'Uganda','1',NULL,NULL),(195,'Ukraine','1',NULL,NULL),(196,'United Arab Emirates','1',NULL,NULL),(197,'United Kingdom','1',NULL,NULL),(198,'Uruguay','1',NULL,NULL),(199,'Uzbekistan','1',NULL,NULL),(200,'Vanuatu','1',NULL,NULL),(201,'Venezuela','1',NULL,NULL),(202,'Vietnam','1',NULL,NULL),(203,'Yemen','1',NULL,NULL),(204,'Zambia','1',NULL,NULL),(205,'Zimbabwe','1',NULL,NULL),(206,'United States','1',NULL,NULL);
/*!40000 ALTER TABLE `mst_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_currency`
--

DROP TABLE IF EXISTS `mst_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_currency` (
  `currencyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_currency`
--

LOCK TABLES `mst_currency` WRITE;
/*!40000 ALTER TABLE `mst_currency` DISABLE KEYS */;
INSERT INTO `mst_currency` VALUES (1,'USD','1',NULL,NULL),(2,'GBP','1',NULL,NULL),(3,'EUR','1',NULL,NULL);
/*!40000 ALTER TABLE `mst_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_exp_daterange`
--

DROP TABLE IF EXISTS `mst_exp_daterange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_exp_daterange` (
  `exprangeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`exprangeid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_exp_daterange`
--

LOCK TABLES `mst_exp_daterange` WRITE;
/*!40000 ALTER TABLE `mst_exp_daterange` DISABLE KEYS */;
INSERT INTO `mst_exp_daterange` VALUES (1,'Within 3 Months',1,NULL,NULL),(2,'3 - 6 Months',1,NULL,NULL),(3,'6 - 12 Months',1,NULL,NULL),(4,'12 - 18 Months',1,NULL,NULL),(5,'18 - 24 Months',1,NULL,NULL),(6,'24 - 36 Months',1,NULL,NULL),(7,'36+ Months',1,NULL,NULL);
/*!40000 ALTER TABLE `mst_exp_daterange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_industry`
--

DROP TABLE IF EXISTS `mst_industry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_industry` (
  `industryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`industryid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_industry`
--

LOCK TABLES `mst_industry` WRITE;
/*!40000 ALTER TABLE `mst_industry` DISABLE KEYS */;
INSERT INTO `mst_industry` VALUES (1,'new','1','2016-03-29 12:30:12','2016-03-29 12:30:12'),(3,'Industry1','','2016-03-29 12:31:07','2016-03-30 09:51:21'),(4,'Health & Safety','1','2016-03-30 18:24:20','2016-03-30 18:24:20'),(5,'Dental','1','2016-03-30 19:51:48','2016-03-30 19:51:48'),(6,'Automotive','1','2016-03-30 19:54:16','2016-03-30 19:54:16'),(8,'Janitorial','1','2016-03-30 20:15:47','2016-03-30 20:15:47'),(9,'deetest','1','2016-03-31 06:17:03','2016-03-31 06:17:03'),(10,'ddafasd','1','2016-03-31 06:17:31','2016-03-31 06:17:31');
/*!40000 ALTER TABLE `mst_industry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_language`
--

DROP TABLE IF EXISTS `mst_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_language` (
  `languageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`languageid`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_language`
--

LOCK TABLES `mst_language` WRITE;
/*!40000 ALTER TABLE `mst_language` DISABLE KEYS */;
INSERT INTO `mst_language` VALUES (3,'Afrikaans',1,NULL,NULL),(5,'Albanian',1,NULL,NULL),(9,'Arabic',1,NULL,NULL),(11,'Armenian',1,NULL,NULL),(16,'Bosnian',1,NULL,NULL),(18,'Bulgarian',1,NULL,NULL),(19,'Cantonese',1,NULL,NULL),(23,'Chinese',1,NULL,NULL),(28,'Croatian',1,NULL,NULL),(29,'Czech',1,NULL,NULL),(30,'Danish',1,NULL,NULL),(32,'Dutch',1,NULL,NULL),(33,'English',1,NULL,NULL),(35,'Estonian',1,NULL,NULL),(38,'Filipino',1,NULL,NULL),(39,'Finnish',1,NULL,NULL),(40,'French',1,NULL,NULL),(42,'Georgian',1,NULL,NULL),(43,'German',1,NULL,NULL),(44,'Greek',1,NULL,NULL),(47,'Hawaiian',1,NULL,NULL),(48,'Hebrew',1,NULL,NULL),(49,'Hindi',1,NULL,NULL),(50,'Hungarian',1,NULL,NULL),(51,'Icelandic',1,NULL,NULL),(52,'Indonesian',1,NULL,NULL),(56,'Italian',1,NULL,NULL),(57,'Japanese',1,NULL,NULL),(61,'Khmer',1,NULL,NULL),(63,'Korean',1,NULL,NULL),(70,'Livonian',1,NULL,NULL),(74,'Macedonian',1,NULL,NULL),(75,'Malay',1,NULL,NULL),(77,'Mandarin',1,NULL,NULL),(79,'Maori',1,NULL,NULL),(82,'Mongolian',1,NULL,NULL),(83,'Norwegian',1,NULL,NULL),(92,'Pashto',1,NULL,NULL),(93,'Persian',1,NULL,NULL),(95,'Polish',1,NULL,NULL),(96,'Portuguese',1,NULL,NULL),(99,'Romanian',1,NULL,NULL),(100,'Russian',1,NULL,NULL),(101,'Sanskrit',1,NULL,NULL),(104,'Serbian',1,NULL,NULL),(105,'Serbo-Croatian',1,NULL,NULL),(108,'Slovak',1,NULL,NULL),(109,'Slovene',1,NULL,NULL),(110,'Spanish',1,NULL,NULL),(111,'Swahili',1,NULL,NULL),(112,'Swedish',1,NULL,NULL),(113,'Tagalog',1,NULL,NULL),(118,'Thai',1,NULL,NULL),(120,'Turkish',1,NULL,NULL),(122,'Ukrainian',1,NULL,NULL),(124,'Urdu',1,NULL,NULL),(127,'Vietnamese',1,NULL,NULL);
/*!40000 ALTER TABLE `mst_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_location`
--

DROP TABLE IF EXISTS `mst_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_location` (
  `locationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`locationid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_location`
--

LOCK TABLES `mst_location` WRITE;
/*!40000 ALTER TABLE `mst_location` DISABLE KEYS */;
INSERT INTO `mst_location` VALUES (1,'DC-NA',1,NULL,NULL),(3,'DC-EU',1,NULL,NULL),(4,'DC-UK',1,NULL,NULL),(7,'frrjallasndlaksd',1,'2016-03-29 09:20:58','2016-03-29 09:21:35');
/*!40000 ALTER TABLE `mst_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_mngnotification`
--

DROP TABLE IF EXISTS `mst_mngnotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_mngnotification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagename` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_mngnotification`
--

LOCK TABLES `mst_mngnotification` WRITE;
/*!40000 ALTER TABLE `mst_mngnotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_mngnotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_model`
--

DROP TABLE IF EXISTS `mst_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_model` (
  `modelid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_model`
--

LOCK TABLES `mst_model` WRITE;
/*!40000 ALTER TABLE `mst_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_notification`
--

DROP TABLE IF EXISTS `mst_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_notification` (
  `notiid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `msgdate` datetime NOT NULL,
  `msg` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fromuserid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `preportid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notiid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_notification`
--

LOCK TABLES `mst_notification` WRITE;
/*!40000 ALTER TABLE `mst_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_packaging`
--

DROP TABLE IF EXISTS `mst_packaging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_packaging` (
  `pkgid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pkgid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_packaging`
--

LOCK TABLES `mst_packaging` WRITE;
/*!40000 ALTER TABLE `mst_packaging` DISABLE KEYS */;
INSERT INTO `mst_packaging` VALUES (2,'per single unit',0,'2016-03-22 18:32:03','2016-03-22 18:32:03'),(10,'bh',1,'2016-03-30 18:33:59','2016-03-30 18:33:59'),(11,'ffffff',1,'2016-03-31 06:37:32','2016-03-31 06:37:32');
/*!40000 ALTER TABLE `mst_packaging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_pageName`
--

DROP TABLE IF EXISTS `mst_pageName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_pageName` (
  `rowid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pageCheckAdd` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckEdit` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckDelete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckPrint` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckExport` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_pageName`
--

LOCK TABLES `mst_pageName` WRITE;
/*!40000 ALTER TABLE `mst_pageName` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_pageName` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_product`
--

DROP TABLE IF EXISTS `mst_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_product` (
  `productid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `subcatid` int(11) DEFAULT NULL,
  `pakaging` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipingcondition` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qtypercase` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `manufacture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpm` int(11) DEFAULT NULL,
  `addedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `roletype` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img0` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img4` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img5` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img6` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img7` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code1` int(11) NOT NULL,
  `codevalue1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code2` int(11) NOT NULL,
  `codevalue2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code3` int(11) DEFAULT NULL,
  `codevalue3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_product`
--

LOCK TABLES `mst_product` WRITE;
/*!40000 ALTER TABLE `mst_product` DISABLE KEYS */;
INSERT INTO `mst_product` VALUES (1,'AAI-965-082','2016-02-29 00:00:00','Belotero Balance',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Balance','Merz','APPROVED',0,'admin',1,'AD','BELOTERO_Package.jpg','','','','','','','',0,'11686',0,'',0,'',NULL,NULL),(2,'RYD-735-547','2016-02-29 00:00:00','Belotero Balance with Lidocaine',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Balance with Lidocaine','Merz','APPROVED',0,'admin',1,'AD','Merz_Belotero_Pack_BALANCE_4C.jpg','','','','','','','',0,'11724',0,'',0,'',NULL,NULL),(3,'CBN-044-472','2016-02-29 00:00:00','Belotero Basic',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Basic','Merz','APPROVED',0,'admin',1,'AD','belotero basic.jpg','','','','','','','',0,'40175',0,'',0,'',NULL,NULL),(4,'KJG-676-279','2016-02-29 00:00:00','Belotero Intense',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Intense','Merz','APPROVED',0,'admin',1,'AD','Belotero_Intense.jpg','','','','','','','',0,'40531',0,'',0,'',NULL,NULL),(5,'SXW-613-654','2016-02-29 00:00:00','Belotero Intense with Lidocaine',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Intense with Lidocaine','Merz','APPROVED',0,'admin',1,'AD','belotero-intense-lidocaine.jpg','','','','','','','',0,'11698',0,'',0,'',NULL,NULL),(6,'YJJ-535-867','2016-02-29 00:00:00','Belotero Soft',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Soft','Merz','APPROVED',0,'admin',1,'AD','16.jpg','','','','','','','',0,'40176',0,'',0,'',NULL,NULL),(7,'WSR-681-637','2016-02-29 00:00:00','Belotero Soft with Lidocaine',1,12,1,1,'1 x 1,0 ml','','',0,'Belotero Soft with Lidocaine','Merz','APPROVED',0,'admin',1,'AD','belotero-soft-lidocaine1.jpg','','','','','','','',0,'11697',0,'',0,'',NULL,NULL),(8,'LMW-987-070','2016-02-29 00:00:00','Durolane',1,9,2,2,'1 x 3,0ml','','',0,'Durolane','Galderma','APPROVED',0,'admin',1,'AD','8231.Jpg','','','','','','','',0,'1082010',0,'',0,'',NULL,NULL),(9,'VLZ-614-927','2016-02-29 00:00:00','Emervel Classic with Lidocaine',1,9,1,1,'1 x 1,0ml','','',0,'Emervel Classic with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','600090_Image310xMax.jpg','','','','','','','',0,'22321',0,'',0,'',NULL,NULL),(10,'WZG-335-333','2016-02-29 00:00:00','Emervel Deep with Lidocaine',1,9,1,1,'1 x 1,0ml','','',0,'Emervel Deep with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','aaaafte.jpg','','','','','','','',0,'22323',0,'',0,'',NULL,NULL),(11,'XDQ-318-849','2016-02-29 00:00:00','Emervel Lips with Lidocaine',1,9,1,1,'1 x 1,0ml','','',0,'Emervel Lips with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','e lips.png','','','','','','','',0,'22325',0,'',0,'',NULL,NULL),(12,'WAO-616-455','2016-02-29 00:00:00','Emervel Touch',1,9,1,1,'1 x 0,5ml','','',0,'Emervel Touch','Galderma','APPROVED',0,'admin',1,'AD','em_touch.png','','','','','','','',0,'22327',0,'',0,'',NULL,NULL),(13,'OSS-146-592','2016-02-29 00:00:00','Emervel Volume with Lidocaine',1,9,1,1,'1 x 2,0ml','','',0,'Emervel Volume with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','600092_Image310xMax.jpg','','','','','','','',0,'22329',0,'',0,'',NULL,NULL),(14,'XJS-112-292','2016-02-29 00:00:00','Euflexxa',1,6,2,2,'3 x 2,0ml','','',0,'Euflexxa','Ferring','APPROVED',0,'admin',1,'AD','hyaluronan-rooster-comb-injection.jpg','','','','','','','',0,'A-18P-2017',0,'',0,'',NULL,NULL),(15,'ITS-730-652','2016-02-29 00:00:00','Juvederm Hydrate',1,1,1,1,'1 x 1,0 ml','','',0,'Juvederm Hydrate','Allergan','APPROVED',0,'admin',1,'AD','Juvederm-Hydrate-pack.jpg','','','','','','','',0,'93942JR',0,'',0,'',NULL,NULL),(16,'VHJ-911-306','2016-02-29 00:00:00','Juvederm Ultra 2',1,1,1,1,'2 x 0,55 ml','','',0,'Juvederm Ultra 2','Allergan','APPROVED',0,'admin',1,'AD','juvederm2.jpg','','','','','','','',0,'94127JR',0,'',0,'',NULL,NULL),(17,'OMS-644-359','2016-02-29 00:00:00','Juvederm Ultra 3',1,1,1,1,'2 x 1,0 ml','','',0,'Juvederm Ultra 3','Allergan','APPROVED',0,'admin',1,'AD','Juvederm_Ultra_3.jpg','','','','','','','',0,'94555JR',0,'',0,'',NULL,NULL),(18,'XIK-181-418','2016-02-29 00:00:00','Juvederm Ultra 4',1,1,1,1,'2 x 1,0 ml','','',0,'Juvederm Ultra 4','Allergan','APPROVED',0,'admin',1,'AD','9.jpg','','','','','','','',0,'94553JR',0,'',0,'',NULL,NULL),(19,'BJK-469-593','2016-02-29 00:00:00','Juvederm Ultra Smile',1,1,1,1,'2 x 0,55 ml','','',0,'Juvederm Ultra Smile','Allergan','APPROVED',0,'admin',1,'AD','Ultra-smile.jpg','','','','','','','',0,'94131JR',0,'',0,'',NULL,NULL),(20,'JPD-493-173','2016-02-29 00:00:00','Juvederm Volbella with Lidocaine',1,1,1,1,'1 x 1,0ml','','',0,'Juvederm Volbella with Lidocaine','Allergan','APPROVED',0,'admin',1,'AD','Juvederm_Volbella_Lidocaine.jpg','','','','','','','',0,'94615JR',0,'',0,'',NULL,NULL),(21,'RZZ-732-521','2016-02-29 00:00:00','Juvederm Volift with lidocaine',1,1,1,1,'2 x 1,0 ml','','',0,'Juvederm Volift with lidocaine','Allergan','APPROVED',0,'admin',1,'AD','400_2015062014561374.jpeg','','','','','','','',0,'94703JR',0,'',0,'',NULL,NULL),(22,'RUZ-012-946','2016-02-29 00:00:00','Juvederm Voluma with Lidocaine',1,1,1,1,'2 x 1,0 ml','','',0,'Juvederm Voluma with Lidocaine','Allergan','APPROVED',0,'admin',1,'AD','Juvederm-Voluma.jpg','','','','','','','',0,'94506JR',0,'',0,'',NULL,NULL),(23,'RET-456-631','2016-02-29 00:00:00','Macrolane VRF20 10mL',1,9,1,1,'1 x 10ml','','',0,'Macrolane VRF20 10mL','Galderma','APPROVED',0,'admin',1,'AD','Macrolane-VRF-20.jpg','','','','','','','',0,'10-60721',0,'',0,'',NULL,NULL),(24,'CST-489-208','2016-02-29 00:00:00','Macrolane VRF20 20mL',1,9,1,1,'1 x 20ml','','',0,'Macrolane VRF20 20mL','Galderma','APPROVED',0,'admin',1,'AD','Macrolane_vrf20_kopen.jpg','','','','','','','',0,'10-60821',0,'',0,'',NULL,NULL),(25,'EJK-864-637','2016-02-29 00:00:00','Macrolane VRF30 10mL',1,9,1,1,'1 x 10ml','','',0,'Macrolane VRF30 10mL','Galderma','APPROVED',0,'admin',1,'AD','6.jpeg','','','','','','','',0,'10-60521',0,'',0,'',NULL,NULL),(26,'EJV-274-118','2016-02-29 00:00:00','Macrolane VRF30 20mL',1,9,1,1,'1 x 20ml','','',0,'Macrolane VRF30 20mL','Galderma','APPROVED',0,'admin',1,'AD','macrolane1.jpg','','','','','','','',0,'10-60621',0,'',0,'',NULL,NULL),(27,'AIW-742-037','2016-02-29 00:00:00','Orthovisc',1,2,2,2,'1 x 2,0ml','','',0,'Orthovisc','Anika Therapeutics','APPROVED',0,'admin',1,'AD','orthovisc-EU-box.jpg','','','','','','','',0,'630-250',0,'',0,'',NULL,NULL),(28,'TZT-426-271','2016-02-29 00:00:00','Radiesse 0.8mL',1,12,1,1,'1 x 0,8 ml','','',0,'Radiesse 0.8mL','Merz','APPROVED',0,'admin',1,'AD','radiesse_box-new.jpg','','','','','','','',0,'8069M5',0,'',0,'',NULL,NULL),(29,'YIB-674-688','2016-02-29 00:00:00','Radiesse 1.5mL',1,12,1,1,'1 x 1,5 ml','','',0,'Radiesse 1.5mL','Merz','APPROVED',0,'admin',1,'AD','radiesse.jpg','','','','','','','',0,'8071M5',0,'',0,'',NULL,NULL),(30,'HZC-811-178','2016-02-29 00:00:00','Restylane 0.5mL',1,9,1,1,'1 x 0,5 ml','','',0,'Restylane 0.5mL','Galderma','APPROVED',0,'admin',1,'AD','restylane_0,5_ml_guenstig_kaufen.jpg','','','','','','','',0,'10-70102',0,'',0,'',NULL,NULL),(31,'YGU-732-327','2016-02-29 00:00:00','Restylane 1mL',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane 1mL','Galderma','APPROVED',0,'admin',1,'AD','restylane_shop.jpg','','','','','','','',0,'10-70012',0,'',0,'',NULL,NULL),(32,'ATA-072-307','2016-02-29 00:00:00','Restylane Lipp Refresh with Lidocaine',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Lipp Refresh with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','Restylane_Lipp_Refresh_with_Lidocaine_1.JPG','','','','','','','',0,'10-74002',0,'',0,'',NULL,NULL),(33,'ORI-419-145','2016-02-29 00:00:00','Restylane Lipp Volume with Lidocaine',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Lipp Volume with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','lips-resty.png','','','','','','','',0,'11073',0,'',0,'',NULL,NULL),(34,'CBK-252-015','2016-02-29 00:00:00','Restylane Perlane 0.5mL',1,9,1,1,'1 x 0,5 ml','','',0,'Restylane Perlane 0.5mL','Galderma','APPROVED',0,'admin',1,'AD','restylane_perlane_0,5_ml_guenstig_kaufen.jpg','','','','','','','',0,'10-70242',0,'',0,'',NULL,NULL),(35,'PAD-879-998','2016-02-29 00:00:00','Restylane Perlane 1mL',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Perlane 1mL','Galderma','APPROVED',0,'admin',1,'AD','Restylane-perlane-kopen.jpg','','','','','','','',0,'10-70212',0,'',0,'',NULL,NULL),(36,'CHR-877-360','2016-02-29 00:00:00','Restylane Perlane with Lidocaine 0.5mL',1,9,1,1,'1 x 0,5 ml','','',0,'Restylane Perlane with Lidocaine 0.5mL','Galderma','APPROVED',0,'admin',1,'AD','restylane-perlane-lido-1-x-05-ml.jpg','','','','','','','',0,'11069',0,'',0,'',NULL,NULL),(37,'ZMQ-985-632','2016-02-29 00:00:00','Restylane Perlane with Lidocaine 1mL',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Perlane with Lidocaine 1mL','Galderma','APPROVED',0,'admin',1,'AD','restylaneperlanelidoc.jpg','','','','','','','',0,'11064',0,'',0,'',NULL,NULL),(38,'LST-641-418','2016-02-29 00:00:00','Restylane Sub-Q with Lidocaine',1,9,1,1,'1 x 2,0 ml','','',0,'Restylane Sub-Q with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','RESTYLANE-SUBQ-2-ML-RESTYLSQ-1.jpg','','','','','','','',0,'10-78012',0,'',0,'',NULL,NULL),(39,'TBN-118-131','2016-02-29 00:00:00','Restylane Vital',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Vital','Galderma','APPROVED',0,'admin',1,'AD','Restylane_VITAL.jpg','','','','','','','',0,'11039',0,'',0,'',NULL,NULL),(40,'TGN-129-216','2016-02-29 00:00:00','Restylane Vital Injector',1,9,1,1,'1 x 2,0 ml','','',0,'Restylane Vital Injector','Galderma','APPROVED',0,'admin',1,'AD','restylane-vital-injector_1.jpg','','','','','','','',0,'10-72102',0,'',0,'',NULL,NULL),(41,'IXK-218-885','2016-02-29 00:00:00','Restylane Vital Injector with Lidocaine',1,9,1,1,'1 x 2,0 ml','','',0,'Restylane Vital Injector with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','no_image.jpg','','','','','','','',0,'10-73702',0,'',0,'',NULL,NULL),(42,'WYO-351-815','2016-02-29 00:00:00','Restylane Vital Light',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Vital Light','Galderma','APPROVED',0,'admin',1,'AD','restylane-vital-light (1).jpg','','','','','','','',0,'11044',0,'',0,'',NULL,NULL),(43,'MSN-511-932','2016-02-29 00:00:00','Restylane Vital Light Injector',1,9,1,1,'1 x 2,0 ml','','',0,'Restylane Vital Light Injector','Galderma','APPROVED',0,'admin',1,'AD','no_image.jpg','','','','','','','',0,'10-72002',0,'',0,'',NULL,NULL),(44,'UKP-248-598','2016-02-29 00:00:00','Restylane Vital Light Injector with Lidocaine',1,9,1,1,'1 x 2,0 ml','','',0,'Restylane Vital Light Injector with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','no_image.jpg','','','','','','','',0,'10-73902',0,'',0,'',NULL,NULL),(45,'XOS-465-700','2016-02-29 00:00:00','Restylane Vital Light with Lidocaine',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Vital Light with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','Restylane-Vital-Light.jpg','','','','','','','',0,'11051',0,'',0,'',NULL,NULL),(46,'WVJ-223-344','2016-02-29 00:00:00','Restylane Vital with Lidocaine',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane Vital with Lidocaine','Galderma','APPROVED',0,'admin',1,'AD','Restylane-Vital.jpg','','','','','','','',0,'11048',0,'',0,'',NULL,NULL),(47,'HZH-141-841','2016-02-29 00:00:00','Restylane with Lidocaine 0.5mL',1,9,1,1,'1 x 0,5 ml','','',0,'Restylane with Lidocaine 0.5mL','Galderma','APPROVED',0,'admin',1,'AD','restylane_lido_05_ml_kopen.jpg','','','','','','','',0,'11059',0,'',0,'',NULL,NULL),(48,'HDX-513-842','2016-02-29 00:00:00','Restylane with Lidocaine 1mL',1,9,1,1,'1 x 1,0 ml','','',0,'Restylane with Lidocaine 1mL','Galderma','APPROVED',0,'admin',1,'AD','normal-2a1b892d22ad41402c9d77876216a7e6-38389780.jpg','','','','','','','',0,'11053',0,'',0,'',NULL,NULL),(49,'MSJ-849-575','2016-02-29 00:00:00','Sculptra',1,15,1,1,'2 x 3,0ml','','',0,'Sculptra','Sanofi-Aventis','APPROVED',0,'admin',1,'AD','sculptra.large.jpg','','','','','','','',0,'80011917',0,'',0,'',NULL,NULL),(50,'PTK-994-161','2016-02-29 00:00:00','Supartz',1,16,2,2,'5 x 2,5ml','','',0,'Supartz','Seikagaku Corporation','APPROVED',0,'admin',1,'AD','supartz-22039_2.jpg','','','','','','','',0,'30061',0,'',0,'',NULL,NULL),(51,'HVJ-291-630','2016-02-29 00:00:00','Surgiderm 18',1,1,1,1,'2 x 0,8 ml','','',0,'Surgiderm 18','Allergan','APPROVED',0,'admin',1,'AD','Surgiderm-18.jpg','','','','','','','',0,'94138JR',0,'',0,'',NULL,NULL),(52,'FLL-707-777','2016-02-29 00:00:00','Surgiderm 24XP',1,1,1,1,'2 x 0,8 ml','','',0,'Surgiderm 24XP','Allergan','APPROVED',0,'admin',1,'AD','40.jpg','','','','','','','',0,'94139JR',0,'',0,'',NULL,NULL),(53,'LVC-517-741','2016-02-29 00:00:00','Surgiderm 30',1,1,1,1,'2 x 0,8 ml','','',0,'Surgiderm 30','Allergan','APPROVED',0,'admin',1,'AD','Surgiderm-30.jpg','','','','','','','',0,'93956JR',0,'',0,'',NULL,NULL),(54,'DZE-400-799','2016-02-29 00:00:00','Surgiderm 30XP',1,1,1,1,'2 x 0,8 ml','','',0,'Surgiderm 30XP','Allergan','APPROVED',0,'admin',1,'AD','29.png','','','','','','','',0,'93957JR',0,'',0,'',NULL,NULL),(55,'WAZ-493-392','2016-02-29 00:00:00','Synvisc',1,10,2,2,'3 x 2,0ml','','',0,'Synvisc','Genzyme','APPROVED',0,'admin',1,'AD','37.jpg','','','','','','','',0,'2333',0,'',0,'',NULL,NULL),(56,'AXI-806-584','2016-02-29 00:00:00','Synvisc One',1,10,2,2,'1 x 6,0ml','','',0,'Synvisc One','Genzyme','APPROVED',0,'admin',1,'AD','synvisc-one-1379950501.jpg','','','','','','','',0,'2403',0,'',0,'',NULL,NULL),(57,'FRA-600-112','2016-02-29 00:00:00','Ellanse-E',1,3,1,1,'2 x 1,0 ml','','',0,'Ellanse-E','AQTIS Medical BV','APPROVED',0,'admin',1,'AD','600079_Image310xMax.jpg','','','','','','','',0,'Ellanse-E',0,'',0,'',NULL,NULL),(58,'TVC-488-162','2016-02-29 00:00:00','Ellanse-L',1,3,1,1,'2 x 1,0 ml','','',0,'Ellanse-L','AQTIS Medical BV','APPROVED',0,'admin',1,'AD','Ellanse-L-2-x-1-0ml.jpg','','','','','','','',0,'Ellanse-L',0,'',0,'',NULL,NULL),(59,'GBP-741-936','2016-02-29 00:00:00','Ellanse-M',1,3,1,1,'2 x 1,0 ml','','',0,'Ellanse-M','AQTIS Medical BV','APPROVED',0,'admin',1,'AD','ellanse-m1.jpg','','','','','','','',0,'Ellanse-M',0,'',0,'',NULL,NULL),(60,'ZYN-026-478','2016-02-29 00:00:00','Ellanse-M Hands',1,3,1,1,'2 x 1,0 ml','','',0,'Ellanse-M Hands','AQTIS Medical BV','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Ellanse-M Hands',0,'',0,'',NULL,NULL),(61,'PMJ-625-809','2016-02-29 00:00:00','Ellanse-S',1,3,1,1,'2 x 1,0 ml','','',0,'Ellanse-S','AQTIS Medical BV','APPROVED',0,'admin',1,'AD','ellanse-s-2x1-ml.jpg','','','','','','','',0,'Ellanse-S',0,'',0,'',NULL,NULL),(62,'SQT-360-472','2016-02-29 00:00:00','Ellanse-S Hands',1,3,1,1,'2 x 1,0 ml','','',0,'Ellanse-S Hands','AQTIS Medical BV','APPROVED',0,'admin',1,'AD','Ellanse_M_Hands_2_x_1_0.jpg','','','','','','','',0,'Ellanse-S Hands',0,'',0,'',NULL,NULL),(63,'MFL-106-013','2016-02-29 00:00:00','Biodermis  PRO SIL 17 GR',1,4,1,3,'17 gr','','',0,'Biodermis  PRO SIL 17 GR','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis  PRO SIL 17 GR',0,'',0,'',NULL,NULL),(64,'BFK-741-199','2016-02-29 00:00:00','Biodermis  PRO SIL 4.25 GR',1,4,1,3,'4.25 gr','','',0,'Biodermis  PRO SIL 4.25 GR','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis  PRO SIL 4.25 GR',0,'',0,'',NULL,NULL),(65,'EDF-002-266','2016-02-29 00:00:00','Biodermis 1.25 x 1.25 \'\' ( 3.17 x 3.17 cm)',1,4,1,3,'Box of 1','','',0,'Biodermis 1.25 x 1.25 \'\' ( 3.17 x 3.17 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis 1.25 x 1.25 \'\' ( 3.17 x 3.17 cm)',0,'',0,'',NULL,NULL),(66,'ZPC-592-420','2016-02-29 00:00:00','Biodermis 12.6\'\' x 3.8\'\' (32 x 9.63 cm) Mastopexy',1,4,1,3,'Box of 2','','',0,'Biodermis 12.6\'\' x 3.8\'\' (32 x 9.63 cm) Mastopexy','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis 12.6\'\' x 3.8\'\' (32 x 9.63 cm) Mastopexy',0,'',0,'',NULL,NULL),(67,'OGP-050-908','2016-02-29 00:00:00','Biodermis 2.75\'\' round  (6.98 x 1.90 cm)',1,4,1,3,'Box of 2','','',0,'Biodermis 2.75\'\' round  (6.98 x 1.90 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis 2.75\'\' round  (6.98 x 1.90 cm)',0,'',0,'',NULL,NULL),(68,'GLE-803-333','2016-02-29 00:00:00','Biodermis 3\'\' round  (7.62 x 1.90 cm)',1,4,1,3,'Box of 2','','',0,'Biodermis 3\'\' round  (7.62 x 1.90 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis 3\'\' round  (7.62 x 1.90 cm)',0,'',0,'',NULL,NULL),(69,'DIB-490-063','2016-02-29 00:00:00','Biodermis EPI NET 2 X 60\'\'',1,4,1,3,'Box of 1','','',0,'Biodermis EPI NET 2 X 60\'\'','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis EPI NET 2 X 60\'\'',0,'',0,'',NULL,NULL),(70,'WSU-470-246','2016-02-29 00:00:00','Biodermis EPI TAPE 2 X 10',1,4,1,3,'Box of 1','','',0,'Biodermis EPI TAPE 2 X 10','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis EPI TAPE 2 X 10',0,'',0,'',NULL,NULL),(71,'SSY-559-315','2016-02-29 00:00:00','Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14,5 cm)',1,4,1,3,'Box of 5','','',0,'Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14,5 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14,5 cm)',0,'',0,'',NULL,NULL),(72,'DDX-604-412','2016-02-29 00:00:00','Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14.5 cm)',1,4,1,3,'Box of 1','','',0,'Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14.5 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14.5 cm)',0,'',0,'',NULL,NULL),(73,'WNZ-079-694','2016-02-29 00:00:00','Biodermis silicone 1.4 x 12.0\'\' (3.55 x 30.48 cm)',1,4,1,3,'box of 2','','',0,'Biodermis silicone 1.4 x 12.0\'\' (3.55 x 30.48 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis silicone 1.4 x 12.0\'\' (3.55 x 30.48 cm)',0,'',0,'',NULL,NULL),(74,'GLP-632-192','2016-02-29 00:00:00','Biodermis silicone 1.4 x 6.0\'\' ( 3.6 x 15 cm)',1,4,1,3,'Box of 1','','',0,'Biodermis silicone 1.4 x 6.0\'\' ( 3.6 x 15 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis silicone 1.4 x 6.0\'\' ( 3.6 x 15 cm)',0,'',0,'',NULL,NULL),(75,'VAK-952-514','2016-02-29 00:00:00','Biodermis silicone 11 x 16\'\' (27.94 x 40.64 cm)',1,4,1,3,'Box of 1','','',0,'Biodermis silicone 11 x 16\'\' (27.94 x 40.64 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis silicone 11 x 16\'\' (27.94 x 40.64 cm)',0,'',0,'',NULL,NULL),(76,'UJS-153-015','2016-02-29 00:00:00','Biodermis silicone 2 x 2.5 \'\' (5 x 6 cm)',1,4,1,3,'Box of 2','','',0,'Biodermis silicone 2 x 2.5 \'\' (5 x 6 cm)','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis silicone 2 x 2.5 \'\' (5 x 6 cm)',0,'',0,'',NULL,NULL),(77,'ZFI-346-221','2016-02-29 00:00:00','Biodermis SILQUE CLENZ',1,4,1,3,'28.4 ml','','',0,'Biodermis SILQUE CLENZ','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis SILQUE CLENZ',0,'',0,'',NULL,NULL),(78,'YJG-709-488','2016-02-29 00:00:00','Biodermis XERAGEL',1,4,1,3,'10 ML','','',0,'Biodermis XERAGEL','Biodermis','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Biodermis XERAGEL',0,'',0,'',NULL,NULL),(79,'LMH-458-839','2016-02-29 00:00:00','Filorga M-HA 10',1,8,1,1,'3','','',0,'Filorga M-HA 10','Filorga','APPROVED',0,'admin',1,'AD','FILORGA_MHA10_3X3ML_345654_1.jpg','','','','','','','',0,'M-HA 10',0,'',0,'',NULL,NULL),(80,'RGJ-377-922','2016-02-29 00:00:00','Filorga M-HA 18',1,8,1,1,'2','','',0,'Filorga M-HA 18','Filorga','APPROVED',0,'admin',1,'AD','Filorga-M-HA18.jpg','','','','','','','',0,'M-HA 18',0,'',0,'',NULL,NULL),(81,'NWX-469-454','2016-02-29 00:00:00','Filorga NCTF 135',1,8,1,1,'5','','',0,'Filorga NCTF 135','Filorga','APPROVED',0,'admin',1,'AD','Filorga_NCTF_135_CF__01803.1450033588.1280.1280.png','','','','','','','',0,'NCTF 135',0,'',0,'',NULL,NULL),(82,'YWQ-532-214','2016-02-29 00:00:00','Filorga NCTF 135HA ',1,8,1,1,'5','','',0,'Filorga NCTF 135HA ','Filorga','APPROVED',0,'admin',1,'AD','NCTF_135HA.png','','','','','','','',0,'NCTF 135HA',0,'',0,'',NULL,NULL),(83,'PMW-120-852','2016-02-29 00:00:00','Filorga X-HA 3',1,8,1,1,'2','','',0,'Filorga X-HA 3','Filorga','APPROVED',0,'admin',1,'AD','filorga-x-ha3-implant-usieciowanego-kwasu-hialuronowego-.jpg','','','','','','','',0,'X-HA 3',0,'',0,'',NULL,NULL),(84,'ZBF-126-708','2016-02-29 00:00:00','Filorga X-HA Volume, 1ml',1,8,1,1,'2','','',0,'Filorga X-HA Volume, 1ml','Filorga','APPROVED',0,'admin',1,'AD','Filorga-X-HA-Volume.jpg_350x350.jpg','','','','','','','',0,'X-HA Volume',0,'',0,'',NULL,NULL),(85,'MPU-445-849','2016-02-29 00:00:00','Perfectha Derm',1,11,1,1,'1 x 1,0ml','','',0,'Perfectha Derm','Laboratoire ObvieLine','APPROVED',0,'admin',1,'AD','perfecta_derm_432x200_websafe.png','','','','','','','',0,'Perfectha Derm',0,'',0,'',NULL,NULL),(86,'YKI-067-495','2016-02-29 00:00:00','Perfectha Derm Deep ',1,11,1,1,'1 x 1,0ml','','',0,'Perfectha Derm Deep ','Laboratoire ObvieLine','APPROVED',0,'admin',1,'AD','31pueuNcsRL.jpg','','','','','','','',0,'Perfectha Derm Deep ',0,'',0,'',NULL,NULL),(87,'JGN-759-440','2016-02-29 00:00:00','Perfectha Derm FineLines',1,11,1,1,'1 x 0,5ml','','',0,'Perfectha Derm FineLines','Laboratoire ObvieLine','APPROVED',0,'admin',1,'AD','PD-finelines-new.jpg','','','','','','','',0,'Perfectha Derm FineLines',0,'',0,'',NULL,NULL),(88,'VCR-766-103','2016-02-29 00:00:00','Perfectha Derm Subskin',1,11,1,1,'1 x 3,0ml','','',0,'Perfectha Derm Subskin','Laboratoire ObvieLine','APPROVED',0,'admin',1,'AD','PD-subskin-new.jpg','','','','','','','',0,'Perfectha Derm Subskin',0,'',0,'',NULL,NULL),(89,'CPP-460-967','2016-02-29 00:00:00','Princess Filler',1,5,1,1,'1 x 1,0 ml','','',0,'Princess Filler','Croma-Pharma GmbH','APPROVED',0,'admin',1,'AD','0qLFwl2FULtVZ6gXBsv5LI9B4NxTsJ.jpg','','','','','','','',0,'Princess Filler',0,'',0,'',NULL,NULL),(90,'YFB-128-445','2016-02-29 00:00:00','Princess Rich',1,5,1,1,'1 x 1,0 ml','','',0,'Princess Rich','Croma-Pharma GmbH','APPROVED',0,'admin',1,'AD','Princess-Rich-1ml.jpg','','','','','','','',0,'Princess Rich',0,'',0,'',NULL,NULL),(91,'VUR-841-779','2016-02-29 00:00:00','Princess Volume',1,5,1,1,'1 x 1,0 ml','','',0,'Princess Volume','Croma-Pharma GmbH','APPROVED',0,'admin',1,'AD','tCPYGXKLk0jqRFqYXwNERDbRtnhTYR.jpg','','','','','','','',0,'Princess Volume',0,'',0,'',NULL,NULL),(92,'RLF-555-987','2016-02-29 00:00:00','Redexis',1,13,1,1,'1 x 1,0 ml','','',0,'Redexis','Prollenium Medical Technologies Inc','APPROVED',0,'admin',1,'AD','redexis.jpg','','','','','','','',0,'Redexis',0,'',0,'',NULL,NULL),(93,'IUO-943-885','2016-02-29 00:00:00','Redexis Ultra',1,13,1,1,'1 x 1,0 ml','','',0,'Redexis Ultra','Prollenium Medical Technologies Inc','APPROVED',0,'admin',1,'AD','redexis_ultra.jpg','','','','','','','',0,'Redexis Ultra',0,'',0,'',NULL,NULL),(94,'IQQ-366-662','2016-02-29 00:00:00','Restylane Day Cream',1,9,1,4,'1 x 50 ml','','',0,'Restylane Day Cream','Galderma','APPROVED',0,'admin',1,'AD','DayCream_L.jpg','','','','','','','',0,'Restylane Day Cream',0,'',0,'',NULL,NULL),(95,'ZNP-961-001','2016-02-29 00:00:00','Restylane Day Creme SPF 15',1,9,1,4,'1 x 50 ml','','',0,'Restylane Day Creme SPF 15','Galderma','APPROVED',0,'admin',1,'AD','Restylane_WhiteningCream_316x339_LR.JPG','','','','','','','',0,'Restylane Day Creme SPF 15',0,'',0,'',NULL,NULL),(96,'LKT-646-884','2016-02-29 00:00:00','Restylane Eye Serum',1,9,1,4,'1 x 15 ml','','',0,'Restylane Eye Serum','Galderma','APPROVED',0,'admin',1,'AD','EyeSerum_L.jpg','','','','','','','',0,'Restylane Eye Serum',0,'',0,'',NULL,NULL),(97,'PUZ-825-624','2016-02-29 00:00:00','Restylane Facial Cleanser',1,9,1,4,'1 x 100 ml','','',0,'Restylane Facial Cleanser','Galderma','APPROVED',0,'admin',1,'AD','Restylane-Facial-Cleanser-Image.jpg','','','','','','','',0,'Restylane Facial Cleanser',0,'',0,'',NULL,NULL),(98,'FIP-998-038','2016-02-29 00:00:00','Restylane Hand Cream',1,9,1,4,'1 x 50 ml','','',0,'Restylane Hand Cream','Galderma','APPROVED',0,'admin',1,'AD','HandCream_L.jpg','','','','','','','',0,'Restylane Hand Cream',0,'',0,'',NULL,NULL),(99,'FRA-779-150','2016-02-29 00:00:00','Restylane Night Cream',1,9,1,4,'1 x 50 ml','','',0,'Restylane Night Cream','Galderma','APPROVED',0,'admin',1,'AD','NightCream_L.jpg','','','','','','','',0,'Restylane Night Cream',0,'',0,'',NULL,NULL),(100,'QRM-166-592','2016-02-29 00:00:00','Restylane Night Serum',1,9,1,4,'1 x 15 ml','','',0,'Restylane Night Serum','Galderma','APPROVED',0,'admin',1,'AD','1423994388514_nightserum_l.jpg','','','','','','','',0,'Restylane Night Serum',0,'',0,'',NULL,NULL),(101,'KSL-316-648','2016-02-29 00:00:00','Restylane Recover Cream',1,9,1,4,'1 x 25 ml','','',0,'Restylane Recover Cream','Galderma','APPROVED',0,'admin',1,'AD','RecoverCream_L.jpg','','','','','','','',0,'Restylane Recover Cream',0,'',0,'',NULL,NULL),(102,'GFK-957-346','2016-02-29 00:00:00','Restylane Whitening Day Creme',1,9,1,4,'1 x 50 ml','','',0,'Restylane Whitening Day Creme','Galderma','APPROVED',0,'admin',1,'AD','Restylane_WhiteningCream_316x339_LR (1).JPG','','','','','','','',0,'Restylane Whitening Day Creme',0,'',0,'',NULL,NULL),(103,'ITZ-974-411','2016-02-29 00:00:00','Revanesse',1,13,1,1,'2 x 1,0 ml','','',0,'Revanesse','Prollenium Medical Technologies Inc','APPROVED',0,'admin',1,'AD','Revanesse-Box.jpg','','','','','','','',0,'Revanesse',0,'',0,'',NULL,NULL),(104,'VVY-955-652','2016-02-29 00:00:00','Revanesse Lips',1,13,1,1,'2 x 1,0 ml','','',0,'Revanesse Lips','Prollenium Medical Technologies Inc','APPROVED',0,'admin',1,'AD','Revanesse Kiss Box.jpg','','','','','','','',0,'Revanesse Lips',0,'',0,'',NULL,NULL),(105,'XTR-859-127','2016-02-29 00:00:00','Revanesse Ultra',1,13,1,1,'2 x 1,0 ml','','',0,'Revanesse Ultra','Prollenium Medical Technologies Inc','APPROVED',0,'admin',1,'AD','Revanesse Ultra Box Thixofix.jpg','','','','','','','',0,'Revanesse Ultra',0,'',0,'',NULL,NULL),(106,'RZZ-788-335','2016-02-29 00:00:00','REVITACARE Bio-Revitalisation',1,14,1,3,'4 ml + 10 ml','','',0,'REVITACARE Bio-Revitalisation','Revitacare','APPROVED',0,'admin',1,'AD','Revitacare-CytoCare-502.jpg','','','','','','','',0,'REVITACARE CytoCare 502',0,'',0,'',NULL,NULL),(107,'MRK-810-237','2016-02-29 00:00:00','REVITACARE CytoCare 502',1,14,1,3,'10 x 5 ml','','',0,'REVITACARE CytoCare 502','Revitacare','APPROVED',0,'admin',1,'AD','Revitacare-CytoCare-516.jpg','','','','','','','',0,'REVITACARE CytoCare 516',0,'',0,'',NULL,NULL),(108,'PFF-632-868','2016-02-29 00:00:00','REVITACARE CytoCare 516',1,14,1,3,'10 x 5 ml','','',0,'REVITACARE CytoCare 516','Revitacare','APPROVED',0,'admin',1,'AD','Revitacare-CytoCare-532.jpg','','','','','','','',0,'REVITACARE CytoCare 532',0,'',0,'',NULL,NULL),(109,'BJM-222-458','2016-02-29 00:00:00','REVITACARE CytoCare 532',1,14,1,3,'10 x 5 ml','','',0,'REVITACARE CytoCare 532','Revitacare','APPROVED',0,'admin',1,'AD','Revitacare-HairCare.jpg','','','','','','','',0,'REVITACARE HairCare',0,'',0,'',NULL,NULL),(110,'TSV-780-058','2016-02-29 00:00:00','REVITACARE StretchCare',1,14,1,3,'10 x 5 ml','','',0,'REVITACARE StretchCare','Revitacare','APPROVED',0,'admin',1,'AD','Revitacare-StretchCare.jpg','','','','','','','',0,'REVITACARE StretchCare',0,'',0,'',NULL,NULL),(111,'KQR-522-053','2016-02-29 00:00:00','Blending - Bleeching 50 ml',1,17,1,3,'50 ml','','',0,'Blending - Bleeching 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','s-l300.jpg','','','','','','','',0,'Blending - Bleeching 50 ml',0,'',0,'',NULL,NULL),(112,'LOR-878-434','2016-02-29 00:00:00','ACTILIFT 50 ml',1,17,1,3,'50 ml','','',0,'ACTILIFT 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','1394030951-06339700.jpg','','','','','','','',0,'ACTILIFT 50 ml',0,'',0,'',NULL,NULL),(113,'ZRR-255-455','2016-02-29 00:00:00','ATROFILIN 50 ml',1,17,1,3,'50 ml','','',0,'ATROFILIN 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','skin-tech-atrofillin-cream-04b.jpg','','','','','','','',0,'ATROFILIN 50 ml',0,'',0,'',NULL,NULL),(114,'EWF-153-228','2016-02-29 00:00:00','CLEANSER',1,17,1,3,'1 Box','','',0,'CLEANSER','Skin Tech','APPROVED',0,'admin',1,'AD','ST005-2.jpg','','','','','','','',0,'CLEANSER',0,'',0,'',NULL,NULL),(115,'ZQQ-100-997','2016-02-29 00:00:00','DHEA PHYTO  50 ml',1,17,1,3,'50 ml','','',0,'DHEA PHYTO  50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','skin-tech-dhea-phyto-cream-d0c.png','','','','','','','',0,'DHEA PHYTO  50 ml',0,'',0,'',NULL,NULL),(116,'KBB-266-577','2016-02-29 00:00:00','EASY PEN LIGHT',1,17,1,3,'1 Box','','',0,'EASY PEN LIGHT','Skin Tech','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'EASY PEN LIGHT',0,'',0,'',NULL,NULL),(117,'BLH-272-388','2016-02-29 00:00:00','Easy Phytic solution 50 ml',1,17,1,3,'50 ml','','',0,'Easy Phytic solution 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','ST016-2.jpg','','','','','','','',0,'Easy Phytic solution 50 ml',0,'',0,'',NULL,NULL),(118,'XZW-937-763','2016-02-29 00:00:00','LIP & EYELID FORMULA',1,17,1,3,'1 Box','','',0,'LIP & EYELID FORMULA','Skin Tech','APPROVED',0,'admin',1,'AD','lip_eyelid_interior.jpg','','','','','','','',0,'LIP & EYELID FORMULA',0,'',0,'',NULL,NULL),(119,'XMK-403-935','2016-02-29 00:00:00','MELABLOCK HSP - SPF 30 - 50 ml',1,17,1,3,'50 ml','','',0,'MELABLOCK HSP - SPF 30 - 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','skin-tech-melablock-hsp-spf-30-suncream-8f5.png','','','','','','','',0,'MELABLOCK HSP - SPF 30 - 50 ml',0,'',0,'',NULL,NULL),(120,'SPW-147-315','2016-02-29 00:00:00','MELABLOCK HSP - SPF 50 - 50 ml',1,17,1,3,'50 ml','','',0,'MELABLOCK HSP - SPF 50 - 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','41pYoobdiOL._SY300_.jpg','','','','','','','',0,'MELABLOCK HSP - SPF 50 - 50 ml',0,'',0,'',NULL,NULL),(121,'UJO-978-864','2016-02-29 00:00:00','New Easy TCA Classic Kit ( 12 peels + 2 cremes)',1,17,1,3,'12+2','','',0,'New Easy TCA Classic Kit ( 12 peels + 2 cremes)','Skin Tech','APPROVED',0,'admin',1,'AD','10344-01-a.jpg','','','','','','','',0,'New Easy TCA Classic Kit ( 12 peels + 2 cremes)',0,'',0,'',NULL,NULL),(122,'QGW-637-268','2016-02-29 00:00:00','NUTRITIVE ACE LIPOIC COMPLEX',1,17,1,3,'50 ml','','',0,'NUTRITIVE ACE LIPOIC COMPLEX','Skin Tech','APPROVED',0,'admin',1,'AD','41cXKzWavkL._SY355_.jpg','','','','','','','',0,'NUTRITIVE ACE LIPOIC COMPLEX',0,'',0,'',NULL,NULL),(123,'UHI-007-947','2016-02-29 00:00:00','ONLY TOUCH',1,17,1,3,'1 Box','','',0,'ONLY TOUCH','Skin Tech','APPROVED',0,'admin',1,'AD','ST_OnlyTouch_big.jpg','','','','','','','',0,'ONLY TOUCH',0,'',0,'',NULL,NULL),(124,'UIZ-148-936','2016-02-29 00:00:00','IPLASE MASK',1,17,1,3,'50 ml','','',0,'IPLASE MASK','Skin Tech','APPROVED',0,'admin',1,'AD','skin-tech-iplase-mask-9a6.jpg','','','','','','','',0,'IPLASE MASK',0,'',0,'',NULL,NULL),(125,'CVE-617-024','2016-02-29 00:00:00','PURIFYING 50 ML',1,17,1,3,'50 ml','','',0,'PURIFYING 50 ML','Skin Tech','APPROVED',0,'admin',1,'AD','skin-tech-purifying-cream-a5c.png','','','','','','','',0,'PURIFYING 50 ML',0,'',0,'',NULL,NULL),(126,'PPY-304-705','2016-02-29 00:00:00','TCA STARTER KID  ( 4 peels + 1 crème)',1,17,1,3,'4+ 1','','',0,'TCA STARTER KID  ( 4 peels + 1 crème)','Skin Tech','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'TCA STARTER KID  ( 4 peels + 1 crème)',0,'',0,'',NULL,NULL),(127,'GET-619-302','2016-02-29 00:00:00','UNIDEEP',1,17,1,3,'1 Box','','',0,'UNIDEEP','Skin Tech','APPROVED',0,'admin',1,'AD','skintech-tca-peeling-05.jpg','','','','','','','',0,'UNIDEEP',0,'',0,'',NULL,NULL),(128,'DTD-681-669','2016-02-29 00:00:00','VIT. E ANTI OXIDANT 50 ML',1,17,1,3,'50 ml','','',0,'VIT. E ANTI OXIDANT 50 ML','Skin Tech','APPROVED',0,'admin',1,'AD','s-l300 (1).jpg','','','','','','','',0,'VIT. E ANTI OXIDANT 50 ML',0,'',0,'',NULL,NULL),(129,'LEI-944-779','2016-02-29 00:00:00','PURIGEL 50 ml',1,17,1,3,'50 ml','','',0,'PURIGEL 50 ml','Skin Tech','APPROVED',0,'admin',1,'AD','skin-tech-purigel-21b.png','','','','','','','',0,'PURIGEL 50 ml',0,'',0,'',NULL,NULL),(130,'IDT-269-290','2016-02-29 00:00:00','Stylage Hydro',1,19,1,1,'1 x 1,0ml','','',0,'Stylage Hydro','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','hydrostylage.jpg','','','','','','','',0,'Stylage Hydro',0,'',0,'',NULL,NULL),(131,'VSN-455-002','2016-02-29 00:00:00','Stylage Hydro Max',1,19,1,1,'1 x 1,0ml','','',0,'Stylage Hydro Max','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','Vivacy-Stylage-HydroMAX.jpg','','','','','','','',0,'Stylage Hydro Max',0,'',0,'',NULL,NULL),(132,'TML-802-882','2016-02-29 00:00:00','Stylage L',1,19,1,1,'2 x 1,0ml','','',0,'Stylage L','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','Stylage_L.jpg','','','','','','','',0,'Stylage L',0,'',0,'',NULL,NULL),(133,'ICA-033-533','2016-02-29 00:00:00','Stylage Special Lips',1,19,1,1,'1 x 0,8ml','','',0,'Stylage Special Lips','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','Vivacy-Stylage-Special-Lips.jpg','','','','','','','',0,'Stylage Special Lips',0,'',0,'',NULL,NULL),(134,'ZQQ-793-330','2016-02-29 00:00:00','Stylage Special Lips with Lidocaine',1,19,1,1,'1 x 0,8ml','','',0,'Stylage Special Lips with Lidocaine','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','31.jpg','','','','','','','',0,'Stylage Special Lips with Lidocaine',0,'',0,'',NULL,NULL),(135,'MXA-873-005','2016-02-29 00:00:00','Stylage M',1,19,1,1,'2 x 1,0ml','','',0,'Stylage M','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','Vivacy-Stylage-M.jpg','','','','','','','',0,'Stylage M',0,'',0,'',NULL,NULL),(136,'OPP-775-111','2016-02-29 00:00:00','Stylage S',1,19,1,1,'2 x 0,8ml','','',0,'Stylage S','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','STYLAGE-S----2-x-08-ML-STY-1.jpg','','','','','','','',0,'Stylage S',0,'',0,'',NULL,NULL),(137,'YAI-006-666','2016-02-29 00:00:00','Stylage XL',1,19,1,1,'2 x 1,0ml','','',0,'Stylage XL','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','resizedimage2.foto_1_portrait_xl.jpg','','','','','','','',0,'Stylage XL',0,'',0,'',NULL,NULL),(138,'ELX-272-142','2016-02-29 00:00:00','Stylage XXL',1,19,1,1,'2 x 1,0ml','','',0,'Stylage XXL','Vivacy Laboratories','APPROVED',0,'admin',1,'AD','XXL_1425987313.jpeg','','','','','','','',0,'Stylage XXL',0,'',0,'',NULL,NULL),(139,'VZV-942-076','2016-02-29 00:00:00','Teosyal 27G Deep Lines ',1,18,1,1,'2 x 1,0 ml','','',0,'Teosyal 27G Deep Lines ','Teoxane','APPROVED',0,'admin',1,'AD','DEEP LINES.jpg','','','','','','','',0,'Teosyal 27G Deep Lines ',0,'',0,'',NULL,NULL),(140,'VOD-114-488','2016-02-29 00:00:00','Teosyal 27G Deep Lines Puresense ',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal 27G Deep Lines Puresense ','Teoxane','APPROVED',0,'admin',1,'AD','m_88.jpg','','','','','','','',0,'Teosyal 27G Deep Lines Puresense ',0,'',0,'',NULL,NULL),(141,'KGG-177-000','2016-02-29 00:00:00','Teosyal 27G Kiss Puresense ',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal 27G Kiss Puresense ','Teoxane','APPROVED',0,'admin',1,'AD','teosyal_kiss_puresense.jpg','','','','','','','',0,'Teosyal 27G Kiss Puresense ',0,'',0,'',NULL,NULL),(142,'HEX-986-847','2016-02-29 00:00:00','Teosyal 30 G Touch Up ',1,18,1,1,'2 x 0,5 ml','','',0,'Teosyal 30 G Touch Up ','Teoxane','APPROVED',0,'admin',1,'AD','Teosyal_Touch_Up_1.jpg','','','','','','','',0,'Teosyal 30 G Touch Up ',0,'',0,'',NULL,NULL),(143,'YXG-140-770','2016-02-29 00:00:00','Teosyal 30 G Touch Up 0,5ml',1,18,1,1,'2 x 0,5 ml','','',0,'Teosyal 30 G Touch Up 0,5ml','Teoxane','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Teosyal 30 G Touch Up 0,5ml',0,'',0,'',NULL,NULL),(144,'YFI-887-736','2016-02-29 00:00:00','Teosyal 30G First Lines Puresense',1,18,1,1,'2 x 0,7ml','','',0,'Teosyal 30G First Lines Puresense','Teoxane','APPROVED',0,'admin',1,'AD','small.png','','','','','','','',0,'Teosyal 30G First Lines Puresense',0,'',0,'',NULL,NULL),(145,'CGV-832-435','2016-02-29 00:00:00','Teosyal 30G Global Action ',1,18,1,1,'2 x 1,0 ml','','',0,'Teosyal 30G Global Action ','Teoxane','APPROVED',0,'admin',1,'AD','Teosyal-Global-Action.jpg','','','','','','','',0,'Teosyal 30G Global Action ',0,'',0,'',NULL,NULL),(146,'EVV-877-199','2016-02-29 00:00:00','Teosyal 30G Global Action Puresense ',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal 30G Global Action Puresense ','Teoxane','APPROVED',0,'admin',1,'AD','global_action_packshot_0.png','','','','','','','',0,'Teosyal 30G Global Action Puresense ',0,'',0,'',NULL,NULL),(147,'KCX-930-869','2016-02-29 00:00:00','Teosyal First Lines ',1,18,1,1,'2 x 0,7 ml','','',0,'Teosyal First Lines ','Teoxane','APPROVED',0,'admin',1,'AD','FIRST LINES.jpg','','','','','','','',0,'Teosyal First Lines ',0,'',0,'',NULL,NULL),(148,'SVV-993-366','2016-02-29 00:00:00','Teosyal Kiss ',1,18,1,1,'2 x 1,0 ml','','',0,'Teosyal Kiss ','Teoxane','APPROVED',0,'admin',1,'AD','Teosyal-Kiss.jpg','','','','','','','',0,'Teosyal Kiss ',0,'',0,'',NULL,NULL),(149,'ZRZ-733-532','2016-02-29 00:00:00','Teosyal Meso ',1,18,1,1,'2 x 1,0 ml','','',0,'Teosyal Meso ','Teoxane','APPROVED',0,'admin',1,'AD','teosyal-meso-1380291475.jpg','','','','','','','',0,'Teosyal Meso ',0,'',0,'',NULL,NULL),(150,'CAW-444-031','2016-02-29 00:00:00','Teosyal Redensity I Puresense 1,0ml',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal Redensity I Puresense 1,0ml','Teoxane','APPROVED',0,'admin',1,'AD','teosyal_puresense_redensity__teosial__1_4.jpg','','','','','','','',0,'Teosyal Redensity I Puresense 1,0ml',0,'',0,'',NULL,NULL),(151,'FYW-663-344','2016-02-29 00:00:00','Teosyal Redensity I Puresense 3,0ml',1,18,1,1,'1 x 3,0ml','','',0,'Teosyal Redensity I Puresense 3,0ml','Teoxane','APPROVED',0,'admin',1,'AD','Teosyal_Redensity_I_Puresense_3mL.jpg','','','','','','','',0,'Teosyal Redensity I Puresense 3,0ml',0,'',0,'',NULL,NULL),(152,'YCE-423-357','2016-02-29 00:00:00','Teosyal Redensity II Puresense 1,0ml',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal Redensity II Puresense 1,0ml','Teoxane','APPROVED',0,'admin',1,'AD','Teosyal-Puresense-Redensity-II-www.allpara.com_.jpg','','','','','','','',0,'Teosyal Redensity II Puresense 1,0ml',0,'',0,'',NULL,NULL),(153,'LSP-044-430','2016-02-29 00:00:00','Teosyal Ultimate',1,18,1,1,'2 x 1,0 ml','','',0,'Teosyal Ultimate','Teoxane','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Teosyal Ultimate',0,'',0,'',NULL,NULL),(154,'XRT-435-183','2016-02-29 00:00:00','Teosyal Ultimate Puresense 1,0ml',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal Ultimate Puresense 1,0ml','Teoxane','APPROVED',0,'admin',1,'AD','ultimate_packshot_0.jpg','','','','','','','',0,'Teosyal Ultimate Puresense 1,0ml',0,'',0,'',NULL,NULL),(155,'CPF-637-571','2016-02-29 00:00:00','Teosyal Ultimate Puresense 3,0ml',1,18,1,1,'1 x 3,0ml','','',0,'Teosyal Ultimate Puresense 3,0ml','Teoxane','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Teosyal Ultimate Puresense 3,0ml',0,'',0,'',NULL,NULL),(156,'BEV-310-589','2016-02-29 00:00:00','Teosyal Ultra Deep',1,18,1,1,'2 x 1,2ml','','',0,'Teosyal Ultra Deep','Teoxane','APPROVED',0,'admin',1,'AD','ULTRA DEEP.jpg','','','','','','','',0,'Teosyal Ultra Deep',0,'',0,'',NULL,NULL),(157,'IPP-729-446','2016-02-29 00:00:00','Teosyal Ultra Deep Puresense 1,0ml',1,18,1,1,'2 x 1,0ml','','',0,'Teosyal Ultra Deep Puresense 1,0ml','Teoxane','APPROVED',0,'admin',1,'AD','ultra_deep_packshot_0.jpg','','','','','','','',0,'Teosyal Ultra Deep Puresense 1,0ml',0,'',0,'',NULL,NULL),(158,'HIG-016-022','2016-02-29 00:00:00','Teosyal Ultra Deep Puresense 1,2ml',1,18,1,1,'2 x 1,2ml','','',0,'Teosyal Ultra Deep Puresense 1,2ml','Teoxane','APPROVED',0,'admin',1,'AD','no-image.jpg','','','','','','','',0,'Teosyal Ultra Deep Puresense 1,2ml',0,'',0,'',NULL,NULL),(159,'NCI-161-067','2016-02-29 00:00:00','Hyalgan',1,7,2,2,'1 x 2,0ml','','',0,'Hyalgan','Fidia','APPROVED',0,'admin',1,'AD','hyalgan-20mg-2ml.jpg','','','','','','','',0,'Hyalgan',0,'',0,'',NULL,NULL),(160,'FNS-318-927','2016-03-22 14:39:26','Product Name',1,1,1,1,'-Select Packaging-',NULL,'Case Weight',0,'Description','Manufacturer','APPROVED',0,'UUZZ-9984',1,'AD','','','','','','','','',0,'',0,'',NULL,NULL,'2016-03-22 18:39:26','2016-03-22 18:39:26'),(161,'DGK-118-445','2016-03-24 16:24:15','new product',1,1,1,1,'-Select Packaging-',NULL,'123',0,'asdasd','asd','APPROVED',0,'UUZZ-9984',1,'AD','','','','','','','','',0,'',0,'',NULL,NULL,'2016-03-24 20:24:15','2016-03-24 20:24:15'),(162,'MGR-183-236','2016-03-24 16:26:24','new product',1,1,1,1,'-Select Packaging-',NULL,'123',0,'asdasd','asd','APPROVED',0,'UUZZ-9984',1,'AD','','','','','','','','',0,'',0,'',NULL,NULL,'2016-03-24 20:26:24','2016-03-24 20:26:24'),(163,'DCS-266-114','2016-03-30 16:12:18','qweqw',6,24,5,3,'-Select Packaging-','jjj','',0,'asdasd','','APPROVED',0,'UUZZ-9984',1,'AD','','','','','','','','',0,'',0,'',NULL,NULL,'2016-03-30 20:12:18','2016-03-30 20:12:18');
/*!40000 ALTER TABLE `mst_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productcode`
--

DROP TABLE IF EXISTS `mst_productcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productcode` (
  `productcodeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `prefixid` int(11) NOT NULL,
  `refbyid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `approveid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `refdate` datetime NOT NULL,
  `approvedate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productcodeid`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productcode`
--

LOCK TABLES `mst_productcode` WRITE;
/*!40000 ALTER TABLE `mst_productcode` DISABLE KEYS */;
INSERT INTO `mst_productcode` VALUES (1,1,11,'admin','','11686','APPROVED','Product Code:11686',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(2,2,11,'admin','','11724','APPROVED','Product Code:11724',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(3,3,11,'admin','','40175','APPROVED','Product Code:40175',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(4,4,11,'admin','','40531','APPROVED','Product Code:40531',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(5,5,11,'admin','','11698','APPROVED','Product Code:11698',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(6,6,11,'admin','','40176','APPROVED','Product Code:40176',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(7,7,11,'admin','','11697','APPROVED','Product Code:11697',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(8,8,11,'admin','','1082010','APPROVED','Product Code:1082010',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(9,9,11,'admin','','22321','APPROVED','Product Code:22321',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(10,10,11,'admin','','22323','APPROVED','Product Code:22323',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(11,11,11,'admin','','22325','APPROVED','Product Code:22325',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(12,12,11,'admin','','22327','APPROVED','Product Code:22327',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(13,13,11,'admin','','22329','APPROVED','Product Code:22329',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(14,14,11,'admin','','A-18P-2017','APPROVED','Product Code:A-18P-2017',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(15,15,11,'admin','','93942JR','APPROVED','Product Code:93942JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(16,16,11,'admin','','94127JR','APPROVED','Product Code:94127JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(17,17,11,'admin','','94555JR','APPROVED','Product Code:94555JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(18,18,11,'admin','','94553JR','APPROVED','Product Code:94553JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(19,19,11,'admin','','94131JR','APPROVED','Product Code:94131JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(20,20,11,'admin','','94615JR','APPROVED','Product Code:94615JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(21,21,11,'admin','','94703JR','APPROVED','Product Code:94703JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(22,22,11,'admin','','94506JR','APPROVED','Product Code:94506JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(23,23,11,'admin','','10-60721','APPROVED','Product Code:10-60721',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(24,24,11,'admin','','10-60821','APPROVED','Product Code:10-60821',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(25,25,11,'admin','','10-60521','APPROVED','Product Code:10-60521',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(26,26,11,'admin','','10-60621','APPROVED','Product Code:10-60621',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(27,27,11,'admin','','630-250','APPROVED','Product Code:630-250',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(28,28,11,'admin','','8069M5','APPROVED','Product Code:8069M5',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(29,29,11,'admin','','8071M5','APPROVED','Product Code:8071M5',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(30,30,11,'admin','','10-70102','APPROVED','Product Code:10-70102',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(31,31,11,'admin','','10-70012','APPROVED','Product Code:10-70012',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(32,32,11,'admin','','10-74002','APPROVED','Product Code:10-74002',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(33,33,11,'admin','','11073','APPROVED','Product Code:11073',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(34,34,11,'admin','','10-70242','APPROVED','Product Code:10-70242',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(35,35,11,'admin','','10-70212','APPROVED','Product Code:10-70212',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(36,36,11,'admin','','11069','APPROVED','Product Code:11069',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(37,37,11,'admin','','11064','APPROVED','Product Code:11064',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(38,38,11,'admin','','10-78012','APPROVED','Product Code:10-78012',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(39,39,11,'admin','','11039','APPROVED','Product Code:11039',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(40,40,11,'admin','','10-72102','APPROVED','Product Code:10-72102',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(41,41,11,'admin','','10-73702','APPROVED','Product Code:10-73702',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(42,42,11,'admin','','11044','APPROVED','Product Code:11044',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(43,43,11,'admin','','10-72002','APPROVED','Product Code:10-72002',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(44,44,11,'admin','','10-73902','APPROVED','Product Code:10-73902',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(45,45,11,'admin','','11051','APPROVED','Product Code:11051',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(46,46,11,'admin','','11048','APPROVED','Product Code:11048',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(47,47,11,'admin','','11059','APPROVED','Product Code:11059',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(48,48,11,'admin','','11053','APPROVED','Product Code:11053',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(49,49,11,'admin','','80011917','APPROVED','Product Code:80011917',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(50,50,11,'admin','','30061','APPROVED','Product Code:30061',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(51,51,11,'admin','','94138JR','APPROVED','Product Code:94138JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(52,52,11,'admin','','94139JR','APPROVED','Product Code:94139JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(53,53,11,'admin','','93956JR','APPROVED','Product Code:93956JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(54,54,11,'admin','','93957JR','APPROVED','Product Code:93957JR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(55,55,11,'admin','','2333','APPROVED','Product Code:2333',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(56,56,11,'admin','','2403','APPROVED','Product Code:2403',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(57,57,11,'admin','','Ellanse-E','APPROVED','Product Code:Ellanse-E',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(58,58,11,'admin','','Ellanse-L','APPROVED','Product Code:Ellanse-L',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(59,59,11,'admin','','Ellanse-M','APPROVED','Product Code:Ellanse-M',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(60,60,11,'admin','','Ellanse-M Hands','APPROVED','Product Code:Ellanse-M Hands',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(61,61,11,'admin','','Ellanse-S','APPROVED','Product Code:Ellanse-S',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(62,62,11,'admin','','Ellanse-S Hands','APPROVED','Product Code:Ellanse-S Hands',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(63,63,11,'admin','','Biodermis  PRO SIL 17 GR','APPROVED','Product Code:Biodermis  PRO SIL 17 GR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(64,64,11,'admin','','Biodermis  PRO SIL 4.25 GR','APPROVED','Product Code:Biodermis  PRO SIL 4.25 GR',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(65,65,11,'admin','','Biodermis 1.25 x 1.25 \'\' ( 3.17 x 3.17 cm)','APPROVED','Product Code:Biodermis 1.25 x 1.25 \'\' ( 3.17 x 3.17 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(66,66,11,'admin','','Biodermis 12.6\'\' x 3.8\'\' (32 x 9.63 cm) Mastopexy','APPROVED','Product Code:Biodermis 12.6\'\' x 3.8\'\' (32 x 9.63 cm) Mastopexy',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(67,67,11,'admin','','Biodermis 2.75\'\' round  (6.98 x 1.90 cm)','APPROVED','Product Code:Biodermis 2.75\'\' round  (6.98 x 1.90 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(68,68,11,'admin','','Biodermis 3\'\' round  (7.62 x 1.90 cm)','APPROVED','Product Code:Biodermis 3\'\' round  (7.62 x 1.90 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(69,69,11,'admin','','Biodermis EPI NET 2 X 60\'\'','APPROVED','Product Code:Biodermis EPI NET 2 X 60\'\'',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(70,70,11,'admin','','Biodermis EPI TAPE 2 X 10','APPROVED','Product Code:Biodermis EPI TAPE 2 X 10',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(71,71,11,'admin','','Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14,5 cm)','APPROVED','Product Code:Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14,5 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(72,72,11,'admin','','Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14.5 cm)','APPROVED','Product Code:Biodermis silicone  4.7 x 5.7 \'\' ( 12 x 14.5 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(73,73,11,'admin','','Biodermis silicone 1.4 x 12.0\'\' (3.55 x 30.48 cm)','APPROVED','Product Code:Biodermis silicone 1.4 x 12.0\'\' (3.55 x 30.48 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(74,74,11,'admin','','Biodermis silicone 1.4 x 6.0\'\' ( 3.6 x 15 cm)','APPROVED','Product Code:Biodermis silicone 1.4 x 6.0\'\' ( 3.6 x 15 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(75,75,11,'admin','','Biodermis silicone 11 x 16\'\' (27.94 x 40.64 cm)','APPROVED','Product Code:Biodermis silicone 11 x 16\'\' (27.94 x 40.64 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(76,76,11,'admin','','Biodermis silicone 2 x 2.5 \'\' (5 x 6 cm)','APPROVED','Product Code:Biodermis silicone 2 x 2.5 \'\' (5 x 6 cm)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(77,77,11,'admin','','Biodermis SILQUE CLENZ','APPROVED','Product Code:Biodermis SILQUE CLENZ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(78,78,11,'admin','','Biodermis XERAGEL','APPROVED','Product Code:Biodermis XERAGEL',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(79,79,11,'admin','','M-HA 10','APPROVED','Product Code:M-HA 10',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(80,80,11,'admin','','M-HA 18','APPROVED','Product Code:M-HA 18',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(81,81,11,'admin','','NCTF 135','APPROVED','Product Code:NCTF 135',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(82,82,11,'admin','','NCTF 135HA','APPROVED','Product Code:NCTF 135HA',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(83,83,11,'admin','','X-HA 3','APPROVED','Product Code:X-HA 3',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(84,84,11,'admin','','X-HA Volume','APPROVED','Product Code:X-HA Volume',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(85,85,11,'admin','','Perfectha Derm','APPROVED','Product Code:Perfectha Derm',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(86,86,11,'admin','','Perfectha Derm Deep ','APPROVED','Product Code:Perfectha Derm Deep ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(87,87,11,'admin','','Perfectha Derm FineLines','APPROVED','Product Code:Perfectha Derm FineLines',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(88,88,11,'admin','','Perfectha Derm Subskin','APPROVED','Product Code:Perfectha Derm Subskin',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(89,89,11,'admin','','Princess Filler','APPROVED','Product Code:Princess Filler',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(90,90,11,'admin','','Princess Rich','APPROVED','Product Code:Princess Rich',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(91,91,11,'admin','','Princess Volume','APPROVED','Product Code:Princess Volume',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(92,92,11,'admin','','Redexis','APPROVED','Product Code:Redexis',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(93,93,11,'admin','','Redexis Ultra','APPROVED','Product Code:Redexis Ultra',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(94,94,11,'admin','','Restylane Day Cream','APPROVED','Product Code:Restylane Day Cream',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(95,95,11,'admin','','Restylane Day Creme SPF 15','APPROVED','Product Code:Restylane Day Creme SPF 15',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(96,96,11,'admin','','Restylane Eye Serum','APPROVED','Product Code:Restylane Eye Serum',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(97,97,11,'admin','','Restylane Facial Cleanser','APPROVED','Product Code:Restylane Facial Cleanser',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(98,98,11,'admin','','Restylane Hand Cream','APPROVED','Product Code:Restylane Hand Cream',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(99,99,11,'admin','','Restylane Night Cream','APPROVED','Product Code:Restylane Night Cream',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(100,100,11,'admin','','Restylane Night Serum','APPROVED','Product Code:Restylane Night Serum',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(101,101,11,'admin','','Restylane Recover Cream','APPROVED','Product Code:Restylane Recover Cream',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(102,102,11,'admin','','Restylane Whitening Day Creme','APPROVED','Product Code:Restylane Whitening Day Creme',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(103,103,11,'admin','','Revanesse','APPROVED','Product Code:Revanesse',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(104,104,11,'admin','','Revanesse Lips','APPROVED','Product Code:Revanesse Lips',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(105,105,11,'admin','','Revanesse Ultra','APPROVED','Product Code:Revanesse Ultra',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(106,106,11,'admin','','REVITACARE CytoCare 502','APPROVED','Product Code:REVITACARE CytoCare 502',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(107,107,11,'admin','','REVITACARE CytoCare 516','APPROVED','Product Code:REVITACARE CytoCare 516',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(108,108,11,'admin','','REVITACARE CytoCare 532','APPROVED','Product Code:REVITACARE CytoCare 532',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(109,109,11,'admin','','REVITACARE HairCare','APPROVED','Product Code:REVITACARE HairCare',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(110,110,11,'admin','','REVITACARE StretchCare','APPROVED','Product Code:REVITACARE StretchCare',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(111,111,11,'admin','','Blending - Bleeching 50 ml','APPROVED','Product Code:Blending - Bleeching 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(112,112,11,'admin','','ACTILIFT 50 ml','APPROVED','Product Code:ACTILIFT 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(113,113,11,'admin','','ATROFILIN 50 ml','APPROVED','Product Code:ATROFILIN 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(114,114,11,'admin','','CLEANSER','APPROVED','Product Code:CLEANSER',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(115,115,11,'admin','','DHEA PHYTO  50 ml','APPROVED','Product Code:DHEA PHYTO  50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(116,116,11,'admin','','EASY PEN LIGHT','APPROVED','Product Code:EASY PEN LIGHT',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(117,117,11,'admin','','Easy Phytic solution 50 ml','APPROVED','Product Code:Easy Phytic solution 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(118,118,11,'admin','','LIP & EYELID FORMULA','APPROVED','Product Code:LIP & EYELID FORMULA',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(119,119,11,'admin','','MELABLOCK HSP - SPF 30 - 50 ml','APPROVED','Product Code:MELABLOCK HSP - SPF 30 - 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(120,120,11,'admin','','MELABLOCK HSP - SPF 50 - 50 ml','APPROVED','Product Code:MELABLOCK HSP - SPF 50 - 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(121,121,11,'admin','','New Easy TCA Classic Kit ( 12 peels + 2 cremes)','APPROVED','Product Code:New Easy TCA Classic Kit ( 12 peels + 2 cremes)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(122,122,11,'admin','','NUTRITIVE ACE LIPOIC COMPLEX','APPROVED','Product Code:NUTRITIVE ACE LIPOIC COMPLEX',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(123,123,11,'admin','','ONLY TOUCH','APPROVED','Product Code:ONLY TOUCH',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(124,124,11,'admin','','IPLASE MASK','APPROVED','Product Code:IPLASE MASK',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(125,125,11,'admin','','PURIFYING 50 ML','APPROVED','Product Code:PURIFYING 50 ML',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(126,126,11,'admin','','TCA STARTER KID  ( 4 peels + 1 crème)','APPROVED','Product Code:TCA STARTER KID  ( 4 peels + 1 crème)',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(127,127,11,'admin','','UNIDEEP','APPROVED','Product Code:UNIDEEP',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(128,128,11,'admin','','VIT. E ANTI OXIDANT 50 ML','APPROVED','Product Code:VIT. E ANTI OXIDANT 50 ML',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(129,129,11,'admin','','PURIGEL 50 ml','APPROVED','Product Code:PURIGEL 50 ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(130,130,11,'admin','','Stylage Hydro','APPROVED','Product Code:Stylage Hydro',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(131,131,11,'admin','','Stylage Hydro Max','APPROVED','Product Code:Stylage Hydro Max',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(132,132,11,'admin','','Stylage L','APPROVED','Product Code:Stylage L',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(133,133,11,'admin','','Stylage Special Lips','APPROVED','Product Code:Stylage Special Lips',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(134,134,11,'admin','','Stylage Special Lips with Lidocaine','APPROVED','Product Code:Stylage Special Lips with Lidocaine',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(135,135,11,'admin','','Stylage M','APPROVED','Product Code:Stylage M',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(136,136,11,'admin','','Stylage S','APPROVED','Product Code:Stylage S',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(137,137,11,'admin','','Stylage XL','APPROVED','Product Code:Stylage XL',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(138,138,11,'admin','','Stylage XXL','APPROVED','Product Code:Stylage XXL',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(139,139,11,'admin','','Teosyal 27G Deep Lines ','APPROVED','Product Code:Teosyal 27G Deep Lines ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(140,140,11,'admin','','Teosyal 27G Deep Lines Puresense ','APPROVED','Product Code:Teosyal 27G Deep Lines Puresense ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(141,141,11,'admin','','Teosyal 27G Kiss Puresense ','APPROVED','Product Code:Teosyal 27G Kiss Puresense ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(142,142,11,'admin','','Teosyal 30 G Touch Up ','APPROVED','Product Code:Teosyal 30 G Touch Up ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(143,143,11,'admin','','Teosyal 30 G Touch Up 0,5ml','APPROVED','Product Code:Teosyal 30 G Touch Up 0,5ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(144,144,11,'admin','','Teosyal 30G First Lines Puresense','APPROVED','Product Code:Teosyal 30G First Lines Puresense',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(145,145,11,'admin','','Teosyal 30G Global Action ','APPROVED','Product Code:Teosyal 30G Global Action ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(146,146,11,'admin','','Teosyal 30G Global Action Puresense ','APPROVED','Product Code:Teosyal 30G Global Action Puresense ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(147,147,11,'admin','','Teosyal First Lines ','APPROVED','Product Code:Teosyal First Lines ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(148,148,11,'admin','','Teosyal Kiss ','APPROVED','Product Code:Teosyal Kiss ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(149,149,11,'admin','','Teosyal Meso ','APPROVED','Product Code:Teosyal Meso ',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(150,150,11,'admin','','Teosyal Redensity I Puresense 1,0ml','APPROVED','Product Code:Teosyal Redensity I Puresense 1,0ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(151,151,11,'admin','','Teosyal Redensity I Puresense 3,0ml','APPROVED','Product Code:Teosyal Redensity I Puresense 3,0ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(152,152,11,'admin','','Teosyal Redensity II Puresense 1,0ml','APPROVED','Product Code:Teosyal Redensity II Puresense 1,0ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(153,153,11,'admin','','Teosyal Ultimate','APPROVED','Product Code:Teosyal Ultimate',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(154,154,11,'admin','','Teosyal Ultimate Puresense 1,0ml','APPROVED','Product Code:Teosyal Ultimate Puresense 1,0ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(155,155,11,'admin','','Teosyal Ultimate Puresense 3,0ml','APPROVED','Product Code:Teosyal Ultimate Puresense 3,0ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(156,156,11,'admin','','Teosyal Ultra Deep','APPROVED','Product Code:Teosyal Ultra Deep',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(157,157,11,'admin','','Teosyal Ultra Deep Puresense 1,0ml','APPROVED','Product Code:Teosyal Ultra Deep Puresense 1,0ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(158,158,11,'admin','','Teosyal Ultra Deep Puresense 1,2ml','APPROVED','Product Code:Teosyal Ultra Deep Puresense 1,2ml',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(159,159,11,'admin','','Hyalgan','APPROVED','Product Code:Hyalgan',1,'2016-02-29 00:00:00','0000-00-00 00:00:00',NULL,NULL),(160,162,1,'UUZZ-9984','UUZZ-9984','123','APPROVED','DIN:123',1,'2016-03-24 16:26:24','2016-03-24 16:26:24','2016-03-24 20:26:24','2016-03-24 20:26:24'),(161,163,5,'UUZZ-9984','UUZZ-9984','123','APPROVED','IBSN:123',1,'2016-03-30 16:12:18','2016-03-30 16:12:18','2016-03-30 20:12:18','2016-03-30 20:12:18');
/*!40000 ALTER TABLE `mst_productcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productimages`
--

DROP TABLE IF EXISTS `mst_productimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productimages` (
  `productimageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `imageurl` text COLLATE utf8_unicode_ci NOT NULL,
  `srno` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productimageid`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productimages`
--

LOCK TABLES `mst_productimages` WRITE;
/*!40000 ALTER TABLE `mst_productimages` DISABLE KEYS */;
INSERT INTO `mst_productimages` VALUES (1,1,'BELOTERO_Package.jpg',0,1,NULL,NULL),(2,2,'Merz_Belotero_Pack_BALANCE_4C.jpg',0,1,NULL,NULL),(3,3,'belotero basic.jpg',0,1,NULL,NULL),(4,4,'Belotero_Intense.jpg',0,1,NULL,NULL),(5,5,'belotero-intense-lidocaine.jpg',0,1,NULL,NULL),(6,6,'16.jpg',0,1,NULL,NULL),(7,7,'belotero-soft-lidocaine1.jpg',0,1,NULL,NULL),(8,8,'8231.Jpg',0,1,NULL,NULL),(9,9,'600090_Image310xMax.jpg',0,1,NULL,NULL),(10,10,'aaaafte.jpg',0,1,NULL,NULL),(11,11,'e lips.png',0,1,NULL,NULL),(12,12,'em_touch.png',0,1,NULL,NULL),(13,13,'600092_Image310xMax.jpg',0,1,NULL,NULL),(14,14,'hyaluronan-rooster-comb-injection.jpg',0,1,NULL,NULL),(15,15,'Juvederm-Hydrate-pack.jpg',0,1,NULL,NULL),(16,16,'juvederm2.jpg',0,1,NULL,NULL),(17,17,'Juvederm_Ultra_3.jpg',0,1,NULL,NULL),(18,18,'9.jpg',0,1,NULL,NULL),(19,19,'Ultra-smile.jpg',0,1,NULL,NULL),(20,20,'Juvederm_Volbella_Lidocaine.jpg',0,1,NULL,NULL),(21,21,'400_2015062014561374.jpeg',0,1,NULL,NULL),(22,22,'Juvederm-Voluma.jpg',0,1,NULL,NULL),(23,23,'Macrolane-VRF-20.jpg',0,1,NULL,NULL),(24,24,'Macrolane_vrf20_kopen.jpg',0,1,NULL,NULL),(25,25,'6.jpeg',0,1,NULL,NULL),(26,26,'macrolane1.jpg',0,1,NULL,NULL),(27,27,'orthovisc-EU-box.jpg',0,1,NULL,NULL),(28,28,'radiesse_box-new.jpg',0,1,NULL,NULL),(29,29,'radiesse.jpg',0,1,NULL,NULL),(30,30,'restylane_0,5_ml_guenstig_kaufen.jpg',0,1,NULL,NULL),(31,31,'restylane_shop.jpg',0,1,NULL,NULL),(32,32,'Restylane_Lipp_Refresh_with_Lidocaine_1.JPG',0,1,NULL,NULL),(33,33,'lips-resty.png',0,1,NULL,NULL),(34,34,'restylane_perlane_0,5_ml_guenstig_kaufen.jpg',0,1,NULL,NULL),(35,35,'Restylane-perlane-kopen.jpg',0,1,NULL,NULL),(36,36,'restylane-perlane-lido-1-x-05-ml.jpg',0,1,NULL,NULL),(37,37,'restylaneperlanelidoc.jpg',0,1,NULL,NULL),(38,38,'RESTYLANE-SUBQ-2-ML-RESTYLSQ-1.jpg',0,1,NULL,NULL),(39,39,'Restylane_VITAL.jpg',0,1,NULL,NULL),(40,40,'restylane-vital-injector_1.jpg',0,1,NULL,NULL),(41,41,'no_image.jpg',0,1,NULL,NULL),(42,42,'restylane-vital-light (1).jpg',0,1,NULL,NULL),(43,43,'no_image.jpg',0,1,NULL,NULL),(44,44,'no_image.jpg',0,1,NULL,NULL),(45,45,'Restylane-Vital-Light.jpg',0,1,NULL,NULL),(46,46,'Restylane-Vital.jpg',0,1,NULL,NULL),(47,47,'restylane_lido_05_ml_kopen.jpg',0,1,NULL,NULL),(48,48,'normal-2a1b892d22ad41402c9d77876216a7e6-38389780.jpg',0,1,NULL,NULL),(49,49,'sculptra.large.jpg',0,1,NULL,NULL),(50,50,'supartz-22039_2.jpg',0,1,NULL,NULL),(51,51,'Surgiderm-18.jpg',0,1,NULL,NULL),(52,52,'40.jpg',0,1,NULL,NULL),(53,53,'Surgiderm-30.jpg',0,1,NULL,NULL),(54,54,'29.png',0,1,NULL,NULL),(55,55,'37.jpg',0,1,NULL,NULL),(56,56,'synvisc-one-1379950501.jpg',0,1,NULL,NULL),(57,57,'600079_Image310xMax.jpg',0,1,NULL,NULL),(58,58,'Ellanse-L-2-x-1-0ml.jpg',0,1,NULL,NULL),(59,59,'ellanse-m1.jpg',0,1,NULL,NULL),(60,60,'no-image.jpg',0,1,NULL,NULL),(61,61,'ellanse-s-2x1-ml.jpg',0,1,NULL,NULL),(62,62,'Ellanse_M_Hands_2_x_1_0.jpg',0,1,NULL,NULL),(63,63,'no-image.jpg',0,1,NULL,NULL),(64,64,'no-image.jpg',0,1,NULL,NULL),(65,65,'no-image.jpg',0,1,NULL,NULL),(66,66,'no-image.jpg',0,1,NULL,NULL),(67,67,'no-image.jpg',0,1,NULL,NULL),(68,68,'no-image.jpg',0,1,NULL,NULL),(69,69,'no-image.jpg',0,1,NULL,NULL),(70,70,'no-image.jpg',0,1,NULL,NULL),(71,71,'no-image.jpg',0,1,NULL,NULL),(72,72,'no-image.jpg',0,1,NULL,NULL),(73,73,'no-image.jpg',0,1,NULL,NULL),(74,74,'no-image.jpg',0,1,NULL,NULL),(75,75,'no-image.jpg',0,1,NULL,NULL),(76,76,'no-image.jpg',0,1,NULL,NULL),(77,77,'no-image.jpg',0,1,NULL,NULL),(78,78,'no-image.jpg',0,1,NULL,NULL),(79,79,'FILORGA_MHA10_3X3ML_345654_1.jpg',0,1,NULL,NULL),(80,80,'Filorga-M-HA18.jpg',0,1,NULL,NULL),(81,81,'Filorga_NCTF_135_CF__01803.1450033588.1280.1280.png',0,1,NULL,NULL),(82,82,'NCTF_135HA.png',0,1,NULL,NULL),(83,83,'filorga-x-ha3-implant-usieciowanego-kwasu-hialuronowego-.jpg',0,1,NULL,NULL),(84,84,'Filorga-X-HA-Volume.jpg_350x350.jpg',0,1,NULL,NULL),(85,85,'perfecta_derm_432x200_websafe.png',0,1,NULL,NULL),(86,86,'31pueuNcsRL.jpg',0,1,NULL,NULL),(87,87,'PD-finelines-new.jpg',0,1,NULL,NULL),(88,88,'PD-subskin-new.jpg',0,1,NULL,NULL),(89,89,'0qLFwl2FULtVZ6gXBsv5LI9B4NxTsJ.jpg',0,1,NULL,NULL),(90,90,'Princess-Rich-1ml.jpg',0,1,NULL,NULL),(91,91,'tCPYGXKLk0jqRFqYXwNERDbRtnhTYR.jpg',0,1,NULL,NULL),(92,92,'redexis.jpg',0,1,NULL,NULL),(93,93,'redexis_ultra.jpg',0,1,NULL,NULL),(94,94,'DayCream_L.jpg',0,1,NULL,NULL),(95,95,'Restylane_WhiteningCream_316x339_LR.JPG',0,1,NULL,NULL),(96,96,'EyeSerum_L.jpg',0,1,NULL,NULL),(97,97,'Restylane-Facial-Cleanser-Image.jpg',0,1,NULL,NULL),(98,98,'HandCream_L.jpg',0,1,NULL,NULL),(99,99,'NightCream_L.jpg',0,1,NULL,NULL),(100,100,'1423994388514_nightserum_l.jpg',0,1,NULL,NULL),(101,101,'RecoverCream_L.jpg',0,1,NULL,NULL),(102,102,'Restylane_WhiteningCream_316x339_LR (1).JPG',0,1,NULL,NULL),(103,103,'Revanesse-Box.jpg',0,1,NULL,NULL),(104,104,'Revanesse Kiss Box.jpg',0,1,NULL,NULL),(105,105,'Revanesse Ultra Box Thixofix.jpg',0,1,NULL,NULL),(106,106,'Revitacare-CytoCare-502.jpg',0,1,NULL,NULL),(107,107,'Revitacare-CytoCare-516.jpg',0,1,NULL,NULL),(108,108,'Revitacare-CytoCare-532.jpg',0,1,NULL,NULL),(109,109,'Revitacare-HairCare.jpg',0,1,NULL,NULL),(110,110,'Revitacare-StretchCare.jpg',0,1,NULL,NULL),(111,111,'s-l300.jpg',0,1,NULL,NULL),(112,112,'1394030951-06339700.jpg',0,1,NULL,NULL),(113,113,'skin-tech-atrofillin-cream-04b.jpg',0,1,NULL,NULL),(114,114,'ST005-2.jpg',0,1,NULL,NULL),(115,115,'skin-tech-dhea-phyto-cream-d0c.png',0,1,NULL,NULL),(116,116,'no-image.jpg',0,1,NULL,NULL),(117,117,'ST016-2.jpg',0,1,NULL,NULL),(118,118,'lip_eyelid_interior.jpg',0,1,NULL,NULL),(119,119,'skin-tech-melablock-hsp-spf-30-suncream-8f5.png',0,1,NULL,NULL),(120,120,'41pYoobdiOL._SY300_.jpg',0,1,NULL,NULL),(121,121,'10344-01-a.jpg',0,1,NULL,NULL),(122,122,'41cXKzWavkL._SY355_.jpg',0,1,NULL,NULL),(123,123,'ST_OnlyTouch_big.jpg',0,1,NULL,NULL),(124,124,'skin-tech-iplase-mask-9a6.jpg',0,1,NULL,NULL),(125,125,'skin-tech-purifying-cream-a5c.png',0,1,NULL,NULL),(126,126,'no-image.jpg',0,1,NULL,NULL),(127,127,'skintech-tca-peeling-05.jpg',0,1,NULL,NULL),(128,128,'s-l300 (1).jpg',0,1,NULL,NULL),(129,129,'skin-tech-purigel-21b.png',0,1,NULL,NULL),(130,130,'hydrostylage.jpg',0,1,NULL,NULL),(131,131,'Vivacy-Stylage-HydroMAX.jpg',0,1,NULL,NULL),(132,132,'Stylage_L.jpg',0,1,NULL,NULL),(133,133,'Vivacy-Stylage-Special-Lips.jpg',0,1,NULL,NULL),(134,134,'31.jpg',0,1,NULL,NULL),(135,135,'Vivacy-Stylage-M.jpg',0,1,NULL,NULL),(136,136,'STYLAGE-S----2-x-08-ML-STY-1.jpg',0,1,NULL,NULL),(137,137,'resizedimage2.foto_1_portrait_xl.jpg',0,1,NULL,NULL),(138,138,'XXL_1425987313.jpeg',0,1,NULL,NULL),(139,139,'DEEP LINES.jpg',0,1,NULL,NULL),(140,140,'m_88.jpg',0,1,NULL,NULL),(141,141,'teosyal_kiss_puresense.jpg',0,1,NULL,NULL),(142,142,'Teosyal_Touch_Up_1.jpg',0,1,NULL,NULL),(143,143,'no-image.jpg',0,1,NULL,NULL),(144,144,'small.png',0,1,NULL,NULL),(145,145,'Teosyal-Global-Action.jpg',0,1,NULL,NULL),(146,146,'global_action_packshot_0.png',0,1,NULL,NULL),(147,147,'FIRST LINES.jpg',0,1,NULL,NULL),(148,148,'Teosyal-Kiss.jpg',0,1,NULL,NULL),(149,149,'teosyal-meso-1380291475.jpg',0,1,NULL,NULL),(150,150,'teosyal_puresense_redensity__teosial__1_4.jpg',0,1,NULL,NULL),(151,151,'Teosyal_Redensity_I_Puresense_3mL.jpg',0,1,NULL,NULL),(152,152,'Teosyal-Puresense-Redensity-II-www.allpara.com_.jpg',0,1,NULL,NULL),(153,153,'no-image.jpg',0,1,NULL,NULL),(154,154,'ultimate_packshot_0.jpg',0,1,NULL,NULL),(155,155,'no-image.jpg',0,1,NULL,NULL),(156,156,'ULTRA DEEP.jpg',0,1,NULL,NULL),(157,157,'ultra_deep_packshot_0.jpg',0,1,NULL,NULL),(158,158,'no-image.jpg',0,1,NULL,NULL),(159,159,'hyalgan-20mg-2ml.jpg',0,1,NULL,NULL),(160,162,'1458851184_56f44d70c7b56.jpg',1,1,'2016-03-24 20:26:24','2016-03-24 20:26:24');
/*!40000 ALTER TABLE `mst_productimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productprefix`
--

DROP TABLE IF EXISTS `mst_productprefix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productprefix` (
  `proprefixid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`proprefixid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productprefix`
--

LOCK TABLES `mst_productprefix` WRITE;
/*!40000 ALTER TABLE `mst_productprefix` DISABLE KEYS */;
INSERT INTO `mst_productprefix` VALUES (1,'DIN',1,NULL,NULL),(2,'MPN',1,NULL,NULL),(3,'EAN',1,NULL,NULL),(4,'GTIN',1,NULL,NULL),(5,'IBSN',1,NULL,NULL),(6,'Item Code',1,NULL,NULL),(7,'Model Number',1,NULL,NULL),(8,'NDC',1,NULL,NULL),(9,'Other',1,NULL,NULL),(10,'Part Number',1,NULL,NULL),(11,'Product Code',1,NULL,NULL),(12,'UPC',1,NULL,NULL);
/*!40000 ALTER TABLE `mst_productprefix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productreport`
--

DROP TABLE IF EXISTS `mst_productreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productreport` (
  `preportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `reviewbyid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `reviewdate` date NOT NULL,
  `reviewremark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `reviewstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approvedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approveddate` date NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`preportid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productreport`
--

LOCK TABLES `mst_productreport` WRITE;
/*!40000 ALTER TABLE `mst_productreport` DISABLE KEYS */;
INSERT INTO `mst_productreport` VALUES (1,1,1,'UUZZ-9984','2016-02-26','asdasd','asdasd','UUZZ-9984','2016-02-26','1',NULL,NULL);
/*!40000 ALTER TABLE `mst_productreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productsimilar`
--

DROP TABLE IF EXISTS `mst_productsimilar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productsimilar` (
  `similarid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `spid` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`similarid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productsimilar`
--

LOCK TABLES `mst_productsimilar` WRITE;
/*!40000 ALTER TABLE `mst_productsimilar` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_productsimilar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_report`
--

DROP TABLE IF EXISTS `mst_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_report` (
  `reportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_report`
--

LOCK TABLES `mst_report` WRITE;
/*!40000 ALTER TABLE `mst_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_security`
--

DROP TABLE IF EXISTS `mst_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_security` (
  `securityid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qtype` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`securityid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_security`
--

LOCK TABLES `mst_security` WRITE;
/*!40000 ALTER TABLE `mst_security` DISABLE KEYS */;
INSERT INTO `mst_security` VALUES (3,3,'this is for testings','1','2016-03-29 09:41:02','2016-03-29 11:40:07'),(6,1,'pp','1','2016-03-29 09:44:24','2016-03-29 09:44:24'),(8,2,'fd dfdf','1','2016-03-29 09:45:13','2016-03-29 09:46:26'),(9,1,'this is for testing','1','2016-03-29 11:39:45','2016-03-29 11:39:45'),(10,1,'what is your name?','1','2016-03-31 06:41:11','2016-03-31 06:41:11'),(11,2,'what is your mother name?','1','2016-03-31 06:41:37','2016-03-31 06:41:37');
/*!40000 ALTER TABLE `mst_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_shipping`
--

DROP TABLE IF EXISTS `mst_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_shipping` (
  `shipid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shipid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_shipping`
--

LOCK TABLES `mst_shipping` WRITE;
/*!40000 ALTER TABLE `mst_shipping` DISABLE KEYS */;
INSERT INTO `mst_shipping` VALUES (2,0,'Room Temperature',0,'2016-03-22 18:33:43','2016-03-22 18:33:43'),(3,0,'Do Not Freeze',0,'2016-03-22 18:33:49','2016-03-22 18:33:49'),(4,0,'Frozen (-30C or colder)',0,'2016-03-22 18:34:08','2016-03-22 18:34:08'),(5,0,'Cold Chain (2C - 8C)',0,'2016-03-22 18:34:21','2016-03-22 18:34:21'),(6,0,'jjj',1,'2016-03-29 12:13:01','2016-03-29 12:13:01');
/*!40000 ALTER TABLE `mst_shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_subcategory`
--

DROP TABLE IF EXISTS `mst_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_subcategory` (
  `subcatid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subcatid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_subcategory`
--

LOCK TABLES `mst_subcategory` WRITE;
/*!40000 ALTER TABLE `mst_subcategory` DISABLE KEYS */;
INSERT INTO `mst_subcategory` VALUES (1,5,'ffffffff',6,'','2016-03-29 12:31:59','2016-03-31 06:25:05'),(2,4,'subcat',3,'1','2016-03-29 12:42:57','2016-03-29 12:43:09'),(3,5,'automotive subcat',6,'1','2016-03-30 20:06:17','2016-03-30 20:06:17'),(4,6,'dental cat subcat',5,'1','2016-03-30 20:06:33','2016-03-30 20:06:33'),(5,7,'sub4444',4,'','2016-03-31 06:22:16','2016-03-31 06:22:41'),(8,1,'test',1,'1','2016-03-31 07:18:05','2016-03-31 07:18:05');
/*!40000 ALTER TABLE `mst_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_timeframe`
--

DROP TABLE IF EXISTS `mst_timeframe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_timeframe` (
  `timeframeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`timeframeid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_timeframe`
--

LOCK TABLES `mst_timeframe` WRITE;
/*!40000 ALTER TABLE `mst_timeframe` DISABLE KEYS */;
INSERT INTO `mst_timeframe` VALUES (1,'Within 1 Week',1,NULL,NULL),(2,'1 - 2 Weeks',1,NULL,NULL),(3,'2 - 3 Weeks',1,NULL,NULL),(4,'3 - 4 Weeks',1,NULL,NULL),(5,'4 - 6 Weeks',1,NULL,NULL),(6,'6 - 12 Weeks',1,NULL,NULL),(7,'12+ Weeks',1,NULL,NULL);
/*!40000 ALTER TABLE `mst_timeframe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_timezone`
--

DROP TABLE IF EXISTS `mst_timezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_timezone` (
  `timezoneid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `timezonevalue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`timezoneid`)
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_timezone`
--

LOCK TABLES `mst_timezone` WRITE;
/*!40000 ALTER TABLE `mst_timezone` DISABLE KEYS */;
INSERT INTO `mst_timezone` VALUES (1,'Africa/Abidjan','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(2,'Africa/Accra','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(3,'Africa/Addis_Ababa','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(4,'Africa/Algiers','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(5,'Africa/Asmara','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(6,'Africa/Bamako','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(7,'Africa/Bangui','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(8,'Africa/Banjul','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(9,'Africa/Bissau','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(10,'Africa/Blantyre','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(11,'Africa/Brazzaville','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(12,'Africa/Bujumbura','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(13,'Africa/Cairo','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(14,'Africa/Casablanca','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(15,'Africa/Ceuta','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(16,'Africa/Conakry','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(17,'Africa/Dakar','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(18,'Africa/Dar_es_Salaam','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(19,'Africa/Djibouti','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(20,'Africa/Douala','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(21,'Africa/El_Aaiun','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(22,'Africa/Freetown','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(23,'Africa/Gaborone','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(24,'Africa/Harare','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(25,'Africa/Johannesburg','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(26,'Africa/Juba','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(27,'Africa/Kampala','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(28,'Africa/Khartoum','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(29,'Africa/Kigali','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(30,'Africa/Kinshasa','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(31,'Africa/Lagos','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(32,'Africa/Libreville','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(33,'Africa/Lome','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(34,'Africa/Luanda','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(35,'Africa/Lubumbashi','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(36,'Africa/Lusaka','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(37,'Africa/Malabo','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(38,'Africa/Maputo','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(39,'Africa/Maseru','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(40,'Africa/Mbabane','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(41,'Africa/Mogadishu','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(42,'Africa/Monrovia','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(43,'Africa/Nairobi','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(44,'Africa/Ndjamena','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(45,'Africa/Niamey','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(46,'Africa/Nouakchott','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(47,'Africa/Ouagadougou','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(48,'Africa/Porto-Novo','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(49,'Africa/Sao_Tome','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(50,'Africa/Tripoli','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(51,'Africa/Tunis','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(52,'Africa/Windhoek','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(53,'America/Adak','1','UTC/GMT -10:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(54,'America/Anchorage','1','UTC/GMT -09:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(55,'America/Anguilla','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(56,'America/Antigua','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(57,'America/Araguaina','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(58,'America/Argentina/Buenos_Aires','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(59,'America/Argentina/Catamarca','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(60,'America/Argentina/Cordoba','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(61,'America/Argentina/Jujuy','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(62,'America/Argentina/La_Rioja','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(63,'America/Argentina/Mendoza','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(64,'America/Argentina/Rio_Gallegos','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(65,'America/Argentina/Salta','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(66,'America/Argentina/San_Juan','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(67,'America/Argentina/San_Luis','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(68,'America/Argentina/Tucuman','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(69,'America/Argentina/Ushuaia','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(70,'America/Aruba','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(71,'America/Asuncion','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(72,'America/Atikokan','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(73,'America/Bahia','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(74,'America/Bahia_Banderas','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(75,'America/Barbados','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(76,'America/Belem','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(77,'America/Belize','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(78,'America/Blanc-Sablon','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(79,'America/Boa_Vista','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(80,'America/Bogota','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(81,'America/Boise','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(82,'America/Cambridge_Bay','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(83,'America/Campo_Grande','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(84,'America/Cancun','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(85,'America/Caracas','1','UTC/GMT -04:30','2016-02-18 14:07:57','2016-02-18 14:07:57'),(86,'America/Cayenne','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(87,'America/Cayman','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(88,'America/Chicago','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(89,'America/Chihuahua','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(90,'America/Costa_Rica','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(91,'America/Creston','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(92,'America/Cuiaba','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(93,'America/Curacao','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(94,'America/Danmarkshavn','1','UTC/GMT +00:00','2016-02-18 06:37:57','2016-02-18 06:37:57'),(95,'America/Dawson','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(96,'America/Dawson_Creek','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(97,'America/Denver','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(98,'America/Detroit','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(99,'America/Dominica','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(100,'America/Edmonton','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(101,'America/Eirunepe','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(102,'America/El_Salvador','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(103,'America/Fort_Nelson','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(104,'America/Fortaleza','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(105,'America/Glace_Bay','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(106,'America/Godthab','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(107,'America/Goose_Bay','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(108,'America/Grand_Turk','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(109,'America/Grenada','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(110,'America/Guadeloupe','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(111,'America/Guatemala','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(112,'America/Guayaquil','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(113,'America/Guyana','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(114,'America/Halifax','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(115,'America/Havana','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(116,'America/Hermosillo','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(117,'America/Indiana/Indianapolis','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(118,'America/Indiana/Knox','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(119,'America/Indiana/Marengo','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(120,'America/Indiana/Petersburg','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(121,'America/Indiana/Tell_City','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(122,'America/Indiana/Vevay','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(123,'America/Indiana/Vincennes','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(124,'America/Indiana/Winamac','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(125,'America/Inuvik','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(126,'America/Iqaluit','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(127,'America/Jamaica','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(128,'America/Juneau','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(129,'America/Kentucky/Louisville','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(130,'America/Kentucky/Monticello','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(131,'America/Kralendijk','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(132,'America/La_Paz','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(133,'America/Lima','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(134,'America/Los_Angeles','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(135,'America/Lower_Princes','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(136,'America/Maceio','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(137,'America/Managua','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(138,'America/Manaus','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(139,'America/Marigot','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(140,'America/Martinique','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(141,'America/Matamoros','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(142,'America/Mazatlan','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(143,'America/Menominee','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(144,'America/Merida','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(145,'America/Metlakatla','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(146,'America/Mexico_City','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(147,'America/Miquelon','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(148,'America/Moncton','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(149,'America/Monterrey','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(150,'America/Montevideo','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(151,'America/Montserrat','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(152,'America/Nassau','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(153,'America/New_York','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(154,'America/Nipigon','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(155,'America/Nome','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(156,'America/Noronha','1','UTC/GMT -02:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(157,'America/North_Dakota/Beulah','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(158,'America/North_Dakota/Center','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(159,'America/North_Dakota/New_Salem','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(160,'America/Ojinaga','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(161,'America/Panama','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(162,'America/Pangnirtung','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(163,'America/Paramaribo','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(164,'America/Phoenix','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(165,'America/Port-au-Prince','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(166,'America/Port_of_Spain','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(167,'America/Porto_Velho','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(168,'America/Puerto_Rico','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(169,'America/Rainy_River','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(170,'America/Rankin_Inlet','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(171,'America/Recife','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(172,'America/Regina','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(173,'America/Resolute','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(174,'America/Rio_Branco','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(175,'America/Santa_Isabel','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(176,'America/Santarem','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(177,'America/Santiago','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(178,'America/Santo_Domingo','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(179,'America/Sao_Paulo','1','UTC/GMT -02:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(180,'America/Scoresbysund','1','UTC/GMT -01:00','2016-02-18 17:37:57','2016-02-18 17:37:57'),(181,'America/Sitka','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(182,'America/St_Barthelemy','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(183,'America/St_Johns','1','UTC/GMT -03:30','2016-02-18 15:07:57','2016-02-18 15:07:57'),(184,'America/St_Kitts','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(185,'America/St_Lucia','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(186,'America/St_Thomas','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(187,'America/St_Vincent','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(188,'America/Swift_Current','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(189,'America/Tegucigalpa','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(190,'America/Thule','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(191,'America/Thunder_Bay','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(192,'America/Tijuana','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(193,'America/Toronto','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(194,'America/Tortola','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(195,'America/Vancouver','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(196,'America/Whitehorse','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(197,'America/Winnipeg','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(198,'America/Yakutat','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(199,'America/Yellowknife','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(200,'Antarctica/Casey','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(201,'Antarctica/Davis','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(202,'Antarctica/DumontDUrville','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(203,'Antarctica/Macquarie','1','UTC/GMT +11:00','2016-02-19 17:37:57','2016-02-19 17:37:57'),(204,'Antarctica/Mawson','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(205,'Antarctica/McMurdo','1','UTC/GMT +13:00','2016-02-19 07:37:57','2016-02-19 07:37:57'),(206,'Antarctica/Palmer','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(207,'Antarctica/Rothera','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(208,'Antarctica/Syowa','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(209,'Antarctica/Troll','1','UTC/GMT +00:00','2016-02-18 06:37:57','2016-02-18 06:37:57'),(210,'Antarctica/Vostok','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(211,'Arctic/Longyearbyen','1','UTC/GMT +01:00','2016-02-18 07:37:57','2016-02-18 07:37:57'),(212,'Asia/Aden','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(213,'Asia/Almaty','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(214,'Asia/Amman','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(215,'Asia/Anadyr','1','UTC/GMT +12:00','2016-02-19 06:37:57','2016-02-19 06:37:57'),(216,'Asia/Aqtau','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(217,'Asia/Aqtobe','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(218,'Asia/Ashgabat','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(219,'Asia/Baghdad','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(220,'Asia/Bahrain','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(221,'Asia/Baku','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(222,'Asia/Bangkok','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(223,'Asia/Beirut','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(224,'Asia/Bishkek','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(225,'Asia/Brunei','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(226,'Asia/Chita','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(227,'Asia/Choibalsan','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(228,'Asia/Colombo','1','UTC/GMT +05:30','2016-02-18 12:07:57','2016-02-18 12:07:57'),(229,'Asia/Damascus','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(230,'Asia/Dhaka','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(231,'Asia/Dili','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(232,'Asia/Dubai','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(233,'Asia/Dushanbe','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(234,'Asia/Gaza','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(235,'Asia/Hebron','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(236,'Asia/Ho_Chi_Minh','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(237,'Asia/Hong_Kong','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(238,'Asia/Hovd','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(239,'Asia/Irkutsk','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(240,'Asia/Jakarta','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(241,'Asia/Jayapura','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(242,'Asia/Jerusalem','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(243,'Asia/Kabul','1','UTC/GMT +04:30','2016-02-18 11:07:57','2016-02-18 11:07:57'),(244,'Asia/Kamchatka','1','UTC/GMT +12:00','2016-02-19 06:37:57','2016-02-19 06:37:57'),(245,'Asia/Karachi','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(246,'Asia/Kathmandu','1','UTC/GMT +05:45','2016-02-18 12:22:57','2016-02-18 12:22:57'),(247,'Asia/Khandyga','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(248,'Asia/Kolkata','1','UTC/GMT +05:30','2016-02-18 12:07:57','2016-02-18 12:07:57'),(249,'Asia/Krasnoyarsk','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(250,'Asia/Kuala_Lumpur','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(251,'Asia/Kuching','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(252,'Asia/Kuwait','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(253,'Asia/Macau','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(254,'Asia/Magadan','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(255,'Asia/Makassar','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(256,'Asia/Manila','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(257,'Asia/Muscat','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(258,'Asia/Nicosia','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(259,'Asia/Novokuznetsk','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(260,'Asia/Novosibirsk','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(261,'Asia/Omsk','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(262,'Asia/Oral','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(263,'Asia/Phnom_Penh','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(264,'Asia/Pontianak','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(265,'Asia/Pyongyang','1','UTC/GMT +08:30','2016-02-18 15:07:57','2016-02-18 15:07:57'),(266,'Asia/Qatar','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(267,'Asia/Qyzylorda','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(268,'Asia/Rangoon','1','UTC/GMT +06:30','2016-02-18 13:07:57','2016-02-18 13:07:57'),(269,'Asia/Riyadh','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(270,'Asia/Sakhalin','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(271,'Asia/Samarkand','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(272,'Asia/Seoul','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(273,'Asia/Shanghai','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(274,'Asia/Singapore','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(275,'Asia/Srednekolymsk','1','UTC/GMT +11:00','2016-02-19 17:37:57','2016-02-19 17:37:57'),(276,'Asia/Taipei','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(277,'Asia/Tashkent','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(278,'Asia/Tbilisi','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(279,'Asia/Tehran','1','UTC/GMT +03:30','2016-02-18 10:07:57','2016-02-18 10:07:57'),(280,'Asia/Thimphu','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(281,'Asia/Tokyo','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(282,'Asia/Ulaanbaatar','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(283,'Asia/Urumqi','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(284,'Asia/Ust-Nera','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(285,'Asia/Vientiane','1','UTC/GMT +07:00','2016-02-18 13:37:58','2016-02-18 13:37:58'),(286,'Asia/Vladivostok','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(287,'Asia/Yakutsk','1','UTC/GMT +09:00','2016-02-18 15:37:58','2016-02-18 15:37:58'),(288,'Asia/Yekaterinburg','1','UTC/GMT +05:00','2016-02-18 11:37:58','2016-02-18 11:37:58'),(289,'Asia/Yerevan','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(290,'Atlantic/Azores','1','UTC/GMT -01:00','2016-02-18 17:37:58','2016-02-18 17:37:58'),(291,'Atlantic/Bermuda','1','UTC/GMT -04:00','2016-02-18 14:37:58','2016-02-18 14:37:58'),(292,'Atlantic/Canary','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(293,'Atlantic/Cape_Verde','1','UTC/GMT -01:00','2016-02-18 17:37:58','2016-02-18 17:37:58'),(294,'Atlantic/Faroe','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(295,'Atlantic/Madeira','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(296,'Atlantic/Reykjavik','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(297,'Atlantic/South_Georgia','1','UTC/GMT -02:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(298,'Atlantic/St_Helena','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(299,'Atlantic/Stanley','1','UTC/GMT -03:00','2016-02-18 15:37:58','2016-02-18 15:37:58'),(300,'Australia/Adelaide','1','UTC/GMT +10:30','2016-02-19 17:07:58','2016-02-19 17:07:58'),(301,'Australia/Brisbane','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(302,'Australia/Broken_Hill','1','UTC/GMT +10:30','2016-02-19 17:07:58','2016-02-19 17:07:58'),(303,'Australia/Currie','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(304,'Australia/Darwin','1','UTC/GMT +09:30','2016-02-18 16:07:58','2016-02-18 16:07:58'),(305,'Australia/Eucla','1','UTC/GMT +08:45','2016-02-18 15:22:58','2016-02-18 15:22:58'),(306,'Australia/Hobart','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(307,'Australia/Lindeman','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(308,'Australia/Lord_Howe','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(309,'Australia/Melbourne','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(310,'Australia/Perth','1','UTC/GMT +08:00','2016-02-18 14:37:58','2016-02-18 14:37:58'),(311,'Australia/Sydney','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(312,'Europe/Amsterdam','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(313,'Europe/Andorra','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(314,'Europe/Athens','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(315,'Europe/Belgrade','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(316,'Europe/Berlin','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(317,'Europe/Bratislava','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(318,'Europe/Brussels','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(319,'Europe/Bucharest','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(320,'Europe/Budapest','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(321,'Europe/Busingen','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(322,'Europe/Chisinau','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(323,'Europe/Copenhagen','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(324,'Europe/Dublin','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(325,'Europe/Gibraltar','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(326,'Europe/Guernsey','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(327,'Europe/Helsinki','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(328,'Europe/Isle_of_Man','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(329,'Europe/Istanbul','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(330,'Europe/Jersey','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(331,'Europe/Kaliningrad','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(332,'Europe/Kiev','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(333,'Europe/Lisbon','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(334,'Europe/Ljubljana','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(335,'Europe/London','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(336,'Europe/Luxembourg','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(337,'Europe/Madrid','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(338,'Europe/Malta','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(339,'Europe/Mariehamn','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(340,'Europe/Minsk','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(341,'Europe/Monaco','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(342,'Europe/Moscow','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(343,'Europe/Oslo','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(344,'Europe/Paris','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(345,'Europe/Podgorica','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(346,'Europe/Prague','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(347,'Europe/Riga','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(348,'Europe/Rome','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(349,'Europe/Samara','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(350,'Europe/San_Marino','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(351,'Europe/Sarajevo','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(352,'Europe/Simferopol','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(353,'Europe/Skopje','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(354,'Europe/Sofia','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(355,'Europe/Stockholm','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(356,'Europe/Tallinn','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(357,'Europe/Tirane','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(358,'Europe/Uzhgorod','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(359,'Europe/Vaduz','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(360,'Europe/Vatican','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(361,'Europe/Vienna','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(362,'Europe/Vilnius','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(363,'Europe/Volgograd','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(364,'Europe/Warsaw','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(365,'Europe/Zagreb','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(366,'Europe/Zaporozhye','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(367,'Europe/Zurich','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(368,'Indian/Antananarivo','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(369,'Indian/Chagos','1','UTC/GMT +06:00','2016-02-18 12:37:58','2016-02-18 12:37:58'),(370,'Indian/Christmas','1','UTC/GMT +07:00','2016-02-18 13:37:58','2016-02-18 13:37:58'),(371,'Indian/Cocos','1','UTC/GMT +06:30','2016-02-18 13:07:58','2016-02-18 13:07:58'),(372,'Indian/Comoro','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(373,'Indian/Kerguelen','1','UTC/GMT +05:00','2016-02-18 11:37:58','2016-02-18 11:37:58'),(374,'Indian/Mahe','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(375,'Indian/Maldives','1','UTC/GMT +05:00','2016-02-18 11:37:58','2016-02-18 11:37:58'),(376,'Indian/Mauritius','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(377,'Indian/Mayotte','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(378,'Indian/Reunion','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(379,'Pacific/Apia','1','UTC/GMT +14:00','2016-02-19 08:37:58','2016-02-19 08:37:58'),(380,'Pacific/Auckland','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(381,'Pacific/Bougainville','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(382,'Pacific/Chatham','1','UTC/GMT +13:45','2016-02-19 08:22:58','2016-02-19 08:22:58'),(383,'Pacific/Chuuk','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(384,'Pacific/Easter','1','UTC/GMT -05:00','2016-02-18 13:37:58','2016-02-18 13:37:58'),(385,'Pacific/Efate','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(386,'Pacific/Enderbury','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(387,'Pacific/Fakaofo','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(388,'Pacific/Fiji','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(389,'Pacific/Funafuti','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(390,'Pacific/Galapagos','1','UTC/GMT -06:00','2016-02-18 12:37:58','2016-02-18 12:37:58'),(391,'Pacific/Gambier','1','UTC/GMT -09:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(392,'Pacific/Guadalcanal','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(393,'Pacific/Guam','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(394,'Pacific/Honolulu','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(395,'Pacific/Johnston','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(396,'Pacific/Kiritimati','1','UTC/GMT +14:00','2016-02-19 08:37:58','2016-02-19 08:37:58'),(397,'Pacific/Kosrae','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(398,'Pacific/Kwajalein','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(399,'Pacific/Majuro','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(400,'Pacific/Marquesas','1','UTC/GMT -09:30','2016-02-18 09:07:58','2016-02-18 09:07:58'),(401,'Pacific/Midway','1','UTC/GMT -11:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(402,'Pacific/Nauru','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(403,'Pacific/Niue','1','UTC/GMT -11:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(404,'Pacific/Norfolk','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(405,'Pacific/Noumea','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(406,'Pacific/Pago_Pago','1','UTC/GMT -11:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(407,'Pacific/Palau','1','UTC/GMT +09:00','2016-02-18 15:37:58','2016-02-18 15:37:58'),(408,'Pacific/Pitcairn','1','UTC/GMT -08:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(409,'Pacific/Pohnpei','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(410,'Pacific/Port_Moresby','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(411,'Pacific/Rarotonga','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(412,'Pacific/Saipan','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(413,'Pacific/Tahiti','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(414,'Pacific/Tarawa','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(415,'Pacific/Tongatapu','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(416,'Pacific/Wake','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(417,'Pacific/Wallis','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(418,'UTC','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58');
/*!40000 ALTER TABLE `mst_timezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_uom`
--

DROP TABLE IF EXISTS `mst_uom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_uom` (
  `uomid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uomid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_uom`
--

LOCK TABLES `mst_uom` WRITE;
/*!40000 ALTER TABLE `mst_uom` DISABLE KEYS */;
INSERT INTO `mst_uom` VALUES (1,'per unit',1,NULL,NULL),(2,'per case',1,NULL,NULL),(3,'per pallet',1,NULL,NULL),(4,'per box',1,NULL,NULL),(5,'per single unit',1,NULL,NULL);
/*!40000 ALTER TABLE `mst_uom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_useraction`
--

DROP TABLE IF EXISTS `mst_useraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_useraction` (
  `actionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_useraction`
--

LOCK TABLES `mst_useraction` WRITE;
/*!40000 ALTER TABLE `mst_useraction` DISABLE KEYS */;
INSERT INTO `mst_useraction` VALUES (1,'FMTB-5098','122.168.204.142','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 10:04:10','Added User','2016-02-18 15:04:10','2016-02-18 15:04:10'),(2,'TONO-1588','122.168.204.142','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 10:04:44','Added User','2016-02-18 15:04:44','2016-02-18 15:04:44'),(3,'TQBU-0602','122.168.204.142','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 10:05:17','Added User','2016-02-18 15:05:17','2016-02-18 15:05:17'),(4,'NTVZ-5516','122.168.204.142','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 10:05:50','Added User','2016-02-18 15:05:50','2016-02-18 15:05:50'),(5,'FFRR-2029','122.168.204.142','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 10:06:28','Added User','2016-02-18 15:06:28','2016-02-18 15:06:28'),(6,'UUZZ-9984','113.193.106.219','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 16:53:26','Logged In','2016-02-18 21:53:26','2016-02-18 21:53:26'),(7,'UUZZ-9984','123.236.192.153','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','2016-02-18 17:11:59','Logged In','2016-02-18 22:11:59','2016-02-18 22:11:59'),(8,'UUZZ-9984','113.193.106.219','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 17:12:19','Logged In','2016-02-18 22:12:19','2016-02-18 22:12:19'),(9,'FFRR-2029','113.193.106.219','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 17:22:51','Logged In','2016-02-18 22:22:51','2016-02-18 22:22:51'),(10,'UUZZ-9984','113.193.106.219','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','2016-02-18 17:23:26','Logged In','2016-02-18 22:23:26','2016-02-18 22:23:26'),(11,'IBOX-8877','123.236.192.153','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','2016-02-18 12:36:28','Added User','2016-02-18 17:36:28','2016-02-18 17:36:28'),(12,'FFRR-2029','123.236.192.153','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','2016-02-18 17:39:33','Logged In','2016-02-18 22:39:33','2016-02-18 22:39:33'),(13,'UUZZ-9984','123.236.192.153','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','2016-02-18 18:14:56','Logged In','2016-02-18 23:14:56','2016-02-18 23:14:56'),(14,'UUZZ-9984','66.49.252.55','ONd4P1yA8P4seqPA9mMjQ8FQtJgHyrfTV24sFDU4','2016-02-18','2016-02-18 18:15:50','Logged In','2016-02-18 23:15:50','2016-02-18 23:15:50'),(15,'UUZZ-9984','123.236.192.153','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','2016-02-18 18:27:37','Logged In','2016-02-18 23:27:37','2016-02-18 23:27:37'),(16,'UUZZ-9984','123.136.199.120','7ooWdGhhTtI9oXb2w6T04hvifSq8Nz4aBRt81ELk','2016-02-19','2016-02-19 08:02:52','Logged In','2016-02-19 13:02:52','2016-02-19 13:02:52'),(17,'UUZZ-9984','66.49.252.55','Ptrc5OCm21936BSXNw3dXa1phyBBr0JftMoga0fz','2016-02-19','2016-02-19 16:43:23','Logged In','2016-02-19 21:43:23','2016-02-19 21:43:23'),(18,'UUZZ-9984','123.236.192.153','baImqtW10R0DPj378azSX0xIXjLmHFUFRDYux8e6','2016-02-19','2016-02-19 17:05:18','Logged In','2016-02-19 22:05:18','2016-02-19 22:05:18'),(19,'UUZZ-9984','66.49.241.175','PtS9IGs9LxRxnRAYqKucwxfN0wX523J5dPLxbZni','2016-02-19','2016-02-19 17:23:00','Logged In','2016-02-19 22:23:00','2016-02-19 22:23:00'),(20,'UUZZ-9984','66.49.241.175','Z0LuZDOYvxiEF6kfZh9FzFPaheUMpyYJ0CqolCGs','2016-02-22','2016-02-22 13:41:26','Logged In','2016-02-22 18:41:26','2016-02-22 18:41:26'),(21,'UUZZ-9984','171.49.153.186','ehO6rRMr3Qr1XYs9VujC9NQIKhHqBZAv4ukF53Wx','2016-02-22','2016-02-22 14:22:34','Logged In','2016-02-22 19:22:34','2016-02-22 19:22:34'),(22,'UUZZ-9984','105.96.211.149','Fi308rR75TjBx68lhdr8cRvPS7ZLUzKBKa2XOyf2','2016-02-22','2016-02-22 17:27:15','Logged In','2016-02-22 22:27:15','2016-02-22 22:27:15'),(23,'UUZZ-9984','123.236.192.153','cMTOUoq4FmnkQoIcfZ4IIrk4YtyFAi6nCOE6x2Vg','2016-02-22','2016-02-22 17:28:32','Logged In','2016-02-22 22:28:32','2016-02-22 22:28:32'),(24,'UUZZ-9984','171.49.153.186','BcgmfAHgPCwUQEBD2zsYWOcx01yaJdFinAHTNo5p','2016-02-23','2016-02-23 07:07:39','Logged In','2016-02-23 12:07:39','2016-02-23 12:07:39'),(25,'UUZZ-9984','113.193.105.162','I3DkhD03PwRxGl7xlgIxsm87SC4nVKE07xLzFeiF','2016-02-23','2016-02-23 15:36:41','Logged In','2016-02-23 20:36:41','2016-02-23 20:36:41'),(26,'UUZZ-9984','123.236.192.153','lwKc8c9GPLQR6tZN8u2uyj6y0Ql6MZvRRdsCnORR','2016-02-24','2016-02-24 09:32:51','Logged In','2016-02-24 14:32:51','2016-02-24 14:32:51'),(27,'UUZZ-9984','66.49.252.55','0ckao5yln32E0xePCytKJuUZlKjveJjXv4lsicQa','2016-02-24','2016-02-24 15:36:40','Logged In','2016-02-24 20:36:40','2016-02-24 20:36:40'),(28,'XPAP-5806','66.49.252.55','0ckao5yln32E0xePCytKJuUZlKjveJjXv4lsicQa','2016-02-24','2016-02-24 10:38:12','Added User','2016-02-24 15:38:12','2016-02-24 15:38:12'),(29,'UUZZ-9984','171.49.145.203','oe16HobfJ7jSXUIDQimlcWGJUptQ535pQ0McIAIF','2016-02-24','2016-02-24 15:49:00','Logged In','2016-02-24 20:49:00','2016-02-24 20:49:00'),(30,'UUZZ-9984','123.236.192.153','hCTIQ35pOugTrgfwyiTfQ3wz8IkiAfzDhcGb50Bn','2016-02-24','2016-02-24 16:28:32','Logged In','2016-02-24 21:28:32','2016-02-24 21:28:32'),(31,'UUZZ-9984','1.22.86.243','bUngtskTpKIQWown7yoTo540ZWM06LAXtduVn1Lg','2016-02-26','2016-02-26 09:50:36','Logged In','2016-02-26 14:50:36','2016-02-26 14:50:36'),(32,'UUZZ-9984','123.236.192.153','9ZouJrtE0ecybPLhmkKDiVFxgrfQ3xNujCX2ZvWT','2016-02-26','2016-02-26 13:26:04','Logged In','2016-02-26 18:26:04','2016-02-26 18:26:04'),(33,'UUZZ-9984','123.236.192.153','9ZouJrtE0ecybPLhmkKDiVFxgrfQ3xNujCX2ZvWT','2016-02-26','2016-02-26 13:29:02','Added Product Code','2016-02-26 18:29:02','2016-02-26 18:29:02'),(34,'UUZZ-9984','66.49.252.55','gn5Pz8kuytH91LVpTIsbryI4sippWLNhphdD9g98','2016-02-29','2016-02-29 09:38:00','Logged In','2016-02-29 14:38:00','2016-02-29 14:38:00'),(35,'UUZZ-9984','123.236.192.153','Hj46knAb3i9I5Gq6cz9KVOvkkkzkvNB5Wn2NnxpJ','2016-02-29','2016-02-29 11:56:00','Logged In','2016-02-29 16:56:00','2016-02-29 16:56:00'),(36,'UUZZ-9984','27.97.43.132','ENzus7ISbTtyRtiDzO09bTyFv1iNGWqJdnpuKeix','2016-02-29','2016-02-29 12:14:58','Logged In','2016-02-29 17:14:58','2016-02-29 17:14:58'),(37,'UUZZ-9984','27.97.43.132','ENzus7ISbTtyRtiDzO09bTyFv1iNGWqJdnpuKeix','2016-02-29','2016-02-29 12:17:49','Deleted Product','2016-02-29 17:17:49','2016-02-29 17:17:49'),(38,'UUZZ-9984','27.97.235.173','V5kvFzlZn9aLFD8hKtRgmEaq4q30NoGynNDnheo2','2016-02-29','2016-02-29 13:24:06','Logged In','2016-02-29 18:24:06','2016-02-29 18:24:06'),(39,'UUZZ-9984','27.97.235.173','V5kvFzlZn9aLFD8hKtRgmEaq4q30NoGynNDnheo2','2016-02-29','2016-02-29 13:25:09','Product Updated','2016-02-29 18:25:09','2016-02-29 18:25:09'),(40,'UUZZ-9984','106.79.232.208','zEBOh9z1D0vd4YebkrSXLG1THO9tbNt2TvYIS8aD','2016-02-29','2016-02-29 22:34:50','Logged In','2016-03-01 03:34:50','2016-03-01 03:34:50'),(41,'UUZZ-9984','106.79.232.208','zEBOh9z1D0vd4YebkrSXLG1THO9tbNt2TvYIS8aD','2016-02-29','2016-02-29 22:36:01','Product Updated','2016-03-01 03:36:01','2016-03-01 03:36:01'),(42,'UUZZ-9984','123.236.192.153','xzk7ycwmnLgznJ3qlF5A8drhG0A7SiWcXQsudLS8','2016-03-03','2016-03-03 05:17:09','Logged In','2016-03-03 10:17:09','2016-03-03 10:17:09'),(43,'UUZZ-9984','171.61.18.122','gTi1aRukxSscdHP1X2mcw2x5GhrtrurlnXijxo1i','2016-03-09','2016-03-09 11:39:39','Logged In','2016-03-09 16:39:39','2016-03-09 16:39:39'),(44,'UUZZ-9984','67.55.4.151','mPfN76rwjX2rhMp7CH4BmHBexDCUg0M1U8DOQ59a','2016-03-09','2016-03-09 12:05:39','Logged In','2016-03-09 17:05:39','2016-03-09 17:05:39'),(45,'UUZZ-9984','172.98.67.121','WfVQ5959yUxyaZpX3DxgGBGnIUucBRtJyo57sSCh','2016-03-09','2016-03-09 12:12:30','Logged In','2016-03-09 17:12:30','2016-03-09 17:12:30'),(46,'UUZZ-9984','172.98.67.121','WfVQ5959yUxyaZpX3DxgGBGnIUucBRtJyo57sSCh','2016-03-09','2016-03-09 12:18:51','Added Industry','2016-03-09 17:18:51','2016-03-09 17:18:51'),(47,'UUZZ-9984','172.98.67.121','WfVQ5959yUxyaZpX3DxgGBGnIUucBRtJyo57sSCh','2016-03-09','2016-03-09 12:19:35','Added Category','2016-03-09 17:19:35','2016-03-09 17:19:35'),(48,'UUZZ-9984','172.98.67.121','WfVQ5959yUxyaZpX3DxgGBGnIUucBRtJyo57sSCh','2016-03-09','2016-03-09 12:19:45','Added Industry','2016-03-09 17:19:45','2016-03-09 17:19:45'),(49,'UUZZ-9984','172.98.67.121','WfVQ5959yUxyaZpX3DxgGBGnIUucBRtJyo57sSCh','2016-03-09','2016-03-09 12:21:46','Added Brand','2016-03-09 17:21:46','2016-03-09 17:21:46'),(50,'UUZZ-9984','67.55.4.151','01OqrZDCgIPKomejbQEDkOVSBHQGRqwVmfBNCD0L','2016-03-10','2016-03-10 09:46:58','Logged In','2016-03-10 14:46:58','2016-03-10 14:46:58'),(51,'UUZZ-9984','67.55.4.151','01OqrZDCgIPKomejbQEDkOVSBHQGRqwVmfBNCD0L','2016-03-10','2016-03-10 09:47:46','Added Category','2016-03-10 14:47:46','2016-03-10 14:47:46'),(52,'UUZZ-9984','67.55.4.151','FCoRtsZFp2CM6bJwTZW10K0QDEgDcPFzc0yCFOOr','2016-03-10','2016-03-10 09:53:38','Logged In','2016-03-10 14:53:38','2016-03-10 14:53:38'),(53,'UUZZ-9984','67.55.4.151','FCoRtsZFp2CM6bJwTZW10K0QDEgDcPFzc0yCFOOr','2016-03-10','2016-03-10 09:54:04','Logged In','2016-03-10 14:54:04','2016-03-10 14:54:04'),(54,'UUZZ-9984','122.175.167.251','IX2GUBL2aU7AMR7uxs9x6SAa7jzFm9VVVOFwWhDz','2016-03-11','2016-03-11 02:44:19','Logged In','2016-03-11 07:44:19','2016-03-11 07:44:19'),(55,'UUZZ-9984','122.175.167.251','iBeJWLUEElldB8ibuOFHLa3kPjbMs9v8z6uD2Vzx','2016-03-11','2016-03-11 03:16:51','Logged In','2016-03-11 08:16:51','2016-03-11 08:16:51'),(56,'UUZZ-9984','67.55.4.151','FLQAzQS5zVLmI1b2MOqc4DvdBb3FVO2nYsFsFb1R','2016-03-11','2016-03-11 07:22:23','Logged In','2016-03-11 12:22:23','2016-03-11 12:22:23'),(57,'UUZZ-9984','67.55.4.151','FLQAzQS5zVLmI1b2MOqc4DvdBb3FVO2nYsFsFb1R','2016-03-11','2016-03-11 07:23:00','Added Sub Category','2016-03-11 12:23:00','2016-03-11 12:23:00'),(58,'UUZZ-9984','67.55.4.151','FLQAzQS5zVLmI1b2MOqc4DvdBb3FVO2nYsFsFb1R','2016-03-11','2016-03-11 07:23:32','Added Sub Category','2016-03-11 12:23:32','2016-03-11 12:23:32'),(59,'UUZZ-9984','67.55.4.151','FLQAzQS5zVLmI1b2MOqc4DvdBb3FVO2nYsFsFb1R','2016-03-11','2016-03-11 07:24:24','Added Brand','2016-03-11 12:24:24','2016-03-11 12:24:24'),(60,'UUZZ-9984','67.55.4.151','FLQAzQS5zVLmI1b2MOqc4DvdBb3FVO2nYsFsFb1R','2016-03-11','2016-03-11 07:24:43','Added Brand','2016-03-11 12:24:43','2016-03-11 12:24:43'),(61,'UUZZ-9984','122.175.167.251','iBeJWLUEElldB8ibuOFHLa3kPjbMs9v8z6uD2Vzx','2016-03-11','2016-03-11 08:23:33','Added Product','2016-03-11 13:23:33','2016-03-11 13:23:33'),(62,'UUZZ-9984','122.175.167.251','iBeJWLUEElldB8ibuOFHLa3kPjbMs9v8z6uD2Vzx','2016-03-11','2016-03-11 08:27:03','Deleted Product','2016-03-11 13:27:03','2016-03-11 13:27:03'),(63,'UUZZ-9984','122.175.167.251','iBeJWLUEElldB8ibuOFHLa3kPjbMs9v8z6uD2Vzx','2016-03-11','2016-03-11 08:27:56','Added Product','2016-03-11 13:27:56','2016-03-11 13:27:56'),(64,'UUZZ-9984','122.175.167.251','iBeJWLUEElldB8ibuOFHLa3kPjbMs9v8z6uD2Vzx','2016-03-11','2016-03-11 08:34:47','Added Product','2016-03-11 13:34:47','2016-03-11 13:34:47'),(65,'UUZZ-9984','122.175.204.66','P4F9r9P4fefqow7IEEn2Ib9h3g4OsxnSLXPRg4th','2016-03-14','2016-03-14 15:14:59','Logged In','2016-03-14 19:14:59','2016-03-14 19:14:59'),(66,'UUZZ-9984','99.224.27.68','dbSIS7h5Y5e9vD94VprEES42p2mWW2m0qWogwxmm','2016-03-15','2016-03-15 15:53:14','Logged In','2016-03-15 19:53:14','2016-03-15 19:53:14'),(67,'UUZZ-9984','99.224.27.68','dbSIS7h5Y5e9vD94VprEES42p2mWW2m0qWogwxmm','2016-03-15','2016-03-15 15:56:40','Added Sub Category','2016-03-15 19:56:40','2016-03-15 19:56:40'),(68,'UUZZ-9984','99.224.27.68','dbSIS7h5Y5e9vD94VprEES42p2mWW2m0qWogwxmm','2016-03-15','2016-03-15 15:58:57','Added Product','2016-03-15 19:58:57','2016-03-15 19:58:57'),(69,'UUZZ-9984','99.224.27.68','dbSIS7h5Y5e9vD94VprEES42p2mWW2m0qWogwxmm','2016-03-15','2016-03-15 16:10:32','Deleted Product','2016-03-15 20:10:32','2016-03-15 20:10:32'),(70,'UUZZ-9984','122.175.204.66','HiPugxR60LLmb0iKG1mkXDWJM5CUcZnmqhwOW4Av','2016-03-16','2016-03-16 13:57:12','Logged In','2016-03-16 17:57:12','2016-03-16 17:57:12'),(71,'UUZZ-9984','171.61.39.75','amRcXvxIxJdx6SOp7E1dPWtS0aaVpVVJP7j2omZs','2016-03-18','2016-03-18 08:44:24','Logged In','2016-03-18 12:44:24','2016-03-18 12:44:24'),(72,'UUZZ-9984','123.236.192.153','yr1hU4PpGrJdYx0XiMKMqMIJEQAeQyD7OXWzFgJA','2016-03-18','2016-03-18 09:32:11','Logged In','2016-03-18 13:32:11','2016-03-18 13:32:11'),(73,'UUZZ-9984','172.98.67.47','M4D1BEvSQspWtv63U2MmutCcbNG68howXGOWJQa1','2016-03-18','2016-03-18 16:51:54','Logged In','2016-03-18 20:51:54','2016-03-18 20:51:54'),(74,'UUZZ-9984','66.49.221.210','6K3yXF5tDkPSEt4TegDkUlXejltJZHNz8FKbSfF4','2016-03-18','2016-03-18 17:28:46','Logged In','2016-03-18 21:28:46','2016-03-18 21:28:46'),(75,'UUZZ-9984','123.236.192.153','BOfkeyX0JwoWetX2TYP9ohsGDFQKejnoBwNnkJa0','2016-03-18','2016-03-18 17:54:23','Logged In','2016-03-18 21:54:23','2016-03-18 21:54:23'),(76,'UUZZ-9984','66.49.221.210','3jdCdydro6BcCXXMUIx2tGVni25Vkk4EFumz81Dq','2016-03-18','2016-03-18 17:56:10','Logged In','2016-03-18 21:56:10','2016-03-18 21:56:10'),(77,'UUZZ-9984','99.224.139.23','SPvciNn5GA2Z4qLQ0IHJO3HlBbnPV5118CW3pgep','2016-03-20','2016-03-20 09:00:04','Logged In','2016-03-20 13:00:04','2016-03-20 13:00:04'),(78,'UUZZ-9984','123.236.192.153','jNwdGQEUgKWxasnfaKWCk5PsCSnmm43Y0ZgErmHw','2016-03-21','2016-03-21 16:01:10','Logged In','2016-03-21 20:01:10','2016-03-21 20:01:10'),(79,'UUZZ-9984','171.61.52.126','lejlDT8kuRGZ1yB6v4ImFgCRujWgGOfST3grtFTZ','2016-03-21','2016-03-21 16:14:59','Logged In','2016-03-21 20:14:59','2016-03-21 20:14:59'),(80,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:15:37','Logged In','2016-03-21 20:15:37','2016-03-21 20:15:37'),(81,'UUZZ-9984','123.236.192.153','jNwdGQEUgKWxasnfaKWCk5PsCSnmm43Y0ZgErmHw','2016-03-21','2016-03-21 16:24:40','Added Posting','2016-03-21 20:24:40','2016-03-21 20:24:40'),(82,'UUZZ-9984','171.61.52.126','lejlDT8kuRGZ1yB6v4ImFgCRujWgGOfST3grtFTZ','2016-03-21','2016-03-21 16:25:37','Added Posting','2016-03-21 20:25:37','2016-03-21 20:25:37'),(83,'UUZZ-9984','171.61.52.126','lejlDT8kuRGZ1yB6v4ImFgCRujWgGOfST3grtFTZ','2016-03-21','2016-03-21 16:28:20','Added Posting','2016-03-21 20:28:20','2016-03-21 20:28:20'),(84,'UUZZ-9984','123.236.192.153','jNwdGQEUgKWxasnfaKWCk5PsCSnmm43Y0ZgErmHw','2016-03-21','2016-03-21 16:30:31','Added Posting','2016-03-21 20:30:31','2016-03-21 20:30:31'),(85,'UUZZ-9984','123.236.192.153','jNwdGQEUgKWxasnfaKWCk5PsCSnmm43Y0ZgErmHw','2016-03-21','2016-03-21 16:33:27','Added Posting','2016-03-21 20:33:27','2016-03-21 20:33:27'),(86,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:40:56','Added Packaging','2016-03-21 20:40:56','2016-03-21 20:40:56'),(87,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:41:05','Updated Packaging','2016-03-21 20:41:05','2016-03-21 20:41:05'),(88,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:41:08','Deleted Packaging','2016-03-21 20:41:08','2016-03-21 20:41:08'),(89,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:41:29','Added Shipping Conditions','2016-03-21 20:41:29','2016-03-21 20:41:29'),(90,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:41:36','Updated Shipping Conditions','2016-03-21 20:41:36','2016-03-21 20:41:36'),(91,'UUZZ-9984','171.61.52.126','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','2016-03-21 16:41:39','Deleted Shipping Conditions','2016-03-21 20:41:39','2016-03-21 20:41:39'),(92,'UUZZ-9984','66.49.221.210','6HT01l3004o86XikOhUydy2kx6bw2x1Cy4AavJtL','2016-03-21','2016-03-21 17:09:14','Logged In','2016-03-21 21:09:14','2016-03-21 21:09:14'),(93,'UUZZ-9984','66.49.221.210','6HT01l3004o86XikOhUydy2kx6bw2x1Cy4AavJtL','2016-03-21','2016-03-21 17:10:03','Added Posting','2016-03-21 21:10:03','2016-03-21 21:10:03'),(94,'UUZZ-9984','123.236.192.153','cYq897EQK4yPw8rkxJ6OUfII5twN7bnw4xpFVYVr','2016-03-22','2016-03-22 10:04:03','Logged In','2016-03-22 14:04:03','2016-03-22 14:04:03'),(95,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:27:44','Logged In','2016-03-22 18:27:44','2016-03-22 18:27:44'),(96,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:32:03','Added Packaging','2016-03-22 18:32:03','2016-03-22 18:32:03'),(97,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:32:40','Logged In','2016-03-22 18:32:40','2016-03-22 18:32:40'),(98,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:33:43','Added Shipping Conditions','2016-03-22 18:33:43','2016-03-22 18:33:43'),(99,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:33:49','Added Shipping Conditions','2016-03-22 18:33:49','2016-03-22 18:33:49'),(100,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:34:08','Added Shipping Conditions','2016-03-22 18:34:08','2016-03-22 18:34:08'),(101,'UUZZ-9984','209.95.50.97','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','2016-03-22 14:34:21','Added Shipping Conditions','2016-03-22 18:34:21','2016-03-22 18:34:21'),(102,'UUZZ-9984','99.224.139.23','8AnZwi9TRtu74J0azbnltrfohzLmuVj4gZXzcUUn','2016-03-22','2016-03-22 21:46:52','Logged In','2016-03-23 01:46:52','2016-03-23 01:46:52'),(103,'UUZZ-9984','99.224.139.23','8AnZwi9TRtu74J0azbnltrfohzLmuVj4gZXzcUUn','2016-03-22','2016-03-22 21:48:11','Added Posting','2016-03-23 01:48:11','2016-03-23 01:48:11'),(104,'UUZZ-9984','122.175.133.132','IsB7opgC8HFjU29bJdOWoWWmi76ojPRbwC7qIASn','2016-03-23','2016-03-23 06:37:37','Logged In','2016-03-23 10:37:37','2016-03-23 10:37:37'),(105,'UUZZ-9984','113.193.105.85','UOwkYPfRma9TvIAXiR6xsTxF6SrpCHOtRA8CRrsf','2016-03-24','2016-03-24 03:11:27','Logged In','2016-03-24 07:11:27','2016-03-24 07:11:27'),(106,'UUZZ-9984','113.193.105.85','G65ukWoWGi2QnZ6QPA0D149kN02kyf7NMtejKBR9','2016-03-24','2016-03-24 06:04:45','Logged In','2016-03-24 10:04:45','2016-03-24 10:04:45'),(107,'UUZZ-9984','1.22.84.93','aLHCw3LGVvbyNCb8YNAu7rCnmk0yrSsNDtgzcuHe','2016-03-24','2016-03-24 16:23:19','Logged In','2016-03-24 20:23:19','2016-03-24 20:23:19'),(108,'UUZZ-9984','1.22.84.93','aLHCw3LGVvbyNCb8YNAu7rCnmk0yrSsNDtgzcuHe','2016-03-24','2016-03-24 16:26:24','Added Product','2016-03-24 20:26:24','2016-03-24 20:26:24'),(109,'UUZZ-9984','99.224.139.23','4U2tBTFjXRunjaHqebdXMKCs8ZALNf4QNY1QCQLg','2016-03-28','2016-03-28 13:11:40','Logged In','2016-03-28 17:11:40','2016-03-28 17:11:40'),(110,'UUZZ-9984','99.224.139.23','4U2tBTFjXRunjaHqebdXMKCs8ZALNf4QNY1QCQLg','2016-03-28','2016-03-28 13:15:20','Added Posting','2016-03-28 17:15:20','2016-03-28 17:15:20'),(111,'UUZZ-9984','122.170.218.171','hDaEc60edE6ybGPCMHoXCTUlYUlTdsaMv8qusP8A','2016-03-28','2016-03-28 14:07:48','Logged In','2016-03-28 18:07:48','2016-03-28 18:07:48'),(112,'UUZZ-9984','122.170.218.171','hDaEc60edE6ybGPCMHoXCTUlYUlTdsaMv8qusP8A','2016-03-28','2016-03-28 14:08:31','Logged In','2016-03-28 18:08:31','2016-03-28 18:08:31'),(113,'UUZZ-9984','1.22.84.31','ZYdWtoaj1HkctCE7l6PX96oTLKmQFobr4USoRKs2','2016-03-28','2016-03-28 18:42:53','Logged In','2016-03-28 22:42:53','2016-03-28 22:42:53'),(114,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 03:34:24','Logged In','2016-03-29 07:34:24','2016-03-29 07:34:24'),(115,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 04:19:24','Added Customer','2016-03-29 08:19:24','2016-03-29 08:19:24'),(116,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 04:19:43','Added Customer','2016-03-29 08:19:43','2016-03-29 08:19:43'),(117,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 04:22:52','Deleted Customer','2016-03-29 08:22:52','2016-03-29 08:22:52'),(118,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:09:48','Added Category','2016-03-29 09:09:48','2016-03-29 09:09:48'),(119,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:12:43','Added Industry','2016-03-29 09:12:43','2016-03-29 09:12:43'),(120,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:13:14','Updated Industry','2016-03-29 09:13:14','2016-03-29 09:13:14'),(121,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:13:24','Updated Industry','2016-03-29 09:13:24','2016-03-29 09:13:24'),(122,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:13:36','Added Industry','2016-03-29 09:13:36','2016-03-29 09:13:36'),(123,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:14:00','Deleted Industry','2016-03-29 09:14:00','2016-03-29 09:14:00'),(124,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:18:29','Added Location','2016-03-29 09:18:29','2016-03-29 09:18:29'),(125,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:18:42','Updated Location','2016-03-29 09:18:42','2016-03-29 09:18:42'),(126,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:19:02','Updated Location','2016-03-29 09:19:02','2016-03-29 09:19:02'),(127,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:19:23','Updated Location','2016-03-29 09:19:23','2016-03-29 09:19:23'),(128,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:00','Updated Location','2016-03-29 09:20:00','2016-03-29 09:20:00'),(129,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:20','Updated Location','2016-03-29 09:20:20','2016-03-29 09:20:20'),(130,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:29','Updated Location','2016-03-29 09:20:29','2016-03-29 09:20:29'),(131,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:46','Added Location','2016-03-29 09:20:46','2016-03-29 09:20:46'),(132,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:58','Added Location','2016-03-29 09:20:58','2016-03-29 09:20:58'),(133,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:21:07','Updated Location','2016-03-29 09:21:07','2016-03-29 09:21:07'),(134,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:21:35','Updated Location','2016-03-29 09:21:35','2016-03-29 09:21:35'),(135,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:22:34','Added Category','2016-03-29 09:22:34','2016-03-29 09:22:34'),(136,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:23:08','Updated Category','2016-03-29 09:23:08','2016-03-29 09:23:08'),(137,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:23:21','Added Category','2016-03-29 09:23:21','2016-03-29 09:23:21'),(138,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:23:49','Updated Category','2016-03-29 09:23:49','2016-03-29 09:23:49'),(139,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:24:04','Deleted Industry','2016-03-29 09:24:04','2016-03-29 09:24:04'),(140,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:25:07','Updated Location','2016-03-29 09:25:07','2016-03-29 09:25:07'),(141,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:25:23','Updated Location','2016-03-29 09:25:23','2016-03-29 09:25:23'),(142,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:26:12','Added Category','2016-03-29 09:26:12','2016-03-29 09:26:12'),(143,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:30:58','Updated Category','2016-03-29 09:30:58','2016-03-29 09:30:58'),(144,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:31:28','Updated Category','2016-03-29 09:31:28','2016-03-29 09:31:28'),(145,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:33:09','Added Category','2016-03-29 09:33:09','2016-03-29 09:33:09'),(146,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:33:27','Updated Category','2016-03-29 09:33:27','2016-03-29 09:33:27'),(147,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:35:35','Deleted Industry','2016-03-29 09:35:35','2016-03-29 09:35:35'),(148,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:36:44','Deleted Industry','2016-03-29 09:36:44','2016-03-29 09:36:44'),(149,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:38:02','Deleted Industry','2016-03-29 09:38:02','2016-03-29 09:38:02'),(150,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:39:20','Added Category','2016-03-29 09:39:20','2016-03-29 09:39:20'),(151,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:39:32','Added Category','2016-03-29 09:39:32','2016-03-29 09:39:32'),(152,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:39:50','Deleted Location','2016-03-29 09:39:50','2016-03-29 09:39:50'),(153,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:39:58','Deleted Location','2016-03-29 09:39:58','2016-03-29 09:39:58'),(154,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:40:32','Added Question','2016-03-29 09:40:32','2016-03-29 09:40:32'),(155,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:40:49','Added Question','2016-03-29 09:40:49','2016-03-29 09:40:49'),(156,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:41:02','Added Question','2016-03-29 09:41:02','2016-03-29 09:41:02'),(157,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:41:17','Added Question','2016-03-29 09:41:17','2016-03-29 09:41:17'),(158,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:41:40','Added Question','2016-03-29 09:41:40','2016-03-29 09:41:40'),(159,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:41:54','Deleted Question','2016-03-29 09:41:54','2016-03-29 09:41:54'),(160,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:42:12','Updated Question','2016-03-29 09:42:12','2016-03-29 09:42:12'),(161,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:43:16','Deleted Question','2016-03-29 09:43:16','2016-03-29 09:43:16'),(162,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:43:29','Deleted Question','2016-03-29 09:43:29','2016-03-29 09:43:29'),(163,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:44:24','Added Question','2016-03-29 09:44:24','2016-03-29 09:44:24'),(164,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:44:45','Deleted Question','2016-03-29 09:44:45','2016-03-29 09:44:45'),(165,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:45:00','Added Question','2016-03-29 09:45:00','2016-03-29 09:45:00'),(166,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:45:13','Added Question','2016-03-29 09:45:13','2016-03-29 09:45:13'),(167,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:45:26','Deleted Question','2016-03-29 09:45:26','2016-03-29 09:45:26'),(168,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:45:59','Updated Question','2016-03-29 09:45:59','2016-03-29 09:45:59'),(169,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:46:26','Updated Question','2016-03-29 09:46:26','2016-03-29 09:46:26'),(170,'WMZG-5179','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:15:56','Added User','2016-03-29 09:15:56','2016-03-29 09:15:56'),(171,'DYAA-0677','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:18:41','Added User','2016-03-29 09:18:41','2016-03-29 09:18:41'),(172,'QGQR-1187','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:19:16','Added User','2016-03-29 09:19:16','2016-03-29 09:19:16'),(173,'MJNN-8551','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:45','Added User','2016-03-29 09:20:45','2016-03-29 09:20:45'),(174,'IBOX-8877','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:20:53','Deleted User','2016-03-29 09:20:53','2016-03-29 09:20:53'),(175,'QGQR-1187','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:44:40','Deleted User','2016-03-29 09:44:40','2016-03-29 09:44:40'),(176,'XPAP-5806','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:44:49','Deleted User','2016-03-29 09:44:49','2016-03-29 09:44:49'),(177,'WMZG-5179','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:44:57','Deleted User','2016-03-29 09:44:57','2016-03-29 09:44:57'),(178,'TONO-1588','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 05:45:23','Deleted User','2016-03-29 09:45:23','2016-03-29 09:45:23'),(179,'UUZZ-9984','122.170.207.159','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 06:52:41','Logged In','2016-03-29 10:52:41','2016-03-29 10:52:41'),(180,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 06:57:29','Added Category','2016-03-29 10:57:29','2016-03-29 10:57:29'),(181,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 06:57:46','Added Category','2016-03-29 10:57:46','2016-03-29 10:57:46'),(182,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 06:57:58','Updated Category','2016-03-29 10:57:58','2016-03-29 10:57:58'),(183,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 06:59:04','Added Brand','2016-03-29 10:59:04','2016-03-29 10:59:04'),(184,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 07:09:59','Updated User Settings','2016-03-29 11:09:59','2016-03-29 11:09:59'),(185,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 07:17:04','Added Category','2016-03-29 11:17:04','2016-03-29 11:17:04'),(186,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 07:38:04','Added Category','2016-03-29 11:38:04','2016-03-29 11:38:04'),(187,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 07:39:45','Added Question','2016-03-29 11:39:45','2016-03-29 11:39:45'),(188,'UUZZ-9984','122.170.207.159','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 07:40:07','Updated Question','2016-03-29 11:40:07','2016-03-29 11:40:07'),(189,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:11:20','Added Packaging','2016-03-29 12:11:20','2016-03-29 12:11:20'),(190,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:11:29','Updated Packaging','2016-03-29 12:11:29','2016-03-29 12:11:29'),(191,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:11:34','Deleted Packaging','2016-03-29 12:11:34','2016-03-29 12:11:34'),(192,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:11:42','Added Packaging','2016-03-29 12:11:42','2016-03-29 12:11:42'),(193,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:11:50','Added Packaging','2016-03-29 12:11:50','2016-03-29 12:11:50'),(194,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:11:59','Added Packaging','2016-03-29 12:11:59','2016-03-29 12:11:59'),(195,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:12:08','Deleted Packaging','2016-03-29 12:12:08','2016-03-29 12:12:08'),(196,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:12:13','Deleted Packaging','2016-03-29 12:12:13','2016-03-29 12:12:13'),(197,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:12:26','Deleted Packaging','2016-03-29 12:12:26','2016-03-29 12:12:26'),(198,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:13:01','Added Shipping Conditions','2016-03-29 12:13:01','2016-03-29 12:13:01'),(199,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:14:16','Added Industry','2016-03-29 12:14:16','2016-03-29 12:14:16'),(200,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:14:37','Updated Industry','2016-03-29 12:14:37','2016-03-29 12:14:37'),(201,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:15:48','Added Category','2016-03-29 12:15:48','2016-03-29 12:15:48'),(202,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:15:57','Added Industry','2016-03-29 12:15:57','2016-03-29 12:15:57'),(203,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:16:50','Added Industry','2016-03-29 12:16:50','2016-03-29 12:16:50'),(204,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:17:07','Updated Industry','2016-03-29 12:17:07','2016-03-29 12:17:07'),(205,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:17:53','Added Category','2016-03-29 12:17:53','2016-03-29 12:17:53'),(206,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:18:12','Updated Category','2016-03-29 12:18:12','2016-03-29 12:18:12'),(207,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:29:50','Logged In','2016-03-29 12:29:50','2016-03-29 12:29:50'),(208,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:30:12','Added Industry','2016-03-29 12:30:12','2016-03-29 12:30:12'),(209,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:30:22','Added Industry','2016-03-29 12:30:22','2016-03-29 12:30:22'),(210,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:30:32','Added Category','2016-03-29 12:30:32','2016-03-29 12:30:32'),(211,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:30:46','Updated Industry','2016-03-29 12:30:46','2016-03-29 12:30:46'),(212,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:30:57','Deleted Industry','2016-03-29 12:30:57','2016-03-29 12:30:57'),(213,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:31:07','Added Industry','2016-03-29 12:31:07','2016-03-29 12:31:07'),(214,'UUZZ-9984','123.236.192.153','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','2016-03-29 08:31:59','Added Sub Category','2016-03-29 12:31:59','2016-03-29 12:31:59'),(215,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:36:15','Added Category','2016-03-29 12:36:15','2016-03-29 12:36:15'),(216,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:36:36','Updated Category','2016-03-29 12:36:36','2016-03-29 12:36:36'),(217,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:36:41','Deleted Industry','2016-03-29 12:36:41','2016-03-29 12:36:41'),(218,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:37:08','Added Category','2016-03-29 12:37:08','2016-03-29 12:37:08'),(219,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:37:18','Added Category','2016-03-29 12:37:18','2016-03-29 12:37:18'),(220,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:42:57','Added Sub Category','2016-03-29 12:42:57','2016-03-29 12:42:57'),(221,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:43:09','Updated Sub Category','2016-03-29 12:43:09','2016-03-29 12:43:09'),(222,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:49:33','Added Brand','2016-03-29 12:49:33','2016-03-29 12:49:33'),(223,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 08:49:48','Updated Category','2016-03-29 12:49:48','2016-03-29 12:49:48'),(224,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 09:28:35','Added Posting','2016-03-29 13:28:35','2016-03-29 13:28:35'),(225,'UUZZ-9984','123.236.192.153','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','2016-03-29 09:35:14','Added Posting','2016-03-29 13:35:14','2016-03-29 13:35:14'),(226,'UUZZ-9984','122.175.148.29','c0wHXW0ok3zxsSG4ATkOzATVo29bOYN9ZsVrU8dF','2016-03-30','2016-03-30 02:14:41','Logged In','2016-03-30 06:14:41','2016-03-30 06:14:41'),(227,'UUZZ-9984','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 02:14:47','Logged In','2016-03-30 06:14:47','2016-03-30 06:14:47'),(228,'UUZZ-9984','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 03:07:24','Added Brand','2016-03-30 07:07:24','2016-03-30 07:07:24'),(229,'UUZZ-9984','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 03:07:38','Added Brand','2016-03-30 07:07:38','2016-03-30 07:07:38'),(230,'UUZZ-9984','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 03:14:27','Logged In','2016-03-30 07:14:27','2016-03-30 07:14:27'),(231,'UUZZ-9984','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 03:18:25','Updated User Settings','2016-03-30 07:18:25','2016-03-30 07:18:25'),(232,'UUZZ-9984','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 03:18:46','Updated User Settings','2016-03-30 07:18:46','2016-03-30 07:18:46'),(233,'UUZZ-9984','122.170.207.159','S2Ta74bzrEtSMtN44UQIFzgSfw9SuL6ICj4zOeVY','2016-03-30','2016-03-30 04:48:37','Logged In','2016-03-30 08:48:37','2016-03-30 08:48:37'),(234,'QWWS-7447','122.170.207.159','S2Ta74bzrEtSMtN44UQIFzgSfw9SuL6ICj4zOeVY','2016-03-30','2016-03-30 03:53:13','Added User','2016-03-30 07:53:13','2016-03-30 07:53:13'),(235,'QWWS-7447','122.170.207.159','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','2016-03-30 04:53:40','Logged In','2016-03-30 08:53:40','2016-03-30 08:53:40'),(236,'UUZZ-9984','122.170.207.159','c0wHXW0ok3zxsSG4ATkOzATVo29bOYN9ZsVrU8dF','2016-03-30','2016-03-30 04:54:37','Logged In','2016-03-30 08:54:37','2016-03-30 08:54:37'),(237,'QWWS-7447','122.170.207.159','c0wHXW0ok3zxsSG4ATkOzATVo29bOYN9ZsVrU8dF','2016-03-30','2016-03-30 04:54:58','Logged In','2016-03-30 08:54:58','2016-03-30 08:54:58'),(238,'UUZZ-9984','122.170.207.159','S2Ta74bzrEtSMtN44UQIFzgSfw9SuL6ICj4zOeVY','2016-03-30','2016-03-30 05:51:21','Updated Industry','2016-03-30 09:51:21','2016-03-30 09:51:21'),(239,'UUZZ-9984','122.170.207.159','S2Ta74bzrEtSMtN44UQIFzgSfw9SuL6ICj4zOeVY','2016-03-30','2016-03-30 05:55:18','Updated Category','2016-03-30 09:55:18','2016-03-30 09:55:18'),(240,'UUZZ-9984','122.170.207.159','I01h7pc4LQSRlr8Su69TSNydgmlQ0MZCdme5EcUA','2016-03-30','2016-03-30 06:02:31','Logged In','2016-03-30 10:02:31','2016-03-30 10:02:31'),(241,'UUZZ-9984','122.170.207.159','S2Ta74bzrEtSMtN44UQIFzgSfw9SuL6ICj4zOeVY','2016-03-30','2016-03-30 06:04:53','Updated Customer Status','2016-03-30 10:04:53','2016-03-30 10:04:53'),(242,'UUZZ-9984','122.170.207.159','O8wcuTOJkQpchSXGLX0OJFoN3Hdyx1xzzoBsX1ew','2016-03-30','2016-03-30 08:14:53','Logged In','2016-03-30 12:14:53','2016-03-30 12:14:53'),(243,'UUZZ-9984','122.170.207.159','ODsqxkKrjUOEeNU8U4FhsOP1m2RTnIkzY3tsQW4Q','2016-03-30','2016-03-30 08:30:16','Logged In','2016-03-30 12:30:16','2016-03-30 12:30:16'),(244,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 14:21:41','Logged In','2016-03-30 18:21:41','2016-03-30 18:21:41'),(245,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 14:24:20','Added Industry','2016-03-30 18:24:20','2016-03-30 18:24:20'),(246,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 14:26:00','Added Packaging','2016-03-30 18:26:00','2016-03-30 18:26:00'),(247,'UUZZ-9984','171.61.17.139','sXJaHnuZe4w6y6creupfmhlvP1OEsxmOyuzA78Ty','2016-03-30','2016-03-30 14:26:28','Logged In','2016-03-30 18:26:28','2016-03-30 18:26:28'),(248,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 14:26:42','Added Packaging','2016-03-30 18:26:42','2016-03-30 18:26:42'),(249,'UUZZ-9984','171.61.17.139','sXJaHnuZe4w6y6creupfmhlvP1OEsxmOyuzA78Ty','2016-03-30','2016-03-30 14:27:25','Added Packaging','2016-03-30 18:27:25','2016-03-30 18:27:25'),(250,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 14:29:06','Added Posting','2016-03-30 18:29:06','2016-03-30 18:29:06'),(251,'UUZZ-9984','171.61.35.240','4gOhTO6EKaIeXlophrrafg38lGUyr7Dnjeixb4oL','2016-03-30','2016-03-30 14:31:13','Logged In','2016-03-30 18:31:13','2016-03-30 18:31:13'),(252,'UUZZ-9984','171.61.35.240','4gOhTO6EKaIeXlophrrafg38lGUyr7Dnjeixb4oL','2016-03-30','2016-03-30 14:33:31','Deleted Packaging','2016-03-30 18:33:31','2016-03-30 18:33:31'),(253,'UUZZ-9984','171.61.35.240','4gOhTO6EKaIeXlophrrafg38lGUyr7Dnjeixb4oL','2016-03-30','2016-03-30 14:33:37','Deleted Packaging','2016-03-30 18:33:37','2016-03-30 18:33:37'),(254,'UUZZ-9984','171.61.35.240','4gOhTO6EKaIeXlophrrafg38lGUyr7Dnjeixb4oL','2016-03-30','2016-03-30 14:33:41','Deleted Packaging','2016-03-30 18:33:41','2016-03-30 18:33:41'),(255,'UUZZ-9984','171.61.35.240','4gOhTO6EKaIeXlophrrafg38lGUyr7Dnjeixb4oL','2016-03-30','2016-03-30 14:33:59','Added Packaging','2016-03-30 18:33:59','2016-03-30 18:33:59'),(256,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 15:51:48','Added Industry','2016-03-30 19:51:48','2016-03-30 19:51:48'),(257,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 15:54:16','Added Industry','2016-03-30 19:54:16','2016-03-30 19:54:16'),(258,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 15:55:01','Added Industry','2016-03-30 19:55:01','2016-03-30 19:55:01'),(259,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 15:57:41','Logged In','2016-03-30 19:57:41','2016-03-30 19:57:41'),(260,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:04:43','Deleted Industry','2016-03-30 20:04:43','2016-03-30 20:04:43'),(261,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:05:03','Deleted Industry','2016-03-30 20:05:03','2016-03-30 20:05:03'),(262,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:05:19','Added Category','2016-03-30 20:05:19','2016-03-30 20:05:19'),(263,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:05:38','Added Category','2016-03-30 20:05:38','2016-03-30 20:05:38'),(264,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:05:56','Added Category','2016-03-30 20:05:56','2016-03-30 20:05:56'),(265,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:06:17','Added Sub Category','2016-03-30 20:06:17','2016-03-30 20:06:17'),(266,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:06:33','Added Sub Category','2016-03-30 20:06:33','2016-03-30 20:06:33'),(267,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:07:48','Added Brand','2016-03-30 20:07:48','2016-03-30 20:07:48'),(268,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:08:01','Added Brand','2016-03-30 20:08:01','2016-03-30 20:08:01'),(269,'UUZZ-9984','171.61.35.240','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','2016-03-30 16:12:18','Added Product','2016-03-30 20:12:18','2016-03-30 20:12:18'),(270,'UUZZ-9984','66.49.221.210','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','2016-03-30 16:15:47','Added Industry','2016-03-30 20:15:47','2016-03-30 20:15:47'),(271,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:10:12','Logged In','2016-03-31 06:10:12','2016-03-31 06:10:12'),(272,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:17:03','Added Industry','2016-03-31 06:17:03','2016-03-31 06:17:03'),(273,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:17:31','Added Industry','2016-03-31 06:17:31','2016-03-31 06:17:31'),(274,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:17:39','Added Industry','2016-03-31 06:17:39','2016-03-31 06:17:39'),(275,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:17:50','Updated Industry','2016-03-31 06:17:50','2016-03-31 06:17:50'),(276,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:18:00','Deleted Industry','2016-03-31 06:18:00','2016-03-31 06:18:00'),(277,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:20:15','Added Category','2016-03-31 06:20:15','2016-03-31 06:20:15'),(278,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:20:31','Updated Category','2016-03-31 06:20:31','2016-03-31 06:20:31'),(279,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:20:42','Deleted Industry','2016-03-31 06:20:42','2016-03-31 06:20:42'),(280,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:22:16','Added Sub Category','2016-03-31 06:22:16','2016-03-31 06:22:16'),(281,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:22:41','Updated Sub Category','2016-03-31 06:22:41','2016-03-31 06:22:41'),(282,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:25:05','Updated Sub Category','2016-03-31 06:25:05','2016-03-31 06:25:05'),(283,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:25:23','Added Sub Category','2016-03-31 06:25:23','2016-03-31 06:25:23'),(284,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:25:52','Added Sub Category','2016-03-31 06:25:52','2016-03-31 06:25:52'),(285,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:28:52','Added Brand','2016-03-31 06:28:52','2016-03-31 06:28:52'),(286,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:29:20','Updated Category','2016-03-31 06:29:20','2016-03-31 06:29:20'),(287,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:37:32','Added Packaging','2016-03-31 06:37:32','2016-03-31 06:37:32'),(288,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:37:42','Added Packaging','2016-03-31 06:37:42','2016-03-31 06:37:42'),(289,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:37:56','Updated Packaging','2016-03-31 06:37:56','2016-03-31 06:37:56'),(290,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:38:02','Deleted Packaging','2016-03-31 06:38:02','2016-03-31 06:38:02'),(291,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:38:22','Added Shipping Conditions','2016-03-31 06:38:22','2016-03-31 06:38:22'),(292,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:38:31','Updated Shipping Conditions','2016-03-31 06:38:31','2016-03-31 06:38:31'),(293,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:38:39','Deleted Shipping Conditions','2016-03-31 06:38:39','2016-03-31 06:38:39'),(294,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:40:03','Added Location','2016-03-31 06:40:03','2016-03-31 06:40:03'),(295,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:40:14','Updated Location','2016-03-31 06:40:14','2016-03-31 06:40:14'),(296,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:40:21','Deleted Location','2016-03-31 06:40:21','2016-03-31 06:40:21'),(297,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:41:11','Added Question','2016-03-31 06:41:11','2016-03-31 06:41:11'),(298,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:41:37','Added Question','2016-03-31 06:41:37','2016-03-31 06:41:37'),(299,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:44:33','Added Question','2016-03-31 06:44:33','2016-03-31 06:44:33'),(300,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:59:01','Deleted Question','2016-03-31 06:59:01','2016-03-31 06:59:01'),(301,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 03:18:05','Added Sub Category','2016-03-31 07:18:05','2016-03-31 07:18:05'),(302,'MKVL-5233','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:19:30','Added User','2016-03-31 06:19:30','2016-03-31 06:19:30'),(303,'IOCW-8486','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:20:55','Added User','2016-03-31 06:20:55','2016-03-31 06:20:55'),(304,'IOCW-8486','171.61.17.139','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','2016-03-31 03:21:03','Logged In','2016-03-31 07:21:03','2016-03-31 07:21:03'),(305,'IOCW-8486','171.61.17.139','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','2016-03-31 03:23:23','Logged In','2016-03-31 07:23:23','2016-03-31 07:23:23'),(306,'IOCW-8486','171.61.17.139','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','2016-03-31 03:25:29','Logged In','2016-03-31 07:25:29','2016-03-31 07:25:29'),(307,'IOCW-8486','171.61.17.139','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','2016-03-31 03:27:00','Logged In','2016-03-31 07:27:00','2016-03-31 07:27:00'),(308,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 03:35:43','Added Customer','2016-03-31 07:35:43','2016-03-31 07:35:43'),(309,'IOCW-8486','171.61.17.139','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','2016-03-31 03:37:19','Logged In','2016-03-31 07:37:19','2016-03-31 07:37:19'),(310,'GRKK-8667','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 02:50:25','Added User','2016-03-31 06:50:25','2016-03-31 06:50:25'),(311,'GRKK-8667','171.61.17.139','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','2016-03-31 03:50:48','Logged In','2016-03-31 07:50:48','2016-03-31 07:50:48'),(312,'IOCW-8486','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 03:10:52','Deleted User','2016-03-31 07:10:52','2016-03-31 07:10:52'),(313,'UUZZ-9984','171.61.17.139','StM7FmRwHxMd9peqtrhsPXBXQ0WKMWBIhBpet4nC','2016-03-31','2016-03-31 04:45:24','Logged In','2016-03-31 08:45:24','2016-03-31 08:45:24'),(314,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 04:49:27','Added Posting','2016-03-31 08:49:27','2016-03-31 08:49:27'),(315,'UUZZ-9984','171.61.17.139','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','2016-03-31 05:02:25','Added Posting','2016-03-31 09:02:25','2016-03-31 09:02:25'),(316,'LZYY-4633','123.236.192.153','StM7FmRwHxMd9peqtrhsPXBXQ0WKMWBIhBpet4nC','2016-03-31','2016-03-31 05:12:39','Added User','2016-03-31 09:12:39','2016-03-31 09:12:39'),(317,'LZYY-4633','123.236.192.153','StM7FmRwHxMd9peqtrhsPXBXQ0WKMWBIhBpet4nC','2016-03-31','2016-03-31 06:13:17','Logged In','2016-03-31 10:13:17','2016-03-31 10:13:17'),(318,'UUZZ-9984','122.175.151.162','DCTEmOdtdnj2Ro5cQYW9Xo2yybKJAJr9rrn8tSzx','2016-03-31','2016-03-31 07:13:06','Logged In','2016-03-31 11:13:06','2016-03-31 11:13:06');
/*!40000 ALTER TABLE `mst_useraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_userlogin`
--

DROP TABLE IF EXISTS `mst_userlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userlogin` (
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorizedpass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ftlogin` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `timezoneid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secquiz1` int(11) DEFAULT NULL,
  `secquiz2` int(11) DEFAULT NULL,
  `secquiz3` int(11) DEFAULT NULL,
  `secans1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visualnoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `audionoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `industry` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userlogin`
--

LOCK TABLES `mst_userlogin` WRITE;
/*!40000 ALTER TABLE `mst_userlogin` DISABLE KEYS */;
INSERT INTO `mst_userlogin` VALUES ('AM',6,'DYAA-0677','$2y$10$OM9HSkZmZQ3uwETLeka4u.Al31ueuwgtEnN.CpC7Pn4dNudmvSEka','$2y$10$RYr0YWztTLg5mY3N35lOiu03jSukssQc7KtwyRmfYRIzUmNI2iofy','1','N','Atlantic/Azores',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','','','','','2016-03-29 05:18:41',NULL,'2016-03-29 09:18:41','2016-03-29 09:18:41'),('AM',6,'FFRR-2029','$2y$10$31djmJFM8Jl/96Bu43ydbO9BtyDdRr4m3BzQnHKG03d71QAijYVx6','$2y$10$gF/KDna6WOmgUXVShYnF2.G.rX8ld0qnxUnh.KC6lyLNjyftyJMwG','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','1','2','2','2016-02-18 10:06:28','hqRNpGDZF5UPZedZl7qAD0B3xgM8IAKHcAlPdgPvCYFVMIypB51MwEQebJcy','2016-02-18 15:06:28','2016-02-18 22:39:46'),('AM',1,'FMTB-5098','$2y$10$ydooDqsgUeQ7pffZbAEFz.dB1Xv0R84pAthn1V1zQ8jOuKGekyDEC','$2y$10$vDTeMSSt.RBYd0lLNLMTWe028K.pFrCVg9HDc1yQ0IsLrrjHChe.O','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','1','1','2016-02-18 10:04:10','QtgpKCF5nQhE7QIstUbH0T37rfYFDtQtEaHDH4yAaOBccfU5g2cqnMQ9prDF','2016-02-18 15:04:10','2016-02-18 21:50:32'),('AD',4,'GRKK-8667','$2y$10$YDLVQbAJGkvd.oddcRNEKul9eXHiltYXd/t8GqOQvCedB2c.Dk6NC','$2y$10$le2kKdX1nKY4/Po6.SbgrO532h1CE5PDWRJ1eOk4kXCYVU5nMXVy.','1','N','Atlantic/Azores',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','6','5','3','','2016-03-31 02:50:25','b3GmIrJLooj845FIUSpmuqMLqUHVv44sGnVpHPsrBnhcFkYcRu1m8PpDOVlm','2016-03-31 06:50:25','2016-03-31 08:19:37'),('AM',1,'LZYY-4633','$2y$10$yw9k1EU1j74LbCqBHozPAu3d4WKuJmHAtPZJtlX1AxBZDyTQJbHqG','$2y$10$01/RnIlKxDTKbFNT6j2KiufmVlmjod2//3hTAKiePZxolf3EzlBem','1','N','Atlantic/Azores',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','1','','2,3','2016-03-31 05:12:39',NULL,'2016-03-31 09:12:39','2016-03-31 09:12:39'),('AD',4,'MJNN-8551','$2y$10$7rz1Ding23JCaHiv9vlTjOSFQMoZ7VUzgD/ETNUvS/YuILI41f37e','$2y$10$UduTUBslw42v6QDGIand/OZeV1OxlURB4DIJ4auN/6pXzhTeqy822','1','N','Atlantic/Cape_Verde',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4,1','1,2','2','2','2016-03-29 05:20:45',NULL,'2016-03-29 09:20:45','2016-03-29 09:20:45'),('TM',3,'MKVL-5233','$2y$10$BcZ8jqhNVx5VzC8aPoXe8eX0xUL8UzchbWL7nVT/nBQqH64E38Cki','$2y$10$4LCKHalXEGJjyr6NafplA.okr9q4qdP4D9HTYTHW.EdZr/vnzBJKe','1','N','Atlantic/Azores',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','1','','1,2','2016-03-31 02:19:30',NULL,'2016-03-31 06:19:30','2016-03-31 06:19:30'),('AD',4,'NTVZ-5516','$2y$10$9lj6cGN6Y7wjJCAKhersjuwVD4QqiUvUri64gLADevEc8ZQI7jMlO','$2y$10$eJpalnXnPVFUMOuPcl4u2OBEP1s.uCP4mtDqyuDK3/aiuUOUFoeuW','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','1','1','2016-02-18 10:05:50',NULL,'2016-02-18 15:05:50','2016-02-18 15:05:50'),('AM',1,'QWWS-7447','$2y$10$ge9B6.rmgRSadQ7QNdTdXOpw4kjHgYDalRrZhTgrSG8oAXiC/WPd.','$2y$10$W/a2SbxSKIQyNJU9E6mObOhs0BCyznwAZ3t1lFf50bCNDJaNqqW7u','1','N','Atlantic/South_Georgia',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','1','1','2','2016-03-30 03:53:13',NULL,'2016-03-30 07:53:13','2016-03-30 07:53:13'),('TM',3,'TQBU-0602','$2y$10$Lb.J2rfaunAa0m4gMjG5Z.1MyPJ8aSfJa0WRKJCDTsKKscuElW3Se','$2y$10$FNsOhdANnV34GIEOwiVjRemjJCbO2ZHloCcsW4.4Kleoi5y16Hs3C','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','2','1','2016-02-18 10:05:17',NULL,'2016-02-18 15:05:17','2016-02-18 15:05:17'),('AD',5,'UUZZ-9984','$2y$10$RA/XfL5Y7gUTNG73b07Yh.biQwoihnolATlIfO9V1vAVFWOu6ZFNu','$2y$10$RA/XfL5Y7gUTNG73b07Yh.biQwoihnolATlIfO9V1vAVFWOu6ZFNu','1','Y','UTC/GMT +06:30',6,8,3,'father','mother','animal','1','1','1,2,3,4,5','1,2,3,4,5','1,2,3,4,5','1,2,3,4,5','2016-02-17 16:01:00','GY1hTbSXNAkKQdbj36Rns4nQqJig1gqvWR8OwKz6K9lGYcirZyJiflBG1OXy','2016-02-17 21:01:00','2016-03-31 10:13:08');
/*!40000 ALTER TABLE `mst_userlogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_userlogon`
--

DROP TABLE IF EXISTS `mst_userlogon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userlogon` (
  `logonid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `starttime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `finishtime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logondate` date NOT NULL,
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`logonid`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userlogon`
--

LOCK TABLES `mst_userlogon` WRITE;
/*!40000 ALTER TABLE `mst_userlogon` DISABLE KEYS */;
INSERT INTO `mst_userlogon` VALUES (1,'UUZZ-9984','122.168.204.142','2016-02-18 14:35:22pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 19:35:22','2016-02-18 19:35:22'),(2,'UUZZ-9984','122.168.204.142','2016-02-18 15:34:47pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 20:34:47','2016-02-18 20:34:47'),(3,'UUZZ-9984','123.236.192.153','2016-02-18 16:48:30pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 21:48:30','2016-02-18 21:48:30'),(4,'FFRR-2029','113.193.106.219','2016-02-18 16:49:51pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AM','2016-02-18 21:49:51','2016-02-18 21:49:51'),(5,'FMTB-5098','113.193.106.219','2016-02-18 16:50:20pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AM','2016-02-18 21:50:20','2016-02-18 21:50:20'),(6,'UUZZ-9984','113.193.106.219','2016-02-18 16:50:38pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 21:50:38','2016-02-18 21:50:38'),(7,'UUZZ-9984','113.193.106.219','2016-02-18 16:53:26pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 21:53:26','2016-02-18 21:53:26'),(8,'UUZZ-9984','123.236.192.153','2016-02-18 17:11:59pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 22:11:59','2016-02-18 22:11:59'),(9,'UUZZ-9984','113.193.106.219','2016-02-18 17:12:19pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 22:12:19','2016-02-18 22:12:19'),(10,'FFRR-2029','113.193.106.219','2016-02-18 17:22:51pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AM','2016-02-18 22:22:51','2016-02-18 22:22:51'),(11,'UUZZ-9984','113.193.106.219','2016-02-18 17:23:26pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 22:23:26','2016-02-18 22:23:26'),(12,'FFRR-2029','123.236.192.153','2016-02-18 17:39:33pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AM','2016-02-18 22:39:33','2016-02-18 22:39:33'),(13,'UUZZ-9984','123.236.192.153','2016-02-18 18:14:56pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 23:14:56','2016-02-18 23:14:56'),(14,'UUZZ-9984','66.49.252.55','2016-02-18 18:15:50pm','','ONd4P1yA8P4seqPA9mMjQ8FQtJgHyrfTV24sFDU4','2016-02-18','AD','2016-02-18 23:15:50','2016-02-18 23:15:50'),(15,'UUZZ-9984','123.236.192.153','2016-02-18 18:27:37pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 23:27:37','2016-02-18 23:27:37'),(16,'UUZZ-9984','123.136.199.120','2016-02-19 08:02:52am','','7ooWdGhhTtI9oXb2w6T04hvifSq8Nz4aBRt81ELk','2016-02-19','AD','2016-02-19 13:02:52','2016-02-19 13:02:52'),(17,'UUZZ-9984','66.49.252.55','2016-02-19 16:43:23pm','','Ptrc5OCm21936BSXNw3dXa1phyBBr0JftMoga0fz','2016-02-19','AD','2016-02-19 21:43:23','2016-02-19 21:43:23'),(18,'UUZZ-9984','123.236.192.153','2016-02-19 17:05:18pm','','baImqtW10R0DPj378azSX0xIXjLmHFUFRDYux8e6','2016-02-19','AD','2016-02-19 22:05:18','2016-02-19 22:05:18'),(19,'UUZZ-9984','66.49.241.175','2016-02-19 17:23:00pm','','PtS9IGs9LxRxnRAYqKucwxfN0wX523J5dPLxbZni','2016-02-19','AD','2016-02-19 22:23:00','2016-02-19 22:23:00'),(20,'UUZZ-9984','66.49.241.175','2016-02-22 13:41:26pm','','Z0LuZDOYvxiEF6kfZh9FzFPaheUMpyYJ0CqolCGs','2016-02-22','AD','2016-02-22 18:41:26','2016-02-22 18:41:26'),(21,'UUZZ-9984','171.49.153.186','2016-02-22 14:22:34pm','','ehO6rRMr3Qr1XYs9VujC9NQIKhHqBZAv4ukF53Wx','2016-02-22','AD','2016-02-22 19:22:34','2016-02-22 19:22:34'),(22,'UUZZ-9984','105.96.211.149','2016-02-22 17:27:15pm','','Fi308rR75TjBx68lhdr8cRvPS7ZLUzKBKa2XOyf2','2016-02-22','AD','2016-02-22 22:27:15','2016-02-22 22:27:15'),(23,'UUZZ-9984','123.236.192.153','2016-02-22 17:28:32pm','','cMTOUoq4FmnkQoIcfZ4IIrk4YtyFAi6nCOE6x2Vg','2016-02-22','AD','2016-02-22 22:28:32','2016-02-22 22:28:32'),(24,'UUZZ-9984','171.49.153.186','2016-02-23 07:07:39am','','BcgmfAHgPCwUQEBD2zsYWOcx01yaJdFinAHTNo5p','2016-02-23','AD','2016-02-23 12:07:39','2016-02-23 12:07:39'),(25,'UUZZ-9984','113.193.105.162','2016-02-23 15:36:41pm','','I3DkhD03PwRxGl7xlgIxsm87SC4nVKE07xLzFeiF','2016-02-23','AD','2016-02-23 20:36:41','2016-02-23 20:36:41'),(26,'UUZZ-9984','123.236.192.153','2016-02-24 09:32:51am','','lwKc8c9GPLQR6tZN8u2uyj6y0Ql6MZvRRdsCnORR','2016-02-24','AD','2016-02-24 14:32:51','2016-02-24 14:32:51'),(27,'UUZZ-9984','66.49.252.55','2016-02-24 15:36:40pm','','0ckao5yln32E0xePCytKJuUZlKjveJjXv4lsicQa','2016-02-24','AD','2016-02-24 20:36:40','2016-02-24 20:36:40'),(28,'UUZZ-9984','171.49.145.203','2016-02-24 15:49:00pm','','oe16HobfJ7jSXUIDQimlcWGJUptQ535pQ0McIAIF','2016-02-24','AD','2016-02-24 20:49:00','2016-02-24 20:49:00'),(29,'UUZZ-9984','123.236.192.153','2016-02-24 16:28:32pm','','hCTIQ35pOugTrgfwyiTfQ3wz8IkiAfzDhcGb50Bn','2016-02-24','AD','2016-02-24 21:28:32','2016-02-24 21:28:32'),(30,'UUZZ-9984','1.22.86.243','2016-02-26 09:50:36am','','bUngtskTpKIQWown7yoTo540ZWM06LAXtduVn1Lg','2016-02-26','AD','2016-02-26 14:50:36','2016-02-26 14:50:36'),(31,'UUZZ-9984','123.236.192.153','2016-02-26 13:26:04pm','','9ZouJrtE0ecybPLhmkKDiVFxgrfQ3xNujCX2ZvWT','2016-02-26','AD','2016-02-26 18:26:04','2016-02-26 18:26:04'),(32,'UUZZ-9984','66.49.252.55','2016-02-29 09:38:00am','','gn5Pz8kuytH91LVpTIsbryI4sippWLNhphdD9g98','2016-02-29','AD','2016-02-29 14:38:00','2016-02-29 14:38:00'),(33,'UUZZ-9984','123.236.192.153','2016-02-29 11:56:00am','','Hj46knAb3i9I5Gq6cz9KVOvkkkzkvNB5Wn2NnxpJ','2016-02-29','AD','2016-02-29 16:56:00','2016-02-29 16:56:00'),(34,'UUZZ-9984','27.97.43.132','2016-02-29 12:14:58pm','','ENzus7ISbTtyRtiDzO09bTyFv1iNGWqJdnpuKeix','2016-02-29','AD','2016-02-29 17:14:58','2016-02-29 17:14:58'),(35,'UUZZ-9984','27.97.235.173','2016-02-29 13:24:06pm','','V5kvFzlZn9aLFD8hKtRgmEaq4q30NoGynNDnheo2','2016-02-29','AD','2016-02-29 18:24:06','2016-02-29 18:24:06'),(36,'UUZZ-9984','106.79.232.208','2016-02-29 22:34:50pm','','zEBOh9z1D0vd4YebkrSXLG1THO9tbNt2TvYIS8aD','2016-02-29','AD','2016-03-01 03:34:50','2016-03-01 03:34:50'),(37,'UUZZ-9984','123.236.192.153','2016-03-03 05:17:09am','','xzk7ycwmnLgznJ3qlF5A8drhG0A7SiWcXQsudLS8','2016-03-03','AD','2016-03-03 10:17:09','2016-03-03 10:17:09'),(38,'UUZZ-9984','171.61.18.122','2016-03-09 11:39:39am','','gTi1aRukxSscdHP1X2mcw2x5GhrtrurlnXijxo1i','2016-03-09','AD','2016-03-09 16:39:39','2016-03-09 16:39:39'),(39,'UUZZ-9984','67.55.4.151','2016-03-09 12:05:39pm','','mPfN76rwjX2rhMp7CH4BmHBexDCUg0M1U8DOQ59a','2016-03-09','AD','2016-03-09 17:05:39','2016-03-09 17:05:39'),(40,'UUZZ-9984','172.98.67.121','2016-03-09 12:12:30pm','','WfVQ5959yUxyaZpX3DxgGBGnIUucBRtJyo57sSCh','2016-03-09','AD','2016-03-09 17:12:30','2016-03-09 17:12:30'),(41,'UUZZ-9984','67.55.4.151','2016-03-10 09:46:58am','','01OqrZDCgIPKomejbQEDkOVSBHQGRqwVmfBNCD0L','2016-03-10','AD','2016-03-10 14:46:58','2016-03-10 14:46:58'),(42,'UUZZ-9984','67.55.4.151','2016-03-10 09:53:38am','','FCoRtsZFp2CM6bJwTZW10K0QDEgDcPFzc0yCFOOr','2016-03-10','AD','2016-03-10 14:53:38','2016-03-10 14:53:38'),(43,'UUZZ-9984','67.55.4.151','2016-03-10 09:54:04am','','FCoRtsZFp2CM6bJwTZW10K0QDEgDcPFzc0yCFOOr','2016-03-10','AD','2016-03-10 14:54:04','2016-03-10 14:54:04'),(44,'UUZZ-9984','122.175.167.251','2016-03-11 02:44:19am','','IX2GUBL2aU7AMR7uxs9x6SAa7jzFm9VVVOFwWhDz','2016-03-11','AD','2016-03-11 07:44:19','2016-03-11 07:44:19'),(45,'UUZZ-9984','122.175.167.251','2016-03-11 03:16:51am','','iBeJWLUEElldB8ibuOFHLa3kPjbMs9v8z6uD2Vzx','2016-03-11','AD','2016-03-11 08:16:51','2016-03-11 08:16:51'),(46,'UUZZ-9984','67.55.4.151','2016-03-11 07:22:23am','','FLQAzQS5zVLmI1b2MOqc4DvdBb3FVO2nYsFsFb1R','2016-03-11','AD','2016-03-11 12:22:23','2016-03-11 12:22:23'),(47,'UUZZ-9984','122.175.204.66','2016-03-14 15:14:59pm','','P4F9r9P4fefqow7IEEn2Ib9h3g4OsxnSLXPRg4th','2016-03-14','AD','2016-03-14 19:14:59','2016-03-14 19:14:59'),(48,'UUZZ-9984','99.224.27.68','2016-03-15 15:53:14pm','','dbSIS7h5Y5e9vD94VprEES42p2mWW2m0qWogwxmm','2016-03-15','AD','2016-03-15 19:53:14','2016-03-15 19:53:14'),(49,'UUZZ-9984','122.175.204.66','2016-03-16 13:57:12pm','','HiPugxR60LLmb0iKG1mkXDWJM5CUcZnmqhwOW4Av','2016-03-16','AD','2016-03-16 17:57:12','2016-03-16 17:57:12'),(50,'UUZZ-9984','171.61.39.75','2016-03-18 08:44:24am','','amRcXvxIxJdx6SOp7E1dPWtS0aaVpVVJP7j2omZs','2016-03-18','AD','2016-03-18 12:44:24','2016-03-18 12:44:24'),(51,'UUZZ-9984','123.236.192.153','2016-03-18 09:32:11am','','yr1hU4PpGrJdYx0XiMKMqMIJEQAeQyD7OXWzFgJA','2016-03-18','AD','2016-03-18 13:32:11','2016-03-18 13:32:11'),(52,'UUZZ-9984','172.98.67.47','2016-03-18 16:51:54pm','','M4D1BEvSQspWtv63U2MmutCcbNG68howXGOWJQa1','2016-03-18','AD','2016-03-18 20:51:54','2016-03-18 20:51:54'),(53,'UUZZ-9984','66.49.221.210','2016-03-18 17:28:46pm','','6K3yXF5tDkPSEt4TegDkUlXejltJZHNz8FKbSfF4','2016-03-18','AD','2016-03-18 21:28:46','2016-03-18 21:28:46'),(54,'UUZZ-9984','123.236.192.153','2016-03-18 17:54:23pm','','BOfkeyX0JwoWetX2TYP9ohsGDFQKejnoBwNnkJa0','2016-03-18','AD','2016-03-18 21:54:23','2016-03-18 21:54:23'),(55,'UUZZ-9984','66.49.221.210','2016-03-18 17:56:10pm','','3jdCdydro6BcCXXMUIx2tGVni25Vkk4EFumz81Dq','2016-03-18','AD','2016-03-18 21:56:10','2016-03-18 21:56:10'),(56,'UUZZ-9984','99.224.139.23','2016-03-20 09:00:04am','','SPvciNn5GA2Z4qLQ0IHJO3HlBbnPV5118CW3pgep','2016-03-20','AD','2016-03-20 13:00:04','2016-03-20 13:00:04'),(57,'UUZZ-9984','123.236.192.153','2016-03-21 16:01:10pm','','jNwdGQEUgKWxasnfaKWCk5PsCSnmm43Y0ZgErmHw','2016-03-21','AD','2016-03-21 20:01:10','2016-03-21 20:01:10'),(58,'UUZZ-9984','171.61.52.126','2016-03-21 16:14:59pm','','lejlDT8kuRGZ1yB6v4ImFgCRujWgGOfST3grtFTZ','2016-03-21','AD','2016-03-21 20:14:59','2016-03-21 20:14:59'),(59,'UUZZ-9984','171.61.52.126','2016-03-21 16:15:37pm','','HwQvum2eAuMd9axTr8zbYDIC3D8DGtLIkg4P5wHx','2016-03-21','AD','2016-03-21 20:15:37','2016-03-21 20:15:37'),(60,'UUZZ-9984','66.49.221.210','2016-03-21 17:09:14pm','','6HT01l3004o86XikOhUydy2kx6bw2x1Cy4AavJtL','2016-03-21','AD','2016-03-21 21:09:14','2016-03-21 21:09:14'),(61,'UUZZ-9984','123.236.192.153','2016-03-22 10:04:03am','','cYq897EQK4yPw8rkxJ6OUfII5twN7bnw4xpFVYVr','2016-03-22','AD','2016-03-22 14:04:03','2016-03-22 14:04:03'),(62,'UUZZ-9984','209.95.50.97','2016-03-22 14:27:44pm','','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','AD','2016-03-22 18:27:44','2016-03-22 18:27:44'),(63,'UUZZ-9984','209.95.50.97','2016-03-22 14:32:40pm','','JHu42uHPkhErldHu07ZRtZ9YYRNYHoGew144SEQT','2016-03-22','AD','2016-03-22 18:32:40','2016-03-22 18:32:40'),(64,'UUZZ-9984','99.224.139.23','2016-03-22 21:46:52pm','','8AnZwi9TRtu74J0azbnltrfohzLmuVj4gZXzcUUn','2016-03-22','AD','2016-03-23 01:46:52','2016-03-23 01:46:52'),(65,'UUZZ-9984','122.175.133.132','2016-03-23 06:37:37am','','IsB7opgC8HFjU29bJdOWoWWmi76ojPRbwC7qIASn','2016-03-23','AD','2016-03-23 10:37:37','2016-03-23 10:37:37'),(66,'UUZZ-9984','113.193.105.85','2016-03-24 03:11:27am','','UOwkYPfRma9TvIAXiR6xsTxF6SrpCHOtRA8CRrsf','2016-03-24','AD','2016-03-24 07:11:27','2016-03-24 07:11:27'),(67,'UUZZ-9984','113.193.105.85','2016-03-24 06:04:45am','','G65ukWoWGi2QnZ6QPA0D149kN02kyf7NMtejKBR9','2016-03-24','AD','2016-03-24 10:04:45','2016-03-24 10:04:45'),(68,'UUZZ-9984','1.22.84.93','2016-03-24 16:23:19pm','','aLHCw3LGVvbyNCb8YNAu7rCnmk0yrSsNDtgzcuHe','2016-03-24','AD','2016-03-24 20:23:19','2016-03-24 20:23:19'),(69,'UUZZ-9984','99.224.139.23','2016-03-28 13:11:40pm','','4U2tBTFjXRunjaHqebdXMKCs8ZALNf4QNY1QCQLg','2016-03-28','AD','2016-03-28 17:11:40','2016-03-28 17:11:40'),(70,'UUZZ-9984','122.170.218.171','2016-03-28 14:07:48pm','','hDaEc60edE6ybGPCMHoXCTUlYUlTdsaMv8qusP8A','2016-03-28','AD','2016-03-28 18:07:48','2016-03-28 18:07:48'),(71,'UUZZ-9984','122.170.218.171','2016-03-28 14:08:31pm','','hDaEc60edE6ybGPCMHoXCTUlYUlTdsaMv8qusP8A','2016-03-28','AD','2016-03-28 18:08:31','2016-03-28 18:08:31'),(72,'UUZZ-9984','1.22.84.31','2016-03-28 18:42:53pm','','ZYdWtoaj1HkctCE7l6PX96oTLKmQFobr4USoRKs2','2016-03-28','AD','2016-03-28 22:42:53','2016-03-28 22:42:53'),(73,'UUZZ-9984','122.170.207.159','2016-03-29 03:34:24am','','HbE0sHZMucsIcGX0b85O9dakaPsr0VDHR7kKV3vx','2016-03-29','AD','2016-03-29 07:34:24','2016-03-29 07:34:24'),(74,'UUZZ-9984','122.170.207.159','2016-03-29 06:52:41am','','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','AD','2016-03-29 10:52:41','2016-03-29 10:52:41'),(75,'UUZZ-9984','123.236.192.153','2016-03-29 08:29:50am','','8ELc0xp3mNnXYinWhgMqtJt6TPMKS3VKBofQrtKP','2016-03-29','AD','2016-03-29 12:29:50','2016-03-29 12:29:50'),(76,'UUZZ-9984','122.175.148.29','2016-03-30 02:14:41am','','c0wHXW0ok3zxsSG4ATkOzATVo29bOYN9ZsVrU8dF','2016-03-30','AD','2016-03-30 06:14:41','2016-03-30 06:14:41'),(77,'UUZZ-9984','122.170.207.159','2016-03-30 02:14:47am','','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','AD','2016-03-30 06:14:47','2016-03-30 06:14:47'),(78,'UUZZ-9984','122.170.207.159','2016-03-30 03:14:27am','','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','AD','2016-03-30 07:14:27','2016-03-30 07:14:27'),(79,'UUZZ-9984','122.170.207.159','2016-03-30 04:48:37am','','S2Ta74bzrEtSMtN44UQIFzgSfw9SuL6ICj4zOeVY','2016-03-30','AD','2016-03-30 08:48:37','2016-03-30 08:48:37'),(80,'QWWS-7447','122.170.207.159','2016-03-30 04:53:40am','','YHZ6FZU4evYSe4yWh8uFon1XiJ1Nc5bOTMESQPWv','2016-03-30','AM','2016-03-30 08:53:40','2016-03-30 08:53:40'),(81,'UUZZ-9984','122.170.207.159','2016-03-30 04:54:37am','','c0wHXW0ok3zxsSG4ATkOzATVo29bOYN9ZsVrU8dF','2016-03-30','AD','2016-03-30 08:54:37','2016-03-30 08:54:37'),(82,'QWWS-7447','122.170.207.159','2016-03-30 04:54:58am','','c0wHXW0ok3zxsSG4ATkOzATVo29bOYN9ZsVrU8dF','2016-03-30','AM','2016-03-30 08:54:58','2016-03-30 08:54:58'),(83,'UUZZ-9984','122.170.207.159','2016-03-30 06:02:31am','','I01h7pc4LQSRlr8Su69TSNydgmlQ0MZCdme5EcUA','2016-03-30','AD','2016-03-30 10:02:31','2016-03-30 10:02:31'),(84,'UUZZ-9984','122.170.207.159','2016-03-30 08:14:53am','','O8wcuTOJkQpchSXGLX0OJFoN3Hdyx1xzzoBsX1ew','2016-03-30','AD','2016-03-30 12:14:53','2016-03-30 12:14:53'),(85,'UUZZ-9984','122.170.207.159','2016-03-30 08:30:16am','','ODsqxkKrjUOEeNU8U4FhsOP1m2RTnIkzY3tsQW4Q','2016-03-30','AD','2016-03-30 12:30:16','2016-03-30 12:30:16'),(86,'UUZZ-9984','66.49.221.210','2016-03-30 14:21:41pm','','y2KM5tnIvlrSX54CkRsSu8FtOvHgl56dfdNyyzZN','2016-03-30','AD','2016-03-30 18:21:41','2016-03-30 18:21:41'),(87,'UUZZ-9984','171.61.17.139','2016-03-30 14:26:28pm','','sXJaHnuZe4w6y6creupfmhlvP1OEsxmOyuzA78Ty','2016-03-30','AD','2016-03-30 18:26:28','2016-03-30 18:26:28'),(88,'UUZZ-9984','171.61.35.240','2016-03-30 14:31:13pm','','4gOhTO6EKaIeXlophrrafg38lGUyr7Dnjeixb4oL','2016-03-30','AD','2016-03-30 18:31:13','2016-03-30 18:31:13'),(89,'UUZZ-9984','171.61.35.240','2016-03-30 15:57:41pm','','IHMCJO7GFMMuaZPopwbJYm8mCxERPcY4Www6zgtC','2016-03-30','AD','2016-03-30 19:57:41','2016-03-30 19:57:41'),(90,'UUZZ-9984','171.61.17.139','2016-03-31 02:10:12am','','kd8SlbgZqmbBCFu9yayebRSxGor2SUXAhVYQ9VSA','2016-03-31','AD','2016-03-31 06:10:12','2016-03-31 06:10:12'),(91,'IOCW-8486','171.61.17.139','2016-03-31 03:21:03am','','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','TM','2016-03-31 07:21:03','2016-03-31 07:21:03'),(92,'IOCW-8486','171.61.17.139','2016-03-31 03:23:23am','','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','TM','2016-03-31 07:23:23','2016-03-31 07:23:23'),(93,'IOCW-8486','171.61.17.139','2016-03-31 03:25:29am','','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','TM','2016-03-31 07:25:29','2016-03-31 07:25:29'),(94,'IOCW-8486','171.61.17.139','2016-03-31 03:27:00am','','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','TM','2016-03-31 07:27:00','2016-03-31 07:27:00'),(95,'IOCW-8486','171.61.17.139','2016-03-31 03:37:19am','','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','TM','2016-03-31 07:37:19','2016-03-31 07:37:19'),(96,'GRKK-8667','171.61.17.139','2016-03-31 03:50:48am','','MDY8c85ypOw7vwV5zT76RzmTmRJ9wQVSqW7CxZky','2016-03-31','AD','2016-03-31 07:50:48','2016-03-31 07:50:48'),(97,'UUZZ-9984','171.61.17.139','2016-03-31 04:45:24am','','StM7FmRwHxMd9peqtrhsPXBXQ0WKMWBIhBpet4nC','2016-03-31','AD','2016-03-31 08:45:24','2016-03-31 08:45:24'),(98,'LZYY-4633','123.236.192.153','2016-03-31 06:13:17am','','StM7FmRwHxMd9peqtrhsPXBXQ0WKMWBIhBpet4nC','2016-03-31','AM','2016-03-31 10:13:17','2016-03-31 10:13:17'),(99,'UUZZ-9984','122.175.151.162','2016-03-31 07:13:06am','','DCTEmOdtdnj2Ro5cQYW9Xo2yybKJAJr9rrn8tSzx','2016-03-31','AD','2016-03-31 11:13:06','2016-03-31 11:13:06');
/*!40000 ALTER TABLE `mst_userlogon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_userrole`
--

DROP TABLE IF EXISTS `mst_userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userrole` (
  `roleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `rolename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userrole`
--

LOCK TABLES `mst_userrole` WRITE;
/*!40000 ALTER TABLE `mst_userrole` DISABLE KEYS */;
INSERT INTO `mst_userrole` VALUES (1,'Account Manager','1','AM','Account Manager'),(2,'Quote Facilitator','1','QF','Quote Facilitator'),(3,'Trigger Man','1','TM','Trigger Man'),(4,'Administrator','1','AD','Sub Administrator'),(5,'Administrator','1','AD','Administrator'),(6,'Account Manager','1','AM','Partner');
/*!40000 ALTER TABLE `mst_userrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `old_useractivitypurge`
--

DROP TABLE IF EXISTS `old_useractivitypurge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `old_useractivitypurge` (
  `actionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `old_useractivitypurge`
--

LOCK TABLES `old_useractivitypurge` WRITE;
/*!40000 ALTER TABLE `old_useractivitypurge` DISABLE KEYS */;
/*!40000 ALTER TABLE `old_useractivitypurge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_master`
--

DROP TABLE IF EXISTS `page_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_master` (
  `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_show_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_path` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_master`
--

LOCK TABLES `page_master` WRITE;
/*!40000 ALTER TABLE `page_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_notification`
--

DROP TABLE IF EXISTS `page_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_notification` (
  `n_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `p_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `n_variable` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `final_noti` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `n_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_notification`
--

LOCK TABLES `page_notification` WRITE;
/*!40000 ALTER TABLE `page_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_advertisment`
--

DROP TABLE IF EXISTS `post_advertisment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_advertisment` (
  `postid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pdate` datetime NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `industry` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ptype` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerrefno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pakaging` text COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `targetprice` int(11) NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `tempid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `archivepost` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `buyer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotefacility` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `act_inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`postid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_advertisment`
--

LOCK TABLES `post_advertisment` WRITE;
/*!40000 ALTER TABLE `post_advertisment` DISABLE KEYS */;
INSERT INTO `post_advertisment` VALUES (1,'admin','2016-03-03 07:10:07','CBN-044-472','','','','','BUY','USD','per box',3,'DC-NA','Within 3 Months','','123456','English','33','2 - 3 Weeks','Albania','NEW',23,'507554','','0','admin','','',0,NULL,NULL),(2,'admin','2016-03-03 07:18:11','WSR-681-637','','','','','BUY','USD','per case',8,'DC-UK','3 - 6 Months','','23456','Albanian','5','1 - 2 Weeks','Albania','NEW',8,'833526','','0','admin','','',0,NULL,NULL),(3,'UUZZ-9984','2016-03-21 16:24:40','RYD-735-547','','','','','SELL','USD','per case',3,'DC-EU','3 - 6 Months','2016-03-22','123456','English','33','1 - 2 Weeks','India','NEW',89,'864499','','0','','UUZZ-9984','',0,'2016-03-21 20:24:40','2016-03-21 20:24:40'),(4,'UUZZ-9984','2016-03-21 16:25:37','RYD-735-547','','','','','BUY','GBP','per case',15,'DC-EU','3 - 6 Months','2016-03-22','23456','English','33','Within 1 Week','India','NEW',123,'110022','','0','UUZZ-9984','','',1,'2016-03-21 20:25:37','2016-03-31 10:58:25'),(5,'UUZZ-9984','2016-03-21 16:28:20','CBN-044-472','','','','','SELL','GBP','per pallet',15,'DC-EU','3 - 6 Months','2016-03-22','123456','Filipino','38','1 - 2 Weeks','Andorra','NEW',123,'811132','','0','','UUZZ-9984','',0,'2016-03-21 20:28:20','2016-03-21 20:28:20'),(6,'UUZZ-9984','2016-03-21 16:30:31','CBN-044-472','','','','','SELL','USD','per unit',3,'DC-EU','3 - 6 Months','2016-03-02','123456','English','33','2 - 3 Weeks','Afghanistan','NEW',5,'494433','','0','','UUZZ-9984','',0,'2016-03-21 20:30:31','2016-03-21 20:30:31'),(7,'UUZZ-9984','2016-03-21 16:33:27','AAI-965-082','','','','','BUY','USD','per case',3,'DC-EU','3 - 6 Months','2016-03-22','23456','English','33','1 - 2 Weeks','India','NEW',5,'654400','','0','UUZZ-9984','','',0,'2016-03-21 20:33:27','2016-03-21 20:33:27'),(8,'UUZZ-9984','2016-03-21 17:10:03','KJG-676-279','','','','','BUY','USD','per unit',500,'DC-EU','6 - 12 Months','2016-03-21','123456','English','33','2 - 3 Weeks','Austria','NEW',45,'689955','','0','UUZZ-9984','','',0,'2016-03-21 21:10:03','2016-03-21 21:10:03'),(9,'UUZZ-9984','2016-03-22 21:48:11','BJK-469-593','','','','','SELL','USD','per unit',10000,'DC-EU','3 - 6 Months','2016-03-22','23456','Czech','29','1 - 2 Weeks','Afghanistan','NEW',500,'554506','','0','','UUZZ-9984','',0,'2016-03-23 01:48:11','2016-03-23 01:48:11'),(10,'UUZZ-9984','2016-03-28 13:15:20','ITS-730-652','','','','','BUY','USD','per unit',1000,'DC-EU','18 - 24 Months','2016-03-28','23456','English','33','2 - 3 Weeks','Canada','NEW',50,'700237','','0','UUZZ-9984','','',0,'2016-03-28 17:15:20','2016-03-28 17:15:20'),(11,'UUZZ-9984','2016-03-29 09:28:35','KJG-676-279','','','','','SELL','USD','per unit',70,'DC-EU','12 - 18 Months','2016-03-28','123456','English','33','12+ Weeks','Iceland,India','NEW',1000,'555225','','0','','UUZZ-9984','',0,'2016-03-29 13:28:35','2016-03-29 13:28:35'),(13,'UUZZ-9984','2016-03-30 14:29:06','XIK-181-418','','','','','SELL','EUR','per box',50000,'DC-UK','Within 3 Months','2016-04-14','11111','English','33','12+ Weeks','Afghanistan','NEW',85,'782885','','0','','UUZZ-9984','',0,'2016-03-30 18:29:06','2016-03-30 18:29:06'),(14,'UUZZ-9984','2016-03-31 04:49:27','FRA-600-112','','','','','BUY','USD','per box',111,'frrjallasndlaksd','12 - 18 Months','2016-03-31','11111','English','33','3 - 4 Weeks','Bolivia','NEW',11111,'533430','','0','UUZZ-9984','','',0,'2016-03-31 08:49:27','2016-03-31 08:49:27'),(15,'UUZZ-9984','2016-03-31 05:02:25','RYD-735-547','','','','','BUY','GBP','per box',2147483647,'DC-EU','6 - 12 Months','2016-03-31','11111','Croatian','28','1 - 2 Weeks','Afghanistan','NEW',999999999,'471144','','0','UUZZ-9984','','',0,'2016-03-31 09:02:25','2016-03-31 09:02:25');
/*!40000 ALTER TABLE `post_advertisment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_customer`
--

DROP TABLE IF EXISTS `post_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_customer` (
  `custid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `edate` date NOT NULL,
  `refuserid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `adduserid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`custid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_customer`
--

LOCK TABLES `post_customer` WRITE;
/*!40000 ALTER TABLE `post_customer` DISABLE KEYS */;
INSERT INTO `post_customer` VALUES (1,'123456','2016-03-03','JUQI-3911','0','AD','admin',NULL,'2016-03-30 10:04:53'),(2,'23456','2016-03-03','OORQ-0229','1','AD','admin',NULL,NULL),(4,'11111','2016-03-29','XPAP-5806','1','AD','UUZZ-9984','2016-03-29 08:19:43','2016-03-29 08:19:43'),(5,'232323','2016-03-31','DYAA-0677','1','AD','UUZZ-9984','2016-03-31 07:35:43','2016-03-31 07:35:43');
/*!40000 ALTER TABLE `post_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_quotation`
--

DROP TABLE IF EXISTS `post_quotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_quotation` (
  `quoteid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` datetime NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `declinemsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offerdeclinemsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptmsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offercrdate` datetime NOT NULL,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quationstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `offerstatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `accdate` date DEFAULT NULL,
  `rejdate` date DEFAULT NULL,
  `lastcntdate` date DEFAULT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `counetr_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `postuserid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `readunreadst` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`quoteid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_quotation`
--

LOCK TABLES `post_quotation` WRITE;
/*!40000 ALTER TABLE `post_quotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_quotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productimport`
--

DROP TABLE IF EXISTS `productimport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productimport` (
  `productid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` text COLLATE utf8_unicode_ci,
  `packaging` text COLLATE utf8_unicode_ci,
  `shippingcond` text COLLATE utf8_unicode_ci,
  `qtypercase` bigint(20) DEFAULT NULL,
  `des` text COLLATE utf8_unicode_ci,
  `mpm` double(8,2) DEFAULT NULL,
  `prefixone` text COLLATE utf8_unicode_ci,
  `codeone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixtwo` tinyint(4) DEFAULT NULL,
  `codetwo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixthree` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codethree` text COLLATE utf8_unicode_ci,
  `img1` text COLLATE utf8_unicode_ci,
  `img2` text COLLATE utf8_unicode_ci,
  `img3` text COLLATE utf8_unicode_ci,
  `img4` text COLLATE utf8_unicode_ci,
  `img5` text COLLATE utf8_unicode_ci,
  `img6` text COLLATE utf8_unicode_ci,
  `img7` text COLLATE utf8_unicode_ci,
  `img8` text COLLATE utf8_unicode_ci,
  `addedby` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productimport`
--

LOCK TABLES `productimport` WRITE;
/*!40000 ALTER TABLE `productimport` DISABLE KEYS */;
/*!40000 ALTER TABLE `productimport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotation_counter`
--

DROP TABLE IF EXISTS `quotation_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotation_counter` (
  `counterid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_date` datetime NOT NULL,
  `counter_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `counter_quantity` int(11) NOT NULL,
  `counter_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counetr_status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`counterid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotation_counter`
--

LOCK TABLES `quotation_counter` WRITE;
/*!40000 ALTER TABLE `quotation_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terms_condition`
--

DROP TABLE IF EXISTS `terms_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terms_condition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trems_condition` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terms_condition`
--

LOCK TABLES `terms_condition` WRITE;
/*!40000 ALTER TABLE `terms_condition` DISABLE KEYS */;
INSERT INTO `terms_condition` VALUES (1,'new terms and conditions',NULL,NULL);
/*!40000 ALTER TABLE `terms_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ind_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Index_val` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test1`
--

DROP TABLE IF EXISTS `test1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `industry` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test1`
--

LOCK TABLES `test1` WRITE;
/*!40000 ALTER TABLE `test1` DISABLE KEYS */;
/*!40000 ALTER TABLE `test1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tm_accepted_offers`
--

DROP TABLE IF EXISTS `tm_accepted_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_accepted_offers` (
  `acc_offr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `counterid` int(11) NOT NULL,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_date` datetime NOT NULL,
  `acc_price` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_quantity` int(11) NOT NULL,
  `acc_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `acc_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noti_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tm_userID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `offerType` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`acc_offr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tm_accepted_offers`
--

LOCK TABLES `tm_accepted_offers` WRITE;
/*!40000 ALTER TABLE `tm_accepted_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tm_accepted_offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-31  7:13:53
