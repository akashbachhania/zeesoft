var ChartsFlotcharts = function() {

    return {
        //main function to initiate the module
        init: function() {
            Metronic.addResizeHandler(function() {
                Charts.initPieCharts();
            });
        },

        initCharts: function() {
            if (!jQuery.plot) {
                return;
            }
            var data = [];
            var totalPoints = 250;
        },

        initBarCharts: function() {
            // horizontal bar chart:
			var sec=$("#sec").val();
			var filtername=$("#filtername").val();
			var from=$("#from").val();
			var to=$("#to").val();
			var industry=$("#industry").val();
			var cat_id=$("#cat_id").val();
			var sub_cat=$("#sub_cat").val();
			var bname=$("#bname").val();
			var searchid=$("#searchid").val();
			var customerdata=$("#customerdata").val();
			var status=$("#status").val();
			var alluserid=$("#alluserid").val();
			var type=$("#type").val();
			var alllocation=$("#alllocation").val();
			//alert(industry);
			//alert(industry);
			var formData = {
				section : sec,
				filtername: filtername,
				from: from,
				to: to,
				industry: industry,
				cat_id: cat_id,
				sub_cat: sub_cat,
				bname: bname,
				searchid: searchid,
				customerdata: customerdata,
				status: status,
				alluserid: alluserid,
				type: type,
				alllocation: alllocation
			}; 
			
		var data = [];
		var ticks = [];
		var newdata = [];
		var name  = [];
		var data1 = [];
		$.ajax({
		url:root + "/report/barChartPro",
		cache: false,
		 type: "POST",
		 data: formData,
		 dataType: "json",
		   success: function(res) {
			   //alert(res);
			 $.each(res, function(i, item) {
				newdata = [res[i].count,i];
				data1.push(newdata);
				name = [i,res[i].lable];
				ticks.push(name);
			});
			console.log(data1);
			console.log(ticks);
            var options = {
                series: {
                    bars: {
                        show: true
                    }
                },
				
                bars: {
					horizontal: true,
					groupWidth: '100%',
					barWidth: .20,
                    lineWidth:.8, // in pixels
                    shadowSize: 0,
                    align: 'left'
                },
				yaxis: {
					ticks: ticks
				},
                grid: {
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1 
                }
            };
            if ($('#chart_1_2').size() !== 0) {
				var dataSet = [
					{ data: data1 }
				];                
				$.plot($("#chart_1_2"), dataSet, options);
            }
          }
        });

		/*
		var data1 = [
						[15, 1],  //Silver
						[1, 2],   //Platinum
						[40, 3],
						[10, 4],
						[5, 5],
						[25, 6]
					];
		
		var ticks = [
						[1, "Mechanical"], [2, "CS"], [3, "Platinum"], [4, "as"], [5, "er"], [6, "zy"]
					];

			console.log(data1);
		*/
        },
/****************************************Pie chart start**********************************************************/										
        initPieCharts: function(filternm) {
			//alert(filternm);
			var sec=$("#sec").val();
			var filtername=$("#filtername").val();
			var from=$("#from").val();
			var to=$("#to").val();
			var industry=$("#industry").val();
			var cat_id=$("#cat_id").val();
			var sub_cat=$("#sub_cat").val();
			var bname=$("#bname").val();
			var searchid=$("#searchid").val();
			var customerdata=$("#customerdata").val();
			var status=$("#status").val();
			var alluserid=$("#alluserid").val();
			var type=$("#type").val();
			var alllocation=$("#alllocation").val();
			
			var formData = {
				section: sec,
				filtername: filtername,
				from: from,
				to: to,
				industry: industry,
				cat_id: cat_id,
				sub_cat: sub_cat,
				bname: bname,
				searchid: searchid,
				customerdata: customerdata,
				status: status,
				alluserid: alluserid,
				type: type,
				alllocation: alllocation
			};
			//alert(formData);
			//alert(ob1);
		var data = [];
		/******************************Ajax call start*********************************************/
	$.ajax({
            url:root + "/report/metroniChartPro",
            cache: false,
			 type: "POST",
			 data: formData,
			   success: function(res) {
				//alert(res);
				 data =JSON.parse(res);
            // DEFAULT
            if ($('#pie_chart').size() !== 0) {
                $.plot($("#pie_chart"), data, {
                    series: {
                        pie: {
                            show: true
                        }
                    }
                });
            }
            // GRAPH 4
            if ($('#pie_chart_4').size() !== 0) {
                $.plot($("#pie_chart_4"), data, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 3 / 4,
                                formatter: function(label, series) {
									 var num = (series.percent);
										var per_n = num.toFixed(2)
                                    return '<div style="font-size:10pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' +(per_n) + '%</div>';
                                },
                                background: {
                                    opacity: 0.5,
                                    color: '#000'
                                }
                            }
                        }
                    },
                    legend: {
                        show: false
                    }
                });
            }
 }
        });
       /***********************************Ajax Call end*****************************************************/     
            function pieHover(event, pos, obj) {
                if (!obj)
                    return;
                percent = parseFloat(obj.series.percent).toFixed(2);
                $("#hover").html('<span style="font-weight: bold; color: ' + obj.series.color + '">' + obj.series.label + ' (' + percent + '%)</span>');
            }

            function pieClick(event, pos, obj) {
				alert('@@@@@@@@@@@@@@');
                if (!obj)
                    return;
                percent = parseFloat(obj.series.percent).toFixed(2);
                alert('' + obj.series.label + ': ' + percent + '%');
            }

        }
/***************************************pie chart end*********************************************************************/
    };

}();