$(document).ready(function(){

    if(String(window.location).match(/userdetails/)){
        
        var userdetailtable = $('#userdetail').DataTable( {
            "ajax": {
                "url":"userdetail_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'userid' },
            { data: 'userrole' },
            { data: 'creationdate' },
            { data: 'lastlogin' },
            { data: 'status' },
            { data: 'action' }
         
            ]
        });

        setInterval( function () {
            userdetailtable.ajax.reload(null,false);
        }, 3000 );  
    }

    /*else if( String(window.location).match(/acceptedoffers/) ){

        $('#acceptedoffers').dataTable();

    }*/
    else if(String(window.location).match(/activity/)){
        var url = $(location).attr('href');
        
        var fn = url.split('/').reverse()[0];

        var lm = url.split('/').reverse()[1];
        
        if( lm == 'activity' ){
            if( fn != '' ){

                var useravtivitytable = $('#useractivity').DataTable( {

                    "ajax": {
                        "url":root + "/activity_ajax/"+fn,
                        "dataSrc": "",
                        "type": 'GET',
                    },
                    columns: [
                    { data: 'userid' },
                    { data: 'lastlogin' },
                    { data: 'ipaddress' },
                    { data: 'activity' }
                 
                    ]
                } );

                setInterval( function () {
                    useravtivitytable.ajax.reload(null,false);
                }, 3000 );

            }
            
        }
        else if( fn == 'activity' ){

                var useravtivitytable = $('#useractivity').DataTable( {

                    "ajax": {
                        "url":root + "/activity_ajax",
                        "dataSrc": "",
                        "type": 'GET',
                    },
                    columns: [
                    { data: 'userid' },
                    { data: 'lastlogin' },
                    { data: 'ipaddress' },
                    { data: 'activity' }
                 
                    ]
                } );

                setInterval( function () {
                    useravtivitytable.ajax.reload(null,false);
                }, 3000 );

            }

       
        $('#exportcsv').click(function() {

                $.ajax({
                    url: root +'/exportActivity',
                    success: function (resp) {
                        
                    }
                });
            
        });
    }
    else if(String(window.location).match(/create/) || String(window.location).match(/edit/) ){
        $(document).ready(function(){
            $("#from,#to").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
        });
        function checkPass(){
            var pass = $("#pass").val();
            var conpass = $("#conpass").val();
            
            if (pass != conpass){
                alert('Passwords do not Match!');
                $("#conpass").parent().parent().addClass('has-error');
                $("#conpass").focus();
                return false;
            }
            $("#conpass").parent().parent().removeClass('has-error');
            return true;

        }

        function checkPassLength(){
            var s = $('#pass').val();
            // check for null or too short
            if (!s || s.length < 6) {
                alert("Pasword must have at least 6 characters."); // prompt user
                $("#pass").parent().parent().addClass('has-error');
                $("#pass").focus();
                return false;
            }
            // check for a number
            else if (/[0-9]/.test(s) === false) {
                alert("Pasword must have at least one number."); // prompt user
                $("#pass").parent().parent().addClass('has-error');
                $("#pass").focus();
                return false;
            }
            // check for a capital letter
            else if (/[a-zA-Z]/.test(s) === false) {
                alert("Pasword must have at least one letter."); // prompt user
                $("#pass").parent().parent().addClass('has-error');
                $("#pass").focus();
                return false;
            }
            $("#pass").parent().parent().removeClass('has-error');
            return true; // return true if all conditions are met
        }

        $("#role,#authpass").change(function(){
            $(this).parent().parent().removeClass('has-error');
        });

        $('#submitButton').click(function(event){
            event.preventDefault();
            var authpass = "{{ Auth::user()->authorizedpass }}";
            
            if ($("#role").val() == 0){
        
                $("#role").parent().parent().addClass('has-error');
                alert("User Role Required"); // prompt user
                $("#role").focus(); //set focus back to control
                return false;
        
            }
        
            if ($("#role").val() ==1)
            {
                var options = $('#indmultipleSelect > option:selected');
                if (options.length == 0) {
                    alert('Industries are Required');
                    $("#indmultipleSelect").focus();
                    return false;
                }
            }

            if ($("#authpass").val() == '') {
                alert('Authorized code is required');
                $("#authpass").parent().parent().addClass('has-error');
                $("#authpass").focus();
                return false;
            } else {
                $.get(root + '/checkauthpass',{authpass:$("#authpass").val()},function(data){
                    if(data == 'success'){

                        if(String(window.location).match(/create/)){
                            $("#c_u_f").submit();    
                        }
                        $("#update_user").submit();                        
                    }
                    else{
                        alert('Authorized Password Incorrect!');
                        $("#authpass").parent().parent().addClass('has-error');
                        $("#authpass").focus();
                    }
                    
                });
            }
            
        });
            

    }
    else if(String(window.location).match(/dbbackup/)){
        $('.fa-download').on('click',function(){
            
            // $.get(root + '/backup',function(data){
                // alert(data);             
            // });
        });
    }
    else if(String(window.location).match(/browse/)){
        
    }

    else if(String(window.location).match(/detail-product-view/)){

          $("#btnsave").on("click", function() {
            var prefix = $('#prefix').val();
            var code = $('#code').val();
            var pid = $('#pid').val();
            // $("#form_productcode").valid();
            if (prefix == '' || code == '') {
                alert('Please Enter The Prefix Code');
                return;
            }

            
            var data1 = {
                Type: 'exists',
                prefix: prefix,
                code: code,
                pid: pid
            };
            var data2 = {
                Type: 'addcode',
                prefix: prefix,
                code: code,
                pid: pid
            };
            $.ajax({
                type: 'POST',
                url: root + '/product/addProductPrefix_ajax',
                cache: false,
                data: data1,
                success: function(data) {
                    if( data == 'exist' ){
                        $('#re').html('Product Code Already Exist').css('color','red');
                        return;
                    }
                    else{
                        $('#re').html('');
                        $.ajax({
                            type: 'POST',
                            url: root + '/product/addProductPrefix_ajax',
                            cache: false,
                            data: data2,
                            success: function(data) {
                                // alert(data);
                                console.log(data);
                                // response = jQuery.parseJSON(data);
                                if (data.msg == 'SUCCESS') {
                                    $('#basic').modal('hide');
                                    // shownoti("Successfully submitted product code request");
                                    
                                    setTimeout(function() {
                                        window.location.href = root+ "/product/detail-product-view/" + data.pid;
                                    }, 1000);
                                }
                                if (data.msg == 'ERROR') {
                                    alert(data.name);
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                //commit(false);
                            }
                        });            
                    }
                },
            });          
            
        });

    }
    else if(String(window.location).match(/suggestedProductCode/)){

        var suggestedProductCodeTable = $('#suggestedProductCodeTable').DataTable( {

            "ajax": {
                "url":"suggestedProductCode_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'suggestdate' },
            { data: 'userid' },
            { data: 'industry' },
            { data: 'brand'},
            { data: 'productName' },
            { data: 'productCode' },
            { data: 'status' },
            // { data: 'edit'}
         
            ]
        } );

        setInterval( function () {
            suggestedProductCodeTable.ajax.reload(null,false);
        }, 3000 );

    }
    else if(String(window.location).match(/suggestedProduct/)){

        

    }
    else if(String(window.location).match(/reportedProduct/)){



    }
    else if(String(window.location).match(/postingCatalog/)){


        

    }else if( String(window.location).match(/posting/) ){

        $('#userposting').dataTable();

    }
    else if( String(window.location).match(/import-post/) ){

    }

    else if(String(window.location).match(/my-offers/)){

        

    }
});