-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: minialpha
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MastPage`
--

DROP TABLE IF EXISTS `MastPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MastPage` (
  `PageId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PageName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DisplayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Parent_Id` int(11) NOT NULL,
  `Level_Idx` int(11) NOT NULL,
  `Idx` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PageId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MastPage`
--

LOCK TABLES `MastPage` WRITE;
/*!40000 ALTER TABLE `MastPage` DISABLE KEYS */;
INSERT INTO `MastPage` VALUES (3,'','Customers','Customers',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(4,'mstcustomer','Customers','Browse Customers',3,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(5,'','Product Catalog','Product Catalog',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(6,'add-product','Product Catalog','Add New Product',5,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(7,'product-catalog','Product Catalog','Browse Products',5,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(8,'suggested-product','Product Catalog','Suggested Product',5,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(9,'suggested-product-code','Product Catalog','Suggested Product Code',5,0,4,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(10,'reported-product','Product Catalog','Reported Product',5,0,5,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(11,'','Postings','Postings',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(12,'posting-catalog','Postings','Browse Postings',11,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(13,'my-posting','Postings','My Postings',11,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(14,'import-post','Postings','Import Postings',11,0,4,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(15,'','Product List Download','Products List Download',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(16,'product-listdownload','Product List Download','Products List Download',15,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(17,'','Quote Queue','Quote Queue',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(18,'quoteandpost_que','Quote Queue','Quote Queue',17,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(19,'','Orders','Orders',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(20,'tm-accepted-offers','Orders','Accepted Offers',19,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(21,'tm-completed-offers','Orders','Completed Offers',19,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(22,'tm-cancelled-order','Orders','Cancelled Orders',19,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(23,'','Settings','Settings',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(24,'my-setting','Settings','My Settings',23,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(25,'notification-setting','Settings','Notifications',23,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(26,'notification','Settings','Notifications Settings',23,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(27,'import-data','Settings','Import Data',23,0,4,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(28,'mstindustry','Settings','Industry List',23,0,5,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(29,'mstcategory','Settings','Category List',23,0,6,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(30,'mstsubcategory','Settings','Sub Category List',23,0,7,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(31,'mstbrand','Settings','Brands List',23,0,8,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(32,'mstlocation','Settings','Location',23,0,9,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(34,'mstsecurity-que','Settings','Security Question List',23,0,11,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(35,'mstterms-condition','Settings','Terms & Conditions',23,0,12,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(36,'databackup','Settings','Database Backup',23,0,13,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(37,'maskproductname','Settings','Website Settings',23,0,15,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(38,'menu_permission','Settings','Menu Permission',23,0,16,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(39,'','Reports','Reports',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(40,'report','Reports','Reports',39,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(41,'','Users','Users',0,0,0,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(42,'userdetails','Users','View Users',41,0,1,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(43,'activity','Users','Users Activity',41,0,2,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(44,'create','Users','Create New User',41,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(45,'mstrole','Settings','Role List',23,0,17,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(46,'databaserestore','Settings','Database Restore',23,0,14,'2016-02-18 06:37:58','2016-02-18 06:37:58'),(47,'my-offer','Postings','My Offers',11,0,3,'2016-02-18 06:37:58','2016-02-18 06:37:58');
/*!40000 ALTER TABLE `MastPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MastRolePermission`
--

DROP TABLE IF EXISTS `MastRolePermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MastRolePermission` (
  `RoleId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `ViewP` int(11) NOT NULL,
  `AddP` int(11) NOT NULL,
  `EditP` int(11) NOT NULL,
  `DeleteP` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`RoleId`,`PageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MastRolePermission`
--

LOCK TABLES `MastRolePermission` WRITE;
/*!40000 ALTER TABLE `MastRolePermission` DISABLE KEYS */;
INSERT INTO `MastRolePermission` VALUES (1,4,1,1,1,1,1,0,NULL,NULL),(1,6,1,1,1,1,1,1,NULL,NULL),(1,7,1,1,1,1,1,1,NULL,NULL),(1,12,1,1,1,0,1,0,NULL,NULL),(1,13,1,1,1,0,0,0,NULL,NULL),(1,14,1,1,0,0,0,0,NULL,NULL),(1,16,1,1,1,0,1,1,NULL,NULL),(1,24,1,1,1,0,0,0,NULL,NULL),(1,25,1,1,1,0,0,0,NULL,NULL),(1,27,1,1,1,0,1,0,NULL,NULL),(1,47,1,1,1,0,0,0,NULL,NULL),(2,18,1,1,1,0,0,0,NULL,NULL),(2,24,1,1,1,1,0,0,NULL,NULL),(2,25,1,1,1,1,0,0,NULL,NULL),(3,20,1,1,1,1,0,0,NULL,NULL),(3,21,1,1,1,1,0,0,NULL,NULL),(3,22,1,1,1,1,0,0,NULL,NULL),(3,24,1,1,1,1,0,0,NULL,NULL),(3,25,1,1,1,1,0,0,NULL,NULL),(4,4,1,1,1,1,0,0,NULL,NULL),(4,6,1,1,1,1,1,0,NULL,NULL),(4,7,1,1,1,1,1,0,NULL,NULL),(4,8,1,1,1,1,1,0,NULL,NULL),(4,9,1,1,1,1,1,0,NULL,NULL),(4,10,1,1,1,1,1,0,NULL,NULL),(4,12,1,1,1,0,1,0,NULL,NULL),(4,13,1,1,1,0,1,0,NULL,NULL),(4,14,1,1,1,0,1,0,NULL,NULL),(4,16,1,0,0,0,0,0,NULL,NULL),(4,18,1,1,1,0,1,0,NULL,NULL),(4,20,1,1,1,0,1,0,NULL,NULL),(4,21,1,1,1,0,1,0,NULL,NULL),(4,22,1,1,1,0,1,0,NULL,NULL),(4,24,1,1,1,0,1,0,NULL,NULL),(4,25,1,1,1,0,1,0,NULL,NULL),(4,26,1,1,1,0,1,0,NULL,NULL),(4,27,1,1,1,0,1,0,NULL,NULL),(4,28,1,1,1,0,1,0,NULL,NULL),(4,29,1,1,1,0,1,0,NULL,NULL),(4,30,1,1,1,0,1,0,NULL,NULL),(4,31,1,1,1,0,1,0,NULL,NULL),(4,32,1,1,1,0,1,0,NULL,NULL),(4,34,1,1,1,0,1,0,NULL,NULL),(4,35,1,1,1,0,1,0,NULL,NULL),(4,36,1,1,1,0,1,0,NULL,NULL),(4,37,1,1,1,0,1,0,NULL,NULL),(4,38,1,1,1,0,1,0,NULL,NULL),(4,40,1,0,0,0,0,0,NULL,NULL),(4,42,1,1,1,0,1,0,NULL,NULL),(4,43,1,1,1,0,1,0,NULL,NULL),(4,44,1,1,1,0,1,0,NULL,NULL),(4,45,1,1,1,0,1,0,NULL,NULL),(4,46,1,1,1,0,1,0,NULL,NULL),(4,47,1,1,1,0,1,0,NULL,NULL),(5,4,1,1,1,1,0,0,NULL,NULL),(5,6,1,1,1,1,0,0,NULL,NULL),(5,7,1,1,1,1,0,0,NULL,NULL),(5,8,1,1,1,1,0,0,NULL,NULL),(5,9,1,1,1,1,0,0,NULL,NULL),(5,10,1,1,1,1,0,0,NULL,NULL),(5,12,1,0,1,1,0,0,NULL,NULL),(5,13,1,1,1,0,0,0,NULL,NULL),(5,14,1,1,1,1,0,0,NULL,NULL),(5,16,1,1,1,1,1,1,NULL,NULL),(5,18,1,1,1,1,0,0,NULL,NULL),(5,20,1,1,1,1,0,0,NULL,NULL),(5,21,1,1,1,1,0,0,NULL,NULL),(5,22,1,1,1,1,0,0,NULL,NULL),(5,24,1,1,1,1,0,0,NULL,NULL),(5,25,1,1,1,1,0,0,NULL,NULL),(5,27,1,1,1,1,0,0,NULL,NULL),(5,28,1,1,1,1,0,0,NULL,NULL),(5,29,1,1,1,1,0,0,NULL,NULL),(5,30,1,1,1,1,0,0,NULL,NULL),(5,31,1,1,1,1,0,0,NULL,NULL),(5,32,1,1,1,1,0,0,NULL,NULL),(5,34,1,1,1,1,0,0,NULL,NULL),(5,35,1,1,1,1,0,0,NULL,NULL),(5,36,1,1,1,1,0,0,NULL,NULL),(5,37,1,1,1,1,0,0,NULL,NULL),(5,38,1,1,1,1,0,0,NULL,NULL),(5,40,1,1,1,1,0,0,NULL,NULL),(5,42,1,1,1,1,0,0,NULL,NULL),(5,43,1,1,1,1,0,0,NULL,NULL),(5,44,1,1,1,1,0,0,NULL,NULL),(5,45,1,0,0,0,0,0,NULL,NULL),(5,46,1,1,0,0,0,0,NULL,NULL),(6,4,1,0,0,0,0,0,NULL,NULL),(14,20,1,0,0,0,0,0,NULL,NULL),(14,21,0,0,1,0,0,0,NULL,NULL),(14,22,0,1,0,1,0,0,NULL,NULL),(14,24,0,1,0,0,0,0,NULL,NULL),(14,25,0,0,1,0,0,0,NULL,NULL),(14,26,0,0,0,1,0,0,NULL,NULL),(14,27,0,0,0,1,0,0,NULL,NULL),(14,28,0,0,1,0,0,0,NULL,NULL),(14,29,0,1,0,0,0,0,NULL,NULL),(14,31,1,0,0,0,0,0,NULL,NULL),(14,32,0,1,0,0,0,0,NULL,NULL),(14,36,0,0,1,0,0,0,NULL,NULL),(14,38,1,0,0,0,0,0,NULL,NULL),(15,4,1,1,0,0,0,0,NULL,NULL),(15,6,1,1,1,0,0,0,NULL,NULL),(15,7,1,1,1,0,0,0,NULL,NULL),(17,4,1,1,1,0,0,0,NULL,NULL),(18,4,1,1,1,1,0,0,NULL,NULL),(18,6,1,1,0,0,0,0,NULL,NULL),(18,7,1,1,0,0,0,0,NULL,NULL),(18,12,1,1,0,0,0,0,NULL,NULL),(18,13,1,1,0,0,0,0,NULL,NULL),(18,14,1,1,0,0,0,0,NULL,NULL),(18,24,1,0,0,0,0,0,NULL,NULL),(18,25,1,0,0,0,0,0,NULL,NULL),(18,47,1,1,0,0,0,0,NULL,NULL);
/*!40000 ALTER TABLE `MastRolePermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `count_notification`
--

DROP TABLE IF EXISTS `count_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `count_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tmsg` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `count_notification`
--

LOCK TABLES `count_notification` WRITE;
/*!40000 ALTER TABLE `count_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `count_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countdata`
--

DROP TABLE IF EXISTS `countdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countdata` (
  `id` int(11) NOT NULL,
  `recordcount` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`recordcount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countdata`
--

LOCK TABLES `countdata` WRITE;
/*!40000 ALTER TABLE `countdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `countdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maskproname`
--

DROP TABLE IF EXISTS `maskproname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maskproname` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maskstatus` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maskproname`
--

LOCK TABLES `maskproname` WRITE;
/*!40000 ALTER TABLE `maskproname` DISABLE KEYS */;
/*!40000 ALTER TABLE `maskproname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_permission`
--

DROP TABLE IF EXISTS `menu_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_permission` (
  `rowId` int(11) NOT NULL,
  `pageView` int(11) NOT NULL,
  `pageAdd` int(11) NOT NULL,
  `pageEdit` int(11) NOT NULL,
  `pageDelete` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permission`
--

LOCK TABLES `menu_permission` WRITE;
/*!40000 ALTER TABLE `menu_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_admin_report`
--

DROP TABLE IF EXISTS `mst_admin_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_admin_report` (
  `reportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reportsql` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_admin_report`
--

LOCK TABLES `mst_admin_report` WRITE;
/*!40000 ALTER TABLE `mst_admin_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_admin_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_brand`
--

DROP TABLE IF EXISTS `mst_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_brand` (
  `brandid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `industryid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`brandid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_brand`
--

LOCK TABLES `mst_brand` WRITE;
/*!40000 ALTER TABLE `mst_brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_category`
--

DROP TABLE IF EXISTS `mst_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_category` (
  `catid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `industryid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_category`
--

LOCK TABLES `mst_category` WRITE;
/*!40000 ALTER TABLE `mst_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_country`
--

DROP TABLE IF EXISTS `mst_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_country` (
  `countryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`countryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_country`
--

LOCK TABLES `mst_country` WRITE;
/*!40000 ALTER TABLE `mst_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_currency`
--

DROP TABLE IF EXISTS `mst_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_currency` (
  `currencyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_currency`
--

LOCK TABLES `mst_currency` WRITE;
/*!40000 ALTER TABLE `mst_currency` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_exp_daterange`
--

DROP TABLE IF EXISTS `mst_exp_daterange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_exp_daterange` (
  `exprangeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`exprangeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_exp_daterange`
--

LOCK TABLES `mst_exp_daterange` WRITE;
/*!40000 ALTER TABLE `mst_exp_daterange` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_exp_daterange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_industry`
--

DROP TABLE IF EXISTS `mst_industry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_industry` (
  `industryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`industryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_industry`
--

LOCK TABLES `mst_industry` WRITE;
/*!40000 ALTER TABLE `mst_industry` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_industry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_language`
--

DROP TABLE IF EXISTS `mst_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_language` (
  `languageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`languageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_language`
--

LOCK TABLES `mst_language` WRITE;
/*!40000 ALTER TABLE `mst_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_location`
--

DROP TABLE IF EXISTS `mst_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_location` (
  `locationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`locationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_location`
--

LOCK TABLES `mst_location` WRITE;
/*!40000 ALTER TABLE `mst_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_mngnotification`
--

DROP TABLE IF EXISTS `mst_mngnotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_mngnotification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagename` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_mngnotification`
--

LOCK TABLES `mst_mngnotification` WRITE;
/*!40000 ALTER TABLE `mst_mngnotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_mngnotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_model`
--

DROP TABLE IF EXISTS `mst_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_model` (
  `modelid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_model`
--

LOCK TABLES `mst_model` WRITE;
/*!40000 ALTER TABLE `mst_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_notification`
--

DROP TABLE IF EXISTS `mst_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_notification` (
  `notiid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `msgdate` datetime NOT NULL,
  `msg` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fromuserid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `preportid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notiid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_notification`
--

LOCK TABLES `mst_notification` WRITE;
/*!40000 ALTER TABLE `mst_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_pageName`
--

DROP TABLE IF EXISTS `mst_pageName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_pageName` (
  `rowid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pageCheckAdd` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckEdit` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckDelete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckPrint` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckExport` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_pageName`
--

LOCK TABLES `mst_pageName` WRITE;
/*!40000 ALTER TABLE `mst_pageName` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_pageName` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_product`
--

DROP TABLE IF EXISTS `mst_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_product` (
  `productid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `subcatid` int(11) DEFAULT NULL,
  `pakaging` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipingcondition` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qtypercase` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `manufacture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpm` int(11) DEFAULT NULL,
  `addedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `roletype` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img0` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img4` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img5` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img6` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img7` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code1` int(11) NOT NULL,
  `codevalue1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code2` int(11) NOT NULL,
  `codevalue2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code3` int(11) DEFAULT NULL,
  `codevalue3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_product`
--

LOCK TABLES `mst_product` WRITE;
/*!40000 ALTER TABLE `mst_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productcode`
--

DROP TABLE IF EXISTS `mst_productcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productcode` (
  `productcodeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `prefixid` int(11) NOT NULL,
  `refbyid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `approveid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `refdate` datetime NOT NULL,
  `approvedate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productcodeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productcode`
--

LOCK TABLES `mst_productcode` WRITE;
/*!40000 ALTER TABLE `mst_productcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_productcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productimages`
--

DROP TABLE IF EXISTS `mst_productimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productimages` (
  `productimageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `imageurl` text COLLATE utf8_unicode_ci NOT NULL,
  `srno` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productimageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productimages`
--

LOCK TABLES `mst_productimages` WRITE;
/*!40000 ALTER TABLE `mst_productimages` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_productimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productprefix`
--

DROP TABLE IF EXISTS `mst_productprefix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productprefix` (
  `proprefixid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`proprefixid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productprefix`
--

LOCK TABLES `mst_productprefix` WRITE;
/*!40000 ALTER TABLE `mst_productprefix` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_productprefix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productreport`
--

DROP TABLE IF EXISTS `mst_productreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productreport` (
  `preportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `reviewbyid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `reviewdate` date NOT NULL,
  `reviewremark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `reviewstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approvedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approveddate` date NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`preportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productreport`
--

LOCK TABLES `mst_productreport` WRITE;
/*!40000 ALTER TABLE `mst_productreport` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_productreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_productsimilar`
--

DROP TABLE IF EXISTS `mst_productsimilar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_productsimilar` (
  `similarid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `spid` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`similarid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_productsimilar`
--

LOCK TABLES `mst_productsimilar` WRITE;
/*!40000 ALTER TABLE `mst_productsimilar` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_productsimilar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_report`
--

DROP TABLE IF EXISTS `mst_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_report` (
  `reportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_report`
--

LOCK TABLES `mst_report` WRITE;
/*!40000 ALTER TABLE `mst_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_security`
--

DROP TABLE IF EXISTS `mst_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_security` (
  `securityid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qtype` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`securityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_security`
--

LOCK TABLES `mst_security` WRITE;
/*!40000 ALTER TABLE `mst_security` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_shipping`
--

DROP TABLE IF EXISTS `mst_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_shipping` (
  `shipid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shipid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_shipping`
--

LOCK TABLES `mst_shipping` WRITE;
/*!40000 ALTER TABLE `mst_shipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_subcategory`
--

DROP TABLE IF EXISTS `mst_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_subcategory` (
  `subcatid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subcatid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_subcategory`
--

LOCK TABLES `mst_subcategory` WRITE;
/*!40000 ALTER TABLE `mst_subcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_timeframe`
--

DROP TABLE IF EXISTS `mst_timeframe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_timeframe` (
  `timeframeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`timeframeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_timeframe`
--

LOCK TABLES `mst_timeframe` WRITE;
/*!40000 ALTER TABLE `mst_timeframe` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_timeframe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_timezone`
--

DROP TABLE IF EXISTS `mst_timezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_timezone` (
  `timezoneid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `timezonevalue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`timezoneid`)
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_timezone`
--

LOCK TABLES `mst_timezone` WRITE;
/*!40000 ALTER TABLE `mst_timezone` DISABLE KEYS */;
INSERT INTO `mst_timezone` VALUES (1,'Africa/Abidjan','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(2,'Africa/Accra','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(3,'Africa/Addis_Ababa','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(4,'Africa/Algiers','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(5,'Africa/Asmara','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(6,'Africa/Bamako','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(7,'Africa/Bangui','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(8,'Africa/Banjul','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(9,'Africa/Bissau','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(10,'Africa/Blantyre','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(11,'Africa/Brazzaville','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(12,'Africa/Bujumbura','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(13,'Africa/Cairo','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(14,'Africa/Casablanca','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(15,'Africa/Ceuta','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(16,'Africa/Conakry','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(17,'Africa/Dakar','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(18,'Africa/Dar_es_Salaam','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(19,'Africa/Djibouti','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(20,'Africa/Douala','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(21,'Africa/El_Aaiun','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(22,'Africa/Freetown','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(23,'Africa/Gaborone','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(24,'Africa/Harare','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(25,'Africa/Johannesburg','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(26,'Africa/Juba','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(27,'Africa/Kampala','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(28,'Africa/Khartoum','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(29,'Africa/Kigali','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(30,'Africa/Kinshasa','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(31,'Africa/Lagos','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(32,'Africa/Libreville','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(33,'Africa/Lome','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(34,'Africa/Luanda','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(35,'Africa/Lubumbashi','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(36,'Africa/Lusaka','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(37,'Africa/Malabo','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(38,'Africa/Maputo','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(39,'Africa/Maseru','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(40,'Africa/Mbabane','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(41,'Africa/Mogadishu','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(42,'Africa/Monrovia','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(43,'Africa/Nairobi','1','UTC/GMT +03:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(44,'Africa/Ndjamena','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(45,'Africa/Niamey','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(46,'Africa/Nouakchott','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(47,'Africa/Ouagadougou','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(48,'Africa/Porto-Novo','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(49,'Africa/Sao_Tome','1','UTC/GMT +00:00','2016-02-18 06:37:56','2016-02-18 06:37:56'),(50,'Africa/Tripoli','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(51,'Africa/Tunis','1','UTC/GMT +01:00','2016-02-18 07:37:56','2016-02-18 07:37:56'),(52,'Africa/Windhoek','1','UTC/GMT +02:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(53,'America/Adak','1','UTC/GMT -10:00','2016-02-18 08:37:56','2016-02-18 08:37:56'),(54,'America/Anchorage','1','UTC/GMT -09:00','2016-02-18 09:37:56','2016-02-18 09:37:56'),(55,'America/Anguilla','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(56,'America/Antigua','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(57,'America/Araguaina','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(58,'America/Argentina/Buenos_Aires','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(59,'America/Argentina/Catamarca','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(60,'America/Argentina/Cordoba','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(61,'America/Argentina/Jujuy','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(62,'America/Argentina/La_Rioja','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(63,'America/Argentina/Mendoza','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(64,'America/Argentina/Rio_Gallegos','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(65,'America/Argentina/Salta','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(66,'America/Argentina/San_Juan','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(67,'America/Argentina/San_Luis','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(68,'America/Argentina/Tucuman','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(69,'America/Argentina/Ushuaia','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(70,'America/Aruba','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(71,'America/Asuncion','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(72,'America/Atikokan','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(73,'America/Bahia','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(74,'America/Bahia_Banderas','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(75,'America/Barbados','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(76,'America/Belem','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(77,'America/Belize','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(78,'America/Blanc-Sablon','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(79,'America/Boa_Vista','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(80,'America/Bogota','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(81,'America/Boise','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(82,'America/Cambridge_Bay','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(83,'America/Campo_Grande','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(84,'America/Cancun','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(85,'America/Caracas','1','UTC/GMT -04:30','2016-02-18 14:07:57','2016-02-18 14:07:57'),(86,'America/Cayenne','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(87,'America/Cayman','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(88,'America/Chicago','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(89,'America/Chihuahua','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(90,'America/Costa_Rica','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(91,'America/Creston','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(92,'America/Cuiaba','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(93,'America/Curacao','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(94,'America/Danmarkshavn','1','UTC/GMT +00:00','2016-02-18 06:37:57','2016-02-18 06:37:57'),(95,'America/Dawson','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(96,'America/Dawson_Creek','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(97,'America/Denver','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(98,'America/Detroit','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(99,'America/Dominica','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(100,'America/Edmonton','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(101,'America/Eirunepe','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(102,'America/El_Salvador','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(103,'America/Fort_Nelson','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(104,'America/Fortaleza','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(105,'America/Glace_Bay','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(106,'America/Godthab','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(107,'America/Goose_Bay','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(108,'America/Grand_Turk','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(109,'America/Grenada','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(110,'America/Guadeloupe','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(111,'America/Guatemala','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(112,'America/Guayaquil','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(113,'America/Guyana','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(114,'America/Halifax','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(115,'America/Havana','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(116,'America/Hermosillo','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(117,'America/Indiana/Indianapolis','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(118,'America/Indiana/Knox','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(119,'America/Indiana/Marengo','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(120,'America/Indiana/Petersburg','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(121,'America/Indiana/Tell_City','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(122,'America/Indiana/Vevay','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(123,'America/Indiana/Vincennes','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(124,'America/Indiana/Winamac','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(125,'America/Inuvik','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(126,'America/Iqaluit','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(127,'America/Jamaica','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(128,'America/Juneau','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(129,'America/Kentucky/Louisville','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(130,'America/Kentucky/Monticello','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(131,'America/Kralendijk','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(132,'America/La_Paz','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(133,'America/Lima','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(134,'America/Los_Angeles','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(135,'America/Lower_Princes','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(136,'America/Maceio','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(137,'America/Managua','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(138,'America/Manaus','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(139,'America/Marigot','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(140,'America/Martinique','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(141,'America/Matamoros','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(142,'America/Mazatlan','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(143,'America/Menominee','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(144,'America/Merida','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(145,'America/Metlakatla','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(146,'America/Mexico_City','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(147,'America/Miquelon','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(148,'America/Moncton','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(149,'America/Monterrey','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(150,'America/Montevideo','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(151,'America/Montserrat','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(152,'America/Nassau','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(153,'America/New_York','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(154,'America/Nipigon','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(155,'America/Nome','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(156,'America/Noronha','1','UTC/GMT -02:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(157,'America/North_Dakota/Beulah','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(158,'America/North_Dakota/Center','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(159,'America/North_Dakota/New_Salem','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(160,'America/Ojinaga','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(161,'America/Panama','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(162,'America/Pangnirtung','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(163,'America/Paramaribo','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(164,'America/Phoenix','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(165,'America/Port-au-Prince','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(166,'America/Port_of_Spain','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(167,'America/Porto_Velho','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(168,'America/Puerto_Rico','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(169,'America/Rainy_River','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(170,'America/Rankin_Inlet','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(171,'America/Recife','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(172,'America/Regina','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(173,'America/Resolute','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(174,'America/Rio_Branco','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(175,'America/Santa_Isabel','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(176,'America/Santarem','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(177,'America/Santiago','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(178,'America/Santo_Domingo','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(179,'America/Sao_Paulo','1','UTC/GMT -02:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(180,'America/Scoresbysund','1','UTC/GMT -01:00','2016-02-18 17:37:57','2016-02-18 17:37:57'),(181,'America/Sitka','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(182,'America/St_Barthelemy','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(183,'America/St_Johns','1','UTC/GMT -03:30','2016-02-18 15:07:57','2016-02-18 15:07:57'),(184,'America/St_Kitts','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(185,'America/St_Lucia','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(186,'America/St_Thomas','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(187,'America/St_Vincent','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(188,'America/Swift_Current','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(189,'America/Tegucigalpa','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(190,'America/Thule','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(191,'America/Thunder_Bay','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(192,'America/Tijuana','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(193,'America/Toronto','1','UTC/GMT -05:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(194,'America/Tortola','1','UTC/GMT -04:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(195,'America/Vancouver','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(196,'America/Whitehorse','1','UTC/GMT -08:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(197,'America/Winnipeg','1','UTC/GMT -06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(198,'America/Yakutat','1','UTC/GMT -09:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(199,'America/Yellowknife','1','UTC/GMT -07:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(200,'Antarctica/Casey','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(201,'Antarctica/Davis','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(202,'Antarctica/DumontDUrville','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(203,'Antarctica/Macquarie','1','UTC/GMT +11:00','2016-02-19 17:37:57','2016-02-19 17:37:57'),(204,'Antarctica/Mawson','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(205,'Antarctica/McMurdo','1','UTC/GMT +13:00','2016-02-19 07:37:57','2016-02-19 07:37:57'),(206,'Antarctica/Palmer','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(207,'Antarctica/Rothera','1','UTC/GMT -03:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(208,'Antarctica/Syowa','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(209,'Antarctica/Troll','1','UTC/GMT +00:00','2016-02-18 06:37:57','2016-02-18 06:37:57'),(210,'Antarctica/Vostok','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(211,'Arctic/Longyearbyen','1','UTC/GMT +01:00','2016-02-18 07:37:57','2016-02-18 07:37:57'),(212,'Asia/Aden','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(213,'Asia/Almaty','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(214,'Asia/Amman','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(215,'Asia/Anadyr','1','UTC/GMT +12:00','2016-02-19 06:37:57','2016-02-19 06:37:57'),(216,'Asia/Aqtau','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(217,'Asia/Aqtobe','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(218,'Asia/Ashgabat','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(219,'Asia/Baghdad','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(220,'Asia/Bahrain','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(221,'Asia/Baku','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(222,'Asia/Bangkok','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(223,'Asia/Beirut','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(224,'Asia/Bishkek','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(225,'Asia/Brunei','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(226,'Asia/Chita','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(227,'Asia/Choibalsan','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(228,'Asia/Colombo','1','UTC/GMT +05:30','2016-02-18 12:07:57','2016-02-18 12:07:57'),(229,'Asia/Damascus','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(230,'Asia/Dhaka','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(231,'Asia/Dili','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(232,'Asia/Dubai','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(233,'Asia/Dushanbe','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(234,'Asia/Gaza','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(235,'Asia/Hebron','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(236,'Asia/Ho_Chi_Minh','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(237,'Asia/Hong_Kong','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(238,'Asia/Hovd','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(239,'Asia/Irkutsk','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(240,'Asia/Jakarta','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(241,'Asia/Jayapura','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(242,'Asia/Jerusalem','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(243,'Asia/Kabul','1','UTC/GMT +04:30','2016-02-18 11:07:57','2016-02-18 11:07:57'),(244,'Asia/Kamchatka','1','UTC/GMT +12:00','2016-02-19 06:37:57','2016-02-19 06:37:57'),(245,'Asia/Karachi','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(246,'Asia/Kathmandu','1','UTC/GMT +05:45','2016-02-18 12:22:57','2016-02-18 12:22:57'),(247,'Asia/Khandyga','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(248,'Asia/Kolkata','1','UTC/GMT +05:30','2016-02-18 12:07:57','2016-02-18 12:07:57'),(249,'Asia/Krasnoyarsk','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(250,'Asia/Kuala_Lumpur','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(251,'Asia/Kuching','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(252,'Asia/Kuwait','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(253,'Asia/Macau','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(254,'Asia/Magadan','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(255,'Asia/Makassar','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(256,'Asia/Manila','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(257,'Asia/Muscat','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(258,'Asia/Nicosia','1','UTC/GMT +02:00','2016-02-18 08:37:57','2016-02-18 08:37:57'),(259,'Asia/Novokuznetsk','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(260,'Asia/Novosibirsk','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(261,'Asia/Omsk','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(262,'Asia/Oral','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(263,'Asia/Phnom_Penh','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(264,'Asia/Pontianak','1','UTC/GMT +07:00','2016-02-18 13:37:57','2016-02-18 13:37:57'),(265,'Asia/Pyongyang','1','UTC/GMT +08:30','2016-02-18 15:07:57','2016-02-18 15:07:57'),(266,'Asia/Qatar','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(267,'Asia/Qyzylorda','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(268,'Asia/Rangoon','1','UTC/GMT +06:30','2016-02-18 13:07:57','2016-02-18 13:07:57'),(269,'Asia/Riyadh','1','UTC/GMT +03:00','2016-02-18 09:37:57','2016-02-18 09:37:57'),(270,'Asia/Sakhalin','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(271,'Asia/Samarkand','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(272,'Asia/Seoul','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(273,'Asia/Shanghai','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(274,'Asia/Singapore','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(275,'Asia/Srednekolymsk','1','UTC/GMT +11:00','2016-02-19 17:37:57','2016-02-19 17:37:57'),(276,'Asia/Taipei','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(277,'Asia/Tashkent','1','UTC/GMT +05:00','2016-02-18 11:37:57','2016-02-18 11:37:57'),(278,'Asia/Tbilisi','1','UTC/GMT +04:00','2016-02-18 10:37:57','2016-02-18 10:37:57'),(279,'Asia/Tehran','1','UTC/GMT +03:30','2016-02-18 10:07:57','2016-02-18 10:07:57'),(280,'Asia/Thimphu','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(281,'Asia/Tokyo','1','UTC/GMT +09:00','2016-02-18 15:37:57','2016-02-18 15:37:57'),(282,'Asia/Ulaanbaatar','1','UTC/GMT +08:00','2016-02-18 14:37:57','2016-02-18 14:37:57'),(283,'Asia/Urumqi','1','UTC/GMT +06:00','2016-02-18 12:37:57','2016-02-18 12:37:57'),(284,'Asia/Ust-Nera','1','UTC/GMT +10:00','2016-02-18 16:37:57','2016-02-18 16:37:57'),(285,'Asia/Vientiane','1','UTC/GMT +07:00','2016-02-18 13:37:58','2016-02-18 13:37:58'),(286,'Asia/Vladivostok','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(287,'Asia/Yakutsk','1','UTC/GMT +09:00','2016-02-18 15:37:58','2016-02-18 15:37:58'),(288,'Asia/Yekaterinburg','1','UTC/GMT +05:00','2016-02-18 11:37:58','2016-02-18 11:37:58'),(289,'Asia/Yerevan','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(290,'Atlantic/Azores','1','UTC/GMT -01:00','2016-02-18 17:37:58','2016-02-18 17:37:58'),(291,'Atlantic/Bermuda','1','UTC/GMT -04:00','2016-02-18 14:37:58','2016-02-18 14:37:58'),(292,'Atlantic/Canary','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(293,'Atlantic/Cape_Verde','1','UTC/GMT -01:00','2016-02-18 17:37:58','2016-02-18 17:37:58'),(294,'Atlantic/Faroe','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(295,'Atlantic/Madeira','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(296,'Atlantic/Reykjavik','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(297,'Atlantic/South_Georgia','1','UTC/GMT -02:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(298,'Atlantic/St_Helena','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(299,'Atlantic/Stanley','1','UTC/GMT -03:00','2016-02-18 15:37:58','2016-02-18 15:37:58'),(300,'Australia/Adelaide','1','UTC/GMT +10:30','2016-02-19 17:07:58','2016-02-19 17:07:58'),(301,'Australia/Brisbane','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(302,'Australia/Broken_Hill','1','UTC/GMT +10:30','2016-02-19 17:07:58','2016-02-19 17:07:58'),(303,'Australia/Currie','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(304,'Australia/Darwin','1','UTC/GMT +09:30','2016-02-18 16:07:58','2016-02-18 16:07:58'),(305,'Australia/Eucla','1','UTC/GMT +08:45','2016-02-18 15:22:58','2016-02-18 15:22:58'),(306,'Australia/Hobart','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(307,'Australia/Lindeman','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(308,'Australia/Lord_Howe','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(309,'Australia/Melbourne','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(310,'Australia/Perth','1','UTC/GMT +08:00','2016-02-18 14:37:58','2016-02-18 14:37:58'),(311,'Australia/Sydney','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(312,'Europe/Amsterdam','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(313,'Europe/Andorra','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(314,'Europe/Athens','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(315,'Europe/Belgrade','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(316,'Europe/Berlin','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(317,'Europe/Bratislava','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(318,'Europe/Brussels','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(319,'Europe/Bucharest','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(320,'Europe/Budapest','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(321,'Europe/Busingen','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(322,'Europe/Chisinau','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(323,'Europe/Copenhagen','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(324,'Europe/Dublin','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(325,'Europe/Gibraltar','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(326,'Europe/Guernsey','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(327,'Europe/Helsinki','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(328,'Europe/Isle_of_Man','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(329,'Europe/Istanbul','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(330,'Europe/Jersey','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(331,'Europe/Kaliningrad','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(332,'Europe/Kiev','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(333,'Europe/Lisbon','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(334,'Europe/Ljubljana','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(335,'Europe/London','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58'),(336,'Europe/Luxembourg','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(337,'Europe/Madrid','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(338,'Europe/Malta','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(339,'Europe/Mariehamn','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(340,'Europe/Minsk','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(341,'Europe/Monaco','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(342,'Europe/Moscow','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(343,'Europe/Oslo','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(344,'Europe/Paris','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(345,'Europe/Podgorica','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(346,'Europe/Prague','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(347,'Europe/Riga','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(348,'Europe/Rome','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(349,'Europe/Samara','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(350,'Europe/San_Marino','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(351,'Europe/Sarajevo','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(352,'Europe/Simferopol','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(353,'Europe/Skopje','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(354,'Europe/Sofia','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(355,'Europe/Stockholm','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(356,'Europe/Tallinn','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(357,'Europe/Tirane','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(358,'Europe/Uzhgorod','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(359,'Europe/Vaduz','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(360,'Europe/Vatican','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(361,'Europe/Vienna','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(362,'Europe/Vilnius','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(363,'Europe/Volgograd','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(364,'Europe/Warsaw','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(365,'Europe/Zagreb','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(366,'Europe/Zaporozhye','1','UTC/GMT +02:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(367,'Europe/Zurich','1','UTC/GMT +01:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(368,'Indian/Antananarivo','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(369,'Indian/Chagos','1','UTC/GMT +06:00','2016-02-18 12:37:58','2016-02-18 12:37:58'),(370,'Indian/Christmas','1','UTC/GMT +07:00','2016-02-18 13:37:58','2016-02-18 13:37:58'),(371,'Indian/Cocos','1','UTC/GMT +06:30','2016-02-18 13:07:58','2016-02-18 13:07:58'),(372,'Indian/Comoro','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(373,'Indian/Kerguelen','1','UTC/GMT +05:00','2016-02-18 11:37:58','2016-02-18 11:37:58'),(374,'Indian/Mahe','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(375,'Indian/Maldives','1','UTC/GMT +05:00','2016-02-18 11:37:58','2016-02-18 11:37:58'),(376,'Indian/Mauritius','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(377,'Indian/Mayotte','1','UTC/GMT +03:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(378,'Indian/Reunion','1','UTC/GMT +04:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(379,'Pacific/Apia','1','UTC/GMT +14:00','2016-02-19 08:37:58','2016-02-19 08:37:58'),(380,'Pacific/Auckland','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(381,'Pacific/Bougainville','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(382,'Pacific/Chatham','1','UTC/GMT +13:45','2016-02-19 08:22:58','2016-02-19 08:22:58'),(383,'Pacific/Chuuk','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(384,'Pacific/Easter','1','UTC/GMT -05:00','2016-02-18 13:37:58','2016-02-18 13:37:58'),(385,'Pacific/Efate','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(386,'Pacific/Enderbury','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(387,'Pacific/Fakaofo','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(388,'Pacific/Fiji','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(389,'Pacific/Funafuti','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(390,'Pacific/Galapagos','1','UTC/GMT -06:00','2016-02-18 12:37:58','2016-02-18 12:37:58'),(391,'Pacific/Gambier','1','UTC/GMT -09:00','2016-02-18 09:37:58','2016-02-18 09:37:58'),(392,'Pacific/Guadalcanal','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(393,'Pacific/Guam','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(394,'Pacific/Honolulu','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(395,'Pacific/Johnston','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(396,'Pacific/Kiritimati','1','UTC/GMT +14:00','2016-02-19 08:37:58','2016-02-19 08:37:58'),(397,'Pacific/Kosrae','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(398,'Pacific/Kwajalein','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(399,'Pacific/Majuro','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(400,'Pacific/Marquesas','1','UTC/GMT -09:30','2016-02-18 09:07:58','2016-02-18 09:07:58'),(401,'Pacific/Midway','1','UTC/GMT -11:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(402,'Pacific/Nauru','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(403,'Pacific/Niue','1','UTC/GMT -11:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(404,'Pacific/Norfolk','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(405,'Pacific/Noumea','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(406,'Pacific/Pago_Pago','1','UTC/GMT -11:00','2016-02-18 07:37:58','2016-02-18 07:37:58'),(407,'Pacific/Palau','1','UTC/GMT +09:00','2016-02-18 15:37:58','2016-02-18 15:37:58'),(408,'Pacific/Pitcairn','1','UTC/GMT -08:00','2016-02-18 10:37:58','2016-02-18 10:37:58'),(409,'Pacific/Pohnpei','1','UTC/GMT +11:00','2016-02-19 17:37:58','2016-02-19 17:37:58'),(410,'Pacific/Port_Moresby','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(411,'Pacific/Rarotonga','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(412,'Pacific/Saipan','1','UTC/GMT +10:00','2016-02-18 16:37:58','2016-02-18 16:37:58'),(413,'Pacific/Tahiti','1','UTC/GMT -10:00','2016-02-18 08:37:58','2016-02-18 08:37:58'),(414,'Pacific/Tarawa','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(415,'Pacific/Tongatapu','1','UTC/GMT +13:00','2016-02-19 07:37:58','2016-02-19 07:37:58'),(416,'Pacific/Wake','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(417,'Pacific/Wallis','1','UTC/GMT +12:00','2016-02-19 06:37:58','2016-02-19 06:37:58'),(418,'UTC','1','UTC/GMT +00:00','2016-02-18 06:37:58','2016-02-18 06:37:58');
/*!40000 ALTER TABLE `mst_timezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_uom`
--

DROP TABLE IF EXISTS `mst_uom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_uom` (
  `uomid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uomid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_uom`
--

LOCK TABLES `mst_uom` WRITE;
/*!40000 ALTER TABLE `mst_uom` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_uom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_useraction`
--

DROP TABLE IF EXISTS `mst_useraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_useraction` (
  `actionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_useraction`
--

LOCK TABLES `mst_useraction` WRITE;
/*!40000 ALTER TABLE `mst_useraction` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_useraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_userlogin`
--

DROP TABLE IF EXISTS `mst_userlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userlogin` (
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorizedpass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ftlogin` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `timezoneid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secquiz1` int(11) DEFAULT NULL,
  `secquiz2` int(11) DEFAULT NULL,
  `secquiz3` int(11) DEFAULT NULL,
  `secans1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visualnoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `audionoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `industry` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userlogin`
--

LOCK TABLES `mst_userlogin` WRITE;
/*!40000 ALTER TABLE `mst_userlogin` DISABLE KEYS */;
INSERT INTO `mst_userlogin` VALUES ('AM',6,'FFRR-2029','$2y$10$31djmJFM8Jl/96Bu43ydbO9BtyDdRr4m3BzQnHKG03d71QAijYVx6','$2y$10$gF/KDna6WOmgUXVShYnF2.G.rX8ld0qnxUnh.KC6lyLNjyftyJMwG','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','1','2','2','2016-02-18 10:06:28','hqRNpGDZF5UPZedZl7qAD0B3xgM8IAKHcAlPdgPvCYFVMIypB51MwEQebJcy','2016-02-18 15:06:28','2016-02-18 22:39:46'),('AM',1,'FMTB-5098','$2y$10$ydooDqsgUeQ7pffZbAEFz.dB1Xv0R84pAthn1V1zQ8jOuKGekyDEC','$2y$10$vDTeMSSt.RBYd0lLNLMTWe028K.pFrCVg9HDc1yQ0IsLrrjHChe.O','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','1','1','2016-02-18 10:04:10','QtgpKCF5nQhE7QIstUbH0T37rfYFDtQtEaHDH4yAaOBccfU5g2cqnMQ9prDF','2016-02-18 15:04:10','2016-02-18 21:50:32'),('TM',3,'IBOX-8877','$2y$10$r/tpXERlZqprX7VBORIBKuyipRGZXdjMlvTrhNcIVULsB99Ej5Sfe','$2y$10$PtaLbFyzDcEw55GOB/bYbeVUfG3sgt0LIlnqKgZcexHW.KV51QtvK','0','N','Atlantic/Azores',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','3','1','1','1','2016-02-18 12:36:28',NULL,'2016-02-18 17:36:28','2016-02-18 17:36:28'),('AD',4,'NTVZ-5516','$2y$10$9lj6cGN6Y7wjJCAKhersjuwVD4QqiUvUri64gLADevEc8ZQI7jMlO','$2y$10$eJpalnXnPVFUMOuPcl4u2OBEP1s.uCP4mtDqyuDK3/aiuUOUFoeuW','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','1','1','2016-02-18 10:05:50',NULL,'2016-02-18 15:05:50','2016-02-18 15:05:50'),('QF',2,'TONO-1588','$2y$10$NKQtR98UJJgasi4UexUb4eLyA2YtSWc1yoRcSFq2uLwVhF3tszMqC','$2y$10$NvBmkOs3IMuGRMGI09KJvu1WFCuByfXsbyj/NStU9E6jYTyG7KC9y','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','2','2','1','2016-02-18 10:04:44',NULL,'2016-02-18 15:04:44','2016-02-18 15:04:44'),('TM',3,'TQBU-0602','$2y$10$Lb.J2rfaunAa0m4gMjG5Z.1MyPJ8aSfJa0WRKJCDTsKKscuElW3Se','$2y$10$FNsOhdANnV34GIEOwiVjRemjJCbO2ZHloCcsW4.4Kleoi5y16Hs3C','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','2','1','2016-02-18 10:05:17',NULL,'2016-02-18 15:05:17','2016-02-18 15:05:17'),('AD',5,'UUZZ-9984','$2y$10$il5jzmicQ1/Wi0/DoToRs.znWiBLifDSVldsPs8DyvNYaKOLFrUVa','$2y$10$UOJyvHIxIg9IlhvjER38VeUcMy04Sm4AZdwBHrcBcbk1s2fIQRO9m','1','Y','Asia/Kolkata',6,8,3,'father','mother','animal','1','1',NULL,NULL,NULL,NULL,'2016-02-17 16:01:00','l6agObfuGu7yWRGT24zLWW7WVACcONyeVkrMhHd1Y8eSCS141LzECgQG0VAJ','2016-02-17 21:01:00','2016-02-19 21:43:52');
/*!40000 ALTER TABLE `mst_userlogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_userlogon`
--

DROP TABLE IF EXISTS `mst_userlogon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userlogon` (
  `logonid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `starttime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `finishtime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logondate` date NOT NULL,
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`logonid`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userlogon`
--

LOCK TABLES `mst_userlogon` WRITE;
/*!40000 ALTER TABLE `mst_userlogon` DISABLE KEYS */;
INSERT INTO `mst_userlogon` VALUES (1,'UUZZ-9984','122.168.204.142','2016-02-18 14:35:22pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 19:35:22','2016-02-18 19:35:22'),(2,'UUZZ-9984','122.168.204.142','2016-02-18 15:34:47pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 20:34:47','2016-02-18 20:34:47'),(3,'UUZZ-9984','123.236.192.153','2016-02-18 16:48:30pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 21:48:30','2016-02-18 21:48:30'),(4,'FFRR-2029','113.193.106.219','2016-02-18 16:49:51pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AM','2016-02-18 21:49:51','2016-02-18 21:49:51'),(5,'FMTB-5098','113.193.106.219','2016-02-18 16:50:20pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AM','2016-02-18 21:50:20','2016-02-18 21:50:20'),(6,'UUZZ-9984','113.193.106.219','2016-02-18 16:50:38pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 21:50:38','2016-02-18 21:50:38'),(7,'UUZZ-9984','113.193.106.219','2016-02-18 16:53:26pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 21:53:26','2016-02-18 21:53:26'),(8,'UUZZ-9984','123.236.192.153','2016-02-18 17:11:59pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 22:11:59','2016-02-18 22:11:59'),(9,'UUZZ-9984','113.193.106.219','2016-02-18 17:12:19pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 22:12:19','2016-02-18 22:12:19'),(10,'FFRR-2029','113.193.106.219','2016-02-18 17:22:51pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AM','2016-02-18 22:22:51','2016-02-18 22:22:51'),(11,'UUZZ-9984','113.193.106.219','2016-02-18 17:23:26pm','','XKBf7VHLSdVdXh8F8HfCq0vozK55yXv4xagBw7cF','2016-02-18','AD','2016-02-18 22:23:26','2016-02-18 22:23:26'),(12,'FFRR-2029','123.236.192.153','2016-02-18 17:39:33pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AM','2016-02-18 22:39:33','2016-02-18 22:39:33'),(13,'UUZZ-9984','123.236.192.153','2016-02-18 18:14:56pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 23:14:56','2016-02-18 23:14:56'),(14,'UUZZ-9984','66.49.252.55','2016-02-18 18:15:50pm','','ONd4P1yA8P4seqPA9mMjQ8FQtJgHyrfTV24sFDU4','2016-02-18','AD','2016-02-18 23:15:50','2016-02-18 23:15:50'),(15,'UUZZ-9984','123.236.192.153','2016-02-18 18:27:37pm','','sh8qakJZC2SS63fuFlzKlFHyjYR0AxJrHLuTssrh','2016-02-18','AD','2016-02-18 23:27:37','2016-02-18 23:27:37'),(16,'UUZZ-9984','123.136.199.120','2016-02-19 08:02:52am','','7ooWdGhhTtI9oXb2w6T04hvifSq8Nz4aBRt81ELk','2016-02-19','AD','2016-02-19 13:02:52','2016-02-19 13:02:52'),(17,'UUZZ-9984','66.49.252.55','2016-02-19 16:43:23pm','','Ptrc5OCm21936BSXNw3dXa1phyBBr0JftMoga0fz','2016-02-19','AD','2016-02-19 21:43:23','2016-02-19 21:43:23'),(18,'UUZZ-9984','123.236.192.153','2016-02-19 17:05:18pm','','baImqtW10R0DPj378azSX0xIXjLmHFUFRDYux8e6','2016-02-19','AD','2016-02-19 22:05:18','2016-02-19 22:05:18'),(19,'UUZZ-9984','66.49.241.175','2016-02-19 17:23:00pm','','PtS9IGs9LxRxnRAYqKucwxfN0wX523J5dPLxbZni','2016-02-19','AD','2016-02-19 22:23:00','2016-02-19 22:23:00'),(20,'UUZZ-9984','66.49.241.175','2016-02-22 13:41:26pm','','Z0LuZDOYvxiEF6kfZh9FzFPaheUMpyYJ0CqolCGs','2016-02-22','AD','2016-02-22 18:41:26','2016-02-22 18:41:26'),(21,'UUZZ-9984','171.49.153.186','2016-02-22 14:22:34pm','','ehO6rRMr3Qr1XYs9VujC9NQIKhHqBZAv4ukF53Wx','2016-02-22','AD','2016-02-22 19:22:34','2016-02-22 19:22:34'),(22,'UUZZ-9984','105.96.211.149','2016-02-22 17:27:15pm','','Fi308rR75TjBx68lhdr8cRvPS7ZLUzKBKa2XOyf2','2016-02-22','AD','2016-02-22 22:27:15','2016-02-22 22:27:15'),(23,'UUZZ-9984','123.236.192.153','2016-02-22 17:28:32pm','','cMTOUoq4FmnkQoIcfZ4IIrk4YtyFAi6nCOE6x2Vg','2016-02-22','AD','2016-02-22 22:28:32','2016-02-22 22:28:32'),(24,'UUZZ-9984','171.49.153.186','2016-02-23 07:07:39am','','BcgmfAHgPCwUQEBD2zsYWOcx01yaJdFinAHTNo5p','2016-02-23','AD','2016-02-23 12:07:39','2016-02-23 12:07:39'),(25,'UUZZ-9984','113.193.105.162','2016-02-23 15:36:41pm','','I3DkhD03PwRxGl7xlgIxsm87SC4nVKE07xLzFeiF','2016-02-23','AD','2016-02-23 20:36:41','2016-02-23 20:36:41');
/*!40000 ALTER TABLE `mst_userlogon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_userrole`
--

DROP TABLE IF EXISTS `mst_userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userrole` (
  `roleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `rolename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userrole`
--

LOCK TABLES `mst_userrole` WRITE;
/*!40000 ALTER TABLE `mst_userrole` DISABLE KEYS */;
INSERT INTO `mst_userrole` VALUES (1,'Account Manager','1','AM','Account Manager'),(2,'Quote Facilitator','1','QF','Quote Facilitator'),(3,'Trigger Man','1','TM','Trigger Man'),(4,'Administrator','1','AD','Sub Administrator'),(5,'Administrator','1','AD','Administrator'),(6,'Account Manager','1','AM','Partner');
/*!40000 ALTER TABLE `mst_userrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `old_useractivitypurge`
--

DROP TABLE IF EXISTS `old_useractivitypurge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `old_useractivitypurge` (
  `actionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `old_useractivitypurge`
--

LOCK TABLES `old_useractivitypurge` WRITE;
/*!40000 ALTER TABLE `old_useractivitypurge` DISABLE KEYS */;
/*!40000 ALTER TABLE `old_useractivitypurge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_master`
--

DROP TABLE IF EXISTS `page_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_master` (
  `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_show_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_path` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_master`
--

LOCK TABLES `page_master` WRITE;
/*!40000 ALTER TABLE `page_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_notification`
--

DROP TABLE IF EXISTS `page_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_notification` (
  `n_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `p_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `n_variable` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `final_noti` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `n_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_notification`
--

LOCK TABLES `page_notification` WRITE;
/*!40000 ALTER TABLE `page_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_advertisment`
--

DROP TABLE IF EXISTS `post_advertisment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_advertisment` (
  `postid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pdate` datetime NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `industry` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ptype` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerrefno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pakaging` text COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `targetprice` int(11) NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `tempid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `archivepost` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `buyer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotefacility` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `act_inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`postid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_advertisment`
--

LOCK TABLES `post_advertisment` WRITE;
/*!40000 ALTER TABLE `post_advertisment` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_advertisment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_customer`
--

DROP TABLE IF EXISTS `post_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_customer` (
  `custid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `edate` date NOT NULL,
  `refuserid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `adduserid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_customer`
--

LOCK TABLES `post_customer` WRITE;
/*!40000 ALTER TABLE `post_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_quotation`
--

DROP TABLE IF EXISTS `post_quotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_quotation` (
  `quoteid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` datetime NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `declinemsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offerdeclinemsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptmsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offercrdate` datetime NOT NULL,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quationstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `offerstatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `accdate` date DEFAULT NULL,
  `rejdate` date DEFAULT NULL,
  `lastcntdate` date DEFAULT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `counetr_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `postuserid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `readunreadst` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`quoteid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_quotation`
--

LOCK TABLES `post_quotation` WRITE;
/*!40000 ALTER TABLE `post_quotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_quotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productimport`
--

DROP TABLE IF EXISTS `productimport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productimport` (
  `productid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` text COLLATE utf8_unicode_ci,
  `packaging` text COLLATE utf8_unicode_ci,
  `shippingcond` text COLLATE utf8_unicode_ci,
  `qtypercase` bigint(20) DEFAULT NULL,
  `des` text COLLATE utf8_unicode_ci,
  `mpm` double(8,2) DEFAULT NULL,
  `prefixone` text COLLATE utf8_unicode_ci,
  `codeone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixtwo` tinyint(4) DEFAULT NULL,
  `codetwo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixthree` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codethree` text COLLATE utf8_unicode_ci,
  `img1` text COLLATE utf8_unicode_ci,
  `img2` text COLLATE utf8_unicode_ci,
  `img3` text COLLATE utf8_unicode_ci,
  `img4` text COLLATE utf8_unicode_ci,
  `img5` text COLLATE utf8_unicode_ci,
  `img6` text COLLATE utf8_unicode_ci,
  `img7` text COLLATE utf8_unicode_ci,
  `img8` text COLLATE utf8_unicode_ci,
  `addedby` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productimport`
--

LOCK TABLES `productimport` WRITE;
/*!40000 ALTER TABLE `productimport` DISABLE KEYS */;
/*!40000 ALTER TABLE `productimport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotation_counter`
--

DROP TABLE IF EXISTS `quotation_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotation_counter` (
  `counterid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_date` datetime NOT NULL,
  `counter_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `counter_quantity` int(11) NOT NULL,
  `counter_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counetr_status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`counterid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotation_counter`
--

LOCK TABLES `quotation_counter` WRITE;
/*!40000 ALTER TABLE `quotation_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terms_condition`
--

DROP TABLE IF EXISTS `terms_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terms_condition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trems_condition` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terms_condition`
--

LOCK TABLES `terms_condition` WRITE;
/*!40000 ALTER TABLE `terms_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `terms_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ind_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Index_val` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test1`
--

DROP TABLE IF EXISTS `test1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `industry` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test1`
--

LOCK TABLES `test1` WRITE;
/*!40000 ALTER TABLE `test1` DISABLE KEYS */;
/*!40000 ALTER TABLE `test1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tm_accepted_offers`
--

DROP TABLE IF EXISTS `tm_accepted_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_accepted_offers` (
  `acc_offr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `counterid` int(11) NOT NULL,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_date` datetime NOT NULL,
  `acc_price` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_quantity` int(11) NOT NULL,
  `acc_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `acc_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noti_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tm_userID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `offerType` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`acc_offr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tm_accepted_offers`
--

LOCK TABLES `tm_accepted_offers` WRITE;
/*!40000 ALTER TABLE `tm_accepted_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tm_accepted_offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-23 10:56:00
