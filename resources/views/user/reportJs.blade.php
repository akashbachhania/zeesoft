<script type="text/javascript">
	$(document).ready(function() {

	   var productno = "{{ \Request::input('productno') }}";
	   var industryid = "{{ \Request::input('industryid') }}";
	   var postno = "{{ \Request::input('postno') }}";
	   var refno = "{{ \Request::input('refno') }}";
	  
	   var industryid_id = "{{ \Request::input('industryid_id') }}";
	   var view_type = "{{ \Request::input('view_type') }}";
	   var industry_id = $('input[name="industry_id"]').val();
	   var sec_name = $('input[name="sec_name"]').val();
	   
       var clauseby = $('input[name="clauseby"]').val();
	   var lastfilter =$('input[name="lastfilter"]').val();

	    $('#datatable_ajax1').DataTable( {
	              "ajax": {
	                  "url": root + "/user/reports_ajax",
	                  dataSrc: "",
	                  "type": 'POST',
	                  "data":{
	                  	"industry_id":industry_id,
	                  	"sec_name":sec_name,
	                  	"dataClauseby":clauseby,
	                  	"lastfilter":lastfilter,
	                  	"productno":productno,
	                  	"industryid":industryid,
	                  	"postno":postno,
	                  	"refno":refno,
	                  	"view_type":"detailed"
	                  }

	              // ajax source
	              } ,
	              columns: [
	                { data: 'product_name' },
	                { data: 'productid' },
	                { data: 'industryname' },
	                { data: 'categoryname' },
	                { data: 'subcategoryname' },
	                { data: 'brandname' },
	                { data: 'ptype' },
	                { data: 'targetprice' },
	                { data: 'currency' },
	                { data: 'quantity' },
	                { data: 'location' },
	                { data: 'post_advertisment_userid' },
	                { data: 'pdate' },
	                { data: 'post_advertisment_pstatus' },
	                { data: 'postno' }
	              ]
	    } );
	});
</script>