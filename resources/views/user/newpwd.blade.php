<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset("/admin-lte/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("admin-lte/dist/css/AdminLTE.min.css") }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset("/admin-lte/plugins/iCheck/square/blue.css") }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="dashboard" style="color: #FF0000"><b style="color: #fff">Ziki</b>&nbsp;<b>Trade</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body" id="login-form">
        <p class="login-box-msg">Change Password</p>
        {!! Form::open(array('url' => 'changepassword','class'=>'form')) !!}
          <input class="form-control" type="hidden" name="userid" value="{{ $userid }}" /> 
          
          <div class="form-group has-feedback">
            
           <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Enter New Password" name="npwd" id="npwd" required="required" />
           <span class="er"></span>
          </div>
          <div class="form-group has-feedback">
            
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm New Password" name="cnpwd" id="cnpwd" required="required"/>
           <span class="err"></span> 
          </div>
          <div class="row">
            <div class="col-xs-8">
            </div><!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" name="rsubmit" id="rsubmit" class="btn btn-primary"> Submit</button>
            </div><!-- /.col -->
          </div>
        {!! Form::close() !!}

      </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->



    <!-- jQuery 2.1.4 -->
    <script src="{{asset("/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset("/admin-lte/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{asset("/admin-lte/plugins/iCheck/icheck.min.js")}}"></script>
    <script>
      // $(function () {
      //   $('input').iCheck({
      //     checkboxClass: 'icheckbox_square-blue',
      //     radioClass: 'iradio_square-blue',
      //     increaseArea: '20%' // optional
      //   });
      // });
    </script>
    <script src="{{asset("/js/jquery.backstrech.min.js")}}"></script>
    <script type="text/javascript">
  
      jQuery(document).ready(function(){
         $.backstretch([

          <?php 
              foreach ($image as $value) {
                echo "'".url('media/bg') . '/' . $value->name."',";
              }

          ?>

          ], {

            fade: 1000,

            duration: 6000

          }

          );
      });

    </script>
    <script type="text/javascript">
      $('#rsubmit').on('click',function(event){
          event.preventDefault();
          $('#npwd').on('keyup',function(){
            $('.er').html('');
          });
          $('#cnpwd').on('keyup',function(){
            $('.err').html(''); 
          });

          if( $('#npwd').val() == '' ){
            $('.er').css('color','red');
            $('.er').html('New Password is Required');
          }
          if( $('#cnpwd').val() == '' ){
            $('.err').css('color','red');
            $('.err').html('Confirm Password is Required');
          }
          if( $('#npwd').val() != $('#cnpwd').val() ){
            $('.err').css('color','red');
            $('.err').html('Please Enter The Same Value Again');
          }
          else{
              $('form').submit();            
          }

          
      });
    </script>
  </body>
</html>


