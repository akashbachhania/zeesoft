@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-md-4">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">User Settings</h3>
        </div><!-- /.box-header -->
        <!-- form start -->

        <form action="{{ url('update') }}" class="form-horizontal" method="post" id="update_user">
          <div class="box-body">
            <div class="form-group">
              
              <label for="userid" class="col-sm-3 control-label">User Id</label>
              
              <div class="col-sm-7">
              <input class="form-control" type="hidden" value="{{ \Crypt::decrypt(\Request::segment(2)) }}" name="u_id" /> 
                {!! Form::text('userid', $user->userid, array('class' => 'form-control','placeholder' => '(System Generated)','required','id'=>'userid','readonly'))!!}
              
              </div>
            
            </div>

            <div class="form-group">
              <label for="role" class="col-sm-3 control-label">User Role</label>
            
              <div class="col-sm-7">
                <select class="form-control" id="role" name="role"> 
                  <option value="0">Select User Role</option>
                  @foreach($userrole as $role)
                    <option value="{{ $role->roleid }}" {{ ($user->roleid == $role->roleid) ? 'selected=selected' :''}}>{{ $role->rolename }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="timezone" class="col-sm-3 control-label">Time Zone</label>
            
              <div class="col-sm-7">
                <select class="form-control" id="timezone" name="timezone">
                  @foreach($timezone as $zone)
                    <option value="{{ $zone->name }}" {{ ($user->timezoneid == $zone->name) ? 'selected=selected' :''}}>({{ $zone->timezonevalue }}) {{ $zone->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>

             <div class="form-group">
            
              <label for="pass" class="col-sm-3 control-label">Password</label>
            
              <div class="col-sm-7">
                
                {!! Form::password('pass',array('class'=>'form-control','maxlength'=>'15','placeholder'=>'Enter New Password','id'=>'pass','onchange'=>'checkPassLength()')) !!}

              </div>

            </div>

            <div class="form-group">

              <label for="conpass" class="col-sm-3 control-label">Confirm Password</label>

              <div class="col-sm-7">

                {!! Form::password('conpass',array('class'=>'form-control','maxlength'=>'15','placeholder'=>'Re-Enter Password','id'=>'conpass','onchange'=>'checkPass()')) !!}

              </div>

            </div>

            <div class="form-group">

              <label for="status" class="col-sm-3 control-label">Status</label>

              <div class="col-sm-7">

                {!! Form::select('status',array('1'=>'Enable','0'=>'Disabled'),null,array('class'=>'form-control','id'=>'status')) !!}

              </div>

            </div>

             <div class="form-group">

              <label for="indmultipleSelect" class="col-sm-3 control-label">Territory</label>

              <div class="col-sm-7">

                <select multiple="" class="form-control industry" id="indmultipleSelect" name="selectedindustry[]">
                  <option value="">Select Industries</option>
                  @foreach( $result as $ind )

                      <option value="{{ $ind->industryid }}" {{ (in_array($ind->industryid,explode(',',$user->industry)) ? 'selected="selected"':'') }}>{{ $ind->name }}</option>

                  @endforeach
                </select>

              </div>

            </div>

            <div class="form-group">
              <label for="cat" class="col-sm-3 control-label"></label>
              <div class="col-sm-7">
                <select multiple="" class="form-control category" id="cat" name="selectcategory[]">
                  <option>Select Categories</option>
                  @if( !empty($user->industry) )
                    @foreach( $usercat as $ucat )
                      <option value="{{ $ucat->catid }}" {{ (in_array($ucat->catid,explode(',',$user->category)) ? 'selected="selected"':'') }}>{{ $ucat->name }}</option>
                    @endforeach

                  @endif
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="subcat" class="col-sm-3 control-label"></label>
              <div class="col-sm-7">
                <select multiple="" class="form-control" id="subcat" name="selectsubcat[]">
                  <option>Select Sub Categories</option>
                  @if( !empty($user->category) )

                    @foreach( $usersubcat as $usc )

                      <option value="{{ $usc->subcatid }}" {{ (in_array($usc->subcatid,explode(',',$user->subcategory)) ? 'selected="selected"':'') }}>{{ $usc->name }}</option>

                    @endforeach

                  @endif
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="brands" class="col-sm-3 control-label"></label>
              <div class="col-sm-7">
                <select multiple="" class="form-control" id="brands" name="selectbrands[]">
                  <option>Select Brands</option>
                  @if( !empty( $user->category ) )
                    @foreach( $userbrand as $ub )

                      <option value="{{ $ub->brandid }}" {{ (in_array($ub->brandid,explode(',',$user->brand)) ? 'selected="selected"':'') }}>{{ $ub->name }}</option>

                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="authpass" class="col-sm-3 control-label">Authorize Changes</label>
              <div class="col-sm-7">

                {!! Form::password('authpass',array('class'=>'form-control','id'=>'authpass','placeholder'=>'Enter Password')) !!}

                {!! Form::hidden('adduser','adduser',array('id'=>'adduser')) !!}

              </div>
            </div>
            @if( Auth::user()->roletype == 'AD' )

            <a href="{{url('userdetails')}}" class="btn btn-default pull-right" style="margin-left: 10px;">Cancel</a>
            {!! Form::submit('Submit',array('class'=>'btn btn-info pull-right','id'=>'submitButton')) !!}
            @else

              @if( $checkVal[0]['EditP']=='1' )
                
                <a href="{{url('userdetails')}}" class="btn btn-default pull-right" style="margin-left: 10px;">Cancel</a>
                {!! Form::submit('Submit',array('class'=>'btn btn-info pull-right','id'=>'submitButton')) !!}

              @endif

            @endif
          </div>
         <!--  <input type="text" name="test">
          <input type="submit" value="Submit" name="submit"> -->
        </form>
      </div><!-- /.box -->
    </div> 

    <div class="col-md-8">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">User Statistics & Reports</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <form method="post" name="reportform" id="reportform">
                <input type="hidden" name="reuserid" id="reuserid" value="{{$user->userid }}" />

                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group row">
                        <label class="col-md-3">Show</label>
                        <div class="col-md-9">
                          <select class="form-control" name="showval" id="showval" tabindex="2">
                            @if($user->roletype =='AD' || $user->roletype =='SA')
                            <option value="all" selected="selected">All</option>
                            @endif

                            @if($user->roletype=='AM')
                            <option value="all" selected="selected">All</option>
                            <option value="activepost">Active postings</option>
                            <option value="cancelpost">Cancelled postings</option>
                            <option value="recvedquote">Received quotes</option>
                            <option value="activecounter">Active counters</option>
                            <option value="completedorder">Completed orders</option>
                            <option value="cancelledorderAm">Cancelled Orders</option>
                            <option value="allcustomers">Customers</option>
                            <option value="activecustomer">Active customers</option>
                            <option value="inactivecustomer">Inactive customers</option>
                            <option value="acceptedquotesAm">Accepted Quotes</option>
                            <option value="declinedquotesAm">Declined Quotes</option>
                            @endif
                            @if( $user->roletype == 'QF' )

                            <option value="allqf" selected="selected">All</option>
                            <option value="completeorderqf">Completed orders</option>
                            <option value="incompleteorder">Incomplete orders</option>
                            <option value="cancelledorder">Cancelled Orders</option>
                            <option value="avtivequote"> Active quotes</option>
                            <option value="activecounterqf">Active counters</option>
                            <option value="acceptedquotes">Accepted Quotes</option>
                            <option value="declinedquotes">Declined Quotes</option>
                            @endif

                            @if( $user->roletype == 'TM' )
                            <option value="all" selected="selected">All</option>
                            @endif
                          </select>
                        </div>  
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group row">
                      <label class="control-label col-md-2">From</label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="from" id="from" readonly="readonly">
                        </div>
                      </div>  
                    </div>
                  </div>
                  <div class="col-md-4" style="margin-right: -10px;">
                    <div class="form-group row">
                      <label class="control-label col-md-1">To</label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="to" id="to" readonly="readonly">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-1">
                    <div class="form-group row">
                      <div class="col-md-12">
                        <button type="button" id="reportgo" name="reportgo" class="btn btn-success m_r20">Go</button>
                      </div> 
                      
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="box box-info" style="border: 1px solid #00c0ef!important">
                <div class="box-header" style="background: #00c0ef;color: #fff;">
                  <i class="fa fa-cogs"></i>
                  <h3 class="box-title">Statistic</h3>
                </div>
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th width="10%"> #</th>
                        <th width="70%">Statistic</th>
                        <th width="20%"> Total</th>
                      </tr>
                    </thead>
                    <tbody id="buypost">
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
@include('user.editJs')
@endsection