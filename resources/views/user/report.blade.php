@extends('layout.master')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					@if( empty( \Request::input('reportbyad') ) )
			  
					   <h3 class="box-title">{{ \Request::input('view_type') . ' By Industry' }}</h3>
                    
                    @else
		               
		               <h3 class="box-title">{{ \Request::input('view_type') . ' By Users' }}</h3>
                		
					@endif
					
				</div>
				<div class="box-body table-responsive">

				    <input type="hidden" name="section" value="{{ \Request::input('view_type') }}" />
					<input type="hidden" name="searchedparam" value="{{ \Request::input('productno') }}" />
					<!-- END PAGE LEVEL STYLES -->
					@if(\Request::input('user_id') )
					<input type="hidden" name="industry_id" value="{{ \Request::input('user_id') }}" />
					@elseif(\Request::input('productno'))
					<input type="hidden" name="industry_id" value="{{ \Request::input('productno') }}" />
					@elseif(\Request::input('postno'))
					<input type="hidden" name="industry_id" value="{{ \Request::input('postno') }}" />
					@else
					<input type="hidden" name="industry_id" value="{{ \Request::input('industryid_id') }}" />
					@endif
					<input type="hidden" name="sec_name" value="{{ \Request::input('view_type') }}" />

					<input type="hidden" name="expandview" id="expandview" value="0"/>
                    <input type="hidden" name="clauseby" id="clauseby" value="{{ \Request::input('clauseby') }}"/>
                    <input type="hidden" name="lastfilter" id="lastfilter" value="{{ \Request::input('q') }}"/>
				  

				  <table class="table table-striped table-bordered table-hover expand_view" id="datatable_ajax1">
                    <thead>
                      
                      @if(\Request::input('view_type')=="Postings") 
                      <tr role="row" class="zeeproduct-heading">
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        
						
                        @elseif(\Request::input('view_type')=="Products")
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Industry </th>
                         <th width="10%" style="text-align:left"> Category </th>
                          <th width="10%" style="text-align:left"> Sub Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Suggestion Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        
                        @elseif(\Request::input('view_type')=="Quotes") 
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                         <th width="10%" style="text-align:left"> QuoteID </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date </th>
                        
                        @elseif(\Request::input('view_type')=="Offers") 
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        <th width="10%" style="text-align:left"> QuoteID </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date </th>
                        
						@elseif(\Request::input('view_type')=="Orders") 
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        
                         <th width="10%" style="text-align:left"> QuoteID </th>
                        <th width="10%" style="text-align:left"> Quote Facilitator </th>
                        <th width="10%" style="text-align:left"> Buyer </th>
                         <th width="10%" style="text-align:left"> Seller </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date</th>
                        
                        @elseif(\Request::input('view_type')=="Customers")
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                         <th width="10%" style="text-align:left"> Customer Ref. No. </th>
                        
                        
						@elseif(\Request::input('view_type')=="Users")
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        
                        @endif
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
				</div>
			</div>
		</div>
	</div>
@include('user.reportJs')
@endsection