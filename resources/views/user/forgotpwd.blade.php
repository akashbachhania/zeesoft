<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset("/admin-lte/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("admin-lte/dist/css/AdminLTE.min.css") }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset("/admin-lte/plugins/iCheck/square/blue.css") }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="dashboard" style="color: #FF0000"><b style="color: #fff">Ziki</b>&nbsp;<b>Trade</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body" id="login-form">
        <p class="login-box-msg">Forgot Password ?</p>
        @if( ($secquiz1 == '') || ($secquiz2 == '') )
        <div class="alert alert-error">
          <span> Security Question not Set, Please Contact your Administrator <a href="index.php">Go Back</a></span> 
        </div>
        @else
        {!! Form::open(array('url' => 'resetpassword','class'=>'form')) !!}
          <input class="form-control" type="hidden" name="userid" value="{{ $userid }}" /> 
            <input class="form-control" type="hidden" name="q1" value="{{ $q1 }}" />
            <input class="form-control" type="hidden" name="q2" value="{{ $q2 }}" />
          <div class="form-group has-feedback">
            <label><b>Q1. </b>{{ $secquiz1 }}</label>
            
            {!! Form::text('answer1', null, array('class' => 'form-control','placeholder' => 'Answer 1','required'))!!}
          
          </div>
          <div class="form-group has-feedback">
            <label><b>Q2. </b>{{ $secquiz2 }}</label>
            {!! Form::text('answer2', null,array('class' => 'form-control','placeholder'=>'Answer 2', 'required')) !!}

          </div>
          <div class="row">
            <div class="col-xs-8">
            </div><!-- /.col -->
            <div class="col-xs-4">
            
                {!! Form::submit('Submit' , array('class' => 'btn btn-primary btn-block btn-flat')) !!}
            </div><!-- /.col -->
          </div>
        {!! Form::close() !!}
        @endif
      </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->



    <!-- jQuery 2.1.4 -->
    <script src="{{asset("/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset("/admin-lte/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{asset("/admin-lte/plugins/iCheck/icheck.min.js")}}"></script>
    <script>
      // $(function () {
      //   $('input').iCheck({
      //     checkboxClass: 'icheckbox_square-blue',
      //     radioClass: 'iradio_square-blue',
      //     increaseArea: '20%' // optional
      //   });
      // });
    </script>
    <script src="{{asset("/js/jquery.backstrech.min.js")}}"></script>
    <script type="text/javascript">
  
      jQuery(document).ready(function(){
         $.backstretch([

          <?php 
              foreach ($image as $value) {
                echo "'".url('media/bg') . '/' . $value->name."',";
              }

          ?>

          ], {

            fade: 1000,

            duration: 6000

          }

          );
      });

    </script>
  </body>
</html>


