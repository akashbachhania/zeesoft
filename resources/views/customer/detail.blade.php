@extends('layout.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Customer List</h3>
			</div>
			<div class="box-body table-responsive">
				<table class="table table-striped table-bordered table-hover" id="customerDetailTable">
					<thead>
						<tr role="row" class="heading">
							<th width="20%">Creation Date</th>

	                        <th width="30%">Customer Reference No.</th>

                            <th width="25%">Uers Id</th>

                            <th width="10%">Status</th>
	                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@include('customer.detailJs')
@endsection