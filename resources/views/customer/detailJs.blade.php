<script type="text/javascript">

    var customerDetailTable = $('#customerDetailTable').DataTable( {

	    "ajax": {
	        "url":root + "/customerDetail_ajax",
	        "dataSrc": "",
	        "type": 'POST',
			"data":{
				id:"{{ \Request::segment(2) }}"
			}
	    },
	    columns: [
	    { data: 'edate' },
	    { data: 'refno' },
	    { data: 'refuserid' },
	    { data: 'msg' }
	    ]
	});

	setInterval( function () {
	    customerDetailTable.ajax.reload();
	}, 3000 );
</script>

