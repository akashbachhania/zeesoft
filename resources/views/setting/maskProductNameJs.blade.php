<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset ("/admin-lte/plugins/iCheck/all.css") }}">
<!-- iCheck 1.0.1 -->
<script src="{{ asset ("/admin-lte/plugins/iCheck/icheck.min.js") }}"></script>
<script type="text/javascript">
	function shownoti(message) {
		$(function() {
			function Toast(type, css, msg) {
				this.type = type;
				this.css = css;
				this.msg = msg;
			}
			var toasts = [
				new Toast('success', 'toast-top-right', message),
			];
			toastr.options.positionClass = 'toast-top-full-width';
			toastr.options.extendedTimeOut = 0; //1000;
			toastr.options.timeOut = 2000;
			toastr.options.fadeOut = 250;
			toastr.options.fadeIn = 250;
			var i = 0;
			/* $('#tryMe').click(function () {

			     $('#tryMe').prop('disabled', true);*/
			delayToasts();
			//});
			function delayToasts() {
				if (i === toasts.length) {
					return;
				}
				var delay = i === 0 ? 0 : 2100;
				window.setTimeout(function() {
					showToast();
				}, delay);
				// re-enable the button        
				if (i === toasts.length - 1) {
					window.setTimeout(function() {
						prop('disabled', false);
						i = 0;
					}, delay + 1000);
				}
			}

			function showToast() {
				var t = toasts[i];
				toastr.options.positionClass = t.css;
				toastr[t.type](t.msg);
				i++;
				delayToasts();
			}
		})
	}
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		// $('input[type="checkbox"].flat-red').iCheck({
  //         checkboxClass: 'icheckbox_flat-green',
  //         radioClass: 'iradio_flat-green'
  //       });

	});
</script>

<script>
    jQuery(document).ready(function() {
        $.ajax({
            url: root + '/setting/maskProductName_ajax',
            type: 'POST',
            data: {
                Type: 'checkMaskQuotes'
            },
			cache: false,
            success: function(data) {
                //called when successful
				// response = jQuery.parseJSON(data);
				if(data.maskstatus=='0')
				{
				$('#maskName').attr("checked", true);
				}
				else
				{
				$('#maskName').attr("checked", false);
				}
            },
            error: function(e) {
                alert('error')
            }
        });
    });
    $('#maskName').click(function() {
        if ($('#maskName').is(':checked')) {
            var maskVal = '0';
        } else {
            var maskVal = '1';
        }
        // alert(maskVal);
        $.ajax({
            url: root + '/setting/maskProductName_ajax',
            type: 'POST',
            data: {
                maskVal: maskVal,
                Type: 'maskQuotes'
            },
            success: function(data) {
                //called when successful
                if (data == '0') {
                    var res5 = "Successfully Mask Product Name";
                    var message5 = res5;
                    shownoti(message5);
                    $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                } else {
                    var res5 = "Successfully Unmask Product Name";
                    var message5 = res5;
                    shownoti(message5);
                    $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                }
            },
            error: function(e) {
                alert('error')
            }
        });
    })
</script>