@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          	<div class="row">
              <div class="col-md-2">
                <h3 class="box-title">Message Center</h3>    
              </div>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                      <div class="form-group">
                         <label class="m_r10">
                           <input type="radio" name="archived" value="UnRead" id="archived" checked="checked">
                           Active
                         </label>
                  
                         <label class="m_r10">
                           <input type="radio" name="archived" value="Read" id="archived">
                           Archived
                         </label>      
                         
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                      @if ( Auth::user()->roletype == 'AD' ) 
                        <button id="statuschange" class="btn btn-success pull-right">Archive</button>
                      @else 
                        
                        @if($checkVal[0]['EditP']=='1')
                          <button id="statuschange" class="btn btn-success pull-right">Archive</button>
                        @endif

                      @endif        
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="box-body">
          	<table id="notificationSettingTable" class="table table-bordered table-striped">
          		<thead>
          			<tr>
          				<th>
          				  <input class="group-checkable" type="checkbox" data-set="#sample_1 .checkboxes">
          				</th>
          				<th>
          				  Date
          				</th>
          				<th>
          				  Notification	
          				</th>
          				<th>
          				  Status	
          				</th>
          			</tr>
          		</thead>
          		<tbody>
          			
          		</tbody>
          	</table>
          </div>

        </div>
    </div>
  </div>
@include('setting.notificationSettingJs')
@endsection