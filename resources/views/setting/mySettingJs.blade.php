<script type="text/javascript">

 function shownoti(message){
	$(function() {
    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];
    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
}
</script>

@if( \Request::input('s') =="loggedin" )

  <script> 
    $(document).ready(function() {
                
        var id = "{{ \Request::input('page') }}";
        var lognoti = "lognoti";
         $.ajax({
            url: root + "/notiAlert",
            type: "post",
            data:{ 'id': id , 'lognoti': lognoti},
            cache: false,
            success: function(res){
            //alert(res);
            
                        $.ajax({
                url: root + "/noti6",
                type: "post",
                data:{id:id},
                cache: false,
                dataType:'html',
                        success: function(res2){
                        //alert(res2);
                            if(res2=='bothclose')
                            {
                            
                            }
                            if(res2=='audioclose')
                            {
                            //alert(res2);
                        var message = res;
                        shownoti(message);
                            }
                            if(res2=='vedioclose')
                            {
                    $('<audio  id="sound" ><source src="" type="audio/ogg"><source src="{{url("assets/notifiles/3/Soft_Velvet_Logo.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/Soft_Velvet_Logo.wav")}}" type="audio/wav"></audio>').appendTo('body');
        
         $('#sound')[0].play(); 
                            }
                        if(res2=='bothstart')
                        {
                        //alert(res2);
                        var message = res;
                        shownoti(message);
                    $('<audio  id="sound" ><source src="" type="audio/ogg"><source src="{{url("assets/notifiles/3/Soft_Velvet_Logo.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/Soft_Velvet_Logo.wav")}}" type="audio/wav"></audio>').appendTo('body');
         $('#sound')[0].play(); 
                        }
                     }
                });
            
            //var message = res;
            //shownoti(message);
            }
    });
    //shownoti("User Created Successfully"); // refresh div after 5 secs
         });
     </script>
@elseif( \Request::input('page') =="ndashboard" )

  <script> 
    $(document).ready(function() {
                
        var id = "{{ \Request::input('page') }}";
        var lognoti = "lognoti";
         $.ajax({
            url: root + "/notiAlert",
            type: "post",
            data:{ 'id': id , 'lognoti': lognoti},
            cache: false,
            success: function(res){
            //alert(res);
            
                        $.ajax({
                url: root + "/noti6",
                type: "post",
                data:{id:id},
                cache: false,
                dataType:'html',
                        success: function(res2){
                        //alert(res2);
                            if(res2=='bothclose')
                            {
                            
                            }
                            if(res2=='audioclose')
                            {
                            //alert(res2);
                        var message = res;
                        shownoti(message);
                            }
                            if(res2=='vedioclose')
                            {
                    $('<audio  id="sound" ><source src="{{url("assets/notifiles/3/0396.ogg")}}" type="audio/ogg"><source src="{{url("assets/notifiles/3/0396.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/0396.wav")}}" type="audio/wav"></audio>').appendTo('body');
        
         $('#sound')[0].play(); 
                            }
                        if(res2=='bothstart')
                        {
                        //alert(res2);
                        var message = res;
                        shownoti(message);
                    $('<audio  id="sound" ><source src="{{url("assets/notifiles/3/0396.ogg")}}" type="audio/ogg"><source src="{{url("assets/notifiles/3/0396.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/0396.wav")}}" type="audio/wav"></audio>').appendTo('body');
         $('#sound')[0].play(); 
                        }
                     }
                });
            
            //var message = res;
            //shownoti(message);
            }
    });
    //shownoti("User Created Successfully"); // refresh div after 5 secs
         });
     </script>
@endif
@if(\Request::input('page') == "nsetting")
            
    <script> 
        $(document).ready(function() {
            
            var id = "{{ \Request::input('page') }}";
            
            var lognoti = "lognoti";
            
            $.ajax({
                url: root + "/notiAlert",
                type: "post",
                data:{ 'id': id , 'lognoti': lognoti},
                cache: false,
                success: function(res){
                //alert(res);
                            $.ajax({
                    url: root + "/noti6",
                    type: "post",
                    data:{id:id},
                    cache: false,
                    dataType:'html',
                            success: function(res2){
                            //alert(res2);
                                if(res2=='bothclose')
                                {
                                
                                }
                                if(res2=='audioclose')
                                {
                                //alert(res2);
                            var message = res;
                            shownoti('Updated My Settings Successully');
                                }
                                if(res2=='vedioclose')
                                {
                        $('<audio  id="sound" ><source src="{{url("assets/notifiles/3/0396.ogg")}}" type="audio/ogg"><source src="{{url("assets/notifiles/3/0396.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/0396.wav")}}" type="audio/wav"></audio>').appendTo('body');
            
             $('#sound')[0].play(); 
                                }
                            if(res2=='bothstart')
                            {
                            //alert(res2);
                            var message = res;
                            shownoti('Updated My Settings Successully');
                        $('<audio  id="sound" ><source src="{{url("assets/notifiles/3/0396.ogg")}}" type="audio/ogg"><source src="{{url("assets/notifiles/3/0396.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/0396.wav")}}" type="audio/wav"></audio>').appendTo('body');
             $('#sound')[0].play(); 
                            }
                         }
                    });
                //var message = res;
                //shownoti(message);
                }
            });
                //shownoti("User Created Successfully"); // refresh div after 5 secs
        });
    </script>
@endif


<script type="text/javascript">
    function checkPasslength(){

      var s = $('#pwd').val();
      // check for null or too short
      if (!s || s.length < 6) {
       return false;
      }
      // check for a number
      if (/[0-9]/.test(s) === false) {
       return false;
      }
      // check for a capital letter
      if ((/[a-z]/.test(s)) === false) {
       return false;
      }
      
      return true; // return true if all conditions are met

    }

    $("#submit-msetting").click(function(event){
        event.preventDefault();
        if($("#timezone").val()=='' || $("#timezone").val()==0){

            alert('Timezone Required');

            $("#timezone").focus();

            return false;

        }

        var pass1 = $("#pwd").val();

        var pass2 = $("#cpwd").val();

        if (pass1 != pass2) {

            alert("Passwords Do not match");

            $("#cpwd").focus();

            return false;

        }

        if((pass1!='') && checkPasslength()==false)
        {
            alert("Pasword must have at least 6 characters and at least one letter and one number."); // prompt user

            $("#pwd").focus(); //set focus back to control

            return false;

        }
        if($("#sque1").val()=='' || $("#sque1").val()==0){

            alert('Security Question 1 Required');

            $("#sque1").focus();

            return false;

        }

        if($("#sans1").val()==''){

            alert('Security Answer 1 Required');

            $("#sans1").focus();

            return false;

        }

        if($("#sque2").val()=='' || $("#sque2").val()==0){

            alert('Security Question 2 Required');

            $("#sque2").focus();

            return false;

        }

        if($("#sans2").val()==''){

            alert('Security Answer 2 Required');

            $("#sans2").focus();

            return false;

        }

        if($("#sque3").val()=='' || $("#sque3").val()==0){

            alert('Security Question 3 Required');

            $("#sque3").focus();

            return false;

        }

        if($("#sans3").val()==''){

            alert('Security Answer 3 Required');

            $("#sans3").focus();

            return false;

        }

        if($("#authpass").val()==''){

            alert('Authorize Password Required');

            $("#authpass").focus();

            return false;

        }

        if($("#authorize").val()!=''){

            $.get(root + '/checkauthpass',{authpass:$("#authpass").val()},function(data){
                if(data != 'success'){
                    alert('Authorization Failed');
                    evet.preventDefault();
                    return false;
                }
                else{   

                    $("#form_settings").submit();                            

                }
            });
        
        }
        return true;

    });

    $("#cancel").click(function(){ location.href="{{ url('dashboard') }}" });
</script>