<script>
    jQuery(document).ready(function() {

		$("#submitButton").click(function(event) {
            event.preventDefault();
			var uploadFile = $("#uploadFile").val();
			var re = /(\.csv)$/i;
			if ($("#itype").val() == '' || $("#itype").val() == 0) {
				alert('Data Type Required');
				return false;
			}
			if ((uploadFile == '') && (!$('#serverchk').checked)) {
				alert('File Upload Required');
				return false;
			}
			if (!re.exec(uploadFile) && (!$('#serverchk').checked))
			{
                alert("File extension not supported!");
                return false;
            }
            if ($("#authchange").val() == '') {
                alert('Authorize Password Required');
                return false;
            }
            if($("#authchange").val() != ''){
               
                $.get(root + '/checkauthpass',{authpass:$("#authchange").val()},function(data){
                    if(data != 'success'){
                        alert('Authorized Password Incorrect!');
                        $("#authchange").parent().parent().addClass('has-error');
                        $("#authchange").focus();
                        return false;
                    }
                    else if( data == 'success' ) {

                        $("#importPostForm").submit();
                    }                    
                });
            }
            
        });
        $("#cancel").click(function() {
            location.href = '{{ url("dashboard") }}';
        });

    });



    function sample()
    {
        var x = $("#itype").val();
        
        if (x == "Brands")
        {
            $('#sample-file').html('<a href="{{ url("sample/brands") }}" >Download Sample File</a>');
        }
        if (x == "Categories")
        {
            $('#sample-file').html('<a href="{{ url("sample/category") }}" >Download Sample File</a>');
        }
        if (x == "Industries")
        {
            $('#sample-file').html('<a href="{{ url("sample/industry") }}" >Download Sample File</a>');
        }
        if (x == "Products")
        {
            $('#sample-file').html('<a href="{{ url("sample/products") }}" >Download Sample File</a>');
        }
        if (x == "Subcategories")
        {
           $('#sample-file').html('<a href="{{ url("sample/sub-category") }}" >Download Sample File</a>');
        }
        if (x == "Posting")
        {
            $('#sample-file').html('<a href="{{ url("posting/sample") }}" >Download Sample File</a>');
        }
        if (x == "Customers")
        {
            var role = "{{ Auth::user()->roletype }}";
            if ((role == "AD") || (role == "SA"))
            {
               $('#sample-file').html('<a href="{{ url("sample/customer") }}" >Download Sample File</a>');
            } else
            {
                $('#sample-file').html('<a href="{{ url("sample/customers") }}" >Download Sample File</a>');
            }
        }
        if (x == "Users")
        {
            $('#sample-file').html('<a href="{{ url("sample/user") }}" >Download Sample File</a>');
        }
    }
	
function showMe (box) {
if($('#serverchk').checked)
	 {
	      $("#uploadFile").disabled = true;
		  
		  //$("#form_importdata").action = "import-pro_server.php";
	 }
else if (!$('#serverchk').checked) 
    {
	     $("#uploadFile").disabled = false;
		// $("#form_importdata").action = "import-pro.php";
    }    
 }

</script>
<script type="text/javascript">
     function shownoti(message){
    $(function() {
    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];
    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
}
</script>

@if( (\Request::input('itype') != '') && (\Request::input('import') == 'successful') )
 <script> 
    $(document).ready(function() {
                
        var id = "{{ \Request::input('import') }}";
        var lognoti = "lognoti";
         $.ajax({
            url: root + "/notiAlert",
            type: "post",
            data:{ 'id': id , 'lognoti': lognoti},
            cache: false,
            success: function(res){
            //alert(res);
            
                        $.ajax({
                url: root + "/noti6",
                type: "post",
                data:{id:id},
                cache: false,
                dataType:'html',
                        success: function(res2){
                        //alert(res2);
                            if(res2=='bothclose')
                            {
                            
                            }
                            if(res2=='audioclose')
                            {
                            // alert(res2);
                        var message = res;
                        shownoti(message);
                            }
                            if(res2=='vedioclose')
                            {
                    $('<audio  id="sound" ><source src="{{url("assets/notifiles/3/0396.ogg")}}" type="audio/ogg"><source src="{{url("assets/notifiles/3/0396.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/0396.wav")}}" type="audio/wav"></audio>').appendTo('body');
        
         $('#sound')[0].play(); 
                            }
                        if(res2=='bothstart')
                        {
                        // alert(res2);

                        var message = "{{ \Request::input('itype') }} successfully Imported";
                        shownoti(message);
                    $('<audio  id="sound" ><source src="{{url("assets/notifiles/3/0396.ogg")}}" type="audio/ogg"><source src="{{url("assets/notifiles/3/0396.mp3")}}" type="audio/mpeg"><source src="{{url("assets/notifiles/3/0396.wav")}}" type="audio/wav"></audio>').appendTo('body');
         $('#sound')[0].play(); 
                        }
                     }
                });
            
            //var message = res;
            //shownoti(message);
            }
    });

  });
 </script>
@endif