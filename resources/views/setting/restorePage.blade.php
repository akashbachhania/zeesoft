@extends('layout.master')

@section('content')
<div class="row">

  <div class="col-md-5">
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
    </div>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Database Restore</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      
      {!! Form::open(array('url'=>'setting/restore','method'=>'POST', 'files'=>true)) !!}
        <div class="box-body" style="margin-left: 30%">
          <div class="form-group">
            <label for="exampleInputFile">DB Restore</label>
            {!! Form::file('sql') !!}
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          {!! Form::submit('Submit', array('class'=>'btn btn-primary pull-right')) !!}
          <!-- <button type="submit" class="btn btn-primary pull-right">Submit</button> -->
        </div>
      {!! Form::close() !!}
    </div>
  </div><!-- /.col -->
</div><!-- /.row -->
@endsection