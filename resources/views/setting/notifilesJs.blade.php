<script type="text/javascript">

 function shownoti(message){
    $(function() {
    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];
    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
}
</script>

<script type="text/javascript">
    <?php if(isset($_GET['upload']) && $_GET['upload'] == 'success'){?>
        var msg = "File uploaded successfully";
        shownoti(msg);
    <?php }elseif(isset($_GET['upload']) && $_GET['upload'] == 'fail'){?>
        var msg = "There is some problem uploading file";
        shownoti(msg);
    <?php }?>
	$(document).ready(function(){
		$('.btnnew').css('cursor','pointer');
	});
	$(".btnnew").on("click", function() {
        var type = $(this).attr('type');
        $('#sound_type').val($.trim(type));
        $('#basic').modal('show');

	});

	$(document).on("click","#btnclose", function() {

	    $('#basic').modal('hide');

	});

	$("#btnsave").on("click", function(event) {
		event.preventDefault();
		if( $('#uploadnoti').val() == '' ){
			alert('Please Upload a File');
			return false;
		}
		if ($("#authchange").val() == '') {
            alert('Authorized code is required');
            $("#authchange").parent().parent().addClass('has-error');
            $("#authchange").focus();
            return false;
        } else {
            $.get(root + '/checkauthpass',{authpass:$("#authchange").val()},function(data){
                if(data == 'success'){
					$('#basic').modal('hide');			
                	$('#changeBannerForm').submit();
                }
                else{
                    alert('Authorized Password Incorrect!');
                    $("#authchange").parent().parent().addClass('has-error');
                    $("#authchange").focus();
                }
                
            });
        }
	});

</script>


<script>
    $(document).ready(function() {
        var audioElement = document.createElement('audio');
        //audioElement.load()

        $.get();

        audioElement.addEventListener("load", function() {
            audioElement.play();
        }, true);

        $('.play').click(function() {
            audioElement.setAttribute('src', "<?php echo URL::to('/assets/notifiles/3/')?>/"+$(this).attr('name'));
            audioElement.play();
        });

        $('.pause').click(function() {
            audioElement.setAttribute('src', "<?php echo URL::to('/assets/notifiles/3/')?>/"+$(this).attr('name'));
            audioElement.pause();
        });
    });
</script>    

