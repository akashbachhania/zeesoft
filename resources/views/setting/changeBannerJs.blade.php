<script type="text/javascript">

	$("#addnewbanner").on("click", function() {

        $('#basic').modal('show');

	});

	$(document).on("click","#btnclose", function() {

	    $('#basic').modal('hide');

	});

	$("#btnsave").on("click", function(event) {
		event.preventDefault();
		if( $('#uploadBanner').val() == '' ){
			alert('Please Upload a Banner');
			return false;
		}
		if ($("#authchange").val() == '') {
            alert('Authorized code is required');
            $("#authchange").parent().parent().addClass('has-error');
            $("#authchange").focus();
            return false;
        } else {
            $.get(root + '/checkauthpass',{authpass:$("#authchange").val()},function(data){
                if(data == 'success'){
					$('#basic').modal('hide');			
                	$('#changeBannerForm').submit();
                }
                else{
                    alert('Authorized Password Incorrect!');
                    $("#authchange").parent().parent().addClass('has-error');
                    $("#authchange").focus();
                }
                
            });
        }
	});

	$('.removeBanner').on('click',function(){
		var id = $(this).attr('id');

		var input = confirm( 'Do you really want to delete this banner?' );
		if( input ){
			$.get(root + '/setting/changeBanner',{id:id,type:'DELETE'},function(data){
				if( data == 'success' ){
					window.location.href = '';
				}
			});
		}
	});

</script>