@extends('layout.master')

@section('content')
	
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success box-solid">
				<div class="box-header">
					<div class="row">
						<div class="col-md-3">
							<h3 class="box-title">Change Banner</h3>		
						</div>
						<div class="col-md-2 col-md-push-7">
							<button class="btn" id="addnewbanner" style="color: #000">Add New</button>
						</div>
					</div>
					
				</div>
				<div class="box-body">
					<?php $i = 0; ?>									
					@foreach( $banner as $image )
					 
					 @if( ($i == 0) || ($i % 4 == 0) )
					 	<div class="row">
					 @endif
					 
					 <div class="col-md-3">
						<div class="box box-success">
			                <div class="box-header with-border">
			                  
			                  <div class="box-tools pull-right">
			                    <button class="btn btn-box-tool removeBanner" id="{{ $image->id }}"><i class="fa fa-times"></i></button>
			                  </div><!-- /.box-tools -->
			                </div><!-- /.box-header -->
			                <div class="box-body">
			                  <img src="{{ url('/')}}/media/bg/{{ $image->name }}" width="100%" height="1000%">
			                </div><!-- /.box-body -->
			            </div><!-- /.box -->
					</div>
					<?php $i++; ?>

					@if( $i % 4 == 0 )
					 	</div>
					@endif

					@endforeach

				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title" style="text-align:left">Upload Banner </h4>
            </div>
            <div class="modal-body" style="text-align:left">
              
            	<form method="post" enctype="multipart/form-data" id="changeBannerForm" action="{{ url('setting/changeBannerAjax') }}" role="form">
            
		            <div class="form-group row">
		              <label for="uploadBanner" class="col-md-4">File Upload</label>
		              <div class="col-md-4">
		                <input type="file" name="uploadBanner" id="uploadBanner">  
		              </div>
		            </div>            
		            <div class="form-group row">
		              <label for="authchange" class="col-md-4">Authorize Changes</label>
		              <div class="col-md-4">
		              	<input type="hidden" name="upban" id="upban" value="Upload Banner">
		                <input type="password" placeholder="Authorize Password" name="authchange" id="authchange" class="form-control">
		              </div>
		            </div>
		        </form>
            </div>
            <div class="modal-footer"> <a href="#" id="btnclose" class="btn btn-default"><i class="fa"> </i>&nbsp; Close</a>
              <button type="button" id="btnsave" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
    </div>
@include('setting.changeBannerJs')
@endsection