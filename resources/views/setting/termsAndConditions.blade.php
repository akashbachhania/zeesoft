@extends('layout.master')

@section('content')

<div class="row">
	<div class="col-md-12">
	  <div class="box box-info">
	    <div class="box-header">
	      <h3 class="box-title">Terms & Condition</h3>
	    </div><!-- /.box-header -->
	    <div class="box-body pad">
	      <form>
	      	<div class="form-body">
	      		<div class="form-group row">
	      			<div class="col-md-12">
	      				<textarea id="trems-con" name="editor1" rows="10" cols="80">
				        	@foreach( $terms as $term)
				        	{{ $term->trems_condition }}
				        	@endforeach
				        </textarea>	 
	      			</div>
	      					
	      		</div>
	      		<div class="form-group row">
	      			<div class="col-md-3">
                    	<button type="button" id="createadd" class="btn btn-success">submit</button>
                        <a href="{{ url('dashboard') }}">
                        	<button type="button" id="cancel" class="btn btn-default">Cancel</button>
                        </a> 
					</div>
	      		</div>
	      	</div>
	        
	      </form>
	    </div>
	  </div><!-- /.box -->
	</div><!-- /.col-->
</div><!-- ./row -->
@include('setting.termsAndConditionsJs')
@endsection