<script type="text/javascript">
$(document).ready(function() {
    $('#datatable_ajax').DataTable( {
              paging: true,
              searching: true,
              "bSort": false,
              "ajax": {
                  "url": root + "/setting/subcategory_ajax",
                  dataSrc: "",
                  "type": 'POST',
                  "data": {
                  _token: $('input[name=_token]').val(),
                 }

              // ajax source
              } ,
              columns: [
                { data: 'industry' },
                { data: 'category' },
                { data: 'subcategory' },
                { data: 'action' },
              ]
    } );
}); 
setInterval( function () {
  $('#datatable_ajax').DataTable().ajax.reload(null,false);
}, 10000 );   
</script>

<script type="text/javascript">

jQuery(document).ready(function() {
    
    $("#name").change(function() {

       checkDuplicate();

    });

    $("#industry").change(function() {

       checkDuplicate();

    });

});
function getState(val) {

 var indmode="category";

 $('#sub_cat').empty();

 var formData = {ind_id:val,type:indmode,_token:$('input[name=_token]').val(),}; 

    $.ajax({

    type: "POST",

    url: root + '/setting/subcategory_ajax',

    data: formData,

    dataType:'json',
   

    success:function(data){

     $("#category").empty();

            var div_data ="<option value=0>Select Category</option>";

                $(div_data).appendTo("#category");

            $.each(data.Category,function(i,data) 

            { 

                var div_data ="<option value="+data.catid+">" + data.name+"</option>";

                $(div_data).appendTo("#category");

            });

         }

    });

}
function checkDuplicate ()

{      
        var pname = $("#name").val();

        var iid = $("#category").val();

        var industry = $("#industry").val();

        if(pname != '' && iid !='0' && iid !='' && industry !='0' && industry !='' )
        {
            $.ajax({
                type: "POST",
                url: root + '/setting/subcategory_ajax',
                async: false,
                data: {
                        pname: $("#name").val(),

                        type: 'exist',
                        
                        industry: $("#industry").val(),

                        iid: $("#category").val(),

                        _token: $('input[name=_token]').val(),
                },
                success: function(data, status, xhr) {

                    if (data == 'exist') {
                        $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                        $("#checkProduct").val('0');
                    } else {
                        $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                        $("#checkProduct").val('1');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        } 
        else
        {
            $("#msgg").show().html('<span></span>');
        }
}  


// this show a new model for add industry
$("#btnnew").on("click", function() {

            $('#basic').modal('show');
            // $('#status').val('');
            $('#name').val('');
            $("label.error").hide();
            $(".error").removeClass("error");
            setTimeout(function() {
                $('#name').focus();
            }, 1000);
});

// this close add industry model


$(document).on("click","#btnclose", function() {
 
    // $('#status').val('');
    $('#name').val('');
    $('#basic').modal('hide');
    $("#msgg").hide();
    $("#checkProduct").val('');
    $('#name').attr('autocomplete', 'false');
});

// this will add and update industry

$("#btnsave").on("click", function() {
   
    
      var name = $('#name').val();

      var status = $('#status').val();

      var checkProduct = $('#checkProduct').val();

      var iid = $('#category').val();

      var id = $('#id').val();

      var industry = $('#industry').val();

      if (industry == '' || industry == '0') {

        alert('Industry Name must be Required');

        return;

      }

      if (iid == '' || iid == '0') {

        alert('Category Name must be Required');

        return;

      }

      if (name == '' || status == '') {

        alert('Sub Category Name must be Required');

        return;

      }
      if( id == '0'){
          if(name != '' && iid != '' && iid != '0')
          {

            var data = {
                type: 'add',
                name: name,
                _token: $('input[name=_token]').val(),
                status: status,
                industry: $("#industry").val(),

                iid: $("#category").val(),

            };
            $.ajax({
                 type: 'POST',
                 url: root + '/setting/subcategory_ajax',
                 cache: false,
                 data: data,
                 async: false,
                 success: function(data) {
                   
                    if (data.msg == 'SUCCESS') {
                                    
                        //shownoti("Industry Added Successfully");
                        $('#basic').modal('hide');
                        var table = $('#datatable_ajax').DataTable();
                        table.ajax.reload(null,false);
                        $("#msgg").hide();
                        $("#checkProduct").val('');
                        $("#name").val('');
                        $("#industry").val('');
                        $("#category").val('');
                    }
                    if (data.msg == 'EROOR') {
                       
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    } //if finish share
    else {
        
        var data = {
            name: name,
            type: 'update',
            status: status,
            _token: $('input[name=_token]').val(),
            id: id,
            industry:industry,
            iid: iid
        };
        $.ajax({
            type: 'POST',
            url: root + '/setting/subcategory_ajax',
            cache: false,
            data: data,
            async: false,
            success: function(data) {
                
                if (data.msg == 'SUCCESS') {
             //shownoti("Industry Updated Successfully");
             
                    $('#basic').modal('hide');
                    $("label.error").hide();
                    $(".error").removeClass("error");
                   var table = $('#datatable_ajax').DataTable();
                   table.ajax.reload(null,false);
                    $("#msgg").hide();
                    $("#checkProduct").val('');
                     $("#industry").val('');
                     $("#category").val('');
                     $('#id').val(0);
                }
                if (data.msg == 'EROOR') {
                    $('#basic').modal('hide');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }
});
 function editme(obj) {
        $('#basic').modal('show');
        // $('#status').val('');
        $('#name').val('');
        $('#id').val(obj);
     
        setTimeout(function() {
            $('#name').focus();
        }, 1000);
         var data = {
            type: 'search',
            _token: $('input[name=_token]').val(),
            id: obj
        };
        $.ajax({
            type: 'POST',
            url: root + '/setting/subcategory_ajax',
            cache: false,
            data: data,
            dataSrc:"Category",
            data: data,
            success: function(data) {
               
                $('#status').val(data.isactive);
                $('#name').val(data.name);
                $('#industry').val(data.industryid);
                getState(data.industryid);

                setTimeout(function() {

                $('#category').val(data.catid); 

                 }, 1000);

                
                $("#msgg").hide();
                $("#checkProduct").val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }

    function deleteme(obj) {
       var data = {
            type: 'delete',
           
            _token: $('input[name=_token]').val(),
            id: obj
        };
        if (confirm("Are you sure you wish to delete this Sub-Category?")) {
            $.ajax({
                type: 'POST',
                url: root + '/setting/subcategory_ajax',
                cache: false,
                data: data,
                
                success: function(data) {
                    
                    if (data.msg == 'ERROR') {
                        alert(response.name)
                    } else {
           
                        window.location = 'industry';
                    }
                     $('#datatable_ajax').DataTable().ajax.reload(null,false);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }
</script>