@extends('layout.master')

@section('content')
  <div class="row">
    <div class="col-md-8">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Import Data</h3>
          <h5 style="font-weight: BOLD;">Note: Please check for duplicates prior to importing data</h5>
        </div><!-- /.box-header -->
        <!-- form start -->
        
        <div class="box-body">
          <form role="form" action="{{ url('setting/importDataStore') }}" id="importPostForm" enctype="multipart/form-data" method="post">
            <div class="form-group row">
              <label class="col-md-4" for="itype">Data <span class="text-danger">*</span></label>
              <div class="col-md-4">
                <select class="form-control" name="itype" id="itype" onchange="sample()">
                  <option>Select Data</option>
                  @if( Auth::user()->roletype == 'AD' )
                  
                    <option value="Brands" > Import Brands</option>
                    <option value="Categories" > Import Categories</option>
                    <option value="Customers"> Import Customers</option>
                    <option value="Industries"> Import Industries</option>
                    <option value="Products" {{ (\Request::input('data-type') == "Products")?'selected="selected"':'' }}> Import Products</option>
                    <option value="Subcategories"> Import Sub-Categories</option>
                    <option value="Users"> Import Users</option>
                  
                  @else
                  
                    <option value="Customers"> Import Customers</option>
                    <option value="Products" {{ (\Request::input('data-type') == "Products")?'selected="selected"':'' }}> Import Products</option>

                  @endif
                </select>                
              </div>
              <div class="col-md-3" id="sample-file" style="text-align:left;"></div>
            </div>
            <div class="form-group row">
              <label class="col-md-4" for="uploadFile">File Upload <span class="text-danger">*</span></label>
              <div class="col-md-4">
                <input type="file" id="uploadFile" name="uploadFile">  
              </div>
            </div>            
            <div class="form-group row">
              <label class="col-md-4" for="authchange">Authorize Changes <span class="text-danger">*</span></label>
              <div class="col-md-4">
                <input type="password" class="form-control" id="authchange" name="authchange" placeholder="Authorize Password">
                <input type="hidden" id="authorize" value="">
              </div>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" id="submitButton" class="btn btn-primary">Submit</button>
            <!-- <button type="reset" id="cancel" class="btn btn-default">Cancel</button> -->
            <a href="{{ url('dashboard') }}" id="cancel" class="btn btn-default">Cancel</a>
          </div>
        </form>
      </div><!-- /.box -->
    </div>    
  </div>
@include('setting.importDataJs')
@endsection