@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Noti Files</h3>
            <div>
              <span style="font-weight:bold; font-size:15px;">Notes:</span>
              <span style="margin-left:5px">
                Type 'Action info' used for action perform in software and 'Sign in info' used for when user sign in.<br>
                Please upload atleast files in atleast two format (.mp3 and .wav) to support all browser.
              </span>
            </div>
            <div class="pagination-panel pull-right"> 
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
           <div class="portlet-body">
              <div class="table-container">
             
                <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                  <thead>
                    <tr role="row" class="heading">
                      <th width="30%"> Sr No </th>
                      <th width="20%"> name </th>
                      <th width="30%"> type </th>
                      <th width="30%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; ?>
                    @foreach( $man as $m )

                      <tr>
                        <td>
                          {{ $i }}
                        </td>
                        <td>
                          {{ $m['basename'] }}
                        </td>
                        <td>
                          @if(strpos($m['basename'], '0396') === false)
                            Sign in info
                          @else
                            Action info
                          @endif
                        </td>
                        <td class="sound">
                          <i class="fa fa-pencil-square-o btnnew" id="btnnew" type="@if(strpos($m['basename'], '0396') === false)Sign @else Action @endif"></i>
                          <span class="play" name="{{ $m['basename'] }}">Play</span>
                          <span class="pause" name="{{ $m['basename'] }}">Stop</span>
                        </td>
                      </tr>
                      <?php $i++ ?>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
 <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title" style="text-align:left">Change Noti Files </h4>
            </div>
            <div class="modal-body" style="text-align:left">
              <form method="post" enctype="multipart/form-data" id="changeBannerForm" action="{{ url('setting/changeNotifilesAjax') }}" role="form">
                  <input type="hidden" name="type" value="" id="sound_type">
                  <div class="form-group row">
                    <label for="uploadBanner" class="col-md-4">File Upload</label>
                    <div class="col-md-4">
                      <input type="file" name="uploadnoti" id="uploadnoti">  
                    </div>
                  </div>            
                  <div class="form-group row">
                    <label for="authchange" class="col-md-4">Authorize Changes</label>
                    <div class="col-md-4">
                      <input type="hidden" name="upnoti" id="upnoti" value="Upload Noti">
                      <input type="password" placeholder="Authorize Password" name="authchange" id="authchange" class="form-control">
                    </div>
                  </div>
              </form>   
            </div>
            <div class="modal-footer"> <a href="#" id="btnclose" class="btn btn-default"><i class="fa"> </i>&nbsp; Close</a>
              <button type="button" id="btnsave" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
@include('setting.notifilesJs')
@endsection