@if( Auth::user()->roletype != 'TM' )
<script type="text/javascript">
	var table = $('#notificationSettingTable').DataTable({

	"ajax": {
            "url":root +"/setting/notificationSetting_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        "aoColumnDefs": [
            { "bSortable": false }
        ],
        columns: [
            { data: 'userCheck' },
            { data: 'suggestdate' },
            { data: 'addmsg' },
            { data: 'status' }        
        ]
	});
</script>
@else

<script type="text/javascript">
	var table = $('#notificationSettingTable').DataTable({

	"ajax": {
            "url":root +"/setting/notificationSetting_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        "aoColumnDefs": [
            { "bSortable": false }
        ],
        columns: [
            { data: 'userCheck' },
            { data: 'sdate' },
            { data: 'accmsg' },
            { data: 'status' }        
        ]
	});
</script>
@endif
<script type="text/javascript">

     $('#notificationSettingTable tbody').on('click', 'tr td:not(:first-child)', function () {	

     	var data = table.row(this).data();

        var page = $(this).parent().find('td a').attr('href');

   		window.location.href = page;
		var hiddennotiid = $('#checkUser').val();
		//alert(hiddennotiid);
    });
</script>