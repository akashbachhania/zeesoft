@extends('layout.master')

@section('content')
<div class="row">

  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#backup" data-toggle="tab">Backup</a></li>
        <li><a href="#restore" data-toggle="tab">Clear Database</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="backup">
          <!-- Post -->
          <div class="post">
            <div class="user-block text-center">
              <h3>To take a Backup, please click here:&nbsp;&nbsp;<a href="{{url('setting/backup')}}" class="fa fa-download red"></a></h3>
            </div><!-- /.user-block -->
          </div><!-- /.post -->
        </div><!-- /.tab-pane -->

        <div class="tab-pane" id="restore">
          <div class="post">
            <div class="user-block text-center">
              <h3>Are you sure you wish to clear the database?&nbsp;&nbsp;<a href="{{ url('setting/clearDB') }}" class="fa fa-trash red"></a></h3>
            </div>
          </div>
        </div><!-- /.tab-pane -->
      </div><!-- /.tab-content -->
    </div><!-- /.nav-tabs-custom -->
  </div><!-- /.col -->
</div><!-- /.row -->
@endsection

<script type="text/javascript">
  function shownoti(message) {
    $(function() {
      function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg;
      }
      var toasts = [
        new Toast('success', 'toast-top-right', message),
      ];
      toastr.options.positionClass = 'toast-top-full-width';
      toastr.options.extendedTimeOut = 0; //1000;
      toastr.options.timeOut = 2000;
      toastr.options.fadeOut = 250;
      toastr.options.fadeIn = 250;
      var i = 0;
      /* $('#tryMe').click(function () {

           $('#tryMe').prop('disabled', true);*/
      delayToasts();
      //});
      function delayToasts() {
        if (i === toasts.length) {
          return;
        }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function() {
          showToast();
        }, delay);
        // re-enable the button        
        if (i === toasts.length - 1) {
          window.setTimeout(function() {
            prop('disabled', false);
            i = 0;
          }, delay + 1000);
        }
      }

      function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
      }
    })
  }
</script>