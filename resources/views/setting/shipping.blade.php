@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Shipping Conditions</h3>
            <div class="pagination-panel pull-right"> 
               <a href="" id="btnnew" class="btn btn-success" data-toggle="modal">
                 <i class="fa fa-plus"> </i>&nbsp; Add New 
               </a> 
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
           <div class="portlet-body">
              <div class="table-container">
             
                <table class="table table-striped table-bordered table-hover" id="shippingConditionTable">
                  <thead>
                    <tr role="row" class="heading">
                      <th width="10%">S.No.</th>
                      <th width="60%"> Shipping Conditions </th>
                      <th width="20%"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
 <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title" style="text-align:left">Shipping Condition Form </h4>
            </div>
            <div class="modal-body" style="text-align:left">
              <form action="#" id="form_sample_2" class="form-horizontal" novalidate>
                <div class="form-body">
                  <div id="msg" name="msg"></div>
                  <div class="form-group row">
                    <label class="col-md-3" style="text-align: right">Name <span class="required"> * </span> </label>
                    <div class="col-md-8">
                      
                        <input type="text" class="form-control" maxlength="80" id="name" name="name">
                        <div id="msgg" style="display: block; margin-bottom: 1px; clear: both; position: relative; top: -1px; margin-left: 6px;font-size: 14px;"></div>
                        <span style="display:none">
                        <input type="text" class="form-control" maxlength="80" value="0"  id="id" name="id">
                        </span>
                        <input type="hidden" class="form-control" name="checkProduct" id="checkProduct" value="">
                      
                    </div>
                  </div>
                  <div class="form-group row" style="display: none">
                    <label class="col-md-3" style="text-align: right">Status <span class="required"> * </span> </label>
                    <div class="col-md-8">
                      
                      <select class="form-control" id="status" name="status">
                        <option value="1" selected>ACTIVE</option>
                        <option value="0">DEACTIVE</option>
                      </select>
                      
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer"> 
              <a href="#" id="btnclose" class="btn btn-default"><i class="fa"> </i>&nbsp; Close</a>
              <button type="button" id="btnsave" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
@include('setting.shpg')
@endsection