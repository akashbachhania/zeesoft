<script type="text/javascript">
    $(document).ready(function(){

        if(String(window.location).match(/acceptedoffers/)){
            var url = root + "/order/offers_ajax/accepted";
        }else if(String(window.location).match(/completedoffers/)){
            var url = root + "/order/offers_ajax/completed";
        }

        var acceptedoffers = $('#acceptedoffers').DataTable( {

            "ajax": {
                "url":url,
                "dataSrc": "",
                "type": 'GET',
            },
            columns: [
                { data: 'quotationno' },
                { data: 'acc_date' },
                { data: 'ptype' },
                { data: 'quantity' },
                { data: 'productName' },
                { data: 'price' },
                { data: 'currency' },
                { data: 'timeframe' },
                { data: 'expdate' },
                { data: 'status' }
            ]
        });

        setInterval( function () {
            acceptedoffers.ajax.reload(null,false);
        }, 3000 );  
    });
</script>