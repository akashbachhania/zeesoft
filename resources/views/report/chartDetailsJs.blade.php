@if( \Request::input('view_type')=="Postings" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'post_advertisment_userid'},
        { data: 'pdate'},
        { data: 'post_advertisment_pstatus'},
        { data: 'postno'}        
        ]
    } );
    var chartDetailsTable1 = $('#chartDetailTable1').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=expand",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'manufacture' },
        { data: 'caseweight' },
        { data: 'qtypercase' },
        { data: 'pakaging'},
        { data: 'shipingcondition' },
        { data: 'productcodes2' },
        { data: 'description' },
        { data: 'ptype' },
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'uom'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'expdate'},
        { data: 'expirydate'},
        { data: 'customerrefno'},
        { data: 'post_advertisment_pakaging'},
        { data: 'post_advertisment_timeframe'},
        { data: 'post_advertisment_country'},
        { data: 'post_advertisment_userid'},
        { data: 'pdate'},
        { data: 'post_advertisment_pstatus'},
        { data: 'postno'}        
        ]
    } );
</script>
@elseif( \Request::input('view_type')=="Products" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'name' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'product_userid'},
        { data: 'pdate'},
        { data: 'status'}        
        ]
    } );
    var chartDetailsTable1 = $('#chartDetailTable1').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=expand",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'name' },
        { data: 'productid'},
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'manufacture'},
        { data: 'caseweight'},
        { data: 'qtypercase'},
        { data: 'pakaging'},
        { data: 'shipingcondition'},
        { data: 'productcodes2'},
        { data: 'description'},
        { data: 'product_userid'},
        { data: 'pdate'},
        { data: 'status'}        
        ]
    } );
</script>
@elseif( \Request::input('view_type')=="Quotes" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'post_advertisment_userid'},
        { data: 'post_advertisment_pdate'},
        { data: 'offerstatus'},
        { data: 'postno'},
        { data: 'quotationno'},
        { data: 'pdate'},
        { data: 'counter_date'}        
        ]
    } );

    var chartDetailsTable1 = $('#chartDetailTable1').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=expand",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'manufacture'},
        { data: 'caseweight'},
        { data: 'qtypercase'},
        { data: 'pakaging'},
        { data: 'shipingcondition'},
        { data: 'productcodes2'},
        { data: 'description'},
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'uom'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'expdate'},
        { data: 'expirydate'},
        { data: 'customerrefno'},
        { data: 'post_advertisment_pakaging'},
        { data: 'post_advertisment_timeframe'},
        { data: 'post_advertisment_country'},
        { data: 'post_advertisment_userid'},
        { data: 'post_advertisment_pdate'},
        { data: 'offerstatus'},
        { data: 'postno'},
        { data: 'quotationno'},
        { data: 'pdate'},
        { data: 'counter_date'}  
        ]
    } );
</script>
@elseif( \Request::input('view_type')=="Offers" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'post_advertisment_userid'},
        { data: 'post_advertisment_pdate'},
        { data: 'offerstatus'},
        { data: 'postno'},
        { data: 'quotationno'},
        { data: 'pdate'},
        { data: 'counter_date'}        
        ]
    } );
    var chartDetailsTable1 = $('#chartDetailTable1').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=expand",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'manufacture'},
        { data: 'caseweight'},
        { data: 'qtypercase'},
        { data: 'pakaging'},
        { data: 'shipingcondition'},
        { data: 'productcodes2'},
        { data: 'description'},
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'uom'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'expdate'},
        { data: 'expirydate'},
        { data: 'customerrefno'},
        { data: 'post_advertisment_language'},
        { data: 'post_advertisment_timeframe'},
        { data: 'post_advertisment_country'},
        { data: 'post_advertisment_userid'},
        { data: 'post_advertisment_pdate'},
        { data: 'offerstatus'},
        { data: 'postno'},
        { data: 'quotationno'},
        { data: 'pdate'},
        { data: 'counter_date'}  
        ]
    } );
</script>
@elseif( \Request::input('view_type')=="Orders" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'post_advertisment_userid'},
        { data: 'post_advertisment_pdate'},
        { data: 'offerstatus'},
        { data: 'postno'},
        { data: 'quotationno'},
        { data: 'quotefacility'},
        { data: 'buyer'},
        { data: 'seller'},
        { data: 'pdate'},
        { data: 'counter_date'}        
        ]
    } );
        var chartDetailsTable1 = $('#chartDetailTable1').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=expand",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
	        { data: 'product_name' },
	        { data: 'productid' },
	        { data: 'industryname' },
	        { data: 'categoryname' },
	        { data: 'subcategoryname'},
	        { data: 'brandname' },
	        { data: 'manufacture'},
	        { data: 'caseweight'},
	        { data: 'qtypercase'},
	        { data: 'pakaging'},
	        { data: 'shipingcondition'},
	        { data: 'productcodes2'},
	        { data: 'description'},
	        { data: 'ptype'},
	        { data: 'targetprice'},
	        { data: 'currency'},
	        { data: 'uom'},
	        { data: 'quantity'},
	        { data: 'location'},
	        { data: 'expdate'},
	        { data: 'expirydate'},
	        { data: 'customerrefno'},
	        { data: 'post_advertisment_pakaging'},
	        { data: 'post_advertisment_language'},
	        { data: 'post_advertisment_timeframe'},
	        { data: 'post_advertisment_country'},
	        { data: 'post_advertisment_userid'},
	        { data: 'post_advertisment_pdate'},
	        { data: 'offerstatus'},
	        { data: 'postno'},
	        { data: 'quotationno'},
	        { data: 'buyer'},
	        { data: 'seller'},
	        { data: 'pdate'},
	        { data: 'counter_date'}  
        ]
    } );
</script>
@elseif( \Request::input('view_type')=="Customers" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'post_advertisment_userid'},
        { data: 'pdate'},
        { data: 'post_advertisment_pstatus'},
        { data: 'postno'}        
        ]
    } );
        var chartDetailsTable1 = $('#chartDetailTable1').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=expand",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
	        { data: 'product_name' },
	        { data: 'productid' },
	        { data: 'industryname' },
	        { data: 'categoryname' },
	        { data: 'subcategoryname'},
	        { data: 'brandname' },
	        { data: 'manufacture'},
	        { data: 'caseweight'},
	        { data: 'qtypercase'},
	        { data: 'pakaging'},
	        { data: 'shipingcondition'},
	        { data: 'productcodes2'},
	        { data: 'description'},
	        { data: 'ptype'},
	        { data: 'targetprice'},
	        { data: 'currency'},
	        { data: 'uom'},
	        { data: 'quantity'},
	        { data: 'location'},
	        { data: 'expdate'},
	        { data: 'expirydate'},
	        { data: 'customerrefno'},
	        { data: 'post_advertisment_pakaging'},
	        { data: 'post_advertisment_timeframe'},
	        { data: 'post_advertisment_country'},
	        { data: 'post_advertisment_userid'},
	        { data: 'pdate'},
	        { data: 'status'},
	        { data: 'postno'},
	        { data: 'refno'}  
        ]
    } );    
</script>
@elseif( \Request::input('view_type')=="Users" )
<script type="text/javascript">
	
	var productno = "{{ \Request::input('productno') }}";
   
    var industryid = "{{ \Request::input('industryid') }}";
   
    var postno = "{{ \Request::input('postno') }}";
   
    var refno = "{{ \Request::input('refno') }}";

	var industryid_id = "{{ \Request::input('industryid_id') }}";
    
    var view_type = "{{ \Request::input('view_type') }}";
    
    var industry_id = $('input[name="industry_id"]').val();
    
    var sec_name = $('input[name="sec_name"]').val();
   
    var clauseby = $('input[name="clauseby"]').val();
    
    var lastfilter =$('input[name="lastfilter"]').val();

	var chartDetailsTable = $('#chartDetailTable').DataTable( {
		"bSort":false,
        "ajax": {
            "url":root +"/report/chartDetailsTable_ajax?industry_id=" + industry_id+"&sec_name="+sec_name+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type=detailed",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'product_name' },
        { data: 'productid' },
        { data: 'industryname' },
        { data: 'categoryname' },
        { data: 'subcategoryname'},
        { data: 'brandname' },
        { data: 'ptype'},
        { data: 'targetprice'},
        { data: 'currency'},
        { data: 'quantity'},
        { data: 'location'},
        { data: 'post_advertisment_userid'},
        { data: 'pdate'},
        { data: 'post_advertisment_pstatus'},
        { data: 'postno'}        
        ]
    } );
</script>
@endif

<script type="text/javascript">
	$(document).ready(function(){
        
        $('#chartDetailTable tbody').on('click', 'tr td', function() {
            var data = chartDetailsTable.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
        });
        
        $('#chartDetailTable1 tbody').on('click', 'tr td', function() {
            var data = chartDetailsTable1.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
        });
	});	
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$('#dv1,#chartDetailTable1_wrapper').css('display','none');

		$('#dv').on('click',function(){
			$('#chartDetailTable1_wrapper,#dv1').removeAttr('style');	
			$('#chartDetailTable_wrapper,#dv').css('display','none');	
		});
		$('#dv1').on('click',function(){
			$('#chartDetailTable_wrapper,#dv').removeAttr('style');	
			$('#chartDetailTable1_wrapper,#dv1').css('display','none');	
		});
	});
</script>
<script type="text/javascript">
	$('#exportcsv').click(function() {
	    var viewtype = $('#expandview').val();	
		var exportData = '';
		var industryid_id = "{{ \Request::input('industryid_id') }}";
		var view_type = "{{ \Request::input('view_type') }}";
		var postno = "{{ \Request::input('postno') }}";
		var productno = "{{ \Request::input('productno') }}";
		var industryid = "{{ \Request::input('industryid') }}";
		var refno = "{{ \Request::input('refno') }}";

		if(view_type=='Products'){

			var productno = "{{ \Request::input('productno') }}";
		}
			
		var user_id = "{{ \Request::input('user_id') }}";
		
		exportData = exportData.substring(0, exportData.length - 1);
		
		var activeproduct = $('input:checkbox[name=activeProduct]').is(':checked');
		if (activeproduct == true)
		{
			var isactive = '1';
		} else
		{
			var isactive = '0';
		}
		
		window.location = root + "/report/posting_csv_detail?industry_id=" + industry_id+"&sec_name="+sec_name+"&productno="+productno+"&industryid="+industryid+"&postno="+postno+"&refno="+refno+"&view_type="+viewtype+"&dataClauseby="+clauseby+"&lastfilter="+lastfilter;

	});
</script>