<script type="text/javascript">
	$(document).ready(function(){
		$("#to,#from,.d").daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			locale: {
	            format: 'DD-MM-YYYY'
	        }
		});
		$('.d').css('cursor','pointer');


		var indmode="industaction";
		var formData = {
			actionmode: indmode
		};
		var div_data = '<label><input id="selectallindustry" type="checkbox" onclick="getvalue(\'selectedindustry\',\'\');" />&nbsp;&nbsp;&nbsp;&nbsp;All</label><br />';
		$("#loadindustry").append(div_data);

		$.ajax({
			type: "POST",
			url: root + "/report/functions",
			data: formData,
			dataType: 'json',
			success: function(data) {
				$.each(data.itemData, function(i, data){
					div_data = '<label><input class="validindustry" type="checkbox" value="'+data.industryid+'" name="selectedindustry[]" onclick="getvalue(\'selectedindustry\','+data.industryid+');" />&nbsp;&nbsp;&nbsp;&nbsp;'+data.name+'</label><br />';
					if($('#allindustryids').val()!='') {
						$('#allindustryids').val($('#allindustryids').val()+','+data.industryid);
					} else {
						$('#allindustryids').val(data.industryid);
					}
					$("#loadindustry").append(div_data);
					});
                                            $("#first-div").removeClass('oper');
			}
		});


		$('#selectallindustry').change(function(){
                    
			if($(this).attr("checked")) {
                                
				$('.validindustry').attr('checked','checked');
				
				var array = $('#allindustryids').val().split(",");
                
                // $("#first-div").addClass('oper').css("position", "relative");
				
				$.each(array,function(i){
                    getvalue('selectedindustry',array[i]);               
				});
                // $("#first-div").removeClass('oper');
			}
		    else {
				
				$('#cat').html('');
				
				$('.validindustry').attr('checked',false);
                                
			}            
		});



	});


	function seelctall(){
    
	    if($('#selectcategoryall').is(':checked')) {
	       $('.indicat').attr('checked','checked');
	       var array = $('#allcategories').val().split(",");
	        console.log(array);
	        $.each(array,function(i){
			   findsubcategory(array[i]);
			});
	    } else {
	        $('.indicat').removeAttr('checked');
	    }
	    $("#first-div").removeClass('oper');
	}
	function seelctallbrands(){
	    if($('#selectbrandsall').is(':checked')) {
	       $('.brandsindi').attr('checked',true);
	    } else {
	        $('.brandsindi').attr('checked',false);
	    }
	}

	function checkallsubcat(){
	    if($('#allsubcategory').is(':checked')) {
	       $('.indisubcat').attr('checked',true);
	    } else {
	        $('.indisubcat').attr('checked',false);
	    }
	}
	function seelctallcustomers1(){
	    if($('#seelctallcustomers').is(':checked')) {
	       $('.indicust').attr('checked',true);
	    } else {
	        $('.indicust').attr('checked',false);
	    }

	}
	function checkallusersid(){
	    if($('#checkallusersid111').is(':checked')) {
	       $('.indiusersid').attr('checked',true);
	    } else {
	        $('.indiusersid').attr('checked',false);
	    }

	}
	function checkalltype(){
	    if($('#checkalltypeid').is(':checked')) {
	       $('.inditype').attr('checked',true);
	    } else {
	        $('.inditype').attr('checked',false);
	    }
	}
	function seelctallloc(){
	    if($('#locall').is(':checked')) {
	       $('.indiloc').attr('checked',true);
	    } else {
	        $('.indiloc').attr('checked',false);
	    }
	}
	function seelctallstatus(){
	    if($('#seelctallstatusid').is(':checked')) {
	       $('.indistaus').attr('checked',true);
	    } else {
	        $('.indistaus').attr('checked',false);
	    }
	}

	function getvalue(sectiontype,value){
	    
	    var yourarray = [];
		yourarray.push(value);
		
		if(yourarray!=""){
		var indmode = "selectreportindustry";
		var formData = {
			ind_id: yourarray,
			actionmode: indmode
		}; //Array 
		var div_data = '';
	            $.ajax({
	                type: "POST",
	                url: root + "/report/functions",
	                async: false,
	                data: formData,
	                dataType: 'json',
	                beforeSend: function(){
	                    //alert(value);
	                    $("#first-div").addClass('oper').css("position", "relative");
	                },
	                success: function(data, status, xhr) {
	                var div_data = "";
	                var div_data2 = "";
	                if (data != null){
	                var div_data = '<div class="spanselectcategory'+value+'">';
	                var div_data2 = '<div class="spanselectbrand'+value+'">';
	                if($('#selectcategoryall').length==0)
	                div_data += '<label><input type="checkbox" id="selectcategoryall" onclick="seelctall();" />&nbsp;&nbsp;&nbsp;&nbsp;All</label><br />';
	                if($('#selectbrandsall').length==0)
	                div_data2 += '<label><input type="checkbox" id="selectbrandsall" onclick="seelctallbrands();" />&nbsp;&nbsp;&nbsp;&nbsp;All</label><br />';
	                $.each(data.itemData, function(i, data){
	                        div_data += '<label><input type="checkbox" class="indicat" name="selectcategory[]" onclick="findsubcategory('+data.catid+')" value="'+data.catid+'" />&nbsp;&nbsp;&nbsp;&nbsp;'+data.name+'</label><br />';
	                        if($('#allcategories').val()==""){
	                            $('#allcategories').val(data.catid);
	                        } else {
	                            var newrec = $('#allcategories').val() +','+ data.catid;
	                            $('#allcategories').val(newrec);
	                        }
	                    });
	                        div_data += '</div>';
	                        if($('div.spanselectcategory'+value).length>0){
	                            $('.spanselectcategory'+value).remove();
	                        } else {
	                            $("#cat").append(div_data);
	                        }
	                        
	                        if(data.brandData[0].brandid!=0) {
	                            $.each(data.brandData, function(i, data){
	                                    div_data2 += '<label><input type="checkbox" class="brandsindi" id="selectbrands'+value+'" name="selectbrands[]" value="'+data.brandid+'" />&nbsp;&nbsp;&nbsp;&nbsp;'+data.name+'</label><br />';
	                            });
	                            div_data2 += '</div>';
	                            if($('div.spanselectbrand'+value).length>0){
	                                   $('div.spanselectbrand'+value).remove();
	                            } else {
	                                   $("#brands").append(div_data2);
	                            }
	                        }
	                    } 
	                    
	                },
	                complete : function(){
	                    //alert('aaaaaaaaaaaaaa');
	                    $("#first-div").removeClass('oper'); 
	                },
	                error: function(jqXHR, textStatus, errorThrown) {
	                    alert(errorThrown);
	                    commit(false);
	                }
	            });
		//}
	        }
	}

	function validateform(){
		var error = false;
		var msg = '';
		  
		if($('select#Section').val() == ""){
			error = true;
			msg+="Section must be selected.\n";
		}
		  
		if(!error)  
			return true;
		else {
			alert(msg);
			return false;
		}
		
	}


	function findsubcategory(catids){
        var categories = [];
        var indmode = "selectsubcategory";
		var formData = {
			cat_id: catids,
			actionmode: indmode
		}; //Array 
	        
		$.ajax({
			type: "POST",
			url: root + "/report/functions",
			async: false,
			data: formData,
			dataType: 'json',
			success: function(data, status, xhr) {
	                
	                var div_data = '';
	                        
	            if($('#allsubcategory').length==0)
				  div_data = '<label><input type="checkbox" onclick="checkallsubcat();" id="allsubcategory" />&nbsp;&nbsp;&nbsp;&nbsp;All</label><br />';
                    if (data != null){
                        div_data += '<div class="subcategory_'+catids+'">';
                        $.each(data.itemData, function(i, data){
                            div_data += '<label><input class="indisubcat" type="checkbox" name="selectsubcat[]" value="'+data.subcatid+'" />&nbsp;&nbsp;'+data.name+'</label><br />';
                        });
                        div_data += '</div>';
					}
                    if($('div.subcategory_'+catids).length>0){
                        $('div.subcategory_'+catids).remove();
                        if($("#subcat > div").length==0) {
                            $('#subcat').html('');
                        }
                    } else {
                            $('#subcat').append(div_data);
                    }
                    
			}
		});
	    
	}	
</script>


<script type="text/javascript">

    $("select#Section").on('change',function() {

        var Section = $("#Section").val();
		
		if(Section == "Offers")
		{

			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Waitting For Buyer" /> Waiting For Buyer</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Waitting For Seller" /> Waitting For Seller</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Waitting For Both" /> Waitting For Both</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Completed" /> Completed</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Cancelled" /> Cancelled</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Accepted" /> Accepted</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Declined" /> Declined</label>');
		}
		if(Section=="Postings")
		{
			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="NEW" /> NEW</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="ACTIVE" /> ACTIVE</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="PENDING" /> PENDING</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="COMPLETE" /> COMPLETE</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="CANCELLED" /> CANCELLED </label><br /><label><input type="checkbox" class="indistaus" name="status[]" value="COUNTERED" /> COUNTERED</label>');
		}
		if(Section=="Products")
		{
			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="1" /> Active</label> <br /><label><input class="indistaus" type="checkbox" class="indistaus" name="status[]" value="0" /> InActive</label> ');
		}
		if(Section=="Quotes")
		{
			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Waitting" /> Waitting</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Accepted" /> Accepted</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Declined" /> Declined</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Countered" /> Countered</label>');
		}
		if(Section=="Orders")
		{
			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Compeleted" /> Compeleted</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Cancelled" /> Cancelled</label><br /><label><input type="checkbox" class="indistaus" name="status[]" value="Accepted" /> Accepted</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="Declined" /> Declined</label>');
		}
		if(Section=="Customers")
		{
			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="0" /> InActive</label><br /><label><input type="checkbox" class="indistaus" name="status[]" value="ACTIVE" /> ACTIVE</label><br /><label><input type="checkbox" class="indistaus" name="status[]" value="CANCELLED" /> CANCELLED</label> ');
		}
		if(Section=="Users")
		{
			$("#status").html('<label><input type="checkbox" id="seelctallstatusid" onclick="seelctallstatus();" /> All</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="1" /> Active</label> <br /><label><input type="checkbox" class="indistaus" name="status[]" value="0" /> InActive</label> ');
		}
    
    });
	/*fetch customers code start*/
	jQuery(document).ready(function() {
		 var custmode="customeraction";

		 var formData = {
			actionmode: custmode
		 };
  
         var div_data = '';

         div_data += '<label><input type="checkbox" id="seelctallcustomers" onclick="seelctallcustomers1();">&nbsp;&nbsp;&nbsp;&nbsp;All</label><br>';
		
		$.ajax({
			type: "POST",
			url: root + "/report/functions",
			data: formData,
			dataType: 'json',
			success: function(data) {
				$.each(data.itemData, function(i, data){

				  div_data += '<label><input type="checkbox" class="indicust" name="customerdata[]" value="'+data.refno+'" />&nbsp;&nbsp;&nbsp;&nbsp;'+data.refno+'</label><br />';
				  
				});
                
                $('#customerdata').append(div_data);
				
			}
			
		});
	});
	<!--fetch customers code end-->
	
/*fetch alluser code start*/
	jQuery(document).ready(function() {
		 
		 var custmode="alluserid";
		 
		 var formData = {
			actionmode: custmode
		 };
         
         var div_data = "";
         
         div_data += '<label><input type="checkbox" id="checkallusersid111" onclick="checkallusersid();" />&nbsp;&nbsp;&nbsp;&nbsp;All</label><br />';
		
		$.ajax({
			type: "POST",
			url: root + "/report/functions",
			data: formData,
			dataType: 'json',
			success: function(data) {
				$.each(data.itemData, function(i, data){

					div_data += '<label><input type="checkbox" class="indiusersid" name="alluserid[]" value="'+data.userid+'" />&nbsp;&nbsp;&nbsp;&nbsp;'+data.userid+'</label><br />';
							
				});
                
                $(div_data).appendTo("#alluserid");
			}
		});
	});
	/*fetch alluser code end */

/*fetch alllocation code start*/
	jQuery(document).ready(function() {
		 
		 var custmode="alllocation";
		 
		 var formData = {
			actionmode: custmode
		 };
         
         var div_data = '';
         
         div_data += '<label><input type="checkbox" id="locall" onclick="seelctallloc();" />&nbsp;&nbsp;&nbsp;&nbsp;All</label><br />';
		
		 $.ajax({
			type: "POST",
			url: root + "/report/functions",
			data: formData,
			dataType: 'json',
			success: function(data) {
				$.each(data.itemData, function(i, data){

					div_data += '<label><input class="indiloc" type="checkbox" name="alllocation[]" value="'+data.name+'" />&nbsp;&nbsp;&nbsp;&nbsp;'+data.name+'</label><br />';
							
				});
                
                $("#alllocation").html(div_data);			
			}
		});
	});
	/*fetch alluser code end*/

	$(function() {
        $(".search").keyup(function() {
            var searchid = $(this).val();
            var dataString = 'search=' + searchid;
            if (searchid != '') {
                $.ajax({
                    type: "POST",
                    url: root + "/posting/search",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        $("#result").html(html).show();
                    }
                });
            }
            return false;
        });
        jQuery(document).on("click", function(e) {
            var $clicked = $(e.target);
            if (!$clicked.hasClass("search")) {
                jQuery("#result").fadeOut();
            }
        });
        $('#searchid').click(function() {
            jQuery("#result").fadeIn();
        });
    });

	function hello(str) {
        var val = str.split(':');
        var first = val[0];
        var second = val[1];
        $('#searchid').val(second);
        $('#pid').val(first);
        var pro_id = first;
        $.ajax({
            type: "POST",
            url: root + "/posting/fetch_productdetail",
            data: {
                pro_id: pro_id
            },
            cache: false,
            success: function(data) {
			
			}
        });
    }

</script>


