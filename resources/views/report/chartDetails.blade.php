@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
          <div class="box-header row">
            <div class="col-md-3">
              <h3 class="box-title">
                {{ $headTitle }}
              </h3>   
            </div>
            <div class="col-md-4 col-md-push-6">
              <button type="button" class="btn btn-success zee-important-product" id="exportcsv">Export to CSV</button>         
              <button type="button" class="btn btn-success zee-important-product" id="dv">Expanded View</button>
              <button type="button" class="btn btn-success zee-important-product" id="dv1">Detailed View</button>
            </div>
             
          </div>

          <div class="box-body">
            <div class="row m_t30">
              <div class="col-md-12">
                <div class="table-responsive">
                  
                    <input type="hidden" name="section" value="{{ \Request::input('view_type') }}" />
                    <input type="hidden" name="searchedparam" value="{{ \Request::input('productno') }}" />
                    <!-- END PAGE LEVEL STYLES -->
                    @if(\Request::input('user_id'))
                    <input type="hidden" name="industry_id" value="{{ \Request::input('user_id') }}" />
                    @elseif(\Request::input('productno'))
                    <input type="hidden" name="industry_id" value="{{ \Request::input('productno') }}" />
                    @elseif(\Request::input('postno'))
                    <input type="hidden" name="industry_id" value="{{ \Request::input('postno') }}" />
                    @else
                    <input type="hidden" name="industry_id" value="{{ \Request::input('industryid_id') }}" />
                    @endif
                    <input type="hidden" name="sec_name" value="{{ \Request::input('view_type') }}" />

                    <input type="hidden" name="expandview" id="expandview" value="0"/>
 
                    <input type="hidden" name="clauseby" id="clauseby" value="{{ \Request::input('clauseby') }}"/>
 
                    <input type="hidden" name="lastfilter" id="lastfilter" value="{{ urlencode(\Request::input('q')) }}"/>

                  <table class="table table-bordered" id="chartDetailTable1">
                    <thead>
                      <tr role="row" class="zeeproduct-heading">
                      @if(\Request::input('view_type')=="Postings")
                      
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Manufacturer </th>
                        <th width="10%" style="text-align:left"> Case Weight </th>
                        <th width="10%" style="text-align:left"> Quantity per Case </th>
                        <th width="10%" style="text-align:left"> Packaging </th>
                        <th width="10%" style="text-align:left"> Shipping Conditions </th>
                        <th width="10%" style="text-align:left"> Product Codes </th>
                        <th width="10%" style="text-align:left"> Description </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Unit of Measurement </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> Expiry Date Range </th>
                        <th width="10%" style="text-align:left"> Exact Expiry Date </th>
                        <th width="10%" style="text-align:left"> Customer Reference Number </th>
                        <th width="10%" style="text-align:left"> Packaging Languages </th>
                        <th width="10%" style="text-align:left"> Timeframe </th>
                        <th width="10%" style="text-align:left"> Country of Origin </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting ID </th>
                       
                        @elseif(\Request::input('view_type')=="Products")
                        <th width="15%" style="text-align:left"> Product Name </th>
                          <th width="15%" style="text-align:left"> Product No. </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                         <th width="10%" style="text-align:left"> Category </th>
                          <th width="10%" style="text-align:left"> Sub Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Manufacturer </th>
                        <th width="10%" style="text-align:left"> Case Weight </th>
                        <th width="10%" style="text-align:left"> Quantity per Case </th>
                        <th width="10%" style="text-align:left"> Packaging </th>
                        <th width="10%" style="text-align:left"> Shipping Conditions </th>
                        <th width="10%" style="text-align:left"> Product Codes </th>
                        <th width="10%" style="text-align:left"> Description </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Suggestion Date </th>
                        <th width="10%" style="text-align:left"> Status </th>

                        
                        @elseif(\Request::input('view_type')=="Quotes")
                       
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Manufacturer </th>
                        <th width="10%" style="text-align:left"> Case Weight </th>
                        <th width="10%" style="text-align:left"> Quantity per Case </th>
                        <th width="10%" style="text-align:left"> Packaging </th>
                        <th width="10%" style="text-align:left"> Shipping Conditions </th>
                        <th width="10%" style="text-align:left"> Product Codes </th>
                        <th width="10%" style="text-align:left"> Description </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Unit of Measurement </th>
                        <th width="10%" style="text-align:left"> Quantity</th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> Expiry Date Range </th>
                        <th width="10%" style="text-align:left"> Exact Expiry Date </th>
                        <th width="10%" style="text-align:left"> Customer Reference Number </th>
                         <th width="10%" style="text-align:left"> Packaging Languages </th>
                        <th width="10%" style="text-align:left"> Timeframe </th>
                        <th width="10%" style="text-align:left"> Country of Origin</th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                         <th width="10%" style="text-align:left"> Posting ID </th>
                        <th width="10%" style="text-align:left"> Quote ID </th>
                        <th width="10%" style="text-align:left"> Creation Date</th>
                        <th width="10%" style="text-align:left"> Modified Date</th>
                        
            
                        @elseif(\Request::input('view_type')=="Offers")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Manufacturer </th>
                        <th width="10%" style="text-align:left"> Case Weight</th>
                        <th width="10%" style="text-align:left"> Quantity per Case </th>
                        <th width="10%" style="text-align:left"> Packaging </th>
                        <th width="10%" style="text-align:left"> Shipping Conditions </th>
                        <th width="10%" style="text-align:left"> Product Codes </th>
                        <th width="10%" style="text-align:left"> Description </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Unit of Measurement </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> Expiry Date Range </th>
                        <th width="10%" style="text-align:left"> Exact Expiry Date </th>
                        <th width="10%" style="text-align:left"> Customer Reference Number </th>
                        <th width="10%" style="text-align:left"> Packaging Languages </th>
                        <th width="10%" style="text-align:left"> Timeframe </th>
                        <th width="10%" style="text-align:left"> Country of Origin </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting ID </th>
                        <th width="10%" style="text-align:left"> Quote ID </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date </th>
                        
                        @elseif(\Request::input('view_type')=="Orders")
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Manufacturer </th>
                        <th width="10%" style="text-align:left"> Case Weight </th>
                        <th width="10%" style="text-align:left"> Quantity per Case </th>
                        <th width="10%" style="text-align:left"> Packaging </th>
                        <th width="10%" style="text-align:left"> Shipping Conditions </th>
                        <th width="10%" style="text-align:left"> Product Codes </th>
                        <th width="10%" style="text-align:left"> Description </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Unit of Measurement </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> Expiry Date Range </th>
                        <th width="10%" style="text-align:left"> Exact Expiry Date </th>
                        
                        <th width="10%" style="text-align:left"> Customer Reference Number </th>
                        <th width="10%" style="text-align:left"> Packaging Languages </th>
                        <th width="10%" style="text-align:left"> Timeframe </th>
                        <th width="10%" style="text-align:left"> Country of Origin </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting ID </th>
                        <th width="10%" style="text-align:left"> Quote ID </th>
                        <th width="10%" style="text-align:left"> Quote Facilitator </th>
                        <th width="10%" style="text-align:left"> Buyer </th>
                        
                        <th width="10%" style="text-align:left"> Seller </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date </th>
                       
                        @elseif(\Request::input('view_type')=="Customers")
                      
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Manufacturer </th>
                        <th width="10%" style="text-align:left"> Case Weight </th>
                        <th width="10%" style="text-align:left"> Quantity per Case </th>
                        <th width="10%" style="text-align:left"> Packaging </th>
                        <th width="10%" style="text-align:left"> Shipping Conditions </th>
                        <th width="10%" style="text-align:left"> Product Codes </th>
                        <th width="10%" style="text-align:left"> Description </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Unit of Measurement </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> Expiry Date Range </th>
                        <th width="10%" style="text-align:left"> Exact Expiry Date </th>
                        
                        <th width="10%" style="text-align:left"> Customer Reference Number </th>
                        <th width="10%" style="text-align:left"> Packaging Languages </th>
                        <th width="10%" style="text-align:left"> Timeframe </th>
                        <th width="10%" style="text-align:left"> Country of Origin </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting ID </th>
                        <th width="10%" style="text-align:left"> Customer Ref Number </th>
                        
                        @elseif(\Request::input('view_type')=="Users")
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        @endif
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <table class="table table-bordered" id="chartDetailTable">
                  <thead>
                    <tr role="row">
                      @if(\Request::input('view_type')=="Postings")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        
                        @elseif(\Request::input('view_type')=="Products")
                        
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Industry </th>
                         <th width="10%" style="text-align:left"> Category </th>
                          <th width="10%" style="text-align:left"> Sub Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> User ID </th>
                        <th width="10%" style="text-align:left"> Suggestion Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        @elseif(\Request::input('view_type')=="Quotes")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                         <th width="10%" style="text-align:left"> QuoteID </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date </th>
                        @elseif(\Request::input('view_type')=="Offers")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        <th width="10%" style="text-align:left"> QuoteID </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date </th>
                        @elseif(\Request::input('view_type')=="Orders")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        
                         <th width="10%" style="text-align:left"> QuoteID </th>
                        <th width="10%" style="text-align:left"> Quote Facilitator </th>
                        <th width="10%" style="text-align:left"> Buyer </th>
                         <th width="10%" style="text-align:left"> Seller </th>
                        <th width="10%" style="text-align:left"> Creation Date </th>
                        <th width="10%" style="text-align:left"> Modified Date</th>
                        
                        @elseif(\Request::input('view_type')=="Customers")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                         <th width="10%" style="text-align:left"> Customer Ref. No. </th>
                        
                        @elseif(\Request::input('view_type')=="Users")
                        <th width="10%" style="text-align:left"> Product Name </th>
                        <th width="15%" style="text-align:left"> Product ID </th>
                        <th width="10%" style="text-align:left"> Industry </th>
                        <th width="10%" style="text-align:left"> Category </th>
                        <th width="10%" style="text-align:left"> Sub-Category </th>
                        <th width="10%" style="text-align:left"> Brand </th>
                        <th width="10%" style="text-align:left"> Type </th>
                        <th width="10%" style="text-align:left"> Target Price </th>
                        <th width="10%" style="text-align:left"> Currency </th>
                        <th width="10%" style="text-align:left"> Quantity </th>
                        <th width="10%" style="text-align:left"> Location </th>
                        <th width="10%" style="text-align:left"> User Id </th>
                        <th width="10%" style="text-align:left"> Posting Date </th>
                        <th width="10%" style="text-align:left"> Status </th>
                        <th width="10%" style="text-align:left"> Posting Id </th>
                        @endif
                    </tr>
                  </thead>
                  <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

        </div>
    </div>
  </div>
@include('report.chartDetailsJs')
@endsection