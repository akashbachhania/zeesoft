@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          	 <h3 class="box-title">Reports & Statistics</h3>
          	 <input type="hidden" value="" id="allcategories" />
          </div>
          <div class="box-body">
          	<div class="row">
          		<div class="col-md-10 col-md-push-1">
          			<form action="{{ url('report/posting_pie_chart') }}" id="report_generate" name="report_generate" method="get" enctype="multipart/form-data" class="form-horizontal" onsubmit="return validateform();">
		          		<div class="form-body">
		                  <div id="loaderID" style="display:none;"><img src="addLoader.GIF"></div>
		                  
		                  <input type="hidden" name="key" value="40" />
		                  <input type="hidden" value="" id="allindustryids" />

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	 <label class="col-md-2 control-label">Section <span class="required">*</span></label>
			                  	 
			                  	 <div class="col-md-4" style="padding-left:0px;">
				                 
				                    <select class="form-control" name="Section" id="Section" tabindex="2">
				                 
				                        <option value="">Select Section</option>
				                        <option value="Postings">Postings</option>
				                        <option value="Products">Products</option>
				                        <option value="Quotes">Quotes</option>
				                        <option value="Offers">Offers</option>
				                        <option value="Orders">Orders</option>
				                        <option value="Customers">Customers</option>
				                 
				                    </select>
				                 
				                 </div>
				             </div>
			                    
		                  </div>

		                  <div class="form-group row">

		                  	<div class="col-md-6">
		                  		
		                  		<label class="col-md-4 control-label">Date Range <span class="required">*</span></label>
			                  	
			                  	<div class="col-md-8 input-group">
			                  		<div class="input-group-addon d">
				                        <i class="fa fa-calendar"></i>
				                    </div>
				                    <input type="text" class="form-control" name="from" id="from" readonly="readonly">
			                  	</div>	
		                  	
		                  	</div>
		                  	
		                  	<div class="col-md-6">
		                  		
		                  		<label class="col-md-4 control-label">To <span class="todate">*</span></label>
			                  	
			                  	<div class="col-md-8 input-group">
			                  		<div class="input-group-addon d">
				                        <i class="fa fa-calendar"></i>
				                    </div>
				                    <input type="text" class="form-control" name="to" id="to" readonly="readonly">
			                  	</div>	

		                  	</div>
		                  	
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Select Industry</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<div id="loadindustry"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Select Category</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<div id="cat"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Select Sub-Category</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<div id="subcat"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Brand Name</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<div id="brands"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Product Name</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<input autocomplete="off" type="text" class="search form-control" name="searchid" id="searchid" placeholder="Search Product Name...">
			                  			<div id="result" style="position:absolute; z-index:5; margin-left:5px; width:96%; font-weight:600; background-color: #ffffff; border: opx solid black; opacity: 1.2; filter: alpha(opacity=60); "></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Customer</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<div id="customerdata"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Status</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
			                  			<div id="status"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid" style="overflow-y: scroll;height: 150px;">
			                  		<div class="box-header">
			                  			<h3 class="box-title">User Id</h3>
			                  		</div>
			                  		<div class="box-body">
			                  			<div id="alluserid"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Type</h3>
			                  		</div>
			                  		<div class="box-body" style="overflow-y: scroll;height: 150px;">
										<label>
											<input type="checkbox" id="checkalltypeid" onclick="checkalltype();" name="type[]" value="Buy" />
											&nbsp;&nbsp;All
										</label><br />

                          				<label>
                          					<input type="checkbox" class="inditype" name="type[]" value="Buy" />
                          					&nbsp;&nbsp;Buy
                          				</label><br />
                          				
                          				<label>
                          					<input type="checkbox" class="inditype" name="type[]" value="Sell" />
                          					&nbsp;&nbsp;Sell
                          				</label><br />
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<div class="box box-solid" style="overflow-y: scroll;height: 150px;">
			                  		<div class="box-header">
			                  			<h3 class="box-title">Location</h3>
			                  		</div>
			                  		<div class="box-body">
			                  			<div id="alllocation"></div>	
			                  		</div>
			                  	</div>
			                 </div>
		                  </div>

		                  <div class="form-group row">
		                  	 <div class="col-md-12">
			                  	<input type="submit" name="addproduct" id="generateButton" value="Generate" class="btn btn-success pull-right">
			                 </div>
		                  </div>

			            </div>	            
		          	</form>		
          		</div>
          	</div>
          	
          </div>
        </div>
    </div>
  </div>
@include('report.js')
@endsection