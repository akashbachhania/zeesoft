@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
          <div class="box-header">
          	 <h3 class="box-title">
          	 	{{ $headTitle }}
          	 </h3>
          </div>

          <div class="box-body">
          	<div class="row">
          		<div class="col-md-12">
          			<div class="box box-success">
          				<div class="box-header" style="background: #00a65a">
          					<h3 class="box-title" style="color: #fff;"><i class="fa fa-gift"></i>&nbsp;&nbsp;{{ $ra }}</h3>
          				</div>
          				<div class="box-body" style="min-height: 300px;">
                      @if((!empty($selectedindustry)) || (!empty($status)) || (!empty($type)) || (!empty($alllocation)))
                      
                         <div id="pie_chart_4" class="chart" style="height: 400px!important;"></div>
                       
                      @else 
                         <div id="chart_1_2" class="chart" style="height: 400px!important;"></div>
                      @endif
          				</div>
                  <input type="hidden" id="sec" name="sec" value="{{ $sec }}" />
                  <input type="hidden" id="from" name="from" value="{{ $fromdate }}" />
                  <input type="hidden" id="to" name="to" value="{{ $todate }}" />
                  <input type="hidden" id="industry" name="industry" value="{{ $selectedindustry }}" />
                  <input type="hidden" id="cat_id" name="cat_id" value="{{ $selectcategory }}" />
                  <input type="hidden" id="sub_cat" name="sub_cat" value="{{ $selectsubcat }}" />
                  <input type="hidden" id="bname" name="bname" value="{{ $selectbrands }}" />
                  <input type="hidden" id="searchid" name="searchid" value="{{ $_REQUEST['searchid'] }}" />
                  <input type="hidden" id="customerdata" name="customerdata" value="{{ $customerdata }}" />
                  <input type="hidden" id="status" name="status" value="{{ $status }}" />
                  <input type="hidden" id="alluserid" name="alluserid" value="{{ $alluserid }}" />
                  <input type="hidden" id="type" name="type" value="{{ $type }}" />
                  <input type="hidden" id="alllocation" name="alllocation" value="{{ $alllocation }}" />
          			</div>
          		</div>
          	</div>
            <div class="row">
              <div class="col-md-2 col-md-push-10">
                <button type="button" class="btn btn-success zee-important-product" id="exportcsv">Export to CSV</button>
              </div>
            </div>

            <div class="row m_t30">
              <div class="col-md-12">
                <?php echo  $tags ?>
              </div>
            </div>

            <div class="row m_t30">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered" id="pie_table">
                    <thead>
                      <tr>
                        <th width="10%" style="text-align:left"> {{ $ra }} </th>
                        <th width="15%" style="text-align:left"> Value (%) </th>
                        <th width="10%" style="text-align:left"> Value (#)</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

        </div>
    </div>
  </div>
@include('report.pieChartJs')
@endsection