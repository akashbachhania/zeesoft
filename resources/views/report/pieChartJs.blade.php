<script>
jQuery(document).ready(function() {       

	ChartsFlotcharts.initPieCharts('{{ $filtername }}');
	
});
</script>

<script type="text/javascript">
    function saveData(val) {
        //alert(val);
        var promode = "deleteproduct";
        var formData = {
            pro_id: val,
            actionmode: promode
        };
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: formData,
            success: function(data)
            {
                alert(data);
            }
        });
    }
</script>
<script>

    function deleteme(obj) {
        var data = "Type=delete&productid=" + obj;
        if (confirm("Do You Want to Delete this?")) {
            $.ajax({
                type: 'GET',
                url: 'service/product_ajax.php',
                cache: false,
                data: data,
                success: function(data, status, xhr) {
                    window.location = 'product-catalog.php';
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var indmode = "industaction";
        var formData = {
            actionmode: indmode
        };
        $.ajax({
            type: "POST",
            url: root + "/report/functions",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $.each(data.itemData, function(i, data) {
                    //alert(data.industryid);
                    var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
                    $(div_data).appendTo("#industry");
                });
            }
        });
    });

    function getState(val) {
        var indmode = "category";
        $('#sub_cat').empty();
        var formData = {
            ind_id: val,
            actionmode: indmode
        };
        $.ajax({
            type: "POST",
            url: "functions.php",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $("#cat_id").empty();
                var div_data = "<option value=0>Select Category</option>";
                $(div_data).appendTo("#cat_id");
                $.each(data.itemData, function(i, data)
                    {
                        var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                        $(div_data).appendTo("#cat_id");
                    });
            }
        });
    }
    //sub category function start
    function subcat(val) {
        var indmode = "subcat";
        var formData = {
            cat_id: val,
            actionmode: indmode
        }; //Array 
        $.ajax({
            type: "POST",
            url: "functions.php",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $("#sub_cat").empty();
                var div_data = "<option value=0>Select Sub Category</option>";
                $(div_data).appendTo("#sub_cat");
                $.each(data.itemData, function(i, data)
                    {
                        var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                        $(div_data).appendTo("#sub_cat");
                    });
            }
        });
    }

    function getbrand(val) {
        var indmode = "brand";
        var formData = {
            ind_id: val,
            actionmode: indmode
        }; //Array 
        $.ajax({
            type: "POST",
            url: "functions.php",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $("#brand").empty();
                var div_data = "<option value=0>Select Brand</option>";
                $(div_data).appendTo("#brand");
                $.each(data.itemData, function(i, data)
                    {
                        var div_data = "<option value=" + data.brandid + ">" + data.name + "</option>";
                        $(div_data).appendTo("#brand");
                    });
            }
        });
    }
</script>

<script type="text/javascript">
	$('#exportcsv').click(function() {
		var sec=$("#sec").val();
	   var filtername=$("#filtername").val();
	   var from = $("#from").val();
	   var to=$("#to").val();
	   
	   
	   var industry=$("#industry").val();
	   var cat_id=$("#cat_id").val();
	   var sub_cat=$("#sub_cat").val();
	   var bname=$("#bname").val();
	   
	   
	   
	   var searchid=$("#searchid").val();
	   //alert(cat_id);
	   //alert('22222'+searchid);
	   var customerdata=$("#customerdata").val();
	   var status=$("#status").val();
	   var alluserid=$("#alluserid").val();
	   var type=$("#type").val();
	   var alllocation=$("#alllocation").val();

		var exportData = '';
		
		exportData = exportData.substring(0, exportData.length - 1);
		var activeproduct = $('input:checkbox[name=activeProduct]').is(':checked');
		if (activeproduct == true)
		{
			var isactive = '1';
		} else
		{
			var isactive = '0';
		}
		window.location = root +"/reports/pie_chart_csv?section=" + sec +"&filtername="+filtername+"&from="+from+"&to="+to+"&industry="+industry+"&cat_id="+cat_id+"&sub_cat="+sub_cat+"&bname="+bname+"&searchid="+searchid+"&customerdata="+customerdata+"&status="+status+"&alluserid="+alluserid+"&type="+type+"&alllocation="+alllocation;
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        
           var sec=$("#sec").val();
           var filtername=$("#filtername").val();
           var from = $("#from").val();
           var to=$("#to").val();
           
           
           var industry=$("#industry").val();
           var cat_id=$("#cat_id").val();
           var sub_cat=$("#sub_cat").val();
           var bname=$("#bname").val();
           
           
           
           var searchid=$("#searchid").val();
           //alert(cat_id);
           //alert('22222'+searchid);
           var customerdata=$("#customerdata").val();
           var status=$("#status").val();
           var alluserid=$("#alluserid").val();
           var type=$("#type").val();
           var alllocation=$("#alllocation").val();

        var pie_table = $('#pie_table').DataTable( {
            destroy:true,
            "ajax": {
                "url":root +"/report/posting_pie_chart_ajax?section=" + sec +"&filtername="+filtername+"&from="+from+"&to="+to+"&industry="+industry+"&cat_id="+cat_id+"&sub_cat="+sub_cat+"&bname="+bname+"&searchid="+searchid+"&customerdata="+customerdata+"&status="+status+"&alluserid="+alluserid+"&type="+type+"&alllocation="+alllocation,
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'name1' },
            { data: 'postpercent' },
            { data: 'industryname' } 
            ]
        } );
    
        ChartsFlotcharts.initBarCharts();
        $('#pie_table tbody').on('click', 'td', function() {
            var data = pie_table.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
        });

    });
        

</script>