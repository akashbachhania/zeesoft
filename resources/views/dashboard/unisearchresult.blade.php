@extends('layout.master')

@section('content')

	<div class="row m_t30">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Product Catalog</h3>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped" id="searchProductTable">
						<thead>
		                    <tr role="row">
		                      <th width="10%"> Product ID </th>
		                      <th width="15%"> Industry </th>
		                      <th width="15%"> Brand </th>
		                      <th width="32%"> Product Name </th>
		                      <th width="20%"> Product Codes </th>
		                       
							  @if ( Auth::user()->roletype =='AD')
		                   
		                      <th width="10%"> Status </th>
		                      
		                      @endif
		                    </tr>
		                </thead>
		                <tbody>
		                  	
		                </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row m_t30">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Posting Catalog</h3>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped" id="searchPostingTable">
						<thead>
		                    <tr role="row">
		                      <th width="10%"> Posting ID </th>
		                      <th width="10%"> Date Posted </th>
		                      <th width="10%"> Industry </th>
		                      <th width="10%"> Category </th>
		                      <th width="5%"> Brand </th>
		                      <th width="5%"> Type</th>
		                      <th width="5%"> Quantity</th>
		                      <th width="35%"> Product Name </th>
		                      <th width="10%"> Exp Date </th>
		                      
							  @if ( Auth::user()->roletype =='AD')
		                      
		                      <th width="10%"> Status </th>
		                      
		                      @endif
		                    </tr>
		                </thead>
		                <tbody>
		                </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@include('dashboard.unisearchresultJs')
@endsection