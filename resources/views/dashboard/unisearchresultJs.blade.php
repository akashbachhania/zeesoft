@if( Auth::user()->roletype == 'AD' )
   <script type="text/javascript">
    var searchProductTable = $('#searchProductTable').DataTable( {

        "ajax": {
            "url":root + "/dashboard/searchProductCatalog",
            "dataSrc": "",
            "type": 'POST',
            "data":{
                "searchname":"{{ \Request::input('searchname') }}"
            },
        },
        columns: [
        { data: 'productnoData' },
        { data: 'industry' },
        { data: 'brand' },
        { data: 'productName' },
        { data: 'code' },
        { data: 'edit' }
     
        ]
    });


    var searchPostingTable = $('#searchPostingTable').DataTable( {

        "ajax": {
            "url":root + "/dashboard/searchPostingCatalog",
            "dataSrc": "",
            "type": 'POST',
            "data":{
                "searchname":"{{ \Request::input('searchname') }}"
            },
        },
        columns: [
        { data: 'postno' },
        { data: 'sdate' },
        { data: 'industry' },
        { data: 'category' },
        { data: 'brand' },
        { data: 'ptype' },
        { data: 'quantity' },
        { data: 'productName' },
        { data: 'timeframe' },
        { data: 'enableaction'}
        ]
    });

  </script>  
@else

    <script type="text/javascript">
        var searchProductTable = $('#searchProductTable').DataTable( {

            "ajax": {
                "url":root + "/dashboard/searchProductCatalog",
                "dataSrc": "",
                "type": 'POST',
                "data":{
                    "searchname":"{{ \Request::input('searchname') }}"
                },
            },
            columns: [
            { data: 'productnoData' },
            { data: 'industry' },
            { data: 'brand' },
            { data: 'productName' },
            { data: 'code' }
         
            ]
        });

        var searchPostingTable = $('#searchPostingTable').DataTable( {

            "ajax": {
                "url":root + "/dashboard/searchPostingCatalog",
                "dataSrc": "",
                "type": 'POST',
                "data":{
                    "searchname":"{{ \Request::input('searchname') }}"
                },
            },
            columns: [
            { data: 'postno' },
            { data: 'sdate' },
            { data: 'industry' },
            { data: 'category' },
            { data: 'brand' },
            { data: 'ptype' },
            { data: 'quantity' },
            { data: 'productName' },
            { data: 'timeframe' }
         
            ]
        });


  </script>

@endif


<script type="text/javascript">
    function deleteme(obj) {
        // var data = "type=delete&productid=" + obj;
        
        var data = {
            type: 'delete',
           
            // _token: $('input[name=_token]').val(),
            id: obj
        };

        var status = confirm("Are you sure you wish to delete this product?");
        if (status == true) {
            $.ajax({
                type: 'POST',
                url: root+'/product/product_delete',
                cache: false,
                data: data,
                success: function(reponse) {
                // response = jQuery.parseJSON(data);
                    // if (response.msg == 'ERROR') {
                    //     alert(response.name)
                    // } else {
                    // window.location = '';
                    // }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        } else {
            window.location = '';
        }
    }
</script>