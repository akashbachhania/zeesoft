<script type="text/javascript">
	function shownoti(message) {
		$(function() {
			function Toast(type, css, msg) {
				this.type = type;
				this.css = css;
				this.msg = msg;
			}
			var toasts = [
				new Toast('success', 'toast-top-right', message),
			];
			toastr.options.positionClass = 'toast-top-full-width';
			toastr.options.extendedTimeOut = 0; //1000;
			toastr.options.timeOut = 2000;
			toastr.options.fadeOut = 250;
			toastr.options.fadeIn = 250;
			var i = 0;
			/* $('#tryMe').click(function () {

			     $('#tryMe').prop('disabled', true);*/
			delayToasts();
			//});
			function delayToasts() {
				if (i === toasts.length) {
					return;
				}
				var delay = i === 0 ? 0 : 2100;
				window.setTimeout(function() {
					showToast();
				}, delay);
				// re-enable the button        
				if (i === toasts.length - 1) {
					window.setTimeout(function() {
						// prop('disabled', false);
						i = 0;
					}, delay + 1000);
				}
			}

			function showToast() {
				var t = toasts[i];
				toastr.options.positionClass = t.css;
				toastr[t.type](t.msg);
				i++;
				delayToasts();
			}
		})
	}
</script>
@if(\Request::input('s') == "loggedin")
			
   <script>
		$(document).ready(
			function() {
				var id = "ndashboard";
				var lognoti = "lognoti";
				$.ajax({
					url: root + "/notiAlert",
					type: "post",
					data: {
						'id': id,
						'lognoti': lognoti
					},
					cache: false,
					success: function(res) {
						//alert(res);
						$.ajax({
							url: root + "/noti6",
							type: "post",
							data: {
								id: id
							},
							cache: false,
							dataType: 'html',
							success: function(res2) {
								//alert(res2);
								if (res2 == 'bothclose') {
								}
								if (res2 == 'audioclose') {
									//alert(res2);
									var message = res;
									shownoti(message);
								}
								if (res2 == 'vedioclose') {
									$('<audio  id="sound" controls="controls"><source src="" type="audio/ogg"><source src="assets/notifiles/3/Soft_Velvet_Logo.mp3" type="audio/mpeg"><source src="assets/notifiles/3/Soft_Velvet_Logo.wav" type="audio/wav"></audio>').appendTo('body');
									$('#sound')[0].play();
									//alert('');
								}
								if (res2 == 'bothstart') {
									//alert(res2);
									var message = res;
									shownoti(message);
									$('<audio  id="sound" controls="controls"><source src="" type="audio/ogg"><source src="assets/notifiles/3/Soft_Velvet_Logo.mp3" type="audio/mpeg"><source src="assets/notifiles/3/Soft_Velvet_Logo.wav" type="audio/wav"></audio>').appendTo('body');
									$('#sound')[0].play();
									//alert('ok');
								}
							}
						});
						//var message = res;
						//shownoti(message);
					}
				});
				//shownoti("User Created Successfully"); // refresh div after 5 secs
			});
</script>
@elseif(\Request::input('page') == "ndashboard")
			
   <script>
		$(document).ready(
			function() {
				var id = "<?php echo $_GET['page']; ?>";
				var lognoti = "lognoti";
				$.ajax({
					url: root + "/notiAlert",
					type: "post",
					data: {
						'id': id,
						'lognoti': lognoti
					},
					cache: false,
					success: function(res) {
						//alert(res);
						$.ajax({
							url: root + "/noti6",
							type: "post",
							data: {
								id: id
							},
							cache: false,
							dataType: 'html',
							success: function(res2) {
								//alert(res2);
								if (res2 == 'bothclose') {
								}
								if (res2 == 'audioclose') {
									//alert(res2);
									var message = res;
									shownoti(message);
								}
								if (res2 == 'vedioclose') {
									$('<audio  id="sound" controls="controls"><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									$('#sound')[0].play();
									//alert('');
								}
								if (res2 == 'bothstart') {
									//alert(res2);
									var message = res;
									shownoti(message);
									$('<audio  id="sound" controls="controls"><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									$('#sound')[0].play();
									//alert('ok');
								}
							}
						});
						//var message = res;
						//shownoti(message);
					}
				});
				//shownoti("User Created Successfully"); // refresh div after 5 secs
			});
</script>
@endif





<script type="text/javascript">
  $(document).ready(

    function() {
      setInterval('autoRefreshDiv()', 3000); // refresh div after 5 secs
    });
  autoRefreshDiv(); // call page loading time
  function autoRefreshDiv()
  {
    var id = 1;
    $.ajax({
      url: root+"/notidash",
      type: "post",
      data: {
        id: id
      },
      cache: false,
      dataType: 'html',
      success: function(res) {
        //alert(res);
        $('#noti').html(res);
      }
    });
  }
</script>

<script>
    function cancelme(obj) {
        var data = "Type=cancelpost&postid=" + obj;
        if (confirm("Do You Want to Cancel this Posting?")) {
            $.ajax({
                type: 'POST',
                url: root + '/posting/cancelPosting',
                cache: false,
                data: data,
                success: function(data, status, xhr) {
                    //document.write(data);
                    window.location = root + '/posting/myPosting?cancel=success';
                },
                error: function(jqXHR, textStatus, errorThrown) {}
            });
        }
    }
	jQuery(document).ready(function($) {
	    $(".clickable-row").click(function() {
	        window.document.location = $(this).data("href");
	    });
	});
</script>

@if( \Request::input('cancel') == "success" )
<script> 
	$(document).ready(function() {

			$.ajax({
				url: root + "/noti6",
				type: "post",
				cache: false,
				dataType:'html',
				success: function(res2){
					if(res2=='bothclose'){}

					if(res2=='audioclose')
					{
						var msg = "Posting Cancel successfully";
						shownoti(msg); 
					}
					if(res2=='vedioclose')
					{
						$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');

						$('#sound')[0].play(); 
					}
					if(res2=='bothstart')
					{
					
						var msg = "Posting Cancel successfully";
						shownoti(msg); 
						$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
						$('#sound')[0].play(); 
					
					}
				}
		});

	});
</script>
@endif
<script type="text/javascript">
	$(document).ready(function(){
		$('.small-box,#bho,#bhn').css('cursor','pointer');
	});
</script>
@if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
<script type="text/javascript">
	$('#mp').on('click',function(){
		window.location.href="{{ url('posting/myPosting') }}";
	});
</script>
@endif
@if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
<script type="text/javascript">
	$('#mo').on('click',function(){
		window.location.href="{{ url('posting/my-offers') }}";
	});
</script>
@endif
@if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
<script type="text/javascript">
	$('#pc').on('click',function(){
		window.location.href="{{ url('posting/postingCatalog') }}";
	});
</script>
@endif
@if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
<script type="text/javascript">
	$('#b').on('click',function(){
		window.location.href="{{ url('product/browse') }}";
	});
</script>
@endif
<script type="text/javascript">
	$('#bho').on('click',function(){
		window.location.href="{{ url('posting/myPosting') }}";
	});
</script>
<script type="text/javascript">
	$('#bhn').on('click',function(){
		window.location.href="{{ url('setting/notificationSetting') }}";
	});
</script>
<script type="text/javascript">

	if( ($('#o_ff_ers li').size() != 0) ){
		
	}

</script>