@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Add New Product</h3>
            <div class="pagination-panel pull-right"> 
               <a href="#" id="btnnew" class="btn green" data-toggle="modal">
                 <i class="fa fa-plus"> </i>&nbsp; Add New 
               </a> 
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            {!! Form::open(array('url' => 'product/store','method'=>'post','id'=>'form_add_product','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
            
               <div class="col-md-5">
                  <div class="blog-img blog-tag-data"> <img src="{{asset('img/large-blank.png')}}" alt="" class="img-responsive hideMeBoy"><br>
                     <div class="col-md-12" style="padding-left:0px;">
                        <div id="ParentDiv">
                           <div id="filediv">
                              <input name="file[]" type="file" id="file786" class="myClass" accept="image/*" />
                              <br>
                           </div>
                           <div id="filedivBawa" style="display:none;">
                              <input name="file[]" type="file" id="file786" class="myClass" accept="image/*" /><br>
                           </div>
                           <input type="button" id="add_more" class="upload btn btn-success" value="Upload"/>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Name <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" autocomplete="off" name="pname" placeholder="Product Name" maxlength="100" tabindex="1">
                        <div id="msgg" style="display: block; margin-bottom: 1px; clear: both; position: relative; top: -1px; margin-left:  6px;font-size: 14px;"> </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Industry <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <select class="form-control" name="industry" id="industry" tabindex="2">
                              <option value="">Select Industry</option>
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Category <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                         <select class="form-control" onchange="subcat(this.value);" name="category" id="cat_id" tabindex="3">
                           <option value="">Select Category</option>
                         </select>
                        <span id="catId_Loader" style="display:none;"><img src="{{asset('img/ajax-loader.gif')}}" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Sub-Category</label>
                     <div class="col-sm-6">
                       <select class="form-control" name="scategory" id="sub_cat" tabindex="4">
                          <option value="">Select Sub Category</option>
                        </select>
                        <span id="subcatId_Loader" style="display:none;"><img src="{{asset('img/ajax-loader.gif')}}"</span>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Brand Name  <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <select class="form-control" name="bname" id="bname" tabindex="5">
                          <option value="">Select Brand</option>
                        </select>
                        <span id="brandId_Loader" style="display:none;"><img src="{{asset('img/ajax-loader.gif')}}"</span> 
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Manufacturer</label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="mname" placeholder="Manufacturer" maxlength="50" tabindex="6">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Case Weight</label>
                     <div class="col-sm-6">
                       <input type="text" class="form-control" autocomplete="off" name="cweight" id="cweight" placeholder="Case Weight" maxlength="15" tabindex="7">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Packaging</label>
                     <div class="col-sm-6">

                          <select type="text" class="form-control" autocomplete="off" name="packaging"  tabindex="8">
                            <option value="">-Select Packaging-</option>
                            
                            @foreach( $packagings as $packaging )
                            <option value="{{ $packaging->name }}">{{ $packaging->name }}</option>
                            @endforeach
                          </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Shipping Conditions </label>
                     <div class="col-sm-6">
                         <select type="text" class="form-control" autocomplete="off" name="scondition[]" tabindex="9">
                           <?php sort($shippings) ?>
                           @foreach( $shippings as $shipping )

                           <option value="{{ $shipping }}">{{ $shipping }}</option>

                           @endforeach
                         </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Quantity Per Case </label>
                     <div class="col-sm-6">
                         <input type="text" class="form-control" autocomplete="off" name="qtycase" id="qtycase"   placeholder="Quantity Per Case" maxlength="10" tabindex="10">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <textarea class="form-control"  name="desc" id="desc" rows="3" tabindex="11"></textarea>
                     </div>
                  </div>
                  @if( Auth::user()->roletype != 'AM' )
                  <div class="form-group">
                     <label class="col-sm-3 control-label">MPM<span class="required"></span></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="MPM" id="MPM" placeholder="Enter MPM" maxlength="50" tabindex="12">
                     </div>
                  </div>
                  @endif
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Code 1 <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <select class="form-control" name="pcode1" id="pcode1"  tabindex="12">
                           <option value="">Select Prefix</option>
                           @foreach($ProductPrefixs as $ProductPrefix)

                              <option value="{{$ProductPrefix->proprefixid}}">{{$ProductPrefix->name}}</option>

                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label"><span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                         <input type="text" class="form-control" autocomplete="off" name="code1" id="code1" placeholder="Enter Product Code"  tabindex="13">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Code 2 </label>
                     <div class="col-sm-6">
                      <select class="form-control" name="pcode2" tabindex="14">
                           <option value="">Select Prefix</option>
                           @foreach($ProductPrefixs as $ProductPrefix)

                              <option value="{{$ProductPrefix->proprefixid}}">{{$ProductPrefix->name}}</option>

                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label"></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="code2" placeholder="Enter Product Code"  tabindex="15">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Code 3 </label>
                     <div class="col-sm-6">
                        <select class="form-control" name="pcode3" tabindex="16">
                           <option value="">Select Prefix</option>
                           @foreach($ProductPrefixs as $ProductPrefix)

                              <option value="{{$ProductPrefix->proprefixid}}">{{$ProductPrefix->name}}</option>

                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label"></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="code3" placeholder="Enter Product Code"  tabindex="17">
                        <input type="hidden" class="form-control" name="checkProduct" id="checkProduct" value="">
                     </div>
                  </div>
                  <div class="form-actions" style="background-color:#FFFFFF; border-top:0px;">
                      <div class="row">
                        <div class="col-sm-offset-3 col-sm-9">
                          <input type="submit" name="addproduct" id="submitButton" value="Submit" class="btn btn-success">
                          <button type="button" id="cancel" class="btn btn-default">Cancel</button>
                        </div>
                      </div>
               </div>
            </form>
         </div><!-- /.box -->
    </div>
  </div>
</section>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
 <script type='text/javascript'>
    $(window).load(function() {
        var inputLocalFont = document.getElementById("image-input");
        if (inputLocalFont) {
            inputLocalFont.addEventListener("change", previewImages, false);
        }

        function previewImages() {
            var fileList = this.files;
            var anyWindow = window.URL || window.webkitURL;
            for (var i = 0; i < fileList.length; i++) {
                var objectUrl = anyWindow.createObjectURL(fileList[i]);
                $('.preview-area').append('<img src="' + objectUrl + '" />');
                window.URL.revokeObjectURL(fileList[i]);
            }
        }
    });
</script>
<script type="text/javascript">
   jQuery(document).ready(function() {
       var indmode="industaction";
         var formData = {
            _token:$('input[name=_token]').val(),
            type:indmode
         };
         $.ajax({
            type: "POST",
            url: "{{url('setting/industry_ajax')}}",
            data: formData,
            dataType: 'json',
            success: function(data) {
               $.each(data.industry, function(i, data)
                  {
                  console.log(data);
                     var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
                     $(div_data).appendTo("#industry");
                  });
            }
         });
   });
    jQuery(document).ready(function() {
        $("#submitButton").click(function() {
            var checkProduct = $("#checkProduct").val();
            if (checkProduct == '0')
            {
                alert('Product Name Already exists')
                return false;
            }
            if ($("#name").val() == '') {
                alert('Product Name Required');
                return false;
            }
            if ($("#industry").val() == '') {
                alert('Industry Required');
                return false;
            }
            if ($("#cat_id").val() == '' || $("#cat_id").val() == 0)
            {
                alert('Category Required');
                return false;
            }
            if ($("#bname").val() == '' || $("#bname").val() == 0) {
                alert('Brand Required');
                return false;
            }
            if (!($.isNumeric($("#cweight").val()))) {
                alert('Case weight must be numeric');
                return false;
            }
            if (!($.isNumeric($("#qtycase").val()))) {
                alert('Quantity per case must be numeric');
                return false;
            }
            if ($("#desc").val() == '') {
                alert('Description Required');
                return false;
            }
            if ($("#pcode1").val() == '') {
                alert('Code Prefix Required');
                return false;
            }
            if ($("#code1").val() == '') {
                alert('Code 1 Required');
                return false;
            }
            // if ($("#file786").val() == '') {
            //     alert('Image required');
            //     return false;
            // } 
            else {
                $('.fadeBox').fadeTo("slow", 0.2);
                $("#loaderID").show();
                $("#form_add_product").submit();
            }
        });
        $("#qtycase").keyup(function() {
            var val = $("#qtycase").val();
            if (parseInt(val) < 0 || isNaN(val)) {
                alert("Please Enter Valid Value");
                $("#qtycase").val("");
                $("#qtycase").focus();
            }
        });
        $("#MPM").keyup(function() {
            var val = $("#MPM").val();
            if (parseInt(val) < 0 || isNaN(val)) {
                alert("Please Enter Valid Value");
                $("#MPM").val("");
                $("#MPM").focus();
            }
        })
        
        $("#cancel").click(function() {
            location.href = 'product-catalog.php?key=key';
        });
        $("#industry").change(function()
            {
                showCategoriesBy_IndustId($(this).val());
                showBrandBy_Industid($(this).val());
            });
    });
    /*---- On change to Industry  || Category -----------------------------------*/
    function showCategoriesBy_IndustId(val) {
        var indmode = "category";
        $("#catId_Loader").fadeIn("fast");
        var formData = {
            ind_id: val,
            type: indmode,
            _token:$('input[name=_token]').val()
        }; //Array 
        $.ajax({
            type: "POST",
            url: "{{url('setting/category_ajax')}}",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $("#catId_Loader").hide();
                $("#cat_id").empty();
                var div_data = "<option value=>Select Category</option>";
                $(div_data).appendTo("#cat_id");
                if (data != null) {
                    $.each(data.Category, function(i, data)
                        {
                            var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                            $(div_data).appendTo("#cat_id");
                        });
                } else {
                    alert('Category Records not found !');
                }
            }
        });
    }

    function showBrandBy_Industid() {
        $("#brandId_Loader").fadeIn("fast");
        var data = {
            industry_id: $("#industry").val(),
            _token:$('input[name=_token]').val(),
            type: 'brand'
        };
        $.ajax({
            type: "POST",
            url: "{{url('setting/brand_ajax')}}",
            data: data,
            dataType: 'json',
            success: function(data) {
                $("#brandId_Loader").hide();
                $("#bname").empty();
                var div_data = "<option value=0>Select Brand</option>";
                $(div_data).appendTo("#bname");
                if (data != null) {
                    $.each(data.Brand, function(i, data)
                        {
                            var div_data = "<option value=" + data.brandid + ">" + data.name + "</option>";
                            $(div_data).appendTo("#bname");
                        });
                } else {
                    alert('Brand Records not found !');
                }
            }
        });
    }

    function subcat(val) {
        $("#subcatId_Loader").fadeIn("fast");
        var indmode = "subcat";
        var formData = {
            cat_id: val,
            _token:$('input[name=_token]').val(),
            type: indmode
        }; //Array 
        $.ajax({
            type: "POST",
            url: "{{url('setting/subcategory_ajax')}}",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $("#subcatId_Loader").hide();
                $("#sub_cat").empty();
                var div_data = "<option value=>Select Sub Category</option>";
                $(div_data).appendTo("#sub_cat");
                if (data != null) {
                    $.each(data.Subcategory, function(i, data)
                        {
                            var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                            $(div_data).appendTo("#sub_cat");
                        });
                } else {
                    //alert('Sub Category not found !');
                }
            }
        });
    }
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#name").change(function() {
            var pname = $("#name").val();
            $.ajax({
                type: "POST",
                url: "{{url('product/ajax')}}",
                async: false,
                data: {
                    pname: $("#name").val()
                },
                success: function(data, status, xhr) {
                    //alert(data);
                    if (data == 'exist') {
                        $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                        $("#checkProduct").val('0');
                    } else {
                        $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                        $("#checkProduct").val('1');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>
<script src="{{asset('js/script.js')}}"></script>
@endsection