<script type="text/javascript">
jQuery(document).ready(function() {
 var indmode="industaction";
	var industry_id= "{{ $productInfo->industryid }}";
				// console.log(industry_id);
    var formData = {
        type: indmode
    };
    $.ajax({
        type: "POST",
        url: "{{url('setting/industry_ajax')}}",
        data: formData,
        dataType: 'json',
        success: function(data) {
            $.each(data.industry, function(i, data)
                {

				var selectedvalue="";
				if(industry_id==data.industryid)
				{
				var selectedvalue="selected='selected'";
				}
                    var div_data = "<option value=" + data.industryid + " " + selectedvalue + ">" + data.name + "</option>";
                    $(div_data).appendTo("#industryDropdown");
                });
        }
    });
});
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$("#submitButton").click(function(event){
			event.preventDefault();
			if($("#name").val()==''){
					alert('Product Name Required');
					return false;
			}
			if($("#industryDropdown").val()==''){
					alert('Industry Required');
					return false;
			}
			if($("#cat_id").val()==0){
					alert('Category Required');
					return false;
			}
			if($("#bname").val()==0){
					alert('Brand Required');
					return false;
			}
			if($("#desc").val()==''){
					alert('Description Required');
					return false;
			}
			if($("#pcode1").val()==''){
					alert('Code Prefix Required');
					return false;
			}
			if($("#code1").val()==''){
					alert('Code 1 Required');
					return false;
			}
			if($("#pstatus").val()==''){
					alert('Product Status Required');
					return false;
			}
			if ($("#authpass").val() == '') {
                alert('Authorized code is required');
                $("#authpass").parent().parent().addClass('has-error');
                $("#authpass").focus();
                return false;
            } else {
                $.get(root + '/checkauthpass',{authpass:$("#authpass").val()},function(data){
                  if(data == 'success'){
                    $('.fadeBox').fadeTo("slow", 0.2);
                    $("#loaderID").show();
                    $("#form_update_product").submit();      
                  }
                  else{
                    alert('Authorized Password Incorrect!');
                    $("#authpass").parent().parent().addClass('has-error');
                    $("#authpass").focus();
                  }
                  
                });
            }
	    });

	function showSelected_Category(){
		var data= {industry_id:"{{ $productInfo->industryid }}",Mode:'showCategory_ByIndustry'}; //Array 
		$.ajax({
			type: "POST",
			url: root + "/setting/genFun",
			data: data,
			dataType:'json',
			success:function(data){
				$("#cat_id").empty();
				var div_data ="<option value=0>Select Category</option>";
				$(div_data).appendTo("#cat_id");
				$.each(data.itemData,function(i,data) 
				{ 
						var div_data ="<option value="+data.catid+">" + data.name+"</option>";
						$(div_data).appendTo("#cat_id");
				});
				$("#cat_id").val("{{ $productInfo->catid }}");
			}
		});
	}
	showSelected_Category();

	function showSelected_SubCategory(){
		var data= {cat_id:"{{ $productInfo->catid }}",Mode:'showSubCategory_byCatid'}; //Array 
		$.ajax({
			type: "POST",
			url: root + "/setting/genFun",
			data: data,
			dataType:'json',
			success:function(data){
				//alert(JSON.stringify(data));
				$("#sub_cat").empty();
				var div_data ="<option value=0>Select Subcategory</option>";
				$(div_data).appendTo("#sub_cat");
				$.each(data.itemData,function(i,data) 
				{ 
						var div_data ="<option value="+data.subcatid+">" + data.name+"</option>";
						$(div_data).appendTo("#sub_cat");
				});
				$("#sub_cat").val("{{ $productInfo->subcatid }}");
			}
		});
	}
	showSelected_SubCategory();

	$("#cancel").on('click',function(){ 
		location.href=root + 'product/browse'; 
	});

	$("#cweight").keyup(function(){

	  var val=$("#cweight").val();
	  
	  if(parseInt(val) < 0 || isNaN(val)){  
		alert("Please Enter Valid Value"); $("#cweight").val(""); $("#cweight").focus(); 
      }
	});

	$("#MPM").keyup(function(){var val=$("#MPM").val();
      if(parseInt(val) < 0 || isNaN(val)){  
					alert("Please Enter Valid Value"); $("#MPM").val(""); $("#MPM").focus(); 
			}
    })

	$("#industryDropdown").change(function()
	{ 
			$("#sub_cat").empty();
			var div_data="<option value=0>Select Subcategory</option>";	
			$(div_data).appendTo("#sub_cat");
			
			showCategoriesBy_IndustId($(this).val());
			showBrandBy_Industid($(this).val()); 
			
	});

    });
</script>
<script type="text/javascript">
function showCategoriesBy_IndustId(val) {
    var indmode = "category";
    $("#catId_Loader").fadeIn("fast");
    var formData = {
        ind_id: val,
        type: indmode,
        _token:$('input[name=_token]').val()
    }; //Array 
    $.ajax({
        type: "POST",
        url: "{{url('setting/category_ajax')}}",
        data: formData,
        dataType: 'json',
        success: function(data) {
            $("#catId_Loader").hide();
            $("#cat_id").empty();
            var div_data = "<option value=>Select Category</option>";
            $(div_data).appendTo("#cat_id");
            if (data != null) {
              

                $.each(data.Category, function(i, data)
                    {
              
                      // var catid = data.catid;
                      // var pcatid = '{{ $productInfo->catid }}';
                      // if( catid == pcatid ){
                      //   var div_data = "<option value=" + data.catid + " selected='selected'>" + data.name + "</option>";
                      // subcat(data.catid);        
                      // }
                      // else{
                        var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                        
                      // }
                      $(div_data).appendTo("#cat_id");
                        
                    });
            } else {
                alert('Category Records not found !');
            }
        }
    });
}
function showBrandBy_Industid() {
    $("#brandId_Loader").fadeIn("fast");
    var data = {
        industry_id: $("#industryDropdown").val(),
        _token:$('input[name=_token]').val(),
        type: 'brand'
    };
    $.ajax({
        type: "POST",
        url: "{{url('setting/brand_ajax')}}",
        data: data,
        dataType: 'json',
        success: function(data) {
            $("#brandId_Loader").hide();
            $("#bname").empty();
            var div_data = "<option value=0>Select Brand</option>";
            $(div_data).appendTo("#bname");
            if (data != null) {
                $.each(data.Brand, function(i, data)
                    {
                        var div_data = "<option value=" + data.brandid + ">" + data.name + "</option>";
                     
                        $(div_data).appendTo("#bname");
                    });
            } else {
                alert('Brand Records not found !');
            }
        }
    });
}
function subcat(val) {
    $("#subcatId_Loader").fadeIn("fast");
    var indmode = "subcat";
    var formData = {
        cat_id: val,
        _token:$('input[name=_token]').val(),
        type: indmode
    }; //Array 
    $.ajax({
        type: "POST",
        url: "{{url('setting/subcategory_ajax')}}",
        data: formData,
        dataType: 'json',
        success: function(data) {
            $("#subcatId_Loader").hide();
            $("#sub_cat").empty();
            var div_data = "<option value=>Select Sub Category</option>";
            $(div_data).appendTo("#sub_cat");
            if (data != null) {
                $.each(data.Subcategory, function(i, data)
                    {


                        var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                        
                        $(div_data).appendTo("#sub_cat");
                    });
            } else {
                //alert('Sub Category not found !');
            }
        }
    });
}
</script>