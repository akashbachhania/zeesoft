<script type="text/javascript">
    var reportedProductTable = $('#reportedProductTable').DataTable( {

		"ajax": {
		    "url":"reportedProduct_ajax",
		    "dataSrc": "",
		    "type": 'POST',
		},
		columns: [
		{ data: 'suggestdate' },
		{ data: 'userid' },
		{ data: 'industry' },
		{ data: 'brand'},
		{ data: 'productName' },
		{ data: 'reportCode' },
		{ data: 'msg' }

		]
	} );

	setInterval( function () {
	    reportedProductTable.ajax.reload(null,false);
	}, 3000 );

	$(document).on('click', '#reportedProductTable tbody tr', function () {   
	    var data = reportedProductTable.row(this).data();

		var page = $(data.productName).attr('href');

		window.location.href = page;
    });
</script>
<script type="text/javascript">
 function shownoti(message){
	$(function() {

    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];

    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;

    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})

}
</script>
@if(\Request::input('suggestpreport')=='true')

	<script> 
		$(document).ready(function() {
			$.ajax({
				url: root + "/noti6",
				type: "post",
				cache: false,
				dataType:'html',
				success: function(res2){

					if(res2=='bothclose')
					{

					}
					if(res2=='audioclose')
					{
						shownoti("Suggested on a Product Report Successfully");
					}
					if(res2=='vedioclose')
					{
						$('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root+'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');

						$('#sound')[0].play(); 
					}
					if(res2=='bothstart')
					{
						shownoti("Suggested on a Product Report Successfully");
						$('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root+'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
						$('#sound')[0].play(); 
					}
				}
			});
		});
	</script>
@endif