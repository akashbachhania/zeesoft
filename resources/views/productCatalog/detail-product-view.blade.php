@extends('layout.master')

@section('content')

          <div class="row">
          	<div class="col-md-4">
          		<div class="box box-primary" style="min-height: 486px">
	                <div class="box-header with-border">
	                	<h4>Product ID: {{ $product->productno }}</h4>
	                </div><!-- /.box-header -->
	                <div class="box-body">
	                  @if( empty($image[0]) )
	                  
	                  	<img class="img-responsive" src="{{ asset('img/noimg.jpg') }}" style="min-height: 400px">
	 
	                  @else

						<div class="BigImage">
                          <div class="MagicZoom" id="image">
              				<div class="MagicZoom">
                        	  <a href="{{ url('productimg/').'/'. ($image[0]->imageurl) }}" id="imageshref" class="MagicZoom" rel="zoom-position:inner;zoom-fade:true">
                      			<img class="img-responsive" src="{{ url('productimg/thumb').'/'. ($image[0]->imageurl) }}" style="min-height: 400px; max-width: 100%!important;">
                      		 </a>
                          	</div>
                          </div>
                        </div>
	                
	                  @endif
	                </div><!-- /.box-body -->
	              </div>
          	</div>

            <div class="col-md-8">
              <div class="box">
                <div class="box-header">
                  <div class="row">
                    <div class="col-md-4">
                        <h3 class="box-title">Product Details</h3>    
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                      <form action="#" class="form-horizontal" name="createad" id="createad">
                        <div class="form-group row">
                          <div class="col-md-9">
                            <select class="form-control" name="selAd" id="selAd">
                              <option value="CreateNewAd">Create New Ad</option>
                              <option value="Buy">View Buy Ads</option>
                              <option value="Sell">View Sell Ads</option>
                            </select>
                          </div>
                          <div class="col-md-3 clear-mar">
                            <button type="button" class="btn btn-success green" id="createselad"> Go</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-md-push-9">
                      @if((Auth::user()->roletype == "AD")||(Auth::user()->roletype == "SA"))
                      @else
                        <a class="btn btn-info" data-toggle="modal" href="#static23"><b>Report Product</b></a>
                      @endif
                    </div>
                  </div>
                  
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="productviewtable" class="table table-hover">
                    <tr>
                      <td><b>Product Name</b></td>
                      <td>:</td>
                      <td>{{ $product->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Industry</b></td>
                      <td>:</td>
                      <td>{{ $product->industry->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Category</b></td>
                      <td>:</td>
                      <td>{{ $product->category->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Sub-Category</b></td>
                      <td>:</td>
                      <td>{{ $product->subcategory->name or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Brand</b></td>
                      <td>:</td>
                      <td>{{ $product->brand->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Manufacturer</b></td>
                      <td>:</td>
                      <td>{{ $product->manufacture or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Case Weight</b></td>
                      <td>:</td>
                      <td>{{ $product->caseweight or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Quantity Per Case</b></td>
                      <td>:</td>
                      <td>{{ ($product->qtypercase == 0)?'':$product->qtypercase }}</td>
                    </tr>
                    <tr>
                      <td><b>Packing</b></td>
                      <td>:</td>
                      <td>{{ $product->pakaging or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Shipping Status</b></td>
                      <td>:</td>
                      <td>{{ $product->shipingcondition or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Minimum Profit Margin</b></td>
                      <td>:</td>
                      <td>{{ ($product->mpm == 0)?'':$product->mpm }} {{ ($product->mpm == 0)?'':' %' }}</td>
                    </tr>
                    <tr>
                      <td><b>Product Codes</b></td>
                      <td>:</td>
                      <td>
                      <?php $r = 0; ?>
	                      @foreach( $product->productCode as $code )
	                      
	                      	{{ str_ireplace(':',': ',str_ireplace(' ','',$code->productcode)) }}{{ ($r == (count($product->productCode) - 1))?'':';' }}      
	                      <?php $r++; ?>

	                      @endforeach

	                     &nbsp;&nbsp;
	                     <a data-toggle="modal" href="#static">+Add</a>                
                      </td>
                    </tr>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
          <div class="row">
          	<div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Description</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Related Products</a></li>
                  <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">Recommended</a></li>

                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
					           {{ $product->description }}                  
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                  	<div class="row">
                    <?php $t = 1; ?>
                      @foreach( $relatedProducts as $relatedProduct )
                        
                        @if( $t == 7 )

                          <?php break ?>

                        @endif
                        <div class="col-md-2">
                        @foreach( $relatedProduct->productImage as $rpi )
                         <?php $asd = $rpi->imageurl;
                            break;
                          ?>
                        
                        @endforeach
                        @if( count($relatedProduct->productImage) != 0 )
                          
                            <a href="{{ url('product/detail-product-view') }}/{{ $relatedProduct->productid }}"> <img src="{{ url('productimg/thumb').'/'. $asd }}"  width="100%" style="min-height:180px;" alt=""/> </a>
                          

                        @else

                          <a href="{{ url('product/detail-product-view') }}/{{ $relatedProduct->productid }}"> <img src="{{ url('productimg/image4.jpg') }}" width="100%" style="min-height:180px;"  alt=""> </a>

                        @endif
                        <div align="center" style="word-wrap: break-word;">
                          <label> <a href="{{ url('product/detail-product-view') }}/{{ $relatedProduct->productid }}"> {{ $relatedProduct->name }} </a> </label>
                        </div>
                        </div>
                        <?php $t++ ?>
                    	@endforeach
                    </div>
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                     <div class="row">
                       <?php $t = 1; ?>
                      @foreach( $recommendedProducts as $recommendedProduct )
                        
                        @if( $t == 7 )

                          <?php break ?>

                        @endif
                        <div class="col-md-2">
                        @foreach( $recommendedProduct->productImage as $rpi )
                         <?php $asd = $rpi->imageurl;
                            break;
                          ?>
                        
                        @endforeach
                        @if( count($recommendedProduct->productImage) != 0 )
                          
                            <a href="{{ url('product/detail-product-view') }}/{{ $recommendedProduct->productid }}"> <img src="{{ url('productimg/thumb').'/'. $asd }}"  width="100%" style="min-height:180px;" alt=""/> </a>
                          

                        @else

                          <a href="{{ url('product/detail-product-view') }}/{{ $recommendedProduct->productid }}"> <img src="{{ url('productimg/image4.jpg') }}" width="100%" style="min-height:180px;"  alt=""> </a>

                        @endif
                        <div align="center" style="word-wrap: break-word;">
                          <label> <a href="{{ url('product/detail-product-view') }}/{{ $recommendedProduct->productid }}"> {{ $recommendedProduct->name }} </a> </label>
                        </div>
                        </div>
                        <?php $t++ ?>
                      @endforeach
                     </div>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div>
          </div>

  <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#3aada3;"><strong>Add Product Code "{{ $product->name }}"</strong></h4>
        </div>
        <div class="modal-body">
          <form action="" id="form_productcode" class="form-horizontal" novalidate="novalidate">
            <input type="hidden" class="form-control" name="pid" id="pid" value="{{ \Request::segment(3) }}">
            <div class="form-group" style="margin-top:5px;" >
              <div class="col-md-12">
                
                <select class="form-control" name="prefix" id="prefix">
                
                  <option value="">Select Prefix</option>

                  @foreach( $productPrefixs as $prefix )
                  
                  <option value="{{ $prefix->proprefixid }}">{{$prefix->name}}</option>
                  
                  @endforeach
                
                </select>

              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" name="code" id="code" placeholder="Enter Code">
                <p id="re"></p>
              </div>
            </div>
            <div class="form-actions" align="left">
              <div class="row">
                <div class="col-md-12">
                  <button type="button" id="btnsave" class="btn btn-primary">Submit</button>
                  <button type="button" id="btnclose" data-dismiss="modal" class="btn btn-default">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="static23" class="modal fade" tabindex="-1" data-backdrop="static23" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#3aada3;"><strong>Report Product "Product ID: {{ $product->productno }}"</strong></h4>
      </div>
      <div class="modal-body">
        <form action="#" id="form_product_report" name="form_product_report" class="form-horizontal" novalidate="novalidate">
          <input type="hidden" class="form-control" name="type" id="type" value="addreport">
          <input type="hidden" class="form-control" name="proid" id="proid" value="{{ \Request::segment(3) }}">
          <div class="form-body">
          <div class="form-group" style="margin-top:5px;" >
            <div class="col-md-12">
              <select class="form-control" name="reason" id="reason">
                <option value="0">Select Reason</option>
                @foreach( $reportReason as $reason )
                  <option value="{{ $reason->reportid }}">{{ $reason->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <textarea class="form-control" name="detail" id="detail" placeholder="Enter Detail "></textarea>
            </div>
          </div>
          <div class="form-actions" align="left">
            <div class="row">
              <div class="col-md-12">
                <button type="button" id="btnsaverreport" class="btn btn-info" data-dismiss="modal">Submit</button>
                <button type="button" id="btnclosereport" data-dismiss="modal" class="btn btn-default">Cancel</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@include('productCatalog.detail-product-viewJs')
@endsection