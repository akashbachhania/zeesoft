@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Review Suggested Product {{ $productInfo->name }}</h3>
            <!-- <div class="pagination-panel pull-right">  -->
               <!-- <a href="#" id="btnnew" class="btn green" data-toggle="modal"> -->
                 <!-- <i class="fa fa-plus"> </i>&nbsp; Add New  -->
               <!-- </a>  -->
            <!-- </div> -->
          </div><!-- /.box-header -->
          <div class="box-body">
            {!! Form::open(array('url' => 'product/suggestProductupdate/'.$productInfo->productid.'','method'=>'post','id'=>'form_update_product','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
            
               <div class="col-md-5">
                  <div class="blog-img blog-tag-data"> 
                    @if( count($productImage) > 0 )

                        <?php $i = 0; ?>
                        @foreach( $productImage as $pi)
                        
                        <img src="{{ url('productimg/thumb').'/'. $pi->imageurl }}" alt="" class="img-responsive hideMeBoy"><br>
                        <?php $i++ ?>
                        @if( $i == 1 )
                           
                           @break
                        
                        @endif
                        @endforeach
                    
                    @else

                    <img src="{{ asset('img/large-blank.png') }}" alt="" class="img-responsive hideMeBoy"><br>
                    
                    @endif


                     <div class="col-md-12" style="padding-left:0px;">
                        <div id="ParentDiv">
                           <div id="filediv">
                              <input name="file[]" type="file" id="file786" class="myClass" accept="image/*" />
                              <br>
                           </div>
                           <div id="filedivBawa" style="display:none;">
                              <input name="file[]" type="file" id="file786" class="myClass" accept="image/*" /><br>
                           </div>
                           <input type="button" id="add_more" class="upload btn btn-success" value="Upload"/>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product ID <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="pid" name="pid" placeholder="Product ID" value="{{ $productInfo->productno }}" disabled="disabled">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Name <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" autocomplete="off" name="pname" placeholder="Product Name" maxlength="100" tabindex="1" value="{{ $productInfo->name }}">
                        <div id="msgg" style="display: block; margin-bottom: 1px; clear: both; position: relative; top: -1px; margin-left:  6px;font-size: 14px;"> </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Industry <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <select class="form-control" name="industry" id="industryDropdown" tabindex="2">
                              <option value="">Select Industry</option>
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Category <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                         <select class="form-control" onchange="subcat(this.value);" name="category" id="cat_id" tabindex="3">
                           <option value="">Select Category</option>
                         </select>
                        <span id="catId_Loader" style="display:none;"><img src="{{asset('img/ajax-loader.gif')}}" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Sub-Category</label>
                     <div class="col-sm-6">
                       <select class="form-control" name="scategory" id="sub_cat" tabindex="4">
                          <option value="">Select Sub Category</option>
                        </select>
                        <span id="subcatId_Loader" style="display:none;"><img src="{{asset('img/ajax-loader.gif')}}"</span>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Brand Name  <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <select class="form-control" name="bname" id="bname" tabindex="5">
                          <option value="">Select Brand</option>
                          @foreach( $brands as $brand )

                            <option value="{{ $brand->brandid }}" {{ ($brand->brandid == $productInfo->brandid) ? 'selected="selected"' : ''}}>{{ $brand->name }}</option>

                          @endforeach
                        </select>
                        <span id="brandId_Loader" style="display:none;"><img src="{{asset('img/ajax-loader.gif')}}"</span> 
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Manufacturer</label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="mname" placeholder="Manufacturer" maxlength="50" tabindex="6" value="{{ $productInfo->manufacture }}">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Case Weight</label>
                     <div class="col-sm-6">
                       <input type="text" class="form-control" autocomplete="off" name="cweight" id="cweight" placeholder="Case Weight" maxlength="15" tabindex="7" value="{{ $productInfo->caseweight }}">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Packaging</label>
                     <div class="col-sm-6">

                        <select type="text" class="form-control" autocomplete="off" name="packaging"  tabindex="8">
                          <option value="">-Select Packaging-</option>
                          
                          @foreach( $packagings as $packaging )
                          <option value="{{ $packaging->name }}" {{ ( $productInfo->pakaging == $packaging->name )?'selected="selected"':'' }}>{{ $packaging->name }}</option>
                          @endforeach
                        </select>
<!--                           <input type="text" class="form-control" autocomplete="off" name="packaging" placeholder="Packaging" maxlength="50" tabindex="8" value="{{ $productInfo->pakaging }}"> -->
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Shipping Conditions </label>
                     <div class="col-sm-6">
                        <select multiple type="text" class="form-control" autocomplete="off" name="scondition[]" tabindex="9">
                           <?php sort($shippings) ?>
                           @foreach( $shippings as $shipping )

                           <option value="{{ $shipping }}" {{ ( $productInfo->shipingcondition == $shipping )?'selected="selected"':'' }}>{{ $shipping }}</option>

                           @endforeach
                         </select>
<!--                          <input type="text" class="form-control" autocomplete="off" name="scondition" placeholder="Shipping Conditions" maxlength="100" tabindex="9" value="{{ $productInfo->shipingcondition }}"> -->
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Quantity Per Case </label>
                     <div class="col-sm-6">
                         <input type="text" class="form-control" autocomplete="off" name="qtycase" id="qtycase"   placeholder="Quantity Per Case" maxlength="10" tabindex="10" value="{{ $productInfo->qtypercase }}">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <textarea class="form-control"  name="desc" id="desc" rows="3" tabindex="11"> {{ $productInfo->description }}</textarea>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">MPM<span class="required"></span> <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="MPM" id="MPM" placeholder="Enter MPM" maxlength="50" tabindex="12" value="{{ $productInfo->mpm }}">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Code 1 <span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                        <select class="form-control" name="pcode1" id="pcode1"  tabindex="12">
                           <option value="">Select Prefix</option>
                           @foreach($ProductPrefixs as $ProductPrefix)

                              <option value="{{$ProductPrefix->proprefixid}}" <?= ( $productInfo->productCode[0]->prefixid == $ProductPrefix->proprefixid ) ? 'selected="selected"' : '' ?> >{{$ProductPrefix->name}}</option>

                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label"><span class="text-danger">*</span></label>
                     <div class="col-sm-6">
                         <input type="text" class="form-control" autocomplete="off" name="code1" id="code1" placeholder="Enter Product Code"  tabindex="13" value="{{ $productInfo->productCode[0]->code }}">
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Code 2 </label>
                     <div class="col-sm-6">
                      <select class="form-control" name="pcode2" tabindex="14">
                           <option value="">Select Prefix</option>

                             @foreach($ProductPrefixs as $ProductPrefix)

                               <option value="{{$ProductPrefix->proprefixid}}" >{{$ProductPrefix->name}}</option>

                             @endforeach

                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label"></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="code2" placeholder="Enter Product Code"  tabindex="15" value="{{ ( count($productInfo->productCode) > 1 ) ? $productInfo->productCode[1]->code : '' }}">
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Product Code 3 </label>
                     <div class="col-sm-6">
                        <select class="form-control" name="pcode3" tabindex="16">
                           <option value="">Select Prefix</option>
                           @foreach($ProductPrefixs as $ProductPrefix)

                              <option value="{{$ProductPrefix->proprefixid}}">{{$ProductPrefix->name}}</option>

                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label"></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" autocomplete="off" name="code3" placeholder="Enter Product Code" tabindex="17">
                        <input type="hidden" class="form-control" name="checkProduct" id="checkProduct" value="">
                     </div>
                  </div>


                  @if( Auth::user()->roletype == 'AD' )
                    <div class="form-group">
                      <label class="col-md-3 control-label">Status</label>
                      <div class="col-md-6">
                        <select class="form-control" name="pstatus" id="pstatus" tabindex="16" >
                          <option value="">Select Status</option>
                          <option value="WAITING" {{ ($productInfo->pstatus == 'WAITING') ? 'selected="selected"' : '' }} >WAITING</option>
                          <option value="PROCESSING" {{ ($productInfo->pstatus == 'PROCESSING') ? 'selected="selected"' : '' }} >PROCESSING</option>
                          <option value="APPROVED" {{ ($productInfo->pstatus == 'APPROVED') ? 'selected="selected"' : '' }} >APPROVED</option>
                          <option value="REJECTED" {{ ($productInfo->pstatus == 'REJECTED') ? 'selected="selected"' : '' }} >REJECTED</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group"> 
                      <label class="col-md-3 control-label">Authorize</label>
                      <div class="col-md-6">
                        <input type="password" autocomplete="off" class="form-control" id="authpass" name="authpass" placeholder="Authorize Password">
                      </div>
                    </div>

                  @endif


                  <div class="form-actions" style="background-color:#FFFFFF; border-top:0px;">
                      <div class="row">
                        <div class="col-sm-offset-3 col-sm-9">
                          <input type="submit" name="addproduct" id="submitButton" value="Submit" class="btn btn-success green">
                          <button type="button" id="cancel" class="btn btn-default">Cancel</button>
                        </div>
                      </div>
               </div>
            </form>
         </div><!-- /.box -->
    </div>
  </div>
</section>
@include('productCatalog.spJs')
@endsection