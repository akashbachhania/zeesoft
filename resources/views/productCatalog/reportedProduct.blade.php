@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Reported Products</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="reportedProductTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th> Date Suggested </th>
                  <th> User ID </th>
                  <th> Industry </th>
                  <th> Brand </th>
                  <th> Product Name </th>
                  <th> Report Type </th>
                  <th> Status </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('productCatalog.reportedProductJs')
@endsection