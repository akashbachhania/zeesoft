<script type="text/javascript">

function deleteme(obj) {

    var data = {
        type: 'delete',
       
        _token: $('input[name=_token]').val(),
        id: obj
    };
    if (confirm("Are you sure you wish to delete this Product?")) {
        $.ajax({
            type: 'POST',
            url: root+'/product/product_delete',
            cache: false,
            data: data,
            
            success: function(data) {
                
                if (data.msg == 'ERROR') {
                    alert(data.name)
                } else {
       
                    // window.location = root + '/product/browse';
                }
                 // $('#datatable_ajax').DataTable().ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }
}


jQuery(document).ready(function() {
   
    var productCatalogTable1 = $('#product-catalog1').DataTable( {

        "ajax": {
            "url":"browseProduct_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'productid' },
        { data: 'industry' },
        { data: 'category' },
        { data: 'brand' },
        { data: 'productname' },
        { data: 'productcodes' }
     
        ]
    } );

    // setInterval( function () {
    //     productCatalogTable1.ajax.reload();
    // }, 3000 );


    var productCatalogTable2 = $('#product-catalog2').DataTable( {

        "ajax": {
            "url":"browseProduct_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'productid' },
        { data: 'industry' },
        { data: 'category' },
        { data: 'brand' },
        { data: 'productname' },
        { data: 'productcodes' },
        { data: 'action' }
     
        ]
    } );

    // setInterval( function () {
    //     productCatalogTable2.ajax.reload();
    // }, 3000 );      


    $('#dvfilter').hide();
   
    $('#show').click(function() {

        $('#dvfilter').toggle('slow');
        // document.cookie ="show=showopen";
        // document.cookie ="bStateSave=true";
    });
   
    // var showvalue = '';
   
    // if (showvalue == 'showopen') {
    //     $('#dvfilter').toggle('slow');
    // }
   
    $('#product-catalog1 tbody').on('click', 'tr td:not(:last-child)', function() {
        
        var data = productCatalogTable1.row(this).data();
        var page = $(this).parent().find('td a').attr('href');
        // alert(page);
        window.location.href = page;
        //window.open(page, '_blank');
    });

    $('#product-catalog2 tbody').on('click', 'tr td:not(:last-child)', function() {
        var data = productCatalogTable1.row(this).data();
        var page = $(this).parent().find('td a').attr('href');
        window.location.href = page;
        //window.open(page, '_blank');
    });
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var indmode="industaction";
        // var industryvalue="";
        // alert(industryvalue);
        var formData = {
            type: indmode
        };
        $.ajax({
            type: "POST",
            url: root + "/setting/industry_ajax",
            data: formData,
            dataType: 'json',
            success: function(data) {
                // console.log(data.industry);
                $.each(data.industry, function(i, data){
                    
                    var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
        
                    $(div_data).appendTo("#industry");
                        
                });
            }
            
        });
    });

</script>
<script type="text/javascript">
     $(document).ready(function() {
        $("#industry").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    $("#cat_id").empty();
                    $("#brand").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    var div_data = "<option value='0'>Select  Categories</option>";
                    $(div_data).appendTo("#cat_id");
                    var div_data2 = "<option value='0'>Select  Brands</option>";
                    $(div_data2).appendTo("#brand");

                    if (data != null && data != '')
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat_id");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brand");
                            });
                    } else
                    {
                        //alert("Categories does not exist");   
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#cat_id").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#sub_cat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");                
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //alert(errorThrown);
                    commit(false);
                }
            });
        });
    });

</script>
<script type="text/javascript">

     $('#r_eset').on('click',function(){
        $('#cat_id,#sub_cat,#industry,#brand').children().removeAttr('selected');
        $('#cat_id,#sub_cat,#brand').html('');
     });
    
    $('#filter').click(function() {
        
        var categoryData = $('#cat_id option:selected').val();
        var scategoryData = $('#sub_cat option:selected').val();
        var brandData = $('#brand option:selected').val();
        var status = $('#status option:selected').val();
        
        var industry = [];
        
        $.each($("#industry option:selected"), function(){            
            industry.push($(this).val());
        });
        
        var industryData = industry.join(",");
        
        var cat_id = [];
        
        $.each($("#cat_id option:selected"), function(){            
            cat_id.push($(this).val());
        });
        
        var categoryData = cat_id.join(",");
        
        var sub_cat = [];
        
        $.each($("#sub_cat option:selected"), function(){            
            sub_cat.push($(this).val());
        });
        
        var scategoryData = sub_cat.join(",");
        
        var brand = [];
        
        $.each($("#brand option:selected"), function(){            
            brand.push($(this).val());
        });
        
        var brandData = brand.join(",");
        // alert("You have selected the country - " + countries.join(", "));
        
        var productCatalogTable1 = $('#product-catalog1').DataTable( {
            destroy:true,
            "ajax": {
                "url":"browseProduct_ajax",
                "dataSrc": "",
                "type": 'POST',
                data:{
                    productActionType:"group_filter",
                    industryData:industryData,
                    categoryData:categoryData,
                    scategoryData:scategoryData,
                    brandData:brandData,
                    status:status,
                },
            },
            columns: [
            { data: 'productid' },
            { data: 'industry' },
            { data: 'category' },
            { data: 'brand' },
            { data: 'productname' },
            { data: 'productcodes' }
         
            ]
        } );

        var productCatalogTable2 = $('#product-catalog2').DataTable( {
            destroy:true,
            "ajax": {
                "url":"browseProduct_ajax",
                "dataSrc": "",
                "type": 'POST',
                data:{
                    productActionType:"group_filter",
                    industryData:industryData,
                    categoryData:categoryData,
                    scategoryData:scategoryData,
                    brandData:brandData,
                    status:status,
                },
            },
            columns: [
            { data: 'productid' },
            { data: 'industry' },
            { data: 'category' },
            { data: 'brand' },
            { data: 'productname' },
            { data: 'productcodes' },
            { data: 'action' }
         
            ]
        } );

        // setInterval( function () {
        //     productCatalogTable2.ajax.reload();
        // }, 3000 );      
        
    });
</script>
<script type="text/javascript">
    function shownoti(message) {
        $(function() {
            function Toast(type, css, msg) {
                this.type = type;
                this.css = css;
                this.msg = msg;
            }
            var toasts = [
                new Toast('success', 'toast-top-right', message),
            ];
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.options.extendedTimeOut = 0; //1000;
            toastr.options.timeOut = 2000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            var i = 0;
            /* $('#tryMe').click(function () {

                 $('#tryMe').prop('disabled', true);*/
            delayToasts();
            //});
            function delayToasts() {
                if (i === toasts.length) {
                    return;
                }
                var delay = i === 0 ? 0 : 2100;
                window.setTimeout(function() {
                    showToast();
                }, delay);
                // re-enable the button        
                if (i === toasts.length - 1) {
                    window.setTimeout(function() {
                        prop('disabled', false);
                        i = 0;
                    }, delay + 1000);
                }
            }

            function showToast() {
                var t = toasts[i];
                toastr.options.positionClass = t.css;
                toastr[t.type](t.msg);
                i++;
                delayToasts();
            }
        })
    }
</script>

@if(\Request::input('add')=='true')

    <script> 
        $(document).ready(function() {

            $.ajax({
                url: root + "/noti6",
                type: "post",
                cache: false,
                dataType:'html',
                success: function(res2){

                    if(res2=='bothclose')
                    {

                    }
                    if(res2=='audioclose')
                    {

                        var msg = "Product submitted successfully";
                        shownoti(msg); 
                    }
                    if(res2=='vedioclose')
                    {
                        $('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root+'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');

                        $('#sound')[0].play(); 
                    }
                    if(res2=='bothstart')
                    {
                        var msg = "Product submitted successfully";
                        shownoti(msg); 
                        $('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root+'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                        $('#sound')[0].play(); 
                    }
                }
            });
        });
    </script>
@endif

@if(\Request::input('update') =="true")

    <script> 
        $(document).ready(function() {

            $.ajax({
                url: root + "/noti6",
                type: "post",
                cache: false,
                dataType:'html',
                success: function(res2){

                    if(res2=='bothclose')
                    {

                    }
                    if(res2=='audioclose')
                    {
                        shownoti("Product Updated successfully"); // refresh div after 5 secs
                    }
                    if(res2=='vedioclose')
                    {
                        $('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root+'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');

                        $('#sound')[0].play(); 
                    }
                        if(res2=='bothstart')
                    {
                        shownoti("Product Updated successfully"); // refresh div after 5 secs
                        $('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root+'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                        $('#sound')[0].play(); 
                    }
                }
            });
        });
    </script>
@endif