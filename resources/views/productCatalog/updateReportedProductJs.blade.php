<script type="text/javascript">
	jQuery(document).ready(function() {
        $("#updateProduct").click(function(event) {
        	event.preventDefault();
            if ($("#pname").val() == '') {

                alert('Product Name Required');

                return false;

            }

            if ($("#reporttype").val() == '') {

                alert('Report Type Required');

                return false;

            }

           

            if ($("#desc").val() == '') {

                alert('Description Required');

                return false;

            }
			
			 if ($("#reportby").val() == '') {

                alert('Report By Required');

                return false;

            }
			
			 if ($("#reviewdate").val() == '') {

                alert('Report Date Required');

                return false;

            }

           

            if ($("#pstatus").val() == '') {

                alert('Product Status Required');

                return false;

            }

            if ($("#authchange").val() == '') {

                alert('Authorize Password Required');

                return false;

            }

            if ($("#authchange").val() != '') {

            	$.get(root + '/checkauthpass',{authpass:$("#authchange").val()},function(data){
                  if(data == 'success'){
                    $('.fadeBox').fadeTo("slow", 0.2);
                    $("#loaderID").show();
                	$("#form_edit_product").submit();          
                  }
                  else{
                    alert('Authorized Password Incorrect!');
                    $("#authchange").parent().parent().addClass('has-error');
                    $("#authchange").focus();
                  }
             		return false;     
                });

            }

        });

        $("#cancel").click(function() {
            location.href = '{{ url("product/reportedProduct") }}';
        });

    });
</script>