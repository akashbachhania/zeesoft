@if(Auth::user()->roletype == "AD")
<script type="text/javascript">
	
	jQuery(document).ready(function(){

		var viewProductadTable = $('#viewProductadTable').DataTable( {

	        "ajax": {
	            "url": root + "/product/viewProductadTable",
	            "data" : {
	            	pro_id : "{{ \Request::segment(3) }}"
	            },
	            "dataSrc": "",
	            "type": 'POST',
	        },
	        columns: [
	        { data: 'postno' },
	        { data: 'sdate' },
	        { data: 'industry' },
	        { data: 'category' },
	        { data: 'brand' },
	        { data: 'ptype' },
	        { data: 'quantity' },
	        { data: 'productName' },
	        { data: 'timeframe' }
	     
	        ]
	    });

	    $('#viewProductadTable tbody').on('click', 'tr td:not(:first-child)', function () {	
	     	var data = viewProductadTable.row(this).data();
	        var page = $(this).parent().find('td a').attr('href');
	   		window.location.href = page;
	    });	

	});

</script>
@else

<script type="text/javascript">
	
	jQuery(document).ready(function(){

		var viewProductadTable = $('#viewProductadTable').DataTable( {

	        "ajax": {
	            "url": root + "/product/viewProductadTable",
	            "data" : {
	            	pro_id : "{{ \Request::segment(3) }}"
	            },
	            "dataSrc": "",
	            "type": 'POST',
	        },
	        columns: [
	        { data: 'postno' },
	        { data: 'sdate' },
	        { data: 'industry' },
	        { data: 'category' },
	        { data: 'brand' },
	        { data: 'ptype' },
	        { data: 'quantity' },
	        { data: 'productName' },
	        { data: 'timeframe' }
	     
	        ]
	    });

	    $('#viewProductadTable tbody').on('click', 'tr td:not(:first-child)', function () {	
	     	var data = viewProductadTable.row(this).data();
	        var page = $(this).parent().find('td a').attr('href');
	   		window.location.href = page;
	    });	

	});
</script>

@endif

<script type="text/javascript">
	jQuery(document).ready(function(){

	    $('#dvfilter').hide();
	    $('#show').click(function(){
		    $('#dvfilter').toggle('slow');
		});


		$("#createselad").on("click", function() {
			var selAd =  $('#selAd').val();
			var productID='{{ $productid[0] }}'+':'+selAd;
			
			if (document.createad.selAd.value=='CreateNewAd')
			{
			 window.location.href = "{{url('product/create-new-add/') .'/'. $productid[0] }}";
				//document.createad.selAd.focus();
				//alert('Please Select the Ad');
				//return (false);// this will not allow form to submit
			}
			else
			{
				 window.location.href = "{{url('product/view-productad/') }}" + "/" + productID;
			}
		});
	});
</script>