@extends('layout.master')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Reported Product {{ $product[0]->name }}</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
	          <div class="row">
	          	<div class="col-md-7">
	          		<form action="{{ url('product/reports_products_action') }}" id="form_edit_product" method="post" enctype="multipart/form-data" class="form-horizontal">
			          	
			          	<input type="hidden" name="productId" value="{{ \Request::segment(3) }}" />
		                <input type="hidden" name="report_id" value="{{ \Request::segment(4) }}" />
		                <input type="hidden" name="addedby" value="{{ $product[0]->addedby }}" />
			          		
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Product Id</label>
		                	<div class="col-md-9">
		                		<input type="text" maxlength="250" id="pid" tabindex="1" class="form-control" disabled name="pid" value="{{ $product[0]->productno }}" placeholder="Product Id">
		                	</div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Product Name</label>
		                	<div class="col-md-9">
                                <input type="text" maxlength="100" id="pname" tabindex="1" class="form-control" name="pname" value="{{ $product[0]->name }}" placeholder="Product Name" disabled>
                            </div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Report Type </label>
		                	<div class="col-md-9">
                                <input type="text" class="form-control" name="reporttype" id="reporttype" value="{{ $reportedproduct[0]->reportby }}" placeholder="Manufacturer" disabled>
                            </div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Comments</label>
		                	<div class="col-md-9">
	                            <textarea class="form-control comment" name="desc" rows="3" id="desc" tabindex="10" disabled>{{ $reportedproduct[0]->reviewremark }}</textarea>
	                        </div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Reported By </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="reportby" id="reportby" maxlength="100" tabindex="8" value="{{ $reportedproduct[0]->reviewbyid }}" placeholder="Report By" disabled>
                            </div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Report Date </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="reviewdate" id="reviewdate" maxlength="10" tabindex="9" value="{{ $reportedproduct[0]->reviewdate }}" placeholder="Report Date" disabled>
                            </div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="pstatus" id="pstatus" tabindex="16">
                                  <option value="">Select Status</option>
                                  <option value="1" {{ ($reportedproduct[0]->isactive == "1")?'selected="selected"':'' }}>Open</option>
                                    <option value="0" {{ ($reportedproduct[0]->isactive == "0")?'selected="selected"':'' }}>Closed</option>
                                </select>
                            </div>
		                </div>
		                <div class="form-group row">
		                	<label class="col-md-3 control-label">Authorize<span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="authchange" name="authchange" placeholder="Authorize Password">
                            </div>
		                </div>
		                <div class="form-group row">
		                	<div class="col-md-offset-3 col-md-9">
                                <input type="submit" id="updateProduct" name="updateProduct" value="Submit" class="btn btn-success">
                                <button type="button" id="cancel" class="btn btn-default">Cancel</button>
                            </div>
		                </div>
		                <div class="form-group row">
		                </div>
		                <div class="form-group row">
		                </div>
		                <div class="form-group row">
		                </div>
		                <div class="form-group row">
		                </div>
		                <div class="form-group row">
		                </div>

			        </form>
	          	</div>
	          	<div class="col-md-5">
	          		<div class="form-group row">
	          			<div class="col-md-12">
	          				<label class="pro_view control-label">
	          					<a href="{{ url('product/detail-product-view') }}/{{ \Request::segment(3) }}" class="view">View Product</a> | <a href="{{ url('product/edit-product') }}/{{ \Request::segment(3) }}" class="view">Edit Product</a> </label>
	          			</div>
	          		</div>
	          	</div>
	          </div>
          </div>
        </div>
    </div>
</div>  
@include('productCatalog.updateReportedProductJs')
@endsection