@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-4">
                <h3 class="box-title">Suggested Products</h3>    
              </div>
              <div class="col-md-6"></div>
              <div class="col-md-2">
                <a class="btn btn-info m_t10" id="show">Filter</a>
              </div>
            </div>

            <div class="row m_t30">
              <div class="col-md-12" id="dvfilter">
                <div class="col-md-3 zee-col">
                  <div class="form-group">
                    <select class="form-control industry" name="industry[]" id="industry" onChange="getState(this.value);getbrand(this.value);" multiple="multiple">
                      <option value="0" class="selectitem">Select Industries</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div id="categroy" class="form-group">
                    <select class="form-control category" onchange="subcat(this.value);" name="category[]" id="cat_id" multiple="multiple">
                      <option value="0">Select categories</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2 zee-category">
                  <div class="form-group">
                    <select class="form-control" name="scategory[]" id="sub_cat" multiple="multiple">
                      <option value="0">Select Sub-categories</option>
                    </select>
                    </div>
                </div>
                <div class="col-md-2 zee-category">
                  <div class="form-group">
                    <select class="form-control" id="brand" name="brand[]" multiple="multiple">
                      <option value="0">Select Brands</option>
                    </select>
                    </div>
                </div>
                <div class="col-md-2 zee-category">
                  <div class="form-group">
                    <button type="button" class="btn btn-danger zee-filter m_t20" id="filter">Show</button>
                    <button type="button" class="btn btn-danger zee-filter m_t20" style="margin-left: 8px;" id="r_eset">Reset</button>
                  </div>
                </div>

              </div>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="suggestedProductTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th> Date Suggested </th>
                  <th> Suggested By </th>
                  <th> Product Id </th>
                  <th> Industry </th>
                  <th> Brand </th>
                  <th> Product Name </th>
                  <th> Product Code </th>
                  <th> Status </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('productCatalog.suggestedProductJs')
@endsection