<script type="text/javascript">

	$("#createselad").on("click", function() {
		var selAd =  $('#selAd').val();
		var productID='{{ \Request::segment(3) }}'+':'+selAd;
		if (document.createad.selAd.value=='CreateNewAd')
		{
		 window.location.href = "{{url('product/create-new-add/') .'/'. \Request::segment(3) }}";
			//document.createad.selAd.focus();
			//alert('Please Select the Ad');
			//return (false);// this will not allow form to submit
		}
		else
		{
			 window.location.href = "{{url('product/view-productad/') }}" + "/" + productID;
		}
		
	});

</script>
<script type="text/javascript">
	$("#btnclosereport").on("click", function() {
        $("label.error").hide();
        $(".error").removeClass("error");
        $('#reason').val('0');
        $('#detail').val('');
    });

	$("#btnsaverreport").on("click", function(event) {
		event.preventDefault();
	    var proid = $('#proid').val();
	    var reason = $('#reason').val();
	    var detail = $('#detail').val();
		 var addreport = $('#type').val();
	    // $("#form_product_report").valid();
	    if (reason == '0') {
	        alert('Please Enter The Reason');
			$( "#reason" ).focus();
	        return false;
	    }
		if (detail == '') {
	        alert('Please Enter The Detail');
			$( "#detail" ).focus();
	        return false;
	    }
	    var datareport = {
	        type: addreport,
	        proid: proid,
	        reason: reason,
	        detail: detail
	    };
	    $.ajax({
	        type: 'POST',
	        url: root + '/product/reportProduct',
	        cache: false,
	        data: datareport,
	        success: function(response) {
	            // response = jQuery.parseJSON(data);
				//alert(data);
	            if (response.msg == 'SUCCESS') {
	                $('#static23').modal('hide');
	                shownoti("Product reported successfully");
	                setTimeout(function() {
	                    window.location.href = root + "/product/detail-product-view/" + response.pid;
	                }, 2000);
	                //alert('Product Code Successful Submit');

	            }
	            if (response.msg == 'EROOR') {
	                alert(response.name);
	            }
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            //commit(false);
	        }
	    });
	});

</script>
<script type="text/javascript">
	function shownoti(message) {
            $(function() {

                function Toast(type, css, msg) {
                    this.type = type;
                    this.css = css;
                    this.msg = msg;
                }
                var toasts = [
                    new Toast('success', 'toast-top-right', message),
                ];

                toastr.options.positionClass = 'toast-top-full-width';
                toastr.options.extendedTimeOut = 0; //1000;
                toastr.options.timeOut = 2000;
                toastr.options.fadeOut = 250;
                toastr.options.fadeIn = 250;

                var i = 0;
                /* $('#tryMe').click(function () {
                     $('#tryMe').prop('disabled', true);*/
                delayToasts();
                //});
                function delayToasts() {
                    if (i === toasts.length) {
                        return;
                    }
                    var delay = i === 0 ? 0 : 2100;
                    window.setTimeout(function() {
                        showToast();
                    }, delay);
                    // re-enable the button        
                    if (i === toasts.length - 1) {
                        window.setTimeout(function() {
                            prop('disabled', false);
                            i = 0;
                        }, delay + 1000);
                    }
                }

                function showToast() {
                    var t = toasts[i];
                    toastr.options.positionClass = t.css;
                    toastr[t.type](t.msg);
                    i++;
                    delayToasts();
                }
            })

        }
</script>