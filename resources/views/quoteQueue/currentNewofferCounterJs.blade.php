<script type="text/javascript">
	function shownoti(message) {
		$(function() {
			function Toast(type, css, msg) {
				this.type = type;
				this.css = css;
				this.msg = msg;
			}
			var toasts = [
				new Toast('success', 'toast-top-right', message),
			];
			toastr.options.positionClass = 'toast-top-full-width';
			toastr.options.extendedTimeOut = 0; //1000;
			toastr.options.timeOut = 2000;
			toastr.options.fadeOut = 250;
			toastr.options.fadeIn = 250;
			var i = 0;
			/* $('#tryMe').click(function () {

			     $('#tryMe').prop('disabled', true);*/
			delayToasts();
			//});
			function delayToasts() {
				if (i === toasts.length) {
					return;
				}
				var delay = i === 0 ? 0 : 2100;
				window.setTimeout(function() {
					showToast();
				}, delay);
				// re-enable the button        
				if (i === toasts.length - 1) {
					window.setTimeout(function() {
						prop('disabled', false);
						i = 0;
					}, delay + 1000);
				}
			}

			function showToast() {
				var t = toasts[i];
				toastr.options.positionClass = t.css;
				toastr[t.type](t.msg);
				i++;
				delayToasts();
			}
		})
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var activeSellPostingTable = $('#activeSellPostingTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/activeSellPosting",
	            "dataSrc": "",
	            "data": {'prodno': '{{ \Request::input("productno") }}','ptype':'{{ \Request::input("ptype") }}'},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'sdate'},
	            { data: 'productno' },
	            { data: 'postno' },
	            { data: 'ptype' },
	            { data: 'quantity' },
	            { data: 'targetprice' },
	            { data: 'currency' },
	            { data: 'uom' },
	            { data: 'timeframe' },
	            { data: 'expdate'}
	        ],
		});

       	var activeBuyPostingTable = $('#activeBuyPostingTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/activeBuyPosting",
	            "dataSrc": "",
	            "data": {'prodno': '{{ \Request::input("productno") }}','ptype':'{{ \Request::input("ptype") }}'},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'sdate'},
	            { data: 'productno' },
	            { data: 'postno' },
	            { data: 'ptype' },
	            { data: 'quantity' },
	            { data: 'targetprice' },
	            { data: 'currency' },
	            { data: 'uom' },
	            { data: 'timeframe' },
	            { data: 'expdate'}
	        ],
		});

        var historicalTransactionTable = $('#historicalTransactionTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/historicalTransaction",
	            "dataSrc": "",
	            "data": {'prodno': '{{ \Request::input("productno") }}','postno':'{{ \Request::input("postno") }}'},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'sdate'},
	            { data: 'productno' },
	            { data: 'postno' },
	            { data: 'ptype' },
	            { data: 'quantity' },
	            { data: 'targetprice' },
	            { data: 'currency' },
	            { data: 'uom' },
	            { data: 'timeframe' },
	            { data: 'expdate'},
	            { data: 'status1'}
	        ],
		});


        var offerhistoryTable = $('#offerhistoryTable').DataTable({
			"ajax": {
	            "url":root+ "/posting/offerHistory",
	            "dataSrc": "",
	            "data": {'post_id':"{{\Request::input('postno')}}",'quotationno':"{{ \Request::input('quotationno') }}"},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'quotationno'},
	            { data: 'dateOffered' },
	            { data: 'user' },
	            { data: 'price' },
	            { data: 'curr' },
	            { data: 'uom' },
	            { data: 'quantity' },
	            { data: 'timeframe' },
	            { data: 'expdaterange' }
	        ],
		});

		var postno = '{{ \Request::input("postno") }}';

     	var stresult ='{{ $mypoststatus }}';
     	var type = "fetchquotepostdata";
        var dataString = 'postno=' + postno + '&type=' + type;

        if (postno != '') {
            $.ajax({
            	type: "POST",
                url: root + "/quoteQueue/fetchFunction",
                data: dataString,
                cache: false,
                success: function(response) {
                	console.log(response);
                	var pro_id = response.productid;
                    var postno = response.postno;
                    $('#postno').html(response.postno);
                    $('#postno').val(response.postno);
                    $('#productno').val(response.productno);
                    var mpmvalue = response.mpm;
                    if (mpmvalue != null) {
                        $('#mpmval').html(response.mpm);
                    } else {
                        var mpval = 0;
                        $('#mpmval').html(mpval);
                    }
                    $('#puserid').val(response.userid);
                    $('#pdate').html(response.pdate);
  					if((response.ptype=='BUY') || (response.ptype=='Buy'))
  					{
	  					$('#buyertr').show();
	  					$('#sellertr').hide();
  					}
  					if((response.ptype=='SELL') || (response.ptype=='Sell'))
  					{
	  					$('#sellertr').show();
	  					$('#buyertr').hide();
  					}
  					$('#pname').html(response.productName);
                    $('#proid').html(response.productno);
                    $('#productid2').val(response.productno);
                    $('#userid').html(response.userid);
                    $('#name').html(response.name);
                    $('#curr').html(response.currency);
                    $('#pcurr').val(response.currency);
                    $('#puom').val(response.uom);
                    $('#plocation').val(response.location);
                    $('#pexpirydate').val(response.expirydate);
                    $('#pplang').val(response.pakaging);
                    $('#plang').html(response.pakaging);
                    $('#shipcond').html(response.shipingcondition);
                    $('#corigin').html(response.country);
          					$('#buyer').html(response.buyer);
          					$('#seller').html(response.seller);
                    var uid = response.userid;
                    $('#cusrefno').html(response.customerrefno);
                    $('#targetprice').html(response.targetprice);
                    $('#location').html(response.location);
                    $('#pstatus').html(response.pstatus);
                    var poststatus = response.pstatus;

                    if ((poststatus =="Accepted")) {
      					$('#savequote').attr("disabled", "disabled");
      				}
      				else
  					{
	                    $("#savequote").prop("disabled", false);
  					}
  					$('#Timeframe').html(response.timeframe);
                    var exactexpdrange = response.expirydate;
                    if (exactexpdrange != null) {
                        $('#eed').html(response.expirydate);
                    } else {
                        $('#eed').html('Not Applicable');
                    }
                    if (exactexpdrange !='') {
                        $('#eed').html(response.expirydate);
                    } else {
                        $('#eed').html('Not Applicable');
                    }
                    $('#expdrange').html(response.expdate);
                    $('#uom').html(response.uom);
                    $('#qty').html(response.quantity);
                    $('#qquantity').val(response.quantity);
                    $('#ptype').html(response.ptype);
                    var posttype = response.ptype;
                    if (posttype == 'BUY') {
                        $("#expd").hide();
                    }
                    var type2 = "fetchcurrentoffers";
                    var dataString2 = {'postno' : postno, 'type' : type2};
          
                }
            });
        }
	});
</script>