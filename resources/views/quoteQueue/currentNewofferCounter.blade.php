@extends('layout.master')

@section('content')
<section>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          	<h3 class="box-title"><span id="pname"></span></h3>
          </div>

 		  <div class="box-body">
 		  	
 		    <div class="row">
 		    	<div class="col-md-12">
			        <label style="color:#000000; font-weight:700">Posting ID : <font id="postno"></font> | Creation Date : <font id="pdate"></font> EST</label>
 		    	</div>
 		    </div>

 		    <div class="row m_t30">
				<div class="col-md-12">
					 <div class="box">
					 	<div class="box-header">
					 		<h3 class="box-title">Offer History</h3>
					 	</div>
					 	<div class="box-body">
					 	   <div class="table-responsive no-padding">
								<table id="offerhistoryTable" class="table table-hover">
									<thead>
					                    <tr role="row">
					                      <th width="10%"> Quote ID</th>
					                      <th width="15%"> Date </th>
					                      <th width="10%"> Price </th>
					                      <th width="5%"> Curr </th>
					                      <th width="10%"> Uom </th>
					                      <th width="5%"> Quantity </th>
					                      <th width="10%"> TimeFrame </th>
					                      <th width="10%"> Exp Date </th>
					                      <th width="15%"> Details</th>
					                      <th width="10%"> Action </th>
					                    </tr>
				                    </thead>
				                    <tbody>
				                    </tbody>
								</table>
							</div>
					 	</div>
					 </div>
				</div>
			</div>
 		  </div>        
         </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
			<div class="box-header">
				
			</div>
			<div class="box-body">
			
				<div class="row m_t30">
	 		    	<div class="col-md-7">
	 		    		<div class="row">
	          	 			<div class="col-md-12">
	          	 				<h4 style="font-weight: 700">Posting Details</h4>
	          	 			</div>
	          	 		</div>

	          	 		<div class="row m_t10">
	          	 			<div class="col-md-12">
	          	 				
	          	 				<div class="table-responsive no-padding">
									<table id="quotepostviewtable" class="table table-hover">
										
										<tbody>
					                        <tr>
					                          <td width="30%"><strong>Product ID </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="proid"> </div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Type</strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="ptype"></div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Quantity</strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="qty"> </div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Target Price </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="targetprice"> </div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>currency</strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="curr"> </div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Unit of Mesaurement</strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="uom"></div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Expiry Date Range </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="expdrange"></div></td>
					                        </tr>
					                        <tr id="expd">
					                          <td width="30%"><strong>Exact Expiry Date</strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="eed"></div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Timeframe </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="Timeframe"></div></td>
					                        </tr >
					                        <tr>
					                          <td width="30%"><strong>Location  </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="location"></div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Packaging Language</strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="plang"> </div></td>
					                        </tr>
					                        <tr>
					                          <td width="30%"><strong>Shipping Conditions </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="shipcond"></div></td>
					                        </tr >
					                        <tr>
					                          <td width="30%"><strong>Country of Origin  </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="corigin"></div></td>
					                        </tr >
					                        <tr id="buyertr">
					                          <td width="30%"><strong>Buyer  </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="buyer"></div></td>
					                        </tr >
					                         <tr id="sellertr">
					                          <td width="30%"><strong>Seller  </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="seller"></div></td>
					                        </tr >
					                        <tr>
					                          <td width="30%"><strong>Status </strong></td>
					                          <td width="10%">:</td>
					                          <td width="60%"><div align="left" id="pstatus"></div></td>
					                        </tr >
					                      </tbody>

									</table>
								</div>
							</div>
						</div>
	 		    	</div>
	 		    	<div class="col-md-5">
	 		    		<div class="col-md-12">
	 		    			<h4><strong>Counter Offer</strong></h4>
	 		    		</div>
	 		    		<div class="col-md-12">
		 		    		<form id="form_add_product" method="post" role="form" class="form-horizontal">
								<div class="form-body">
									
									<div id="loaderID" style="display:none;"><img src="addLoader.GIF"></div>

									<input type="hidden" name="puserid" id="puserid" />
		                          	<input type="hidden" name="pcurr" id="pcurr"/>
		                            <input type="hidden" name="puom" id="puom"/>
		                            <input type="hidden" name="plocation" id="plocation"/>
		                            <input type="hidden" name="pexpirydate" id="pexpirydate"/>
		                            <input type="hidden" name="pplang" id="pplang"/>
		                            <input type="hidden" name="postno" id="postno"/>
		                            <input type="hidden" name="productno" id="productno"/>

		                            <div class="form-group row">
		                            	<div class="col-md-12">
		                            		<input type="text" class="form-control" id="qprice" name="qprice" placeholder="Enter Price" maxlength="100" tabindex="1">
				                           <div id="msgg" style="display: block; margin-bottom: 1px; clear: both; position: relative; top: -1px; margin-left:6px;font-size: 14px;"> </div>
		                            	</div>
		                            </div>

		                            <div class="form-group row">
		                            	<div class="col-md-12">
		                         
		                                    <select class="form-control" name="qcurrency" id="qcurrency" tabindex="2">
			                     		         <option value="">Select Currency</option>
			                            		 @foreach( $currencies as $currency )
													<option value="{{ $currency->name }}" {{ ($currency->name==isset($postDetails[0]['currency'])?$postDetails[0]['currency']:'')? 'selected="selected"':'' }}>
													{{ $currency->name }}</option>
												 @endforeach
			                                </select>
		                            	</div>
		                            </div>

		                            <div class="form-group row">
		                            	<div class="col-md-12">
		                            		<input type="text" class="form-control" id="qquantity" name="qquantity" placeholder="Enter Quantity" maxlength="100" tabindex="1">
		                            	</div>
		                            </div>

		                            <div class="form-group row">
		                            	<div class="col-md-12">
		                            		<select class="form-control" name="qtimeframe" id="qtimeframe">
			                                    <option value="">Select Timeframe</option>

			                                    @foreach( $timeframes as $timeframe )

			                                    <option value="{{ $timeframe->name }}" {{ ($timeframe->name==isset($postDetails[0]['timeframe'])?$postDetails[0]['timeframe']:'')? 'selected="selected"':'' }}>
			                                    	{{ $timeframe->name }}
			                                    </option>

			                                    @endforeach

			                                </select>
		                            	</div>
		                            </div>

		                            <div class="form-group row">
		                            	<div class="col-md-12">
		                            		<select class="form-control" name="qexpdtarnge" id="qexpdtarnge" tabindex="4">
		                            			<option value="">Select Exp. Daterange</option>

		                            			@foreach( $expdateranges as $expdaterange )

		                            			<option value="{{$expdaterange->name}}" {{ ($expdaterange->name==isset($postDetails[0]['timeframe'])?$postDetails[0]['expdate']:'')? 'selected="selected"':'' }}>
		                            				{{ $expdaterange->name }}
		                            			</option>	

		                            			@endforeach
		                            		</select>
		                            	</div>
		                            </div>

		                            <div class="form-group row">
		                            	<div class="col-md-12">
		                            		<input type="password" class="form-control" id="authpass" name="authpass" placeholder="Enter Auth. Code" maxlength="100" tabindex="1">
				                            <div id="msgg" style="display: block; margin-bottom: 1px; clear: both; position: relative; top: -1px; margin-left: 	6px;font-size: 14px;"> </div>
		                            	</div>
		                            </div>

		                            <div class="row form-group">
		                            	<div class="col-md-12">
		                            		
		                            		<div class="form-actions" style="background-color:#FFFFFF; border-top:0px;">
				                              <div class="row" align="left">
				                                <div class="col-md-9">
				                                  <input type="button" name="savequote" id="savequote" value="Submit" class="btn btn-success">
				                                </div>
				                              </div>
				                            </div>
		                            	</div>	
		                            </div>
		                        </div>
							</form>
						</div>
	 		    	</div>
	 		    </div>
	 		</div>
 		</div>
 	</div>
 </div>

<div class="row">
	<div class="col-md-12">
		 <div class="box">
		 	<div class="box-header">
		 		<h3 class="box-title">Active Sell Postings</h3>
		 	</div>
		 	<div class="box-body">
		 	   <div class="table-responsive no-padding">
					<table id="activeSellPostingTable" class="table table-hover">
						<thead>
		                    <tr role="row">
		                      <th width="10%"> Date</th>
		                      <th width="10%"> Product ID </th>
		                      <th width="10%"> Posting ID </th>
		                      <th width="10%"> Type </th>
		                      <th width="10%"> Quantity </th>
		                      <th width="10%"> Price </th>
		                      <th width="10%"> Curr </th>
		                      <th width="10%"> UoM </th>
		                      <th width="10%"> Timeframe</th>
		                      <th width="10%"> Exp Date Range </th>
		                    </tr>
		                </thead>
		                <tbody>
		                </tbody>
					</table>
				</div>
		 	</div>
		 </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		 <div class="box">
		 	<div class="box-header">
		 		<h3 class="box-title">Active Buy Postings</h3>
		 	</div>
		 	<div class="box-body">
		 	   <div class="table-responsive no-padding">
					<table id="activeBuyPostingTable" class="table table-hover">
						<thead>
		                    <tr role="row">
		                      <th width="10%"> Date</th>
		                      <th width="10%"> Product ID </th>
		                      <th width="10%"> Posting ID </th>
		                      <th width="10%"> Type </th>
		                      <th width="10%"> Quantity </th>
		                      <th width="10%"> Price </th>
		                      <th width="10%"> Curr </th>
		                      <th width="10%"> UoM </th>
		                      <th width="10%"> Timeframe</th>
		                      <th width="10%"> Exp Date Range </th>
		                    </tr>
		                </thead>
		                <tbody>
		                </tbody>
					</table>
				</div>
		 	</div>
		 </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		 <div class="box">
		 	<div class="box-header">
		 		<h3 class="box-title">Historical Transactions</h3>
		 	</div>
		 	<div class="box-body">
		 	   <div class="table-responsive no-padding">
					<table id="historicalTransactionTable" class="table table-hover">
						<thead>
		                    <tr role="row">
		                      <th width="10%"> Date</th>
		                      <th width="10%"> Product ID </th>
		                      <th width="10%"> Posting ID </th>
		                      <th width="10%"> Type </th>
		                      <th width="10%"> Quantity </th>
		                      <th width="10%"> Price </th>
		                      <th width="10%"> Curr </th>
		                      <th width="10%"> UoM </th>
		                      <th width="10%"> Timeframe</th>
		                      <th width="10%"> Exp Date Range </th>
		                      <th width="10%"> Status </th>
		                    </tr>
		                </thead>
		                <tbody>
		                </tbody>
					</table>
				</div>
		 	</div>
		 </div>
	</div>
</div>
</section>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
               <form action="#" id="form_sample_2" class="form-horizontal" novalidate>
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                     <h4 class="modal-title" style="text-align:left"><span id="typeuser"></span> <span>Form</span> </h4>
                  </div>
                  <div class="modal-body" style="text-align:left">
                     
                        <div class="form-body">
                           <div id="msg" name="msg"></div>
                           <div class="form-group">
                              <label class="control-label col-md-3">Name <span class="required">
                              * </span>
                              </label>
                              <div class="col-md-8">
                                 <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" maxlength="80" id="name" name="name">
                                    <input type="hidden" class="form-control" name="userpostId" id="userpostId" value="">
                                    <input type="hidden" class="form-control" name="usertype" id="usertype" value="">
                                 </div>
                              </div>
                           </div>
                        </div>
                     
                  </div>
                  <div class="modal-footer">
                     <a href="#" id="btnclose" class="btn default"><i class="fa">
                     </i>&nbsp; Close</a>
                     <button type="button" id="btnsave" class="btn blue">Save changes</button>
                  </div>
               </div>
            </div>
           </form>
         </div>
@include('quoteQueue.currentNewofferCounterJs')
@endsection