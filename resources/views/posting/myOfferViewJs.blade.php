<script type="text/javascript">
 function shownoti(message){
	$(function() {

    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];

    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;

    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})

}
</script>
<script>
jQuery(document).ready(function() {
    var postno = '{{ \Request::segment("3") }}';
    var stresult ='{{ $postStatus }}';
    
    var type = "fetchallposts";
    var dataString = 'postno=' + postno + '&type=' + type;
    if(postno != ''){

        $.ajax({
            type: "POST",
            url: "{{ url('posting/fetchFunction') }}",
            data: dataString,
            cache: false,
            success: function(response){
// console.log(response);
             // response = jQuery.parseJSON(data);
             //alert(data);
             var pro_id =response.productid;
             $('#postno').html(response.postno);
             $('#userid').html(response.userid);
             $('#pdate').html(response.pdate);
             $('#pname').html(response.name);
             $('#productid').html(response.productno);
             $('#name').html(response.name);
             $('#industryid').html(response.industry);
             $('#catid').html(response.category);
             $('#subcatid').html(response.subcategory);
             $('#brandid').html(response.brand);
             $('#manufacture').html(response.manufacture);
             $('#pakaging').html(response.pakaging);
             
             $('#packlanguage').html(response.packlang);
             
             $('#shipingcondition').html(response.shipingcondition);
             $('#countryorgin').html(response.country);
             $('#procodes').html(response.procodes);
             $('#cusrefno').html(response.customerrefno);
             $('#targetprice').html(response.targetprice);
             $('#currency').html(response.currency);
             $('#location').html(response.location);
             $('#pstatus').html(response.pstatus);
             //$('#pstatus').html(stresult);
             $('#timeframe').html(response.timeframe);
            var exactexpdrange =  response.expirydate;
            //alert(exactexpdrange);
            if(exactexpdrange!='')
            {
                $('#expirydate').html(response.expirydate);
                }
                else
                {
                    $('#expirydate').html('Not Applicable');
                    }
             $('#expdate').html(response.expdate);
             $('#uom').html(response.uom);
             $('#quantity').html(response.quantity);
             $('#ptype').html(response.ptype);
             
             $("#fimges").html(response.image);

             $("#procodes").html(response.productcodes);
             MagicZoom.refresh();
             
        }
        });
    }
return false;  
});
</script>
<script type="text/javascript">
    
var datatable_ajax = $('#datatable_ajax').DataTable( {

    "ajax": {
        "url":"{{ url('posting/offerHistory') }}",
        "dataSrc": "",
        "data"   : {
            postid:"{{\Request::segment(3)}}",
            quotationno:"{{\Request::segment(4)}}"
        },
        "type": 'POST',
    },
    columns: [
    { data: 'quotationno' },
    { data: 'sdate' },
    { data: 'price' },
    { data: 'currency' },
    { data: 'uom' },
    { data: 'quantity' },
    { data: 'timeframe' },
    { data: 'expdate' },
    { data: 'detail' },
    { data: 'edits' }
 
    ]
});    
</script>
<script type="text/javascript">
    function acceptnew(obj) {
      $('#termscondition').attr('checked', false);
     $('span:has(input:not(:checked))').removeClass('checked');
     $('#authorization').val('');
    $('#acceptbasic').modal('show');
    var data ="Type=search&id="+ obj;
    $.ajax({
      type:'GET',
      url:'service/my-posting-offer.php',
      cache: false,
      data: data,
      success: function(data) {
         //alert(data);
         //document.write(data);
        response = jQuery.parseJSON(data);
        $('#acceptProductno').text(response.productno);
        $('#acceptquotationno').text(response.quotationno);
        $('#acceptquoteid').val(response.quoteid);
        $('#acceptcounterid').val(response.counterid);
        $('#acceptProductName').text(response.ProductName);
        $('#acceptBrandName').text(response.BrandName);
        $('#acceptptype').text(response.ptype);
        $('#acceptquantity').text(response.quantity);
        $('#acceptprice').text(response.price);
        $('#acceptcurrency').text(response.currency);
        $('#acceptuom').text(response.uom);
        $('#accepttimeframe').text(response.timeframe);
        $('#acceptexpirydate').text(response.expirydate);
        $('#acceptpostno').val(response.postno);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //commit(false);
      }
    });
  };
</script>