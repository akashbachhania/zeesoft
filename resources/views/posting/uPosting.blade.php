@extends('layout.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">User Postings</h3>
			</div>
			<div class="box-body table-responsive">
				<table class="table table-striped table-bordered table-hover" id="uPostingTable">
					<thead>
	                    <tr role="row">
	                      <th width="8%"> Posting ID </th>
	                      <th width="10%"> Date Posted </th>
	                      <th width="5%"> Brand </th>
	                      <th width="5%"> Type</th>
	                      <th width="5%"> Quantity</th>
	                      <th width="27%"> Product Name </th>
	                      <th width="5%"> Price </th>
	                      <th width="5%"> Curr </th>
	                      <th width="10%"> User Id </th>
	                      <th width="10%"> Cust Ref </th>
	                      <th width="5%"> Status </th>
	                      <th width="5%"> Action </th>
	                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@include('posting.uPostingJs')
@endsection