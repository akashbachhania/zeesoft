<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.0/jquery-migrate.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#datepicker").daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});

		$('#addtype').on('change', function() {
	      if ( this.value == 'BUY'){

			$("#epd").hide();
	      
	      }
		  
		  if ( this.value == ''){

			$("#epd").hide();
	      
	      }
	      if (this.value == 'SELL'){

			$("#epd").show();
	      
	      }
	    
	    });
	});

	function change(aid){
	  
	  var iname=aid.name;
	  
	  if(iname.indexOf('http://')>=0)
	  {
	    //document.getElementById("bimg").src=iname;
	  }
	  else
	  {
			$.ajax({
			type:'POST',
			data:'',
			url:'ajax.php?aid='+iname,
			success:function(data){
			//alert(data);
			var Arr=data.split('|');
				$('#image').html(Arr[0]);
			}
		});
		
	  }
	}

	function validate(){

      	var searchid = document.getElementById("searchid").value;
		var addprice = document.getElementById("addprice").value;
		var addcurrency = document.getElementById("addcurrency").value;
		var addqty = document.getElementById("addqty").value;
		var addlocation = document.getElementById("addlocation").value;
		var addexpdrange = document.getElementById("addexpdrange").value;
		var addcurrefno = document.getElementById("addcurrefno").value;
		var addtimeframe = document.getElementById("addtimeframe").value;
		var selectedlanguage = document.getElementById("selectedlanguage").value;
		var addcountry = document.getElementById("addcountry").value;
		var addprono = document.getElementById("addprono").value;
		

		
         if( searchid =="" )
         {
            alert( "Please Select a Product!" );
			document.getElementById("searchid").focus();
            return false;
         }
		 if( addprono =="" )
         {
            alert( "Please Select a Product!" );
			document.getElementById("searchid").focus();
            return false;
         }
         if( document.crateadd.addtype.value == "" )
         {
            alert( "Please Select Add Type!" );
			document.getElementById("addtype").focus();
            return false;
         }
		 if(addprice =="" )
         {
            alert( "Please Fill Product Price!" );
			document.getElementById("addprice").focus();
            return false;
         }
		 if( document.crateadd.addcurrency.value == "" )
         {
            alert( "Please Select Currency!" );
			document.getElementById("addcurrency").focus();
            return false;
         }
		 if(addqty =="" )
         {
            alert( "Please Fill Product Quantity!" );
			document.getElementById("addqty").focus();
            return false;
         }
		 if( document.crateadd.addlocation.value == "" )
         {
            alert( "Please Select Location!" );
			document.getElementById("addlocation").focus();
            return false;
         }
		 if( document.crateadd.addexpdrange.value == "" )
         {
            alert( "Please Select Exp. Daterange!" );
			document.getElementById("addexpdrange").focus();
            return false;
         }
		 if( document.crateadd.addcurrefno.value == "" )
         {
            alert( "Please Select Customer Refrence No.!" );
			document.getElementById("addcurrefno").focus();
            return false;
         }
		 if( document.crateadd.selectedlanguage.value == "" )
         {
            alert( "Please Select Packaging Language!" );
			document.getElementById("selectedlanguage").focus();
            return false;
         }
		 if( document.crateadd.addtimeframe.value == "" )
         {
            alert( "Please Select Timeframe!" );
			document.getElementById("addtimeframe").focus();
            return false;
         }
		 // if( document.crateadd.addcountry.value == "" )
   //       {
   //          alert( "Please Select Country Of Origin!" );
			// document.getElementById("addcountry").focus();
   //          return false;
   //       }
         return( true );
      }


      function hello(str) {
        var val = str.split(':');
        var first = val[0];
        var second = val[1];
        $('#searchid').val(second);
        $('#pid').val(first);
        var pro_id = first;
        $.ajax({
            type: "POST",
            url: "fetch_productdetail",
            data: {
                pro_id: pro_id
            },
            cache: false,
            success: function(response) {
                // console.log(data.productno);
                // response = jQuery.parseJSON(data);
                $('#productid').html(response.productno);
                $('#name').html(response.name);
                $('#industryid').html(response.industry);
                $('#catid').html(response.category);
                $('#subcatid').html(response.subcategory);
                $('#brandid').html(response.brand);
                $('#manufacture').html(response.manufacture);
                $('#caseweight').html(response.caseweight);
                var qtyprcase = response.qtypercase;
               // alert(qtyprcase);
                if((qtyprcase !='') || (qtyprcase != 0))
                {
					$('#qtypercase').html(response.qtypercase);
				}
                
                $('#pakaging').html(response.pakaging);
                $('#shipingcondition').html(response.shipingcondition);
                $('#description').html(response.description);
                $('#addprono').val(response.productno);
                $("#procodes").html(response.productcodes);
                $("#fimges").html(response.image);
                
                
            }
        });
    }
    $(function() {
        $(".search").keyup(function() {
            var searchid = $(this).val();
            var dataString = 'search=' + searchid;
            if (searchid != '') {
                $.ajax({
                    type: "POST",
                    url: "search",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        $("#result").html(html).show();
                    }
                });
            }
			else
			{
			 $.ajax({
                    type: "POST",
                    url: "search",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        $("#result").html('').show();
                    }
                });
			}
            return false;
        });
        jQuery(document).live("click", function(e) {
            var $clicked = $(e.target);
            if (!$clicked.hasClass("search")) {
                jQuery("#result").fadeOut();
            }
        });
        $('#searchid').click(function() {
            jQuery("#result").fadeIn();
        });
    });
</script>