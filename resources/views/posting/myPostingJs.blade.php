@if( Auth::user()->roletype == 'AM' )

<script type="text/javascript">
	var myPostingTable = $('#myPostingTable').DataTable( {

	    "ajax": {
	        "url":"myPosting_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	    },
	    columns: [
	    { data: 'postno' },
	    { data: 'sdate' },
	    { data: 'brand' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productName' },
	    { data: 'currency'},
	    { data: 'customerrefno'},
	    { data: 'poststatus'},
	    { data: 'edit'}
	 
	    ]
	});
</script>
@else

<script type="text/javascript">
	var myPostingTable = $('#myPostingTable').DataTable( {

	    "ajax": {
	        "url":"myPosting_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	    },
	    columns: [
	    { data: 'postno' },
	    { data: 'sdate' },
	    { data: 'brand' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productName' },
	    { data: 'price' },
	    { data: 'currency'},
	    { data: 'customerrefno'},
	    { data: 'poststatus'},
	    { data: 'edit'}
	 
	    ]
	});
</script>

@endif
<script type="text/javascript">
    function cancelme(obj) {
        var data = "Type=cancelpost&postid=" + obj;
        if (confirm("Are you sure you wish to cancel this Posting?")) {
            $.ajax({
                type: 'POST',
                url: root + '/posting/cancelPosting',
                cache: false,
                data: data,
                success: function(data, status, xhr) {
                    window.location = root + '/posting/myPosting?cancel=success';
                },
                error: function(jqXHR, textStatus, errorThrown) {}
            });
        }
    }
</script>

<script type="text/javascript">

 function shownoti(message){
	$(function() {
    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];
    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
}
</script>


      @if( isset($_REQUEST['page']) && ($_REQUEST['page']=="upposting"))
			<script> 
				$(document).ready(
							function() {
							var id = "{{ $_REQUEST['page'] }}";
						    var lognoti = "lognoti";
					 $.ajax({
										url: root + "/notiAlert",
										type: "post",
									    data:{ 'id': id , 'lognoti': lognoti},
										cache: false,
										success: function(res){
										//alert(res);
										
										$.ajax({
											url: root + "/noti6",
											type: "post",
											data:{id:id},
											cache: false,
											dataType:'html',
													success: function(res2){
													//alert(res2);
														if(res2=='bothclose')
														{
														
														}
														if(res2=='audioclose')
														{
														//alert(res2);
													var message = "Edited Posting successully";
													shownoti(message);
														}
														if(res2=='vedioclose')
														{
												$('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									
									 $('#sound')[0].play(); 
														}
													if(res2=='bothstart')
													{
													//alert(res2);
													var message = "Edited Posting successully";
													shownoti(message);
												$('<audio  id="sound" ><source src="'+root+'/assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="'+root+'/assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									 $('#sound')[0].play(); 
													}
												 }
											});
										
										//var message = res;
										//shownoti(message);
										}
								});
                                //shownoti("User Created Successfully"); // refresh div after 5 secs
                     });
				 </script>
			@endif
            
                       @if( isset($_REQUEST['page']) && ($_REQUEST['page']=="addpost"))
						    <script> 
				$(document).ready(
							function() {
							
									$.ajax({
											url: root + "/noti6",
											type: "post",
											cache: false,
											dataType:'html',
													success: function(res2){
													//alert(res2);
													//alert(1111111111);
														if(res2=='bothclose')
														{
														
														}
														if(res2=='audioclose')
														{
														//alert(res2);
													//var message = res;
													//shownoti(message);
													
													 var msg = "Added Posting Successully";
													shownoti(msg); 
														}
														if(res2=='vedioclose')
														{
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									
									 $('#sound')[0].play(); 
														}
													if(res2=='bothstart')
													{
													//alert(res2);
													//var message = res;
													//shownoti(message);
													 var msg = "Added Posting Successully";
													shownoti(msg); 
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									 $('#sound')[0].play(); 
													}
												 }
											});
							
							
                     });
				 </script>
		            	@endif
                                               @if( isset($_REQUEST['cancel']) && ($_REQUEST['cancel']=="success"))
						    <script> 
				$(document).ready(
							function() {
							
									$.ajax({
											url: root + "/noti6",
											type: "post",
											cache: false,
											dataType:'html',
													success: function(res2){
													//alert(res2);
													//alert(1111111111);
														if(res2=='bothclose')
														{
														
														}
														if(res2=='audioclose')
														{
														//alert(res2);
													//var message = res;
													//shownoti(message);
													
													 var msg = "Cancelled Posting Successully";
													shownoti(msg); 
														}
														if(res2=='vedioclose')
														{
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									
									 $('#sound')[0].play(); 
														}
													if(res2=='bothstart')
													{
													//alert(res2);
													//var message = res;
													//shownoti(message);
													 var msg = "Cancelled Posting Successully";
													shownoti(msg); 
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									 $('#sound')[0].play(); 
													}
												 }
											});
							
							
                     });
				 </script>
		            	@endif
                        
                        @if(isset($_REQUEST['repost']) && ($_REQUEST['repost']=="success"))
						    <script> 
				$(document).ready(
							function() {
							
									$.ajax({
											url: root + "/noti6",
											type: "post",
											cache: false,
											dataType:'html',
													success: function(res2){
													//alert(res2);
													//alert(1111111111);
														if(res2=='bothclose')
														{
														
														}
														if(res2=='audioclose')
														{
														//alert(res2);
													//var message = res;
													//shownoti(message);
													
													 var msg = "Reposted Posting Successully";
													shownoti(msg); 
														}
														if(res2=='vedioclose')
														{
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									
									 $('#sound')[0].play(); 
														}
													if(res2=='bothstart')
													{
													//alert(res2);
													//var message = res;
													//shownoti(message);
													 var msg = "Reposted Posting Successully";
													shownoti(msg); 
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="'+root +'/assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									 $('#sound')[0].play(); 
													}
												 }
											});
							
							
                     });
				 </script>
		        @endif
@if( Auth::user()->roletype == 'AM' )
<script type="text/javascript">
	$('input[type=radio][name=archived]').on('change', function() {
      switch ($(this).val()) {
        case '1':
          var radioread = '1'
          break;
        case '2':
          var radioread = '2'
          break;
		case '3':
          var radioread = '3'
          break;
      }

	    var myPostingTable = $('#myPostingTable').DataTable( {
	    	destroy:true,
		    "ajax": {
		        "url":"myPosting_ajax",
		        "dataSrc": "",
		        "type": 'POST',
		        "data":{
		        	"productActionType":"group_filter",
		        	"radioread":radioread,
		        }
		    },
		    columns: [
		    { data: 'postno' },
		    { data: 'sdate' },
		    { data: 'brand' },
		    { data: 'ptype' },
		    { data: 'quantity' },
		    { data: 'productName' },
		    { data: 'currency'},
		    { data: 'customerrefno'},
		    { data: 'poststatus'},
		    { data: 'edit'}
		 
		    ]
		});
    });
</script>
@else

<script type="text/javascript">
	$('input[type=radio][name=archived]').on('change', function() {
      switch ($(this).val()) {
        case '1':
          var radioread = '1'
          break;
        case '2':
          var radioread = '2'
          break;
		case '3':
          var radioread = '3'
          break;
      }

	    var myPostingTable = $('#myPostingTable').DataTable( {
	    	destroy:true,
		    "ajax": {
		        "url":"myPosting_ajax",
		        "dataSrc": "",
		        "type": 'POST',
		        "data":{
		        	"productActionType":"group_filter",
		        	"radioread":radioread,
		        }
		    },
		    columns: [
		    { data: 'postno' },
		    { data: 'sdate' },
		    { data: 'brand' },
		    { data: 'ptype' },
		    { data: 'quantity' },
		    { data: 'productName' },
		    { data: 'price' },
		    { data: 'currency'},
		    { data: 'customerrefno'},
		    { data: 'poststatus'},
		    { data: 'edit'}
		 
		    ]
		});
    });
</script>
@endif
@if( Auth::user()->roletype == 'AM' )

<script type="text/javascript">

     $('#r_eset').on('click',function(){
        $('#cat_id,#sub_cat,#industry,#brand,#type,#timeframe,#status').children().removeAttr('selected');
        $('#cat_id,#sub_cat,#brand').html('');
     });

	$('#filter').click(function() {
      // var sortData = $("#sort option:selected").val();
      //var industryData = $("#industry option:selected").val();
      var categoryData = $('#cat_id option:selected').val();
      var scategoryData = $('#sub_cat option:selected').val();
      var brandData = $('#brand option:selected').val();
      var status = $('#status option:selected').val();
	  var t_ype = $('#type option:selected').val();
	  var t_imeframe = $('#timeframe option:selected').val();

	  var industry = [];
			$.each($("#industry option:selected"), function(){            
				industry.push($(this).val());
			});
			var industryData = industry.join(",");
			
			var cat_id = [];
			$.each($("#cat_id option:selected"), function(){            
				cat_id.push($(this).val());
			});
			var categoryData = cat_id.join(",");
			
			var sub_cat = [];
			$.each($("#sub_cat option:selected"), function(){            
				sub_cat.push($(this).val());
			});
			var scategoryData = sub_cat.join(",");
			
			var brand = [];
			$.each($("#brand option:selected"), function(){            
				brand.push($(this).val());
			});
			var brandData = brand.join(",");
			
			var myPostingTable = $('#myPostingTable').DataTable( {
		    	destroy:true,
			    "ajax": {
			        "url":"myPosting_ajax",
			        "dataSrc": "",
			        "type": 'POST',
			        "data":{
			        	"productActionType":"group_filter",
			        	"industryData":industryData,
			        	"categoryData":categoryData,
			        	"scategoryData":scategoryData,
			        	"brandData":brandData,
			        	"status":status,
			        	"type":t_ype,
			        	"timeframe":t_imeframe

			        }
			    },
			    columns: [
			    { data: 'postno' },
			    { data: 'sdate' },
			    { data: 'brand' },
			    { data: 'ptype' },
			    { data: 'quantity' },
			    { data: 'productName' },
			    { data: 'currency'},
			    { data: 'customerrefno'},
			    { data: 'poststatus'},
			    { data: 'edit'}
			 
			    ]
			});

    });
</script>

@else

<script type="text/javascript">

     $('#r_eset').on('click',function(){
        $('#cat_id,#sub_cat,#industry,#brand,#type,#timeframe,#status').children().removeAttr('selected');
        $('#cat_id,#sub_cat,#brand').html('');
     });

	$('#filter').click(function() {
      // var sortData = $("#sort option:selected").val();
      //var industryData = $("#industry option:selected").val();
      var categoryData = $('#cat_id option:selected').val();
      var scategoryData = $('#sub_cat option:selected').val();
      var brandData = $('#brand option:selected').val();
      var status = $('#status option:selected').val();
	  var t_ype = $('#type option:selected').val();
	  var t_imeframe = $('#timeframe option:selected').val();

	  var industry = [];
			$.each($("#industry option:selected"), function(){            
				industry.push($(this).val());
			});
			var industryData = industry.join(",");
			
			var cat_id = [];
			$.each($("#cat_id option:selected"), function(){            
				cat_id.push($(this).val());
			});
			var categoryData = cat_id.join(",");
			
			var sub_cat = [];
			$.each($("#sub_cat option:selected"), function(){            
				sub_cat.push($(this).val());
			});
			var scategoryData = sub_cat.join(",");
			
			var brand = [];
			$.each($("#brand option:selected"), function(){            
				brand.push($(this).val());
			});
			var brandData = brand.join(",");
			
			var myPostingTable = $('#myPostingTable').DataTable( {
		    	destroy:true,
			    "ajax": {
			        "url":"myPosting_ajax",
			        "dataSrc": "",
			        "type": 'POST',
			        "data":{
			        	"productActionType":"group_filter",
			        	"industryData":industryData,
			        	"categoryData":categoryData,
			        	"scategoryData":scategoryData,
			        	"brandData":brandData,
			        	"status":status,
			        	"type":t_ype,
			        	"timeframe":t_imeframe

			        }
			    },
			    columns: [
			    { data: 'postno' },
			    { data: 'sdate' },
			    { data: 'brand' },
			    { data: 'ptype' },
			    { data: 'quantity' },
			    { data: 'productName' },
			    { data: 'price' },
			    { data: 'currency'},
			    { data: 'customerrefno'},
			    { data: 'poststatus'},
			    { data: 'edit'}
			 
			    ]
			});

    });
</script>

@endif
<script type="text/javascript">
	jQuery(document).ready(function() {
        $('.dvfilter').hide();
        $('#show').click(function() {
            $('.dvfilter').toggle('slow');
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var indmode = "industaction";
        var formData = {
            type: indmode
        };
        $.ajax({
            type: "POST",
            url: root + "/setting/industry_ajax",
            data: formData,
            dataType: 'json',
            success: function(data) {
                $.each(data.industry, function(i, data) {
                    //alert(data.industryid);
                    var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
                    $(div_data).appendTo("#industry");
                });
            }
        });
    });

    //sub category function start
 $(document).ready(function() {
        $("#industry").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    $("#cat_id").empty();
                    $("#brand").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    var div_data = "<option value='0'>Select  Categories</option>";
                    $(div_data).appendTo("#cat_id");
                    var div_data2 = "<option value='0'>Select  Brands</option>";
                    $(div_data2).appendTo("#brand");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat_id");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brand");
                            });
                    } else
                    {
                        //alert("Categories does not exist");	
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#cat_id").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#sub_cat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");				
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>