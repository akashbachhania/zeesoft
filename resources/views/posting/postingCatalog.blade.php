@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-2">
                <h3 class="box-title">Postings</h3>    
              </div>
              <div class="col-md-4">
                
              </div>
              <div class="col-md-6">
                @if ( Auth::user()->roletype == 'AD')
                  <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
                @elseif($checkVal[0]['AddP']=='1')
                  <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
                @endif
                <button type="button" class="btn btn-info pull-right m_r5" id="show">Filter</button>
                @if ( Auth::user()->roletype == 'AD')
                  <a class="pull-right black m_r5">Bulk Action
                    <select class="table-group-action-input input-inline input-small input-sm" id="statuschange">
                      <option value="" selected>Change Status</option>
                      <option value="DISABLE">DISABLE</option>
                      <option value="ENABLE">ENABLE</option>
                      <option value="DELETE">DELETE</option>
                    </select></a>
                @endif    
              </div>
            </div>
            <div class="row m_t30">
              <div class="col-md-12 zee-margin dvfilter" id="dvfilter" style="display:none">
                <div class="col-md-3 zee-col">
                  <div class="form-group">
                    <select class="form-control industry" name="industry[]" id="industry" onChange="getState(this.value);getbrand(this.value);" multiple="multiple">
                      <option value="0" class="selectitem">Select Industries</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div id="categroy" class="form-group">
                    <select class="form-control category" onchange="subcat(this.value);" name="category[]" id="cat_id" multiple="multiple">
                      <option value="0">Select categories</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div class="form-group">
                  <select class="form-control" name="scategory[]" id="sub_cat" multiple="multiple">
                    <option value="0">Select Sub-categories</option>
                  </select>
                  </div>
                </div>
                <div class="col-md-2 zee-category">
                <div class="form-group">
                  <select class="form-control" id="brand" name="brand[]" multiple="multiple">
                    <option value="0">Select Brands</option>
                  </select>
                  </div>
                </div>
            </div>
            <div class="col-md-12 zee-margin dvfilter" id="dvfilter" style="display:none">
                <div class="col-md-3 zee-category">
                 <div class="form-group">
                  <select class="form-control" id="type" name="type">
                    <option value="">Select Type</option>
                    <option value="BUY">BUY</option>
                    <option value="SELL">SELL</option>
                  </select>
                  </div>
                </div>

                <div class="col-md-3 zee-category">
                 <div class="form-group">
                  <select class="form-control" id="timeframe" name="timeframe">
                    <option value="">Select Timeframe</option>
                    @foreach( $timeframes as $timeframe )
                      <option value="{{ $timeframe->name }}" > {{ $timeframe->name }} </option>
                    @endforeach
                  </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div class="form-group">
                    <button type="button" class="btn zee-filter btn-danger" id="filter">Show</button>
                    <button type="button" class="btn zee-filter btn-danger" id="r_eset" style="margin-left: 8px;">Reset</button>
                  </div>
                </div>
            </div>
            
            
          </div><!-- /.box-header -->
          <div class="box-body">
            @if(Auth::user()->roletype == "AD")
              <table id="postingCatalogTable1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th><input class="group-checkable" type="checkbox" data-set="#sample_1 .checkboxes"></th>
                    <th> Posting ID </th>
                    <th> Date Posted </th>
                    <th> Industry </th>
                    <th> Category </th>
                    <th> Brand </th>
                    <th> Type</th>
                    <th> Quantity</th>
                    <th> Product Name </th>
                    <th> Exp Date </th>
                    <th> Status </th>
                  </tr>
                </thead>
              </table>

            @else
              <table id="postingCatalogTable2" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th> Posting ID </th>
                    <th> Date Posted </th>
                    <th> Industry </th>
                    <th> Category </th>
                    <th> Brand </th>
                    <th> Type</th>
                    <th> Quantity</th>
                    <th> Product Name </th>
                    <th> Exp Date </th>
                  </tr>
                  
                </thead>
                <tbody>
                </tbody>
              </table>
            @endif
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('posting.postingCatalogJs')
@endsection