<script>
    
    jQuery(document).ready(function() {
    	$("#datepicker").daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});

	    // document.cookie ="bStateSave=true";
        var userid = '{{ Auth::user()->userid }}';
		var tempid = '{{ $postStatus }}';
	    var postno = '{{ \Request::segment(3) }}';
		var type = "fetchallposts";
        var dataString = 'postno=' + postno + '&type=' + type;
        if (postno != '') {
			//alert(tempid); 
            $.ajax({
                type: "POST",
                url: root+"/posting/fetchFunction",
                data: dataString,
                cache: false,
                success: function(response) {
                	console.log(response);
                    //console.log(data);
					//document.write(data);
					//alert(data);
                    // response = jQuery.parseJSON(data);
					//document.write(response);
                    var pro_id = response.productid;
                    $('#postno').html(response.postno);
                    $('#pdate').html(response.pdate);
                    $('#pname').html(response.name);
                    $('#productid').html(response.productno);
                    $('#productid2').val(response.productno);
                    $('#userid').html(response.userid);
                    $('#puserid').val(response.userid);
                    var puid = response.userid;
                    var uid = '{{ Auth::user()->userid }}';
                    //alert(puid);
                    //alert(uid);
                    if (puid == uid) {
                        $('#sendoffer-form').hide();
                        //$('#languagehide').hide();
                    }
                    if (puid != uid) {
                        $('#languagehide').hide();
						$('#countryorgin').hide();
                    }
                   
                    $('#name').html(response.name);
                    $('#industryid').html(response.industry);
                    $('#catid').html(response.category);
                    $('#subcatid').html(response.subcategory);
                    $('#brandid').html(response.brand);
                    $('#manufacture').html(response.manufacture);
                    $('#pakaging').html(response.pakaging);
					
					if (puid == uid) {
                       $('#countryorgin').html(response.country);
                        //$('#languagehide').hide();
                    }
                    if (puid != uid) {
                        $('#languagehide').hide();
						$('#countryorgin').hide();
						$('#countryhide').hide();
                    }
					
					if (uid == 'admin') {
						$('#countryorgin').html(response.country);
                        $('#languagehide').show();
						$('#countryorgin').show();
						
                    }
                    $('#packlanguage').html(response.packlang);
                    $('#shipingcondition').html(response.shipingcondition);
                    $('#procodes').html(response.procodes);
                    //$('#pstatus').html(response.pstatus);
					if (puid == uid) {
					 $('#pstatus').html(tempid);
					 }
					 else
					 {
					  $('#hst').hide();
					 }
                    var uid = response.userid;
                    var targetprice = response.targetprice;
                    if ((userid == uid) || (userid == 'admin')) {
                        $('#cusrefno').html(response.customerrefno);
                        $('#targetprice').html((parseFloat(targetprice)).toFixed(2));
                        $('#location').html(response.location);
                    } else {
                        //$('#hst').hide();
                        $('#hprice').hide();
                        $('#hcrn').hide();
                        $('#hloc').hide();
                    }
                    $('#timeframe').html(response.timeframe);
                    if (userid == response.userid) {
                        var exactexpdrange = response.expirydate;
                        if (exactexpdrange != '0000-00-00 00:00:00') {
                            $('#expirydate').html(response.expirydate);
                        } else {
                            $('#expirydate').html('Not Applicable');
                        }
						if (exactexpdrange != '') {
                            $('#expirydate').html(response.expirydate);
                        } else {
                            $('#expirydate').html('Not Applicable');
                        }
						
                    } else {
                        $('#expd').hide();
                    }
                    $('#expdate').html(response.expdate);
                    $('#uom').html(response.uom);
                    $('#quantity').html(response.quantity);
                    $('#ptype').html(response.ptype);
                    var posttype = response.ptype;
					
                    $("#fimges").html(response.image);

                    $("#procodes").html(response.productcodes);
                MagicZoom.refresh();
                }
            });
        }
        return false;
    });
</script>


<script type="text/javascript">
	jQuery(document).ready(function() {

	    $("#btnnew").on("click", function() {
	      $('#basic').modal('show');
	      $("#msgg").hide();
	      //$("#checkProduct").val('');
	    });

	    $("#statuschange").change(function() {
	      var postid = {{ \Request::segment(3) }};
	      var pname = $("#statuschange").val();
	      if (pname == 'DELETE') {
	        var actionType = 'delete';
	      }
	      if (pname == 'ENABLE') {
	        var actionType = 'enableaction';
	      }
	      if (pname == 'DISABLE') {
	        var actionType = 'disableaction';
	      }
	      if (pname == 'ENABLE' || pname == 'DISABLE') {
	        input_box = confirm("Are you sure you wish to change the status of this Posting ?");
	      } else {
	        input_box = confirm("Are you sure you wish to delete this Posting ?");
	      }
	      //alert(input_box);
	      if (input_box == true && pname != '') {
	        $.ajax({
	          type: "POST",
	          url: root+"/posting/detailViewDelete",
	          async: false,
	          data: {
	            postno: postid,
	            action: actionType
	          },
	          success: function(data, status, xhr) {

	          	  if( data == 'under process' ){
	          	  	window.location.href= root + "/posting/postingCatalog?v=Posting is under process";
	          	  }
	          	  if( data == 'done' ){
	          	  	window.location.href = root + '/posting/postingCatalog?v=Deleted';
	          	  }

				  if(data=='1')
				  {
				  	
				  	if( pname == 'ENABLE' ){
		                shownoti('Enabled Posting Successully');
		            }
		            else if( pname == 'DISABLE' ){
		                shownoti('Disabled Posting Successully');
		            }
		            setInterval( function () {
					    window.location.href = "";
					}, 3000 );
		            
				  }
	          },
	          error: function(jqXHR, textStatus, errorThrown) {
	            alert(errorThrown);
	            commit(false);
	          }
	        });
	      }
	    });
	});


	function change(aid){

	  //alert(aid);
	  var iname=aid.name;
	  //alert(iname);
	  if(iname.indexOf('http://')>=0)
	  {
	    //document.getElementById("bimg").src=iname;
	  }
	  else
	  {
			$.ajax({
			type:'POST',
			data:'',
			url:'ajax.php?aid='+iname,
			success:function(data){
			//alert(data);
			var Arr=data.split('|');
				$('#image').html(Arr[0]);
			}
		});
		
	  }
	}
</script>

<script type="application/javascript">
    jQuery(document).ready(function() {
    	
    	var uid = '{{ Auth::user()->userid }}';
    	
        var postno = '{{ \Request::segment(3) }}';
		var type = "fetchallposts";
        var dataString = 'postno=' + postno + '&type=' + type;
        //alert(dataString);
		 $.ajax({
	        type: "POST",
	        url: root+"/posting/fetchFunction",
	         data: dataString,
	        cache: false,
	        success: function(response) {
	       	
	        // response = jQuery.parseJSON(data);					
	        var puid = response.userid; 
	        var posttype = response.ptype;
		       
	        if((posttype=='SELL') || (posttype=='Sell') && (puid != uid))
				{
			   $('#expd2').hide();
				}
				
				if((posttype=='BUY') || (posttype=='Buy'))
				{
				   $('#expd2').show();
				}                  
	  	    }
         });
    	
        $("#go").on("click", function() {  	
            $('#basic').modal('show');
        });
    });
    $("#btnclose").click(function() {
        $('#basic').modal('hide');
		 $('#form_sample_2')[0].reset();
    });
    $("#saveoffer").click(function() {
        if ($("#price").val() == '') {
            alert('Price Required');
            return false;

        }

        if ($("#offerCurrency").val() == '') {
            alert('Currency Required');
            return false;
        }



        if ($("#offerUoM").val() == '') {
            alert('UoM Required');
            return false;
        }
		
		 if ($("#offerqty").val() == '') {
            alert('Quantity Required');
            return false;
        }


        if ($("#offerlocation").val() == '') {
            alert('Location By Required');
            return false;
        }

        if ($("#offerdaterange").val() == '') {
            alert('Range Date Required');
            return false;
        }

        if ($("#offerlanguage").val() == '') {
            alert('Language Required');
            return false;
        }

        if ($("#offertimeframe").val() == '') {
            alert('Time Frame Failed');
            return false;
        }
		else
		{
			var postno = '{{ \Request::segment(3) }}';
			var type = 'addoffer';
			var pro_id = $('#productid2').val();
			var price = $('#price').val();
			var offerCurrency = $('#offerCurrency').val();
			var offerUoM = $('#offerUoM').val();
			var offerqty = $('#offerqty').val();
			var offerlocation = $('#offerlocation').val();
			var offerdaterange = $('#offerdaterange').val();
			var datepicker = $('#datepicker').val();
			var offerlanguage = $('#offerlanguage').val();
			var offertimeframe = $('#offertimeframe').val();
			var addcountry = $('#addcountry').val();
			var puserid = $('#puserid').val();

			var data = {
		        type: type,
		        postno: postno,
		        pro_id: pro_id,
		        price: price,
		        offerCurrency: offerCurrency,
		        offerUoM: offerUoM,
		        offerqty: offerqty,
		        offerlocation: offerlocation,
				offerdaterange: offerdaterange,
		        datepicker: datepicker,
		        offerlanguage:offerlanguage,
		        offertimeframe: offertimeframe,
				addcountry: addcountry,
		        puserid:puserid,
	      }; 
		
		 $.ajax({
			type: "POST",
			url: root+"/posting/addOffer",
			data: data, 
			cache: false,
			success: function(data)
			{
			//alert(data);
			if(data==1)
			{
				var message = "Successfully submitted Offer";
				shownoti(message);
				$('#basic').modal('hide');
				$('#form_sample_2')[0].reset();
				window.location.href= root + '/posting/postingCatalog';
			}
			}
		});
	  }
    });
</script>
<script type="text/javascript">
    function shownoti(message) {
        $(function() {
            function Toast(type, css, msg) {
                this.type = type;
                this.css = css;
                this.msg = msg;
            }
            var toasts = [
                new Toast('success', 'toast-top-right', message),
            ];
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.options.extendedTimeOut = 0; //1000;
            toastr.options.timeOut = 2000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            var i = 0;
            /* $('#tryMe').click(function () {

                 $('#tryMe').prop('disabled', true);*/
            delayToasts();
            //});
            function delayToasts() {
                if (i === toasts.length) {
                    return;
                }
                var delay = i === 0 ? 0 : 2100;
                window.setTimeout(function() {
                    showToast();
                }, delay);
                // re-enable the button        
                if (i === toasts.length - 1) {
                    window.setTimeout(function() {
                        // prop('disabled', false);
                        i = 0;
                    }, delay + 1000);
                }
            }

            function showToast() {
                var t = toasts[i];
                toastr.options.positionClass = t.css;
                toastr[t.type](t.msg);
                i++;
                delayToasts();
            }
        })
    }
</script>