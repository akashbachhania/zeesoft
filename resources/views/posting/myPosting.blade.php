@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-2">
                <h3 class="box-title">My Postings</h3>    
              </div>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                      <div class="form-group">
                         <label class="m_r10">
                           <input type="radio" name="archived" value="1" id="archived">
                           Archived
                         </label>      
                         <label class="m_r10">
                           <input type="radio" name="archived" value="2" id="archived">
                           Active
                         </label>
                         <label>
                           <input type="radio" name="archived" value="3" id="archived" checked="checked">
                           All
                         </label>
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                      @if ( Auth::user()->roletype == 'AD' ) 
                        <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
                      @else 
                        
                        @if($checkVal[0]['AddP']=='1')
                          <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
                        @endif

                      @endif
                      <button type="button" class="btn btn-info pull-right m_r5 zee-filter" id="show">Filter</button>        
                  </div>
                </div>
              </div>
            </div>
            <div class="row m_t30">
              <div class="col-md-12 col-xs-12 zee-margin dvfilter" id="dvfilter" style="display:none">
                <div class="col-md-3 zee-col">
                  <div class="form-group">
                    <select class="form-control industry" name="industry[]" id="industry" onChange="getState(this.value);getbrand(this.value);" multiple="multiple">
                      <option value="0" class="selectitem">Select Industries</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div id="categroy">
                   <div class="form-group">
                    <select class="form-control category" onchange="subcat(this.value);" name="category[]" id="cat_id" multiple="multiple">
                      <option value="0">Select categories</option>
                    </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                 <div class="form-group">
                  <select class="form-control" name="scategory[]" id="sub_cat" multiple="multiple">
                    <option value="0">Select Sub-categories</option>
                  </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                 <div class="form-group">
                  <select class="form-control" id="brand" name="brand[]" multiple="multiple">
                    <option value="0">Select Brands</option>
                  </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-xs-12 zee-margin dvfilter" id="dvfilter" style="display: none;">  

                <div class="col-md-3 zee-category">
                 <div class="form-group">
                  <select class="form-control" id="type" name="type">
                    <option value="">Select Type</option>
                    <option value="BUY">BUY</option>
                    <option value="SELL">SELL</option>
                  </select>
                  </div>
                </div>

                <div class="col-md-3 zee-category">
                 <div class="form-group">
                  <select class="form-control" id="timeframe" name="timeframe">
                    <option value="">Select Timeframe</option>
                    @foreach( $timeframes as $timeframe )
                      <option value="{{ $timeframe->name }}" > {{ $timeframe->name }} </option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <div class="col-md-3 zee-category">
                  <div class="form-group">
                      <select class="form-control" id="status">
                        <option value="0">Select Status</option>
                        <option value="NEW">NEW</option>
                        <option value="Submitted">SUBMITTED</option>
                        <option value="Completed">COMPLETE</option>
                       <option value="CANCELLED">CANCELLED</option>
                      </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div class="form-group">
                  <button type="button" class="btn btn-danger zee-filter" id="filter">Show</button>
                  <button type="button" class="btn zee-filter btn-danger" id="r_eset" style="margin-left: 8px;">Reset</button>
                  </div>
                </div>
              </div>

            </div>
          </div><!-- /.box-header -->
          <div class="box-body">

              <table id="myPostingTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="8%"> Posting ID </th>
                    <th width="10%"> Date Posted </th>
                    <th width="5%"> Brand </th>
                    <th width="5%"> Type </th>
                    <th width="5%"> Quantity</th>
                    <th width="29%"> Product Name </th>
                    @if( Auth::user()->roletype != 'AM' )
                    <th width="5%"> Price </th>
                    @endif
                    <th width="5%"> Curr</th>
                    <th width="8%"> Cust Ref</th>
                    <th width="5%"> Status </th>
                    <th width="30%"> Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('posting.myPostingJs')
@endsection