<script type="text/javascript">
    $(document).ready(function(){
                var postingCatalogTable1 = $('#postingCatalogTable1').DataTable( {

            "ajax": {
                "url":"postingCatalog_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0 ] }
            ],
            columns: [
            { data: 'productCheck' },
            { data: 'postno' },
            { data: 'sdate' },
            { data: 'industry'},
            { data: 'category' },
            { data: 'brand' },
            { data: 'ptype' },
            { data: 'quantity'},
            { data: 'productName'},
            { data: 'timeframe'},
            { data: 'enableaction'}
         
            ]
        } );
        var postingCatalogTable2 = $('#postingCatalogTable2').DataTable( {

            "ajax": {
                "url":"postingCatalog_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0 ] }
            ],
            columns: [
            { data: 'postno' },
            { data: 'sdate' },
            { data: 'industry'},
            { data: 'category' },
            { data: 'brand' },
            { data: 'ptype' },
            { data: 'quantity'},
            { data: 'productName'},
            { data: 'timeframe'}
         
            ]
        } );    

        // setInterval( function () {
        //     postingCatalogTable1.ajax.reload();
        // }, 3000 );

        $('#postingCatalogTable1 tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
            var data = postingCatalogTable1.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
             //window.open(page, '_blank');
        });

        // setInterval( function () {
        //     postingCatalogTable2.ajax.reload();
        // }, 3000 );

        $('#postingCatalogTable2 tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
            var data = postingCatalogTable2.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
             //window.open(page, '_blank');
        });    

        $('.group-checkable').on('click',function(){
            
            if( $('.group-checkable').is(':checked') ){
                $('.checkProduct').prop('checked',true);
            }else{
                $('.checkProduct').prop('checked',false);
            }


        });
    });
</script>
<script type="text/javascript">
	$('#statuschange').change(function() {

        var exportData = '';
        var i = 0;
        $.each($("input[name='checkProduct']:checked"), function() {
          exportData = exportData + "'" + ($(this).val()) + "',";
          i++;

        });
        exportData = exportData.substring(0, exportData.length - 1);
        if (i != 0) {
          var statusData = $("#statuschange option:selected").val();
		  // alert(statusData);
		  //alert(exportData);
		  if(statusData=='ENABLE' || statusData=='DISABLE')
		  {
          input_box = confirm("Are you sure you wish to change the status of this Posting?");
		  }
		  else
		  {
		  	input_box = confirm("Are you sure you wish to delete this Posting?");
		  }
          if (input_box == true && statusData != '')
          {
         
            var postingCatalogTable1 = $('#postingCatalogTable1').DataTable( {
            	destroy:true,
	            "ajax": {
	                "url":"postingCatalog_ajax",
	                "dataSrc": "",
	                "type": 'POST',
	                "data":{
	                	statusActionType:'shatusChange',
	                	statusData:statusData,
	                	checkUser:exportData,
	                }
	            },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
	            columns: [
	            { data: 'productCheck' },
	            { data: 'postno' },
	            { data: 'sdate' },
	            { data: 'industry'},
	            { data: 'category' },
	            { data: 'brand' },
	            { data: 'ptype' },
	            { data: 'quantity'},
	            { data: 'productName'},
	            { data: 'timeframe'},
	            { data: 'enableaction'}
	         
	            ]
	        } );
	        var postingCatalogTable2 = $('#postingCatalogTable2').DataTable( {
	        	destroy:true,
	            "ajax": {
	                "url":"postingCatalog_ajax",
	                "dataSrc": "",
	                "type": 'POST',
	                "data":{
	                	statusActionType:'shatusChange',
	                	statusData:statusData,
	                	checkUser:exportData,
	                }

	            },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
	            columns: [
	            { data: 'postno' },
	            { data: 'sdate' },
	            { data: 'industry'},
	            { data: 'category' },
	            { data: 'brand' },
	            { data: 'ptype' },
	            { data: 'quantity'},
	            { data: 'productName'},
	            { data: 'timeframe'}
	         
	            ]
	        } );
	        
            $("#statuschange option[value='']").removeAttr('selected');
            if( statusData == 'DELETE' ){
                shownoti('Deleted Posting Successully');    
            }
            else if( statusData == 'ENABLE' ){
                shownoti('Enabled Posting Successully');
            }
            else if( statusData == 'DISABLE' ){
                shownoti('Disabled Posting Successully');
            }
            
          }
        }
      }
    );
</script>
<script>

    jQuery(document).ready(function() {
        $('.dvfilter').hide();
        $('#show').click(function() {
            $('.dvfilter').toggle('slow');
			 // document.cookie ="show=showopen";
			 // document.cookie ="bStateSave=true";
        });
		
		
        $('#postingCatalogTable1 tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
            var data = table.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
             //window.open(page, '_blank');
        });
        $('#postingCatalogTable2 tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
            var data = table.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
             //window.open(page, '_blank');
        });
    });
</script> 

<script type="text/javascript">
    jQuery(document).ready(function() {
        var indmode = "industaction";
		var formData = {
            type: indmode
        };
        $.ajax({
            type: "POST",
            url: "{{ url('setting/industry_ajax') }}",
            data: formData,
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                $.each(data.industry, function(i, data) {
                    
                    var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
					
                    $(div_data).appendTo("#industry");
                });
            }
        });
    });

    //sub category function start
	 $(document).ready(function() {
        $("#industry").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    $("#cat_id").empty();
                    $("#brand").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    var div_data = "<option value='0'>Select  Categories</option>";
                    $(div_data).appendTo("#cat_id");
                    var div_data2 = "<option value='0'>Select  Brands</option>";
                    $(div_data2).appendTo("#brand");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat_id");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brand");
                            });
                    } else
                    {
                        //alert("Categories does not exist");	
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
		
	/**************************Defulit open*************************************/
	
	/*********************************End********************************/
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#cat_id").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#sub_cat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");				
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
		
		/******************************************************************/

	/*****************************************************************/	
    });
</script>

<script type="text/javascript">
	
     $('#r_eset').on('click',function(){
        $('#cat_id,#sub_cat,#industry,#brand,#type,#timeframe').children().removeAttr('selected');
        $('#cat_id,#sub_cat,#brand').html('');
     });

     $('#filter').click(function() {
      // var sortData = $("#sort option:selected").val();
     // var industryData = $("#industry option:selected").val();
      var categoryData = $('#cat_id option:selected').val();
      var scategoryData = $('#sub_cat option:selected').val();
      var brandData = $('#brand option:selected').val();
	  var t_ype = $('#type option:selected').val();
      var t_imeframe = $('#timeframe option:selected').val();

	  	var industry = [];
			$.each($("#industry option:selected"), function(){            
				industry.push($(this).val());
			});
			
			var industryData = industry.join(",");
			
			
			var cat_id = [];
			$.each($("#cat_id option:selected"), function(){            
				cat_id.push($(this).val());
			});
			var categoryData = cat_id.join(",");
		
			var sub_cat = [];
			$.each($("#sub_cat option:selected"), function(){            
				sub_cat.push($(this).val());
			});
			var scategoryData = sub_cat.join(",");
				
			var brand = [];
			$.each($("#brand option:selected"), function(){            
				brand.push($(this).val());
			});
			var brandData = brand.join(",");
			//alert(brandData);
	  
			var postingCatalogTable1 = $('#postingCatalogTable1').DataTable( {
            	destroy:true,
	            "ajax": {
	                "url":"postingCatalog_ajax",
	                "dataSrc": "",
	                "type": 'POST',
	                "data":{
	                	"industryData":industryData,
						"categoryData":categoryData,
						"scategoryData":scategoryData,
						"brandData":brandData,
						"productActionType":"group_filter",
                        "type":t_ype,
                        "timeframe":t_imeframe
	                }
	            },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
	            columns: [
	            { data: 'productCheck' },
	            { data: 'postno' },
	            { data: 'sdate' },
	            { data: 'industry'},
	            { data: 'category' },
	            { data: 'brand' },
	            { data: 'ptype' },
	            { data: 'quantity'},
	            { data: 'productName'},
	            { data: 'timeframe'},
	            { data: 'enableaction'}
	         
	            ]
	        } );
	        var postingCatalogTable2 = $('#postingCatalogTable2').DataTable( {
	        	destroy:true,
	            "ajax": {
	                "url":"postingCatalog_ajax",
	                "dataSrc": "",
	                "type": 'POST',
	                "data":{
	                	"industryData":industryData,
						"categoryData":categoryData,
						"scategoryData":scategoryData,
						"brandData":brandData,
						"productActionType":"group_filter",
	                }

	            },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
	            columns: [
	            { data: 'postno' },
	            { data: 'sdate' },
	            { data: 'industry'},
	            { data: 'category' },
	            { data: 'brand' },
	            { data: 'ptype' },
	            { data: 'quantity'},
	            { data: 'productName'},
	            { data: 'timeframe'}
	         
	            ]
	        } );
	  		
      // if (sortData == '') {
        // alert('please select the Sort By');
      // } else {
        // grid.setAjaxParam("productActionType", "group_filter");
        // grid.setAjaxParam("customActionName", sortData);
        // grid.setAjaxParam("industryData", industryData);
        // grid.setAjaxParam("categoryData", categoryData);
        // grid.setAjaxParam("scategoryData", scategoryData);
        // grid.setAjaxParam("brandData", brandData);
        // grid.setAjaxParam("id", grid.getSelectedRows());
        // grid.getDataTable().ajax.reload();
        // grid.clearAjaxParams();
      // }
    });
</script>
<script type="text/javascript">
    function shownoti(message) {
        $(function() {
            function Toast(type, css, msg) {
                this.type = type;
                this.css = css;
                this.msg = msg;
            }
            var toasts = [
                new Toast('success', 'toast-top-right', message),
            ];
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.options.extendedTimeOut = 0; //1000;
            toastr.options.timeOut = 2000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            var i = 0;
            /* $('#tryMe').click(function () {

                 $('#tryMe').prop('disabled', true);*/
            delayToasts();
            //});
            function delayToasts() {
                if (i === toasts.length) {
                    return;
                }
                var delay = i === 0 ? 0 : 2100;
                window.setTimeout(function() {
                    showToast();
                }, delay);
                // re-enable the button        
                if (i === toasts.length - 1) {
                    window.setTimeout(function() {
                        // prop('disabled', false);
                        i = 0;
                    }, delay + 1000);
                }
            }

            function showToast() {
                var t = toasts[i];
                toastr.options.positionClass = t.css;
                toastr[t.type](t.msg);
                i++;
                delayToasts();
            }
        })
    }
</script>
@if(\Request::input('page') == "addpost")
    <script>
        $(document).ready(
            function() {
                var id = "{{ \Request::input('page') }}";
                var lognoti = "lognoti";
                $.ajax({
                    url: root + "/notiAlert",
                    type: "post",
                    data: {
                        'id': id,
                        'lognoti': lognoti
                    },
                    cache: false,
                success: function(res) {
                    //alert(res);
                    $.ajax({
                        url: root + "/noti6",
                        type: "post",
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'html',
                        success: function(res2) {
                            //alert(res2);
                            if (res2 == 'bothclose') {
                            }
                            if (res2 == 'audioclose') {
                                //alert(res2);
                                var message = res;
                                shownoti(message);
                            }
                            if (res2 == 'vedioclose') {
                                $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                                $('#sound')[0].play();
                            }
                            if (res2 == 'bothstart') {
                                //alert(res2);
                                var message = res;
                                shownoti(message);
                                $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                                $('#sound')[0].play();
                            }
                        }
                    });
                    //var message = res;
                    //shownoti(message);
                }
            });
            //shownoti("User Created Successfully"); // refresh div after 5 secs
        });
</script>
@endif

@if( \Request::input('v') == 'Posting is under process' )
    
    <script type="text/javascript">
        shownoti('Posting is under process');
    </script>

@elseif( \Request::input('v') == 'Deleted' )
    
    <script type="text/javascript">
        shownoti('Deleted Posting Successully');
    </script>

@endif
