@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">My Offers</h3>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="myOffersTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th> Posting ID </th>
                  <th> Quote ID </th>
                  <th> Date Offered </th>
                  <th> Industry </th>
                  <th> Brand </th>
                  <th> Type </th>
                  <th> Quantity </th>
                  <th> Product Name </th>
                  <th> Offer Price </th>
                  <th> Curr </th>
                  <th> TimeFrame </th>
                  <th> Exp Date </th>
                  <th> Status </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('posting.myOffersJs')
@endsection