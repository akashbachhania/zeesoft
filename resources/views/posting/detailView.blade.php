@extends('layout.master')

@section('content')
<section class="content">
<div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          	<div class="col-md-3">
          		<h3 class="box-title" id="pname"></h3>	
          	</div>
      		
        	@if(Auth::user()->roletype == 'AD')

		        <div class="col-md-3 col-md-push-6">
		          <div class="row" style="">
		            <div class="col-md-10 form-group">
		              <select class="table-group-action-input form-control input-inline input-small input-sm" id="statuschange">
		                <option value="" selected>Change Status</option>
		                <option value="DISABLE">DISABLED</option>
		                <option value="ENABLE">ENABLED</option>
		                <option value="DELETE">DELETE</option>
		              </select>
		            </div>
		          </div>
		        </div>
        	@endif

        	@if(Auth::user()->roletype == 'AM')

				@if($checkVal[0]['AddP']=='1')
			 
			        <div class="col-md-3 col-md-push-6" id="sendoffer-form">
			          <form action="#" class="form-horizontal">
			            <div class="row">
			              <div class="col-md-10 form-group">
			                <select class="form-control">
			                  <option>Submit Offer</option>
			                </select>
			              </div>
			              <div class="col-md-3"> <a href="#" id="btnnew"  data-toggle="modal">
			                <button type="button"  class="btn btn-success">Go</button>
			                </a> </div>
			            </div>
			          </form>
			        </div>
        		
        		@endif

        	@endif

          </div>

          <div class="box-body">

          	 <div class="row">
          	 	<div class="col-md-12">
          	 		<div class="col-md-6">
          	 			<label style="color:#000000; font-weight:700">Posting ID : <font id="postno"></font> | Creation Date : <font id="pdate"></font> EST</label>	
          	 		</div>
          	 		

          	 		@if(Auth::user()->roletype == 'AD')
					  					  
					  @if( $postStatus == 0 )
					  
					  	@if($postingStatus == 0)
					  
				          <div class="col-md-1 col-md-push-5"><span class="label label-sm label-danger" style="font-size:13px !important">Disabled</span></div>
				        @else
				          
			              <div class="col-md-1 col-md-push-5"><span class="label label-sm label-danger" style="font-size:13px !important">EXPIRED</span></div>
			            @endif
			          
			          @else
			              
			              <div class="col-md-1 col-md-push-5"><span class="label label-sm label-success" style="font-size:13px !important"> Enabled </span></div>
			          @endif

			        @endif

          	 	</div>
          	 </div>

          	 <div class="row m_t30">
          	 	<div class="col-md-4">
          	 		<div class="blog-img blog-tag-data" id="fimges"> 
                
                		<img src="{{url('img/large-blank.png')}}" alt="" class="img-responsive hideMeBoy"> 
                	</div>
          	 	</div>

          	 	<div class="col-md-8">

          	 		<div class="row">
          	 			<div class="col-md-12">
          	 				<h4 style="font-weight: 700">Product Details</h4>
          	 			</div>
          	 		</div>

          	 		<div class="row m_t10">
          	 			<div class="col-md-12">
          	 				
          	 				<div class="table-responsive no-padding">
								<table id="productviewtable" class="table table-hover">
									<tbody>
										@if( Auth::user()->userid == "UUZZ-9984" )
									
					                        <tr>
					                          <td><strong>User ID </strong></td>
					                          <td>:</td>
					                          <td><div align="left" id="userid"></div></td>
					                        </tr>

					                    @endif

										<tr>
											<td><strong>Product ID</strong></td>
											<td>:</td>
											<td><div align="left" id="productid"> </div></td>
										</tr>
										<tr>
											<td><strong>Product Name</strong></td>
											<td>:</td>
											<td><div align="left" id="name"></div></td>
										</tr>
										<tr>
											<td><strong>Industry</strong></td>
											<td>:</td>
											<td><div align="left" id="industryid"> </div></td>
										</tr>
				                        <tr>
				                          <td><strong>Category </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="catid"> </div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Sub-Category </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="subcatid"> </div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Brand</strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="brandid"></div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Manufacturer </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="manufacture"></div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Packaging </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="pakaging"></div></td>
				                        </tr >
				                        <tr>
				                          <td><strong>Shipping Conditions </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="shipingcondition"></div></td>
				                        </tr>
				                        <tr {{ $x }}>
				                          <td><strong>Product Codes </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="procodes"> </div></td>
				                        </tr>
									</tbody>
								</table>
							</div>	
          	 			</div>
          	 		</div>

          	 		<div class="row m_t30">
          	 			<div class="col-md-12">
          	 				<h4 style="font-weight: 700">Posting Details</h4>
          	 			</div>
          	 		</div>

          	 		<div class="row m_t10">
          	 			<div class="col-md-12">
          	 				<div class="table-responsive no-padding">
								<table id="productviewtable" class="table table-hover">
									<tbody>
										<tr>
				                          <td><strong>Type </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="ptype"> </div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Quantity </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="quantity"></div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Unit Of Measurement</strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="uom"> </div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Expiry Date Range </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="expdate"> </div></td>
				                        </tr>
				                        <tr id="expd">
				                          <td><strong>Exact Expiry Date </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="expirydate"> </div></td>
				                        </tr>
				                        <tr>
				                          <td><strong>Timeframe</strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="timeframe"></div></td>
				                        </tr>
				                        <tr id="hst">
				                          <td><strong>Status </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="pstatus"></div></td>
				                        </tr>
				                        <tr id="hcrn">
				                          <td><strong>Customer Refrence No. </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="cusrefno"> </div></td>
				                        </tr>
				                        <tr id="hprice">
				                          <td><strong>Price</strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="targetprice"></div></td>
				                        </tr>
				                        <tr id="hloc">
				                          <td><strong>Location </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="location"></div></td>
				                        </tr>
				                        <tr id="countryhide">
				                          <td><strong>Country of Origin</strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="countryorgin"></div></td>
				                        </tr>
				                        <tr id="languagehide">
				                          <td><strong>Packaging Language </strong></td>
				                          <td>:</td>
				                          <td><div align="left" id="packlanguage"></div></td>
				                        </tr>
									</tbody>
								</table>
							</div>	
          	 			</div>
          	 		</div>
          	 	</div>
          	 </div>

          </div>
        </div>
    </div>
</div>
</section>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="text-align:left">Send Offer </h4>
      </div>
      <div class="modal-body" style="text-align:left">
      	<!-- form start -->
      	  <form action="#" id="form_sample_2" class="form-horizontal"  novalidate="novalidate">

      	  	<div class="form-body">
            
	            <div id="msg" name="msg"></div>
	            
	            <input type="hidden" name="productid2" id="productid2" />
	            
	            <input type="hidden" name="puserid" id="puserid" />
	            
	            <div class="form-group row">
	              
	              <label class="col-md-4" style="text-align:right">Price <span class="required"> * </span> </label>
	              
	              <div class="col-md-6">
	              
	                <input type="text" class="form-control" maxlength="20" id="price" name="price">
	              
	              </div>
	            
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">Currency <span class="required">*</span> </label>
	              <div class="col-md-6">

                    <select class="form-control" name="offerCurrency" id="offerCurrency">
                    	<option value="" class="selectitem">Select Currency</option>
                    
                    	@foreach( $currencies as $curr )
                    
                    		<option value="{{ $curr->name }}"> {{ $curr->name }} </option>
                    
                    	@endforeach
                  	
                  	</select>
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">UoM <span class="required"9> * </span> </label>
	              <div class="col-md-6">
	                  <select class="form-control" name="offerUoM" id="offerUoM">
	                    <option value="" class="selectitem">Select UoM</option>
	                    @foreach( $uom as $u )
	                    <option value="{{ $u->name }}"> {{ $u->name }}</option>
	                    @endforeach
	                  </select>
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">Quantity <span class="required"> * </span> </label>
	              <div class="col-md-6">
	                <input type="text" class="form-control" maxlength="5" id="offerqty" name="offerqty">
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">Location <span class="required"9> * </span> </label>
	              <div class="col-md-6">
	                  <select class="form-control" name="offerlocation" id="offerlocation">
	                    <option value="" class="selectitem">Select Location</option>
	                    @foreach( $locations as $location )
	                    <option value="{{ $location->name }}">{{ $location->name }}</option>
	                    @endforeach
	                  </select>
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">Expiry Date Range <span class="required"9> * </span> </label>
	              <div class="col-md-6">
	                  <select class="form-control" name="offerdaterange" id="offerdaterange">
	                    <option value="" class="selectitem">Select Expiry Date Range</option>
	                    @foreach( $expdateranges as $expdaterange )
	                    <option value="{{ $expdaterange->name }}">{{ $expdaterange->name }}</option>
	                    @endforeach
	                  </select>
	              </div>
	            </div>

	            <div class="form-group row" id="expd2">
	              <label class="col-md-4" style="text-align:right">Exact Expiry Date <span class="required"9> * </span> </label>
	              <div class="col-md-6">
	              	 <div class="input-group">
	              	 	<div class="input-group-addon">
	                        <i class="fa fa-calendar"></i>
                        </div>
	                    <input type="text" class="form-control" readonly name="datepicker" style="background-color:#FFFFFF" id="datepicker">	
	              	 </div>
	              	  
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">Language <span class="required"> * </span> </label>
	              <div class="col-md-6">
	                  
	                  <select multiple class="form-control" name="offerlanguage[]" id="offerlanguage">
	                    <option value="">Select Language</option>
	                    @foreach( $ml as $lang )
	                    	
	                      <option value="{{ $lang->languageid }}" {{ ( $lang->name == 'English' ) ? 'selected="selected"' : '' }}>{{ $lang->name }}</option>

	                    @endforeach
	                  </select>
	               
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">Country Of Origin :</label>
	              <div class="col-md-6">
	              	 
	                  <select class="form-control" name="addcountry[]"  id="addcountry" multiple class="form-control country">
	                  <option value="">Select Countries</option>
	                  @foreach( $countries as $country )
	                  <option value="{{ $country->name }}">{{ $country->name }}</option>
	                  @endforeach
	                  </select>
	               
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-md-4" style="text-align:right">TimeFrame <span class="required"9> * </span> </label>
	              <div class="col-md-6">

	                  <select class="form-control" name="offertimeframe" id="offertimeframe">
	                    <option value="" class="selectitem">Select TimeFrame</option>
	                    @foreach( $timeframes as $timeframe )
	                    <option value="{{ $timeframe->name }}">{{ $timeframe->name }}</option>
	                    @endforeach
	                  </select>
	              
	              </div>
	            </div>	

            </div>

      	  </form>
      	<!-- form end -->
      </div>
      <div class="modal-footer"> <a href="#" id="btnclose" class="btn btn-default" ><i class="fa"> </i>&nbsp; Cancel</a>
        <button type="button" id="saveoffer" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>

@include('posting.jsDetails')
@endsection