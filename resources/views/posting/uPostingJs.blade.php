<script type="text/javascript">
	var uPostingTable = $('#uPostingTable').DataTable( {

        "ajax": {
            "url":root + "/posting_ajax",
            "dataSrc": "",
            "type": 'POST',
            "data":{
            	postuserid:"{{ \Crypt::decrypt(\Request::segment(2)) }}"
            }
        },
        columns: [
        { data: 'postno' },
        { data: 'sdate' },
        { data: 'brand' },
        { data: 'ptype' },
        { data: 'quantity' },
        { data: 'productName' },
        { data: 'price'},
        { data: 'currency'},
        { data: 'userid'},
        { data: 'customerrefno'},
        { data: 'enableaction'},
        { data : 'edit'}
     
        ]
    });

    setInterval( function () {
        uPostingTable.ajax.reload();
    }, 3000 );
</script>