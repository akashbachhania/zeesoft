@extends('layout.master')

@section('content')
  <div class="row">
    <div class="col-md-8">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Import Postings</h3>
          <h5 style="font-weight: BOLD;">Note: Please check for duplicates prior to importing data</h5>
          @if(\Request::input('import') == "successful")

            <div class="alert alert-success">
              <button class="close" data-close="alert"></button>
              Data Import process successful! {{ \Request::input('count') }} Records Added into System.
            </div>
          @endif
        </div><!-- /.box-header -->
        <!-- form start -->
        
        <div class="box-body">
          <form role="form" action="{{ url('posting/importPostStore') }}" id="importPostForm" enctype="multipart/form-data" method="post">
            <div class="form-group row">
              <label class="col-md-4" for="itype">Data</label>
              <div class="col-md-4">
                <select class="form-control" name="itype" id="itype" onchange="sample()">
                  <option>Select Data</option>
                  <option value="Posting"> Import Posting</option>
                </select>                
              </div>
              <div class="col-md-3" id="sample-file" style="text-align:left;"></div>
            </div>
            <div class="form-group row">
              <label class="col-md-4" for="uploadFile">File Upload</label>
              <div class="col-md-4">
                <input type="file" id="uploadFile" name="uploadFile">  
              </div>
            </div>            
            <div class="form-group row">
              <label class="col-md-4" for="authchange">Authorize Changes</label>
              <div class="col-md-4">
                <input type="password" class="form-control" id="authchange" name="authchange" placeholder="Authorize Password">
              </div>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" id="submitButton" class="btn btn-primary">Submit</button>
            <!-- <button type="reset" id="cancel" class="btn btn-default">Cancel</button> -->
            <a href="{{ url('posting/postingCatalog') }}" id="cancel" class="btn btn-default">Cancel</a>
          </div>
        </form>
      </div><!-- /.box -->
    </div>    
  </div>
@include('posting.importPostJs')
@endsection