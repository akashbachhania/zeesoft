<script type="text/javascript">
    var myOffersTable = $('#myOffersTable').DataTable( {

            "ajax": {
                "url":"my-offers-ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'postno' },
            { data: 'quotationno' },
            { data: 'offerdate' },
            { data: 'IndustryName' },
            { data: 'brandName'},
            { data: 'producttype' },
            { data: 'qtypercase' },
            { data: 'productName' },
            { data: 'offerPrice'},
            { data: 'offercurrency'},
            { data: 'offertimeframe'},
            { data: 'expdate'},
            { data: 'quationstatus'}
         
            ]
        } );

        setInterval( function () {
            myOffersTable.ajax.reload();
        }, 3000 );

    $('#myOffersTable tbody').on('click', 'tr td:not(:last-child)', function () { 
        var data = myOffersTable.row(this).data();
        var page = $(this).parent().find('td a').attr('href');
        window.location.href = page;
    });

</script>