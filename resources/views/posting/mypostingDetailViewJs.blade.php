<script type="text/javascript">
 function shownoti(message){
	$(function() {

    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];

    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;

    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})

}
</script>

<script>
jQuery(document).ready(function() {
    var postno = '{{ \Request::segment("3") }}';
    var stresult ='{{ $postStatus }}';
    
    var type = "fetchallposts";
    var dataString = 'postno=' + postno + '&type=' + type;
    if(postno != ''){

        $.ajax({
            type: "POST",
            url: "{{ url('posting/fetchFunction') }}",
            data: dataString,
            cache: false,
            success: function(response){
// console.log(response);
             // response = jQuery.parseJSON(data);
             //alert(data);
             var pro_id =response.productid;
             $('#postno').html(response.postno);
             $('#userid').html(response.userid);
             $('#pdate').html(response.pdate);
             $('#pname').html(response.name);
             $('#productid').html(response.productno);
             $('#name').html(response.name);
             $('#industryid').html(response.industry);
             $('#catid').html(response.category);
             $('#subcatid').html(response.subcategory);
             $('#brandid').html(response.brand);
             $('#manufacture').html(response.manufacture);
             $('#pakaging').html(response.pakaging);
             
             $('#packlanguage').html(response.packlang);
             
             $('#shipingcondition').html(response.shipingcondition);
             $('#countryorgin').html(response.country);
             $('#procodes').html(response.procodes);
             $('#cusrefno').html(response.customerrefno);
             $('#targetprice').html(response.targetprice);
             $('#currency').html(response.currency);
             $('#location').html(response.location);
             $('#pstatus').html(response.pstatus);
             //$('#pstatus').html(stresult);
             $('#timeframe').html(response.timeframe);
            var exactexpdrange =  response.expirydate;
            //alert(exactexpdrange);
            if(exactexpdrange!='')
            {
                $('#expirydate').html(response.expirydate);
                }
                else
                {
                    $('#expirydate').html('Not Applicable');
                    }
             $('#expdate').html(response.expdate);
             $('#uom').html(response.uom);
             $('#quantity').html(response.quantity);
             $('#ptype').html(response.ptype);
             
             $("#fimges").html(response.image);

             $("#procodes").html(response.productcodes);
             MagicZoom.refresh();
             
        }
        });
    }
return false;  
});
</script>
<script>
function goBack() {
    window.history.back();
}
</script>
<script>
  jQuery(document).ready(function() {
    $("#btnnew").on("click", function() {
      $('#basic').modal('show');
      $("#authorization").val('');
    });
  });
  
   $("#conditiontbasic").on("click", function() {
   //alert();
    window.open("trems_condition.php?key=13");
   });

  function acceptnew(obj) {
      $('#termscondition').attr('checked', false);
     $('span:has(input:not(:checked))').removeClass('checked');
     $('#authorization').val('');
    $('#acceptbasic').modal('show');
    var data ="Type=search&id="+ obj;
    $.ajax({
      type:'GET',
      url:'service/my-posting-offer.php',
      cache: false,
      data: data,
      success: function(data) {
         //alert(data);
         //document.write(data);
        response = jQuery.parseJSON(data);
        $('#acceptProductno').text(response.productno);
        $('#acceptquotationno').text(response.quotationno);
        $('#acceptquoteid').val(response.quoteid);
        $('#acceptcounterid').val(response.counterid);
        $('#acceptProductName').text(response.ProductName);
        $('#acceptBrandName').text(response.BrandName);
        $('#acceptptype').text(response.ptype);
        $('#acceptquantity').text(response.quantity);
        $('#acceptprice').text(response.price);
        $('#acceptcurrency').text(response.currency);
        $('#acceptuom').text(response.uom);
        $('#accepttimeframe').text(response.timeframe);
        $('#acceptexpirydate').text(response.expirydate);
        $('#acceptpostno').val(response.postno);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //commit(false);
      }
    });
  };
  $("#acceptclose").click(function() {
    $('#acceptbasic').modal('hide');
     $('#termscondition').attr('checked', false);
     $('span:has(input:not(:checked))').removeClass('checked');
     $('#authorization').val('');
  });

  function counternew(obj) {
    $('#counternew').modal('show');
    var data = "Type=countersearch&id=" + obj;
    var dataoffer = "Type=counteroffer&id=" + obj;
    $.ajax({
      type: 'GET',
      url: 'service/my-posting-offer.php',
      cache: false,
      data: data,
      success: function(data) {
        response = jQuery.parseJSON(data);
            $('#counterquotationno').text(response.quotationno);
            $('#counterquotationnoval').val(response.quotationno);
            $('#counterproductno').val(response.productno);
            $('#counterpostno').val(response.postno);
            $('#counterquoteid').val(response.quoteid);
            $('#counterProductName').text(response.ProductName);
            $('#counterBrandName').text(response.BrandName);
            $('#counterpatype').text(response.ptype);
            $('#counterqty').text(response.quantity);
            $('#counterprice').text(response.price);
            $('#counterqty').text(response.quantity);
            $('#counterquantity').val(response.quantity);
            $('.countercrr').text(response.currency);
            $('#counteruom').text(response.uom);
            $('#countertimeframe').val(response.timeframe);
            $('#counterdaterange').val(response.expdate);
            $('#counterexpdate').text(response.expirydate);
            $('#counterdetail').text(response.detail);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //commit(false);
      }
    });
    
    $.ajax({
      type: 'GET',
      url: 'service/my-posting-offer.php',
      cache: false,
      data: dataoffer,
      success: function(data) {
        //response = jQuery.parseJSON(data);
        $('#offerhistory').html(data);
       
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //commit(false);
      }
    });
  };
   function counternewoff(obj) {
    $('#counternew').modal('show');
    var data = "Type=countersearchnew&id=" + obj;
    var dataoffer = "Type=counteroffer&counternewoff=counternewoffer&id=" + obj;
    $.ajax({
      type: 'GET',
      url: 'service/my-posting-offer.php',
      cache: false,
      data: data,
      success: function(data) {
        response = jQuery.parseJSON(data);
        $('#counterquotationno').text(response.quotationno);
        $('#counterquotationnoval').val(response.quotationno);
        $('#counterquantity').val(response.quantity);
        $('#counterproductno').val(response.productno);
        $('#counterpostno').val(response.postno);
        $('#counterquoteid').val(response.quoteid);
        $('#counterProductName').text(response.ProductName);
        $('#counterBrandName').text(response.BrandName);
        $('.countercrr').text(response.currency);
        $('#counteruom').text(response.uom);
        $('#countertimeframe').val(response.timeframe);
        $('#counterdaterange').val(response.expdate);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //commit(false);
      }
    });
    
    $.ajax({
      type: 'GET',
      url: 'service/my-posting-offer.php',
      cache: false,
      data: dataoffer,
      success: function(data) {
          //document.write(data);
        //response = jQuery.parseJSON(data);
        $('#offerhistory').html(data);
       
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //commit(false);
      }
    });
  };
</script> 
<script>
  jQuery(document).ready(function() {
  
function init() {
    // Clear forms here
    var inputVal = document.getElementById("offerqty").value = "";
      var authpass = document.getElementById("counterauthorization").value = "";
}
 });
   function change(aid)
{
  //alert(aid);
  var iname=aid.name;
  //alert(iname);
  if(iname.indexOf('http://')>=0)
  {
    //document.getElementById("bimg").src=iname;
  }
  else
  {
        $.ajax({
        type:'POST',
        data:'',
        url:'ajax.php?aid='+iname,
        success:function(data){
        //alert(data);
        var Arr=data.split('|');
            $('#image').html(Arr[0]);
        }
    });
    
  }
}
  $("#counterclose").click(function() {
    $('#counternew').modal('hide');
    $("#counterprice").val('');
    $("#counterquantity").val('');
    $("#countertimeframe").val('0');
    $("#counterdaterange").val('0');
    $("#counterauthorization").val('');
  });
</script> 
<script type="application/javascript">
  var authpass = "{{ Auth::user()->authorizedpass }}";
  /*jQuery(document).ready(function() {
    $("#go").on("click", function() {
      $('#basic').modal('show');
    });
  });*/
  $("#btnclose").click(function() {
    $('#basic').modal('hide');
    $("#authorization").val('');
  });
  $("#savecounter").click(function() {
    if ($("#counterprice").val() == '') {
      alert('Price Required');
      $("#counterprice").focus();
      return false;
    }
    if ($("#counterquantity").val() == '') {
      alert('Quantity Required');
      $("#counterquantity").focus();
      return false;
    }
    if ($("#countertimeframe").val() == '0') {
      alert('Select TimeFrame Required');
      $("#countertimeframe").focus();
      return false;
    }
    if ($("#counterdaterange").val() == '0') {
      alert('Select Expiry Date Range Required');
      $("#counterdaterange").focus();
      return false;
    }
    if ($("#counterauthorization").val() == '') {
      alert('Authorization Password Required');
      $("#counterauthorization").focus();
      return false;
    }
    if ($("#counterauthorization").val() != authpass) {
      alert('Authorization Failed');
      $("#counterauthorization").focus();
      return false;
    } else {
      var counterprice = $("#counterprice").val();
      var counterquoteid = $("#counterquoteid").val();
      var counterquantity = $("#counterquantity").val();
      // alert(counterquantity);
      var countertimeframe = $("#countertimeframe").val();
      var counterdaterange = $("#counterdaterange").val();
      var counterpostno = $("#counterpostno").val();
      var counterproductno = $("#counterproductno").val();
      var counterquotationno = $("#counterquotationnoval").val();
      var counterData = {
        Type: 'counteradd',
        counterprice: counterprice,
        counterquoteid: counterquoteid,
        counterquantity: counterquantity,
        countertimeframe: countertimeframe,
        counterdaterange: counterdaterange,
        counterpostno: counterpostno,
        counterquotationno: counterquotationno,
        counterproductno: counterproductno
      }; //Array 
      
     
      $.ajax({
        type: "POST",
        url: "service/my-posting-offer.php",
        async: false,
        data: counterData,
        dataType: 'html',
        success: function(data, status, xhr) {
          //document.write(data);
          $('#counternew').modal('hide');
          shownoti("Successfully Counter By You");
          $("#counterprice").val('');
          $("#counterquantity").val('');
          $("#countertimeframe").val('0');
          $("#counterdaterange").val('0');
          $("#counterauthorization").val('');
          setTimeout(function() {
            window.location.href = "myposting-detail-view.php?post_id={{ \Request::input('post_id') }}";
          }, 2000);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(errorThrown);
          commit(false);
        }
      });
    }
  });
  $("#saveoffer").click(function() {
    if ($("#price").val() == '') {
      alert('Price Required');
      $("#price").focus();
      return false;
    }
    if ($("#offerCurrency").val() == '') {
      alert('Currency Required');
      $("#offerCurrency").focus();
      return false;
    }
    if ($("#offerUoM").val() == '') {
      alert('UoM Required');
      $("#offerUoM").focus();
      return false;
    }
    if ($("#offerqty").val() == '') {
      alert('Quantity Required');
      $("#offerqty").focus();
      return false;
    }
    if ($("#offerlocation").val() == '') {
      alert('Location By Required');
      $("#offerlocation").focus();
      return false;
    }
    if ($("#offerdaterange").val() == '') {
      alert('Range Date Required');
      $("#offerdaterange").focus();
      return false;
    }
    if ($("#datepicker").val() == '') {
      alert('Exact Expiry Date Required');
      $("#datepicker").focus();
      return false;
    }
    if ($("#offerlanguage").val() == '') {
      alert('Language Required');
      $("#offerlanguage").focus();
      return false;
    }
    if ($("#offertimeframe").val() == '') {
      alert('Time Frame Failed');
      $("#offertimeframe").focus();
      return false;
    }
  });
  $("#saveaccept").click(function() {
    if ($('input[type=checkbox]:checked').length == 0) {
      alert("termscondition Required");
      $("#termscondition").focus();
      return false;
    }
    if ($("#authorization").val() == '') {
      alert('Authorize Password Required');
      $("#authorization").focus();
      return false;
    }
    if ($("#authorization").val() != authpass) {
      alert('Authorization Failed');
      $("#authorization").focus();
      return false;
    } else {
        
      var termcond =$('#termscondition:checked').val();
      var acceptquoteid = $("#acceptquoteid").val();
      var acceptcounterid = $("#acceptcounterid").val();
      var acceptpostno = $("#acceptpostno").val();
      var acceptData = {
        Type: 'acceptupdate',
        termcond: termcond,
        acceptquoteid: acceptquoteid,
        acceptcounterid: acceptcounterid,
        acceptpostno:acceptpostno
      }; 
      $.ajax({
        type: "POST",
        url: "service/my-posting-offer.php",
        async: false,
        data: acceptData,
        dataType: 'html',
        success: function(data, status, xhr) {
          //document.write(data);
          //alert("ok");
          shownoti("Successfully accepted the offer");
          $('#acceptbasic').modal('hide');
          $("#termcond").val('');
          $("#acceptquoteid").val('');
          $("#acceptcounterid").val('');
           setTimeout(function() {
            window.location.href ="my-posting.php?key=13";
          }, 2000);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(errorThrown);
          commit(false);
        }
      });
    }
  });
  function declinenew(obj) {
        var counterId = obj;
        var postno = '{{ \REQUEST::segment(3) }}';
        var declineval = confirm("Are you sure you wish to decline this offer ?");
        if(declineval==true)
        {
        var declineData = { Type:'decline',acceptcounterid:counterId,postno:postno }
        $.ajax({
        type: "POST",
        url: "service/my-posting-offer.php",
        async: false,
        data: declineData,
        dataType: 'html',
        success: function(data, status, xhr) {
          //document.write(data);
          shownoti("Successfully declined offer.");
          setTimeout(function() {
            window.location.href = "myposting-detail-view.php?post_id={{ \REQUEST::segment(3) }}";
          }, 2000);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(errorThrown);
          commit(false);
        }
      });   
        }
    };
    function declineoffnew(obj) {
        var quoteId = obj;
        var postno = '{{ \REQUEST::segment(3) }}';
        var declineval = confirm("Are you sure you wish to decline this offer ?");
        if(declineval==true)
        {
        var declineData = { 
        Type:'decline',
        acceptquoteid:quoteId,
        postno:postno }
        $.ajax({
        type: "POST",
        url: "service/my-posting-offer.php",
        async: false,
        data: declineData,
        dataType: 'html',
        success: function(data, status, xhr) {
          //document.write(data);
          shownoti("Successfully declined offer.");
          setTimeout(function() {
            window.location.href = "myposting-detail-view.php?post_id={{ \REQUEST::segment(3) }}";
          }, 2000);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(errorThrown);
          commit(false);
        }
      });   
        }
    };
</script>

@if(!empty(\Request::input('quotationno')))
<script>
jQuery(document).ready(function() {       
var data= {post_id:"{{ \REQUEST::segment(3) }}",quotationno:"<?php echo $_GET['quotationno']; ?>",Mode:'update_quotation_noti_status'}; //Array 
var changStatus="<?php echo $_GET['chstatus']; ?>";
 if(changStatus!='Archived')
 {
        $.ajax({
                type: "POST",
                url: "functions.php",
                data: data,
                success:function(data)
                {
                //alert(data);
                }
            });
            }
});

</script>
@endif

@if( Auth::user()->roletype != 'AD' )
<script type="text/javascript">
    
var datatable_ajax = $('#datatable_ajax').DataTable( {

    "ajax": {
        "url":"{{ url('posting/offer') }}",
        "dataSrc": "",
        "type": 'POST',
    },
    columns: [
    { data: 'quotationno' },
    { data: 'sdate' },
    { data: 'price' },
    { data: 'currency' },
    { data: 'uom' },
    { data: 'quantity' },
    { data: 'timeframe' },
    { data: 'expdate' },
    { data: 'detail' },
    { data: 'edits'}
    ]
});    
</script>
@else
<script type="text/javascript">
    
var datatable_ajax = $('#datatable_ajax').DataTable( {

    "ajax": {
        "url":"{{ url('posting/offer') }}",
        "dataSrc": "",
        "type": 'POST',
    },
    columns: [
    { data: 'quotationno' },
    { data: 'sdate' },
    { data: 'price' },
    { data: 'currency' },
    { data: 'uom' },
    { data: 'quantity' },
    { data: 'timeframe' },
    { data: 'expdate' },
    { data: 'detail' }
 
    ]
});    
</script>
@endif