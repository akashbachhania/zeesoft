<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $title or 'Ziki Trade' }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    
    <link rel="stylesheet" type="text/css" href="{{ asset("/css/toastr.min.css")}}">
    <link rel="stylesheet" href="{{ asset ("/admin-lte/plugins/datatables/dataTables.bootstrap.css") }}">
    <!-- Theme style -->
    <link href="{{ asset("/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset("/css/custom.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("/zoomingfiles/magiczoom.css")}}">
    <style type="text/css">
        select.input-sm{
            height:34px!important;   
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <!-- DataTables -->
    <script src="{{ asset ("/admin-lte/plugins/datatables/jquery.dataTables.js") }}"></script>
    <script src="{{ asset ("/admin-lte/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset ("/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <!-- FastClick -->
    <script src="{{ asset ("/admin-lte/plugins/fastclick/fastclick.min.js") }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>

    <script type="text/javascript">
        var root = "{{url('/')}}";
    </script>
    <script type="text/javascript">
        var __lc = {};
        __lc.license = 6594891;
        (function() {
          var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
          lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
    </script>
    <script type="text/javascript" src="{{ asset ("/js/custom.js") }}"></script>
    <script src="{{ asset ("/zoomingfiles/magiczoom.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/js/toastr.min.js") }}"></script>
    @if( \Request::segment(2) == 'posting_pie_chart' )

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset("/js/jquery.flot.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("/js/jquery.flot.resize.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("/js/jquery.flot.pie.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("/js/jquery.flot.stack.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("/js/jquery.flot.crosshair.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("/js/jquery.flot.categories.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("/js/charts-flotcharts.js") }}"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    @endif
    <script type="text/javascript">
      $(document).ready(function() {
           setInterval('autoRefreshDiv()', 3000); // refresh div after 5 secs
      });
      autoRefreshDiv(); // call page loading time
      function autoRefreshDiv()
      {
        var id = 1;
        $.ajax({
          url: root + "/notidash",
          type: "post",
          data: {
            id: id
          },
          cache: false,
          dataType: 'html',
          success: function(res) {
            //alert(res);
            $('#noti1').html(res);
            $('#countNoti').html($('#noti1 li').size());
          }
        });
      }
    </script>
</head>
<body class="skin-blue">
<div class="wrapper">

    <!-- Header -->
    @include('layout.header')

    <!-- Sidebar -->
    @include('layout.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or null }}
                <small style="color: red;font-weight: 600">{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <!-- <ol class="breadcrumb"> -->
                @if( \Request::segment(1) != null)
                    <!-- <li><a href="#"><i class="fa fa-dashboard"></i>{{ \Request::segment(1) }}</a></li> -->
                @endif

                @if( \Request::segment(2) != null )
                    <!-- <li class="active">{{ \Request::segment(2) }}</li> -->
                @endif

            <!-- </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('layout.footer')

</div><!-- ./wrapper -->


</body>
</html>