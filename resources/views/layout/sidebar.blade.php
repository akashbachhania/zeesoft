<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/zikiCircle.png") }}" width="45px" height="45px" style="background: #fff;" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Welcome {{ Auth::user()->userid }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="{{ url('searchresult') }}" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" class="form-control" name="searchname" autocomplete="off" id="searchname" placeholder="Search..."/>
                <span class="input-group-btn">
                  <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
          </div>
      </form>
      <!-- /.search form -->


      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- <li class="header"></li> -->
        <!-- Optionally, you can add icons to the links -->
        <li class=""><a href="{{ url('dashboard') }}"><span>Dashboard</span></a></li>
        
        @if( in_array( 'Customers',Session::get('module') ) )
            
            <li class="treeview">
                <a href="#"><span>Customers</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  @if( in_array( 'Browse Customers',Session::get('displayname') ) )

                    <li><a href="{{ url('customer/browse') }}">Browse Customers</a></li>
                  
                  @endif

                </ul>
            </li>

        @endif
        
        @if( in_array( 'Product Catalog',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Product Catalog</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @if( in_array( 'Add New Product',Session::get('displayname') ) )  
                    
                    <li><a href="{{ url('product/add') }}">Add New Product</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Browse Products',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('product/browse') }}">Browse Products</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Suggested Products',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('product/suggestedProduct') }}">Suggested Products</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Suggested Product Codes',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('product/suggestedProductCode') }}">Suggested Products Codes</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Reported Products',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('product/reportedProduct') }}">Reported Products</a></li>
                  
                  @endif
                
                </ul>
            </li>

        @endif
        
        @if( in_array( 'Postings',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Postings</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                  @if( in_array( 'Browse Postings',Session::get('displayname') ) )  
                  
                    <li><a href="{{ url('posting/postingCatalog') }}">Browse Postings</a></li>
                  
                  @endif
                  
                  @if( in_array( 'My Postings',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('posting/myPosting') }}">My Postings</a></li>
                  
                  @endif

                  @if( in_array( 'My Offers',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('posting/my-offers') }}">My Offers</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Import Posting',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('posting/import-post') }}">Import Posting</a></li>
                  
                  @endif
                
                </ul>
            </li>

        @endif

        @if( in_array( 'Product List Download',Session::get('module') ) )
        
<!--             <li class="treeview">
                <a href="#"><span>Product List Download</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                  @if( in_array( 'Products List Download',Session::get('displayname') ) )

                    <li><a href="#">Products List Download</a></li>
                  
                  @endif

                </ul>
            </li> -->
        
        @endif

        @if( in_array( 'Quote Queue',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Quote Queue</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                  @if( in_array( 'Quote Queue',Session::get('displayname') ) )  

                    <li><a href="{{ url('quoteQueue/quoteandpost') }}">Quote Queue</a></li>

                  @endif

                </ul>
            </li>

        @endif

        @if( in_array( 'Orders',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Orders</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                  @if( in_array( 'Accepted Offers',Session::get('displayname') ) )

                    <li><a href="{{url('order/acceptedoffers')}}">Accepted Offers</a></li>
                  
                  @endif

                  @if( in_array( 'Completed Orders',Session::get('displayname') ) )

                    <li><a href="{{url('order/completedoffers')}}">Completed Orders</a></li>
                  
                  @endif

                  @if( in_array( 'Cancelled Orders',Session::get('displayname') ) )

                    <li><a href="#">Cancelled Orders</a></li>
                  
                  @endif

                </ul>
            </li>

        @endif

        @if( Auth::user()->roleid == '1' )

          <li class="treeview">
            <a href="#"><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>

            <ul class="treeview-menu">

              <li {{ (\Request::segment(1) == 'create') ? 'class="white"' : '' }}><a href="{{url('create')}}">Create New Users</a></li>

            </ul>

          </li>

        @endif

        @if( in_array( 'Users',Session::get('module') ) )

            <li class="treeview {{ (\Request::segment(1) == 'userdetails') ? 'active' : '' }}">
                <a href="#"><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                  @if( in_array( 'View Users',Session::get('displayname') ) )
                  
                    <li {{ (\Request::segment(1) == 'userdetails') ? 'class="white"' : '' }}><a href="{{url('userdetails')}}">View Users</a></li>
                  
                  @endif

                  @if( in_array( 'User Activity',Session::get('displayname') ) )

                    <li {{ (\Request::segment(1) == 'activity') ? 'class="white"' : '' }}><a href="{{ url('activity') }}">User Activity</a></li>
                  
                  @endif

                  @if( in_array( 'Create New User',Session::get('displayname') ) )

                    <li {{ (\Request::segment(1) == 'create') ? 'class="white"' : '' }}><a href="{{url('create')}}">Create New Users</a></li>
                
                  @endif
                    
                </ul>
            </li>

        @endif

        @if( in_array( 'Settings',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @if( in_array( 'My Settings',Session::get('displayname') ) )

                    <li><a href="{{ url('setting/mySetting') }}">My Settings</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Notifications',Session::get('displayname') ) )

                    <li><a href="{{ url('setting/notificationSetting') }}">Message Center</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Import Data',Session::get('displayname') ) )
                    @if( Auth::user()->roletype != 'AM' )
                    <li><a href="{{ url('setting/importData') }}">Import Data</a></li>
                    @endif
                  @endif
                  
                  @if( in_array( 'Industry List',Session::get('displayname') ) )

                    <li><a href="{{ url('setting/industry') }}">Industry List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Category List',Session::get('displayname') ) )

                    <li><a href="{{ url('setting/category') }}">Category List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Sub Category List',Session::get('displayname') ) )
                  
                    <li><a href="{{ url('setting/subcategory') }}">Sub Category List</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Brands List',Session::get('displayname') ) )  
                  
                    <li><a href="{{ url('setting/brand') }}">Brands List</a></li>
                  
                  @endif
                  
                  
                  @if( in_array( 'Database Backup',Session::get('displayname') ) )  
                  
                    <li><a href="{{ url('setting/dbbackup') }}">Database Backup</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Database Restore',Session::get('displayname') ) )  
                  
                    <li><a href="{{ url('setting/dbrestore') }}">Database Restore</a></li>
                  
                  @endif
                                    
                  @if( in_array( 'Menu Permission',Session::get('displayname') ) )  
                  
                    <li><a href="{{ url('setting/menuPermission') }}">Menu Permission</a></li>
                  
                  @endif
                  
                  @if( in_array( 'Role List',Session::get('displayname') ) )  
                  
                    <li><a href="{{ url('setting/roleList') }}">Role List</a></li>
                  
                  @endif  
                  
                </ul>

            </li>

        @endif
        <li class="treeview">
            @if( Auth::user()->roletype != 'AM' )
          <a href="#"><span>Website Options</span> <i class="fa fa-angle-left pull-right"></i></a>
          @endif
          <ul class="treeview-menu">
            
            @if( Auth::user()->roletype != 'AM' )
              <li><a href="{{ url('setting/packaging') }}">Packaging</a></li>
              <li><a href="{{ url('setting/shippingConditions') }}">Shipping Conditions</a></li>                  
            @endif

            @if( in_array( 'Location',Session::get('displayname') ) )  
            
              <li><a href="{{ url('setting/location') }}">Location</a></li>
            
            @endif
            
            @if( in_array( 'Security Question List',Session::get('displayname') ) )  
            
              <li><a href="{{ url('setting/mstsecurity') }}">Security Question List</a></li>
            
            @endif
            
            @if( in_array( 'Terms & Conditions',Session::get('displayname') ) )  
            
              <li><a href="{{ url('setting/termsAndConditions') }}">Terms & Conditions</a></li>
            
            @endif

            @if( in_array( 'Website Settings',Session::get('displayname') ) )  
                  
              <li><a href="{{ url('setting/maskProductName') }}">Website Setting</a></li>
            
            @endif

            @if( Auth::user()->roletype == 'AD' )

              <li><a href="{{ url('setting/changeBanner') }}">Banner</a></li>

              <li><a href="{{ url('setting/changeNotifiles') }}">Noti Files</a></li>

            @endif

          </ul>
        </li>


        @if( in_array( 'Reports',Session::get('module') ) )

            <li class="treeview">
                <a href="#"><span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @if( in_array( 'Reports',Session::get('displayname') ) )

                    <li><a href="{{ url('report') }}">Reports</a></li>
                
                  @endif
                
                </ul>
            </li>

        @endif
    </ul><!-- /.sidebar-menu -->

</section>
<!-- /.sidebar -->
</aside>
<script>
      function searchvalidateform(){
        var error = false;
        var msg = '';
        
        if($('#searchname').val() == ""){
          error = true;
          msg+="Please enter keywords.\n";
        }
       /* if($('input#from').val() == "" || $('input#to').val() == ""){
          error = true;
          msg+="Date range must be selected.";
        }*/
        
      if(!error)  
        return true;
      else {
        alert(msg);
        return false;
      }
      }
</script>