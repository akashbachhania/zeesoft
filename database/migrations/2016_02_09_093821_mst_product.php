<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_product', function (Blueprint $table) {
            $table->increments('productid');
            $table->string('productno',25);
            $table->dateTime('date');
            $table->string('name',250);
            $table->integer('industryid');
            $table->integer('brandid');
            $table->integer('catid')->default(NULL)->nullable();
            $table->integer('subcatid')->default(NULL)->nullable();
            $table->string('pakaging',100)->default(NULL)->nullable();
            $table->string('shipingcondition',100)->default(NULL)->nullable();
            $table->string('caseweight',100)->default(NULL)->nullable();
            $table->integer('qtypercase')->default(NULL)->nullable();
            $table->text('description');
            $table->string('manufacture',100)->default(NULL)->nullable();
            $table->string('pstatus',25)->default(NULL)->nullable();
            $table->integer('mpm')->default(NULL)->nullable();
            $table->string('addedby',25);
            $table->tinyInteger('isactive')->default(1);
            $table->string('roletype',25)->default(NULL)->nullable();
            $table->string('img0',100);
            $table->string('img1',100);
            $table->string('img2',100);
            $table->string('img3',100);
            $table->string('img4',100);
            $table->string('img5',100);
            $table->string('img6',100);
            $table->string('img7',100);
            $table->integer('code1');
            $table->string('codevalue1',100);
            $table->integer('code2');
            $table->string('codevalue2',100);
            $table->integer('code3')->default(NULL)->nullable();
            $table->string('codevalue3',100)->default(NULL)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
