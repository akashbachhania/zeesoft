<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_permission', function (Blueprint $table) {
            $table->integer('rowId');
            $table->integer('pageView');
            $table->integer('pageAdd');
            $table->integer('pageEdit');
            $table->integer('pageDelete');
            $table->integer('pagePrint');
            $table->integer('pageExport');
            $table->string('userid',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
