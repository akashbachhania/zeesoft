<?php
$username       = 'root';
$password       = '';
$databasename   = 'zeesoft';
$now            = str_replace(":", "", date("Y-m-d H:i:s"));
$outputfilename = $databasename . '-' . $now . '.sql';
$outputfilename = str_replace(" ", "-", $outputfilename);
system('mysqldump -u ' . $username . ' -p' . $password . ' ' . $databasename . ' mst_industry mst_category mst_subcategory mst_brand mst_product mst_productcode mst_productimages mst_productreport post_advertisment post_quotation tm_accpted_offers post_customer mst_userlogin mst_userrole mst_location maskproname >' . $outputfilename);
$zipfname = $databasename . "_" . date("Y-m-d_H-i-s") . ".zip";
$zip      = new ZipArchive();
if ($zip->open($zipfname, ZIPARCHIVE::CREATE)) {
    $zip->addFile($outputfilename, $outputfilename);
    $zip->close();
}
// read zip file and send it to standard output
if (file_exists($zipfname)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($zipfname));
    flush();
    readfile($zipfname);
    exit;
}
?>