-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2016 at 03:37 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeesoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_product`
--

CREATE TABLE IF NOT EXISTS `mst_product` (
  `productid` int(10) UNSIGNED NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `subcatid` int(11) DEFAULT NULL,
  `pakaging` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipingcondition` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qtypercase` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `manufacture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpm` int(11) DEFAULT NULL,
  `addedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `roletype` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img0` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img4` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img5` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img6` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img7` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code1` int(11) NOT NULL,
  `codevalue1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code2` int(11) NOT NULL,
  `codevalue2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code3` int(11) DEFAULT NULL,
  `codevalue3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_product`
--

INSERT INTO `mst_product` (`productid`, `productno`, `date`, `name`, `industryid`, `brandid`, `catid`, `subcatid`, `pakaging`, `shipingcondition`, `caseweight`, `qtypercase`, `description`, `manufacture`, `pstatus`, `mpm`, `addedby`, `isactive`, `roletype`, `img0`, `img1`, `img2`, `img3`, `img4`, `img5`, `img6`, `img7`, `code1`, `codevalue1`, `code2`, `codevalue2`, `code3`, `codevalue3`, `created_at`, `updated_at`) VALUES
(1, 'OBD-685-694', '2016-02-24 06:06:30', 'test', 1, 2, 2, 2, NULL, NULL, NULL, 0, 'test', 'TATA', 'APPROVED', 0, 'UUZZ-9984', 1, 'AD', '', '', '', '', '', '', '', '', 0, '', 0, '', NULL, NULL, NULL, NULL),
(2, 'NFN-773-962', '2016-02-26 01:56:51', 'asd', 2, 3, 2, 0, NULL, 'asdasd', 'asdasd', 0, 'asdasd', 'asdasd', 'REJECTED', 0, 'FEEP-1338', 1, 'AD', '', '', '', '', '', '', '', '', 0, '', 0, '', NULL, NULL, '2016-02-25 20:26:52', '2016-02-25 20:26:52'),
(3, 'JIK-677-895', '2016-02-26 03:14:04', 'dfdsf', 2, 3, 3, 0, NULL, 'dasdad', 'wdasdad', 0, 'asdad', 'sadad', 'WAITING', 0, 'JSOL-6911', 1, 'AM', '', '', '', '', '', '', '', '', 0, '', 0, '', NULL, NULL, '2016-02-25 21:44:04', '2016-02-25 21:44:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_product`
--
ALTER TABLE `mst_product`
  ADD PRIMARY KEY (`productid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_product`
--
ALTER TABLE `mst_product`
  MODIFY `productid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
