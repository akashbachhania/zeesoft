-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2016 at 03:38 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeesoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_productprefix`
--

CREATE TABLE IF NOT EXISTS `mst_productprefix` (
  `proprefixid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_productprefix`
--

INSERT INTO `mst_productprefix` (`proprefixid`, `name`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'DIN', 1, NULL, NULL),
(2, 'MPN', 1, NULL, NULL),
(3, 'EAN', 1, NULL, NULL),
(4, 'GTIN', 1, NULL, NULL),
(5, 'IBSN', 1, NULL, NULL),
(6, 'Item Code', 1, NULL, NULL),
(7, 'Model Number', 1, NULL, NULL),
(8, 'NDC', 1, NULL, NULL),
(9, 'Other', 1, NULL, NULL),
(10, 'Part Number', 1, NULL, NULL),
(11, 'Product Code', 1, NULL, NULL),
(12, 'UPC', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_productprefix`
--
ALTER TABLE `mst_productprefix`
  ADD PRIMARY KEY (`proprefixid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_productprefix`
--
ALTER TABLE `mst_productprefix`
  MODIFY `proprefixid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
